# XCHEM EXAMPLE

## Hydrogen - Tiny

The input here provided, shows the smalles kind of calculation possible with XCHEM. Run with 1 processero it shouldn't take more than
a couple of minutes. The small size is primarily a consequence of the following paramters:

* Minimal angular momenta for GABS Gaussians
	* lMonocentric = 0 
	* kMonocentric = 0
* Small number of exponents for GABS Gaussians
	* nexp = 2

* Small active space, containing few configurations
	* ras2 = 2 1 1 0 1 1 1 0

* Small number of parent ions
	* ciroParentStates = 1

* Small number of Bsplines
	* numberNodes = 20

* Small number (one) close coupline channels
	* closeCouplingChannels=1 (0 0)
