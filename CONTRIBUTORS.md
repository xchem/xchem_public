XChem
==========

XChem is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 2.1.

XChem is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with XChem. If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2023, XChem Authors


NOTE: Some of the files distributed with XChem may be subject to a more
permissive or otherwise compatible license. Some of the files from external
sources may be subject to a more restrictive or otherwise incompatible license.


General citations
=================

* **Xchem**:
  *J. Chem. Theory Comput. 2017, 13, 2, 499–514* . DOI: [10.1021/acs.jctc.6b00907](https://doi.org/10.1021/acs.jctc.6b00907)

* **GABs basis**:
  *Phys. Rev. A. 2014, 90, 012506* . DOI: [10.1103/PhysRevA.90.012506](https://doi.org/10.1103/PhysRevA.90.012506)

* **Code development**:
  *Dataverse UAM DOI: [10.21950/GHWTML](https://doi.org/10.21950/GHWTML) *


Contributors
============

The following is a list of people who have contributed at some point to the
code of XChem. They are hereby acknowledged and collectively identified as
"XChem Authors".

Luca Argenti  
Vicent Borràs  
Inés Corral  
Jesús Gonzalez-Vazquez  
Markus Klinker  
Carlos Marante  
Fernando Martín  
