#!/bin/bash
if [ $OVERWRITE -eq 1 ]; then
	if [ -d $scatDir ]; then
		rm -rf $scatDir
	fi
	mkdir $scatDir
	mkdir $scatStorageDir
	mkdir $scatInputDir
	mkdir $scatSubmitDir
	mkdir $scatLogDir
else
	if [ ! -d $scatDir ]; then
		mkdir $scatDir
		mkdir $scatStorageDir
		mkdir $scatInputDir
		mkdir $scatSubmitDir
		mkdir $scatLogDir
	fi
fi
