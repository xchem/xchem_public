!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

import os, shutil, sys
from xchem_abstract import *
import subprocess
import xchem_keywords
from xchem_error import *
from molcas_input_generators import *
from xchem_input_generators import *
from molcas_manager import MolcasManager
from xchem_manager import XchemManager
from parsing import guga,multipole,lin_dep_kill_output
import h5_extractors
import osutils
from basis_set import BasisSet, ghost_basis_creator, multighost_basis_creator
import itertools
import submission_tools
from determinant import Determinant

class Rdm_qci(XchemModule):
	def __init__( self, input_dict, info = False):
		super().__init__(input_dict, 'rdm_qci' , info = info)
		#
		self.add_keyword( xchem_keywords.nact					  )
		self.add_keyword( xchem_keywords.ciroParentStates		  )
		self.add_keyword( xchem_keywords.symmParentStates		  )
		self.add_keyword( xchem_keywords.spinParentStates		  )
		self.add_keyword( xchem_keywords.spinAugmentedStates	  )
		self.add_keyword( xchem_keywords.spinAugmentingElectron  )
		self.add_keyword( xchem_keywords.nameSourceStates		  )
		self.add_keyword( xchem_keywords.symmSourceStates		  )
		self.add_keyword( xchem_keywords.ciroSourceStates		  )
		self.add_keyword( xchem_keywords.createSourceFromParent  )
		self.add_keyword( xchem_keywords.maxMultipoleOrder       )
		self.add_keyword( xchem_keywords.forceC1			      )
		self.add_keyword( xchem_keywords.defer ) 
		self.add_keyword( xchem_keywords.gugaDir )
		self.add_keyword( xchem_keywords.coreHoleParentIon ) 
		self.add_keyword( xchem_keywords.coreHoleSource ) 

		self._check_keywords()
		self.MOLCAS = MolcasManager( WorkDir= self.WorkDir, 
									 MOLCAS_OUTPUT = self.WorkDir)
		self.xchem = XchemManager()

	def _module_specific_checks(self):
		new_ciro_pi = []
		for ciro in self.input_dict['ciroParentStates']:
			try:
				ciro = int(ciro)
			except:
				pass
			new_ciro_pi.append(ciro)
		self.input_dict['ciroParentStates'] = new_ciro_pi
		if self.input_dict['ciroSourceStates'] is not None:
			new_ciro_src = []
			for ciro in self.input_dict['ciroSourceStates']:
				try:
					ciro = int(ciro)
				except:
					pass
				new_ciro_src.append(ciro)
			self.input_dict['ciroSourceStates'] =  new_ciro_src

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		osutils.mkdir( self.WorkDir )
		os.chdir( self.WorkDir )
		data_dirs = self.load_previous_meta_data(['basis','orbitals','integrals'])


		self.log_message('Run Molcas to extract molecular properties and generate guga table without symmetry and expanded ras3') 
		gateway = gateway_input( os.path.join(data_dirs['basis'], self.SymProject + '.guessorb.h5'), 
								 input_dict = self.input_dict)
		seward = seward_input( oneo = False, noguess = False, options = self.input_dict['sewardOptions'] )
		self.MOLCAS.run(gateway + seward, Project = self.DesymProject)

		dontComputeGuga = False
		if self.input_dict['gugaDir']:
			gugaDir = osutils.getDirPath( self.input_dict['gugaDir'] )
			self.log_message('Using guga tables from {}'.format(gugaDir))
			for gugaFile in os.listdir(gugaDir):
				osutils.symlink(os.path.join(gugaDir, gugaFile), gugaFile)
			dontComputeGuga = True
			
		if os.path.isfile('augmented.guga') and dontComputeGuga:
			self.log_message('Found file augmented.guga. Not recalculating.')
		else:
			osutils.symlink(os.path.join( data_dirs['orbitals'],self.SymProject + '.DeSymOrb' ), 'INPORB')
			rasscf = rasscf_input( self.input_dict, symm = 1, ciro = 1, spin=self.input_dict['spinAugmentedStates'],
								   lumo = True, cion = True, prsd = True, prwf = 0.0, desym = True, expand_ras3 =2, tigh = 1000, cimx = 1,
								   expand_nact = [1,0,1])
			guga_parser = guga.GugaParser( 'augmented.guga' )
			self.MOLCAS.run(rasscf, Project = self.DesymProject, guga_only = True, parsefct = guga_parser )

		self.meta_dict['permanent_dipole'] = h5_extractors.permanent_dipole( self.DesymProject + '.guessorb.h5' )
		self.meta_dict['nuclear_repulsion'] = h5_extractors.nuclear_repulsion( self.DesymProject + '.guessorb.h5' )
		self.log_message('Nuclear repulsion = {:10.5f} au'.format(self.meta_dict['nuclear_repulsion']))
		self.log_message('Permanent dipole = {}'.format( " ".join( [ "{:10.5f} au".format(x) for x in self.meta_dict['permanent_dipole'] ])))

		self.log_message('Compute CI vectors of PI states, and apply creation operators for all orbitals in expanded active space')	
		osutils.symlink(os.path.join(os.environ['XCHEM_SUBMIT'],self.input_dict['orbitalFile'] ), 'INPORB')
		for f in [ self.SymProject + s for s in ['.RunFile','.OneInt','.OrdInt'] ]:
			osutils.symlink(os.path.join(data_dirs['basis'],f), f)
		self.meta_dict['numParentIons'] = []
		PI_h5_files = []
		for iPI,symm in enumerate(self.input_dict['symmParentStates']):
			#JOB
			self.log_message('Consider symmetry: {}'.format(symm), priority=2)
			ci_file = str(iPI+1) + '.PI.csf'
			det_file = str(iPI+1) + '.PI.det'
			guga_file = str(iPI+1) + '.PI.guga'
			guga_parser = guga.GugaParser( guga_file )
			spin = self.input_dict['spinParentStates'][iPI]
			spin_electron = self.input_dict['spinAugmentingElectron'][iPI]
			hexs = iPI + 1 in self.input_dict['coreHoleParentIon']
			ciro = self.input_dict['ciroParentStates'][iPI]
			self.log_message('Create guga table',priority=3)
			if os.path.isfile( guga_file ) and dontComputeGuga:
				self.log_message('Found file {}. Not recalculating.'.format(guga_file), priority = 3)
			else:
				rasscf = rasscf_input( self.input_dict, symm = symm, ciro = 1, spin = spin,
								  	   lumo = True, prsd = True, prwf = 0.0, tigh = 1000, cimx = 1, cion = True, hexs = hexs)
				self.MOLCAS.run( rasscf, Project = self.SymProject, parsefct = guga_parser, guga_only = True )
			if type(ciro) is int:
				self.meta_dict['numParentIons'].append(ciro)
				self.log_message('Compute CI vectors in CSFs',priority=3)
				rasscf = rasscf_input( self.input_dict, symm = symm, ciro = ciro, spin = spin,
									   lumo = True,cion = True, hexs = hexs, cimx = 200)
				self.MOLCAS.run( rasscf, Project = self.SymProject, guga_only = False )
				h5_extractors.ci_vectors( self.SymProject + '.rasscf.h5', ci_file )
				shutil.move( self.SymProject + '.rasscf.h5', str(iPI+1) + '.PI.h5' )
				self.log_message('Transform to determinants',priority=3)
				csf2det = csf2det_input( ci_file = ci_file, guga_file = guga_file, det_file = det_file)
				self.xchem.run('CSF2Det',csf2det)
			else:
				custom_det_file = osutils.getFilePath( ciro )
				osutils.symlink( custom_det_file, det_file )
				determinant = Determinant( custom_det_file )
				rasscf = rasscf_input( self.input_dict, symm = symm, ciro = determinant.num_roots, spin = spin,
								  	   lumo = True, tigh = 1000, cimx = 1, cion = True )
				self.MOLCAS.run( rasscf, Project = self.SymProject, guga_only = True )
				self.meta_dict['numParentIons'].append( determinant.num_roots )
				self.log_message('Transform states read from file to CSFs in expanded active space',priority=3)
				det2csf = det2csf_input( ci_file = ci_file, 
										 det_file = det_file,
										 guga_file = guga_file )
				self.xchem.run('Det2CSF', det2csf)
				h5_extractors.load_ci_vector_to_h5( self.SymProject + '.rasscf.h5', ci_file, str(iPI+1) + '.PI.h5')
			PI_h5_files.append(str(iPI+1) + '.PI.h5')
			

			self.log_message('Create augmented states',priority=3)
			augment = augment_input( det_file, self.input_dict, spin_electron)
			self.xchem.run('augment',augment)
			ci_file=[]
			det_file=[]
			self.xchem.input_cache = '.xchem.D2C_{}'.format(iPI+1)
			for iorb in range(num_orb_active(self.input_dict)+2):
				ci_file.append( '{}.aug_{:02d}.PI.csf'.format(iPI+1,iorb+1) )
				det_file.append( 'augment.{}-{:02d}.det'.format(spin_electron[0], iorb+1) )
			det2csf = det2csf_input( ci_file = ci_file,
									 det_file = det_file,
									 guga_file = 'augmented.guga')
			self.log_message('Transform state augmented to CSFs')
			self.proc_list.waitForFreeProc( os.environ['OMP_NUM_THREADS'] )
			p = self.xchem.run('Det2CSF', det2csf, wait = False)
			self.proc_list.add( p )
		self.proc_list.waitForAllProcs()

		self.log_message('Create CI vecotrs of source states in expanded active space')
		self.meta_dict['numSourceStates'] = []
		for iSRC,name in enumerate(self.input_dict['nameSourceStates']):
			self.log_message('Consider source states: {}'.format(name), priority=2)
			ci_file = str(iSRC+1) + '.SRC.csf'
			det_file = str(iSRC+1) + '.SRC.det'
			guga_file = str(iSRC+1) + '.SRC.guga'
			guga_parser = guga.GugaParser( guga_file )
			symm = self.input_dict['symmSourceStates'][iSRC]
			ciro = self.input_dict['ciroSourceStates'][iSRC]
			hexs = iSRC + 1 in self.input_dict['coreHoleSource']

			if os.path.isfile( guga_file ) and dontComputeGuga:
				self.log_message('Found file {}. Not recalculating.'.format(guga_file), priority = 3)
			else:
				self.log_message('Compute CI vectors and guga table', priority=3)
				rasscf = rasscf_input( self.input_dict, symm=symm,ciro=1,spin=self.input_dict['spinAugmentedStates'],
									   lumo=True,cion=True,prsd=True,prwf=0.0, tigh = 1000, cimx = 1, expand_nact = [1,0,0] )	
				self.MOLCAS.run(rasscf, Project=self.SymProject,parsefct=guga_parser, guga_only = True)

			if type(ciro) is int:
				self.meta_dict['numSourceStates'].append(ciro)
				rasscf = rasscf_input( self.input_dict, symm=symm,ciro=ciro,spin=self.input_dict['spinAugmentedStates'],
									   lumo=True,cion=True, expand_nact = [1,0,0], hexs = hexs, cimx = 200 )	
				self.MOLCAS.run(rasscf, Project=self.SymProject)
				h5_extractors.ci_vectors( self.SymProject + '.rasscf.h5', ci_file )
				csf2det = csf2det_input( ci_file = ci_file, guga_file = guga_file, det_file = det_file)			
				self.log_message('Transform to determinants',priority=3)
				self.xchem.run('CSF2Det',csf2det)
				ci_file = str(iSRC+1) + '.ras3.SRC.csf'
				self.log_message('Transform to CSFs in expanded active space',priority=3)
				det2csf = det2csf_input( ci_file = ci_file, det_file=det_file,guga_file='augmented.guga')
				self.xchem.run('Det2CSF',det2csf)
			else:
				custom_det_file = osutils.getFilePath( ciro )
				osutils.symlink( custom_det_file, det_file )
				determinant = Determinant( custom_det_file )
				self.meta_dict['numSourceStates'].append( determinant.num_roots )
				self.log_message('Transform states read from file to CSFs in expanded active space',priority=3)
				ci_file = str(iSRC+1) + '.ras3.SRC.csf'
				det2csf = det2csf_input( ci_file = ci_file, 
										 det_file = det_file,
										 guga_file = 'augmented.guga' )
				self.xchem.run('Det2CSF', det2csf)

		self.log_message('Turn CIs into CSFs (augemnted PIs and source) into determinants')
		csfs2csf = csfs2csf_input(ciroParentStates=self.input_dict['ciroParentStates'], norb=num_orb_active( self.input_dict ) +2 ,
								  nameSourceStates = self.input_dict['nameSourceStates'], result_file = 'xchem.csf' )
		self.xchem.run('CSFs2CSF',csfs2csf)
		csf2det = csf2det_input( ci_file = 'xchem.csf', det_file='xchem.det', guga_file='augmented.guga',ascii=False)
		self.xchem.run('CSF2Det',csf2det)
		self.xchem.run('detSplit',xchem_cmd_line_input = 'xchem.det')

		self.log_message('Compute PI multipoles in length gauge')
		multipole_parser = multipole.MultipoleParser( num_parent_ions= sum(self.meta_dict['numParentIons']) )
		rassi, file_list = rassi_input(onel = True, mein = True, operator = 'Mltpl', order = self.input_dict['maxMultipoleOrder'], return_file_list = True, files = PI_h5_files)
		self.MOLCAS.run(rassi, Project = self.SymProject )
		osutils.concatenate( file_list = file_list , parse_fct = multipole_parser , rm_src = True )
		multipole_parser.write(os.path.join(self.DataDir,'multipoles.dat'))

		self.log_message('Compute PI multipoles in velocity gauge')
		multipole_parser = multipole.MultipoleParser( num_parent_ions= sum(self.meta_dict['numParentIons']) )
		rassi, file_list = rassi_input(onel = False, mein = True, operator = 'Velocity', order = 1, return_file_list = True, files = PI_h5_files)
		self.MOLCAS.run(rassi, Project = self.SymProject )
		osutils.concatenate( file_list = file_list , parse_fct = multipole_parser , rm_src = True )
		multipole_parser.write(os.path.join(self.DataDir,'velocities.dat'))
		h5_extractors.sfs_energies( self.SymProject + '.rassi.h5', 'En' )
		self.out_files = []
		self.add_out_files( 'En_*' )
		self.mv_out_files()
		self.out_files = []

		nStates = ( num_orb_active( self.input_dict ) + 2 ) * sum(self.meta_dict['numParentIons'])
		if self.input_dict['nameSourceStates']: 
			nStates += sum(self.meta_dict['numSourceStates']) 
		self.log_message('Compute reduced density matrices for pairs of all {} states.'.format(nStates))
		trd1_file_list = open('trd1_file_list','w')
		trd2_file_list = open('trd2_file_list','w')
		if self.input_dict['defer']:
			new_job = submission_tools.need_new_job( int(nStates * ( nStates+1 ) / 2), int(os.environ['XCHEM_MAX_NUM_NODES']))
		for ijob, (jState,iState) in enumerate(itertools.combinations_with_replacement( range(1, nStates + 1), r = 2 )):
			if iState == jState:
				self.log_message('Compute reduced density matrices for states i={} and j>={}'.format(iState,iState), priority=2)
			trd1_file = '{:03d}-{:03d}.trd1'.format(iState,jState)
			trd1 = trd1_element_input(trd_file = trd1_file, det_file = 'xchem.det', iState=iState, jState=jState, norb=num_orb_occ(self.input_dict)+2)	
			trd2_file = '{:03d}-{:03d}.trd2'.format(iState,jState)
			trd2 = trd2_element_input(trd_file = trd2_file, det_file = 'xchem.det', iState=iState, jState=jState, norb=num_orb_occ(self.input_dict)+2)	
			if self.input_dict['defer']:
				if new_job[ijob]:
					self.job_list.add_job( in_files = { self.WorkDir : [r'xchem\.det_\d{3}'] }, out_files = { self.WorkDir : [r'\d{3}-\d{3}\.trd[12]']})
				task1 = self.xchem.task('trd1Element',trd1)
				task2 = self.xchem.task('trd2Element',trd2)
				self.job_list.add_task( task1 )
				self.job_list.add_task( task2 )
			else:
				self.xchem.run('trd1Element',trd1)
				self.xchem.run('trd2Element',trd2)
			trd1_file_list.write('{}\n'.format(trd1_file))
			trd2_file_list.write('{}\n'.format(trd2_file))

		if self.input_dict['defer']:
			self.job_list.next_block()

		trd1_file_list.close()
		trd2_file_list.close()
		
		operators = [('ham1E','T', '1.0', None ), ('ov','T', nelectrons(self.input_dict) + 1 , None), ('kin','T','1.0', None)]
		for i in range(1,4):
			operators.append( ('dip{}'.format(i),'T','1.0', self.meta_dict['permanent_dipole'][i-1] ))	
			operators.append( ('vel{}'.format(i),'F','1.0', None ))	

		self.log_message('Compute total one and two electron reduced density matrices')
		trd1_build = trd1_build_input(trd_file_list='trd1_file_list',trd_file='trd1')
		if self.input_dict['defer']:
			if self.input_dict['saveTwoElInt']:
				in_files = { 
					self.WorkDir : [r'\d{3}-\d{3}\.trd[12]',r'trd[12]_file_list', r'nuclearRepulsion',r'.*bas','lk\.dat'], 
					data_dirs['orbitals'] : [r'pqrs\.int', r'\.orb\.int$', r'basis_block\.info', r'orbs\.sym',r'mpcg\.bin']
					}
				out_files = {
					self.DataDir :[ r'(ov|Ham|dip.|vel.|kin)_\d{3}-\d{3}\.out','(symmetry|interface.*|ham\.eVals)']
					}
			else:
				in_files = { 
					self.WorkDir : [r'\d{3}-\d{3}\.trd[12]',r'trd[12]_file_list', r'nuclearRepulsion',r'.*bas','lk\.dat'], 
					data_dirs['integrals'] : [r'pqrs\.int'], data_dirs['orbitals'] : [ r'\.orb\.int$', r'basis_block\.info', r'orbs\.sym',r'mpcg\.bin']
					}
				out_files = {
					self.DataDir :[ r'(ov|Ham|dip.|vel.|kin)_\d{3}-\d{3}\.out','(symmetry|interface.*|ham\.eVals)']
					}
			self.job_list.add_job( in_files = in_files, out_files = out_files )
			task = self.xchem.task('trd1Build', trd1_build)
			self.job_list.add_task( task )
		else:
			self.xchem.run('trd1Build', trd1_build)
		trd2_build = trd2_build_input(trd_file_list='trd2_file_list',trd_file='trd2')
		if self.input_dict['defer']:
			task = self.xchem.task('trd2Build', trd2_build)
			self.job_list.add_task( task )
		else:
			self.xchem.run('trd2Build', trd2_build)

		osutils.lnregexls( data_dirs['orbitals'], self.WorkDir, 'orb.int$')
		if self.input_dict['saveTwoElInt']:
			for f in ['pqrs.int', 'orbs.sym']:
				osutils.symlink( os.path.join( data_dirs['orbitals'], f) , f)
		else:
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'orbs.sym') , 'orbs.sym')
				osutils.symlink( os.path.join( data_dirs['integrals'], 'pqrs.int') , 'pqrs.int')
		self.log_message('Compute operator matrix elements of two electron hamiltonian from orbital integrals and two electron reduced density matrix')
		twoIntOrb2OperatorMatrix = twoIntOrb2OperatorMatrix_input( self.input_dict, self.meta_dict )
		if self.input_dict['defer']:
			task = self.xchem.task('twoIntOrb2OperatorMatrix_new',  twoIntOrb2OperatorMatrix)
			self.job_list.add_task( task )
		else:
			self.xchem.run('twoIntOrb2OperatorMatrix_new',  twoIntOrb2OperatorMatrix)

		
		self.log_message('Compute operator matrix elements of one electron operators from orbital integrals and two electron reduced density matrix')
		for name, hermitian, scale_factor, dipole in operators :
			self.log_message('Consider operator: {}'.format(name),priority=2)
			oneIntOrb2OperatorMatrix = oneIntOrb2OperatorMatrix_input( self.input_dict, name, self.meta_dict )
			if self.input_dict['defer']:
				task = self.xchem.task('oneIntOrb2OperatorMatrix_new',  
							    xchem_file_input = oneIntOrb2OperatorMatrix, 
								xchem_cmd_line_input = '{} {}'.format(scale_factor, hermitian))
				self.job_list.add_task( task ) 
			else:
				task = self.xchem.run('oneIntOrb2OperatorMatrix_new',  
							    xchem_file_input = oneIntOrb2OperatorMatrix, 
								xchem_cmd_line_input = '{} {}'.format(scale_factor, hermitian))
			if dipole and abs(dipole) > 1e-12:
				subtractDiagonal = subtractDiagonal_input( dipole, name )
				if self.input_dict['defer']:
					task = self.xchem.task('subtractDiagonal',subtractDiagonal)
					self.job_list.add_task( task )
				else:
					self.xchem.run('subtractDiagonal',subtractDiagonal)

		self.log_message('Compute complete Hamiltonian')
		sumOneIntTwoIntNuclear = sumOneIntTwoIntNuclear_input( self.meta_dict )
		if self.input_dict['defer']:
			task = self.xchem.task('sumOneIntTwoIntNuclear', sumOneIntTwoIntNuclear)
			self.job_list.add_task( task )
		else:
			self.xchem.run('sumOneIntTwoIntNuclear', sumOneIntTwoIntNuclear)
		operators[0] = ('Ham',None,None,None )

		self.log_message('Break up total operator matrices into blocks according to symmetry')
		symSplit = symSplit_input(self.input_dict, self.meta_dict)
		for name,_,_,_ in operators:
			self.log_message('Consider operator: {}'.format(name),priority=2)
			if name == 'ov':
				xchem_cmd_line_input = '{}.state.int T'.format(name)
			else:
				xchem_cmd_line_input = '{}.state.int'.format(name)
			if self.input_dict['defer']:
				task = self.xchem.task('symSplit',xchem_file_input = symSplit, xchem_cmd_line_input = xchem_cmd_line_input)
				self.job_list.add_task( task )
			else:
				self.xchem.run('symSplit',xchem_file_input = symSplit, xchem_cmd_line_input = xchem_cmd_line_input)
			self.add_out_files( '{}_*-*.out'.format(name)  )
		self.add_out_files('symmetry')

		self.log_message('Create interface(s) for computations with BSplines')
#VICENT MODIFICATION
		if self.input_dict['multighost']:
			ghost_bases = multighost_basis_creator( self.input_dict )
		else:
			ghost_bases = ghost_basis_creator( self.input_dict )
#END OF VICENT MODIFICATION
		for basis in ghost_bases:
			basis.writeGaussians()
		GABS_interface_sym = GABS_interface_sym_input(self.input_dict, self.meta_dict)
		for iirrep in range(1,len(self.meta_dict['irreps']) +1 ):
			if  self.input_dict['defer']:
				task = self.xchem.task( 'GABS_interface_sym', 
								xchem_file_input = GABS_interface_sym,
								xchem_cmd_line_input= str(iirrep) )
				self.job_list.add_task( task )
			else:
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'mpcg.bin'), 'mpcg.bin')
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'basis_block.info'), 'basis_block.info')
				self.xchem.run( 'GABS_interface_sym', 
								xchem_file_input = GABS_interface_sym,
								xchem_cmd_line_input= str(iirrep) )
		self.add_out_files( 'interface*' )

		self.log_message('Diagonalize Hamiltonian')
		computeEvals = computeEvals_input()
		if  self.input_dict['defer']:
			task = self.xchem.task('computeEvals',computeEvals)
			self.job_list.add_task( task )
		else:
			self.xchem.run('computeEvals',computeEvals)
		self.add_out_files( 'ham.eVals' )

		if self.input_dict['defer']: 
			self.job_list.dump( os.path.join( self.DataDir, 'rdm_qci.submission.json'), uid = uid )
		else:
			self.mv_out_files()
		self.dump_meta_data()
