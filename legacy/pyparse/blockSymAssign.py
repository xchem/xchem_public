!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

f1name=input()
f2name=input()
f3name=input()
f1handel=open(f1name.strip(' \t'))
f2handel=open(f2name.strip(' \t'))
f3handel=open(f3name.strip(' \t'))

block=[]
sym=[]
c=0

for i,line in enumerate(f3handel):
    if (i==0):
        pntGrp=line.strip('  \t\n')
    if (i==1):
        print(len(line.strip('  \t\n').split(' ')),pntGrp)
        print(line.strip(' \n'))
    if (i>2):
        line=line.strip(' \t')
        for s,n in enumerate(line.split(' ')):
            for j in range(int(n)):
                print(s+1," ", end = "")
print(" ")

for line in f1handel:
    c=c+1
    block.append(tuple(line.split( )))

count=0
for line in f2handel:
    count=count+1
    sym.append(tuple(line.split( )))

c1=False
if (count==0):
    c1=True

print(c)
for s,o1,l,m,k,g in block:
    if (c1):
        print(s,o1,l,m,k,g,1)
    else:
        for o2,i in sym:
            if (o1==o2):
                print(s,o1,l,m,k,g,i)
