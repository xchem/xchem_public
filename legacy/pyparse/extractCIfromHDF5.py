!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import h5py
import sys

filename = sys.argv[1]
f = h5py.File(filename, 'r')

data = f["CI_VECTORS"]
nRoot = data.shape[0]
nCSF = data.shape[1]

print("%05d %010d" % (nRoot,nCSF))
for iRoot in range(nRoot):
	for iCSF in range(nCSF):  
		print("%.20E " % data[iRoot,iCSF],end='')
	print("")
