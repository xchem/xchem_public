!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import pyparsing as pp
import sys
import re
fname = sys.argv[1]

plusorminus = pp.Literal('+') | pp.Literal('-')
number = pp.Word(pp.nums)
word = pp.Word(pp.alphas)
point = pp.Literal('.')
e = pp.CaselessLiteral('E')
integer = pp.Combine( pp.Optional(plusorminus) + number )
floatnum = pp.Combine( integer + pp.Optional( point + pp.Optional(number) ) + pp.Optional( e + integer ))

ciConf = pp.OneOrMore(pp.Word("0ud2"), stopOn=pp.Optional(plusorminus)+pp.Literal("0."))
detConf = pp.Word("0ab2")
weight = number.setResultsName('numerator') + pp.Literal('/') + number.setResultsName('denominator')

header = pp.SkipTo(pp.Literal('conf/sym'),include=True) + pp.OneOrMore(number) + word + word

ci = integer + ciConf.setResultsName('conf') + floatnum + floatnum

det = plusorminus.setResultsName('sign') + pp.Literal('sqrt(') + weight + pp.Literal(')') + pp.Literal('|') + detConf.setResultsName('conf') + pp.Literal('|')

gugaEntry = ci + pp.OneOrMore( pp.Group( det ) ).setResultsName('dets') 
gugaTable = pp.OneOrMore( pp.Group( gugaEntry ) ).setResultsName('guga')
parser = pp.Optional( header ) + gugaTable

ress = parser.parseFile( fname )

detConfReplace={"2":"#","a":"+","b":"-","0":"."}
detConfReplace = dict((re.escape(k), v) for k, v in detConfReplace.items())
pattern = re.compile("|".join(detConfReplace.keys()))

nDet = len(ress['guga'])
nCAS = len(ress['guga'][0]['dets'][0]['conf'])

print(nDet,nCAS)
for i,res in enumerate(ress['guga']):
	nDet = len(res['dets'])
	fconf = str("").join(res['conf'])
	print(fconf, nDet, i+1)
	for det in res['dets']:
		sgn = det['sign']
		numer = int(det['numerator'])
		denom = int(det['denominator'])
		pConf = pattern.sub(lambda m: detConfReplace[re.escape(m.group(0))], det['conf'])
		print("%c%i %i %s " % (sgn,numer,denom,pConf),end="")
	print("")
