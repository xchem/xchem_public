!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import pyparsing as pp
from itertools import combinations
import sys
fname = sys.argv[1]

plusorminus = pp.Literal('+') | pp.Literal('-')
point = pp.Literal('.')
number = pp.Word(pp.nums)
integer = pp.Combine( pp.Optional(plusorminus) + number )

spin = pp.Word(pp.nums)
l = integer 
m = integer

comment = pp.Literal('#') + pp.restOfLine + pp.lineEnd

a   = pp.Combine(pp.CaselessLiteral('A'))
ap  = pp.Combine(pp.CaselessLiteral('A') + pp.Literal(''' ' '''))
app = pp.Combine(pp.CaselessLiteral('A') + pp.Literal(''' '' '''))
b   = pp.Combine(pp.CaselessLiteral('B'))
ag  = pp.Combine(pp.CaselessLiteral('Ag'))
au  = pp.Combine(pp.CaselessLiteral('Au'))
bg  = pp.Combine(pp.CaselessLiteral('Bg'))
bu  = pp.Combine(pp.CaselessLiteral('Bu'))
a1  = pp.Combine(pp.CaselessLiteral('A')+pp.Literal('1'))
a2  = pp.Combine(pp.CaselessLiteral('A')+pp.Literal('2'))
b1  = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('1'))
b2  = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('2'))
b3  = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('3'))
b1g = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('1')+pp.CaselessLiteral('g'))
b2g = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('2')+pp.CaselessLiteral('g'))
b3g = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('3')+pp.CaselessLiteral('g'))
b1u = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('1')+pp.CaselessLiteral('u'))
b2u = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('2')+pp.CaselessLiteral('u'))
b3u = pp.Combine(pp.CaselessLiteral('B')+pp.Literal('3')+pp.CaselessLiteral('u'))

irrep = a ^ ap ^ app ^ b   ^ ag  ^ au  ^ bg  ^ bu  ^ a1  ^ a2  ^ b1  ^ b2  ^ b3  ^ b1g ^ b2g ^ b3g ^ b1u ^ b2u ^ b3u 

CCESym = pp.Literal('[') + pp.Combine(spin + irrep).setResultsName("sym") + pp.Literal(']') 
PI = pp.Combine(spin + irrep + point + number)
electron = pp.Combine(l + pp.ZeroOrMore(" ") + m)
electrons = pp.delimitedList(electron, delim = ",")
channel =  pp.Group(PI.setResultsName('PI') + pp.Literal('(') + \
	pp.Group(electrons).setResultsName("electrons") + pp.Literal(')'))

CCE = CCESym + pp.Literal('{') + pp.OneOrMore(channel).setResultsName("channels") + pp.Literal('}')

head = pp.SkipTo(CCESym)

parser = head.setResultsName('head') + pp.OneOrMore(pp.Group(CCE)).setResultsName("CC")

parser.ignore( comment )

parserResult = parser.parseFile( fname )

def writeCC(f, sym, PI, e, sym2 = None, PI2 = None, e2 = None, head = None):
	ccf = open(f, 'w')
	if head is not None:
		ccf.write(head)
	if all(p is None for p in (sym2,PI2,e2)):
		ccf.write("[%s]{\n" %(sym))	
		ccf.write("%s ( %s )\n" %(PI,e))
		ccf.write("}\n")
	else:
		if sym == sym2:
			if PI == PI2:
				ccf.write("[%s]{\n" %(sym))	
				ccf.write("%s ( %s, %s )\n" %(PI,e,e2))
				ccf.write("}\n")
			else:
				ccf.write("[%s]{\n" %(sym))	
				ccf.write("%s ( %s )\n" %(PI,e))
				ccf.write("%s ( %s )\n" %(PI2,e2))
				ccf.write("}\n")
		else:
			ccf.write("[%s]{\n" %(sym))	
			ccf.write("%s ( %s )\n" %(PI,e))
			ccf.write("}\n")
			ccf.write("[%s]{\n" %(sym2))	
			ccf.write("%s ( %s )\n" %(PI2,e2))
			ccf.write("}\n")
	ccf.close()

PWCs = []
header = parserResult['head'][0]

for cc in parserResult['CC']:
	for channel in cc['channels']:
		for electron in channel['electrons']:
			PWCs.append((cc['sym'],channel['PI'],electron))

for i,(sym,PI,e) in enumerate(PWCs):
	closeCouplingFileName = fname+"_"+str(i)+"-"+str(i)
	writeCC(closeCouplingFileName, sym, PI, e, head=header)

for PWC1,PWC2 in combinations(enumerate(PWCs), 2):
	i,(sym,PI,e) = PWC1
	j,(sym2,PI2,e2) = PWC2
	closeCouplingFileName = fname+"_"+str(i)+"-"+str(j)
	writeCC(closeCouplingFileName, sym, PI, e, sym2, PI2, e2, head=header)

ccfInfo = open(".nchannel","w")
ccfInfo.write(str(len(PWCs)))
ccfInfo.close()

