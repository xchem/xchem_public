!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import pyparsing as pp
import sys
import re
fname = sys.argv[1]

## Define different kinds of numbers
plusorminus = pp.Literal('+') | pp.Literal('-')
number = pp.Word(pp.nums)
word = pp.Word(pp.alphas)
point = pp.Literal('.')
e = pp.CaselessLiteral('E')
integer = pp.Combine( pp.Optional(plusorminus) + number )
floatnum = pp.Combine( integer + pp.Optional( point + pp.Optional(number) ) + pp.Optional( e + integer ))

## Define line that containes number of CSFs
nCSFsPattern = pp.Literal('Number of CSFs') + integer.setResultsName('nCSFs')

# Define CSF configurations Det configuraions and rational weights
ciConf = pp.OneOrMore(pp.Word("0ud2"), stopOn=pp.Optional(plusorminus)+pp.Literal("0."))
detConf = pp.Word("0ab2")
weight = number.setResultsName('numerator') + pp.Literal('/') + number.setResultsName('denominator')

# Define a guga entry (CSF + sum(weight*det)
ci = integer + ciConf.setResultsName('conf') + floatnum + floatnum
ciTail = pp.Literal('Natural orbitals and occupation numbers') | pp.Literal('XCHEM remark: gugaOnly exists') | pp.Literal("printout of CI-coefficients larger than")
det = plusorminus.setResultsName('sign') + pp.Literal('sqrt(') + weight + pp.Literal(')') + pp.Literal('|') + detConf.setResultsName('conf') + pp.Literal('|')
gugaStart = pp.Literal('conf/sym') + pp.OneOrMore(integer) + pp.Literal("Coeff  Weight")

# Set up replacement dictonary to transform Molcas format to format used in Jesus code
detConfReplace={"2":"#","a":"+","b":"-","0":"."}
detConfReplace = dict((re.escape(k), v) for k, v in detConfReplace.items())
pattern = re.compile("|".join(detConfReplace.keys()))

with open(fname) as f:
	nDet = 0
	nCSFs = None
	dets = []
	inHead = True
	iCSF=0
	finished = False
	for line in f:
		if finished:
			break
		if nCSFs is None:
			results = nCSFsPattern.scanString( line )
			for res,_,_ in results:
				nCSFs = int(res['nCSFs'])
		else:
			if inHead:
				results = gugaStart.scanString(line)
				for res,_,_ in results:
					inHead = False
			else:
				if iCSF < nCSFs:
					resultCSF = ci.scanString( line )
				else:
					resultCSF = ciTail.scanString( line )

				resultDet = det.scanString( line )
				for res,_,_ in resultDet:
					nDet += 1
					dets.append(res)

				for res,_,_ in resultCSF:
					if iCSF > 0:
						if iCSF == 1:
							nCAS = len(fconf)
							print(nCSFs,nCAS)
						print(fconf,nDet,iCSF)
						for d in dets:
							sgn = d['sign']
							numer = int(d['numerator'])
							denom = int(d['denominator'])
							pConf = pattern.sub(lambda m: detConfReplace[re.escape(m.group(0))], d['conf'])
							print("%c%i %i %s " % (sgn,numer,denom,pConf),end="")
						print("")
						if iCSF % 100 == 0: sys.stdout.flush()
						dets = []
						nDet = 0
					iCSF += 1
					if iCSF <= nCSFs:
						fconf = str("").join(res['conf'])
					else:
						finished = True
