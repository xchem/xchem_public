!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

import sys
fname=sys.argv[1]
fhandel=open(fname.strip(' \t'))

end=False
c=0
for line in fhandel:
	if ' Number of electrons in active shells ' in line:
		nElec=int(line.rsplit(None,1)[-1])
	if ' Number of active orbitals ' in line:
		norb=int(line.rsplit(None,1)[-1])
	if ' Number of CSFs ' in line:
		nCSF=int(line.rsplit(None,1)[-1])
		if nCSF==1 and nElec==2*norb:
			print nCSF,norb
			print "2" * norb,
			print "1 1"
			print "1 1 ",
			print "#" * norb
			break	
	dets=[]
	if ' SGUGA ' in line:
		print nCSF,norb
		next(fhandel)
		while True:
			record_string=next(fhandel)
			record_list=record_string.split()
			csfRecord=True
			for iCSF in record_list:
				try:
					iCSF=int(iCSF)
				except:
					csfRecord=False
				if csfRecord or end:
					nDet=len(dets)
					if nDet > 0:
						c=c+1
						print csf,nDet,c
						for det in dets: 
							print det," ",
						print ""
					if end: sys.exit()
					csf=""
					for i,s in enumerate(reversed(record_list)):
						if ")" in s: break
						if i > 1: csf+=s[::-1]
					csf=csf[::-1]
					dets=[]
				else:
					if iCSF in '+-':
						record_string=record_string.replace("/"," ")
						record_string=record_string.replace("|","")
						detInfo=record_string.split()
						slater=detInfo[3]
						slater=slater.replace("2","#")
						slater=slater.replace("a","+")
						slater=slater.replace("b","-")
						slater=slater.replace("0",".")
						if detInfo[0] == '+':
							detString=detInfo[1]+" "+detInfo[2]+" "+slater
						else:
							detString="-"+detInfo[1]+" "+detInfo[2]+" "+slater
						dets.append(detString)
					else:
						end=True
				break
