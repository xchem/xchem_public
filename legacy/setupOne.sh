#!/bin/bash
if [ $OVERWRITE -eq 1 ]; then
	if [ -d $casDir ]; then
		rm -rf $casDir
	fi
	mkdir $casDir
	mkdir $casTempMolcasDir
	mkdir $casTempMolproDir
	mkdir $casInputDir
else
	if [ ! -d $casDir ]; then
		mkdir $casDir
		mkdir $casTempMolcasDir
		mkdir $casTempMolproDir
		mkdir $casInputDir
	fi
fi
