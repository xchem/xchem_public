#!/bin/bash
source SCAT_PARAMS

if [ -z $mem ]; then
	export mem=120000
fi
if [ -z ${c} ]; then
	export c=16
fi
export OMP_NUM_THREADS=2

if [ $submit -eq 1 ]; then 
	sbatch --qos=$qos -c ${c} --mem=$mem calcs/submitScatBSES_CAP.sh
else
	bash calcs/submitScatBSES_CAP.sh
fi
