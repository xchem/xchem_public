#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

$xchemSrc/BasicMatrixElements -gif SAE_GeneralInputFile > $scatLogDir/BasicMatrixElements.out 2>&1
mv scatDir/SAE $scatDir/SAE
touch $scatDir/.scatBME
