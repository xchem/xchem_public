#/bin/bash
source SCAT_PARAMS

nChannel=$(cat .nchannel)
for (( iC = 0 ; iC < $nChannel ; iC++ )); do
	sed "s/##c1/$iC/g ; s/##c2/$iC/g" calcs/submitScatBSES_H.sh > H_${iC}_${iC}.sh
	if [ $submit -eq 1 ]; then
		sbatch H_${iC}_${iC}.sh 
		sleep 0.5
	else
		bash H_${iC}_${iC}.sh 
	fi
	rm H_${iC}_${iC}.sh
done
