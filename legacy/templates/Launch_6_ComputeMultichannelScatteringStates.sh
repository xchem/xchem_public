#!/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatMCSS.sh
else
	bash calcs/submitScatMCSS.sh
fi
