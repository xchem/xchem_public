#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

cp $scatInputDir/$CCF . 
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .

cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_S.out 2>&1 &

if [[ $gauge = *"l"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis x -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLX.out 2>&1 &
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis y -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLY.out 2>&1 &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis z -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLZ.out 2>&1 &
	fi
fi
if [[ $gauge = *"v"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis x -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVX.out 2>&1 &
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis y -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVY.out 2>&1 &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op Dipole -axis z -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVZ.out 2>&1 &
	fi
fi

wait 
