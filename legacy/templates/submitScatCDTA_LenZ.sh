#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

cp $scatInputDir/$CCF .
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .

cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

log=$scatLogDir/ComputeDipoleTransitionAmplitudes_LenZ.out_Emin${Emin}_Emax${Emax}_cm${cm}

if [[ -z $brasym && -z $ketsym ]]; then
	echo "ERROR: either bra or ketsym need to be defined" > $log
	exit 1
elif [[ -z $ketsym ]]; then 
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm  > $log 2>&1
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log 2>&1
elif [[ -z $brasym ]]; then
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -ketsym $ketsym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm  > $log 2>&1
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -ketsym $ketsym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log 2>&1
else
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -ketsym $ketsym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm  > $log 2>&1
	$xchemSrc/ComputeDipoleTransitionAmplitudes -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -ketsym $ketsym -gau l -axis z -trans $trans -boxi $boxi -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log 2>&1
fi
