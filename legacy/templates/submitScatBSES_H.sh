#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

c1=##c1
c2=##c2
cp $scatInputDir/${CCF}_${c1}-${c2} .
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .

cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op H -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_H_${c1}-${c2}.out 2>&1
