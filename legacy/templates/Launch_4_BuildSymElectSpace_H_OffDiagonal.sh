#/bin/bash
export cap=0
export cond=0
source SCAT_PARAMS

nChannel=$(cat .nchannel)
for (( iC = 0 ; iC < $nChannel ; iC++ )); do
	for (( jC = $iC+1 ; jC < $nChannel ; jC++ )); do 
		sed "s/##c1/$iC/g ; s/##c2/$jC/g" calcs/submitScatBSES_H.sh > H_${iC}_${jC}.sh
		if [ $submit -eq 1 ]; then 
			sbatch H_${iC}_${jC}.sh
			sleep 0.5
		else
			bash H_${iC}_${jC}.sh
		fi
		rm H_${iC}_${jC}.sh
	done
done
