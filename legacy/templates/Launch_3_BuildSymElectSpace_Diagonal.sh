#/bin/bash
export cap=0
export cond=0
source SCAT_PARAMS

nChannel=$(cat .nchannel)
for (( iC = 0 ; iC < $nChannel ; iC++ )); do
	sed "s/##c1/$iC/g ; s/##c2/$iC/g" calcs/submitScatBSES.sh > OpMat_${iC}_${iC}.sh
	if [ $submit -eq 1 ]; then
		sbatch OpMat_${iC}_${iC}.sh 
		sleep 0.5
	else
		bash OpMat_${iC}_${iC}.sh 
	fi
	rm OpMat_${iC}_${iC}.sh
done
