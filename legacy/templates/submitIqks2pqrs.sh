#!/bin/bash
if [[ $WorkDir = *'$'* ]]; then
	WorkDir=$(eval echo "$WorkDir")	
fi
export WorkDir=$WorkDir
echo "WorkDir= $WorkDir"
if [ ! -d $WorkDir ]; then
    mkdir -p $WorkDir
fi

cd $orbTempDir

log=$WorkDir/log

if [ -e pqrs.int ]; then
	echo " File with fully transformed integrals pqrs.int already exists -> skip"
else
	echo "jobid=${SLURM_JOB_ID}" > $log
	echo "Copying orbtials and basis two electron integrals to node" >> $log
	cp $orbInputDir/iqks2pqrs.xchem $WorkDir/iqks2pqrs.xchem
	cp $orbTempDir/iqks.int $WorkDir/iqks.int
	cp $orbTempDir/ijrs.int $WorkDir/ijrs.int
	cp $orbTempDir/ion.homemade.bin $WorkDir/ion.homemade.bin
	
	cd $WorkDir

	$xchemSrc/iqks2pqrs.exe $WorkDir/iqks2pqrs.xchem >> $log
	test=$(grep -c "Happy" $log)
	if [ $test -eq 1 ];then
		echo "Partial integral transformation (ijkl -> iqks) done" >> $log
		bzip2 < $log > $orbTempDir/iqks2pqrs.log.bz2
		mv $WorkDir/pqrs.int $orbTempDir/ham2E.orb.int
	else
		echo "Partial integral transformation (ijkl -> iqks) FAILED" >> $log
		bzip2 < $log > $orbTempDir/iqks2pqrs.log.fail.bz2
	fi
fi	
