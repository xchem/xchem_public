#!/bin/bash
if [[ $WorkDir = *'$'* ]]; then
	WorkDir=$(eval echo "$WorkDir")	
fi
export WorkDir=$WorkDir
echo "WorkDir= $WorkDir"
if [ ! -d $WorkDir ]; then
    mkdir $WorkDir
else
    echo "NOTE Working Directory $WorkDir existed and did no have to be created."
fi
if [ ! -d $WorkDir ]; then
    echo "ERROR Working Directory could not be created."
fi
export baseOrbDir=$(basename $orbDir)
export qcSpec=${baseOrbDir#*_}
