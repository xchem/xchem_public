#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

cp $scatInputDir/$CCF .
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .

cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

$xchemSrc/ComputeSymmetricElectronicSpaceBoxEigenstates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -cond > $scatLogDir/ComputeSymmetricElectronicSpaceBoxEigenstates.out 2>&1
