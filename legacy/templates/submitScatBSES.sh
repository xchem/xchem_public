#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

c1=##c1
c2=##c2
cp $scatInputDir/${CCF}_${c1}-${c2} . 
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .
cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op S -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_S_${c1}-${c2}.out 2>&1 &

if [[ $gauge = *"l"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis x -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLX_${c1}-${c2}.out 2>&1 &
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis y -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLY_${c1}-${c2}.out 2>&1 &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis z -gau l -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipLZ_${c1}-${c2}.out 2>&1 &
	fi
fi
if [[ $gauge = *"v"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis x -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVX_${c1}-${c2}.out 2>&1 & 
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis y -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVY_${c1}-${c2}.out 2>&1 &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op Dipole -axis z -gau v -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_DipVZ_${c1}-${c2}.out 2>&1 &
	fi
fi

wait

$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op H -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_H_${c1}-${c2}.out 2>&1

if [ $cap -eq 1 ]; then
	$xchemSrc/BuildSymmetricElectronicSpace -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile ${CCF}_${c1}-${c2} -esdir $qcSpec -op CAP -brasym $brasym > $scatLogDir/BuildSymmetricElectronicSpace_CAP_${c1}-${c2}.out 2>&1
fi

