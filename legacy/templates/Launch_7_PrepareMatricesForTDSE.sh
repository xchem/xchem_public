#!/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatPMTDSE.sh
else
	bash calcs/submitScatPMTDSE.sh
fi
