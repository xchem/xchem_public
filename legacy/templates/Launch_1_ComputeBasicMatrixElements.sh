#!/bin/bash
source SCAT_PARAMS

if [ $cap -eq 1 ]; then
	echo "ATTENTION: cap is set to 1. Make sure valide absorbers are included inf scat_*/input/AbsorptionPotential"
fi
if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatBME.sh
else
	bash calcs/submitScatBME.sh
fi
