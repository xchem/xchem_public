#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

cp $scatInputDir/$CCF .
cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .

cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

if [[ $gauge = *"l"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis x -gau l > $scatLogDir/PrepareMatricesForTDSE_DipLX.out &
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis y -gau l > $scatLogDir/PrepareMatricesForTDSE_DipLY.out &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis z -gau l > $scatLogDir/PrepareMatricesForTDSE_DipLZ.out &
	fi
fi

if [[ $gauge = *"v"* ]]; then
	if [[ $axes = *"x"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis x -gau v > $scatLogDir/PrepareMatricesForTDSE_DipVX.out &
	fi
	if [[ $axes = *"y"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis y -gau v > $scatLogDir/PrepareMatricesForTDSE_DipVY.out &
	fi
	if [[ $axes = *"z"* ]]; then
		$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op dipole -axis z -gau v > $scatLogDir/PrepareMatricesForTDSE_DipVZ.out &
	fi
fi

if [ $cap -eq 1 ]; then
	$xchemSrc/PrepareMatricesForTDSE -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -brasym $brasym -op CAP > $scatLogDir/PrepareMatricesForTDSE_CAP.out &
fi

wait
