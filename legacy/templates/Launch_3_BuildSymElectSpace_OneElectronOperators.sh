#/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatBSES_ONEO.sh
else
	bash calcs/submitScatBSES_ONEO.sh
fi
