!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import numpy as np
import sys

if (len(sys.argv)<3):
 print("At least two arguments are required:")
 print("First argument: GUGA table file")
 print("Second argument and following CI vector in Determinants")
 quit()

def readguga_det(fich):
 dets={}
 if ".bz2" in fich:
  import bz2
  f=bz2.open(fich)
  print("bz2 file ",fich)
 else:
  f=open(fich)
  print("regular file ",fich)
 line=f.readline()
 kk=line.split()
 nCSF=int(kk[0])
 norb=int(kk[1])
 CSF=[]
 print("Number of CSFs ",nCSF)
 for iCSF in range(nCSF):
  CSFinfo=[]
  line=f.readline()
  line=f.readline()
  kk=line.split()
  norm=0.
  for idet in range(int(len(kk)/3)):
   num=int(kk[3*(idet+1)-3])
   den=int(kk[3*(idet+1)-2])
   det=kk[3*(idet+1)-1]
   det=det.replace("#","3")
   det=det.replace("+","1")
   det=det.replace("-","2")
   det=det.replace(".","0")
   coef=1.
   if (num<0):
    coef=-1.
   coef=coef*np.sqrt(abs(num))/np.sqrt(den)
   norm+=coef*coef
   if det not in dets:
    dets[det]=[]
   infodet=[iCSF,coef]
   dets[det].append(infodet)
  if (norm-1.)**2 > 1e-6:
   print("WARNING norm CSF ",iCSF,norm)
   quit()
 f.close()
 return dets,nCSF,norb

def det2csf(gugatable,nCSF,npot,dets):
 CI=np.zeros( (npot,nCSF) )
 print("CI vector allocated ",npot,nCSF)
 for idet in dets:
  coef=dets[idet]
  for iCSF in gugatable[idet]:
   for ipot in range(npot):
    CI[ipot,iCSF[0]]+=iCSF[1]*coef[ipot]
 return CI

guga,nCSF,norb=readguga_det(sys.argv[1])
print("GUGA ready ",nCSF,norb,len(guga))

for ifich in range(2,len(sys.argv)):
 with open(sys.argv[ifich]) as f:
  dets={}
  line=f.readline()
  kk=line.split()
  npot=int(kk[0])
  ndets=int(kk[1])
  print("Number of Determinants ",ndets,sys.argv[ifich])
  for idet in range(ndets): 
   line=f.readline()
   kk=line.split()
   det=kk[0]
   if len(det)<=norb:
    for i in range(norb-len(det)):
     det=det+"0"
   else:
    print ("Determinants in file have less orbitals than guga")
    print ("GUGA :",norb)
    print ("Determinant ",len(det),det)
    quit()
   norm=0.
   coef=[]
   for i in range(npot):
    coef.append(float(kk[i+1]))
    norm+=coef[i]*coef[i]
# Only non-zero coefficients are passed
   if norm>=1e-15:
    if det in dets:
     coef_p=dets[det]
     for i in range(npot):
      coef[i]+=coef_p[i]
    dets[det]=coef
 print("Determinants ready, let's convert ",len(dets))
 CI=det2csf(guga,nCSF,npot,dets)
 print("Conversion ready")
 with open(sys.argv[ifich]+".csf","w") as f:
  f.write(str(npot)+" "+str(nCSF)+"\n")
  for ipot in range(npot):
   for iCSF in range(nCSF):
    f.write(" ")
    if CI[ipot,iCSF]*CI[ipot,iCSF]<=1e-15:
     f.write("0.0")
    else:
     f.write("{:.15e}".format(CI[ipot,iCSF]))
   f.write("\n")
