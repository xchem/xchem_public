!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

def prepareccm(cc):
 # Assign per parent
 PI=[]
 chan=[]
 iPI=0
 #print(cc,cc[0],cc[0][0],cc[0][1])
 for i in range(len(cc)):
  iPI=len(PI)
  for j in range(len(PI)):
   if cc[i][0] == PI[j]:
    iPI=j
  if iPI==len(PI):
   PI.append(cc[i][0])
   chan.append([])
  chan[iPI].append(cc[i][1])

 line="&ccm\n defer=T\n closeCouplingChannels="
 for i in range(len(PI)):
  line=line+str(PI[i])+"("
  for j in range(len(chan[i])):
   line=line+str(chan[i][j])
   if j != len(chan[i])-1:
    line=line+","
  line=line+")"
  if i != len(PI)-1:
   line=line+";"

 return line


import sys

if len(sys.argv)<2:
 print("Two arguments are required:")
 print("1) File containing the &bsplines section with the total Close Coupling")
 print("2) File where the splitted Close Coupling will be written")
 print("Alternative a third argument can be used to specify the direction of the dipole")
 quit()

### Reading for bsplines section
bsp=False
listop=" operators=H S Dipole"
cc0=""
with open(sys.argv[1],"r") as f:
 for line in f:
  if "&" in line:
   bsp=False
  if "&bsplines" in line:
   bsp=True
  if bsp:
   if "CouplingChannels=" in line:
    cc0=line.replace("\n","")
   if "cap" in line:
    listop+="CAP"
if len(sys.argv)>3:
 listop+="\n axes="+sys.argv[3]

if len(cc0)==0:
 print("Close Coupling empty")
 quit()


f=open(sys.argv[2],"w")
#print(kk)
kk2=cc0.split("=")
#print(kk2)
kk=kk2[1].split(";")
#print(kk)
cc=[]
for i in kk:
 kk2=i.split("(")
 #print(kk2)
 pi=int(kk2[0])
 kk3=kk2[1].replace(")"," ").split(",")
 for j in kk3:
  #print (pi,j)
  cc.append([pi,j.strip()])

print ("Number of channels ",len(cc))
line=input ("Number of blocks? ")
nb=int(line)

## CC by blocks bcc
bcc=[]
for i in range(nb):
 bcc.append([])

## Assign cc to the blocks
ib=0
for i in cc: 
 bcc[ib].append(i)
 ib+=1
 if (ib==nb):
  ib=0

ib=0
for i in bcc:
 print(i)
 if ib != 0:
  f.write("&nowait\n")
 line=prepareccm(i)
 line=line+"\n"+listop+"\n"
 f.write (line)
 ib+=1

print("Diagonal part, blocks ",ib)

# line="&ccm\n"
# line=line+cc0+"\n"
# line=line+"operators=Dipole\n"
# if len(sys.argv)>=4:
#  line=line+"axes="+sys.argv[3]+"\n"
# f.write (line)
#f.write(cc)

ib=0
for i in range(nb):
 for j in range(i+1,nb):
  ccm=[]
  for k in bcc[i]:
   ccm.append(k)
  for k in bcc[j]:
   ccm.append(k)
  if ib != 0 :
   f.write("&nowait\n")
  line=prepareccm(ccm)
  line=line+"\n"+listop+"\n"
  f.write(line)
  ib+=1

print("Off-diagonal part, blocks ",ib)


line="&ccm\n defer=T\n"
line=line+cc0+"\n"
line=line+listop+"\n"
f.write (line)

f.close()
