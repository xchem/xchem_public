!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

!This program assumes the grid points over the sphere are equidistant. This can
!be used to generate them (https://doi.org/10.1145/2513456.2513499)

PROGRAM CheckRmim

implicit none

real*8, parameter   :: PI = 3.1415926535897
real*8, allocatable :: Density(:)
character*400          OutputFname,tmpString, kkStr
character(len=:),allocatable    :: InputFname
logical                information,first
integer                nAtoms,nOrbs,blockSize,i,j,l
integer                TotOrbs,nPoints
real*8                 x,y,z,origin(3),axis(3,3),val
integer, allocatable:: CounterOrbs(:)


write(*,*) 'Total number of orbitals in the file'
read(*,*) TotOrbs 
allocate(Density(TotOrbs),CounterOrbs(TotOrbs+1))
write(*,*) 'Input file name'
read(*,*) kkStr
allocate(InputFname, source = trim(adjustl(adjustr(kkStr))))
write(*,*) 'Output file name'
read(*,*) OutputFname
open(1,file=trim(InputFname), status='old', form='formatted')

! READING GRID INFORMATION
Information = .TRUE.
do while (information)
  read(1,'(A)') tmpString
  if (index(tmpString,'Natom=') .ne. 0) then
    read(tmpString,*) kkStr, nAtoms
    do i=1,nAtoms
      read(1,*) kkstr,x,y,z
      write(*,'(A,3E24.14)') trim(kkstr)//' atom at: ',x,y,z 
    enddo
    cycle
  endif
  if (index(tmpString,'N_of_Grids=') .ne. 0) then
    read(tmpString,*) kkStr, nOrbs
    cycle
  endif
  if (index(tmpString,'Block_Size=') .ne. 0) then
    read(tmpString,*) kkStr, blockSize
    cycle
  endif
  if (index(tmpString,'N_of_Points=') .ne. 0) then
    read(tmpString,*) kkStr, nPoints
    cycle
  endif
  if (index(tmpString,'Origin=') .ne. 0) then
    read(tmpString,*) kkStr, (origin(i),i=1,3)
    do i=1,3
      read(1,*) kkStr, (Axis(i,j),j=1,3)
    enddo
    cycle
  endif
  if (index(tmpString,'GridName=') .ne. 0) then
    write(*,*) 'Orbitals in the file: '
    write(*,'(I5,A)') 1,'  '//trim(tmpString)
    do i=1,nOrbs-1
      read(1,'(A)') tmpString
      write(*,'(I5,A)') i+1,'  '//trim(tmpString)
    enddo
    if (nOrbs-1 .ne. TotOrbs ) stop 'The number of requested orbitals differs from the number of orbitals in the file'
   endif
   if (index(tmpString,'Title=') .ne. 0) Information=.FALSE. 
enddo
  
! READING GRID POINTS
information = .true.
i=1
CounterOrbs=0
Density(:) = 0.d0
first=.true.
do while (information)
  if (counterOrbs(TotOrbs) .eq. npoints) then
    information = .FALSE.
    cycle
  endif
  if (i .ne. nOrbs) then
    if (.not.first) then
      read(1,*) tmpString
    endif
    first=.false.
    do l=1,min(npoints-counterOrbs(i),blockSize)
      read(1,*) tmpString
      read(tmpString,*) val
      Density(i)= Density(i) + val**2
      counterOrbs(i) = counterOrbs(i) + 1
    enddo
    i=i+1
  else
    do l = 1,min(npoints-counterOrbs(i)+1,blockSize+1)
      read(1,*) tmpString
      counterOrbs(i) = counterOrbs(i) + 1
    enddo
    counterOrbs(i) = counterOrbs(i) - 1
    i = 1
  endif
enddo

! WRITTING ANGULAR PART TO OUTPUT FILE

open(1,file=OutputFname,status='replace',form='formatted')
do i=1,TotOrbs
  val = 0.d0
  write(1,'(A,I5)') 'Orbital number: ',i
  val = val + (Density(i)*4.0*PI)/nPoints
  write(1,'(E24.14)') val 
enddo

END PROGRAM
