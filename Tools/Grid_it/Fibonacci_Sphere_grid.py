!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

import numpy as np
import sys


GoldenRation = (np.sqrt(5.0)+1.0)/(2.0)
GoldenAngle = (2.0-GoldenRation)*(2.0*np.pi)
OrbitalFile =sys.argv[1]
GatewayFile =sys.argv[2]
r = float(sys.argv[3])
dA = float(sys.argv[4])
npoints = int((4.0*np.pi*(r**2))/dA)
if (npoints == 0): npoints = 1
print(r,npoints)
MolcasFile = sys.argv[5]
nOrb = int(sys.argv[6])
Orbs=[]
Syms=[]
for i in range(nOrb):
  Orbs.append(sys.argv[7+i*2])
  Syms.append(sys.argv[7+i*2+1])
M = open(MolcasFile,'w')
G = open(GatewayFile,'r')
for line in G:
  M.write(line)
G.close()
M.write('>>> COPY $CurrDir/'+OrbitalFile+' INPORB\n')
M.write('&GRID_IT\n')
if (nOrb > 0):
  M.write('ORBI\n')
  M.write(str(len(Orbs))+'\n')
  for i in range(len(Orbs)):
    M.write(Orbs[i]+' '+Syms[i]+'\n')
M.write('NOLUSCUS\n')
M.write('ASCII\n')
M.write('GRID\n')
M.write(str(npoints)+'\n')
if (npoints == 1):
  M.write('0.0 0.0 0.0')
else:
  dif = (np.pi*2)/npoints
  for i in range(npoints):
    a1 = np.arcsin(-1.0 + 2.0 * float(i+1) / (npoints+1))
    a2 = GoldenAngle*float(i+1)
    M.write(str(r*np.cos(a1)*np.cos(a2))+' '+str(r*np.cos(a1)*np.sin(a2))+' '+str(r*np.sin(a1))+'\n')
M.close()
