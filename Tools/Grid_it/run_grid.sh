#!/bin/bash
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1

export MOLCAS=/home/proyectos/fotospline/xchem/OpenMolcas/
export OMP_NUM_THREADS=1
export CurrDir=$(pwd)
export WorkDir=/temporal/vicent/slurm_tmp_${SLURM_JOB_ID}
export SrcDir=$(pwd)

#Number of orbitals in the file
nOrb=23
#Distance between successive spheres
rDif=0.1
#Orbital file (molcas)
OrbFile="co.RasOrb"
#Gateway file (molcas)
GatwayFile="co.gateway"
#Area of each grid point on the sphere surface
dA=0.01

cp Fibonacci_Sphere_grid.py $WorkDir
cp $CurrDir/$OrbFile $WorkDir
cp $CurrDir/$GatwayFile $WorkDir
cd $WorkDir
python3 $SrcDir/Fibonacci_Sphere_grid.py $OrbFile $GatwayFile $(echo $1*$rDif | bc) $dA input_$1 0
pymolcas input_$1 > out_molcas$1
echo "$nOrb" > input
echo "input_$1.grid" >> input
echo "out_$1" >> input
$SrcDir/CheckSphRminFibonacci.exe < input 
cp out_* $CurrDir
cd $CurrDir
rm -r $WorkDir
echo "done $1"
