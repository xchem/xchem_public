!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3


import sys, h5py
import numpy as np

### Reading for bsplines section
bsp=False
cc0=""
with open(sys.argv[1],"r") as f:
 for line in f:
  if "&" in line:
   bsp=False
  if "&bsplines" in line:
   bsp=True
  if bsp:
   if "CouplingChannels=" in line:
    cc0=line.replace("\n","")

if len(cc0)==0:
 print("Close Coupling empty")
 quit()


kk2=cc0.split("=")
kk=kk2[1].split(";")
cc=[]
for i in kk:
 kk2=i.split("(")
 pi=int(kk2[0])
 kk3=kk2[1].replace(")"," ").split(",")
 for j in kk3:
  cc.append([pi,j.strip()])

nPIs=0
PI=''
PIs=[]
for i in range(len(cc)):
 if (PI != cc[i][0]):
  PI=cc[i][0]
  PIs.append(PI)

nPIs=len(PIs)
print('The number of PIs is: ',nPIs)

Mult=sys.argv[2]
Sym=sys.argv[3]
nPolyOrbs=int(sys.argv[4])
Poly=bool(sys.argv[5])
nOps=int(sys.argv[6])
operators=[]
for iOp in range(nOps):
 if (sys.argv[7+iOp][:3] != 'Dip'):
  operators.append(sys.argv[7+iOp]+'_0.0')
 else:
  operators.append(sys.argv[7+iOp])
print(operators) 
Start=1
if (Poly): Start=0

if (Poly):
 BraFolderLabel='Loc'
else:
 BraFolderLabel=Mult+Sym+'.'+str(PIs[0])
TotalSize=0
infBasis=[]
for jPI in range(Start,nPIs+1):
 if (jPI == 0):
  KetFolderLabel='Loc'
 else:
  KetFolderLabel=Mult+Sym+'.'+str(PIs[jPI-1])

 MetaData=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0QC.h5','r')
 print(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0QC.h5')
 TotalSize+=MetaData['S_0.0QC']['S_0.0QCMeta'][2]
 infBasis.append(MetaData['S_0.0QC']['S_0.0QCMeta'][2])
 if (jPI != 0):
  for iCh in range(len(cc)):
   if (cc[iCh][0] == PIs[jPI-1]):
    print(cc[iCh][1])
    for icharacter in range(len(cc[iCh][1])):
     if (cc[iCh][1][icharacter] ==' '): endL = icharacter
     if (icharacter > 0 and cc[iCh][1][icharacter] !=' ' and cc[iCh][1][icharacter-1] == ' '): StartM = icharacter
    l=cc[iCh][1][:endL]
    m=cc[iCh][1][StartM:]
    print(l+m)
    if (Poly):
     MetaData=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0_'+l+'.'+m+'LocPWC.h5','r')
     print(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0_'+l+'.'+m+'LocPWC.h5')
     TotalSize+=MetaData['S_0.0_'+l+'.'+m+'LocPWC']['S_0.0_'+l+'.'+m+'LocPWCMeta'][2]-1
     infBasis[jPI-Start]+=MetaData['S_0.0_'+l+'.'+m+'LocPWC']['S_0.0_'+l+'.'+m+'LocPWCMeta'][2]-1
    else:
     MetaData=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0_'+l+'.'+m+'SRCPWC.h5','r')
     print(BraFolderLabel+'_'+KetFolderLabel+'/'+'S_0.0_'+l+'.'+m+'SRCPWC.h5')
     TotalSize+=MetaData['S_0.0_'+l+'.'+m+'SRCPWC']['S_0.0_'+l+'.'+m+'SRCPWCMeta'][2]-1
     infBasis[jPI-Start]+=MetaData['S_0.0_'+l+'.'+m+'SRCPWC']['S_0.0_'+l+'.'+m+'SRCPWCMeta'][2]-1

print(TotalSize)
print(infBasis)
Loc=0
if (Poly): Loc = infBasis[0]
nPoly=nPIs*nPolyOrbs+Loc
infBasisAll=[]
infBasisAll.append(nPoly)
for iBas in range(len(infBasis)):
 if (iBas != 0 or not Poly): infBasisAll.append(infBasisAll[iBas-1]+infBasis[iBas]-nPolyOrbs)

print(infBasisAll)
for op in operators:
 opMat=np.zeros((TotalSize,TotalSize))
 for iPI in range(Start,nPIs+1):
  if (iPI == 0):
   BraFolderLabel='Loc'
  else:
   BraFolderLabel=Mult+Sym+'.'+str(PIs[iPI-1])
  if (iPI == 0):
   iStart = 0
   iEnd=infBasis[0]
   for jPI in range(Start,nPIs+1):
    if (jPI == 0):
     KetFolderLabel='Loc'
    else:
     KetFolderLabel=Mult+Sym+'.'+str(PIs[jPI-1])
   
    Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'QC.h5','r')
    print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'QC.h5')
    if (jPI == 0): 
     jStart = 0
     jEnd=infBasis[0]
    if (jPI != 0):
     jStartPoly = infBasis[0]+nPolyOrbs*(jPI-1) 
     jEndPoly = infBasis[0]+nPolyOrbs*(jPI)
     jStart = infBasisAll[jPI-1]
     jEnd = infBasisAll[jPI-1]+Data[op+'QC'][op+'QCMeta'][2]-nPolyOrbs
    if (jPI == 0):
     opMat[iStart:iEnd,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()
    else:
     opMat[iStart:iEnd,jStartPoly:jEndPoly]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[:,:nPolyOrbs]
     opMat[iStart:iEnd,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[:,nPolyOrbs:]
     for iCh in range(len(cc)):
      bsCounter=0
      if (cc[iCh][0] == PIs[jPI-1]):
       print(cc[iCh][1])
       for icharacter in range(len(cc[iCh][1])):
        if (cc[iCh][1][icharacter] ==' '): endL = icharacter
        if (icharacter > 0 and cc[iCh][1][icharacter] !=' ' and cc[iCh][1][icharacter-1] == ' '): StartM = icharacter
       jl=cc[iCh][1][:endL]
       jm=cc[iCh][1][StartM:]
       Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+jl+'.'+jm+'LocPWC.h5','r')
       print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+jl+'.'+jm+'LocPWC.h5')
       opMat[iStart:iEnd,jEnd:jEnd+Data[op+'_'+jl+'.'+jm+'LocPWC'][op+'_'+jl+'.'+jm+'LocPWCMeta'][2]-1]=Data[op+'_'+jl+'.'+jm+'LocPWC'][op+'_'+jl+'.'+jm+'LocPWCMat'][:,:].transpose()[:,:Data[op+'_'+jl+'.'+jm+'LocPWC'][op+'_'+jl+'.'+jm+'LocPWCMeta'][2]-1]
       jEnd+=Data[op+'_'+jl+'.'+jm+'LocPWC'][op+'_'+jl+'.'+jm+'LocPWCMeta'][2]-1
  if (iPI != 0):
   Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'QC.h5','r')
   iStartPoly = infBasis[0]+nPolyOrbs*(iPI-1) 
   iEndPoly = infBasis[0]+nPolyOrbs*(iPI)
   iStart = infBasisAll[iPI-1]
   iEnd = infBasisAll[iPI-1]+Data[op+'QC'][op+'QCMeta'][1]-nPolyOrbs
   for jPI in range(Start,nPIs+1):
    if (jPI == 0):
     KetFolderLabel='Loc'
    else:
     KetFolderLabel=Mult+Sym+'.'+str(PIs[jPI-1])
   
    Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'QC.h5','r')
    print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'QC.h5')
    if (jPI == 0): 
     jStart = 0
     jEnd=infBasis[0]
    if (jPI != 0):
     jStartPoly = infBasis[0]+nPolyOrbs*(jPI-1) 
     jEndPoly = infBasis[0]+nPolyOrbs*(jPI)
     jStart = infBasisAll[jPI-1]
     jEnd = infBasisAll[jPI-1]+Data[op+'QC'][op+'QCMeta'][2]-nPolyOrbs
    if (jPI == 0):
     opMat[iStartPoly:iEndPoly,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[:nPolyOrbs,:]
     opMat[iStart:iEnd,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[nPolyOrbs:,:]
    else:
     opMat[iStartPoly:iEndPoly,jStartPoly:jEndPoly]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[:nPolyOrbs,:nPolyOrbs]
     opMat[iStartPoly:iEndPoly,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[:nPolyOrbs,nPolyOrbs:]
     opMat[iStart:iEnd,jStartPoly:jEndPoly]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[nPolyOrbs:,:nPolyOrbs]
     opMat[iStart:iEnd,jStart:jEnd]=Data[op+'QC'][op+'QCMat'][:,:].transpose()[nPolyOrbs:,nPolyOrbs:]
     for iCh in range(len(cc)):
      if (cc[iCh][0] == PIs[jPI-1]):
       print(cc[iCh][1])
       for icharacter in range(len(cc[iCh][1])):
        if (cc[iCh][1][icharacter] ==' '): endL = icharacter
        if (icharacter > 0 and cc[iCh][1][icharacter] !=' ' and cc[iCh][1][icharacter-1] == ' '): StartM = icharacter
       jl=cc[iCh][1][:endL]
       jm=cc[iCh][1][StartM:]
       Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+jl+'.'+jm+'SRCPWC.h5','r')
       print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+jl+'.'+jm+'SRCPWC.h5')
       opMat[iStart:iEnd,jEnd:jEnd+Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMeta'][2]-1]=Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMat'][:,:].transpose()[nPolyOrbs:,:Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMeta'][2]-1]
       opMat[iStartPoly:iEndPoly,jEnd:jEnd+Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMeta'][2]-1]=Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMat'][:,:].transpose()[:nPolyOrbs,:Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMeta'][2]-1]
       jEnd+=Data[op+'_'+jl+'.'+jm+'SRCPWC'][op+'_'+jl+'.'+jm+'SRCPWCMeta'][2]-1
   for iCh in range(len(cc)):
    if (cc[iCh][0] == PIs[iPI-1]):
     print(cc[iCh][1])
     for icharacter in range(len(cc[iCh][1])):
      if (cc[iCh][1][icharacter] ==' '): endL = icharacter
      if (icharacter > 0 and cc[iCh][1][icharacter] !=' ' and cc[iCh][1][icharacter-1] == ' '): StartM = icharacter
     il=cc[iCh][1][:endL]
     im=cc[iCh][1][StartM:]
     for jPI in range(Start,nPIs+1):
      if (jPI == 0):
       KetFolderLabel='Loc'
      else:
       KetFolderLabel=Mult+Sym+'.'+str(PIs[jPI-1])
     
      if (jPI != 0):
       Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+il+'.'+im+'PWCSRC.h5','r')
       print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+il+'.'+im+'PWCSRC.h5')
       jStartPoly = infBasis[0]+nPolyOrbs*(jPI-1) 
       jEndPoly = infBasis[0]+nPolyOrbs*(jPI)
       jStart = infBasisAll[jPI-1]
       jEnd = infBasisAll[jPI-1]+Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][2]-nPolyOrbs
       print(iEnd,Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,jStartPoly,jEndPoly)
       print(TotalSize,iEnd+Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,jEndPoly-jStartPoly,Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,nPolyOrbs)
       opMat[iEnd:iEnd+Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,jStartPoly:jEndPoly]=Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMat'][:,:].transpose()[:Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,:nPolyOrbs]
       opMat[iEnd:iEnd+Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,jStart:jEnd]=Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMat'][:,:].transpose()[:Data[op+'_'+il+'.'+im+'PWCSRC'][op+'_'+il+'.'+im+'PWCSRCMeta'][1]-1,nPolyOrbs:]
       for iCh in range(len(cc)):
        bsCounter=0
        if (cc[iCh][0] == PIs[jPI-1]):
         print(cc[iCh][1])
         for icharacter in range(len(cc[iCh][1])):
          if (cc[iCh][1][icharacter] ==' '): endL = icharacter
          if (icharacter > 0 and cc[iCh][1][icharacter] !=' ' and cc[iCh][1][icharacter-1] == ' '): StartM = icharacter
         jl=cc[iCh][1][:endL]
         jm=cc[iCh][1][StartM:]
         Data=h5py.File(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC.h5','r')
         print(BraFolderLabel+'_'+KetFolderLabel+'/'+op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC.h5')
         opMat[iEnd:iEnd+Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][1]-1,jEnd:jEnd+Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][2]-1]=Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMat'][:,:].transpose()[:Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][1]-1,:Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][2]-1]
         jEnd+=Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][2]-1
     iEnd+=Data[op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWC'][op+'_'+il+'.'+im+'_'+jl+'.'+jm+'PWCPWCMeta'][1]-1
 h5File=h5py.File(op+'.h5','w')
 h5File.create_dataset('Mat',data=opMat)
