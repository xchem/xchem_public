!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

#!/bin/env python3

import numpy as np
import sys

if (len(sys.argv)<3):
 print("At least two arguments are required:")
 print("First argument: GUGA table file")
 print("Second argument and following CI vector in CSFs")
 quit()

def readguga_CSF(fich):
 if ".bz2" in fich:
  f=bz2.open(fich)
  print("bz2 file ",fich)
 else:
  f=open(fich)
  print("regular file ",fich)
 line=f.readline()
 kk=line.split()
 nCSF=int(kk[0])
 norb=int(kk[1])
 CSFs=[]
 for iCSF in range(nCSF):
  norm=0.
  line=f.readline()
  line=f.readline()
  kk=line.split()
  dets={}
  for idet in range(int(len(kk)/3)):
   num=int(kk[3*(idet+1)-3])
   den=int(kk[3*(idet+1)-2])
   det=kk[3*(idet+1)-1]
   det=det.replace("#","3")
   det=det.replace("+","1")
   det=det.replace("-","2")
   det=det.replace(".","0")
   coef=1.
   if (num<0):
    coef=-1.
   coef=coef*np.sqrt(abs(num))/np.sqrt(den)
   norm+=coef*coef
   dets[det]=coef
   CSFs.append(dets)
  if (norm-1.)**2 > 1e-6:
   print("WARNING norm CSF ",iCSF,norm)
   print(line)
   quit()
 f.close()
 return CSFs,nCSF,norb

def csf2det(gugatable,nCSF,npot,CI):
 import numpy as np
 dets={}
 for iCSF in range(nCSF):
  CSFdets=gugatable[iCSF]
  for idet in CSFdets:
   if idet in dets:
    detcoef=dets[idet]
   else:
    detcoef=np.zeros( npot )
   for ipot in range(npot):
    detcoef[ipot]+=CSFdets[idet]*CI[ipot,iCSF]
   dets[idet]=detcoef
 return dets

guga,nCSF,norb=readguga_CSF(sys.argv[1])
print("GUGA ready ",nCSF,norb)
for ifich in range(2,len(sys.argv)):
 with open(sys.argv[ifich]) as f:
  line=f.readline()
  kk=line.split()
  npot=int(kk[0])
  iCSF=int(kk[1])
  if iCSF != nCSF:
   print("GUGA not valid, CSFs in GUGA ",nCSF)
   print("CSFs in file ",iCSF,sys.argv[ifich])
   quit()
  CI=np.zeros( (npot,nCSF) )
  for ipot in range(npot): 
   line=f.readline()
   kk=line.split()
   for iCSF in range(nCSF):
    CI[ipot,iCSF]=float(kk[iCSF])
   
 dets=csf2det(guga,nCSF,npot,CI)
 listtoremove=[]
 for idet in dets:
  kk=0.
  for coef in dets[idet]:
   kk+=coef
  if coef <1e-10:
   listtoremove.append(idet)
 for idet in listtoremove:
  dets.pop(idet)

 with open(sys.argv[ifich]+".det","w") as f:
  f.write(str(npot)+" "+str(len(dets))+" norb0\n" )
  for idet in dets:
   kk=idet
   for ipot in dets[idet]:
	if ipot**2 >=1e-15:
     kk=kk+" {:.15e}".format(ipot)
	else:
     kk=kk+" 0.0"
   kk=kk+"\n"
   f.write(kk)
