!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM autoAugment
!CODE THAT CREATES ALL THE INPUT FILES NECESSARY
!TO AUTOMATICALLY RUN MOMOPOLY SCRIPTS
    USE class_Gateway
    USE class_inputString
    USE createInputUtilities
    USE lkIO
    IMPLICIT none
    INTEGER :: i,nSym,iSym,electronsParent,nRas3
    INTEGER :: lMonocentric,kMonocentric,totalLMonocentric
    INTEGER :: maxMultipoleOrder
    !INTEGER :: multiplicityParent
    INTEGER :: multiplicityAugmented
    INTEGER :: occupiedNoSym
    INTEGER :: nHoleRas1
    INTEGER :: nElecRas2
    INTEGER :: nElecRas3
    INTEGER :: BSplineNodes
    LOGICAL :: multiGhost,forceC1
    INTEGER, ALLOCATABLE :: arrI1(:)
    INTEGER, ALLOCATABLE :: arrI2(:)
    CHARACTER*100, ALLOCATABLE :: arrC1(:)
    CHARACTER*100, ALLOCATABLE :: arrC2(:)
    CHARACTER*100, ALLOCATABLE :: arrC3(:)
    CHARACTER*100 :: fich,token
    CHARACTER*100 :: gatewayFile
    CHARACTER*100 :: orbitalFileParent
    CHARACTER*100 :: monocentricBasisFile
    CHARACTER*100 :: project
    CHARACTER*100 :: rdmMethod
    CHARACTER*100 :: typeOrbital
    CHARACTER*100 :: CCF
    CHARACTER*100 :: lkMonocentricFile
    CHARACTER*200 :: dir
    REAL*8 :: linearDependenceThr,Rmin,Rmax
    TYPE(inputString) :: ras1
    TYPE(inputString) :: ras2
    TYPE(inputString) :: ras3
    TYPE(inputString) :: inactive
    TYPE(inputString) :: casOccupied
    TYPE(inputString) :: symmetryStatesParent
    TYPE(inputString) :: numberStatesParent
    TYPE(inputString) :: symmetryStatesSource
    TYPE(inputString) :: numberStatesSource
    TYPE(inputString) :: createSourceFromParent
    TYPE(inputString) :: sourceFiles
    TYPE(inputString) :: multiplicityParent
    TYPE(gateway) :: molcasGate
    TYPE(lk_) :: lkPairs
    
    fich=""
    dir=""
    CALL GETARG(1,dir)
    CALL GETARG(2,fich)
    
    IF (LEN(TRIM(fich)).eq.0) THEN
        STOP "ERROR no input defined"
    ENDIF
    IF (LEN(TRIM(dir)).eq.0) THEN
        STOP "ERROR no output directory defined"
    ENDIF           
    WRITE(6,'(A16,x,A10)') "Input file:",fich   
    CALL readInput( fich,&
      gatewayFile,&
      ras1,&
      ras2,&
      ras3,&
      inactive,&
      symmetryStatesParent,&
      numberStatesParent,&
      symmetryStatesSource,&
      numberStatesSource,&
      createSourceFromParent,&
      sourceFiles,&
      orbitalFileParent,&
      project,&
      monocentricBasisFile,&
      nHoleRas1,&
      nElecRas2,&
      nElecRas3,&
      linearDependenceThr,&
      multiplicityParent,&
      multiplicityAugmented,&
      maxMultipoleOrder,&
      lMonocentric,&
      kMonocentric,&
      rdmMethod,&
      multiGhost,&
      typeOrbital,&
      forceC1,&
      Rmin,&
      Rmax,&
      BSplineNodes,&
      CCF,&
      lkMonocentricFile)

    IF (len(trim(lkMonocentricFile)).gt.0) THEN 
        WRITE(6,'(A)') "Read from file"
        call lkPairs%create(trim(lkMonocentricFile))
    ELSE
        IF (lMonocentric.lt.0) STOP "negative lMax make no sense"
        IF (kMonocentric.lt.0) STOP "negative kMax make no sense"
        call lkPairs%create(lMonocentric,kMonocentric)
    ENDIF
    WRITE(6,'(A)') " "
    
    CALL molcasGate%setProjectName(project)
    CALL molcasGate%setMonocentricBasisFile(monocentricBasisFile)
    CALL molcasGate%readGateway(gatewayFile)
    
    !OPEN(2,file=TRIM(dir)//"/gateway.local.sym.cas", STATUS="replace")
    !OPEN(3,file=TRIM(dir)//"/gateway.local.noSym.cas", STATUS="replace")
    !OPEN(4,file=TRIM(dir)//"/pro", STATUS="replace")
    !CALL molcasGate%writeMolcasGateway(2,-1,.TRUE.)
    !CALL molcasGate%writeMolcasGateway(3,-1,.FALSE.)
    !CALL molcasGate%writeMolproGateway(4,orbitalFileParent,multiplicityAugmented)
    !CLOSE(2)
    !CLOSE(3)
    
    casOccupied=inactive+ras1
    casOccupied=casOccupied+ras2
    casOccupied=casOccupied+ras3
    CALL casOccupied%sumString(occupiedNoSym)
    
    electronsParent=nElecRas2
    CALL inactive%sumString(i)
    electronsParent=electronsParent+2*i
    !CALL ras1%sumString(i)
    !electronsParent=electronsParent+2*i-nHoleRas1

    !CALL writeMulti(4,casOccupied,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas3,&
    !  symmetryStatesParent,numberStatesParent,&
    !  electronsParent,orbitalFileParent,multiplicityParent)
    !CALL writeMatrop(4)
    !CLOSE(4)
   
    OPEN(2,file=TRIM(dir)//"/seward.cas")
    CALL writeSeward2(2,.FALSE.,.FALSE.)
    CLOSE(2)
    OPEN(2,file=TRIM(dir)//"/seward.oneo.cas")
    CALL writeSeward2(2,.TRUE.,.FALSE.)
    CLOSE(2)

    !OPEN(2,file=TRIM(dir)//"/rasscf.local.parent.noSym.cas",STATUS="replace")
    !CALL writeRasscf(2,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2,nElecRas3,&
    !  symmetryStatesParent,numberStatesParent,0,multiplicityParent,&
    !  .FALSE.,.FALSE.,.TRUE.,.TRUE.)
    !CLOSE(2)
    
    CALL symmetryStatesParent%sizeString(nSym)
    CALL symmetryStatesParent%writeString(token)
    !DO iSym=1,nSym
    !    WRITE(fich,'(A,A,I1,A)'),TRIM(dir),"/rasscf.local.parent.sym-",iSym,".cas"
    !    OPEN(2,file=TRIM(fich),STATUS="replace")
    !    CALL writeRasscf(2,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2,nElecRas3,& 
    !      symmetryStatesParent,numberStatesParent,iSym,multiplicityParent,&
    !      .TRUE.,.FALSE.,.TRUE.,.FALSE.,.TRUE.)
    !    CLOSE(2)
    !ENDDO
    
    !OPEN(2,file=TRIM(dir)//"/createLauncher.sh",STATUS="replace")
    !CALL writeCreateLauncher(2,numberStatesSource,numberStatesParent,ras1,ras2,ras3,inactive,project)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/mix.xchem",STATUS="replace")
    !CALL writeMix(2,numberStatesSource,numberStatesParent,ras1,ras2,ras3,casOccupied)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/basisInterface.xchem",STATUS="replace")
    !CALL writeInterface(2,multiplicityParent,ras1,ras2,ras3)
    !CLOSE(2)
    
    CALL numberStatesParent%getArray(arrI1)
    CALL ras2%sumString(nRas3)
    nRas3=nRas3+2
    arrI1=arrI1*nRas3
    CALL numberStatesParent%setArray(arrI1)
    
    !OPEN(2,file=TRIM(dir)//"/rasscf.local.source.noSym.ras3.cas",STATUS="replace")
    !CALL writeRasscf(2,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2,nElecRas3,&
    !  symmetryStatesParent,numberStatesParent,0,multiplicityAugmented,&
    !  .TRUE.,.TRUE.,.TRUE.,.TRUE.,.TRUE.)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/caspt2.cas",STATUS="replace")
    !CALL writeCaspt2(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/desy.cas",STATUS="replace")
    !CALL writeDesy(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/seward.local.mltpl.cas",STATUS="replace")
    !CALL writeSeward(2,"oneEl",maxMultipoleOrder)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/rassi.local.hamiltonian.cas",STATUS="replace")
    !CALL writeRassi(2,0)
    !CLOSE(2)
    
    !DO i=1,maxMultipoleOrder
    !    WRITE(fich,'(A,A,I1,A)') TRIM(dir),"/rassi.local.mltpl-",i,".cas"
    !    OPEN(2,file=fich,STATUS="replace")
    !    CALL writeRassi(2,i)
    !    CLOSE(2)
    !ENDDO
    
    !OPEN(2,file=TRIM(dir)//"/gateway.monpo.cas",STATUS="replace")
    !CALL molcasGate%writeMolcasGateway(2,lMonocentric,kMonocentric,.FALSE.,multiGhost,.FALSE.)
    !CLOSE(2)

    OPEN(2,file=TRIM(dir)//"/gateway.monpo.cas",STATUS="replace")
    CALL molcasGate%writeMolcasGateway(2,lkPairs,.FALSE.,multiGhost)
    CLOSE(2)

    OPEN(2,file=TRIM(dir)//"/gateway.monpo.sym.cas",STATUS="replace")
    CALL molcasGate%writeMolcasGateway(2,lkPairs,.TRUE.,multiGhost)
    CLOSE(2)
    
    !CALL casOccupied%sumString(nOrb)
    !
    !CALL sourceFiles%getArray(arrC1)
    !CALL createSourceFromParent%getArray(arrC2)
    !CALL numberStatesSource%getArray(arrI1)
    !CALL numberStatesSource%getArray(arrI2)
    !iSym=0
    !DO i=1,size(arrC1)
    !    arrI2=0
    !    arrI2(i)=arrI1(i)
    !    CALL numberStatesSource%setArray(arrI2)
    !    
    !    IF (TRIM(arrC2(i)).eq."T") THEN
    !        iSym=iSym+1
    !        arrI2=arrI1
    !        arrI2(iSym)=arrI2(i)
    !        CALL numberStatesSource%setArray(arrI2)
    !        WRITE(fich,'(A,A,A,A)'),TRIM(dir),"/rasscf.monpo.source-",TRIM(arrC1(i)),".sym.cas"
    !        OPEN(2,file=fich,STATUS="replace")
    !        CALL writeRasscf(2,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2+1,nElecRas3,&
    !          symmetryStatesSource,numberStatesSource,iSym,multiplicityAugmented,&
    !          .TRUE.,.FALSE.,.TRUE.,.FALSE.)
    !        CLOSE(2)
    !    ENDIF
    !ENDDO
    
    !OPEN(2,file=TRIM(dir)//"/several.monpo.xchem",STATUS="replace")
    !CALL writeSeveral(2,nOrb,sourceFiles)
    !CLOSE(2)
    
    OPEN(2,file=TRIM(dir)//"/compressTwoElectronIntegrals.xchem",STATUS="replace")
    CALL writeCompressTwoElectronIntegrals(2,lMonocentric,kMonocentric,lkMonocentricFile)
    CLOSE(2)

    OPEN(2,file=TRIM(dir)//"/compressTwoElectronIntegrals.dontCompress.xchem",STATUS="replace")
    CALL writeCompressTwoElectronIntegrals(2, lMonocentric, kMonocentric,lkMonocentricFile,dontCompress = .TRUE.)
    CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/oneIntBas2Orb.xchem",STATUS="replace")
    !CALL writeOneIntBas2Orb(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/twoIntBas2Orb.xchem",STATUS="replace")
    !CALL writeTwoIntBas2Orb(2,occupiedNoSym)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/oneIntOrb2OperatorMatrix.xchem",STATUS="replace")
    !CALL writeOneIntOrb2OperatorMatrix(2,electronsParent+1)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/twoIntOrb2OperatorMatrix.xchem",STATUS="replace")
    !CALL writeTwoIntOrb2OperatorMatrix(2)
    !CLOSE(2)

    !CALL ras3%sumString(nRas3)
    !OPEN(2,file=TRIM(dir)//"/createIntegralCSFMap.xchem",STATUS="replace")
    !CALL writeCreateIntegral2CSFMap(2,occupiedNoSym+2,nRas3)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/sumOneIntTwoIntNuclear.xchem",STATUS="replace")
    !CALL writeSumOneIntTwoIntNuclear(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/oneIntRDMCreate.xchem",STATUS="replace")
    !CALL writeOneIntRDMCreate(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/twoIntRDMCreate.xchem",STATUS="replace")
    !CALL writeTwoIntRDMCreate(2)
    !CLOSE(2)
    
    !OPEN(2,file=TRIM(dir)//"/orthoNormalize.xchem",STATUS="replace")
    !CALL writeOrthoNormalize(2)
    !CLOSE(2)

    !OPEN(2,file=TRIM(dir)//"/linDepKill.xchem",STATUS="replace")
    !CALL writeLinDepKill(2,lMonocentric,kMonocentric,nOrb,linearDependenceThr)
    !CLOSE(2)

    WRITE(6,*) "Happy"
END PROGRAM
