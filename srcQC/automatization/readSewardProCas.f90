!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM readSewardsProCas
    USE class_inputString
    IMPLICIT none
    CHARACTER*200 :: proFile
    CHARACTER*200 :: casFile
    CHARACTER*200 :: outFile
    CHARACTER*300 :: token,fmt
    CHARACTER*300 :: fich
    CHARACTER*25 :: ist,soll
    CHARACTER*100 :: orbitalLabel(56)
    CHARACTER*5 :: occupation
    TYPE(inputString) :: line
    INTEGER :: nSym,iSym,iOrb,nOrb,countOrb,iLab
    INTEGER :: stat
    INTEGER, ALLOCATABLE :: symSize(:)
    LOGICAL :: check,symSeen
    REAL*8 :: energy,temp
    
    orbitalLabel(1)="1s"
    orbitalLabel(2)="2px"
    orbitalLabel(3)="2py"
    orbitalLabel(4)="2pz"
    orbitalLabel(5)="3d2-"
    orbitalLabel(6)="3d1-"
    orbitalLabel(7)="3d0"
    orbitalLabel(8)="3d1+"
    orbitalLabel(9)="3d2+"
    orbitalLabel(10)="3s0"
    orbitalLabel(11)="4f3-"
    orbitalLabel(12)="4f2-"
    orbitalLabel(13)="4f1-"
    orbitalLabel(14)="4f0"
    orbitalLabel(15)="4f1+"
    orbitalLabel(16)="4f2+"
    orbitalLabel(17)="4f3+"
    orbitalLabel(18)="4p1-"
    orbitalLabel(19)="4p0"
    orbitalLabel(20)="4p1+"
    orbitalLabel(21)="5g4-"
    orbitalLabel(22)="5g3-"
    orbitalLabel(23)="5g2-"
    orbitalLabel(24)="5g1-"
    orbitalLabel(25)="5g0"
    orbitalLabel(26)="5g1+"
    orbitalLabel(27)="5g2+"
    orbitalLabel(28)="5g3+"
    orbitalLabel(29)="5g4+"
    orbitalLabel(30)="5d2-"
    orbitalLabel(31)="5d1-"
    orbitalLabel(32)="5d0"
    orbitalLabel(33)="5d1+"
    orbitalLabel(34)="5d2+"
    orbitalLabel(35)="5s0"
    orbitalLabel(36)="6h5-"
    orbitalLabel(37)="6h4-"
    orbitalLabel(38)="6h3-"
    orbitalLabel(39)="6h2-"
    orbitalLabel(40)="6h1-"
    orbitalLabel(41)="6h0"
    orbitalLabel(42)="6h1+"
    orbitalLabel(43)="6h2+"
    orbitalLabel(44)="6h3+"
    orbitalLabel(45)="6h4+"
    orbitalLabel(46)="6h5+"
    orbitalLabel(47)="6f3-"
    orbitalLabel(48)="6f2-"
    orbitalLabel(49)="6f1-"
    orbitalLabel(50)="6f0"
    orbitalLabel(51)="6f1+"
    orbitalLabel(52)="6f2+"
    orbitalLabel(53)="6f3+"
    orbitalLabel(54)="6px"
    orbitalLabel(55)="6py"
    orbitalLabel(56)="6pz"

    CALL GETARG(1,proFile)
    CALL GETARG(2,casFile)
    CALL GETARG(3,outFile)
    IF (    (LEN(TRIM(proFile)).eq.0).OR.&
        (LEN(TRIM(casFile)).eq.0).OR.&
        (LEN(TRIM(outFile)).eq.0)) THEN
        STOP "ERROR input incomlpete"
    ENDIF
    WRITE(6,'(A,A)') "molpro file:", proFile
    WRITE(6,'(A,A)') "molcas file:", casFile
    WRITE(6,'(A,A)') "output File:", outFile
    
    OPEN(1,FILE=proFile, STATUS="old")
    OPEN(2,FILE=casFile, STATUS="old")
    OPEN(3,FILE=outFile, STATUS="replace")
    
    DO      
        READ(2,'(A)',IOSTAT=stat) token
        IF (IS_IOSTAT_END(stat)) THEN
            STOP "Basis set specifications not found in molcas file"
        ENDIF
        IF (INDEX(token,"Basis set specifications").gt.0) EXIT
    ENDDO
    READ(2,'(A)') token
    CALL line%setString(token)
    CALL line%process()
    CALL line%sizeString(nSym)
    nSym=nSym-2
    ALLOCATE(symSize(nSym))
    
    PRINT*, "number of symmetries",nSym
    
    READ(2,*) fich,token,(symSize(iSym),iSym=1,nSym)
    WRITE(fmt,'(A,I0,A)') "(A,",nSym,"(x,I3))"
    WRITE(6,TRIM(fmt)) "Basis functions:",((symSize(iSym)),iSym=1,nSym)
    
    
    WRITE(6,'(A,x,A)') "Looking for NATURAL ORBITALS in",TRIM(proFile)
    DO
        READ(1,'(A)',IOSTAT=stat) token
        IF (IS_IOSTAT_END(stat)) EXIT
        IF (INDEX(token,"NATURAL ORBITALS").ne.0)  THEN
            WRITE(6,'(A,x,A)') "NATURAL ORBITALS found in",TRIM(proFile)
            EXIT 
        ENDIF
    ENDDO
    IF (IS_IOSTAT_END(stat)) THEN
        WRITE(6,'(A,x,A)') "NATURAL ORBITALS not found in",TRIM(proFile)
        REWIND(1)
        WRITE(6,'(A,x,A)') "Looking for ELECTRON ORBITALS in",TRIM(proFile)
        DO
            READ(1,'(A)',IOSTAT=stat) token
            IF (IS_IOSTAT_END(stat)) THEN
                STOP "Neither ELECTRON nor NATURAL ORBITALS could be found in molpro file"
            ENDIF
            IF (INDEX(token,"ELECTRON ORBITALS").ne.0) THEN
                WRITE(6,'(A,x,A)') "ELECTRON ORBITALS found in",TRIM(proFile)
                EXIT 
            ENDIF
        ENDDO
    ENDIF
    !DEBUG
    REWIND(2)
    !END DEBUG
    DO 
        READ(2,'(A)') token
        IF (INDEX(token,"Symmetry adapted Basis Functions").ne.0) EXIT
    ENDDO
    WRITE(fich,'(A,A)') TRIM(proFile),".OrbEnergies"
    OPEN(101,file=fich,STATUS="replace")
    WRITE(fich,'(A,A)') TRIM(proFile),".OrbOccupations"
    OPEN(102,file=fich,STATUS="replace")
    
    WRITE(3,'(A)') "orb.pro"
    WRITE(3,'(A)') "symOrb"
    WRITE(3,'(I1)') nSym
    WRITE(101,*) nsym
    WRITE(102,*) nsym
    DO iSym=1,nSym
        symSeen=.FALSE.
        WRITE(3,'(I3)') symSize(iSym)
        IF (symSize(iSym).eq.0) THEN
            CYCLE
        ENDIF
        countOrb=0
        DO
            READ(1,'(A)') token
            !CALL line%setString(token)
            !CALL line%process()
            !CALL line%sizeString(nOrb)
            check=.FALSE.
            DO iLab=1,SIZE(orbitalLabel)
                IF (INDEX(token,TRIM(orbitalLabel(iLab))).ne.0) THEN
                    check=.TRUE.
                ENDIF
            ENDDO
            IF (check) THEN
                WRITE(3,'(A)') TRIM(token)
            ELSE
                IF (countOrb+1.lt.10) THEN
                    WRITE(soll,'(A1,I1,A,I1,A1)') " ",countOrb+1,".",iSym," "
                ELSE
                    WRITE(soll,'(A1,I2,A,I1,A1)') " ",countOrb+1,".",iSym," "
                ENDIF

                IF (INDEX(token(1:10),TRIM(soll)).ne.0) THEN
                    IF (.NOT.(symSeen)) THEN
                        WRITE(101,*) symSize(iSym)
                        WRITE(102,*) symSize(iSym)
                    ENDIF 
                    READ(token,*) temp,occupation,energy       
                    WRITE(101,*) energy
                    WRITE(102,*) TRIM(occupation)
                    symSeen=.TRUE.
                    countOrb=countOrb+1
                    IF (countOrb.eq.symSize(iSym)) EXIT
                ENDIF
            ENDIF
        ENDDO
        DO iOrb=1,symSize(iSym)
            DO 
                READ(2,'(A)') token
                check=.FALSE.
                DO iLab=1,SIZE(orbitalLabel)
                    IF (INDEX(token,TRIM(orbitalLabel(iLab))).ne.0) THEN
                        check=.TRUE.
                    ENDIF
                ENDDO
                IF (check) THEN
                    WRITE(3,'(A)') TRIM(token)
                    EXIT
                ENDIF
            ENDDO
        ENDDO 
    ENDDO
    WRITE(6,'(A)') "Happy"
END PROGRAM
