!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM reOrderSym2NoSym
    USE class_inputString
    USE orbitalIO
    IMPLICIT none
    
    CHARACTER*200 :: infoFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: outFileCasFormat
    CHARACTER*200 :: outFileBinFormat
    CHARACTER*200 :: referenceRasorb
    CHARACTER*200 :: rasOrbFile
    CHARACTER*200 :: token
    TYPE(inputString) :: line
    TYPE(orbital_) :: orbIn
    TYPE(orbital_) :: orbOut

    INTEGER :: nSym,iSym,iOrb,jOrb,nOrb,nReOrder,kOrb,nBas
    INTEGER :: counter1, counter2, temp
    INTEGER, ALLOCATABLE :: symSize(:)
    INTEGER, ALLOCATABLE :: ras1(:)
    INTEGER, ALLOCATABLE :: ras2(:)
    INTEGER, ALLOCATABLE :: ras3(:)
    INTEGER, ALLOCATABLE :: inactive(:)
    INTEGER, ALLOCATABLE :: reOrder(:)
    INTEGER, ALLOCATABLE :: reOrderMatrix(:,:)
    REAL*8, ALLOCATABLE :: orb(:,:)
    REAL*8, ALLOCATABLE :: orbRe(:,:)
    REAL*8, ALLOCATABLE :: energies(:)
    REAL*8 :: e
    LOGICAL :: check
    
    CALL GETARG(1,infoFile)
    CALL GETARG(2,orbitalFile)
    CALL GETARG(3,referenceRasorb)
    CALL GETARG(4,outFileBinFormat)
    CALL GETARG(5,rasOrbFile)
    IF (    (LEN(TRIM(infoFile)).eq.0).OR.&
        (LEN(TRIM(orbitalFile)).eq.0).OR.&
        (LEN(TRIM(referenceRasorb)).eq.0).OR.&
        (LEN(TRIM(outFileBinFormat)).eq.0)) THEN
        STOP "ERROR input incomlpete"
    ENDIF
    
    OPEN(10,file=infoFile,STATUS="old")
    OPEN(30,file=referenceRasorb,STATUS="old")
    
    READ(10,*)
    READ(10,'(A)') token
    CALL line%setString(token)
    CALL line%process()
    CALL line%sizeString(nSym)
    
    ALLOCATE(symSize(nSym))
    ALLOCATE(ras1(nSym))
    ALLOCATE(ras2(nSym))
    ALLOCATE(ras3(nSym))
    ALLOCATE(inactive(nSym))
    
    READ(10,*) (symSize(iSym),iSym=1,nSym)
    READ(10,*) (inactive(iSym),iSym=1,nSym)
    READ(10,*) (ras1(iSym),iSym=1,nSym)
    READ(10,*) (ras2(iSym),iSym=1,nSym)
    READ(10,*) (ras3(iSym),iSym=1,nSym)
    
    nOrb=SUM(symSize)
    nReOrder=SUM(inactive)+SUM(ras1)+SUM(ras2)+SUM(ras3)
    
    ALLOCATE(energies(nOrb))
    ALLOCATE(reOrder(nOrb))
    ALLOCATE(reOrderMatrix(nOrb,nOrb))
    ALLOCATE(orbRe(nOrb,nOrb))

    CALL orbIn%readOrbital(orbitalFile,'f')
    CALL orbIn%getOrbital(orb)

    DO 
        READ(30,'(A)') token
        IF (INDEX(token,"#ONE").gt.0) EXIT
    ENDDO
    READ(30,*)
    counter1=0
    DO iSym=1,nSym
        IF (symSize(iSym).eq.0) CYCLE
        !READ(30,'(4(E18.12))') (energies(iOrb+counter1),iOrb=1,inactive(iSym)),&
        READ(30,'(10(1X,ES11.4))') (energies(iOrb+counter1),iOrb=1,inactive(iSym)),&
          (e,iOrb=1,symSize(iSym)-inactive(iSym))
        counter1=counter1+inactive(iSym)
        !print*,"DEBUG ", counter1
    ENDDO
    REWIND(30)
    
    DO 
        READ(30,'(A)') token
        IF (INDEX(token,"#ONE").gt.0) EXIT
    ENDDO
    READ(30,*)
    DO iSym=1,nSym
        IF (symSize(iSym).eq.0) CYCLE
        !READ(30,'(4(E18.12))') (e,iOrb=1,inactive(iSym)),&
        READ(30,'(10(1X,ES11.4))') (e,iOrb=1,inactive(iSym)),&
          (energies(iOrb+counter1),iOrb=1,ras1(iSym)),&
          (e,iOrb=1,symSize(iSym)-inactive(iSym)-ras1(iSym))
        counter1=counter1+ras1(iSym)
        !print*, counter1
    ENDDO
    REWIND(30)

    
    DO 
        READ(30,'(A)') token
        IF (INDEX(token,"#ONE").gt.0) EXIT
    ENDDO
    READ(30,*)
    DO iSym=1,nSym
        IF (symSize(iSym).eq.0) CYCLE
        !READ(30,'(4(E18.12))') (e,iOrb=1,inactive(iSym)+ras1(iSym)),&
        READ(30,'(10(1X,ES11.4))') (e,iOrb=1,inactive(iSym)+ras1(iSym)),&
          (energies(iOrb+counter1),iOrb=1,ras2(iSym)),&
          (e,iOrb=1,symSize(iSym)-inactive(iSym)-ras1(iSym)-ras2(iSym))
        counter1=counter1+ras2(iSym)
        !print*, counter1
    ENDDO
    REWIND(30)

    DO 
        READ(30,'(A)') token
        IF (INDEX(token,"#ONE").gt.0) EXIT
    ENDDO
    READ(30,*)
    DO iSym=1,nSym
        IF (symSize(iSym).eq.0) CYCLE
        !READ(30,'(4(E18.12))') (e,iOrb=1,inactive(iSym)+ras1(iSym)+ras2(iSym)),&
        READ(30,'(10(1X,ES11.4))') (e,iOrb=1,inactive(iSym)+ras1(iSym)+ras2(iSym)),&
          (energies(iOrb+counter1),iOrb=1,ras3(iSym)),&
          (e,iOrb=1,symSize(iSym)-inactive(iSym)-ras1(iSym)-ras2(iSym)-ras3(iSym))
        counter1=counter1+ras3(iSym)
        !print*, counter1
    ENDDO
    REWIND(30)

    DO 
        READ(30,'(A)') token
        IF (INDEX(token,"#ONE").gt.0) EXIT
    ENDDO
    READ(30,*)
    DO iSym=1,nSym
        IF (symSize(iSym).eq.0) CYCLE
        !READ(30,'(4(E18.12))') (e,iOrb=1,inactive(iSym)+ras1(iSym)+ras2(iSym)+ras3(iSym)),&
        READ(30,'(10(1X,ES11.4))') (e,iOrb=1,inactive(iSym)+ras1(iSym)+ras2(iSym)+ras3(iSym)),&
          (energies(iOrb+counter1),iOrb=1,symSize(iSym)-inactive(iSym)-ras1(iSym)-ras2(iSym)-ras3(iSym))
        counter1=counter1+symSize(iSym)-inactive(iSym)-ras1(iSym)-ras2(iSym)-ras3(iSym)
        !print*, counter1
    ENDDO
    
    !WRITE(6,'(A)') "Orbital energies in symmetry order"
    !WRITE(6,'(10(F8.4))') (energies(iOrb),iOrb=1,nOrb)
    
    DO iOrb=1,nOrb
        reOrder(iOrb)=iOrb
    ENDDO
    DO iOrb=1,nOrb
        check=.FALSE.
        DO jOrb=iOrb+1,nOrb
            IF (energies(iOrb).gt.energies(jOrb)) THEN
                e=energies(jOrb)
                DO kOrb=jOrb,iOrb+1,-1
                    energies(kOrb)=energies(kOrb-1)
                ENDDO
                energies(iOrb)=e        
                
                temp=reOrder(jOrb)
                DO kOrb=jOrb,iOrb+1,-1
                    reOrder(kOrb)=reOrder(kOrb-1)
                ENDDO
                reOrder(iOrb)=temp
            ENDIF
        ENDDO
    ENDDO
    
    reOrderMatrix=0
    DO iOrb=1,nOrb
        reOrderMatrix(reOrder(iOrb),iOrb)=1
    ENDDO
    
    !WRITE(6,'(A)') "Orbital energies in desymmetry order"
    !WRITE(6,'(10(F8.4))') (energies(iOrb),iOrb=1,nOrb)
    
    DO iOrb=1,nOrb
        reOrder(iOrb)=MAXLOC(reOrderMatrix(iOrb,:),1)
    ENDDO
    
    WRITE(6,'(A)') "Reoder from DESY to non symmetric"
    WRITE(6,'(10(x,I3))') (reOrder(iOrb),iOrb=1,nOrb)
    
    
    DO iOrb=1,nOrb
        orbRe(iOrb,:)=MATMUL(reOrderMatrix,orb(iOrb,:))
    ENDDO 

    CALL orbOut%initializeEmpty(nOrb)
    CALL orbOut%setSym(1,orbRe)
    CALL orbOut%writeOrbital(rasOrbFile,'f')
    CALL orbOut%writeOrbital(outFileBinFormat,'b')
END PROGRAM
