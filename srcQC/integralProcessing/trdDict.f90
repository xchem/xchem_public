!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program trdFiles

use Configuration_

implicit none
integer :: norb,totorb
type(Configuration) inCSFs

integer nCSF
character*100 fich,tmpDet

integer totalDets,nBlocks,iBlock,nLocalCSF,nCSF2Cons
integer, allocatable :: blocksInf(:,:),InfCSFs(:),dims(:)
logical, allocatable :: Detlist(:,:,:)
logical, allocatable :: tmpDetlist(:,:)
integer, allocatable :: ndet(:)
real*8, allocatable :: amp(:,:)
real*8, allocatable :: RDM(:,:,:)
logical, allocatable :: CSF2Consider(:)
real*8 :: RDMthrs=1e-16, CSFthrs=1e-12

integer :: mdet,idet,jdet
logical :: RowColEq
integer :: iCSF,i,j,jCSF
integer nRDM
character*200 way,wayDet

write(6,*) "The rows and columns of the part of the nCSF x nCSF matrix are the same( yes or no)"
read(5,*) way
write(6,*) "if yes: FirstRow, LastRow. If no: FirstRow, LastRow, FirstCol, LastCol."
if (trim(way) .eq. 'yes') then
 allocate(dims(2))
 RowColEq = .true.
 read(5,*) dims(1),dims(2)
 FirstCol = FirstRow
 LastCol = LastRow 
else
 allocate(dims(4))
 RowColEq = .false.
 read(5,*) dims(1),dims(2),dims(3),dims(4)
endif
write(6,*) "File with the CI vector (in CSFs) and format (ascii or binary)"
read(5,*) fich,way
write(6,*) trim(fich)," ",trim(way)
call readCSF0(fich,way,1,npot,nCSF)
if(RowColEq) then
 mLocalCSF = dims(2)-dims(1)+1
else
 if ((maxval(dims(:))-minval(dims(:))+1) .ge. (dims(2)-dims(1)+1 +dims(4)-dims(3)+1 )) then
  nLocalCSF = dims(2)-dims(1)+1 +dims(4)-dims(3)+1
 else
  nLocalCSF = (maxval(dims(:))-minval(dims(:))+1
 endif
endif
allocate(CICSF(npot,nLocalCSF),CSF2Consider(nLocalCSF))
if (RowColEq) then
 call readCSFBlock(way,1,npot,nCSF,nLocalCSF,CICSF,dims(1),dims(2),InfCSFs)
else 
 call readCSFBlock(way,1,npot,nCSF,nLocalCSF,CICSF,dims(1),dims(2),InfCSFs,dims(3),dims(4))
endif
write(6,*) "CI vector in CSFs ready ",nCSF,npot
CSF2Consider=.false.
counter=0
do iCSF=1,nLocalCSF
 counter = counter + 1
 if (abs(minval(CICSF(:,i))) .gt. CSFthrs .or. abs(maxval(CICSF(:,i))) .gt. CSFthrs) CSF2Consider(iCSF) =.true.
enddo
nCSF2Cons = counter
write(6,*) 'Number of CSF to consider',counter,nLocalCSF
write(6,*) "File with the guga table"
read(5,*) fich 
open(1,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File does not exist ",trim(fich)
 stop 'File with guga table is compulsory'
endif
!!! Reading nCSF and norb from guga table
read(1,*) i,norb
if (i.ne.nCSF) then
 write(6,*) "Number of configurations GUGA table ",i
 write(6,*) "Number of configurations in CI vector ",nCSF
 stop 'Number of configurations different in GUGA and CI'
endif
mdet=0
call inCSFs%initConfiguration(norb,nCSF2Cons)
call inCSFs%readit(1,mdet,CSF2Consider,InfCSFs)
close(1)
write(6,*) "Number of inactive orbitals"
read(5,*) totorb
totorb=totorb+norb
!! Take maxdet
mdet=0
do iCSF=1,nCSF2Cons
 call inCSFs%takenDet(iCSF,idet)
 if (idet.ge.mdet) mdet=idet
enddo
 
write(6,*) "Allocating one supervector of Determinants ",mdet
allocate(tmpDetlist(mdet,norb))
write(6,*) "Total orbitals ",totorb
allocate(RDM(totorb,totorb,nCSF2Cons))

allocate(Detlist(norb,mdet,nCSF2Cons),amp(mdet,nCSF2Cons),ndet(nCSF2Cons))

write(6,*) "File with the output"
read(5,*) fich
icounter = 0
do iCSF=dims(1),dims(2)
 icounter
 call inCSFs%takeDet(mdet,iCSF,tmpDetlist,ndet(iCSF),amp(:,iCSF))
 write(6,*) "Checking ",iCSF,nLocalCSF,ndet(iCSF)
 RDM=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE(jCSF,i,j)
 do jCSF=iCSF+1,blocksInf(iBlock,3)-blocksInf(iBlock,4)+1 
!!! Creating RDM
  do i=1,ndet(iCSF)
   do j=1,ndet(jCSF)
    call doRDM(norb,Detlist(:,i,iCSF),amp(i,iCSF),Detlist(:,j,jCSF),amp(j,jCSF),RDM(:,:,jCSF),totorb)
   enddo
  enddo
 enddo
!$OMP END PARALLEL DO
 write(6,*) "End parallel"
!!! Checking RDMs different of zero
 do jCSF=1,nLocalCSF
   nRDM=0
   do i=1,totorb
    do j=1,totorb
     if (RDM(i,j,jCSF)**2.ge.RDMthrs) nRDM=nRDM+1
    enddo
   enddo
!!! Writting RDM different of zero
   if (nRDM.gt.0) then
    write(1) iCSF,jCSF,nRDM
    do i=1,totorb
     do j=1,totorb
      if (RDM(i,j,jCSF)**2.ge.RDMthrs) then
       write(1) i,j,RDM(i,j,jCSF)
      endif
     enddo
    enddo
   endif

 enddo
enddo

end program
subroutine doRDM(norb,Det1,amp1,Det2,amp2,RDM,totorb)
implicit none
integer, intent(in) :: norb,totorb
integer*1, intent(in) :: Det1(norb),Det2(norb)
real*8, intent(in) :: amp1,amp2
real*8, intent(inout) :: RDM(totorb,totorb)
real*8 start,finish
logical sDet1(2*norb),sDet2(2*norb)

integer ndiff
integer iorb,jorb
real*8 sRDM(2*totorb,2*totorb)
integer diforb(2),signo
real*8 kk
integer sinorb
call cpu_time(finish)
print*, '("Time = ",f6.3," seconds.")',finish-start
!!! Transform to spin-orbitals
sDet1=.false.
sDet2=.false.
do iorb=1,norb
 if (Det1(iorb).eq.1) then
  sDet1(iorb*2-1)=.true.
 endif
 if (Det2(iorb).eq.1) then
  sDet2(iorb*2-1)=.true.
 endif
 if (Det1(iorb).eq.2) then
  sDet1(iorb*2-0)=.true.
 endif
 if (Det2(iorb).eq.2) then
  sDet2(iorb*2-0)=.true.
 endif
 if (Det1(iorb).eq.3) then
  sDet1(iorb*2-0)=.true.
  sDet1(iorb*2-1)=.true.
 endif
 if (Det2(iorb).eq.3) then
  sDet2(iorb*2-0)=.true.
  sDet2(iorb*2-1)=.true.
 endif
enddo

ndiff=0
do iorb=1,norb*2
 if (sDet1(iorb).neqv.sDet2(iorb)) then
  ndiff=ndiff+1
 endif
enddo

if (ndiff.le.2) then
 sinorb=totorb-norb
 sinorb=sinorb*2
 sRDM=0.
 if (ndiff.eq.0) then
  do iorb=1,sinorb
   sRDM(iorb,iorb)=sRDM(iorb,iorb)+amp1*amp2
  enddo
  do iorb=1,norb*2
   if (sDet1(iorb).and.sDet2(iorb)) then
    sRDM(sinorb+iorb,sinorb+iorb)=sRDM(sinorb+iorb,sinorb+iorb)+amp1*amp2
   endif
  enddo
 else
  do iorb=1,norb*2
   if (sDet1(iorb).neqv.sDet2(iorb)) then
    if (sDet1(iorb)) diforb(1)=iorb
    if (sDet2(iorb)) diforb(2)=iorb
   endif
  enddo
  do iorb=1,norb*2
   if (sDet1(iorb) .and. iorb.lt.abs(diforb(1)) ) diforb(1)=-diforb(1)
   if (sDet2(iorb) .and. iorb.lt.abs(diforb(2)) ) diforb(2)=-diforb(2)
  enddo
  kk=amp1*amp2
  do iorb=1,2
   if (diforb(iorb).lt.0) then
    kk=-kk
    diforb(iorb)=-diforb(iorb)
   endif
  enddo
  sRDM(sinorb+diforb(1),sinorb+diforb(2))=sRDM(sinorb+diforb(1),sinorb+diforb(2))+kk
 endif
!!! Going to spatial orbitals
 do iorb=1,totorb
  do jorb=1,totorb
   RDM(iorb,jorb)=RDM(iorb,jorb)+sRDM(2*iorb-1,2*jorb-1)+sRDM(2*iorb,2*jorb)
  enddo
 enddo
endif

return
end subroutine doRDM

