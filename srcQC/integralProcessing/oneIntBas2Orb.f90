!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntBas2Orb
!CODE THAT TRANSFORMS ONE ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRELS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.
    !USE omp_lib
    USE oneIntIO
    USE orbitalIO
    USE class_inputString
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(oneInt_) :: hBas
    TYPE(oneInt_) :: hOrb
    TYPE(orbital_) :: orb
    TYPE(inputString) :: inString
    TYPE(inputString) :: outString
    TYPE(inputString) :: hermitianString
    CHARACTER*200, ALLOCATABLE :: hermitianMat(:)
    CHARACTER*200, ALLOCATABLE :: inFiles(:)
    CHARACTER*200, ALLOCATABLE :: outFiles(:)
    
    INTEGER :: occupied
    INTEGER :: nFiles,iFiles
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: oneElectronOperatorBasisFiles
    CHARACTER*200 :: oneElectronOperatorOrbitalFiles
    CHARACTER*200 :: hermitian
    
    REAL*8, ALLOCATABLE :: oneInt(:,:),orb1(:),orb2(:),ov(:,:)
    INTEGER :: iBas,jBas,iOrb,jOrb
    INTEGER :: nBas
    INTEGER :: workingUnit
    !PARALLELIZATION VARIABLES        
    !INTEGER :: nCPU
    
    NAMELIST /INPUT/        orbitalFile,&
                            occupied,&
                            oneElectronOperatorBasisFiles,&
                            oneElectronOperatorOrbitalFiles,&
                            hermitian
                            
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    
    CALL hermitianString%setString(TRIM(hermitian))
    CALL hermitianString%process("-")
    CALL hermitianString%getArray(hermitianMat)
    
    CALL orb%readOrbital(orbitalFile,"b") 
    CALL inString%setString(oneElectronOperatorBasisFiles)
    CALL outString%setString(oneElectronOperatorOrbitalFiles)
    CALL inString%process("-")
    CALL outString%process("-")
    CALL inString%sizeString(nFiles)
    CALL outString%sizeString(iBas)
    IF (nFiles.ne.iBas) THEN        
        STOP "One and only one ouput files must be provided for each input file"
    ENDIF
    CALL inString%getCharArray(inFiles)
    CALL outString%getCharArray(outFiles)
    
    !nCPU=OMP_get_max_threads()
    !WRITE(6,'(A,x,I4)') "number of available threads:",nCPU        
    !IF (nFiles.le.nCPU) THEN
    !        CALL omp_set_num_threads(nFiles)
    !ELSE
    !        CALL omp_set_num_threads(nCPU)
    !ENDIF
    !nCPU=OMP_get_max_threads()
    !WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU        
    
    !!$OMP PARALLEL &
    !!$OMP DEFAULT (none) &
    !!$OMP SHARED (nFiles,occupied,inFiles,outFiles,orb) &
    !!$OMP PRIVATE (iFiles,ov,nBas,oneInt,iOrb,jOrb,iBas,jBas,hOrb,hBas,orb1,orb2) 
    !!$OMP DO
    DO iFiles=1,nFiles
        workingUnit=500+iFiles
        ALLOCATE(ov(occupied,occupied))
        ov=0.d0
        WRITE(6,'(A,A)') "Processing file: ",TRIM(inFiles(iFiles))
        CALL hBas%readOneInt(TRIM(inFiles(iFiles)),"b",.TRUE.,workingUnit,TRIM(hermitianMat(iFiles)))
        CALL hBas%getSym(oneInt)
        nBas=SIZE(oneInt,1)
        
        DO iOrb=1,occupied
            CALL orb%getOrbital(orb1,iOrb)
            DO jOrb=1,occupied
                CALL orb%getOrbital(orb2,jOrb)
                DO iBas=1,nBas
                    DO jBas=1,nBas
                        ov(iOrb,jOrb)=ov(iOrb,jOrb)+orb1(iBas)*orb2(jBas)*oneInt(iBas,jBas)
                    ENDDO
                ENDDO
            ENDDO
        ENDDO
                
        CALL hBas%destroy
        
        CALL hOrb%initializeEmpty(occupied)
        CALL hOrb%setSym(ov)
        CALL hOrb%writeOneInt(TRIM(outFiles(iFiles))//".fmt","f",.TRUE.,workingUnit)
        CALL hOrb%writeOneInt(TRIM(outFiles(iFiles)),"b",.FALSE.,workingUnit)
        CALL hOrb%destroy
        DEALLOCATE(ov)
    ENDDO
    !!$OMP END DO
    !!$OMP END PARALLEL
    
    WRITE(6,'(A)') "Happy"
END PROGRAM
