!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM trdBuild
!Code that read a set of i-j.trd2 files for combination of states
!and assambles these into the complete two electron reduced density 
!matrix. Furthermore the one electron density matrix is produced from
!the assambled two electron density matrix
    IMPLICIT none
!CONTROL VARIABLES
    INTEGER :: stat
    LOGICAL :: exists
    CHARACTER*200 :: inputFile
!INPUT VARIABLES
    CHARACTER*200 :: trd1ElementFileList
    CHARACTER*200 :: trd1File
    CHARACTER*100 :: outputFormat
!STRUCTURE VARIABLES
    REAL*8, ALLOCATABLE :: rdm1(:,:,:)
    CHARACTER*200 :: trd1ElementFile
    INTEGER :: nState
    INTEGER :: nOrb
    LOGICAL :: broken
!TEMP VARS
    INTEGER :: counter
    INTEGER :: nElement
    INTEGER :: iElement
    INTEGER :: state1,state2
    INTEGER :: iOrb,jOrb,kOrb,lOrb,ind
    INTEGER :: lowOrb
    INTEGER :: nPm,iPm,pm(8,4)
    REAL*8 :: rdmVal

    NAMELIST /INPUT/ trd1ElementFileList,&
      trd1File,&
      outputFormat

    outputFormat="b"
    trd1File="trd1"
    trd1ElementFileList=""

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)      

    WRITE(6,'(A)') "Assembling trd1 from element files"
    OPEN(1,FILE=TRIM(trd1ElementFileList),STATUS="old")
    counter=0
    broken=.FALSE.
    DO
        READ(1,*,IOSTAT=stat) trd1ElementFile  
        IF (stat.ne.0) EXIT
        counter=counter+1
        OPEN(2,FILE=TRIM(trd1ElementFile),STATUS="old",FORM="unformatted")
        READ(2,IOSTAT=stat) nOrb,nState,state1,state2,nElement
        IF (stat.ne.0) THEN
            broken=.TRUE.
            WRITE(6,'(A,A)') "erroneous trd1 file: empty ",TRIM(trd1ElementFile)
            CYCLE
        ENDIF
        IF (counter.eq.1) THEN
            WRITE(6,'(A,F12.6,A)') "Allocate and initialize rdm1 matrix with. ",REAL(nOrb**2*nState*(nState+1)*8)/1024.**2,&
                " MB needed"
            ALLOCATE(rdm1(nOrb,nOrb,nState*(nState+1)/2))
            rdm1=0.d0
        ENDIF
        WRITE(6,'(A,I0,A,A,A,I0,A,I0)') "Reading ",nElement," values from file ",TRIM(trd1ElementFile)," for states: ",&
            state1," and ",state2
        DO iElement=1,nElement
            READ(2,IOSTAT=stat) iOrb,jOrb,rdmVal
            IF (stat.ne.0) THEN
                broken=.TRUE.
                WRITE(6,'(A,A)') "erroneous trd1 file: incomplete ",TRIM(trd1ElementFile)
                EXIT
            ENDIF
            rdm1(iOrb,jOrb,state1*(state1-1)/2+state2)=rdmVal
        ENDDO
        CLOSE(2)
    ENDDO
    CLOSE(1)
    IF (broken) THEN
        STOP "ERROR: some trd1 files were incomplete"
    ENDIF

    !WRITE(6,'(A)') "Use trd2 to create trd1"
    !WRITE(6,'(A,F12.6,A)') "Allocate and initialize rdm1 matrix with. ",REAL(nOrb**2*nState*(nState+1)*8)/1024**2," MB needed"
    !ALLOCATE(rdm1(nOrb,nOrb,nState*(nState+1)/2))
    !rdm1=0.d0
    !DO iOrb=1,nOrb
    !    DO jOrb=1,nOrb
    !        DO kOrb=1,nOrb
    !            rdm1(iOrb,jOrb,:)=rdm1(iOrb,jOrb,:)+rdm2(iOrb,kOrb,jOrb,kOrb,:)
    !        ENDDO
    !    ENDDO
    !ENDDO
    !!Multiplying by two to account for integration over spins
    !rdm1=rdm1*2

    WRITE(6,'(A)') "Writing trd1"
    IF (outputFormat(1:1).eq."b") THEN
        OPEN(2,FILE=TRIM(trd1File),STATUS="replace",FORM="unformatted")
        WRITE(2) nOrb,nState
    ELSE
        OPEN(2,FILE=TRIM(trd1File),STATUS="replace")
        WRITE(2,*) nOrb,nState
    ENDIF
    DO iOrb=1,nOrb
        DO jOrb=1,nOrb
            IF (ANY(ABS(rdm1(iOrb,jOrb,:)).ge.1e-14)) THEN
                IF (outputFormat(1:1).eq."b") THEN
                    WRITE (2) iOrb,jOrb,.FALSE.
                    WRITE(2) (rdm1(iOrb,jOrb,ind),ind=1,nState*(nState+1)/2)
                ELSE
                    WRITE (2,'(2(x,I0),x,L)') iOrb,jOrb,.FALSE.
                    WRITE(2,*) (rdm1(iOrb,jOrb,ind),ind=1,nState*(nState+1)/2)
                ENDIF
            ELSE
                IF (outputFormat(1:1).eq."b") THEN
                    WRITE (2) iOrb,jOrb,.TRUE.
                ELSE
                    WRITE (2,'(2(x,I0),x,L)') iOrb,jOrb,.TRUE.
                ENDIF
            ENDIF
        ENDDO
    ENDDO
    WRITE(6,'(A)') "Happy"
END PROGRAM
