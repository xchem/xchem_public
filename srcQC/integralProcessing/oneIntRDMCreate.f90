!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntRDMCreate
!PROGRAM THAT FROM A LIST OF CI VECTORS (ciVector_), A TWO ELECTRON 
!INTEGRAL TABLE (oneInt_ ) AND A MAP FROM INTEGRALS TO CSF PAIRS (oneInt2CSFMap_)
!CREATES THE MATRIX ELEMENTS FOR THESE CI VECTORS
    USE ciVectorIO
    USE RDMIO
    USE oneIntCSFMapIO
    USE class_inputString
    USE stopWatch
    USE omp_lib
    IMPLICIT none
    LOGICAL :: debug=.FALSE.
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(ciVector_) :: ciVector
    TYPE(oneIntCSFMap_) :: oneIntCSFMap
    TYPE(RDM_) :: oneIntRDM
    CHARACTER*200 :: inputFile,fmt,token
    CHARACTER*200 :: ciVectorFile
    CHARACTER*200 :: oneIntCSFMapFile
    CHARACTER*200 :: oneIntRDMFile
    CHARACTER*200 :: outputFormat

    REAL*8, ALLOCATABLE :: rdmMat(:,:)        
    REAL*8, ALLOCATABLE :: ciMx(:,:),tempVec(:)
    REAL*8 :: val

    INTEGER, ALLOCATABLE :: csf(:,:)
    INTEGER, ALLOCATABLE :: zahler(:),nenner(:)
        
    INTEGER :: progress,progressTemp,nRecord
    INTEGER :: counter
    INTEGER*4 :: nCPU
    INTEGER :: occupied,i,j
    INTEGER :: nCSF,iCSF,jCSF,tCSF        
    INTEGER :: nState,iState,jState
    
    NAMELIST /INPUT/ ciVectorFile,&
      oneIntCSFMapFile,&
      oneIntRDMFile,&
      outputFormat

    outputFormat="b"

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT)
    !READ(1,NML=INPUT,IOSTAT=stat)
    !IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    IF (LEN(oneIntRDMFile).eq.0) STOP "ERROR must provide output file"

    CALL ciVector%readCI(TRIM(ciVectorFile),"f")
    CALL ciVector%getNState(nState)
    CALL ciVector%getNCSF(tCSF)
    
    WRITE(6,'(A,I0)') "Number of States found in CIVectorFile: ",nState

    ALLOCATE(ciMx(tCSF,nState))
    DO iState=1,nState
        CALL ciVector%getCiVector(iState,tempVec)
        ciMx(:,iState)=tempVec
    ENDDO

    CALL oneIntCSFMap%openOneIntCSFMap(TRIM(oneIntCSFMapFile),2)
    CALL oneIntCSFMap%getOccupied(occupied)
    WRITE(6,'(A,I0)') "Number of Orbitals in CSFMap: ",occupied

    IF (outputFormat(1:1).eq."b") THEN
        CALL oneIntRDM%initializeNew(occupied,nState,oneIntRDMFile,"b",3)
    ELSE
        CALL oneIntRDM%initializeNew(occupied,nState,oneIntRDMFile,"f",3)
    ENDIF

    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of available threads:",nCPU        
    IF (nState.le.nCPU) THEN
        CALL omp_set_num_threads(nState)
    ELSE
        CALL omp_set_num_threads(nCPU)
    ENDIF
    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU        

    ALLOCATE(rdmMat(nState,nState))
    CALL timer%initialize()
    counter=0
    progressTemp=-1
    nRecord=occupied**2
    DO 
        counter=counter+1
        progress=FLOOR(REAL(counter)/REAL(nRecord)*100+0.5)
        rdmMat=0.d0
        CALL oneIntCSFMap%getRecord(i,j,nCSF,csf,zahler,nenner,stat)
        IF (IS_IOSTAT_END(stat)) THEN
            EXIT
        ENDIF

        IF (nCSF.eq.0) THEN
            CALL oneIntRDM%writeNextRecordInfo(i,j,.TRUE.)
        ELSE
            !$OMP PARALLEL &
            !$OMP DEFAULT (none) &
            !$OMP SHARED (nState,nCSF,ciMX,zahler,nenner,rdmMat,csf) &
            !$OMP PRIVATE (iState,jState,iCSF,val) 
            !$OMP DO COLLAPSE(2)
            DO iState=1,nState
                DO jState=1,nState
                    val=0.d0
                    DO iCSF=1,nCSF
                        val=val+&
                          ciMx(csf(iCSF,1),iState)*ciMx(csf(iCSF,2),jState)*&
                          SQRT(ABS(REAL(zahler(iCSF)))/REAL(nenner(iCSF)))*&
                          zahler(iCSF)/ABS(zahler(iCSF))  
                    ENDDO
                    rdmMat(jState,iState)=rdmMat(jState,iState)+val
                ENDDO
            ENDDO
            !$OMP END DO NOWAIT
            !$OMP END PARALLEL
            CALL oneIntRDM%writeNextRecordInfo(i,j,.FALSE.)
            CALL oneIntRDM%writeNextRecordData(nState,rdmMat)
        ENDIF
        IF (progress.ne.progressTemp) THEN
            WRITE(token,'(A,I0,A,I0,A,I0,A)') "progress: ",counter," of ",nRecord," records (",progress,"%)"
            CALL timer%time(TRIM(token))
            progressTemp=progress
        ENDIF
    ENDDO
    WRITE(6,'(A)') "Happy"
END PROGRAM 
