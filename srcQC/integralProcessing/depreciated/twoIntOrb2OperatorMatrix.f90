!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntOrb2OperatorMatrix
!PROGRAM THAT FROM A LIST OF CI VECTORS (ciVector_), A TWO ELECTRON 
!INTEGRAL TABLE (twoInt_ ) AND A MAP FROM INTEGRALS TO CSF PAIRS (twoInt2CSFMap_)
!CREATES THE MATRIX ELEMENTS FOR THESE CI VECTORS
        USE ciVectorIO
        USE twoIntCSFMapIO
        USE oneIntIO
        USE twoIntIO
        USE class_inputString
        IMPLICIT none
        LOGICAL :: debug=.FALSE.
        INTEGER :: stat,stat1,stat2
        LOGICAL :: exists
        TYPE(ciVector_) :: ciVector
        TYPE(twoIntCSFMap_) :: twoIntCSFMap
        TYPE(twoInt_) :: twoInt
        TYPE(oneInt_) :: output
        TYPE(inputString) :: orbitalString
        CHARACTER*200 :: inputFile
        CHARACTER*200 :: ciVectorFile
        CHARACTER*200 :: twoIntCSFMapFile
        CHARACTER*200 :: twoIntFile
        CHARACTER*200 :: outputFile
        CHARACTER*200 :: activeOrbitals

        INTEGER, ALLOCATABLE :: ci1(:)
        INTEGER, ALLOCATABLE :: ci2(:)
        REAL*8, ALLOCATABLE :: operatorMatrix(:,:)        

        REAL*8 :: integral,ciCoef1,ciCoef2
        REAL*8 :: val
        INTEGER, ALLOCATABLE :: csf(:,:)
        INTEGER, ALLOCATABLE :: zahler(:),nenner(:)

        INTEGER :: nCSF,iCSF,jCSF        
        INTEGER :: nState,iState,jState
               
        INTEGER,ALLOCATABLE :: orbitals(:),ras3Orbitals(:)
        INTEGER :: nOrb,iOrb
        
        INTEGER :: occupied,local,ras3
        INTEGER :: nIntegral

        INTEGER :: i1,j1,k1,l1
        INTEGER :: i2,j2,k2,l2
        INTEGER :: tempI(1),tempJ(1),tempK(1),tempL(1)

        INTEGER :: augtype
        INTEGER :: nLastState
        INTEGER, ALLOCATABLE :: lastStates(:)
        INTEGER :: symFct
        INTEGER :: symmetryFactor
        NAMELIST /INPUT/        ciVectorFile,&
                                twoIntCSFMapFile,&
                                twoIntFile,&
                                outputFile,&
                                activeOrbitals

        ras3=2
        activeOrbitals="" 
        augType=0
        nLastState=0


        CALL GETARG(1,inputFile)
        IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
        INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
        IF (.NOT.exists) STOP "input file does not exist"
        OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
        IF (stat.ne.0) STOP "failure to open input file"
        READ(1,NML=INPUT,IOSTAT=stat)
        IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
        WRITE(UNIT=6,NML=INPUT)
        CLOSE(1)

        INQUIRE(FILE="aug.dat",EXIST=exists)
        IF (exists) THEN
                WRITE(6,'(A)') "aug.dat exists. Only compute matrix elements in compliance" 
                WRITE(6,'(A)') "with augmentation type and specified states"
                OPEN(1,FILE="aug.dat",STATUS="old")
                READ(1,*) nLastState,augType
                IF (augType.gt.0.AND.nLastState.gt.0) THEN
                        ALLOCATE(lastStates(nLastState))
                        READ(1,*) (lastStates(i1),i1=1,nLastState)
                ENDIF
        ELSE
                WRITE(6,'(A)') "aug.dat does not exists." 
                WRITE(6,'(A)') "Compute all matrix elements."
        ENDIF

        CALL ciVector%readCI(TRIM(ciVectorFile),"f")
        CALL ciVector%getNState(nState)
        CALL twoInt%readTwoInt(TRIM(twoIntFile),"b",2)
        CALL twoIntCSFMap%openTwoIntCSFMap(TRIM(twoIntCSFMapFile),1)
        CALL twoIntCSFMap%getOccupied(occupied)
        local=occupied-ras3

        IF (LEN(TRIM(activeOrbitals)).eq.0) THEN
                ALLOCATE(orbitals(occupied))
                DO iOrb=1,occupied
                        orbitals(iOrb)=iOrb
                ENDDO
                WRITE(6,'(A)') "No active ras3 orbitals were specified:"
                WRITE(6,'(A)') "assumed to be first two nonlocalized orbitals"
                WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
                WRITE(6,'(<occupied>(x,I4))') (orbitals(iOrb),iOrb=1,occupied)
        ELSE
                CALL orbitalString%setString(activeOrbitals)
                CALL orbitalString%process("-")
                CALL orbitalString%getArray(ras3Orbitals)
                CALL orbitalString%sizeString(nOrb)
                ALLOCATE(orbitals(local+nOrb))
                DO iOrb=1,local
                        orbitals(iOrb)=iOrb
                ENDDO
                DO iOrb=local+1,local+nOrb
                        orbitals(iOrb)=ras3Orbitals(iOrb-local)
                ENDDO
                WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
                WRITE(6,'(<local+nOrb>(x,I4))') (orbitals(iOrb),iOrb=1,local+nOrb)
        ENDIF

        WRITE(6,'(A,I1)') "Augmentation Type=",augType
        IF (augType.gt.0) THEN
                WRITE(6,'(A,<nLastState>(x,I4))') "Last states:",(lastStates(i1),i1=1,nLastState)
        ENDIF

        ALLOCATE(operatorMatrix(nState,nState))
        operatorMatrix=0.d0
        nIntegral=0
        DO  
                CALL twoInt%getRecord(i1,j1,k1,l1,integral,stat1)
                IF (stat1.ne.0) THEN
                        WRITE(6,'(A,I8,A)') "End of integral file reached after reading ",nIntegral," integrals"
                        EXIT
                ENDIF
                !WRITE(6,'(A,4(x,I4))'), "DEBUG integral indeces:",i1,j1,k1,l1
                nIntegral=nIntegral+1
                !WRITE(6,'(A,4(x,I4))') "DEBUG ",i1,j1,k1,l1
                IF (.NOT.(ANY(orbitals.eq.i1).AND.ANY(orbitals.eq.j1).AND.&
                          ANY(orbitals.eq.k1).AND.ANY(orbitals.eq.l1))) THEN
                                !DEBUG
                                !WRITE(6,'(A,4(x,I3))') "DEBUG skip",i1,j1
                                CYCLE
                ENDIF
                tempI=MINLOC(ABS(orbitals-i1))
                tempJ=MINLOC(ABS(orbitals-j1))
                tempK=MINLOC(ABS(orbitals-k1))
                tempL=MINLOC(ABS(orbitals-l1))
                i1=tempI(1)
                j1=tempJ(1)
                k1=tempK(1)
                l1=tempL(1)
                DO 
                        CALL twoIntCSFMap%getRecord(i2,j2,k2,l2,nCSF,csf,zahler,nenner,stat2)
                        IF (stat2.ne.0) THEN
                                WRITE(6,'(A,4(x,I4),A)') "ERROR: could not match integral:",i1,j1,k1,l1," to guga coefs."
                                STOP "ERROR: check output file for info"
                        ENDIF
                        !WRITE(6,'(A,4(x,I4))'), "DEBUG   csfMap indeces:",i2,j2,k2,l2
                        IF (i1.ne.i2.OR.j1.ne.j2.OR.k1.ne.k2.OR.l1.ne.l2) CYCLE
                        !symFct=symmetryFactor(i1,j1,k1,l1)
                        DO iState=1,nState
                                DO jState=iState,nState
                                        val=0.d0
                                        IF (augType.eq.1) THEN
                                                IF (.NOT.((ANY(lastStates.eq.iState)).OR.(ANY(lastStates.eq.jState)))) THEN
                                                        CYCLE
                                                ENDIF
                                        ELSEIF (augType.eq.2) THEN
                                                IF (.NOT.((ANY(lastStates.eq.iState)).AND.(ANY(lastStates.eq.jState)))) THEN
                                                        CYCLE
                                                ENDIF
                                        ENDIF
                                        DO iCSF=1,nCSF
                                                CALL ciVector%getCiVectorCoef(iState,csf(iCSF,1),ciCoef1)
                                                CALL ciVector%getCiVectorCoef(jState,csf(iCSF,2),ciCoef2)
                                                IF (debug) THEN
                                                        WRITE(6,'(A,4(x,I2),A,F10.5,x,A,2(x,I3),x,A,I4,A,I4,x,A,2(x,F10.5))') &
                                                                "I(",i2,j2,k2,l2,")=",integral,"added for csf1,2",csf(iCSF,1),csf(iCSF,2),&
                                                                "with det coef=",zahler(iCSF),"/",nenner(iCSF),"and ci coefs",ciCoef1,ciCoef2
                                                ENDIF
                                                val=val+integral*ciCoef1*ciCoef2*SQRT(ABS(REAL(zahler(iCSF)))/REAL(nenner(iCSF)))*zahler(iCSF)/ABS(zahler(iCSF))
                                        ENDDO
                                        operatorMatrix(iState,jState)=operatorMatrix(iState,jState)+val
                                        WRITE(6,'(A,4I5,F18.12,x,F18.12)') "DEBUG: i,j,k,l, symfct",i1,j1,k1,l1,val,integral
                                ENDDO
                        ENDDO
                        EXIT
                ENDDO
        ENDDO
        CALL output%initializeEmpty(nState)
        CALL output%setSym(operatorMatrix)
        CALL output%writeOneInt(TRIM(outputFile),"f")
        WRITE(6,'(A)') "Happy"
END PROGRAM 
FUNCTION symmetryFactor(i,j,k,l) RESULT (symFct)
        IMPLICIT none
        INTEGER,INTENT(in) :: i,j,k,l
        INTEGER :: symFct
        symFct=1
        IF ((i.eq.j).AND.(k.eq.l).AND.(i.ne.k)) THEN
                symFct=2
        ELSEIF ((i.ne.j).AND.(k.eq.l)) THEN
                symFct=4
        ELSEIF ((i.eq.j).AND.(k.ne.l)) THEN
                symFct=4
        ELSEIF ((i.ne.j).AND.(k.ne.l)) THEN
                symFct=4
                IF (.NOT.(((i.eq.k).AND.(j.eq.l)).OR.((i.eq.l).AND.(j.eq.k)))) THEN
                        symFct=8
                ENDIF
        ENDIF
END FUNCTION
