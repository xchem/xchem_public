!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM bin2fmt
    implicit none
    character*200 :: inFile
    character*200 :: outFile,fmt
    integer :: nBas,iBas,jBas
    integer :: nsym
    real*8, allocatable :: mat(:,:)

    call getarg(1,inFile)
    call getarg(2,outFile)

    open(1,file=inFile,status="old",form="unformatted")
    open(2,file=outFile,status="replace")

    read(1) nsym
    if (nsym.ne.1) then
        stop "bin2fmt currrently only works for one symmetry"
    endif
    read(1) nBas
    allocate(mat(nBas,nBas))
    do iBas=1,nBas
        read(1) (mat(jBas,iBas),jBas=1,nBas)
    enddo
    close(1)
    write(2,'(I0)') 1
    write(2,'(2(x,I0))') nBas,nBas
    write(fmt,'(A,I0,A)') "(",nBas,"(x,E20.12))"
    do iBas=1,nBas
        write(2,fmt) (mat(jBas,iBas),jBas=1,nBas)
    enddo
    close(2)
end program
