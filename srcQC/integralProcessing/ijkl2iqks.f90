!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

!This needs to get a lot more efficient still
!either find way to eliminate more orbitals
!or make it faster somehow in loop part
    USE omp_lib
    USE orbitalIO
    USE twoIntIO
    USE stopWatch
    USE searchInsert
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(orbital_) :: orb
    TYPE(twoInt_) :: twoInt
    
    INTEGER ::occupied,local,nBas
    INTEGER :: iBas,jBas,kBas,lBas
    INTEGER :: jOrb,lOrb
    INTEGER :: counter,nSkippedIntegrals
    INTEGER :: printFreq
    INTEGER :: pm(4,8),iPm,nPm

    INTEGER :: nrOfnodes
    INTEGER :: node
    INTEGER :: key
    LOGICAL :: keyIndex
    CHARACTER*3 :: keyChar
    CHARACTER*20 :: suff

    REAL*8 :: twoElBas,thrCf,thrCt
    REAL*8, ALLOCATABLE :: oM(:,:),iqks(:,:,:,:)
    REAL*8, ALLOCATABLE :: tempVec(:)
    INTEGER, ALLOCATABLE :: tempIntVec(:)

    LOGICAL :: finish
    INTEGER :: nCachedInt,nCachedRec,iC
    INTEGER, ALLOCATABLE :: indexCache(:,:)
    REAL*8, ALLOCATABLE :: integralCache(:)

    LOGICAL, ALLOCATABLE :: allRelZero(:)
    LOGICAL :: skip,areIntegrals
    INTEGER :: integralFmt
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: integralFile
    CHARACTER*200 :: outegralFile
    CHARACTER*200 :: token
    CHARACTER*200 :: outputFormat
    CHARACTER*200 :: suffixFile
    CHARACTER*200 :: fname

    INTEGER*4 :: nCPU

    NAMELIST /INPUT/        orbitalFile,&
      occupied,&
      integralFile,&
      outegralFile,&
      local,&
      outputFormat,&
      printFreq,&
      nCPU,&
      thrCf,&
      thrCt,&
      suffixFile
    
    outputFormat="b"
    local=0                                
    printFreq=1e6
    nCPU=0
    thrCf=1e-18
    thrCt=1e-40

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    WRITE(6,'(A)') "Look for integrals in one of the following three formats:"
    WRITE(6,'(3(A))') "    1) ",trim(integralFile),".int"
    WRITE(6,'(3(A))') "    2) ",trim(integralFile),".int.$suffix"
    INQUIRE(file=trim(integralFile),exist=exists)
    areIntegrals=.false.
    IF (exists) THEN
        areIntegrals=.true.
        integralFmt=1
    ENDIF
    IF (.not.areIntegrals) THEN
        OPEN(100,file=trim(suffixFile),status="old")
        READ(100,*) suff
        REWIND(100)
        INQUIRE(file=trim(integralFile)//'.'//trim(suff),exist=exists)
        IF (exists) THEN
            areIntegrals=.true.
            integralFmt=2
        ENDIF
    ENDIF
    IF (areIntegrals) THEN
        WRITE(6,'(A,I0)') "Integrals found in format:",integralFmt
    ELSE
        WRITE(6,'(A)') "ERROR: no integrals found"
        STOP "ERROR: no integrals found"
    ENDIF

    IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
    WRITE(6,'(A)') "Loading Orbitals"

    CALL orb%readOrbital(orbitalFile,"b") 
    CALL orb%getNOrb(nBas)
    WRITE(6,'(A,I0)') "Basis fcts in orbital file: ",nBas

    IF (integralFmt.eq.1) THEN
        fname=TRIM(integralFile)
    ELSEIF (integralFmt.eq.2) THEN
        fname=TRIM(integralFile)//'.'//trim(suff)
    ENDIF
    OPEN(1,file=TRIM(fname),FORM="unformatted",STATUS="old")
    keyChar=""
    READ(1,iostat=stat) nBas,keyChar
    IF (keyChar.eq."key") THEN
        keyIndex=.TRUE.
        WRITE(6,'(A)') "Reading integrals as key,value"
    ELSE
        keyIndex=.FALSE.
        REWIND(1)
        READ(1) nBas
        WRITE(6,'(A)') "Reading integrals as i,j,k,l,value"
    ENDIF
    CLOSE(1)

    WRITE(6,'(A)') "Allocating Memory"
    ALLOCATE(oM(nBas,occupied))
    ALLOCATE(iqks(nBas,local,nBas,local))
    iqks=0.d0
   
    WRITE(6,'(A)') "Preparing Orbitals"
    DO jOrb=1,occupied
        CALL orb%getOrbital(tempVec,jOrb)
        oM(:,jOrb)=tempVec
    ENDDO
    CALL orb%destroy()
    ALLOCATE(allRelZero(nBas))
    allRelZero=.FALSE.
   
    counter=0
    DO iBas=1,nBas
        IF (ALL(ABS(oM(iBas,:)).lt.thrCf)) THEN
            allRelZero(iBas)=.TRUE.
            counter=counter+1
        ENDIF
    ENDDO
    ALLOCATE(tempIntVec(counter))
    counter=0
    DO iBas=1,nBas
        IF (allRelZero(iBas)) THEN
            counter=counter+1
            tempIntVec(counter)=iBas
        ENDIF
    ENDDO

    WRITE(6,'(A)') "Basis coefficient that are zero for all orbitals"
    WRITE(6,'(25(I5,x))') (tempIntVec(iBas),iBas=1,counter) 
    DEALLOCATE(tempIntVec)

    WRITE(6,'(A)') "Allocating cache"
    ALLOCATE(indexCache(4,printFreq*8),integralCache(printFreq*8))

    IF (nCPU.eq.0) THEN
        nCPU=OMP_get_max_threads()  
        WRITE(6,'(A,I0)') "Set number of CPUs to maximum = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ELSE
        WRITE(6,'(A,I0)') "Use userdefined number of CPUs = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ENDIF
    counter=0
    nSkippedIntegrals=0
    nCachedInt=0
    nCachedRec=0
    finish=.FALSE.
    


    WRITE(6,'(A)') "Commence"
    CALL timer%initialize()
    DO  
        IF (integralFmt.eq.1) THEN
            fname=trim(integralFile)
        ELSE
            READ(100,*,iostat=stat) suff
            fname=trim(integralFile)//"."//trim(suff)
            IF (stat.ne.0) THEN
                WRITE(6,'(A)') "All files processed"
                EXIT
            ENDIF
        ENDIF
        WRITE(6,'(A,A)') "Process integrals from file: ",trim(fname)
        INQUIRE(FILE=trim(fname), EXIST=exists)
        IF (.not.exists) THEN
            STOP "ERROR: required integral file not found"
        ENDIF
        OPEN(1,file=trim(fname),status="old",form="unformatted")
        READ(1)
        DO
            DO
                IF (keyIndex) THEN
                    READ(1,IOSTAT=stat) key,twoElBas
                    CALL key2ind(key,nBas,iBas,jBas,kBas,lBas)
                ELSE
                    READ(1,IOSTAT=stat) iBas,jBas,kBas,lBas,twoElBas
                ENDIF
                IF (stat.ne.0) THEN
                    WRITE(6,'(A)') "End of file encountered"
                    finish=.TRUE.
                    EXIT
                ENDIF
                CALL indexSymmetry(iBas,jBas,kBas,lBas,nBas,allRelZero,npm,pm,skip)
                counter=counter+1
                IF (skip) THEN
                    nSkippedIntegrals=nSkippedIntegrals+1
                ELSE
                    DO ipm=1,npm
                        indexCache(:,nCachedInt+ipm)=pm(:,ipm)
                        integralCache(nCachedInt+ipm)=twoElBas
                    ENDDO
                    nCachedRec=nCachedRec+1
                    nCachedInt=nCachedInt+npm
                    IF (nCachedRec.eq.printFreq) EXIT
                ENDIF
            ENDDO
            !$OMP PARALLEL &
            !$OMP DEFAULT(none) &
            !$OMP PRIVATE(jOrb,lOrb,iC,skip) &
            !$OMP SHARED(local,oM,iqks,thrCf) &
            !$OMP SHARED(indexCache,integralCache,nCachedInt)
            !$OMP DO COLLAPSE(2) 
            DO jOrb=1,local
                DO lOrb=1,local
                    DO iC=1,nCachedInt
                        IF ((ABS(oM(indexCache(2,iC),jOrb)).gt.thrCf).AND.&
                          (ABS(oM(indexCache(4,iC),lOrb)).gt.thrCf)) THEN
                            iqks(indexCache(1,iC),jOrb,indexCache(3,iC),lOrb)=&
                              iqks(indexCache(1,iC),jOrb,indexCache(3,iC),lOrb)+&
                              oM(indexCache(2,iC),jOrb)*oM(indexCache(4,iC),lOrb)*&
                              integralCache(iC) 
                        ENDIF
                    ENDDO
                ENDDO
            ENDDO
            !$OMP END DO 
            !$OMP END PARALLEL
            nCachedInt=0
            nCachedRec=0
            WRITE(token,'(A,I14,A,I14,A)') "Basis integral record ",counter,&
              " processed (of which irrelvant since last print: ",nSkippedIntegrals,")"
            nSkippedIntegrals=0
            CALL timer%time(TRIM(token))
            IF (finish) THEN
                WRITE(6,'(A)') "All intergrals processed"
                EXIT
            ENDIF
        ENDDO
        CLOSE(1)
        finish=.FALSE.
        IF (integralFmt.eq.1) EXIT
    ENDDO

    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "Write contracted integrals to file"
    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,file=TRIM(outegralFile),STATUS="replace")
        WRITE(1,'(4(I0,x))' ) nBas,local,nBas,local
    ELSE 
        OPEN(1,file=TRIM(outegralFile),STATUS="replace",FORM="unformatted")
        WRITE(1) nBas,local,nBas,local
    ENDIF

    DO iBas=1,nBas
        DO jOrb=1,local
            DO kBas=1,nBas
                DO lOrb=1,local
                    IF (ABS(iqks(iBas,jOrb,kBas,lOrb)).gt.thrCt) THEN
                        IF (outputFormat(1:1).eq."f") THEN
                            WRITE(1,'(4(I0,x),E18.10)') iBas,jOrb,kBas,lOrb,iqks(iBas,jOrb,kBas,lOrb)
                        ELSE
                            WRITE(1) iBas,jOrb,kBas,lOrb,iqks(iBas,jOrb,kBas,lOrb)
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    WRITE(6,'(A)') "Happy"
END PROGRAM

SUBROUTINE indexSymmetry(iBas,jBas,kBas,lBas,nBas,allRelZero,npm,pm,skip)
    IMPLICIT none
    INTEGER, INTENT(in) :: iBas,jBas,kBas,lBas,nBas
    LOGICAL, INTENT(in) :: allRelZero(nBas)
    INTEGER, INTENT(inout) :: pm(4,8)
    INTEGER, INTENT(out) :: npm
    LOGICAL, INTENT(out) :: skip
    skip=.FALSE.
    IF ((iBas.eq.jBas).AND.(iBas.eq.kBas).AND.(iBas.eq.lBas)) THEN !iiii 
        pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
        nPm=1
        IF (allRelZero(iBas)) THEN
            skip=.TRUE.
        ENDIF
    ELSEIF ((iBas.eq.jBas).AND.(kBas.eq.lBas).AND.(iBas.ne.kBas)) THEN !iijj
        pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas !WAS MISSING
        pm(1,2)=kBas;pm(2,2)=lBas;pm(3,2)=iBas;pm(4,2)=jBas
        nPm=2
        IF (allRelZero(iBas).OR.allRelZero(kBas)) THEN
            skip=.TRUE.
        ENDIF
    ELSEIF ((iBas.ne.jBas).AND.(kbas.eq.lBas)) THEN !ijkk
        IF (allRelZero(kBas).OR.(allRelZero(iBas).AND.allRelZero(jBas))) THEN
            skip=.TRUE.
        ELSE
            pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
            pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
            pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
            pm(1,4)=kBas;pm(2,4)=lBas;pm(3,4)=jBas;pm(4,4)=iBas
            nPm=4
        ENDIF
    ELSEIF ((iBas.eq.jBas).AND.(kbas.ne.lBas)) THEN !iijk
        IF (allRelZero(iBas).OR.(allRelZero(kBas).AND.allRelZero(lBas))) THEN
            skip=.TRUE.
        ELSE
            pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
            pm(1,2)=iBas;pm(2,2)=jBas;pm(3,2)=lBas;pm(4,2)=kBas
            pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
            pm(1,4)=lBas;pm(2,4)=kBas;pm(3,4)=iBas;pm(4,4)=jBas
            nPm=4
        ENDIF
    ELSEIF ((iBas.ne.jBas).AND.(kBas.ne.lBas)) THEN !ijij
        nPm=4
        IF (.NOT.(((iBas.eq.kBas).AND.(jBas.eq.lBas)).OR.((iBas.eq.lBas).AND.(jBas.eq.kBas)))) THEN !ijkl
            nPm=8
        ENDIF
        IF (nPm.eq.4) THEN  
            IF (allRelZero(iBas).AND.allRelZero(jBas)) THEN
                skip=.TRUE.
            ELSE
                pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
                pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
            ENDIF
        ELSE
            IF ((allRelZero(iBas).AND.allRelZero(jBas)).OR.(allRelZero(kBas).AND.allRelZero(lBas))) THEN
                skip=.TRUE.
            ELSE
                pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
                pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
                pm(1,5)=kBas;pm(2,5)=lBas;pm(3,5)=iBas;pm(4,5)=jBas
                pm(1,6)=kBas;pm(2,6)=lBas;pm(3,6)=jBas;pm(4,6)=iBas
                pm(1,7)=lBas;pm(2,7)=kBas;pm(3,7)=iBas;pm(4,7)=jBas
                pm(1,8)=lBas;pm(2,8)=kBas;pm(3,8)=jBas;pm(4,8)=iBas
            ENDIF
        ENDIF
    ENDIF
END SUBROUTINE
