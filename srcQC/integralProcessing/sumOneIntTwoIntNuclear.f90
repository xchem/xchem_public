!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM sumOneIntTwoIntNuclear
!PROGRAM THAT TAKES ONEELECTRON HAMILTONIAN,
!TWO ELECTRON HAMILTONIAN AND NUCLEAR REPULSION
!AND ADD THEM UP TO GIVE THE TOTAL HAMILTONIAN
    USE oneIntIO
    USE diagonalizations
    IMPLICIT none 
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(oneInt_) :: oneEl
    TYPE(oneInt_) :: twoEl
    TYPE(oneInt_) :: totHam
    TYPE(oneInt_) :: ov
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: oneElFile
    CHARACTER*200 :: twoElFile
    CHARACTER*200 :: nucRepFile
    CHARACTER*200 :: totHamFile
    CHARACTER*200 :: ovFile,inputFormat,outputFormat
    INTEGER :: nState,iState,jState
    REAL*8, ALLOCATABLE :: oneElMat(:,:)
    REAL*8, ALLOCATABLE :: twoElMat(:,:)
    REAL*8, ALLOCATABLE :: totHamMat(:,:)
    REAL*8, ALLOCATABLE :: ovMat(:,:)
    REAL*8 :: nuclearRepulsion
    
    NAMELIST /INPUT/        oneElFile,&
      twoElFile,&
      nucRepFile,&
      totHamFile,&
      ovFile,&
      inputFormat,&
      outputFormat
    
    inputFormat="b"
    outputFormat="b"
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    OPEN(1,FILE=TRIM(nucRepFile),STATUS="old")
    READ(1,*) nuclearRepulsion
    CLOSE(1)
    
    WRITE(6,'(A,x,F18.12)') "nuclear Repulsion Energy Read=",nuclearRepulsion
    
    CALL oneEl%readOneInt(TRIM(oneElFile),inputFormat(1:1),.TRUE.,1,"o")
    CALL twoEl%readOneInt(TRIM(twoElFile),inputFormat(1:1),.TRUE.,1,"o")
    CALL ov%readOneInt(TRIM(ovFile),inputFormat(1:1),.TRUE.,1,"o")
    
    CALL oneEl%getSymSize(nState)
    
    CALL oneEl%getSym(oneElMat)
    CALL twoEl%getSym(twoElMat)
    CALL ov%getSym(ovMat)
    
    ALLOCATE(totHamMat(nState,nState))
    totHamMat=0.d0
    totHamMat=oneElMat
    
    DO iState=1,nState
        DO jState=1,nState
            totHamMat(iState,jState)=totHamMat(iState,jState)+nuclearRepulsion*ovMat(iState,jState)
        ENDDO
    ENDDO
    
    CALL totHam%initializeEmpty(nState)
    CALL totHam%setSym(totHamMat)
    CALL totHam%writeOneInt(TRIM(oneElFile),outputFormat(1:1),.TRUE.,1,"rch")
    
    CALL totHam%destroy()
    totHamMat=totHamMat+twoElMat
    
    CALL totHam%initializeEmpty(nState)
    CALL totHam%setSym(totHamMat)
    CALL totHam%writeOneInt(TRIM(totHamFile),outputFormat(1:1),.TRUE.,1,"rch")
   
    WRITE(6,'(A)') "Happy"
END PROGRAM
