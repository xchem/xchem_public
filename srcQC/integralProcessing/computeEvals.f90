!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM computeEVals
    USE oneIntIO
    USE diagonalizations
    IMPLICIT none
!STATUS VARS
    INTEGER :: stat
    LOGICAL :: exists
    CHARACTER*200 :: inputFile,inputFormat

!INPUT VARS
    CHARACTER*200 :: opFile
    CHARACTER*200 :: ovFile
    CHARACTER*200 :: eValFile
    CHARACTER*200 :: fmt
    LOGICAL :: readMany,printMat

!STRUCTURAL VARS
    INTEGER :: nState
    TYPE(oneInt_) :: op,ov
    REAL*8, ALLOCATABLE :: opMat(:,:)
    REAL*8, ALLOCATABLE :: ovMat(:,:)
    REAL*8, ALLOCATABLE :: eVals(:)
    REAL*8, ALLOCATABLE :: superOpMat(:,:)
    REAL*8, ALLOCATABLE :: superOvMat(:,:)
    REAL*8, ALLOCATABLE :: superOpMatT(:,:)
    REAL*8, ALLOCATABLE :: superOvMatT(:,:)

!TEMP VARS
    INTEGER :: iState,nBlock,iBlock,jBlock
    INTEGER :: nBas,iBas,jBas,nBasBlock
    INTEGER :: nBasBlockX,nBasBlockY
    INTEGER :: iBasStartX,iBasEndX
    INTEGER :: iBasStartY,iBasEndY
    INTEGER :: temp
    CHARACTER*200, ALLOCATABLE :: ovF(:),opF(:)

    NAMELIST /INPUT/ opFile,&
      ovFile,&
      eValFile,&
      inputFormat,&
      readMany,&
      printMat

    ovFile=""
    eValFile="evals"
    inputFormat="b"
    readMany=.FALSE.
    printMat=.FALSE.

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    IF (.not.readMany) THEN
        WRITE(6,'(A)') "Loading Data"
        CALL ov%readOneInt(TRIM(ovFile),inputFormat(1:1),.TRUE.,1,"o")
        CLOSE(1)
        CALL op%readOneInt(TRIM(opFile),inputFormat(1:1),.TRUE.,1,"o")
        CLOSE(1)
        
        CALL ov%getSymSize(nState)
        CALL ov%getSym(ovMat)
        CALL op%getSym(opMat)

        ALLOCATE(eVals(nState))
        eVals=0.d0

        WRITE(6,'(A)') "Diagonalizing"
        CALL eig(nState,opMat,ovMat,eVals)

        OPEN(100,file=trim(eValFile))
        DO iState=1,nState
            WRITE(100,'(I0,x,F20.12)') iState,eVals(iState)
        ENDDO
    ELSE
        OPEN(1,file=ovFile,status="old")
        OPEN(2,file=opFile,status="old")
        READ(1,*) nBlock
        READ(2,*) temp
        IF (temp.ne.nBlock) STOP "ERROR: unequal number of blocks"
        nBas=0
        ALLOCATE(ovF(nBlock))
        ALLOCATE(opF(nBlock))
        WRITE(6,'(A)') "Reading Diagonal Block Sizes"
        DO iBlock=1,nBlock 
            READ(1,*) (ovF(jBlock),jBlock=1,nBlock)
            READ(2,*) (opF(jBlock),jBlock=1,nBlock)
            OPEN(3,file=trim(ovF(iBlock)),status="old")
            OPEN(4,file=trim(opF(iBlock)),status="old")
            READ(3,*) nBasBlock
            READ(4,*) temp
            IF (nBasBlock.ne.temp) THEN 
                STOP "ERROR blocks with operator and OV mat of different size"
            ENDIF
            WRITE(6,'(A,I0,3(A))') "Block of size ",nBasBlock," for files ",trim(ovF(iBlock))," and ",trim(opF(iBlock))
            nBas=nBas+nBasBlock
            CLOSE(3)
            CLOSE(4)
        ENDDO
        WRITE(6,'(A,I0)') "Total size of matrix ",nBas
        REWIND(1)
        REWIND(2)
        ALLOCATE(superOvMat(nBas,nBas))
        ALLOCATE(superOpMat(nBas,nBas))
        ALLOCATE(superOvMatT(nBas,nBas))
        ALLOCATE(superOpMatT(nBas,nBas))
        superOvMat=0.d0
        superOpMat=0.d0
        superOvMatT=0.d0
        superOpMatT=0.d0
       
        iBasStartX=1
        iBasEndX=0
        READ(1,*) 
        READ(2,*)
        DO iBlock=1,nBlock
            iBasStartY=1
            iBasEndY=0
            READ(1,*) (ovF(jBlock),jBlock=1,nBlock)
            READ(2,*) (opF(jBlock),jBlock=1,nBlock)
            DO jBlock=1,nBlock
                WRITE(6,'(A,2(I0,x))') "Read data for matrices of block ",iBlock,jBlock

                OPEN(3,file=trim(ovF(jBlock)),status="old")
                READ(3,*) nBasBlockX,nBasBlockY
                IF (jBlock.eq.1) THEN
                    iBasEndX=iBasEndX+nBasBlockX
                ENDIF
                ALLOCATE(ovMat(nBasBlockY,nBasBlockX))
                ALLOCATE(opMat(nBasBlockY,nBasBlockX))
                ovMat=0.d0
                opMat=0.d0
                DO iBas=1,nBasBlockX 
                    READ(3,*) (ovMat(jBas,iBas),jBas=1,nBasBlockY)
                ENDDO
                CLOSE(3)

                OPEN(3,file=trim(opF(jBlock)),status="old")
                READ(3,*) nBasBlockX,nBasBlockY
                DO iBas=1,nBasBlockX 
                    READ(3,*) (opMat(jBas,iBas),jBas=1,nBasBlockY)
                ENDDO
                CLOSE(3)
    
                iBasEndY=iBasEndY+nBasBlockY
                WRITE(6,'(2(A,I0))') "Write blocks to supermatrix with indeces X ",iBasStartX," to ",iBasEndX
                WRITE(6,'(2(A,I0))') "                             and indeces Y ",iBasStartY," to ",iBasEndY
                superOvMat(iBasStartY:iBasEndY,iBasStartX:iBasEndX)=ovMat
                superOpMat(iBasStartY:iBasEndY,iBasStartX:iBasEndX)=opMat
                iBasStartY=iBasStartY+nBasBlockY

                DEALLOCATE(ovMat,opMat)
            ENDDO
            iBasStartX=iBasStartX+nBasBlockX
        ENDDO
        CLOSE(1)
        CLOSE(2)

        ALLOCATE(eVals(nBas))
        eVals=0.d0

        superOpMatT=superOpMat
        superOvMatT=superOvMat
        WRITE(6,'(A)') "Diagonalizing"
        CALL eig(nBas,superOpMatT,superOvMatT,eVals)

        OPEN(100,file=trim(eValFile))
        DO iBas=1,nBas
            WRITE(100,'(I0,x,F20.12)') iBas,eVals(iBas)
        ENDDO
        CLOSE(100)

        IF (printMat) THEN
            WRITE(fmt,'(A,I0,A)') "(",nBas,"(x,E18.12))"
            OPEN(100,file="opMat.built",status="replace")
            DO iBas=1,nBas
                WRITE(100,fmt) (superOpMat(jBas,iBas),jBas=1,nBas)
            ENDDO
            CLOSE(100)
            OPEN(100,file="ovMat.built",status="replace")
            DO iBas=1,nBas
                WRITE(100,fmt) (superOvMat(jBas,iBas),jBas=1,nBas)
            ENDDO
            CLOSE(100)
        ENDIF
    ENDIF
    WRITE(6,'(A)') "Happy"

END PROGRAM
