!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM normalizeCI
!PROGRAM THAT NORMALIZES CI VECTORS
!INPUT GIVEN THROUGH COMMAND LINE ARGUMENTS
!ARG1 input ci file (mandatory)
!ARG2 input ci file type b=binary, f=formatted (mandatory)
!ARG3 output file name. Optional, if not provided input file will be overwritten
    USE ciVectorIO
    IMPLICIT none
    CHARACTER*200 :: inCI
    CHARACTER*200 :: sorte
    CHARACTER*200 :: sorteOut
    CHARACTER*200 :: outCI
    TYPE(ciVector_) :: ci
    
    inCI=" "        
    outCI=" "        
    sorte=" "        
    
    CALL GETARG(1,inCI)
    CALL GETARG(2,sorte)
    CALL GETARG(3,outCI)
    CALL GETARG(4,sorteOut)
    
    IF (LEN(TRIM(inCI)).eq.0) STOP "ERROR: first commmand line arg must be input Ci vect file"
    IF (LEN(TRIM(sorte)).eq.0) STOP "ERROR: 2nd commmand line arg must specify format (b)inary"
    IF (LEN(TRIM(outCI)).eq.0) THEN
        PRINT*, "Normlalized CI will overwrite input file"
        outCI=inCI
    ENDIF
    IF (LEN(TRIM(sorteOut)).eq.0) THEN
        PRINT*, "Normlalized CI will overwrite input file"
        sorteOut=sorte
    ENDIF
    
    CALL ci%readCI(TRIM(inCI),sorte(1:1))
    CALL ci%normalize()
    CALL ci%writeCI(TRIM(outCI),sorteOut(1:1))
END PROGRAM
