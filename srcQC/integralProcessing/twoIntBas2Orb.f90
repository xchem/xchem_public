!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

!This needs to get a lot more efficient still
!either find way to eliminate more orbitals
!or make it faster somehow in loop part
    USE omp_lib
    USE twoIntIO
    USE orbitalIO
    USE stopWatch
    USE searchInsert
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(twoInt_) :: gBas
    TYPE(twoInt_) :: gOrb
    TYPE(orbital_) :: orb
    
    INTEGER ::occupied,local
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: twoElectronOperatorBasisFile
    CHARACTER*200 :: twoElectronOperatorOrbitalFile
    CHARACTER*200 :: token,fmt
    CHARACTER*200 :: outputFormat
    
    REAL*8 :: orb1,orb2,orb3,orb4
    REAL*8 :: twoElBas,twoElOrb,temp
    REAL*8, ALLOCATABLE :: oM(:,:)
    REAL*8, ALLOCATABLE :: tempVec(:)
    INTEGER :: iTemp
    INTEGER :: iBas,jBas,kBas,lBas
    INTEGER :: iOrb,jOrb,kOrb,lOrb
    INTEGER :: nBas
    INTEGER :: pm(4,8),iPm,nPm
    INTEGER :: nAugElectrons,counter,counter2
    INTEGER :: nInd,iInd,jInd,tInd,startInd,endInd
    INTEGER :: lLow,nmono
    INTEGER :: key
    INTEGER :: printFreq
    INTEGER, ALLOCATABLE :: presentOrb(:)
    INTEGER, ALLOCATABLE :: tempIntArr(:)
    INTEGER, ALLOCATABLE :: IDs(:)
    INTEGER, ALLOCATABLE :: idx(:,:)
    REAL*8, ALLOCATABLE :: integrals(:)
    
    LOGICAL, ALLOCATABLE :: noBasCoef(:)
    LOGICAL, ALLOCATABLE :: yesOrbCoef(:)
    
    !PARALLELIZATION VARIABLES        
    INTEGER :: nrOfNodes,node,tasksPerNode
    INTEGER*4 :: nCPU
    
    NAMELIST /INPUT/        orbitalFile,&
      occupied,&
      twoElectronOperatorBasisFile,&
      twoElectronOperatorOrbitalFile,&
      local,&
      nrOfNodes,&
      node,&
      outputFormat,&
      printFreq,&
      tasksPerNode
    
    outputFormat="b"
    nrOfNodes=0
    tasksPerNode=0
    node=0
    local=0                                
    printFreq=10000

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
   

    CALL orb%readOrbital(orbitalFile,"b") 
    OPEN(1,file=TRIM(twoElectronOperatorBasisFile),FORM="unformatted",STATUS="old")
    CALL orb%getNOrb(nBas)
    WRITE(6,'(A,I0)') "Basis fcts in orbital file: ",nBas
    READ(1) nBas
   

    ALLOCATE(oM(nBas,occupied))
    ALLOCATE(noBasCoef(nBas))
    ALLOCATE(yesOrbCoef(occupied))
    
    DO iOrb=1,occupied
        CALL orb%getOrbital(tempVec,iOrb)
        oM(:,iOrb)=tempVec
    ENDDO
    CALL orb%destroy()
    
    
    
    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of available threads:",nCPU        
    IF (occupied**2.le.nCPU) THEN
        CALL omp_set_num_threads(occupied**2)
    ELSE
        CALL omp_set_num_threads(nCPU)
    ENDIF
    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU        

    IF (tasksPerNode.eq.0) THEN
        tasksPerNode=16/nCPU
    ENDIF
    WRITE(6,'(A,I0)') "Tasks per node set to ",tasksPerNode
    nrOfNodes=tasksPerNode*nrOfNodes
    
    WRITE(6,'(A)') "identify orbital indeces with one electron only in augmented orbitals"
    tInd=0
    !$OMP PARALLEL &
    !$OMP DEFAULT (none) &
    !$OMP SHARED(occupied,tInd,local) &
    !$OMP PRIVATE(iOrb,jOrb,kOrb,lOrb,lLow,nmono)
    !$OMP DO REDUCTION(+:tInd)
    DO iOrb=1,occupied
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb)
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied
                    nmono=0
                    IF (iOrb.gt.local) nmono=nmono+1
                    IF (jOrb.gt.local) nmono=nmono+1
                    IF (kOrb.gt.local) nmono=nmono+1
                    IF (lOrb.gt.local) nmono=nmono+1
                    IF (nmono.lt.3)  tInd=tInd+1
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    !$OMP ENDDO
    !$OMP END PARALLEL
    WRITE(6,'(I10,A,I18,A)') tInd," of ",occupied**4," indeces must be calculated"
    IF (node.gt.nrOfNodes) THEN
        STOP "ERROR: node cannot be geater than number of nodes"
    ELSEIF (nrOfNodes.gt.0.AND.node.gt.0) THEN
        nInd=CEILING(REAL(tInd)/REAL(nrOfNodes))   
        startInd=nInd*(node-1)+1
        IF (node.lt.nrOfNodes) THEN
            endInd=nInd*node
        ELSE
            endInd=tInd
            nInd=endInd-startInd+1
        ENDIF                
        WRITE(6,'(A,I5,A,I10,A)') "node ",node," of ",nrOfNodes," nodes "
        WRITE(6,'(A,I10,A,I10,A)') "Working of indeces ",startInd," to ",endInd," in this node"
    ELSE
        WRITE(6,'(A)') "no distribution to different nodes."
        nInd=tInd
        startInd=1
        endInd=nInd
    ENDIF
    
    ALLOCATE(idx(4,nInd))
    ALLOCATE(integrals(nInd))
    integrals=0.d0
    
    iInd=0
    jInd=0
    DO iOrb=1,occupied
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb)
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied
                    nmono=0
                    IF (iOrb.gt.local) nmono=nmono+1
                    IF (jOrb.gt.local) nmono=nmono+1
                    IF (kOrb.gt.local) nmono=nmono+1
                    IF (lOrb.gt.local) nmono=nmono+1
                    IF (nmono.ge.3) CYCLE
                    iInd=iInd+1
                    IF (iInd.ge.startInd.AND.iInd.le.endInd) THEN
                        jInd=jInd+1     
                        idx(1,jInd)=iOrb
                        idx(2,jInd)=jOrb
                        idx(3,jInd)=kOrb
                        idx(4,jInd)=lOrb
                    ENDIF
                    IF (iInd.gt.endInd) EXIT
                ENDDO
                IF (iInd.gt.endInd) EXIT
            ENDDO
            IF (iInd.gt.endInd) EXIT
        ENDDO
        IF (iInd.gt.endInd) EXIT
    ENDDO

    jOrb=0
    DO iOrb=1,occupied
        IF (ANY(idx.eq.iOrb)) THEN
            jOrb=jOrb+1
        ENDIF
    ENDDO
    ALLOCATE(presentOrb(jOrb))
    presentOrb=0
    jOrb=0
    DO iOrb=1,occupied
        IF (ANY(idx.eq.iOrb)) THEN
            jOrb=jOrb+1
            presentOrb(jOrb)=iOrb
        ENDIF
    ENDDO
    noBasCoef=.TRUE.
    counter=0
    DO iBas=1,nBas
        DO iOrb=1,jOrb
            IF (ABS(oM(iBas,presentOrb(iOrb))).ge.1e-14) THEN
                noBasCoef(iBas)=.FALSE.
                counter=counter+1
                EXIT
            ENDIF
        ENDDO
        !IF (ANY(ABS(oM(iBas,:)).ge.1e-14)) THEN
        !    noBasCoef(iBas)=.FALSE.
        !    counter=counter+1
        !ENDIF
    ENDDO
    
    WRITE(6,'(I8,A)') nBas," basis functions in compressed integral file"
    WRITE(6,'(I5,A,I5,A)') nBas-counter," of ",nBas," orbital coefficients are zero for all orbitals. These are:"
   
    ALLOCATE(tempIntArr(nBas-counter))
    counter=0
    DO iBas=1,nBas
        IF (noBasCoef(iBas)) THEN
            counter=counter+1
            tempIntArr(counter)=iBas
        ENDIF
    ENDDO
    WRITE(fmt,'(A,I0,A)') "(",counter,"(x,I0))"
    WRITE(6,TRIM(fmt)) (tempIntArr(iBas),iBas=1,counter)
    DEALLOCATE(tempIntArr)
   
    counter=0
    counter2=0
    CALL timer%initialize()
    CALL timer%time("test stop watch dead time")
    DO
        READ(1,IOSTAT=stat) iBas,jBas,kBas,lBas,twoElBas
        !READ(1,IOSTAT=stat) key,twoElBas
        !CALL key2ind(key,nBas,iBas,jBas,kBas,lBas)

        IF (stat.ne.0) EXIT

        counter=counter+1

        IF (noBasCoef(iBas).OR.noBasCoef(jBas).OR.noBasCoef(kBas).OR.noBasCoef(lBas)) THEN
            counter2=counter2+1
        ELSE
            pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
            nPm=1
            IF ((iBas.eq.jBas).AND.(kBas.eq.lBas).AND.(iBas.ne.kBas)) THEN
                pm(1,2)=kBas;pm(2,2)=lBas;pm(3,2)=iBas;pm(4,2)=jBas
                nPm=2
            ELSEIF ((iBas.ne.jBas).AND.(kbas.eq.lBas)) THEN
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
                pm(1,4)=kBas;pm(2,4)=lBas;pm(3,4)=jBas;pm(4,4)=iBas
                nPm=4
            ELSEIF ((iBas.eq.jBas).AND.(kbas.ne.lBas)) THEN
                pm(1,2)=iBas;pm(2,2)=jBas;pm(3,2)=lBas;pm(4,2)=kBas
                pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
                pm(1,4)=lBas;pm(2,4)=kBas;pm(3,4)=iBas;pm(4,4)=jBas
                nPm=4
            ELSEIF ((iBas.ne.jBas).AND.(kBas.ne.lBas)) THEN
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
                pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
                nPm=4
                IF (.NOT.(((iBas.eq.kBas).AND.(jBas.eq.lBas)).OR.((iBas.eq.lBas).AND.(jBas.eq.kBas)))) THEN
                    pm(1,5)=kBas;pm(2,5)=lBas;pm(3,5)=iBas;pm(4,5)=jBas
                    pm(1,6)=kBas;pm(2,6)=lBas;pm(3,6)=jBas;pm(4,6)=iBas
                    pm(1,7)=lBas;pm(2,7)=kBas;pm(3,7)=iBas;pm(4,7)=jBas
                    pm(1,8)=lBas;pm(2,8)=kBas;pm(3,8)=jBas;pm(4,8)=iBas
                    nPm=8
                ENDIF
            ENDIF

            yesOrbCoef=.TRUE.
            !$OMP PARALLEL &
            !$OMP DEFAULT(none) &
            !$OMP PRIVATE(iOrb) &
            !$OMP SHARED(oM,yesOrbCoef,iBas,jBas,kBas,lBas,occupied)
            !$OMP DO SCHEDULE(static)
            DO iOrb=1,occupied
                IF (ABS(oM(iBas,iOrb)).lt.1e-14) THEN
                    IF (ABS(oM(jBas,iOrb)).lt.1e-14) THEN
                        IF (ABS(oM(kBas,iOrb)).lt.1e-14) THEN
                            IF (ABS(oM(lBas,iOrb)).lt.1e-14) THEN
                                yesOrbCoef(iOrb)=.FALSE.
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDDO
            !$OMP END DO
            !$OMP END PARALLEL

            !$OMP PARALLEL &
            !$OMP DEFAULT (none) &
            !$OMP SHARED (twoElBas,nInd,nPm,pm,oM,integrals,idx,yesOrbCoef) &
            !$OMP PRIVATE (iInd,twoElOrb,iPm,temp,iOrb)
            !$OMP DO SCHEDULE(static)
            DO iInd=1,nInd  !nInd very large        
                iOrb=0
                IF (yesOrbCoef(idx(1,iInd)).AND.yesOrbCoef(idx(2,iInd)).AND.yesOrbCoef(idx(3,iInd)).AND.yesOrbCoef(idx(4,iInd))) THEN
                    temp=0.d0
                    DO iPm=1,nPm !nPm at most 8
                        IF (ABS(oM(pm(1,iPm),idx(1,iInd))).ge.1e-14) THEN
                            IF (ABS(oM(pm(2,iPm),idx(2,iInd))).ge.1e-14) THEN
                                IF (ABS(oM(pm(3,iPm),idx(3,iInd))).ge.1e-14) THEN
                                    IF (ABS(oM(pm(4,iPm),idx(4,iInd))).ge.1e-14) THEN
                                        temp=temp+&
                                          oM(pm(1,iPm),idx(1,iInd))*oM(pm(2,iPm),idx(2,iInd))*&
                                          oM(pm(3,iPm),idx(3,iInd))*oM(pm(4,iPm),idx(4,iInd))
                                    ENDIF
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDDO
                    IF (ABS(temp).ge.1e-14) THEN
                        twoElOrb=temp*twoElBas
                        integrals(iInd)=integrals(iInd)+twoElOrb
                    ENDIF
                ENDIF
            ENDDO
            !$OMP END DO
            !$OMP END PARALLEL
        ENDIF
        
        IF (mod(counter,printFreq).eq.0) THEN
            WRITE(token,'(A,I12,A,I12,A)') "Basis integral record ",counter," processed (of which irrelvant: ",counter2,")"
            counter2=0
            CALL timer%time(TRIM(token))
        ENDIF
    ENDDO        
    CALL timer%time("Orbitals done")        
    
    CALL gOrb%initializeEmpty(occupied,nInd)
    DO iInd=1,nInd
        CALL gOrb%setIndex(iInd,idx(1,iInd),idx(2,iInd),idx(3,iInd),idx(4,iInd))
        CALL gOrb%setVal(iInd,integrals(iInd))
    ENDDO
    IF (outputFormat(1:1).eq."b") THEN
        CALL gOrb%writeTwoInt(TRIM(twoElectronOperatorOrbitalFile),"b")
    ELSE
        CALL gOrb%writeTwoInt(TRIM(twoElectronOperatorOrbitalFile),"f")
    ENDIF
    
    WRITE(6,'(A)') "Happy"
END PROGRAM
