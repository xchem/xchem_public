!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM trdBuild
!Code that read a set of i-j.trd2 files for combination of states
!and assambles these into the complete two electron reduced density 
!matrix. Furthermore the one electron density matrix is produced from
!the assambled two electron density matrix
    USE sortingAlgorithms
    USE searchInsert
    USE omp_lib
    USE stopWatch
    IMPLICIT none
!CONTROL VARIABLES
    INTEGER :: stat
    INTEGER :: counter
    LOGICAL :: exists
    CHARACTER*200 :: inputFile
    TYPE(stopWatch_) :: timer
!INPUT VARIABLES
    CHARACTER*200 :: trd2ElementFileList
    CHARACTER*200 :: trd2File
    !CHARACTER*100 :: trd1File
    CHARACTER*100 :: outputFormat
!STRUCTURE VARIABLES
    REAL*8, ALLOCATABLE :: rdm2(:)
    REAL*8, ALLOCATABLE :: rdm2List(:)
    INTEGER*8, ALLOCATABLE :: rdm2Order(:)
    INTEGER*8, ALLOCATABLE :: rdm2Keys(:)
    !REAL*8, ALLOCATABLE :: rdm1(:,:,:)
    CHARACTER*200 :: trd2ElementFile
    INTEGER :: nState,un
    INTEGER :: nOrb,nVal
!TEMP VARS
    INTEGER :: prog,tProg
    INTEGER :: nElement
    INTEGER :: iElement
    INTEGER*4 :: nCPU
    INTEGER :: iState,jState,state1,state2
    INTEGER :: iOrb,jOrb,kOrb,lOrb,ind,key
    INTEGER :: lowOrb
    INTEGER :: nPm,iPm,pm(8,4)
    INTEGER, ALLOCATABLE :: statePairs(:,:)
    REAL*8,ALLOCATABLE :: temp(:)
    REAL*8 :: rdmVal

    NAMELIST /INPUT/ trd2ElementFileList,&
      trd2File,&
      outputFormat
      !trd1File,&

    trd2ElementFileList=""
    outputFormat="b"
    trd2File="trd2"
    !trd1File="trd1"
    

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)      

    WRITE(6,'(A)') "Assembling trd2 from element files"
    nElement=0
    WRITE(trd2ElementFile,'(I3.3,A,I3.3,A)') 1,"-",1,".trd2"
    OPEN(2,FILE=TRIM(trd2ElementFile),STATUS="old",FORM="unformatted")
    READ(2,IOSTAT=stat) nOrb,nState,state1,state2,nVal
    CLOSE(2)

    nCPU=OMP_get_max_threads()
    IF (nState.ge.nCPU) THEN  
        CALL omp_set_num_threads(nCPU)
    ELSE
        nCPU=1
        CALL omp_set_num_threads(nCPU)
    ENDIF
    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU

    IF (outputFormat(1:1).eq."b") THEN
        OPEN(4,file=TRIM(trd2file),STATUS="replace",FORM="unformatted")
        WRITE(4) nOrb,nState 
    ELSE
        OPEN(4,file=TRIM(trd2file),STATUS="replace")
        WRITE(4,'(I0,x,I0)') nOrb,nState 
    ENDIF
    WRITE(6,'(A,I0,A,I0)') "number of State= ",nState," ; number of orbitals=",nOrb
  
    CALL timer%initialize()
    WRITE(6,'(A)') "Counting non zero elements"
    DO iState=1,nState
        DO jState=1,iState
            WRITE(trd2ElementFile,'(I3.3,A,I3.3,A)') iState,"-",jState,".trd2"
            OPEN(1,file=TRIM(trd2ElementFile),STATUS="old",FORM="unformatted")
            READ(1,IOSTAT=stat) nOrb,nState,state1,state2,nVal
            IF (state1.ne.iState.or.state2.ne.jState) THEN
                STOP "ERROR: inconsitent rdm files"    
            ENDIF
            nElement=nElement+nVal
            CLOSE(1)
        ENDDO
    ENDDO
    CALL timer%time("done")
    WRITE(6,'(A,I0)') " ->Number of nonzero values=",nElement
    ALLOCATE(rdm2Order(nElement))
    ALLOCATE(rdm2List(nElement))
    ALLOCATE(rdm2Keys(nElement))
    ALLOCATE(rdm2(nState*(nState+1)/2))
    ALLOCATE(temp(nElement))
    counter=0
    WRITE(6,'(A)') "Load nonzero Elements"
    DO iState=1,nState
        DO jState=1,iState
            WRITE(trd2ElementFile,'(I3.3,A,I3.3,A)') iState,"-",jState,".trd2"
            OPEN(1,file=TRIM(trd2ElementFile),STATUS="old",FORM="unformatted")
            READ(1,IOSTAT=stat) nOrb,nState,state1,state2,nVal
            DO iElement=1,nVal
                counter=counter+1
                READ(1) iOrb,jOrb,kOrb,lOrb,rdmVal
                rdm2Keys(counter)=iState*(nState+1)*(nOrb+1)**4+jState*(nOrb+1)**4+(nOrb+1)**3*iOrb+(nOrb+1)**2*jOrb+&
                    kOrb*(nOrb+1)+lOrb
                rdm2List(counter)=rdmVal
                rdm2Order(counter)=counter
            ENDDO
            CLOSE(1)
        ENDDO
    ENDDO
    CALL timer%time("done")
    WRITE(6,'(A)') "Sorting keys"
    CALL QSortInt(nElement,rdm2Keys,rdm2Order)
    CALL timer%time("done")
    temp=rdm2List
    WRITE(6,'(A)') "ordering values for fast access"
    DO iElement=1,nElement
        rdm2List(iElement)=temp(rdm2Order(iElement))
    ENDDO
    DEALLOCATE(rdm2Order,temp)
    CALL timer%time("done")

    counter=0
    tprog=0
    WRITE(6,'(A)') "Iterate over space of possible values and write to file"
    DO iOrb=1,nOrb
        DO jOrb=iOrb,nOrb
            DO kOrb=iOrb,nOrb
                IF (iOrb.eq.kOrb) THEN
                    lowOrb=MAX(jOrb,kOrb)
                ELSE
                    lowOrb=kOrb
                ENDIF
                DO lOrb=lowOrb,nOrb
                    pm(1,1)=iOrb;pm(1,2)=jOrb;pm(1,3)=kOrb;pm(1,4)=lOrb
                    nPm=1
                    IF ((iOrb.eq.jOrb).AND.(kOrb.eq.lOrb).AND.(iOrb.ne.kOrb)) THEN
                        pm(2,1)=kOrb;pm(2,2)=lOrb;pm(2,3)=iOrb;pm(2,4)=jOrb
                        nPm=2
                    ELSEIF ((iOrb.ne.jOrb).AND.(kOrb.eq.lOrb)) THEN
                        pm(2,1)=jOrb;pm(2,2)=iOrb;pm(2,3)=kOrb;pm(2,4)=lOrb
                        pm(3,1)=kOrb;pm(3,2)=lOrb;pm(3,3)=iOrb;pm(3,4)=jOrb
                        pm(4,1)=kOrb;pm(4,2)=lOrb;pm(4,3)=jOrb;pm(4,4)=iOrb
                        nPm=4
                    ELSEIF ((iOrb.eq.jOrb).AND.(kOrb.ne.lOrb)) THEN
                        pm(2,1)=iOrb;pm(2,2)=jOrb;pm(2,3)=lOrb;pm(2,4)=kOrb
                        pm(3,1)=kOrb;pm(3,2)=lOrb;pm(3,3)=iOrb;pm(3,4)=jOrb
                        pm(4,1)=lOrb;pm(4,2)=kOrb;pm(4,3)=iOrb;pm(4,4)=jOrb
                        nPm=4
                    ELSEIF ((iOrb.ne.jOrb).AND.(kOrb.ne.lOrb)) THEN
                        pm(2,1)=jOrb;pm(2,2)=iOrb;pm(2,3)=kOrb;pm(2,4)=lOrb
                        pm(3,1)=iOrb;pm(3,2)=jOrb;pm(3,3)=lOrb;pm(3,4)=kOrb
                        pm(4,1)=jOrb;pm(4,2)=iOrb;pm(4,3)=lOrb;pm(4,4)=kOrb
                        nPm=4
                        IF (.NOT.(((iOrb.eq.kOrb).AND.(jOrb.eq.lOrb)).OR.((iOrb.eq.lOrb).AND.(jOrb.eq.kOrb)))) THEN
                            pm(5,1)=kOrb;pm(5,2)=lOrb;pm(5,3)=iOrb;pm(5,4)=jOrb
                            pm(6,1)=kOrb;pm(6,2)=lOrb;pm(6,3)=jOrb;pm(6,4)=iOrb
                            pm(7,1)=lOrb;pm(7,2)=kOrb;pm(7,3)=iOrb;pm(7,4)=jOrb
                            pm(8,1)=lOrb;pm(8,2)=kOrb;pm(8,3)=jOrb;pm(8,4)=iOrb
                            nPm=8
                        ENDIF
                    ENDIF
                    DO iPm=1,nPm
                        rdm2=0.0
                        !$OMP PARALLEL &
                        !$OMP DEFAULT (none) &
                        !$OMP SHARED (nState,pm,nOrb,iPm,nElement,rdm2Keys,rdm2,rdm2List) &
                        !$OMP PRIVATE (iState,jState,key,ind) 
                        !$OMP DO 
                        DO iState=nState,1,-1
                            DO jState=1,iState
                                key=iState*(nState+1)*(nOrb+1)**4+jState*(nOrb+1)**4+(nOrb+1)**3*pm(iPm,1)+(nOrb+1)**2*pm(iPm,2)+&
                                    (nOrb+1)*pm(iPm,3)+pm(iPm,4)
                                CALL binarySearch(key,nElement,rdm2Keys,ind) 
                                IF (ind.lt.0) THEN
                                    rdm2(iState*(iState-1)/2+jState)=rdm2List(ABS(ind))
                                ENDIF
                            ENDDO
                        ENDDO
                        !$OMP ENDDO
                        !$OMP END PARALLEL

                        IF (ANY(ABS(rdm2).gt.1e-14)) THEN    
                            IF (outputFormat(1:1).eq."b") THEN
                                WRITE(4) pm(iPm,1),pm(iPm,2),pm(iPm,3),pm(iPm,4),.FALSE.
                                WRITE(4) (rdm2(iState),iState=1,nState*(nState+1)/2)
                            ELSE
                                WRITE(4,'(4(x,I0),x,L)') pm(iPm,1),pm(iPm,2),pm(iPm,3),pm(iPm,4),.FALSE.
                                WRITE(4,*) (rdm2(iState),iState=1,nState*(nState+1)/2)
                            ENDIF
                        ELSE
                            IF (outputFormat(1:1).eq."b") THEN
                                WRITE(4) pm(iPm,1),pm(iPm,2),pm(iPm,3),pm(iPm,4),.TRUE.
                            ELSE
                                WRITE(4,'(4(x,I0),x,L)') pm(iPm,1),pm(iPm,2),pm(iPm,3),pm(iPm,4),.TRUE.
                            ENDIF
                        ENDIF
                    ENDDO
                ENDDO
            ENDDO
            counter=counter+1
            prog=INT(REAL(counter)/REAL(nOrb*(nOrb+1)/2)*100)
            IF (prog.ne.tprog) THEN
                WRITE(6,'(A,I0,A)') "  -progress: ",prog,"%"
                tprog=prog
            ENDIF
        ENDDO
    ENDDO
    CALL timer%time("done")
    WRITE(6,'(A)') "Happy"
END PROGRAM
