!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

    USE omp_lib
    USE orbitalIO
    USE twoIntIO
    USE stopWatch
    USE searchInsert
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(orbital_) :: orb
    TYPE(twoInt_) :: twoInt
    
    INTEGER ::occupied,local,nBas
    INTEGER :: iBas,jBas,kBas,lBas
    INTEGER :: iOrb,jOrb,kOrb,lOrb
    INTEGER :: counter,nSkippedIntegrals
    INTEGER :: printFreq
    INTEGER :: pm(4,8),iPm,nPm

    INTEGER :: nrOfnodes
    INTEGER :: node
    INTEGER :: key
    LOGICAL :: keyIndex
    CHARACTER*3 :: keyChar
    CHARACTER*20 :: suff

    REAL*8 :: twoElBas,thrCf,thrCt
    REAL*8, ALLOCATABLE :: oM(:,:)
    real*8, allocatable :: pqrs1(:,:,:,:),pqrs2(:,:,:,:)
    REAL*8, ALLOCATABLE :: tempVec(:)
    INTEGER, ALLOCATABLE :: tempIntVec(:)

    LOGICAL :: finish
    INTEGER :: nCachedInt,nCachedRec,iC
    INTEGER, ALLOCATABLE :: indexCache(:,:),outindex(:)
    REAL*8, ALLOCATABLE :: integralCache(:)

    LOGICAL, ALLOCATABLE :: allRelZero(:)
    LOGICAL :: skip,areIntegrals
    INTEGER :: integralFmt
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: integralFile
    CHARACTER*200 :: basisFile
    CHARACTER*200 :: outegralFile
    CHARACTER*200 :: token
    CHARACTER*200 :: outputFormat
    CHARACTER*200 :: suffixFile
    CHARACTER*200 :: fname

    INTEGER*4 :: nCPU
!! JGV
integer, allocatable :: basdict(:)
integer oldnbas,lLow
real*8, allocatable :: tmp(:,:,:,:)
logical ex,rmint,readprev

    NAMELIST /INPUT/        orbitalFile,&
      occupied,&
      integralFile,&
      basisFile, &
      outegralFile,&
      local,&
      outputFormat,&
      printFreq,&
      nCPU,&
      thrCf,&
      thrCt,&
      rmint,&
      readprev
    
    outputFormat="b"
    local=0                                
    printFreq=10e6
    nCPU=0
    thrCf=1e-18
    thrCt=1e-10
    rmint=.true.
    readprev=.true.

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
   

 
    WRITE(6,'(A)') "Look for basis file ",trim(basisFile),".dat.$suffix"
    WRITE(6,'(A)') "Look for integrals ",trim(integralFile)
    INQUIRE(file=trim(integralFile),exist=exists)
    areIntegrals=.false.
    IF (exists) THEN
        areIntegrals=.true.
    ENDIF

    IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
    WRITE(6,'(A)') "Loading Orbitals"

    CALL orb%readOrbital(orbitalFile,"b") 
    CALL orb%getNOrb(nBas)
    WRITE(6,'(A,I0)') "Basis fcts in orbital file: ",nBas

    fname=trim(integralFile)
    OPEN(1,file=TRIM(fname),FORM="unformatted",STATUS="old")
    keyChar=""
    READ(1,iostat=stat) nBas,keyChar
    IF (keyChar.eq."key") THEN
        keyIndex=.TRUE.
        WRITE(6,'(A)') "Reading integrals as key,value"
    ELSE
        keyIndex=.FALSE.
        REWIND(1)
        READ(1) nBas
        WRITE(6,'(A)') "Reading integrals as i,j,k,l,value"
    ENDIF
    CLOSE(1)

    WRITE(6,'(A)') "Allocating Memory"
    ALLOCATE(oM(nBas,occupied))
   
    WRITE(6,'(A)') "Preparing Orbitals"
    DO jOrb=1,occupied
        CALL orb%getOrbital(tempVec,jOrb)
        oM(:,jOrb)=tempVec
    ENDDO
    CALL orb%destroy()

    
!!! JGV
write(6,*) "Reading the basis file ",trim(basisfile)
open(1,file=basisfile,status="old",iostat=stat)
if (stat.ne.0) STOP "File with the reduced number of basis set is not present"
oldnbas=nbas
allocate(basdict(oldnbas))
basdict=0
read(1,*) nbas
jbas=0
do ibas=1,nbas
 jbas=jbas+1
 read(1,*) lbas
 basdict(lbas)=jbas
enddo
write(6,*) "Reduced number of basis ",nbas
!write(6,*) basdict(:)
allocate(tmp(oldnbas,occupied,1,1))
tmp(:,:,1,1)=oM(:,:)
deallocate(oM)
allocate(oM(nbas,occupied))
do ibas=1,nbas
 do jbas=1,oldnbas
  if (basdict(jbas).eq.ibas) then
!   write(6,*) "Putting in the place ",ibas," the basis ",jbas
!$OMP PARALLEL DO
   do iOrb=1,occupied
    oM(ibas,iOrb)=tmp(jbas,iOrb,1,1)
   enddo
!$OMP END PARALLEL DO
  endif
 enddo
enddo
deallocate(tmp)
write(6,*) "Reduced orbitals prepared "
   
    ALLOCATE(allRelZero(nBas))
    allRelZero=.FALSE.
    counter=0
    DO iBas=1,nBas
        IF (ALL(ABS(oM(iBas,:)).lt.thrCf)) THEN
            allRelZero(iBas)=.TRUE.
            counter=counter+1
        ENDIF
    ENDDO
    ALLOCATE(tempIntVec(counter))
    counter=0
    DO iBas=1,nBas
        IF (allRelZero(iBas)) THEN
            counter=counter+1
            tempIntVec(counter)=iBas
        ENDIF
    ENDDO

    WRITE(6,'(A,I0)') "Basis coefficient that are zero for all orbitals ",counter
    WRITE(6,'(25(I5,x))') (tempIntVec(iBas),iBas=1,counter) 
    DEALLOCATE(tempIntVec)

    WRITE(6,'(A)') "Allocating cache"
    ALLOCATE(indexCache(4,printFreq*8),integralCache(printFreq*8))

    IF (nCPU.eq.0) THEN
        nCPU=OMP_get_max_threads()  
        WRITE(6,'(A,I0)') "Set number of CPUs to maximum = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ELSE
        WRITE(6,'(A,I0)') "Use userdefined number of CPUs = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ENDIF
    counter=0
    nSkippedIntegrals=0
    nCachedInt=0
    nCachedRec=0
    finish=.FALSE.

allocate(tmp(nbas,nbas,nbas,local))
tmp=0.d0

    WRITE(6,'(A)') "Commence"
    CALL timer%initialize()
        fname=trim(integralFile)
        WRITE(6,'(A,A)') "Process integrals from file: ",trim(fname)
        INQUIRE(FILE=trim(fname), EXIST=exists)
        IF (.not.exists) THEN
            STOP "ERROR: required integral file not found"
        ENDIF
        OPEN(1,file=trim(fname),status="old",form="unformatted")
        READ(1)
        DO
            DO
                IF (keyIndex) THEN
                    READ(1,IOSTAT=stat) key,twoElBas
                    CALL key2ind(key,oldnBas,iBas,jBas,kBas,lBas)
                ELSE
                    READ(1,IOSTAT=stat) iBas,jBas,kBas,lBas,twoElBas
                ENDIF
                IF (stat.ne.0) THEN
                    WRITE(6,'(A)') "End of file encountered"
                    finish=.TRUE.
                    EXIT
                ENDIF
!!! Transforming basis to the reduced set
                iOrb=basdict(iBas)
                jOrb=basdict(jBas)
                kOrb=basdict(kBas)
                lOrb=basdict(lBas)
                if (iOrb.eq.0 .or. jOrb.eq.0 .or. kOrb.eq.0 .or. lOrb.eq.0) then
                 write(6,*) "Basis in the file ",iBas,jBas,kBas,lBas
                 write(6,*) "Basis transformation ",iOrb,jOrb,lOrb,kOrb
                 write(6,*) basdict(:)
                 stop "Transformation imposible"
                endif
                iBas=iOrb
                jBas=jOrb
                kBas=kOrb
                lBas=lOrb
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                CALL indexSymmetry(iBas,jBas,kBas,lBas,nBas,allRelZero,npm,pm,skip)
                counter=counter+1
                IF (skip) THEN
                    nSkippedIntegrals=nSkippedIntegrals+1
                ELSE
                    DO ipm=1,npm
                        indexCache(:,nCachedInt+ipm)=pm(:,ipm)
                        integralCache(nCachedInt+ipm)=twoElBas
                    ENDDO
                    nCachedRec=nCachedRec+1
                    nCachedInt=nCachedInt+npm
                    IF (nCachedRec.eq.printFreq) EXIT
                ENDIF
            ENDDO
            !$OMP PARALLEL &
            !$OMP DEFAULT(none) &
            !$OMP PRIVATE(jOrb,lOrb,iC,skip) &
            !$OMP SHARED(local,oM,tmp,thrCf) &
            !$OMP SHARED(indexCache,integralCache,nCachedInt) 
            !$OMP DO 
            DO lOrb=1,local
             DO iC=1,nCachedInt
                tmp(indexCache(1,iC),indexCache(2,iC),indexCache(3,iC),lOrb)=&
                   tmp(indexCache(1,iC),indexCache(2,iC),indexCache(3,iC),lOrb)+&
                   oM(indexCache(4,iC),lOrb)* integralCache(iC) 
             ENDDO
            ENDDO
            !$OMP END DO 
            !$OMP END PARALLEL
            nCachedInt=0
            nCachedRec=0
            WRITE(token,'(A,I14,A,I14,A)') "Basis integral record ",counter,&
              " processed (of which irrelvant since last print: ",nSkippedIntegrals,")"
            nSkippedIntegrals=0
            CALL timer%time(TRIM(token))
            IF (finish) THEN
                WRITE(6,'(A)') "All intergrals processed"
                EXIT
            ENDIF
        ENDDO
    CLOSE(1)

deallocate(indexcache,integralcache)

write(token,"(A)") "Contraction from [ij|kl] to [ij|ks] is ready"
CALL timer%time(TRIM(token))

allocate(pqrs1(nbas,local,nbas,local))

write(token,"(A)") "Starting contraction ijks->iqks with q local"
pqrs1=0.d0
CALL timer%time(TRIM(token))
!$OMP PARALLEL DO &
!$OMP PRIVATE(jBas) &
!$OMP COLLAPSE(4)
do iBas=1,nBas
 do kBas=1,nBas
  do lOrb=1,local
   do jOrb=1,local

    do jBas=1,nBas
     pqrs1(iBas,jOrb,kBas,lOrb)=pqrs1(iBas,jOrb,kBas,lOrb)+&
      tmp(iBas,jBas,kBas,lOrb)*oM(jBas,jOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction ijks->iqks with q local"
CALL timer%time(TRIM(token))

allocate(pqrs2(nbas,nbas,local,local))
write(token,"(A)") "Starting contraction ijks->ijrs with r local"
CALL timer%time(TRIM(token))
pqrs2=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE (kBas) &
!$OMP COLLAPSE(4)
do iBas=1,nBas
 do jBas=1,nBas
  do lOrb=1,local
   do kOrb=1,local

    do kBas=1,nBas
     pqrs2(iBas,jBas,kOrb,lOrb)=pqrs2(iBas,jBas,kOrb,lOrb)+&
      tmp(iBas,jBas,kBas,lOrb)*oM(kBas,kOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction ijks->ijrs with r local"
CALL timer%time(TRIM(token))
deallocate(tmp)

allocate(tmp(nbas,local,occupied,local))
write(token,"(A)") "Starting contraction from iqks->iqrs with q local"
CALL timer%time(TRIM(token))
tmp=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE(kBas) &
!$OMP COLLAPSE(4)
do iBas=1,nBas
 do jOrb=1,local
  do lOrb=1,local
   do kOrb=1,occupied

    do kBas=1,nbas
     tmp(iBas,jOrb,kOrb,lOrb)=tmp(iBas,jOrb,kOrb,lOrb)+&
      pqrs1(iBas,jOrb,kBas,lOrb)*oM(kBas,kOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction from iqks->iqrs with q local"
CALL timer%time(TRIM(token))
deallocate(pqrs1)
allocate(pqrs1(occupied,local,occupied,local))
write(token,"(A)") "Starting contraction from iqrs->pqrs with q local"
CALL timer%time(TRIM(token))
pqrs1=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE(iBas) &
!$OMP COLLAPSE(4)
do jOrb=1,local
 do kOrb=1,occupied
  do lOrb=1,local
   do iOrb=1,occupied

    do iBas=1,nbas
     pqrs1(iOrb,jOrb,kOrb,lOrb)=pqrs1(iOrb,jOrb,kOrb,lOrb)+&
      tmp(iBas,jOrb,kOrb,lOrb)*oM(iBas,iOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction from iqrs->pqrs with q local"
CALL timer%time(TRIM(token))
deallocate(tmp)

allocate(tmp(nbas,occupied,local,local))
write(token,"(A)") "Starting contraction from ijrs->iqrs with r local"
CALL timer%time(TRIM(token))
tmp=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE(jBas) &
!$OMP COLLAPSE(4)
do iBas=1,nBas
 do kOrb=1,local
  do lOrb=1,local
   do jOrb=1,occupied

    do jBas=1,nbas
     tmp(iBas,jOrb,kOrb,lOrb)=tmp(iBas,jOrb,kOrb,lOrb)+&
      pqrs2(iBas,jBas,kOrb,lOrb)*oM(jBas,jOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction from ijrs->iqrs with r local"
CALL timer%time(TRIM(token))
deallocate(pqrs2)
allocate(pqrs2(occupied,occupied,local,local))
write(token,"(A)") "Starting contraction from iqrs->pqrs with r local"
CALL timer%time(TRIM(token))
pqrs2=0.d0
!$OMP PARALLEL DO &
!$OMP PRIVATE(iBas) &
!$OMP COLLAPSE(4)
do jOrb=1,occupied
 do kOrb=1,local
  do lOrb=1,local
   do iOrb=1,occupied

    do iBas=1,nbas
     pqrs2(iOrb,jOrb,kOrb,lOrb)=pqrs2(iOrb,jOrb,kOrb,lOrb)+&
      tmp(iBas,jOrb,kOrb,lOrb)*oM(iBas,iOrb)
    enddo

   enddo
  enddo
 enddo
enddo
!$OMP END PARALLEL DO
write(token,"(A)") "End of contraction from iqrs->pqrs with r local"
CALL timer%time(TRIM(token))
deallocate(tmp)

allocate(indexcache(4,printFreq),integralCache(printFreq))
allocate(outindex(4))

!!! Summing the previous contributions ... only symmetric ones are required
inquire(file=trim(outegralFile),exist=ex)
if (ex .and. readprev) then
 write(token,"(A)") "Reading previous data"
 CALL timer%time(TRIM(token))
 if (outputFormat(1:1).eq."f") then
  open(1,file=TRIM(outegralFile),status="old")
  read(1,*) iOrb,jOrb,kOrb,lOrb
 else
  open(1,file=TRIM(outegralFile),form="unformatted",status="old")
  read(1) iOrb,jOrb,kOrb,lOrb
 endif
 if (iorb.ne.occupied.or.jorb.ne.local.or.korb.ne.occupied.or.lorb.ne.local) then
  write(6,*) "According to input must be ",occupied,local,occupied,local
  write(6,*) "In the previous file is ",iorb,jorb,korb,lorb
  write(6,*) "WARNING the number of orbitals is not correct"
  stop "WARNING the number of orbitals is not correct"
 endif
  
 counter=0 
 ncachedint=0
 finish=.false.
 do
  ncachedint=ncachedint+1
  counter=counter+1
  if (outputFormat(1:1).eq."f") then
   read(1,*,iostat=stat) indexcache(:,ncachedint),integralCache(ncachedint)
  else
   read(1,iostat=stat) indexcache(:,ncachedint),integralCache(ncachedint)
  endif
  if (stat.ne.0) then
   ncachedint=ncachedint-1
   counter=counter-1
   finish=.true.
  endif
  if (ncachedint.eq.printfreq.or.finish) then
!$OMP PARALLEL DO PRIVATE(ibas,jbas,outindex)
   do ibas=1,ncachedint
    call checkorder(indexcache(:,ibas),local,jbas,outindex)
    if (jbas.eq.1) then
     pqrs1(outindex(1),outindex(2),outindex(3),outindex(4))=&
     pqrs1(outindex(1),outindex(2),outindex(3),outindex(4))+integralcache(ibas)
    else
     pqrs2(outindex(1),outindex(2),outindex(3),outindex(4))=&
     pqrs2(outindex(1),outindex(2),outindex(3),outindex(4))+integralcache(ibas)
    endif
   enddo
!$OMP END PARALLEL DO
   WRITE(token,'(A,I14,A)') "One block of ",ncachedint," read"
   CALL timer%time(TRIM(token))
   ncachedint=0
  endif
  if (finish) exit
 enddo
 close(1)
 write(token,"(A,I0)") "Total integrals read ",counter
 call timer%time(trim(token))
endif
  

write(token,"(A)") "Writing transformed integrals"
CALL timer%time(TRIM(token))
!! Having into account permutation properties only a subset of the integrals are printed
    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,file=TRIM(outegralFile),STATUS="replace")
        WRITE(1,'(4(I0,x))' ) occupied,local,occupied,local
    ELSE 
        OPEN(1,file=TRIM(outegralFile),STATUS="replace",FORM="unformatted")
        WRITE(1) occupied,local,occupied,local
    ENDIF
    counter=0
    ncachedint=0
    DO iOrb=1,local
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb) 
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied   
                    IF ((jOrb.gt.local).AND.(kOrb.gt.local).AND.(lOrb.gt.local)) CYCLE
                !! Lets put the index in cache
                    ncachedint=ncachedint+1
                    indexcache(1,ncachedint)=iorb
                    indexcache(2,ncachedint)=jorb
                    indexcache(3,ncachedint)=korb
                    indexcache(4,ncachedint)=lorb

                !!! Write them
                    if (ncachedint.eq.printfreq) then
!write(6,*) "Integrals still in cache ",ncachedint
                !!! Order them
!$OMP PARALLEL DO PRIVATE( outindex,jbas)
                        do ibas=1,ncachedint
                            call checkorder(indexcache(:,ibas),local,jbas,outindex)
                            if (jbas.eq.1) then
                                integralcache(ibas)=pqrs1(outindex(1),outindex(2),outindex(3),outindex(4))
                            else
                                integralcache(ibas)=pqrs2(outindex(1),outindex(2),outindex(3),outindex(4))
                            endif
                        enddo 
!$OMP END PARALLEL DO
                        do ibas=1,ncachedint
                            call writeint(1,outputFormat,indexcache(:,ibas),integralcache(ibas),counter,thrCt)
                        enddo
                        ncachedint=0
                        write(token,"(A,I0)") "Number of non-zero integrals ",counter
                        CALL timer%time(TRIM(token))
                    endif
               !!!!

                ENDDO
            ENDDO
        ENDDO
    ENDDO
                !!! Write them
                    if (ncachedint.ne.0) then
!write(6,*) "Integrals still in cache ",ncachedint
                !!! Order them
!$OMP PARALLEL DO PRIVATE( outindex,jbas )
                        do ibas=1,ncachedint
                            call checkorder(indexcache(:,ibas),local,jbas,outindex)
                            if (jbas.eq.1) then
                                integralcache(ibas)=pqrs1(outindex(1),outindex(2),outindex(3),outindex(4))
                            else
                                integralcache(ibas)=pqrs2(outindex(1),outindex(2),outindex(3),outindex(4))
                            endif
                        enddo 
!$OMP END PARALLEL DO
                        do ibas=1,ncachedint
                            call writeint(1,outputFormat,indexcache(:,ibas),integralcache(ibas),counter,thrCt)
                        enddo
                    endif
               !!!!

    write(token,"(A,I0)") "Number of non-zero integrals ",counter
    CALL timer%time(TRIM(token))

    if (rmint) then
     write(6,*) "Now let's remove the basis integral file ",trim(integralFile)
     write(fname,"(A,A)") "rm -f ",trim(integralFile)
     call system(fname)
    endif
    WRITE(6,'(A)') "Happy"
END PROGRAM

SUBROUTINE indexSymmetry(iBas,jBas,kBas,lBas,nBas,allRelZero,npm,pm,skip)
    IMPLICIT none
    INTEGER, INTENT(in) :: iBas,jBas,kBas,lBas,nBas
    LOGICAL, INTENT(in) :: allRelZero(nBas)
    INTEGER, INTENT(inout) :: pm(4,8)
    INTEGER, INTENT(out) :: npm
    LOGICAL, INTENT(out) :: skip
    skip=.FALSE.
    IF ((iBas.eq.jBas).AND.(iBas.eq.kBas).AND.(iBas.eq.lBas)) THEN !iiii 
        pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
        nPm=1
        IF (allRelZero(iBas)) THEN
            skip=.TRUE.
        ENDIF
    ELSEIF ((iBas.eq.jBas).AND.(kBas.eq.lBas).AND.(iBas.ne.kBas)) THEN !iijj
        pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas !WAS MISSING
        pm(1,2)=kBas;pm(2,2)=lBas;pm(3,2)=iBas;pm(4,2)=jBas
        nPm=2
        IF (allRelZero(iBas).OR.allRelZero(kBas)) THEN
            skip=.TRUE.
        ENDIF
    ELSEIF ((iBas.ne.jBas).AND.(kbas.eq.lBas)) THEN !ijkk
        IF (allRelZero(kBas).OR.(allRelZero(iBas).AND.allRelZero(jBas))) THEN
            skip=.TRUE.
        ELSE
            pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
            pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
            pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
            pm(1,4)=kBas;pm(2,4)=lBas;pm(3,4)=jBas;pm(4,4)=iBas
            nPm=4
        ENDIF
    ELSEIF ((iBas.eq.jBas).AND.(kbas.ne.lBas)) THEN !iijk
        IF (allRelZero(iBas).OR.(allRelZero(kBas).AND.allRelZero(lBas))) THEN
            skip=.TRUE.
        ELSE
            pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
            pm(1,2)=iBas;pm(2,2)=jBas;pm(3,2)=lBas;pm(4,2)=kBas
            pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
            pm(1,4)=lBas;pm(2,4)=kBas;pm(3,4)=iBas;pm(4,4)=jBas
            nPm=4
        ENDIF
    ELSEIF ((iBas.ne.jBas).AND.(kBas.ne.lBas)) THEN !ijij
        nPm=4
        IF (.NOT.(((iBas.eq.kBas).AND.(jBas.eq.lBas)).OR.((iBas.eq.lBas).AND.(jBas.eq.kBas)))) THEN !ijkl
            nPm=8
        ENDIF
        IF (nPm.eq.4) THEN  
            IF (allRelZero(iBas).AND.allRelZero(jBas)) THEN
                skip=.TRUE.
            ELSE
                pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
                pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
            ENDIF
        ELSE
            IF ((allRelZero(iBas).AND.allRelZero(jBas)).OR.(allRelZero(kBas).AND.allRelZero(lBas))) THEN
                skip=.TRUE.
            ELSE
                pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
                pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
                pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
                pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
                pm(1,5)=kBas;pm(2,5)=lBas;pm(3,5)=iBas;pm(4,5)=jBas
                pm(1,6)=kBas;pm(2,6)=lBas;pm(3,6)=jBas;pm(4,6)=iBas
                pm(1,7)=lBas;pm(2,7)=kBas;pm(3,7)=iBas;pm(4,7)=jBas
                pm(1,8)=lBas;pm(2,8)=kBas;pm(3,8)=jBas;pm(4,8)=iBas
            ENDIF
        ENDIF
    ENDIF
END SUBROUTINE


subroutine checkorder(prevorder,local,un,neworder)
integer, intent(in) :: local,prevorder(4)
integer, intent(out) :: neworder(4),un

                    IF ((prevorder(3).le.local).OR.(prevorder(4).le.local)) THEN
                        IF (prevorder(3).le.prevorder(4)) THEN  
                            un=1
                            neworder(1)=prevorder(2)
                            neworder(2)=prevorder(1)
                            neworder(3)=prevorder(4)
                            neworder(4)=prevorder(3)
                        ELSE
                            un=1
                            neworder(1)=prevorder(2)
                            neworder(2)=prevorder(1)
                            neworder(3)=prevorder(3)
                            neworder(4)=prevorder(4)
                        ENDIF
                    ELSE
                            un=2
                            neworder(1)=prevorder(3)
                            neworder(2)=prevorder(4)
                            neworder(3)=prevorder(1)
                            neworder(4)=prevorder(2)
                    ENDIF
return
end subroutine checkorder

subroutine writeint(un,outputFormat,order,twoel,counter,thrCt)
    integer,intent(in) :: un,order(4)
    character*200,intent(in) :: outputFormat
    real*8, intent(in) :: twoel,thrCt
    integer,intent(inout) :: counter

    if (abs(twoel).gt.thrct) then
     counter=counter+1
     if (outputformat(1:1).eq."f") then
      write(un,'(4(I0,x),E24.16)') order(:),twoel
     else
      write(un) order(:),twoel
     endif
    endif

end subroutine writeint
