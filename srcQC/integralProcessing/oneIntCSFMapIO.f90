!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE oneIntCSFMapIO
!CLASS FOR OBJECTS THAT LINK TWO ELECTRON INTEGRAL ij
!TO THE MATRIX ELEMENT <csf1|g|csf2> THAT DEPEND ON IT
    IMPLICIT none
    PRIVATE
    !NEW    
    PUBLIC OPERATOR(+)
    !END NEW    
    PUBLIC oneIntCSFMap_
    !INTERNAL TYPES
    TYPE csfList_
        INTEGER :: nCSF=0
        INTEGER :: next=1
        INTEGER*4, ALLOCATABLE :: csf(:,:)
        INTEGER*4, ALLOCATABLE :: zahler(:)
        INTEGER*4, ALLOCATABLE :: nenner(:)
    END TYPE
    !CLASS STRUCTURE
    TYPE oneIntCSFMap_
        PRIVATE
        TYPE(csfList_), ALLOCATABLE :: csfList(:,:)
        INTEGER :: occupied=0
        INTEGER :: un=0
        LOGICAL :: alive=.FALSE.
        CONTAINS
            PROCEDURE :: create
            PROCEDURE :: destroy
            PROCEDURE :: incrementCSFSpace
            PROCEDURE :: addCSF
            PROCEDURE :: openOneIntCSFMap
            PROCEDURE :: writeOneIntCSFMap
            PROCEDURE :: getRecord
            PROCEDURE :: getOccupied
            PROCEDURE :: allocateCSF
            PROCEDURE :: reset
            PROCEDURE :: compress
    END TYPE
        
    !NEW    
    INTERFACE OPERATOR (+)
        PROCEDURE addOneIntCSFMap
    END INTERFACE OPERATOR (+)
    !END NEW

    CONTAINS
        !NEW
        FUNCTION addOneIntCSFMap(a,b)
            IMPLICIT none
            INTEGER :: occupied
            INTEGER :: iCSF,jCSF
            INTEGER :: iOrb,jOrb
            TYPE(oneIntCSFMap_),INTENT(in) :: a,b
            TYPE(oneIntCSFMap_) :: addOneIntCSFMap
            occupied=a%occupied
            CALL addOneIntCSFMap%create(occupied)
            DO iOrb=1,occupied
                DO jOrb=1,occupied
                    iCSF=a%csfList(iOrb,jOrb)%nCSF
                    jCSF=b%csfList(iOrb,jOrb)%nCSF
                    addOneIntCSFMap%csfList(iOrb,jOrb)%nCSF=iCSF+jCSF
                ENDDO
            ENDDO
            CALL addOneIntCSFMap%allocateCSF
            DO iOrb=1,occupied
                DO jOrb=1,occupied
                    iCSF=a%csfList(iOrb,jOrb)%nCSF
                    jCSF=b%csfList(iOrb,jOrb)%nCSF
                    IF (iCSF+jCSF.gt.0) THEN
                        addOneIntCSFMap%csfList(iOrb,jOrb)%csf(1:iCSF,:)=a%csfList(iOrb,jOrb)%csf
                        addOneIntCSFMap%csfList(iOrb,jOrb)%csf(iCSF+1:iCSF+jCSF,:)=b%csfList(iOrb,jOrb)%csf
                        addOneIntCSFMap%csfList(iOrb,jOrb)%nenner(1:iCSF)=a%csfList(iOrb,jOrb)%nenner
                        addOneIntCSFMap%csfList(iOrb,jOrb)%nenner(iCSF+1:iCSF+jCSF)=b%csfList(iOrb,jOrb)%nenner
                        addOneIntCSFMap%csfList(iOrb,jOrb)%zahler(1:iCSF)=a%csfList(iOrb,jOrb)%zahler
                        addOneIntCSFMap%csfList(iOrb,jOrb)%zahler(iCSF+1:iCSF+jCSF)=b%csfList(iOrb,jOrb)%zahler
                    ENDIF
                ENDDO
            ENDDO
            addOneIntCSFMap%alive=.TRUE.
        END FUNCTION
        !END NEW

        !CLASS METHODS
        SUBROUTINE create(this,occupied)
            IMPLICIT none
            INTEGER, INTENT(in) :: occupied
            CLASS(oneIntCSFMap_) :: this
            this%occupied=occupied
            ALLOCATE(this%csfList(occupied,occupied))
        END SUBROUTINE
        SUBROUTINE destroy(this)
            IMPLICIT none
            INTEGER :: iOrb,jOrb
            CLASS(oneIntCSFMap_) :: this
            this%un=0
            this%occupied=0
            this%alive=.FALSE.
            DEALLOCATE(this%csfList)
        END SUBROUTINE
        
        SUBROUTINE incrementCSFSpace(this,i,j)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER, INTENT(in) :: i,j
            this%csfList(i,j)%nCSF=this%csfList(i,j)%nCSF+1
        END SUBROUTINE 
                
        SUBROUTINE allocateCSF(this)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER :: nCSF,i,j,occ
            occ=this%occupied
            DO i=1,occ
                DO j=1,occ
                    nCSF=this%csfList(i,j)%nCSF
                    ALLOCATE(this%csfList(i,j)%csf(nCSF,2))
                    ALLOCATE(this%csfList(i,j)%zahler(nCSF))
                    ALLOCATE(this%csfList(i,j)%nenner(nCSF))
                ENDDO
            ENDDO
        END SUBROUTINE
        
        SUBROUTINE addCSF(this,i,j,csf1,csf2,zahler,nenner)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER, INTENT(in) :: i,j,csf1,csf2,zahler,nenner
            INTEGER :: nCSF,next
            nCSF=this%csfList(i,j)%nCSF
            next=this%csfList(i,j)%next
            IF (next.gt.nCSF) STOP "ERROR: csfSpace overflow"
            
            this%csfList(i,j)%csf(next,1)=csf1
            this%csfList(i,j)%csf(next,2)=csf2
            this%csfList(i,j)%zahler(next)=zahler
            this%csfList(i,j)%nenner(next)=nenner
            next=next+1
            this%csfList(i,j)%next=next
        END SUBROUTINE
        
        SUBROUTINE openOneIntCSFMap(this,fich,un)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            CHARACTER(len=*), INTENT(in) :: fich
            INTEGER, INTENT(in) :: un
            this%un=un
            OPEN(un,file=TRIM(fich),status="old",FORM="unformatted")
            READ(un) this%occupied
        END SUBROUTINE
        
        SUBROUTINE getRecord(this,i,j,nCSF,csf,zahler,nenner,stat)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER,INTENT(out) :: i,j,nCSF,stat
            INTEGER,ALLOCATABLE,INTENT(inout) :: csf(:,:),nenner(:),zahler(:)
            INTEGER :: iCSF
            READ (this%un,IOSTAT=stat) i,j,nCSF
            IF (stat.ne.0) RETURN
            IF (nCSF.eq.0) RETURN
            IF (ALLOCATED(csf)) THEN
                IF (size(csf,1).ne.nCSF.OR.size(csf,2).ne.2) THEN
                    DEALLOCATE(csf)
                    ALLOCATE(csf(nCSF,2))
                ENDIF
            ELSE
                    ALLOCATE(csf(nCSF,2))
            ENDIF
            IF (ALLOCATED(zahler)) THEN
                IF (size(zahler,1).ne.nCSF) THEN
                    DEALLOCATE(zahler)
                    ALLOCATE(zahler(nCSF))
                ENDIF
            ELSE
                    ALLOCATE(zahler(nCSF))
            ENDIF
            IF (ALLOCATED(nenner)) THEN
                IF (size(nenner,1).ne.nCSF) THEN
                    DEALLOCATE(nenner)
                    ALLOCATE(nenner(nCSF))
                ENDIF
            ELSE
                ALLOCATE(nenner(nCSF))
            ENDIF
            READ(this%un)(csf(iCSF,1),&
              csf(iCSF,2),&
              zahler(iCSF),&
              nenner(iCSF),iCSF=1,nCSF)
        END SUBROUTINE 
        
        SUBROUTINE writeOneIntCSFMap(this,fich,sorte)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            CHARACTER*200 :: fmt
            CHARACTER(len=*), INTENT(in) :: fich
            CHARACTER(len=*), INTENT(in), OPTIONAL :: sorte
            
            LOGICAL :: binary
            INTEGER :: i,j
            INTEGER :: occupied,stat,nCSF,iCSF
            binary=.FALSE.
            IF (PRESENT(sorte)) THEN
                IF (sorte(1:1).eq.'b') binary=.TRUE.
            ENDIF
            IF (binary) OPEN(1,file=TRIM(fich),status="replace",FORM="unformatted",IOSTAT=stat)
            IF (.NOT.binary) OPEN(1,file=TRIM(fich),status="replace",IOSTAT=stat)
            occupied=this%occupied
            IF (binary) WRITE(1) occupied
            IF (.NOT.binary) WRITE(1,*) occupied
            DO i=1,occupied
                DO j=1,occupied
                    nCSF=this%csfList(i,j)%nCSF
                    IF (binary)WRITE(1,IOSTAT=stat) i,j,nCSF
                    IF (.NOT.binary)WRITE(1,*,IOSTAT=stat) i,j,nCSF
                    IF (nCSF.gt.0) THEN
                        IF (binary) THEN
                             WRITE(1,IOSTAT=stat) (this%csfList(i,j)%csf(iCSF,1),&
                               this%csfList(i,j)%csf(iCSF,2),&
                               this%csfList(i,j)%zahler(iCSF),&
                               this%csfList(i,j)%nenner(iCSF),iCSF=1,nCSF)
                        ELSE   
                            WRITE(fmt,'(A,I0,A)') "(",nCSF,&
                              "(x,I8,x,I8,x,A1,x,I8,x,A1,x,x,I8,x,A1))"
                            WRITE(1,TRIM(fmt),IOSTAT=stat) (this%csfList(i,j)%csf(iCSF,1),&
                              this%csfList(i,j)%csf(iCSF,2),":",&
                              this%csfList(i,j)%zahler(iCSF),"/",&
                              this%csfList(i,j)%nenner(iCSF),"|",iCSF=1,nCSF)
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
            CLOSE(1)
        END SUBROUTINE
        
        SUBROUTINE compress(this)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER :: iOrb,jOrb,counter
            INTEGER :: iCSF,nCSF,tCSF(2),csf(2)
            INTEGER :: zahler,nenner,tZ,tN
            INTEGER :: newNCSF,occupied
            INTEGER, ALLOCATABLE :: newZahler(:),newNenner(:),newCSF(:,:)
            occupied=this%occupied
            DO iOrb=1,occupied
                DO jOrb=1,occupied
                    nCSF=this%csfList(iOrb,jOrb)%nCSF
                    IF (nCSF.le.1) CYCLE
                    tCSF(1)=this%csfList(iOrb,jOrb)%csf(1,1)
                    tCSF(2)=this%csfList(iOrb,jOrb)%csf(1,2)
                    tZ=this%csfList(iOrb,jOrb)%zahler(1)
                    tN=this%csfList(iOrb,jOrb)%nenner(1)
                    !can increase mCSF every
                    !time a new block
                    !commences
                    newNCSF=0
                    counter=0 
                    DO iCSF=1,nCSF
                        csf=this%csfList(iOrb,jOrb)%csf(iCSF,:)
                        zahler=this%csfList(iOrb,jOrb)%zahler(iCSF)
                        nenner=this%csfList(iOrb,jOrb)%nenner(iCSF)
                        IF (csf(1).ne.tCsf(1).OR.csf(2).ne.tCsf(2).OR.ABS(tZ).ne.ABS(zahler).OR.tN.ne.nenner) THEN
                            IF (abs(counter).gt.0) THEN
                                newNCSF=newNCSF+1 
                            ENDIF
                            tCSF=csf
                            tZ=zahler
                            tN=nenner
                            counter=tZ/abs(tZ)
                        ELSE
                            counter=counter+zahler/ABS(zahler)
                        ENDIF
                    ENDDO 
                    IF (abs(counter).gt.0) THEN
                        newNCSF=newNCSF+1
                    ENDIF
                    ALLOCATE(newZahler(newNCSF))
                    ALLOCATE(newNenner(newNCSF))
                    ALLOCATE(newCSF(newNCSF,2))
                    tCSF(1)=this%csfList(iOrb,jOrb)%csf(1,1)
                    tCSF(2)=this%csfList(iOrb,jOrb)%csf(1,2)
                    tZ=this%csfList(iOrb,jOrb)%zahler(1)  
                    tN=this%csfList(iOrb,jOrb)%nenner(1)
                    !must wait until end to
                    !of block of equialent
                    !terms before storing >
                    !figure this out
                    newNCSF=0
                    counter=0
                    DO iCSF=1,nCSF
                        csf=this%csfList(iOrb,jOrb)%csf(iCSF,:)
                        zahler=this%csfList(iOrb,jOrb)%zahler(iCSF)
                        nenner=this%csfList(iOrb,jOrb)%nenner(iCSF)
                        IF (csf(1).eq.tCsf(1).AND.csf(2).eq.tCsf(2).AND.ABS(tZ).eq.ABS(zahler).AND.tN.eq.nenner) THEN
                            counter=counter+zahler/ABS(zahler)                                            
                        ELSE
                            IF (abs(counter).gt.0) THEN
                                newNCSF=newNCSF+1
                                newZahler(newNCSF)=abs(tZ)*counter**3/abs(counter)
                                newNenner(newNCSF)=abs(tN)
                                newCSF(newNCSF,:)=tCSF
                            ENDIF
                            tCSF=csf
                            tZ=zahler
                            tN=nenner
                            counter=tZ/abs(tZ)
                        ENDIF
                    ENDDO
                    IF (abs(counter).gt.0) THEN
                        newNCSF=newNCSF+1
                        newZahler(newNCSF)=abs(tZ)*counter**3/abs(counter)
                        newNenner(newNCSF)=tN
                        newCSF(newNCSF,:)=tCSF
                    ENDIF
                    this%csfList(iOrb,jOrb)%nCSF=newNCSF
                    DEALLOCATE(this%csfList(iOrb,jOrb)%csf)
                    DEALLOCATE(this%csfList(iOrb,jOrb)%zahler)
                    DEALLOCATE(this%csfList(iOrb,jOrb)%nenner)
                    ALLOCATE(this%csfList(iOrb,jOrb)%csf(newNCSF,2))
                    ALLOCATE(this%csfList(iOrb,jOrb)%zahler(newNCSF))
                    ALLOCATE(this%csfList(iOrb,jOrb)%nenner(newNCSF))
                    this%csfList(iOrb,jOrb)%csf=newCSF
                    this%csfList(iOrb,jOrb)%nenner=newNenner
                    this%csfList(iOrb,jOrb)%zahler=newZahler
                    DEALLOCATE(newZahler)
                    DEALLOCATE(newNenner)
                    DEALLOCATE(newCSF)
                ENDDO
            ENDDO
        END SUBROUTINE
        SUBROUTINE getOccupied(this,n)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            INTEGER, INTENT(out) :: n
            n=this%occupied
        END SUBROUTINE
        SUBROUTINE reset(this)
            IMPLICIT none
            CLASS(oneIntCSFMap_) :: this
            REWIND(this%un)
            READ(this%un) this%occupied
        END SUBROUTINE
END MODULE
