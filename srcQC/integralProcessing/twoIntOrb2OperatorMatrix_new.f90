!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntOrb2OperatorMatrix_new
    USE omp_lib
    USE RDMIO
    USE twoIntIO
    USE oneIntIO
    USE stopWatch
    USE sortingAlgorithms
    USE searchInsert

    IMPLICIT none
    !CONTROL VARS
    INTEGER :: stat
    LOGICAL :: exists
    LOGICAL :: debug
    TYPE(stopWatch_) :: timer
    CHARACTER*200 :: inputFile,fmt
    !INPUT VARS
    CHARACTER*200 :: twoIntRDMFile
    CHARACTER*200 :: twoIntFile
    CHARACTER*200 :: outputFile
    CHARACTER*200 :: outputFormat

    INTEGER :: nSource
    INTEGER :: nParent
    INTEGER :: nInactive
    INTEGER :: printFreq

    !STRUCTURAL VARS
    TYPE(RDM_) :: twoIntRDM
    TYPE(twoInt_) :: twoInt
    TYPE(oneInt_) :: output

    REAL*8, ALLOCATABLE :: operatorMatrix(:,:)
    REAL*8, ALLOCAtABLE :: rdm(:),rdmMat(:,:)

    INTEGER :: nLocalOrb
    INTEGER :: nStateTotal
    INTEGER :: nStateRDM
    INTEGER :: nXOrb
    INTEGER :: occupied
    INTEGER :: nSourceAndLocalAugState
    INTEGER :: m1,m2
    INTEGER :: nIntegral
    INTEGER :: nRDM
    INTEGER :: nOrbInRdm
    INTEGER, ALLOCATABLE :: rdmKey(:) 

    !TEMPORAL VARS
    LOGICAL :: empty
    INTEGER :: key,location
    INTEGER :: iRDM,iPm,nPm,pm(4,8)
    INTEGER :: iOrb,jOrb,iIon
    INTEGER :: iState, jState
    INTEGER :: kState, lState
    INTEGER :: ind1(4),ind2(4)
    INTEGER, ALLOCATABLE :: stateID1(:),stateID2(:)
    INTEGER, ALLOCATABLE :: augmentingOrb(:)
    INTEGER, ALLOCATABLE :: order(:)
    REAL*8 :: integral
    REAL*8, ALLOCATABLE :: tempRealArr2d(:,:)
    CHARACTER*200 :: msg

    !PARALLELIZATION VARS
    INTEGER*4 :: nCPU

    NAMELIST /INPUT/ twoIntRDMFile,&
      twoIntFile,&
      outputFile,&
      nInactive,&
      nSource,&
      nParent,&
      debug,&
      nCPU,&
      printFreq,&
      outputFormat

    outputFormat="b"
    printFreq=10000
    nCPU=0
    debug=.FALSE.
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    CALL twoInt%readTwoInt(TRIM(twoIntFile),"b",2)
    CALL twoIntRDM%initializeOld(TRIM(twoIntRDMFile),"b",1)
    CALL twoInt%getNBas(occupied)
    CALL twoIntRDM%getStates(nStateRDM)
    CALL twoIntRDM%getOccupied(nOrbInRdm)

    nLocalOrb=(nStateRDM-nSource)/nParent+nInactive-2
    nXOrb=occupied-nLocalOrb
    nStateTotal=(occupied-nInactive)*nParent+nSource
    nSourceAndLocalAugState=nStateRDM-2*nParent

    WRITE(6,'(A)') "Orbitals in RDM (polycentric) : "
    WRITE(6,'(A,I0)') "     ",nLocalOrb
    WRITE(6,'(A)') "Diffuse Orbitals : "
    WRITE(6,'(A,I0)') "     ",nXOrb
    WRITE(6,'(A)') "Total number of states to construct operator matrix :"
    WRITE(6,'(A,I0)') "     ",nStateTotal
    WRITE(6,'(A)') "Number of states in the RDM :"
    WRITE(6,'(A,I0)') "     ",nStateRDM
    WRITE(6,'(A)') "Number of States not resulting from augmentation in diffuse orbitals:"
    WRITE(6,'(A,I0)') "     ",nSourceAndLocalAugState
    WRITE(6,'(A)') "Number of orbitals defining rdm file"
    WRITE(6,'(A,I0)') "     ",nOrbInRdm


    ALLOCATE(operatorMatrix(nStateTotal,nStateTotal))
    ALLOCATE(rdm(nStateRDM*(nStateRDM+1)/2))
    ALLOCATE(stateID1(nStateTotal))
    ALLOCATE(stateID2(nStateTotal))
    ALLOCATE(augmentingOrb(nStateTotal))
    operatorMatrix=0.d0

    DO iState=1,nSource
        augmentingOrb(iState)=0
    ENDDO
    jState=nSource
    DO iOrb=1,occupied-nInactive
        DO iIon=1,nParent
            jState=jState+1
            augmentingOrb(jState)=iOrb
        ENDDO
    ENDDO

    DO iOrb=1,nSourceAndLocalAugState
        stateID1(iOrb)=iOrb
        stateID2(iOrb)=iOrb
    ENDDO
    jOrb=nSourceAndLocalAugState
    DO iOrb=1,nXOrb
        DO iIon=1,nParent
            jOrb=jOrb+1
            stateID1(jOrb)=nSourceAndLocalAugState+iIon
            stateID2(jOrb)=nSourceAndLocalAugState+iIon+nParent
        ENDDO
    ENDDO

    IF (debug) THEN
        WRITE(6,'(A)') "stateID1"
        WRITE(6,'(10(x,I4))') (stateID1(iState),iState=1,nStateTotal)
        WRITE(6,'(A)') ""
        WRITE(6,'(A)') "stateID2"
        WRITE(6,'(10(x,I4))') (stateID2(iState),iState=1,nStateTotal)
    ENDIF

    WRITE(6,'(A)') "READ trd2"

    nRDM=0
    DO
        READ(1,iostat=stat) ind2(1),ind2(2),ind2(3),ind2(4),empty
        IF (stat.ne.0) EXIT
        IF (.NOT.empty) THEN
            nRDM=nRDM+1
            !IF (mod(nRDM,10000).eq.0) THEN
            !    print*, nRDM
            !ENDIF
            READ(1)
        ENDIF
    ENDDO
    WRITE(6,'(A,I0)') "number of relevant rdms = ",nRDM
    WRITE(6,'(A,E18.6)') "memory necessary in GB = ",4*REAL(nRDM*nStateRDM*(nStateRDM+1))/REAL(1024**3)
    WRITE(6,'(A)') "Allocating Memory"
    ALLOCATE(rdmMat(nRDM,nStateRDM*(nStateRDM+1)/2))
    ALLOCATE(rdmKey(nRDM))
    ALLOCATE(order(nRDM))
    REWIND(1)
    READ(1)
    nRDM=0
    WRITE(6,'(A)') "Reading Matrices"
    DO
        READ(1,iostat=stat) ind2(1),ind2(2),ind2(3),ind2(4),empty
        IF (stat.ne.0) EXIT
        IF (.NOT.empty) THEN
            nRDM=nRDM+1
            !IF (mod(nRDM,100).eq.0) THEN
            !    print*, nRDM
            !ENDIF
            order(nRDM)=nRDM
            READ(1) ((rdmMat(nRDM,iState*(iState-1)/2+jState),jState=1,iState),iState=1,nStateRDM)
            rdmKey(nRDM)=ind2(1)+ind2(2)*(nOrbInRdm+1)+ind2(3)*(nOrbInRdm+1)**2+ind2(4)*(nOrbInRdm+1)**3
        ENDIF
    ENDDO
    CLOSE(1)

    ALLOCATE(tempRealArr2d(nRDM,nStateRDM*(nStateRDM+1)/2))

    WRITE(6,'(A)') "Sort RDMs"
    CALL QSortInt(nRDM,rdmKey,order)
    tempRealArr2d=rdmMat

    DO iRDM=1,nRDM
        rdmMat(iRDM,:)=tempRealArr2d(order(iRDM),:)
    ENDDO
    DEALLOCATE(tempRealArr2d,order)

    IF (nCPU.eq.0) THEN
        nCPU=OMP_get_max_threads()  
        WRITE(6,'(A,I0)') "Set number of CPUs to maximum = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ELSE
        WRITE(6,'(A,I0)') "Use userdefined number of CPUs = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ENDIF
    WRITE(6,'(A)') "BEGIN COMPUTATION"
    CALL timer%initialize()
    nIntegral=0
    DO  
        CALL twoInt%getRecord(ind1(1),ind1(2),ind1(3),ind1(4),integral,stat)
        IF (stat.ne.0) THEN
            WRITE(6,'(A,I8,A)') "End of integral file reached after reading ",nIntegral," integrals"
            EXIT
        ENDIF
        IF (debug) THEN
            WRITE(6,'(A)') ""
            WRITE(6,'(A)') ""
            WRITE(6,'(A,4(x,I4),A,F12.6)') "two e- integral: ",ind1," = ",integral
        ENDIF
        nIntegral=nIntegral+1
        IF (mod(nIntegral,printFreq).eq.0) THEN
            WRITE(msg,'(A,I0)') "Integrals processes = ", nIntegral
            CALL timer%time(trim(msg))
        ENDIF
        CALL posMax2Val(ind1,nLocalOrb,m1,m2) 
        IF (debug) THEN
            WRITE(6,'(A,I0)') "integral key : ",ind1(1)+ind1(2)*(nOrbInRdm+1)+ind1(3)*(nOrbInRdm+1)**2+ind1(4)*(nOrbInRdm+1)**3 
        ENDIF
        IF (debug) THEN 
            WRITE(6,'(A,4(x,I4))') "rdm adapted two e- integral: ",ind1
            WRITe(6,'(A,2(x,I4))') "max indeces: ",m1,m2
        ENDIF
        !if all orbitals are local, the rdm must be added to all states
        !if one orbital is diffuse, 
        !if two orbitals are diffuse, 
        CALL getPerms(ind1(1),ind1(2),ind1(3),ind1(4),nPm,pm)
        DO iPm=1,nPm  
            key=pm(1,iPM)+pm(2,iPM)*(nOrbInRdm+1)+pm(3,iPM)*(nOrbInRdm+1)**2+pm(4,iPM)*(nOrbInRdm+1)**3
            CALL binarySearch(key,nRDM,rdmKey,location)
            IF (debug) THEN
                WRITE(6,'(A,4(x,I0),A,x,I0)') "Searched for RDM ",pm(:,iPm)," with key :",key 
                IF (location.ge.0) THEN
                    WRITE(6,'(A)') "RDM not found"
                ELSE
                    WRITE(6,'(A,x,I0)') "RDM found at  :",ABS(location)
                ENDIF
                WRITE(6,'(A)') "----"
            ENDIF
            IF (location.ge.0) THEN
                CYCLE
            ELSE    
                rdm=rdmMat(ABS(location),:)
            ENDIF
            IF (m1.le.nLocalOrb.AND.&
              m2.le.nLocalOrb) THEN
                !$OMP PARALLEL &
                !$OMP DEFAULT(none) &
                !$OMP PRIVATE(iState,jState,kState,lState) &
                !$OMP SHARED(integral,stateID1,stateID2) &
                !$OMP SHARED(nStateTotal,nSourceAndLocalAugState) &
                !$OMP SHARED(operatorMatrix,rdm,augmentingOrb)
                !$OMP DO
                DO iState=1,nStateTotal
                    IF (iState.gt.nSourceAndLocalAugState) THEN
                        kState=stateID1(iState)
                    ELSE
                        kState=iState
                    ENDIF
                    DO jState=iState,nStateTotal
                        IF (jState.gt.nSourceAndLocalAugState) THEN
                            IF (augmentingOrb(iState).eq.augmentingOrb(jState)) THEN
                                lState=stateID1(jState)
                            ELSE
                                lState=stateID2(jState)
                            ENDIF
                        ELSE
                            lState=jState
                        ENDIF
                        operatorMatrix(iState,jState)=&
                          operatorMatrix(iState,jState)+&
                          rdm(lState*(lState-1)/2+kState)*integral
                    ENDDO
                ENDDO
                !$OMP END DO
                !$OMP END PARALLEL
            ELSEIF (m1.le.nLocalOrb.OR.&
              m2.le.nLocalOrb) THEN
                DO iState=1,nSourceAndLocalAugState
                    kState=iState
                    DO jState=nSourceAndLocalAugState+(m1-nLocalOrb-1)*nParent+1,&
                              nSourceAndLocalAugState+(m1-nLocalOrb)*nParent
                        lState=stateID1(jState)
                        operatorMatrix(iState,jState)=&
                          operatorMatrix(iState,jState)+&
                          rdm(lState*(lState-1)/2+kState)*integral
                    ENDDO
                ENDDO
            ELSE
                DO iState=nSourceAndLocalAugState+(m2-nLocalOrb-1)*nParent+1,&
                          nSourceAndLocalAugState+(m2-nLocalOrb)*nParent
                    kState=stateID1(iState)

                    IF (m1.eq.m2) THEN
                        DO jState=iState,nSourceAndLocalAugState+(m2-nLocalOrb)*nParent
                            IF (augmentingOrb(iState).eq.augmentingOrb(jState)) THEN
                                lState=stateID1(jState)
                            ELSE
                                lState=stateID2(jState)
                            ENDIF
                            operatorMatrix(iState,jState)=&
                              operatorMatrix(iState,jState)+&
                              rdm(lState*(lState-1)/2+kState)*integral
                        ENDDO
                    ELSE
                        DO jState=nSourceAndLocalAugState+(m1-nLocalOrb-1)*nParent+1,&
                                  nSourceAndLocalAugState+(m1-nLocalOrb)*nParent
                            lState=stateID2(jState)
                            operatorMatrix(iState,jState)=&
                              operatorMatrix(iState,jState)+&
                              rdm(lState*(lState-1)/2+kState)*integral
                        ENDDO
                    ENDIF
                ENDDO
            ENDIF
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Hermitize"

    DO iState=1,nStateTotal
        DO jState=iState+1,nStateTotal
            operatorMatrix(jState,iState)=operatorMatrix(iState,jState)
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Establish new order"

    ALLOCATE(order(nStateTotal))
    DO iOrb=1,nSource
        stateID1(iOrb)=iOrb
        order(iOrb)=iOrb
    ENDDO
    jOrb=nSource
    DO iOrb=1,occupied-nInactive
        DO iIon=1,nParent
            jOrb=jOrb+1
            order(jOrb)=jOrb
            stateID1(jOrb)=(iIon-1)*occupied+iOrb+nSource
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Sort new order"


    ALLOCATE(tempRealArr2d(nStateTotal,nStateTotal))
    tempRealArr2d=operatorMatrix
    CALL QSortInt(nStateTotal,stateID1,order)

    WRITE(6,'(A)') "Apply new order"
    DO iState=1,nStateTotal
        DO jState=1,nStateTotal
            operatorMatrix(iState,jState)=tempRealArr2d(order(iState),order(jState))
        ENDDO
    ENDDO
    DEALLOCATE(tempRealArr2d)

    WRITE(6,'(A)') "Write data to disk"

    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,file=trim(outputFile),status="replace")
        WRITE(1,'(I0)') 1
        WRITE(1,'(I0)') nStateTotal
        WRITE(fmt,'(A,I0,A)') "(",nStateTotal,"(x,E30.20))"
        DO iState=1,nStateTotal
            WRITE(1,trim(fmt)) (operatorMatrix(jState,iState),jState=1,nStateTotal)
        ENDDO
    ELSE
        OPEN(1,file=trim(outputFile),status="replace",form="unformatted")
        WRITE(1) 1
        WRITE(1) nStateTotal
        DO iState=1,nStateTotal
            WRITE(1) (operatorMatrix(jState,iState),jState=1,nStateTotal)
        ENDDO
    ENDIF
    WRITE(6,'(A)') "Happy"

END PROGRAM

SUBROUTINE posMax2Val(ind,local,m1,m2)
    IMPLICIT none
    INTEGER, INTENT(inout) :: ind(4)
    INTEGER, INTENT(in) :: local
    INTEGER :: p1(1),p2(1)
    INTEGER,INTENT(out) :: m1,m2
    INTEGER :: indT(4)

    indT=ind
    m1=MAXVAL(indT)
    p1=MINLOC(ABS(indT-m1))
    indT(p1(1))=0
    m2=MAXVAL(indT)
    p2=MINLOC(ABS(indT-m2))
    IF (m1.le.local) THEN
        RETURN
    ELSEIF (m2.le.local) THEN
        ind(p1(1))=local+1
    ELSE
        IF (m1.eq.m2) THEN
            ind(p1(1))=local+1
            ind(p2(1))=local+1
        ELSE
            ind(p1(1))=local+2
            ind(p2(1))=local+1
        ENDIF
    ENDIF
END SUBROUTINE

SUBROUTINE getPerms(iBas,jBas,kBas,lBas,nPm,pm) 
    IMPLICIT none
    INTEGER,INTENT(in) :: iBas,jBas,kBas,lBas
    INTEGER, INTENT(out) :: nPm
    INTEGER, INTENT(inout) :: pm(4,8)
    pm(1,1)=iBas;pm(2,1)=jBas;pm(3,1)=kBas;pm(4,1)=lBas
    nPm=1
    IF ((iBas.eq.jBas).AND.(kBas.eq.lBas).AND.(iBas.ne.kBas)) THEN
        pm(1,2)=kBas;pm(2,2)=lBas;pm(3,2)=iBas;pm(4,2)=jBas
        nPm=2
    ELSEIF ((iBas.ne.jBas).AND.(kbas.eq.lBas)) THEN
        pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
        pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
        pm(1,4)=kBas;pm(2,4)=lBas;pm(3,4)=jBas;pm(4,4)=iBas
        nPm=4
    ELSEIF ((iBas.eq.jBas).AND.(kbas.ne.lBas)) THEN
        pm(1,2)=iBas;pm(2,2)=jBas;pm(3,2)=lBas;pm(4,2)=kBas
        pm(1,3)=kBas;pm(2,3)=lBas;pm(3,3)=iBas;pm(4,3)=jBas
        pm(1,4)=lBas;pm(2,4)=kBas;pm(3,4)=iBas;pm(4,4)=jBas
        nPm=4
    ELSEIF ((iBas.ne.jBas).AND.(kBas.ne.lBas)) THEN
        pm(1,2)=jBas;pm(2,2)=iBas;pm(3,2)=kBas;pm(4,2)=lBas
        pm(1,3)=iBas;pm(2,3)=jBas;pm(3,3)=lBas;pm(4,3)=kBas
        pm(1,4)=jBas;pm(2,4)=iBas;pm(3,4)=lBas;pm(4,4)=kBas
        nPm=4
        IF (.NOT.(((iBas.eq.kBas).AND.(jBas.eq.lBas)).OR.((iBas.eq.lBas).AND.(jBas.eq.kBas)))) THEN
            pm(1,5)=kBas;pm(2,5)=lBas;pm(3,5)=iBas;pm(4,5)=jBas
            pm(1,6)=kBas;pm(2,6)=lBas;pm(3,6)=jBas;pm(4,6)=iBas
            pm(1,7)=lBas;pm(2,7)=kBas;pm(3,7)=iBas;pm(4,7)=jBas
            pm(1,8)=lBas;pm(2,8)=kBas;pm(3,8)=jBas;pm(4,8)=iBas
            nPm=8
        ENDIF
    ENDIF
END SUBROUTINE
