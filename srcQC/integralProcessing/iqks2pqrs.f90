!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

!This needs to get a lot more efficient still
!either find way to eliminate more orbitals
!or make it faster somehow in loop part
    USE omp_lib
    USE orbitalIO
    USE twoIntIO
    USE stopWatch
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(orbital_) :: orb
    TYPE(twoInt_) :: twoInt
    
    INTEGER ::occupied,local,nBas
    INTEGER :: iBas,kBas,jBas
    INTEGER :: jOrb,lOrb,kOrb,iOrb
    INTEGER :: iCache,nCache
    INTEGER :: printFreq,lLow
    LOGICAL :: allIntegralsRead

    INTEGER :: counter,counter2

    INTEGER, ALLOCATABLE :: indexCache(:,:)
    REAL*8, ALLOCATABLE :: integralCache(:)

    REAL*8 :: twoElBas,thrCt
    REAL*8, ALLOCATABLE :: oM(:,:)
    REAL*8, ALLOCATABLE :: iqrs1(:,:,:,:),pqrs1(:,:,:,:)
    REAL*8, ALLOCATABLE :: iqrs2(:,:,:,:),pqrs2(:,:,:,:)
    REAL*8, ALLOCATABLE :: tempVec(:)
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: iqksFile,ijrsFile
    CHARACTER*200 :: outegralFile
    CHARACTER*200 :: token
    CHARACTER*200 :: outputFormat

    INTEGER*4 :: nCPU

    NAMELIST /INPUT/        orbitalFile,&
      occupied,&
      iqksFile,&
      ijrsFile,&
      outegralFile,&
      local,&
      outputFormat,&
      printFreq,&
      nCPU,&
      thrCt
    
    outputFormat="b"
    local=0                                
    printFreq=1e6
    nCPU=0
    thrCt=1e-16

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
   

    WRITE(6,'(A)') "Loading Orbitals"
    CALL orb%readOrbital(orbitalFile,"b") 
    CALL orb%getNOrb(nBas)
    WRITE(6,'(A,I0)') "Basis fcts in orbital file: ",nBas

   
    WRITE(6,'(A)') "Allocating Memory"
    ALLOCATE(oM(nBas,occupied))
    ALLOCATE(iqrs1(nBas,local,occupied,local))
    ALLOCATE(iqrs2(nBas,occupied,local,local))
    iqrs1=0.d0
    iqrs2=0.d0
   
    WRITE(6,'(A)') "Preparing Oribtals"
    DO jOrb=1,occupied
        CALL orb%getOrbital(tempVec,jOrb)
        oM(:,jOrb)=tempVec
    ENDDO
    CALL orb%destroy()

    IF (nCPU.eq.0) THEN
        nCPU=OMP_get_max_threads()  
        WRITE(6,'(A,I0)') "Set number of CPUs to maximum = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ELSE
        WRITE(6,'(A,I0)') "Use userdefined number of CPUs = ",nCPU
        CALL omp_set_num_threads(nCPU)
    ENDIF

    OPEN(1,file=TRIM(iqksFile),FORM="unformatted",STATUS="old")
    READ(1) 
    counter=0
    counter2=0
        
    WRITE(6,'(A)') "Allocating cache"
    nCache=printFreq
    ALLOCATE(indexCache(4,nCache))
    ALLOCATE(integralCache(nCache))

    allIntegralsRead=.FALSE.
    WRITE(6,'(A)') "Create iqrs integrals from iqks file"
    CALL timer%initialize()
    DO
        DO iCache=1,printFreq 
            READ(1,IOSTAT=stat) iBas,jOrb,kBas,lOrb,twoElBas
            IF (isnan(twoElBas)) then
                write(6,"(4(A,I0))") "XCHEM_WARNING NaN integral for the term bas ",iBas," orb ",jOrb," bas ",kBas," orb ",lOrb
            ENDIF
            IF (stat.ne.0) THEN
                nCache=counter2
                allIntegralsRead=.TRUE.
                EXIT
            ENDIF
            counter2=counter2+1
            indexCache(1,iCache)=iBas
            indexCache(2,iCache)=jOrb
            indexCache(3,iCache)=kBas
            indexCache(4,iCache)=lOrb
            integralCache(iCache)=twoElBas
        ENDDO
        counter=counter+counter2
        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP PRIVATE(kOrb) &
        !$OMP SHARED (occupied,oM,iqrs1,iCache,nCache,integralCache,indexCache)
        !$OMP DO
        DO kOrb=1,occupied
            DO iCache=1,nCache
                iqrs1(indexCache(1,iCache),indexCache(2,iCache),kOrb,indexCache(4,iCache))=&
                    iqrs1(indexCache(1,iCache),indexCache(2,iCache),kOrb,indexCache(4,iCache))+&
                    integralCache(iCache)*oM(indexCache(3,iCache),kOrb)
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL
        WRITE(token,'(A,I12,A)') "Basis integral record ",counter," processed"
        counter2=0
        CALL timer%time(TRIM(token))
        IF (allIntegralsRead) EXIT
    ENDDO
    CLOSE(1)

    OPEN(1,file=TRIM(ijrsFile),FORM="unformatted",STATUS="old")
    READ(1) 
    counter=0
    counter2=0
    allIntegralsRead=.FALSE.
    nCache=printFreq
    WRITE(6,'(A)') "Create iqrs integrals from ijrs file"
    DO
        DO iCache=1,printFreq
            READ(1,IOSTAT=stat) iBas,jBas,kOrb,lOrb,twoElBas
            IF (isnan(twoElBas)) then
                write(6,"(4(A,I0))") "XCHEM_WARNING NaN integral for the term bas ",iBas," bas ",jBas," orb ",kOrb," orb ",lOrb
            ENDIF
            IF (stat.ne.0) THEN
                nCache=counter2
                allIntegralsRead=.TRUE.
                EXIT
            ENDIF
            counter2=counter2+1
            indexCache(1,iCache)=iBas
            indexCache(2,iCache)=jBas
            indexCache(3,iCache)=kOrb
            indexCache(4,iCache)=lOrb
            integralCache(iCache)=twoElBas
        ENDDO
        counter=counter+counter2
        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP PRIVATE(jOrb) &
        !$OMP SHARED (occupied,oM,iqrs2,iCache,nCache,integralCache,indexCache)
        !$OMP DO
        DO jOrb=1,occupied
            DO iCache=1,nCache
                iqrs2(indexCache(1,iCache),jOrb,indexCache(3,iCache),indexCache(4,iCache))=&
                    iqrs2(indexCache(1,iCache),jOrb,indexCache(3,iCache),indexCache(4,iCache))+&
                    integralCache(iCache)*oM(indexCache(2,iCache),jOrb)
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL
        WRITE(token,'(A,I12,A)') "Basis integral record ",counter," processed"
        counter2=0
        CALL timer%time(TRIM(token))
        IF (allIntegralsRead) EXIT
    ENDDO
    CLOSE(1)

    WRITE(6,'(A)') "Create pqrs integrals from iqrs"
    ALLOCATE(pqrs1(occupied,local,occupied,local))
    ALLOCATE(pqrs2(occupied,occupied,local,local))
    pqrs1=0.d0
    pqrs2=0.d0

    !$OMP PARALLEL &
    !$OMP DEFAULT(none) &
    !$OMP PRIVATE (iOrb,jOrb,kOrb,lOrb,iBas) &
    !$OMP SHARED(local,occupied,nBas,iqrs1,iqrs2,oM,pqrs1,pqrs2)
    !$OMP DO
    DO iOrb=1,occupied
        DO jOrb=1,local
            DO kOrb=1,occupied
                DO lOrb=1,local
                    DO iBas=1,nBas
                        pqrs1(iOrb,jOrb,kOrb,lOrb)=pqrs1(iOrb,jOrb,kOrb,lOrb)+oM(iBas,iOrb)*iqrs1(iBas,jOrb,kOrb,lOrb)
                    ENDDO
                ENDDO
            ENDDO
        ENDDO
        DO jOrb=1,occupied
            DO kOrb=1,local
                DO lOrb=1,local
                    DO iBas=1,nBas
                        pqrs2(iOrb,jOrb,kOrb,lOrb)=pqrs2(iOrb,jOrb,kOrb,lOrb)+oM(iBas,iOrb)*iqrs2(iBas,jOrb,kOrb,lOrb)
                    ENDDO
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    !$OMP END DO
    !$OMP END PARALLEL

    DEALLOCATE(iqrs1,iqrs2)
     
    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,file=TRIM(outegralFile),STATUS="replace")
        WRITE(1,'(4(I0,x))' ) occupied,local,occupied,local
    ELSE 
        OPEN(1,file=TRIM(outegralFile),STATUS="replace",FORM="unformatted")
        WRITE(1) occupied,local,occupied,local
    ENDIF

    !DO iOrb=1,occupied
    !    DO jOrb=1,local
    !        DO kOrb=1,occupied
    !            DO lOrb=1,local
    !                IF (ABS(pqrs(iOrb,jOrb,kOrb,lOrb)).gt.1e-14) THEN
    !                    IF (outputFormat(1:1).eq."f") THEN
    !                        WRITE(1,'(4(I0,x),E18.10)') iOrb,jOrb,kOrb,lOrb,pqrs(iOrb,jOrb,kOrb,lOrb)
    !                    ELSE
    !                        WRITE(1) iOrb,jOrb,kOrb,lOrb,pqrs(iOrb,jOrb,kOrb,lOrb)
    !                    ENDIF
    !                ENDIF
    !            ENDDO
    !        ENDDO
    !    ENDDO
    !ENDDO
    counter=0
    DO iOrb=1,local
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb) 
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied   
                    IF ((jOrb.gt.local).AND.(kOrb.gt.local).AND.(lOrb.gt.local)) CYCLE
                    IF ((kOrb.le.local).OR.(lOrb.le.local)) THEN
                        IF (kOrb.le.lOrb) THEN  
                            IF (ABS(pqrs1(jOrb,iOrb,lOrb,kOrb)).gt.thrCt) THEN
                                counter=counter+1
                                IF (outputFormat(1:1).eq."f") THEN
                                    WRITE(1,'(4(I0,x),E24.16)') iOrb,jOrb,kOrb,lOrb,pqrs1(jOrb,iOrb,lOrb,kOrb)
                                ELSE
                                    WRITE(1) iOrb,jOrb,kOrb,lOrb,pqrs1(jOrb,iOrb,lOrb,kOrb)
                                ENDIF
                            ENDIF
                        ELSE
                            IF (ABS(pqrs1(jOrb,iOrb,kOrb,lOrb)).gt.thrCt) THEN
                                counter=counter+1
                                IF (outputFormat(1:1).eq."f") THEN
                                    WRITE(1,'(4(I0,x),E24.16)') iOrb,jOrb,kOrb,lOrb,pqrs1(jOrb,iOrb,kOrb,lOrb)
                                ELSE
                                    WRITE(1) iOrb,jOrb,kOrb,lOrb,pqrs1(jOrb,iOrb,kOrb,lOrb)
                                ENDIF
                            ENDIF
                        ENDIF
                    ELSE
                        IF (ABS(pqrs2(kOrb,lOrb,iOrb,jOrb)).gt.thrCt) THEN
                            counter=counter+1
                            IF (outputFormat(1:1).eq."f") THEN  
                                WRITE(1,'(4(I0,x),E24.16)') iOrb,jOrb,kOrb,lOrb,pqrs2(kOrb,lOrb,iOrb,jOrb)
                            ELSE
                                WRITE(1) iOrb,jOrb,kOrb,lOrb,pqrs2(kOrb,lOrb,iOrb,jOrb)
                            ENDIF
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    write(6,"(A,I0)") "Number of non-zero integrals ",counter
    WRITE(6,'(A)') "Happy"
END PROGRAM
