!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program checkOvOrb

USE orbitalIO

implicit none
real*8 kk
TYPE(orbital_) :: orb1,orb2
character*200 ovFile,OrbFile1,OrbFile2
integer nSym,nBasAll,iBas,jBas
real*8, allocatable :: ovBas(:,:),OrbMat1(:,:),OrbMat2(:,:),tmp1(:,:)

read(*,*) ovFile
read(*,*) OrbFile1
read(*,*) OrbFile2
OPEN(1,FILE=TRIM(ovFile),STATUS="old",FORM="unformatted")
READ(1) nSym
IF (nSym.ne.1) THEN 
    WRITE(6,'(A)') "ERROR: this code can only handel one sym for the moment"
    STOP "ERROR: this code can only handel one sym for the moment"
ENDIF
READ(1) nBasAll
ALLOCATE(ovBas(nBasAll,nBasAll))
DO iBas=1,nBasAll
    READ(1) (ovBas(jBas,iBas),jBas=1,iBas)
    DO jBas=1,iBas-1
        ovBas(iBas,jBas)=ovBas(jBas,iBas)
    ENDDO
ENDDO
CLOSE(1)
WRITE(6,'(A)') "  ______  "
WRITE(6,'(A)') ""

CALL orb1%readOrbital(TRIM(OrbFile1),'b')
CALL orb2%readOrbital(TRIM(OrbFile2),'b')
CALL orb1%getOrbitalMat(OrbMat1)
CALL orb2%getOrbitalMat(OrbMat2)
call orb1%destroy() 
call orb2%destroy() 

allocate(tmp1(nBasAll,size(OrbMat2,2)))
tmp1 = matmul(OvBas,OrbMat2)
deallocate(OvBas)
allocate(OvBas(size(OrbMat1,2),size(OrbMat2,2)))
OvBas=matmul(transpose(OrbMat1),tmp1)


DO iBas=1,size(OrbMat1,2)
  kk=0.0
  DO jBas=1,size(OrbMat2,2)
    kk = kk + OvBas(iBas,jbas)**2
  ENDDO
  write(*,*) ibas,kk,OvBas(iBas,iBas)
  !write(*,*) 'Max/min Overlap for orbital', iBas, maxval(OvBas(iBas,:)),maxloc(OvBas(iBas,:)),minval(OvBas(iBas,:)),minloc(OvBas(iBas,:))
ENDDO
end program
