!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM linDepKill
!CODE THAT REMOVES LINEAR DEPENDENCIES FROM A BASIS SET
!COMPOSED OF A LOCAL PART THAT WILL REMAIN UNTOUCHED
!AND AN ADDED PART THAT WILL GET FUCKED WITH (i.e. MONOCENTRIC BASIS)
    USE blockIO
    USE orbitalIO
    USE diagonalizations
    USE basisIO
    USE matmat
    USE lkIO
    IMPLICIT none
    !VARIABLES
    !SYSTEM VARS
    INTEGER :: stat
    LOGICAL :: exists,debug
    CHARACTER*200 :: inputFile

    !INPUT VARS
    CHARACTER*200 :: ovFile
    CHARACTER*200 :: locOrbFile
    CHARACTER*200 :: linIndepOrbFile
    CHARACTER*200 :: blockInfoFile
    CHARACTER*200 :: orbSymFile
    CHARACTER*200 :: diagMethod
    CHARACTER*200 :: excludedBlocksFile
    CHARACTER*200 :: lkMonocentricFile
    REAL*8 :: linDepThr
    INTEGER :: nOrbLoc
    INTEGER :: lMax
    INTEGER :: kMax
    LOGICAL :: asymLocOrb
    LOGICAL :: singleBasisSet
    LOGICAL :: dynamicThr
    LOGICAL :: multiGhost
    LOGICAL :: basisCreate
    LOGICAL :: printFmtOrbs
    LOGICAL :: printBinOrbs
    LOGICAL :: renormalize
    LOGICAL :: ONLoc
    LOGICAL :: binaryLocOrb
    INTEGER :: onlyK,onlyM,onlyL
    
    !STRUCUTRAL VARS
    INTEGER :: nIrrep
    INTEGER :: nSym
    INTEGER :: nBasAll
    INTEGER :: nBasLoc
    INTEGER :: nOrbLocSym
    INTEGER :: nOrbLI
    INTEGER :: nOrbLISym
    INTEGER :: nOrbMonLD
    INTEGER :: nOrbMonLDSym
    INTEGER :: nOrbMonLISym
    INTEGER :: nBlock
    INTEGER :: nExcludedBlock
    INTEGER :: orbCount
    TYPE(lk_) :: lkPairs

    REAL*8 :: error
    REAL*8 :: thrScale 
    CHARACTER*6 :: irrep(8)
    CHARACTER*6 :: pntGrp

    INTEGER, ALLOCATABLE :: orbLocSym(:)
    INTEGER, ALLOCATABLE :: excludedBlocks(:,:)

    REAL*8, ALLOCATABLE :: orbLoc(:,:)
    REAL*8, ALLOCATABLE :: orbLocT(:,:)
    REAL*8, ALLOCATABLE :: orbMonLD(:,:)
    REAL*8, ALLOCATABLE :: orbMonLI(:,:)
    REAL*8, ALLOCATABLE :: orbLI(:,:)
    REAL*8, ALLOCATABLE :: orbTrunc(:,:)
    REAL*8, ALLOCATABLE :: orbLILoc(:,:)
    REAL*8, ALLOCATABLE :: orbLIMon(:,:)

    REAL*8, ALLOCATABLE :: ovBas(:,:)
    REAL*8, ALLOCATABLE :: ovOrbLocLoc(:,:)
    REAL*8, ALLOCATABLE :: ovOrbMonLDMonLD(:,:)
    REAL*8, ALLOCATABLE :: ovOrbLILI(:,:)
    REAL*8, ALLOCATABLE :: ovOrbLILILoc(:,:)
    REAL*8, ALLOCATABLE :: ovOrbLILIMon(:,:)
    REAL*8, ALLOCATABLE :: ovOrbLocMonLD(:,:)

    REAL*8, ALLOCATABLE :: ULX(:,:)
    REAL*8, ALLOCATABLE :: UMX(:,:)
    REAL*8, ALLOCATABLE :: A(:,:)
    REAL*8, ALLOCATABLE :: O(:,:)
    REAL*8, ALLOCATABLE :: Lambda(:,:)
    REAL*8, ALLOCATABLE :: eVal(:)

    TYPE(block_), ALLOCATABLE :: block(:)
    TYPE(orbital_) :: orb

    !BASIS CREATE VARS
    INTEGER :: kT,lT
    INTEGER :: nContraction(20),basisBlock(20)
    INTEGER :: basisL
    INTEGER :: offsetB,offsetC
    INTEGER :: nPrimitive
    TYPE(basis_) :: basis

    !TEMP VARS
    CHARACTER*50 :: label
    CHARACTER*200 :: token
    CHARACTER*200 :: fmt
    INTEGER :: ii(2)
    INTEGER :: iSym
    INTEGER :: i,j,k,l,m
    INTEGER :: iBlock,jBlock
    INTEGER :: iBas,jBas
    INTEGER :: iOrb,jOrb
    INTEGER :: iGhost
    INTEGER :: tempInt
    LOGICAL :: relevant
    LOGICAL :: printOrbSym
    REAL*8, ALLOCATABLE :: tempRealArr(:,:)
    REAL*8, ALLOCATABLE :: tempRealArr2(:,:)
    REAL*8, ALLOCATABLE :: tempRealArr3(:,:)

    !DEFINE INPUT NAMELIST
    NAMELIST /INPUT/ ovFile,&
        locOrbFile,&
        linDepThr,&
        linIndepOrbFile,&
        nOrbLoc,&
        blockInfoFile,&
        lMax,&
        kMax,&
        diagMethod,&
        asymLocOrb,&
        debug,&
        onlyK,&
        onlyL,&
        onlyM,&
        singleBasisSet,&
        dynamicThr,&
        linDepThr,&
        multiGhost,&
        basisCreate,&
        orbSymFile,&
        excludedBlocksFile,&
        printFmtOrbs,&
        printBinOrbs,&
        renormalize,&
        ONLoc,&
        lkMonocentricFile,&
        binaryLocOrb

    binaryLocOrb=.TRUE.
    orbSymFile=""
    excludedBlocksFile=""
    singleBasisSet=.FALSE.
    multiGhost=.FALSE.
    dynamicThr=.TRUE.
    basisCreate=.FALSE.
    onlyK=9999
    onlyL=9999
    onlyM=9999

    nBlock=0
    nOrbLoc=0
    asymLocOrb=.FALSE.
    lMax=-1
    kMax=-1
    debug=.FALSE.
    blockInfoFile=""
    ovFile=""
    locOrbFile=""
    linIndepOrbFile="linIndepOrb.bin"
    linDepThr=0.d0
    diagMethod="lapack"
    renormalize=.true.
    ONLoc=.true.
    lkMonocentricFile=""

    printFmtOrbs=.true.
    printBinOrbs=.true.

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
   
    WRITE(6,'(A)') "RESTRICTION INFO"
    IF (len(trim(lkMonocentricFile)).gt.0) THEN 
        WRITE(6,'(A)') "Read from file"
        call lkPairs%create(trim(lkMonocentricFile))
    ELSE
        IF (lMax.lt.0) STOP "negative lMax make no sense"
        IF (kMax.lt.0) STOP "negative kMax make no sense"
        WRITE(6,'(A,I3,2x,A,I3)') "lMax=",lMax,"kMax=",kMax
        call lkPairs%create(lMax,kMax)
    ENDIF
    WRITE(6,'(A)') " "

    IF (LEN(TRIM(orbSymFile)).eq.0) THEN
        printOrbSym=.FALSE.
    ELSE
        printOrbSym=.TRUE.
        OPEN(701,FILE=TRIM(orbSymFile),STATUS="replace")
    ENDIF

    !Read overlap
    WRITE(6,'(A)') "Read overlap"
    IF (LEN(TRIM(ovFile)).eq.0) THEN
        WRITE(6,'(A)') "ERROR: overlap file not provided"
        STOP "ERROR: overlap file not provided"
    ENDIF
    OPEN(1,FILE=TRIM(ovFile),STATUS="old",FORM="unformatted")
    READ(1) nSym
    IF (nSym.ne.1) THEN 
        WRITE(6,'(A)') "ERROR: this code can only handel one sym for the moment"
        STOP "ERROR: this code can only handel one sym for the moment"
    ENDIF
    READ(1) nBasAll
    ALLOCATE(ovBas(nBasAll,nBasAll))
    DO iBas=1,nBasAll
        READ(1) (ovBas(jBas,iBas),jBas=1,iBas)
        DO jBas=1,iBas-1
            ovBas(iBas,jBas)=ovBas(jBas,iBas)
        ENDDO
    ENDDO
    CLOSE(1)
    WRITE(6,'(A)') "  ______  "
    WRITE(6,'(A)') ""

    !Read local orbitals
    IF (nOrbLoc.ne.0) THEN
        WRITE(6,'(A)') "Read local orbitals"
        IF (LEN(TRIM(locOrbFile)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: local orbtital file not provided"
            STOP "ERROR: local orbtital file not provided"
        ENDIF
        IF (binaryLocOrb) THEN
            OPEN(1,FILE=TRIM(locOrbFile),STATUS="old",FORM="unformatted")
            READ(1) nSym
            READ(1) nBasLoc
            ALLOCATE(tempRealArr(nBasLoc,nBasLoc))
            ALLOCATE(orbLoc(nBasAll,nOrbLoc))
            tempRealArr=0.d0
            orbLoc=0.d0
            READ(1) ((tempRealArr(iBas,iOrb),iBas=1,nBasLoc),iOrb=1,nBasLoc)
            orbLoc(1:nBasLoc,:)=tempRealArr(:,1:nOrbLoc)
            DEALLOCATE(tempRealArr)
            !!!!! Orthonormalizing local orbitals
        ELSE
            CALL orb%readOrbital(TRIM(locOrbFile),'f')
            CALL orb%getOrbitalMat(tempRealArr)
            CALL orb%getSymSize(nBasLoc)
            ALLOCATE(orbLoc(nBasAll,nOrbLoc))
            orbLoc = 0.d0
            orbLoc(1:nBasLoc,:)=tempRealArr(:,1:nOrbLoc)
            DEALLOCATE(tempRealArr)
            call orb%destroy()
        ENDIF
    ELSE
        nBasLoc=0
        WRITE(6,'(A)') "no Local Orbitals"
    ENDIF
    CLOSE(1)

    !Read list of exculded Guassian Blocks (if given)
    IF (LEN(TRIM(excludedBlocksFile)).ne.0) THEN
        WRITE(6,'(A)') "Read list of exculded Guassian Blocks"
        OPEN(1,file=trim(excludedBlocksFile),status="old")
        READ(1,*) nExcludedBlock
        ALLOCATE(excludedBlocks(nExcludedBlock,3))
        DO iBlock=1,nExcludedBlock
            READ(1,*) (excludedBlocks(iBlock,i),i=1,3)
        ENDDO
    ELSE
        nExcludedBlock=0
    ENDIF
    CLOSE(1)

    !Read information about blocks
    ALLOCATE(orbLocSym(nOrbLoc))
    WRITE(6,'(A)') "Read information about blocks and local orbs"
    IF (LEN(blockInfoFile).eq.0) THEN
        STOP "ERROR: blockInfoFile is required"
    ENDIF
    OPEN(1,FILE=TRIM(blockInfoFile),STATUS="old")
    READ(1,*) nIrrep,pntGrp
    print*, nIrrep,pntGrp
    READ(1,*) (irrep(iSym),iSym=1,nIrrep)
    print*,(irrep(iSym),iSym=1,nIrrep)
    READ(1,*) (orbLocSym(iOrb),iOrb=1,nOrbLoc)
    READ(1,*) nBlock
    WRITE(6,'(A,I0)') "Number of blocks= ",nBlock

    IF (printOrbSym) THEN
        WRITE(6,'(A)') "Orbital Symmetry File will be written"
        DO iOrb=1,nOrbLoc
            WRITE(701,'(A6)') irrep(orbLocSym(iOrb))
        ENDDO
    ENDIF

    ALLOCATE(block(nBlock))
    nOrbMonLD=0
    IF (singleBasisSet) THEN
        tempInt=0
    ELSE
        tempInt=nBasLoc
    ENDIF
    iGhost=0
    DO iBlock=1,nBlock
        READ(1,*) iBas,label,l,m,k,iGhost,iSym
        IF (asymLocOrb) THEN
            CALL block(iBlock)%create(k,l,m,iBas,tempInt,label,1,iGhost)
        ELSE
            CALL block(iBlock)%create(k,l,m,iBas,tempInt,label,iSym,iGhost)
        ENDIF
        tempInt=tempInt+iBas

        relevant=.TRUE.
        !IF (l.gt.lMax.OR.k.gt.kMax) relevant=.FALSE.  
        IF (.not.lkPairs%is_lk(l,k)) relevant=.FALSE.
        IF (onlyK.ne.9999.AND.onlyK.ne.k) relevant=.FALSE. 
        IF (onlyL.ne.9999.AND.onlyL.ne.l) relevant=.FALSE.
        IF (onlyM.ne.9999.AND.onlyM.ne.m) relevant=.FALSE.
        DO jBlock=1,nExcludedBlock  
            IF (l.eq.excludedBlocks(jBlock,1).AND. &
                m.eq.excludedBlocks(jBlock,2).AND. &
                k.eq.excludedBlocks(jBlock,3)) THEN
                relevant=.FALSE.
            ENDIF
        ENDDO   
        IF (multiGhost) THEN
            IF (iGhost.ne.l) THEN
                relevant=.FALSE.
            ENDIF
        ENDIF
        IF (relevant) THEN
            nOrbMonLD=nOrbMonLD+iBas
        ELSE
            CALL block(iBlock)%remove()
        ENDIF
        CALL block(iBlock)%printInfo()
    ENDDO

    CLOSE(1)
    WRITE(6,'(A)') "  ______  "
    WRITE(6,'(A)') ""

    WRITE(6,'(A)') "Overlap and Local Orbitals read and prepared for lin dep killing"
    WRITE(6,'(A,I0)') "local Basis Fcts= ",nBasLoc
    WRITE(6,'(A,I0)') "total Basis Fcts= ",nBasAll
    WRITE(6,'(A,I0)') "local Orbitals= ",nOrbLoc
    WRITE(6,'(A,I0)') "raw monocentric Orbitals= ",nOrbMonLD

    !!!!orbLoc(41,5)=0.497458214091E-03 !4f1+
    !!!!orbLoc(42,5)=0.133844065302E-02 !4f1+
    !!!!orbLoc(83,5)=0.272831472905E-03 !4f1+ 
    !!!!orbLoc(113,5)=0.272831472905E-03 !4f1+
    IF (nOrbLoc.gt.0) THEN
        ALLOCATE(ovOrbLocLoc(nOrbLoc,nOrbLoc))
        ovOrbLocLoc=0.d0
        ALLOCATE(tempRealArr(nBasAll,nOrbLoc))
        tempRealArr=0.d0
        call dmatmat(ovBas, orbLoc, 'n', 'n',tempRealArr)
        call dmatmat(orbLoc,tempRealArr,'t','n',ovOrbLocLoc)
        DEALLOCATE(tempRealArr)
        ALLOCATE(tempRealArr(nOrbLoc,nOrbLoc))
        tempRealArr = ovOrbLocLoc
        WRITE(6,'(A)') "Diagonal of Local Overlaps"
        DO iOrb=1,nOrbLoc
            WRITE(6,'(E18.12)') tempRealArr(iOrb,iOrb)
            tempRealArr(iOrb,iOrb)=0.d0
        ENDDO
        ii=MAXLOC(ABS(tempRealArr))
        i=ii(1)
        j=ii(2)
        error=ovOrbLocLoc(i,j)
        WRITE(6,'(A,E18.12,A,2(x,I0))') "largest deviation in local overlap= ",error," for orbitals: ",i,j
        DEALLOCATE(tempRealArr)


        !!! Orthonormalizing
        ALLOCATE(eVal(nOrbLoc),tempRealArr(nOrbLoc,norbloc),tempRealArr2(norbloc,norbloc))
        tempRealArr=ovOrbLocLoc
        CALL diag(tempRealArr,nOrbLoc,eVal)
        ovorblocloc=0.
        do iorb=1,norbloc
         ovorblocloc(iorb,iorb)=eval(iorb)**(-.5)
        enddo
        !!! S^(-1./2.)
        call dmatmat(tempRealArr,ovorblocloc,'n','n',tempRealArr2)
        call dmatmat(tempRealArr2,tempRealArr,'n','t',ovorblocloc)
        deallocate(tempRealArr,tempRealArr2,eVal)
        ALLOCATE(tempRealArr(nBasAll,norbloc))
        call dmatmat(orbloc,ovorblocloc,'n','t',tempRealArr)
        !!! New orbitals
        orbloc=tempRealArr
        deallocate(tempRealArr)
        write(6,*) "Lowdin orthonormalization done"

        ovOrbLocLoc=0.d0
        ALLOCATE(tempRealArr(nBasAll,nOrbLoc))
        tempRealArr=0.d0
        call dmatmat(ovBas, orbLoc, 'n', 'n',tempRealArr)
        call dmatmat(orbLoc,tempRealArr,'t','n',ovOrbLocLoc)
        DEALLOCATE(tempRealArr)
        ALLOCATE(tempRealArr(nOrbLoc,nOrbLoc))
        tempRealArr = ovOrbLocLoc
        WRITE(6,'(A)') "Diagonal of Local Overlaps"
        DO iOrb=1,nOrbLoc
            WRITE(6,'(E18.12)') tempRealArr(iOrb,iOrb)
            tempRealArr(iOrb,iOrb)=0.d0
        ENDDO
        ii=MAXLOC(ABS(tempRealArr))
        i=ii(1)
        j=ii(2)
        error=ovOrbLocLoc(i,j)
        WRITE(6,'(A,E18.12,A,2(x,I0))') "largest deviation in local overlap= ",error," for orbitals: ",i,j
        DEALLOCATE(tempRealArr)

    ENDIF

    WRITE(6,'(A)') "  ______  "
    WRITE(6,'(A)') ""
    
    WRITE(6,'(A)') "Find largest overlap eigenvalue across all symmetries"
    DO iSym=1,8
        !Set up matrices and calculate orbital overlaps
        nOrbLocSym=COUNT(orbLocSym.eq.iSym)
        nOrbMonLDSym=0
        DO iBlock=1,nBlock
            IF (block(iBlock)%removed().OR.block(iBlock)%getSym().ne.iSym) CYCLE
            nOrbMonLDSym=nOrbMonLDSym+block(iBlock)%getNBas()
        ENDDO
        WRITE(6,'(A,I0,A,2(x,I0))') "  Of symmetry ",iSym," there are: Local Orbtitals, Monocentric Bases Fcts",&
          nOrbLocSym,nOrbMonLDSym
        IF (nOrbMonLDSym.eq.0) THEN
            WRITE(6,'(A)') "  Nothing to be done here. Jump to next Symmetry"
            CYCLE
        ENDIF

        WRITE(6,'(A)') "  Set up matrices and calculate orbital overlaps"
        ALLOCATE(orbMonLD(nBasAll,nOrbMonLDSym))
        ALLOCATE(ovOrbMonLDMonLD(nOrbMonLDSym,nOrbMonLDSym))
        orbMonLD=0.d0
        ovOrbMonLDMonLD=0.d0

        ALLOCATE(A(nOrbMonLDSym,nOrbMonLDSym))
        ALLOCATE(eVal(nOrbMonLDSym))
        ALLOCATE(O(nOrbMonLDSym,nOrbMonLDSym))
        A=0.d0
        O=0.d0
        eVal=0.d0

        IF (nOrbLocSym.ne.0) THEN
            ALLOCATE(ovOrbLocMonLD(nOrbLocSym,nOrbMonLDSym))
            ALLOCATE(orbLocT(nBasAll,nOrbLocSym))
            orbLocT=0.d0
            ovOrbLocMonLD=0.d0
            jOrb=0
            DO iOrb=1,nOrbLoc
                IF (orbLocSym(iOrb).ne.iSym) CYCLE
                jOrb=jOrb+1
                orbLocT(:,jOrb)=orbLoc(:,iOrb)
            ENDDO
        ENDIF

        iOrb=0
        DO iBlock=1,nBlock
            IF (block(iBlock)%removed().OR.block(iBlock)%getSym().ne.iSym) CYCLE
            DO iBas=block(iBlock)%getOffset()+1,block(iBlock)%getOffset()+block(iBlock)%getNBas()
                iOrb=iOrb+1
                orbMonLD(iBas,iOrb)=1.d0
            ENDDO
        ENDDO

        WRITE(6,'(A,I0,A,I0)') "  Size Lin Dep Mono Orbital ",SIZE(orbMonLD,1)," x ",SIZE(orbMonLD,2)
        
        WRITE(6,'(A)') "  Calculate monocentric-monocentric overlap"
        DO iOrb=1,nOrbMonLDSym
            i=MAXLOC(orbMonLD(:,iOrb),1)
            DO jOrb=1,iOrb
                j=MAXLOC(orbMonLD(:,jOrb),1)
                ovOrbMonLDMonLD(iOrb,jOrb)=ovOrbMonLDMonLD(iOrb,jOrb)+ovBas(i,j)
            ENDDO
            DO jOrb=1,iOrb
                ovOrbMonLDMonLD(jOrb,iOrb)=ovOrbMonLDMonLD(iOrb,jOrb)
            ENDDO
        ENDDO

        IF (nOrbLocSym.ne.0) THEN
            WRITE(6,'(A)') "  Calculate monocentric-local overlap"
            ALLOCATE(tempRealArr(nOrbLocSym,nBasAll))
            !tempRealArr=MATMUL(TRANSPOSE(orbLocT),ovBas)
            !ovOrbLocMonLD=MATMUL(tempRealArr,orbMonLD)
            call dmatmat(orbLocT,ovBas,'t','n',tempRealArr)
            call dmatmat(tempRealArr,orbMonLD,'n','n',ovOrbLocMonLD)
            DEALLOCATE(tempRealArr)
        ENDIF

        !Find largest eigenvalue
        WRITE(6,'(A)') "  Find Largest Eigenvalue"
        IF(nOrbLocSym.eq.0) THEN
            A=ovOrbMonLDMonLD
            O=A 
        ELSE
            IF (ONLoc) THEN
                !A=MATMUL(TRANSPOSE(ovOrbLocMonLD),ovOrbLocMonLD)
                call dmatmat(ovOrbLocMonLD,ovOrbLocMonLD,'t','n',A)
                A=ovOrbMonLDMonLD-A
                O=A
            ELSE
                allocate(tempRealArr(nOrbLoc,nOrbMonLDSym))
                tempRealArr =0.d0
                tempRealArr2=0.d0
                tempRealArr3=0.d0
                A = ovOrbMonLDMonLD
                call dmatmat(ovOrbLocMonLD,ovOrbLocMonLD,'t','n',A,alpha=-2.d0,beta=1.d0)
                call dmatmat(ovOrbLocLoc,ovOrbLocMonLD,'n','n',tempRealArr)
                call dmatmat(ovOrbLocMonLD,tempRealArr,'t','n',A,beta=1.d0)
                O=A

                !
                write(6,'(A)') "DEBUG: insert non ON loc ovs"
                allocate(tempRealArr2(nOrbMonLDSym,nOrbMonLDSym))
                allocate(tempRealArr3(nOrbMonLDSym,nOrbMonLDSym))
                !<><I><>
                call dmatmat(ovOrbLocMonLD,ovOrbLocMonLD,'t','n',tempRealArr2)
                !<><S><>
                call dmatmat(ovOrbLocLoc,ovOrbLocMonLD,'n','n',tempRealArr)
                write(6,'(A,E24.16)') "DEBUG max diff test (<I><> vs <>) = ", maxval(abs(tempRealArr-ovOrbLocMonLD))
                call dmatmat(ovOrbLocMonLD,tempRealArr,'t','n',tempRealArr3)
                write(6,'(A,E24.16)') "DEBUG max diff test (<><I><> vs <><S><>) = ", maxval(abs(tempRealArr2-tempRealArr3))
                deallocate(tempRealArr)
                deallocate(tempRealArr2)
                !
                deallocate(tempRealArr3)
            ENDIF
        ENDIF
        WRITE(6,'(A,A)') "  Diagonalize using ",TRIM(diagMethod)
        CALL diag(O,nOrbMonLDSym,eVal,.FALSE.)

        IF (debug) THEN
            WRITE(6,'(A)') "  Eigenvalues:"
            WRITE(fmt,'(A,I0,A)') '(',nOrbMonLDSym,'(x,F9.5))'
            WRITE(6,trim(fmt)) (eVal(iOrb),iOrb=1,nOrbMonLDSym)
        ENDIF
        WRITE(6,'(A,F9.5)') "  Largest eVal: ",MAXVAL(eVal)
        IF (MAXVAL(eVal).gt.thrScale) thrScale=MAXVAL(eVal)
        DEALLOCATE(orbMonLD)
        DEALLOCATE(ovOrbMonLDMonLD)
        DEALLOCATE(A)
        DEALLOCATE(eVal)
        DEALLOCATE(O)
        IF (nOrbLocSym.ne.0) THEN
            DEALLOCATE(orbLocT,ovOrbLocMonLD)
        ENDIF
        WRITE(6,'(A)') ""
    ENDDO

    WRITE(6,'(A,F18.12)') "Largest eigenvalues of A Matrices ",thrScale
    IF (dynamicThr) THEN
        WRITE(6,'(A)') "Dynamic scaling enalbled: adjust threshold by max eigenvalue"
        linDepThr=linDepThr*thrScale
        WRITE(6,'(A,E22.14)') "New threshold = ", linDepThr
    ELSE
        WRITE(6,'(A)') "Dynamic scaling of threshold disabled"
    ENDIF

    WRITE(6,'(A)') "  ______  "
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "Create Linearly Independt Orbitals by Symmetry"
    nOrbLI=nOrbLoc
    ALLOCATE(orbLI(nBasAll,nBasAll))
    DO iOrb=1,nOrbLoc
        orbLI(:,iOrb)=orbLoc(:,iOrb)
    ENDDO
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    orbCount=nOrbLoc
    DO iSym=1,8
        !Set up matrices and calculate orbital overlaps
        nOrbLocSym=COUNT(orbLocSym.eq.iSym)
        nOrbMonLDSym=0
        DO iBlock=1,nBlock
            IF (block(iBlock)%removed().OR.block(iBlock)%getSym().ne.iSym) CYCLE
            nOrbMonLDSym=nOrbMonLDSym+block(iBlock)%getNBas()
        ENDDO
        WRITE(6,'(A,I0,A,2(x,I0))') "  Of symmetry ",iSym," there are: Local Orbtitals, Monocentric Bases Fcts",&
          nOrbLocSym,nOrbMonLDSym
        IF (nOrbMonLDSym.eq.0) THEN
            WRITE(6,'(A)') "  Nothing to be done here. Jump to next Symmetry"
            CYCLE
        ENDIF

        WRITE(6,'(A)') "  Set up matrices and calculate orbital overlaps"
        ALLOCATE(orbMonLD(nBasAll,nOrbMonLDSym))
        ALLOCATE(orbMonLI(nBasAll,nOrbMonLDSym))
        ALLOCATE(ovOrbMonLDMonLD(nOrbMonLDSym,nOrbMonLDSym))
        orbMonLD=0.d0
        orbMonLI=0.d0
        ovOrbMonLDMonLD=0.d0

        ALLOCATE(A(nOrbMonLDSym,nOrbMonLDSym))
        ALLOCATE(eVal(nOrbMonLDSym))
        ALLOCATE(Lambda(nOrbMonLDSym,nOrbMonLDSym))
        ALLOCATE(O(nOrbMonLDSym,nOrbMonLDSym))
        A=0.d0
        O=0.d0
        Lambda=0.d0
        eVal=0.d0

        ALLOCATE(UMX(nOrbMonLDSym,nOrbMonLDSym))
        UMX=0.d0

        IF (nOrbLocSym.ne.0) THEN
            ALLOCATE(ovOrbLocMonLD(nOrbLocSym,nOrbMonLDSym))
            ALLOCATE(ULX(nOrbLocSym,nOrbMonLDSym))
            ALLOCATE(orbLocT(nBasAll,nOrbLocSym))
            orbLocT=0.d0
            ovOrbLocMonLD=0.d0
            ULX=0.d0
            jOrb=0
            DO iOrb=1,nOrbLoc
                IF (orbLocSym(iOrb).ne.iSym) CYCLE
                jOrb=jOrb+1
                orbLocT(:,jOrb)=orbLoc(:,iOrb)
            ENDDO
        ENDIF


        iOrb=0
        DO iBlock=1,nBlock
            IF (block(iBlock)%removed().OR.block(iBlock)%getSym().ne.iSym) CYCLE
            DO iBas=block(iBlock)%getOffset()+1,block(iBlock)%getOffset()+block(iBlock)%getNBas()
                iOrb=iOrb+1
                orbMonLD(iBas,iOrb)=1.d0
            ENDDO
        ENDDO

        WRITE(6,'(A,I0,A,I0)') "  Size Lin Dep Mono Orbital ",SIZE(orbMonLD,1)," x ",SIZE(orbMonLD,2)
        
        WRITE(6,'(A)') "  Calculate monocentric-monocentric overlap"
        DO iOrb=1,nOrbMonLDSym
            i=MAXLOC(orbMonLD(:,iOrb),1)
            DO jOrb=1,iOrb
                j=MAXLOC(orbMonLD(:,jOrb),1)
                ovOrbMonLDMonLD(iOrb,jOrb)=ovOrbMonLDMonLD(iOrb,jOrb)+ovBas(i,j)
            ENDDO
            DO jOrb=1,iOrb
                ovOrbMonLDMonLD(jOrb,iOrb)=ovOrbMonLDMonLD(iOrb,jOrb)
            ENDDO
        ENDDO

        IF (nOrbLocSym.ne.0) THEN
            WRITE(6,'(A)') "  Calculate monocentric-local overlap"
            ALLOCATE(tempRealArr(nOrbLocSym,nBasAll))
            !tempRealArr=MATMUL(TRANSPOSE(orbLocT),ovBas)
            !ovOrbLocMonLD=MATMUL(tempRealArr,orbMonLD)
            call dmatmat(orbLocT,ovBas,'t','n',tempRealArr)
            call dmatmat(tempRealArr,orbMonLD,'n','n',ovOrbLocMonLD)
            DEALLOCATE(tempRealArr)
        ENDIF

        !Create Linearly Indep Monocentric Orbitals
        WRITE(6,'(A)') "  Create Linearly Indep Monocentric Orbitals"
        IF(nOrbLocSym.eq.0) THEN
            A=ovOrbMonLDMonLD
            O=A 
        ELSE
            IF (ONLoc) THEN
                !A=MATMUL(TRANSPOSE(ovOrbLocMonLD),ovOrbLocMonLD)
                call dmatmat(ovOrbLocMonLD,ovOrbLocMonLD,'t','n',A)
                A=ovOrbMonLDMonLD-A
                O=A
            ELSE    
                write(6,'(A)') "DEBUG: insert non ON loc ovs"
                allocate(tempRealArr(nOrbLoc,nOrbMonLDSym))
                A = ovOrbMonLDMonLD
                !A = A - 2 * MATMUL(TRANSPOSE(ovOrbLocMonLD),ovOrbLocMonLD)
                !tempRealArr = MATMUL(ovOrbLocLoc, ovOrbLocMonLD)
                !A = A + MATMUL(TRANSPOSE(ovOrbLocMonLD),tempRealArr)
                call dmatmat(ovOrbLocMonLD,ovOrbLocMonLD,'t','n',A,alpha=-2.d0,beta=1.d0)
                call dmatmat(ovOrbLocLoc,ovOrbLocMonLD,'n','n',tempRealArr)
                call dmatmat(ovOrbLocMonLD,tempRealArr,'t','n',A,beta=1.d0)
                O=A
                deallocate(tempRealArr)
            ENDIF
        ENDIF
        WRITE(6,'(A,A)') "  Diagonalize using ",TRIM(diagMethod)
        CALL diag(O,nOrbMonLDSym,eVal)

        nOrbMonLISym=0
        DO iOrb=1,nOrbMonLDSym
            IF (eVal(iOrb).gt.linDepThr) THEN
                nOrbMonLISym=nOrbMonLISym+1
                orbCount=orbCount+1
                IF (renormalize) THEN
                    Lambda(iOrb,iOrb)=1/sqrt(eVal(iOrb))
                    WRITE(6,'(A,I0,A,E18.12)') "To normalize scale orbital ",orbCount, " by ",sqrt(eVal(iOrb))
                ELSE
                    Lambda(iOrb,iOrb)=1.d0
                ENDIF
            ELSE           
                O(:,iOrb)=0.d0
            ENDIF
        ENDDO
        WRITE(6,'(A,I0)') "  Linearly Idependent Monocentric Orbitals= ",nOrbMonLISym
        nOrbLISym=nOrbMonLISym+nOrbLocSym

        !UMX=MATMUL(O,Lambda)
        call dmatmat(O,Lambda,'n','n',UMX)
        IF (nOrbLocSym.ne.0) THEN
            !ULX=-MATMUL(ovOrbLocMonLD,UMX)
            call dmatmat(ovOrbLocMonLD,UMX,'n','n',ULX,alpha=-1.d0)
        ENDIF

        WRITE(6,'(A)') "  Calculate Lin Indep Orbs = orbLoc*ULX+orbMonLD*UMX - Dimensions:" 
        WRITE(6,'(8(A,I0),A)') "  [",SIZE(orbLocT,1),",",SIZE(orbLocT,2),"]x[",SIZE(ULX,1),",",SIZE(ULX,2),"]+[",&
          SIZE(orbMonLD,1),",",SIZE(orbMonLD,2),"]x[",SIZE(UMX,1),",",SIZE(UMX,2),"]"
   
        IF(nOrbLocSym.eq.0) THEN
            !orbMonLI=MATMUL(orbMonLD,UMX)
            call dmatmat(orbMonLD,UMX,'n','n',orbMonLI)
        ELSE
            !orbMonLI=MATMUL(orbLocT,ULX)+MATMUL(orbMonLD,UMX)
            call dmatmat(orbMonLD,UMX,'n','n',orbMonLI)
            call dmatmat(orbLocT,ULX,'n','n',orbMonLI,beta=1.d0)
        ENDIF

        jOrb=0
        DO iOrb=nOrbMonLDSym-nOrbMonLISym+1,nOrbMonLDSym
            jOrb=jOrb+1
            orbLI(:,jOrb+nOrbLI)=orbMonLI(:,iOrb)
        ENDDO
        nOrbLI=nOrbLI+nOrbMonLISym
    
        IF (printOrbSym) THEN
            DO iOrb=1,nOrbMonLISym
                WRITE(701,'(A6)') irrep(iSym)
            ENDDO
        ENDIF

        DEALLOCATE(orbMonLD)
        DEALLOCATE(orbMonLI)
        DEALLOCATE(ovOrbMonLDMonLD)
        DEALLOCATE(A)
        DEALLOCATE(eVal)
        DEALLOCATE(Lambda)
        DEALLOCATE(O)
        DEALLOCATE(UMX)
        IF (nOrbLocSym.ne.0) THEN
            DEALLOCATE(orbLocT,ovOrbLocMonLD,ULX)
        ENDIF
        WRITE(6,'(A)')
    ENDDO
    WRITE(6,'(A)') "Linear dependencies removed for all symmetries"
    !Write linearly independent orbitals
    WRITE(6,'(A)') "Write Orbitals"
    IF (LEN(TRIM(ovFile)).eq.0) THEN
        WRITE(6,'(A)') "ERROR: overlap file not provided"
        STOP "ERROR: overlap file not provided"
    ENDIF


    !IF (nOrbLoc.gt.0) THEN
    !    CALL orb%destroy()
    !    CALL orb%initializeEmpty(nBasLoc) 
    !    ALLOCATE(tempRealArr(nBasLoc,nBasLoc))
    !    jOrb=0
    !    DO iOrb=1,nOrbLI/nBasLoc+1
    !        tempRealArr=0.d0
    !        IF (iOrb.eq.nOrbLI/nBasLoc+1) THEN
    !        ELSE
    !            tempRealArr(1:nBasLoc,1:nBasLoc)=orbLI(1:nBasLoc,1+jOrb:nBasLoc+jOrb)
    !        ENDIF
    !        CALL orb%setSym(1,tempRealArr)
    !        WRITE(token,'(A,I0,A)') TRIM(linIndepOrbFile)//".polyOnly.",iOrb,".molden"
    !        CALL orb%writeOrbital(TRIM(token),"f","mol")   
    !        jOrb=jOrb+nBasLoc
    !    ENDDO
    !    DEALLOCATE(tempRealArr)
    !ENDIF

    WRITE(6,'(A)') "Check Linear Independence and calculate pollution"
    ALLOCATE(orbTrunc(nBasAll,nOrbLI))
    ALLOCATE(orbLIMon(nBasAll,nOrbLI))
    ALLOCATE(orbLILoc(nBasAll,nOrbLI))
    ALLOCATE(ovOrbLILI(nOrbLI,nOrbLI))
    ALLOCATE(ovOrbLILILoc(nOrbLI,nOrbLI))
    ALLOCATE(ovOrbLILIMon(nOrbLI,nOrbLI))
    ALLOCATE(tempRealArr(nBasAll,nOrbLI))

    orbTrunc=orbLI(:,1:nOrbLI)
    orbLIMon=orbLI(:,1:nOrbLI)
    orbLILoc=orbLI(:,1:nOrbLI)
    orbLILoc(nBasLoc+1:nBasAll,:)=0.d0
    orbLIMon(1:nBasLoc,:)=0.d0
    ovOrbLILI=0.d0
    ovOrbLILIMon=0.d0
    ovOrbLILILoc=0.d0
    error=0.d0

    tempRealArr=0.d0
    !tempRealArr=MATMUL(ovBas,orbLI)
    !ovOrbLILI=MATMUL(TRANSPOSE(orbLI),tempRealArr)
    call dmatmat(ovBas,orbTrunc,'n','n',tempRealArr)
    call dmatmat(orbTrunc,tempRealArr,'t','n',ovOrbLILI)
   
    tempRealArr=0.d0
    !tempRealArr=MATMUL(ovBas,orbLILoc)
    !ovOrbLILILoc=MATMUL(TRANSPOSE(orbLILoc),tempRealArr)
    call dmatmat(ovBas,orbLILoc,'n','n',tempRealArr)
    call dmatmat(orbLILoc,tempRealArr,'t','n',ovOrbLILILoc)

    tempRealArr=0.d0
    !tempRealArr=MATMUL(ovBas,orbLIMon)
    !ovOrbLILIMon=MATMUL(TRANSPOSE(orbLIMon),tempRealArr)
    call dmatmat(ovBas,orbLIMon,'n','n',tempRealArr)
    call dmatmat(orbLIMon,tempRealArr,'t','n',ovOrbLILIMon)

    DO iOrb=1,nOrbLI
        WRITE(6,'(A,I5,3(A,E12.6))') "norm of orbtital ",iOrb," = ",ovOrbLILI(iOrb,iOrb),&
          " local contribution= ",ovOrbLILILoc(iOrb,iOrb),&
          " monocentric contribution= ",ovOrbLILIMon(iOrb,iOrb)
        ovOrbLILI(iOrb,iOrb)=0.d0
    ENDDO
    DEALLOCATE(ovOrbLILILoc)
    DEALLOCATE(ovOrbLILIMon)
    ii=MAXLOC(ABS(ovOrbLILI))
    i=ii(1)
    j=ii(2)
    error=ovOrbLILI(i,j)
    WRITE(6,'(A,E18.12,A,2(x,I0))') "largest deviation in overlap= ",error," for orbitals: ",i,j
    IF (ABS(error).gt.linDepThr) THEN 
        WRITE(6,'(A)') "WARNING: Error in overlap exceed linear dependence removal thr"
        WRITE(6,'(A,E18.12)') "error=",error
        WRITE(6,'(A,E18.12)') "thr=",linDepThr
    ENDIF
    WRITE(6,'(A)')
    WRITE(6,'(A)')


    IF (printFmtOrbs.or.printBinOrbs) THEN
        WRITE(6,'(A,2(x,I0))') "Total orbitals ",nBasAll,nOrbLI
        CALL orb%initializeEmpty(nBasAll) 
        CALL orb%setSym(1,orbLI)
    ENDIF
    IF (printFmtOrbs) CALL orb%writeOrbital(TRIM(linIndepOrbFile)//".RasOrb","f")
    IF (printBinOrbs) CALL orb%writeOrbital(TRIM(linIndepOrbFile)//".bin","b")

    nContraction=0
    basisL=1
    IF (basisCreate) THEN
        WRITE(6,'(A)') "Basis Creation Flag found - Use Lin Indep Orbs to generate basis set files"
        WRITE(6,'(A)') "In orbtial retain only the last relevant block of basis coefs" 
        DO iOrb=1,nOrbLI
            DO iBas=nBasAll,1,-1 
                IF (ABS(orbLI(iBas,iOrb)).gt.1e-30) EXIT
            ENDDO
            DO iBlock=nBlock,1,-1
                IF (block(iBlock)%getOffset().lt.iBas) EXIT              
            ENDDO
            k=block(iBlock)%getK()
            l=block(iBlock)%getL()
            IF (iOrb.eq.1) THEN
                kT=k
                lT=l
            ELSE
                IF (kT.ne.k.OR.lT.ne.l) THEN
                    basisL=basisL+1
                    IF (basisL.gt.20) THEN
                        STOP "Code cannot handle bases with L>20."   
                    ENDIF
                    kT=k
                    lT=l
                ENDIF
            ENDIF
            basisBlock(basisL)=iBlock
            nContraction(basisL)=nContraction(basisL)+1
            WRITE(6,'(A,I0,A,I0,A,I0,A,I0)') "Orbital ",iOrb," associated with basis k=",k," ,l=",l," in Block ",iBlock
            WRITE(6,'(A)') "Set coeffecient of all preceding blocks to 0"
            DO jBlock=1,iBlock-1
                k=block(jBlock)%getK()
                l=block(jBlock)%getL()
                !WRITE(6,'(A,I0,A,I0,A)') "  ->Set k=",k," ,l=",l," block to zero"
                iBas=block(jBlock)%getOffset()
                jBas=block(jBlock)%getNBas()
                orbLI(iBas+1:iBas+jBas,iOrb)=0.d0
            ENDDO
        ENDDO
        tempRealArr=0.d0
        !tempRealArr=MATMUL(ovBas,orbLI)
        !ovOrbLILI=MATMUL(TRANSPOSE(orbLI),tempRealArr)
        call dmatmat(ovBas,orbTrunc,'n','n',tempRealArr)
        call dmatmat(orbTrunc,tempRealArr,'t','n',ovOrbLILI)
        DO iOrb=1,nOrbLI
            WRITE(6,'(A,I5,1(A,F24.12))') "new norm of orbital before renormalization ",iOrb," = ",ovOrbLILI(iOrb,iOrb)
            orbLI(:,iOrb)=orbLI(:,iOrb)/sqrt(ovOrbLILI(iOrb,iOrb))
        ENDDO
        WRITE(6,'(A)') "Renormalize"
        tempRealArr=0.d0
        !tempRealArr=MATMUL(ovBas,orbLI)
        !ovOrbLILI=MATMUL(TRANSPOSE(orbLI),tempRealArr)
        call dmatmat(ovBas,orbTrunc,'n','n',tempRealArr)
        call dmatmat(orbTrunc,tempRealArr,'t','n',ovOrbLILI)
        DO iOrb=1,nOrbLI
            WRITE(6,'(A,I5,1(A,F24.12))') "new norm of orbital after renormalization ",iOrb," = ",ovOrbLILI(iOrb,iOrb)
        ENDDO
        !!DEBUG
        !CALL orb%setSym(1,orbLI)
        !CALL orb%writeOrbital(TRIM(linIndepOrbFile)//".renorm.RasOrb","f")
        !CALL orb%writeOrbital(TRIM(linIndepOrbFile)//".renorm.bin","b")
        !!DEBUG
        offsetC=0
        DO iBas=1,COUNT(nContraction.gt.0)
            k=block(basisBlock(iBas))%getK()
            l=block(basisBlock(iBas))%getL()
            offsetB=block(basisBlock(iBas))%getOffset()
            nPrimitive=block(basisBlock(iBas))%getNBas()
            WRITE(6,'(7(A,I0))') "Write basis for l=",l," ,k=",k,&
              " basis=",iBas," block=",basisBlock(iBas),&
              " with nPrimitive=",nPrimitive," and nContraction=",nContraction(iBas)," and offset=",offsetB
            WRITE(label,'(A2,I0,A3,I0,A)') "l.",l,"_k.",k,".bas"
            CALL basis%create(block(1)%getNBas(),nContraction(iBas))
            CALL basis%setContractionMatrix(block(1)%getNBas(),nContraction(iBas),&
              orbLI(offsetB+1:offsetB+nPrimitive,offsetC+1:offsetC+nContraction(iBas)))
            CALL basis%writeBasis(TRIM(label))
            offsetC=offsetC+nContraction(iBas)
            CALL basis%destroy()
        ENDDO
        !CONTINUE HERE!
    ENDIF
   
    WRITE(6,'(A)') "----Happy-----"
END PROGRAM
