!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoElectronDensity
    USE RDMIO
    USE cubeIO
    IMPLICIT none
    LOGICAL :: exists
    INTEGER :: stat
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: RDMFile
    CHARACTER*200 :: rdmFormat
    CHARACTER*200 :: cubeFile

    CHARACTER*200 :: fich
    INTEGER :: nState

    TYPE(RDM_) :: RDM
    TYPE(cube_), ALLOCATABLE :: orbitalCube(:)
    TYPE(cube_), ALLOCATABLE :: densityCube(:)

    REAL*8, ALLOCATABLE :: RDMMatrix(:,:)
    INTEGER :: iTemp
    INTEGER :: iState,jState
    INTEGER :: iOrb,jOrb
    INTEGER :: nOccupied
    LOGICAL :: empty

    NAMELIST /INPUT/ RDMFile,&
        rdmFormat,&
        cubeFile
    
    rdmFormat="b"

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    IF (rdmFormat(1:1).eq."b") THEN
        CALL RDM%initializeOld(TRIM(rdmFile),"b",300)
    ELSE
        CALL RDM%initializeOld(TRIM(rdmFile),"f",300)
    ENDIF
    CALL RDM%getOccupied(nOccupied)
    CALL RDM%getStates(nState)

    WRITE(6,'(A,I4)') "Number of occupied Orbitals= ",nOccupied
    WRITE(6,'(A,I4)') "Number of States= ",nState

    ALLOCATE (orbitalCube(nOccupied))
    ALLOCATE (densityCube(nState))
    ALLOCATE (RDMMatrix(nState,nState))

    OPEN(100,FILE=TRIM(cubeFile))
    READ(100,*) iTemp
    IF (iTemp.ne.nOccupied) THEN
        PRINT*, "ERROR: number of orbitals in RDM:",nOccupied
        PRINT*, "ERROR: number of cube files:",iTemp
        STOP "ERROR: RDM and number of cube files disagree"
    ENDIF
    DO iOrb=1,nOccupied
        READ(100,*) fich
        WRITE(6,'(A,A)') "Read cube file ",TRIM(fich)
        CALL orbitalCube(iOrb)%create(TRIM(fich))
    ENDDO

    DO iState=1,nState
        CALL densityCube(iState)%create(orbitalCube(1))
    ENDDO

    DO 
        CALL RDM%getNextRecordInfo(iOrb,jOrb,empty,stat)
        IF (stat.ne.0) EXIT
        IF (.NOT.empty) THEN
            CALL RDM%getNextRecordData(nState,RDMMatrix)        
            DO iState=1,nState
                densityCube(iState)=densityCube(iState)+RDMMatrix(iState,iState)*(orbitalCube(iOrb)*orbitalCube(jOrb))
            ENDDO
        ENDIF
    ENDDO

    DO iState=1,nState
        CALL densityCube(iState)%writeCube("dens",400)
    ENDDO
END PROGRAM 
