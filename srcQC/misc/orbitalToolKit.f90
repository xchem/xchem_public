!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE orbitalToolKit
    USE diagonalizations
    IMPLICIT none
    CONTAINS
        FUNCTION norm(nBas,vec,ovIn) RESULT(n)
            INTEGER, INTENT(in) :: nBas
            REAL*8, INTENT(in) :: vec(nBas)
            REAL*8, INTENT(in), OPTIONAL :: ovIn(nBas,nBas) 
            REAL*8 :: ov(nbas,nBas)
            LOGICAL :: ONBas
            REAL*8 :: n
            INTEGER :: iBas,jBas
            IF (PRESENT(ovIN)) THEN
                ONBas=.FALSE.
                ov=ovIn
            ELSE
                ONBas=.TRUE.
            ENDIF
            n=0.d0
            DO iBas=1,nBas
                IF (ONBas) THEN 
                    n=n+vec(iBas)**2
                ELSE
                    DO jBas=1,nBas
                        n=n+vec(iBas)*vec(jBas)*ov(iBas,jBas)
                    ENDDO
                ENDIF
            ENDDO
            n=sqrt(n)
        END FUNCTION
        FUNCTION overlap(nBas,vec1,vec2,ovIn) RESULT(o)
            INTEGER, INTENT(in) :: nBas
            REAL*8, INTENT(in) :: vec1(nBas)
            REAL*8, INTENT(in) :: vec2(nBas)
            REAL*8, INTENT(in), OPTIONAL :: ovIn(nBas,nBas) 
            REAL*8 :: ov(nbas,nBas)
            LOGICAL :: ONBas
            REAL*8 :: o
            INTEGER :: iBas,jBas
            IF (PRESENT(ovIN)) THEN
                ONBas=.FALSE.
                ov=ovIn
            ELSE
                ONBas=.TRUE.
            ENDIF
            o=0.d0
            DO iBas=1,nBas
                IF (ONBas) THEN 
                    o=o+vec1(iBas)*vec2(iBas)
                ELSE
                    DO jBas=1,nBas
                        o=o+vec1(iBas)*vec2(jBas)*ov(iBas,jBas)
                    ENDDO
                ENDIF
            ENDDO
        END FUNCTION
        SUBROUTINE overlaps(nVec,nBas,orb,ovOrb,ovBasIn)
            IMPLICIT none
            INTEGER, INTENT(in) :: nVec
            INTEGER, INTENT(in) :: nBas
            REAL*8, INTENT(in) :: orb(nBas,nVec)
            REAL*8, INTENT(out) :: ovOrb(nVec,nVec)
            REAL*8, INTENT(in), OPTIONAL :: ovBasIn(nBas,nBas)
            REAL*8, ALLOCATABLE :: ovBas(:,:)
            LOGICAL :: ONBas
            INTEGER :: iVec,jVec
            IF (PRESENT(ovBasIn)) THEN
                ONBas=.FALSE.
                ALLOCATE(ovBas(nBas,nBas))
                ovBas=ovBasIn
            ELSE
                ONBas=.TRUE.
            ENDIF

            DO iVec=1,nVec
                DO jVec=1,nVec
                    IF (ONBas) THEN
                        ovOrb(iVec,jVec)=overlap(nBas,orb(:,iVec),orb(:,jVec))
                    ELSE
                        ovOrb(iVec,jVec)=overlap(nBas,orb(:,iVec),orb(:,jVec),ovBas)
                    ENDIF
                ENDDO
            ENDDO
            DO iVec=1,nVec
                WRITE(6,'(<nVec>(x,E12.6))') (ovOrb(jVec,iVec),jVec=1,nVec)
            ENDDO
            WRITE(6,'(A)') ""
        END SUBROUTINE
        SUBROUTINE overlaps2(nBas,nVec1,nVec2,orb1,orb2,ovOrb,ovBasIn)
            IMPLICIT none
            INTEGER, INTENT(in) :: nVec1,nVec2
            INTEGER, INTENT(in) :: nBas
            REAL*8, INTENT(in) :: orb1(nBas,nVec1)
            REAL*8, INTENT(in) :: orb2(nBas,nVec1)
            REAL*8, INTENT(out) :: ovOrb(nVec1,nVec2)
            REAL*8, INTENT(in), OPTIONAL :: ovBasIn(nBas,nBas)
            REAL*8, ALLOCATABLE :: ovBas(:,:)
            LOGICAL :: ONBas
            INTEGER :: iVec,jVec
            IF (PRESENT(ovBasIn)) THEN
                ONBas=.FALSE.
                ALLOCATE(ovBas(nBas,nBas))
                ovBas=ovBasIn
            ELSE
                ONBas=.TRUE.
            ENDIF

            DO iVec=1,nVec1
                DO jVec=1,nVec2
                    IF (ONBas) THEN
                        ovOrb(iVec,jVec)=overlap(nBas,orb1(:,iVec),orb2(:,jVec))
                    ELSE
                        ovOrb(iVec,jVec)=overlap(nBas,orb1(:,iVec),orb2(:,jVec),ovBas)
                    ENDIF
                ENDDO
            ENDDO
            DO iVec=1,nVec2
                WRITE(6,'(<nVec2>(x,E12.6))') (ovOrb(jVec,iVec),jVec=1,nVec1)
            ENDDO
            WRITE(6,'(A)') ""
        END SUBROUTINE
        SUBROUTINE gramSchmidt(nVec,nBas,vecLD,ovIn)
            IMPLICIT none
            INTEGER, INTENT(in) :: nVec
            INTEGER, INTENT(in) :: nBas
            REAL*8, INTENT(inout) :: vecLD(nBas,nVec)
            REAL*8, INTENT(in), OPTIONAL :: ovIn(nBas,nBas)
            REAL*8, ALLOCATABLE :: ov(:,:)
            REAL*8 :: orbNorm
            LOGICAL :: ONBas

            REAL*8 :: vecLI(nBas,nVec)
            INTEGER :: iVec,jVec
            IF (PRESENT(ovIN)) THEN
                ONBas=.FALSE.
                ALLOCATE(ov(nBas,nBas))
                ov=ovIn
            ELSE
                ONBas=.TRUE.
            ENDIF
            IF (ONBas) THEN
                DO  iVec=1,nVec
                    vecLI(:,iVec)=vecLD(:,iVec)
                    IF (iVec.gt.1) THEN
                        DO jVec=1,iVec-1
                            vecLI(:,iVec)=vecLI(:,iVec)-&
                              (overlap(nBas,vecLD(:,iVec),vecLI(:,jVec))/norm(nBas,vecLI(:,jVec))**2)*&
                              vecLI(:,jVec)
                        ENDDO
                    ENDIF
                    orbNorm=norm(nBas,vecLI(:,iVec))
                    vecLI(:,iVec)=vecLI(:,iVec)/orbNorm
                    WRITE(6,'(A,I0,A,E12.6)') "GS Orbital ",iVec," has norm ",orbNorm
                ENDDO
            ELSE
                DO  iVec=1,nVec
                    vecLI(:,iVec)=vecLD(:,iVec)
                    IF (iVec.gt.1) THEN
                        DO jVec=1,iVec-1
                            vecLI(:,iVec)=vecLI(:,iVec)-&
                              (overlap(nBas,vecLD(:,iVec),vecLI(:,jVec),ov)/norm(nBas,vecLI(:,jVec),ov)**2)*&
                              vecLI(:,jVec)
                        ENDDO
                    ENDIF
                    orbNorm=norm(nBas,vecLI(:,iVec),ov)
                    vecLI(:,iVec)=vecLI(:,iVec)/orbNorm
                    WRITE(6,'(A,I0,A,E12.6)') "GS Orbital ",iVec," has norm ",orbNorm
                ENDDO
            ENDIF
            vecLD=vecLI    
        END SUBROUTINE
        SUBROUTINE rmLinDepOrb(nVec,nBas,orbMat,ovMat,thr)
            IMPLICIT none
            INTEGER, INTENT(in) :: nVec
            INTEGER, INTENT(in) :: nBas    
            REAL*8, INTENT(in) :: thr
            REAL*8, INTENT(inout) :: ovMat(nVec,nVec)
            REAL*8, INTENT(inout) :: orbMat(nBas,nVec)
            REAL*8 :: eVal(nVec)
            REAL*8 :: lambda(nVec,nVec)
            REAL*8 :: UMX(nVec,nVec)
            INTEGER :: nIndep,iVec
            lambda=0.d0
            nIndep=0
            CALL diag(ovMat,nVec,eVal)
            DO iVec=1,nVec  
                IF (eval(iVec).gt.thr) THEN
                    nIndep=nIndep+1
                    lambda(iVec,iVec)=1/sqrt(eVal(iVec))
                ELSE    
                    ovMat(:,iVec)=0.d0
                ENDIF
            ENDDO
            WRITE(6,'(2(A,I))') "Total orbitals: ",nVec," linearly independent: ",nIndep
            UMX=MATMUL(ovMat,lambda)
            orbMat=MATMUL(orbMat,UMX)
        END SUBROUTINE
        SUBROUTINE rmLinDepOrbOrb(nBas,nVec1,nVec2,orbMat1,orbMat2,ovMat,thr)
            IMPLICIT none
            INTEGER, INTENT(in) :: nVec1,nVec2
            INTEGER, INTENT(in) :: nBas    
            REAL*8, INTENT(in) :: thr
            REAL*8, INTENT(in) :: ovMat(nVec1,nVec2)
            REAL*8, INTENT(in) :: orbMat1(nBas,nVec1)
            REAL*8, INTENT(inout) :: orbMat2(nBas,nVec2)
            REAL*8 :: eVal(nVec2)
            REAL*8 :: lambda(nVec2,nVec2)
            REAL*8 :: UMX(nVec2,nVec2)
            REAL*8 :: ULX(nVec1,nVec2)
            REAL*8 :: A(nVec2,nVec2),O(nVec2,nVec2)
            REAL*8 :: I(nVec2,nVec2)
            INTEGER :: nIndep,iVec
            lambda=0.d0
            nIndep=0
            A=0.d0
            O=0.d0
            UMX=0.d0
            ULX=0.d0
            I=0.d0
            A=MATMUL(TRANSPOSE(ovMat),ovMat)
            DO iVec=1,nVec2
                I(iVec,iVec)=1.d0
            ENDDO
            A=I-A
            O=A
            CALL diag(O,nVec2,eVal)
            WRITE(6,'(A,<nVec2>(x,E12.6))'), "eVals of Metric",eVal
            DO iVec=1,nVec2
                IF (eval(iVec).gt.thr) THEN
                    nIndep=nIndep+1
                    lambda(iVec,iVec)=1/sqrt(eVal(iVec))
                ELSE    
                    O(:,iVec)=0.d0
                ENDIF
            ENDDO
            WRITE(6,'(2(A,I))') "Total orbitals: ",nVec2," linearly independent: ",nIndep
            UMX=MATMUL(O,lambda)
            ULX=-MATMUL(ovMat,UMX)
            orbMat2=MATMUL(orbMat1,ULX)+MATMUL(orbMat2,UMX)
        END SUBROUTINE
END MODULE

