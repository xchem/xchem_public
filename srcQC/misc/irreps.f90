!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE class_irreps
    IMPLICIT none
    PRIVATE
    PUBLIC OPERATOR(*)
    PUBLIC irrep_
    TYPE irrep_
        CHARACTER*6 :: label
        CHARACTER*6 :: group
        INTEGER :: ind
        CONTAINS
            PROCEDURE :: createFromLabel
            PROCEDURE :: createFromIndex
            PROCEDURE :: getGroup
            PROCEDURE :: getLabel
            PROCEDURE :: getIndex
            GENERIC :: create => createFromLabel,createFromIndex 
    END TYPE

    !OPERATOR OVERLOADING
    INTERFACE OPERATOR (*)
        PROCEDURE mlt
    END INTERFACE OPERATOR (*)

    CONTAINS 
        FUNCTION mlt(a,b)
            IMPLICIT none
            TYPE(irrep_),INTENT(in) :: a,b
            TYPE(irrep_) :: mlt
            CHARACTER*6 :: grp
            INTEGER :: l1,l2,l3,tmp
            INTEGER :: c1(1,1)
            INTEGER :: ci(2,2),cs(2,2),c2(2,2)
            INTEGER :: c2h(4,4),c2v(4,4),d2(4,4)
            INTEGER :: d2h(8,8)
            !group table for c1 
            c1(1,1)=1
            !group table for c2
            c2(1,1)=1; c2(2,2)=1 
            c2(1,2)=2; 
            !group table for ci
            ci(1,1)=1; ci(2,2)=1
            ci(1,2)=2; 
            !group table for cs
            cs(1,1)=1; cs(2,2)=1
            cs(1,2)=2; 
            !group table for c2h
            c2h(1,1)=1; c2h(2,2)=1; c2h(3,3)=1; c2h(4,4)=1
            c2h(1,2)=2; c2h(2,3)=4; c2h(3,4)=2
            c2h(1,3)=3; c2h(2,4)=3 
            c2h(1,4)=4
            !group table for c2v
            c2v(1,1)=1; c2v(2,2)=1; c2v(3,3)=1; c2v(4,4)=1
            c2v(1,2)=2; c2v(2,3)=4; c2v(3,4)=2
            c2v(1,3)=3; c2v(2,4)=3
            c2v(1,4)=4
            !group table for d2
            d2(1,1)=1; d2(2,2)=1; d2(3,3)=1; d2(4,4)=1
            d2(1,2)=2; d2(2,3)=4; d2(3,4)=2
            d2(1,3)=3; d2(2,4)=3
            d2(1,4)=4 
            !group table for d2h
            d2h(1,1)=1; d2h(2,2)=1; d2h(3,3)=1; d2h(4,4)=1; d2h(5,5)=1; d2h(6,6)=1; d2h(7,7)=1; d2h(8,8)=1
            d2h(1,2)=2; d2h(2,3)=4; d2h(3,4)=2; d2h(4,5)=8; d2h(5,6)=2; d2h(6,7)=4; d2h(7,8)=2
            d2h(1,3)=3; d2h(2,4)=3; d2h(3,5)=7; d2h(4,6)=7; d2h(5,7)=3; d2h(6,8)=3
            d2h(1,4)=4; d2h(2,5)=6; d2h(3,6)=8; d2h(4,7)=6; d2h(5,8)=4
            d2h(1,5)=5; d2h(2,6)=5; d2h(3,7)=5; d2h(4,8)=5
            d2h(1,6)=6; d2h(2,7)=8; d2h(3,8)=6
            d2h(1,7)=7; d2h(2,8)=7
            d2h(1,8)=8
            IF (a%getGroup() .ne. b%getGroup()) THEN
                STOP "ERROR: cannot multiply irreps belonging to different groups"
            ENDIF
            grp=a%getGroup()
            l1=a%getIndex()
            l2=b%getIndex()
            IF (l2.lt.l1) THEN
                tmp=l2
                l2=l1
                l1=tmp
            ENDIF
            SELECT CASE(grp)
                CASE("c1")  
                    l3=c1(l1,l2)
                CASE("c2")  
                    l3=c2(l1,l2)
                CASE("ci")  
                    l3=ci(l1,l2)
                CASE("cs")  
                    l3=cs(l1,l2)
                CASE("c2h") 
                    l3=c2h(l1,l2)
                CASE("c2v") 
                    l3=c2v(l1,l2)
                CASE("d2")  
                    l3=d2(l1,l2)
                CASE("d2h") 
                    l3=d2h(l1,l2)
                CASE DEFAULT
                    STOP "ERROR unknown point group"
            END SELECT
            CALL mlt%createFromIndex(l3,grp)
        END FUNCTION

        !CREATOR
        SUBROUTINE createFromLabel(this,label,group)
            IMPLICIT none
            CLASS(irrep_) :: this
            CHARACTER*6, INTENT(in) :: label,group
            this%label=label
            this%group=group
            SELECT CASE(group)
                CASE("c1")
                    this%ind=1
                CASE("c2")
                    SELECT CASE(label)
                        CASE("a"); this%ind=1
                        CASE("b"); this%ind=2
                    END SELECT
                CASE("ci")
                    SELECT CASE(label)
                        CASE("ag"); this%ind=1
                        CASE("au"); this%ind=2
                    END SELECT
                CASE("cs") 
                    SELECT CASE(label)
                        CASE("a'"); this%ind=1
                        CASE("a''"); this%ind=2
                    END SELECT 
                CASE("c2h")
                    SELECT CASE(label)
                        CASE("ag"); this%ind=1
                        CASE("bg"); this%ind=2
                        CASE("au"); this%ind=3
                        CASE("bu"); this%ind=4
                    END SELECT
                CASE("c2v")
                    SELECT CASE(label)  
                        CASE("a1"); this%ind=1
                        CASE("a2"); this%ind=2
                        CASE("b1"); this%ind=3
                        CASE("b2"); this%ind=4
                    END SELECT
                CASE("d2") 
                    SELECT CASE(label)
                        CASE("a"); this%ind=1
                        CASE("b1"); this%ind=2
                        CASE("b2"); this%ind=3
                        CASE("b3"); this%ind=4
                    END SELECT
                CASE("d2h")
                    SELECT CASE(label)
                        CASE("ag"); this%ind=1
                        CASE("b1g"); this%ind=2
                        CASE("b2g"); this%ind=3
                        CASE("b3g"); this%ind=4
                        CASE("au"); this%ind=5
                        CASE("b1u"); this%ind=6
                        CASE("b2u"); this%ind=7
                        CASE("b3u"); this%ind=8
                    END SELECT
            END SELECT
        END SUBROUTINE
        SUBROUTINE createFromIndex(this,ind,group)
            IMPLICIT none
            CLASS(irrep_) :: this
            CHARACTER*6, INTENT(in) :: group
            INTEGER, INTENT(in) :: ind
            this%ind=ind
            this%group=group
            SELECT CASE(group)
                CASE("c1")
                    this%label="a"
                CASE("c2")
                    SELECT CASE(ind)
                        CASE(1); this%label="a"
                        CASE(2); this%label="b"
                    END SELECT
                CASE("ci")
                    SELECT CASE(ind)
                        CASE(1); this%label="ag"
                        CASE(2); this%label="au"
                    END SELECT
                CASE("cs") 
                    SELECT CASE(ind)
                        CASE(1); this%label="a'" 
                        CASE(2); this%label="a''"
                    END SELECT 
                CASE("c2h")
                    SELECT CASE(ind)
                        CASE(1); this%label="ag"
                        CASE(2); this%label="bg"
                        CASE(3); this%label="au"
                        CASE(4); this%label="bu"
                    END SELECT
                CASE("c2v")
                    SELECT CASE(ind)  
                        CASE(1); this%label="a1"
                        CASE(2); this%label="a2"
                        CASE(3); this%label="b1"
                        CASE(4); this%label="b2"
                    END SELECT
                CASE("d2") 
                    SELECT CASE(ind)
                        CASE(1); this%label="a" 
                        CASE(2); this%label="b1"
                        CASE(3); this%label="b2"
                        CASE(4); this%label="b3"
                    END SELECT
                CASE("d2h")
                    SELECT CASE(ind)
                        CASE(1); this%label="ag" 
                        CASE(2); this%label="b1g"
                        CASE(3); this%label="b2g"
                        CASE(4); this%label="b3g"
                        CASE(5); this%label="au" 
                        CASE(6); this%label="b1u"
                        CASE(7); this%label="b2u"
                        CASE(8); this%label="b3u"
                    END SELECT
            END SELECT
        END SUBROUTINE

        !GETTERS
        FUNCTION getGroup(this)
            IMPLICIT none 
            CLASS(irrep_) :: this
            CHARACTER*6 :: getGroup
            getGroup=this%group
        END FUNCTION
        FUNCTION getLabel(this)
            IMPLICIT none 
            CLASS(irrep_) :: this
            CHARACTER*6 :: getLabel
            getLabel=this%label
        END FUNCTION
        FUNCTION getIndex(this)
            IMPLICIT none 
            CLASS(irrep_) :: this
            INTEGER :: getIndex
            getIndex=this%ind
        END FUNCTION
END MODULE

