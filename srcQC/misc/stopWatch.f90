!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE stopWatch
!CLASS FOR STOP WATCH OBJECTS
!MEASURES TIME BETWEEN FIRST AND MOST RECENT
!CALL TIME CALL TO OBJECT
!ALLOWS TO SPECIFY A MESSAGE ALONGSIDE TIMING INFO
    IMPLICIT none
    PRIVATE
    PUBLIC stopWatch_
    TYPE stopWatch_
        PRIVATE
        CHARACTER*100 :: Label
        INTEGER :: clickCountZero=0
        INTEGER :: clickCountPrevious=0
        INTEGER :: clickRate=0
        CONTAINS 
            PROCEDURE :: initialize
            PROCEDURE :: getTime
            PROCEDURE :: printTime
            GENERIC :: time => getTime,printTime
    END TYPE

CONTAINS

    SUBROUTINE initialize(this,label)
        IMPLICIT none
        CLASS(stopWatch_) :: this
        INTEGER :: clickRate,clickCount
        CHARACTER(len=*), INTENT(in),OPTIONAL :: label
        IF (PRESENT(label)) THEN
            this%label=TRIM(label)
        ELSE
            this%label="timer"
        ENDIF
        CALL system_clock(clickCount,clickRate)
        this%clickCountZero=clickCount
        this%clickCountPrevious=clickCount
        this%clickRate=clickRate
    END SUBROUTINE
    
    SUBROUTINE getTime(this,secsT,secsP)
        IMPLICIT none
        CLASS(stopWatch_) :: this
        REAL*8, INTENT(out) :: secsT
        REAL*8, INTENT(out),OPTIONAL :: secsP
        INTEGER :: clickCount,clickRate
        CALL system_clock(clickCount,clickRate)
        secsT=REAL(clickCount-this%clickCountZero)/REAL(clickRate)
        IF (present(secsP)) THEN
            secsP=REAL(clickCount-this%clickCountPrevious)/REAL(clickRate)
            this%clickCountPrevious=clickCount
        ENDIF
    END SUBROUTINE
    
    SUBROUTINE printTime(this,eventIn)
        IMPLICIT none
        CLASS(stopWatch_) :: this
        CHARACTER(len=*), INTENT(in),OPTIONAL :: eventIn
        CHARACTER*200 :: event
        REAL*8 :: secsT
        REAL*8 :: secsP
        INTEGER :: clickCount,clickRate
        event="StopWatch Call"
        IF (present(eventIn)) event=TRIM(eventIn)
        CALL system_clock(clickCount,clickRate)
        secsT=REAL(clickCount-this%clickCountZero)/REAL(clickRate)
        secsP=REAL(clickCount-this%clickCountPrevious)/REAL(clickRate)
        this%clickCountPrevious=clickCount
        WRITE(6,'(A,x,F18.12,x,A,x,F18.12,x,A,x,A)') TRIM(event),secsT,"seconds passed",&
          secsP,"seconds passed since last call to",TRIM(this%label)
    END SUBROUTINE
END MODULE
