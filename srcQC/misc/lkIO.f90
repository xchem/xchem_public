!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module lkIO
    implicit none
    private
    public lk_ 
    type lk_
        integer :: nlkPairs
        integer :: BigL
        integer, allocatable :: lk(:,:)
        contains
            procedure :: is_lk
            procedure :: getBigL
            procedure :: createFromFile
            procedure :: createFromMaxVals
            procedure :: getL
            procedure :: getNrLs
            procedure :: getK
            procedure :: getMaxK
            generic :: create => createFromFile,createFromMaxVals
    end type

    contains
        subroutine createFromFile(this, file)
            class(lk_) :: this
            character(len=*), intent(in) :: file
            integer :: il
            integer :: BigLTemp,BigL 
            open(1,file=trim(file),status="old")
            read(1,*) this.nlkPairs
            allocate(this.lk(2,this.nlkPairs))
            BigL=0
            do il=1, this.nlkPairs
                read(1,*) this.lk(1,il), this.lk(2,il)
                BigLTemp = this.lk(1,il) + 2 * this.lk(2,il)
                if (BigLTemp > BigL) BigL = BigLTemp
            enddo
            this.BigL = BigL
            close(1)
        end subroutine createFromFile

        subroutine createFromMaxVals(this,lMax,kMax)
            class(lk_) :: this
            integer, intent(in) :: lMax,kMax
            integer :: il
            this.BigL = lMax + 2 * kMax
            this.nlkPairs = lMax + 1
            allocate(this.lk(2,this.nlkPairs))
            do il = 1, this.nlkPairs
                this.lk(1,il) = il - 1 
                this.lk(2,il) = kMax
            enddo
        end subroutine createFromMaxVals
 
        logical function is_lk(this,l,k) result(is_lk_present)
            class(lk_) :: this
            integer, intent(in) :: l,k
            integer :: il
            is_lk_present = .false.
            do il = 1, this.nlkPairs 
                if (this.lk(1,il) .eq. l .and. this.lk(2,il) .ge. k ) then
                    is_lk_present = .true.
                    exit
                endif
            enddo
        end function is_lk

        integer function getBigL(this) result(BigL)
            class(lk_) :: this
            BigL = this.BigL
        end function getBigL

        integer function getK(this, l) result(kOut)
            class(lk_) ::  this
            integer, intent(in) :: l
            integer :: il
            do il = 1, this.nlkPairs
                if (this.lk(1,il) .eq. l) then
                    kOut = this.lk(2,il) 
                endif
            enddo
        end function getK

        integer function getL(this, i) result(lOut)
            class(lk_) ::  this
            integer, intent(in) :: i
            lOut = this.lk(1,i)
        end function getL

        integer function getMaxK(this) result(kOut)
            class(lk_) ::  this
            kOut = maxVal(this.lk(2,:))
        end function getMaxK

        integer function getNrLs(this) result(nrLs)
            class(lk_) ::  this
            NrLs = this.nlkPairs
        end function getNrLs
end module
        
