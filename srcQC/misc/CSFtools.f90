!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module CSFtools
contains
subroutine readCSF0(fich,way,un,npot,nCSF)
implicit none
character*100, intent(in) :: fich,way
integer, intent(in) :: un
integer, intent(out) :: nCSF,npot
integer i

select case(way)
 case("ascii")
  open(un,file=fich,status="old",iostat=i)
 case("binary")
  open(un,file=fich,status="old",iostat=i,form="unformatted")
 case default
   write(6,*) "Format of the CI vector must be text or binary"
   stop "Format of the CI vector must be text or binary"
end select

if (i.ne.0) then
 write(6,*) "File does not exist ",trim(fich)
 stop 'File with CI vector is compulsory'
endif

select case(way)
 case("binary")
  read(un) npot,nCSF
 case("ascii")
  read(un,*) npot,nCSF
end select

end subroutine

subroutine readCSF(way,un,npot,nCSF,CI)
implicit none
character*100, intent(in) :: way
integer, intent(in) :: un,npot,nCSF
real*8, intent(out) :: CI(npot,nCSF)

integer iCSF,iroot

do iroot=1,npot
 select case(way)
  case("binary")
   read(un) (CI(iroot,iCSF),iCSF=1,nCSF)
  case("ascii")
   read(un,*) (CI(iroot,iCSF),iCSF=1,nCSF)
 end select
enddo
close(un)

end subroutine

subroutine writeCSF(way,fich,un,npot,nCSF,CI)
implicit none
character*100, intent(in) :: way,fich
integer, intent(in) :: un,npot,nCSF
real*8, intent(in) :: CI(npot,nCSF)

integer iCSF,iroot

select case(way)
 case("binary")
  open(un,file=fich,form="unformatted")
  write(un) npot,nCSF
  do iroot=1,npot
   write(un) (CI(iroot,iCSF),iCSF=1,nCSF)
  enddo
  close(un)
 case("ascii")
  open(un,file=fich)
  write(un,"(2(x,I10.10))") npot,nCSF
  do iroot=1,npot
   write(un,"(5(x,E40.30e3))") (CI(iroot,iCSF),iCSF=1,nCSF)
  enddo
  close(un)
 case default
  write(6,*) "Format of CI vector in CSF must be binary or ascii"
  stop "Format of CI vector in CSF must be binary or ascii"
end select

end subroutine
end module
