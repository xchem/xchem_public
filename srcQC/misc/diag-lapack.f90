!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module diagonalizations
contains

subroutine diag(A,npot,eig,evctIn)
implicit none
integer*8, intent(in) :: npot
logical, intent(in),optional :: evctIn
real*8,intent(inout) :: A(npot,npot)
real*8, intent(out) :: eig(npot)
logical :: evct
character*1 :: jobz

integer :: info,lwork
real*8 :: work(1)
real*8, allocatable :: work2(:)

evct=.TRUE.
IF (present(evctIn)) THEN
    evct=evctIn
ENDIF

lwork=-1
info=0

IF (evct) THEN
    jobz="V"
ELSE
    jobz="N"
ENDIF

call dsyev(jobz, "U", npot, A, npot, eig, WORK,LWORK,info)
lwork=int(work(1))
allocate (work2(lwork))
call dsyev(jobz, "U", npot, A, npot, eig, WORK2,LWORK,info)
deallocate (work2)

return
end subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine order(A,n,v,desc)
implicit none
integer, intent(in) :: n
real*8, intent(inout) :: A(n,n)
real*8, intent(inout) ::  v(n)
logical, intent(in) :: desc

real*8 kk
integer n2

integer i,j,k

do i=1,n-1
 do j=i+1,n
  if (v(j).le.v(i)) then
   kk=v(j)
   v(j)=v(i)
   v(i)=kk
   do k=1,n
    kk=A(k,j)
    A(k,j)=A(k,i)
    A(k,i)=kk
   enddo
  endif
 enddo
enddo

if (desc) then
 n2=n
 if (mod(n,2).eq.1) n2=n-1
 do i=1,n2/2
  kk=v(i)
  v(i)=v(n-i+1)
  v(n-i+1)=kk
  do k=1,n
   kk=A(k,i)
   A(k,i)=A(k,n-i+1)
   A(k,n-i+1)=kk
  enddo
 enddo
endif


return
end subroutine
!!!!!!!!!! COMPLEX
subroutine cdiag(A,npot,eig)
implicit none
integer, intent(in) :: npot
complex*16,intent(inout) :: A(npot,npot)
real*8, intent(out) :: eig(npot)

integer info,lwork
complex*16 work(1)
complex*16, allocatable :: work2(:)
real*8 rwork(1)
real*8, allocatable :: rwork2(:)



lwork=-1
call zheev("V", "U", npot, A, npot, eig, work,lwork,rwork,info)
lwork=int(real(work(1)))
allocate (work2(lwork),rwork2(lwork))
call zheev("V", "U", npot, A, npot, eig, work2,lwork,rwork2,info)
deallocate(work2,rwork2)

return
end subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine corder(A,n,v,desc)
implicit none
integer, intent(in) :: n
complex*16, intent(inout) :: A(n,n)
real*8, intent(inout) ::  v(n)
logical, intent(in) :: desc

real*8 kk
complex*16 ckk
integer n2

integer i,j,k

do i=1,n-1
 do j=i+1,n
  if (v(j).le.v(i)) then
   kk=v(j)
   v(j)=v(i)
   v(i)=kk
   do k=1,n
    ckk=A(k,j)
    A(k,j)=A(k,i)
    A(k,i)=kk
   enddo
  endif
 enddo
enddo

if (desc) then
 n2=n
 if (mod(n,2).eq.1) n2=n-1
 do i=1,n2/2
  kk=v(i)
  v(i)=v(n-i+1)
  v(n-i+1)=kk
  do k=1,n
   ckk=A(k,i)
   A(k,i)=A(k,n-i+1)
   A(k,n-i+1)=kk
  enddo
 enddo
endif


return
end subroutine


!!!!!!!!!!!
! Calculating eigenvalues from a non-orthonormal basis using the overlap
!! n is the size
!! ov enters as the overlap and leaves as the transformation matrix to a orthonormal basis
!! A enters as the Hamiltonian and leaves as the rotation matrix
!! w contains the eigenvalues
subroutine eig(n,A,ov,w,othr)
implicit none
integer, intent(in) :: n
real*8, intent(inout) :: A(n,n),ov(n,n)
real*8, intent(out) :: w(n)

real*8, intent(in), optional :: othr
real*8 :: thr=1e-5
real*8 ov0(n,n)

integer i,j,k,l
integer nli
real*8, allocatable :: tmpmat(:,:),tmpvec(:)
real*8, allocatable :: tmpmat2(:,:),tmpmat3(:,:)
real*8 kk

if (present(othr)) thr=othr

ov0=ov
call diag(ov,n,w)
call order(ov,n,w,.true.)
nli=0
do i=1,n
 if (w(i).ge.thr) then
  nli=nli+1
  do j=1,n
   ov(j,i)=ov(j,i)/sqrt(w(i))
  enddo
 else
  if (.false.) write(6,*) "Remove linear dependencies ",w(i)
  do j=1,n
   ov(j,i)=0.
  enddo
 endif
enddo

allocate(tmpmat(nli,nli),tmpvec(nli) )
allocate(tmpmat2(n,nli),tmpmat3(n,nli) )


if (.false.) then
!!!Checking othonormality

 write(6,*) "Linear independent ",nli," from ",n
 do i=1,nli
  do j=1,nli
   tmpmat(i,j)=0.
   do k=1,n
    do l=1,n
     tmpmat(i,j)=tmpmat(i,j)+ov(k,i)*ov(l,j)*ov0(k,l)
    enddo
   enddo
   write(6,*) "Done ",i,j,tmpmat(i,j)
  enddo
 enddo
 kk=0.
 do i=1,nli
  do j=1,nli
   if (i.eq.j) then
    kk=kk+(1-tmpmat(i,j))**2
   else
    kk=kk+tmpmat(i,j)**2
   endif
  enddo
 enddo
 write(6,*) "New basis functions ",kk

endif

do i=1,nli
 do j=1,n
  tmpmat2(j,i)=ov(j,i)
 enddo
enddo

tmpmat3=matmul(A,tmpmat2)
tmpmat=matmul(transpose(tmpmat2),tmpmat3)


!do i=1,nli
! do j=1,nli
!  tmpmat(i,j)=0.
!  do k=1,n
!   do l=1,n
!    tmpmat(i,j)=tmpmat(i,j)+ov(k,i)*ov(l,j)*A(k,l)
!   enddo
!  enddo
! enddo
!enddo

call diag(tmpmat,nli,tmpvec)
tmpmat3=matmul(tmpmat2,tmpmat)
A=0.
w=0.
do i=1,nli
 w(i)=tmpvec(i)
 do j=1,nli
  A(j,i)=tmpmat3(j,i)
 enddo
enddo


end subroutine

subroutine linSolver_dgesv(N,AIN,BIN,B)
    IMPLICIT none
    INTEGER, INTENT(in) :: N
    REAL*8, INTENT(in) :: AIN(N,N),BIN(N)
    REAL*8, INTENT(out) :: B(N)
    REAL*8 :: A(N,N)
    INTEGER :: IPIV(N)
    INTEGER :: NRHS,LDA,LDB,INFO

    NRHS=1
    LDA=N
    LDB=N
    A=AIN
    B=BIN

    CALL DGESV(N,NRHS,A,LDA,IPIV,B,LDB,INFO)
    IF (INFO.ne.0) THEN
        WRITE(6,'(A,I0)') "INFO in DGEVS calc: ",INFO
        !STOP "ERROR: linSolver_dgels got non zero return in calculation"
    ENDIF
END SUBROUTINE

subroutine linSolver_dgels(N,AIN,BIN,B)
    IMPLICIT none
    INTEGER, INTENT(in) :: N
    REAL*8, INTENT(in) :: AIN(N,N),BIN(N)
    REAL*8, INTENT(out) :: B(N)
    REAL*8 :: A(N,N)
    INTEGER :: M,NRHS,LDA,LDB,LWORK,INFO
    REAL*8 :: WORK(28975)

    M=N
    NRHS=1
    LDA=N
    LDB=N
    LWORK=28975
    !ALLOCATE(WORK(LWORK))
    A=AIN
    B=BIN

    CALL DGELS("N",M,N,NRHS,A,LDA,B,LDB,WORK,LWORK,INFO)
    LWORK=int(WORK(1)) 
    IF (INFO.ne.0) THEN
        WRITE(6,'(A,I0)') "INFO in DGELS analysis: ",INFO
        !STOP "ERROR: linSolver_dgels got non zero return in analysis"
    ENDIF
    !DEALLOCATE(WORK)
    !ALLOCATE(WORK(LWORK))
    !CALL DGELS("N",M,N,NRHS,A,LDA,B,LDB,WORK,LWORK,INFO) 
    !IF (INFO.ne.0) THEN
    !    WRITE(6,'(A,I0)') "INFO in DGELS calc: ",INFO
    !    !STOP "ERROR: linSolver_dgels got non zero return in calculation"
    !ENDIF
END SUBROUTINE

subroutine linSolver_dgelsd(N,AIN,BIN,B)
    INTEGER, INTENT(in) :: N
    REAL*8, INTENT(in) :: AIN(N,N),BIN(N)
    REAL*8, INTENT(out) :: B(N)
    REAL*8 :: A(N,N),S(N)
    INTEGER :: M,NRHS,LDA,LDBL,LWORK,INFO,RANK
    INTEGER :: IWORK(289750)
    REAL*8 :: WORK(289750)
    REAL*8 :: RCOND

    M=N
    NRHS=1
    LDA=N
    LDB=N
    LWORK=289750
    RCOND=1e-8
    A=AIN
    B=BIN

    CALL DGELSD(M,N,NRHS,A,LDA,B,LDB,S,RCOND,RANK,WORK,LWORK,IWORK,INFO)
    IF (INFO.ne.0) THEN
        WRITE(6,'(A,I0)') "INFO in DGELS analysis: ",INFO
    ENDIF
    WRITE(6,'(A,I0)') "Effective Rank of matrix =", RANK
END SUBROUTINE
end module
