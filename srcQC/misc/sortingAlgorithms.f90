!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE sortingAlgorithms
IMPLICIT none

CONTAINS
RECURSIVE SUBROUTINE QSortInt(nA,A,O)
INTEGER, INTENT(in) :: nA
INTEGER, INTENT(inout) :: A(nA),O(nA)

INTEGER :: left,right
INTEGER :: temp
INTEGER :: marker
INTEGER :: pivot
REAL*8 :: random

IF (nA.gt.1) THEN
    CALL random_number(random)
    pivot=A(int(random*real(nA-1))+1) !random pivot
    left=0
    right=nA+1
    DO WHILE (left<right)
        right=right-1
        DO WHILE (A(right)>pivot)
            right=right-1 
        ENDDO
        left=left+1
        DO WHILE (A(left)<pivot)
            left=left+1
        ENDDO
        IF (left<right) THEN
            temp=A(left)
            A(left)=A(right)
            A(right)=temp
            temp=O(left)
            O(left)=O(right)
            O(right)=temp
        ENDIF
    ENDDO
    IF (left.eq.right) THEN 
        marker=left+1
    ELSE
        marker=left
    ENDIF
    CALL QSortInt(marker-1,A(:marker-1),O(:marker-1))
    CALL QSortInt(nA-marker+1,A(marker:),O(marker:))
ENDIF
END SUBROUTINE
END MODULE
