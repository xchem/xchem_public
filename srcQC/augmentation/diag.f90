!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

SUBROUTINE diag(r,in_mat,out_mat,w)
IMPLICIT none 
	
	INTEGER, INTENT(IN) :: r
	INTEGER :: ierr
	REAL*8, DIMENSION(r,r), INTENT(in) :: in_mat	
	REAL*8, DIMENSION(r,r), INTENT(out) :: out_mat
	REAL*8, DIMENSION(r,r) :: re_d, im_d	 
	REAL*8, DIMENSION(r,r) :: re_t, im_t !temporary
	REAL*8, DIMENSION(r) :: w, fv1, fv2
	REAL*8, DIMENSION(2,r) :: fm1
	
	re_t=in_mat
	im_t=0.d0
	
	CALL ch(r,r,re_t,im_t,w,2,re_d,im_d, fv1, fv2, fm1, ierr)
        IF (MAXVAL(im_d).gt.1e-20) THEN
                PRINT*, "WARNING: complex part arises in diagonalization of real hermitian matrix"
        ENDIF
	out_mat=re_d
END SUBROUTINE diag

SUBROUTINE diag_cmplx(r,in_mat,out_mat,w)
IMPLICIT none
        
        INTEGER, INTENT(IN) :: r
        INTEGER :: ierr 
        COMPLEX *16, DIMENSION(r,r), INTENT(in) :: in_mat       
        REAL*8, DIMENSION(r,r) :: re_d, im_d
        COMPLEX*16, DIMENSION(r,r) :: out_mat
        REAL*8, DIMENSION(r,r) :: re_t, im_t !temporary
        REAL*8, DIMENSION(r) :: w, fv1, fv2
        REAL*8, DIMENSION(2,r) :: fm1
        
        re_t=REAL(in_mat)
        im_t=AIMAG(in_mat)
        
        CALL ch(r,r,re_t,im_t,w,2,re_d,im_d, fv1, fv2, fm1, ierr)
        
        out_mat=DCMPLX(re_d,im_d)
END SUBROUTINE diag_cmplx	

subroutine order(A,n,v,desc)
implicit none
integer, intent(in) :: n
real*8, intent(inout) :: A(n,n)
real*8, intent(inout) ::  v(n)
logical, intent(in) :: desc

real*8 kk
integer n2

integer i,j,k

do i=1,n-1
 do j=i+1,n
  if (v(j).le.v(i)) then
   kk=v(j)
   v(j)=v(i)
   v(i)=kk
   do k=1,n
    kk=A(k,j)
    A(k,j)=A(k,i)
    A(k,i)=kk
   enddo
  endif
 enddo
enddo

if (desc) then
 n2=n
 if (mod(n,2).eq.1) n2=n-1
 do i=1,n2/2
  kk=v(i)
  v(i)=v(n-i+1)
  v(n-i+1)=kk
  do k=1,n
   kk=A(k,i)
   A(k,i)=A(k,n-i+1)
   A(k,n-i+1)=kk
  enddo
 enddo
endif
return
end
