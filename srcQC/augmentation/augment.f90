!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program augment

implicit none
integer npot,norb,nDet,norb0
real*8, allocatable :: CIDet(:,:)
integer, allocatable :: Detlist(:,:),tmpDet(:)

character*100 fich,tmpchar
integer i,iDet,j
integer signo,spin
real*8, allocatable :: innorm(:),outnorm(:)

write(6,*) "CI file to be augmented (in Slater Determinants format)"
read(5,*) fich

open(1,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File with CI vector in Slater does not exist ",trim(fich)
 stop 'File with CI vector is compulsory'
endif
read(1,*) npot,nDet,norb0
allocate(CIDet(npot,nDet),Detlist(nDet,norb0))
allocate(innorm(npot),outnorm(npot))
do iDet=1,nDet
 read(1,*) tmpchar,(CIDet(i,iDet),i=1,npot)
 read(tmpchar,"(100I1.1)") (Detlist(iDet,i),i=1,norb0)
enddo
write(6,*) "CI ready, minimum augmentation ",norb0
do i=1,npot
 innorm(i)=0.
 do iDet=1,nDet
  innorm(i)=innorm(i)+CIDet(i,iDet)**2
 enddo
enddo
write(6,*) "Beta or alfa augmentations"
read(5,*) fich
select case(fich)
 case("alpha")
  spin=1
  write(6,*) "alpha"
 case("beta")
  spin=2
  write(6,*) "beta"
 case default
  stop 'The augmented electron must be alpha or beta'
end select

write(6,*) "Number of orbitals to be augmented"
read(5,*) norb
if (norb.lt.norb0) stop 'Number of augmentation cannot be lower than the active space'

allocate(tmpDet(norb))
do i=1,norb
 if (spin.eq.1) then
  write(fich,"(A,I2.2,A)") "augment.a-",i,".det"
 else
  write(fich,"(A,I2.2,A)") "augment.b-",i,".det"
 endif
 write(6,*) "Writting augmentation orbital ",i,trim(fich)
 open(1,file=fich)
 write(1,"(3(x,I10.10))") npot,nDet,norb

 do j=1,npot
  outnorm(j)=0.
 enddo

 do iDet=1,nDet
  tmpDet=0
!!! Copying the Determinant up to norb0 and the rest are 0
  do j=1,norb0
   tmpdet(j)=Detlist(iDet,j)
  enddo

  if (tmpdet(i).eq.3 .or. tmpdet(i).eq.spin) then
!!! Impossible to augment this Det
   signo=0
  else
!!! Counting electrons to know the sign until the augmenting orbital
   signo=1
   if (i.ne.1) then
    do j=1,i-1
     if (tmpdet(j).eq.1 .or. tmpdet(j).eq.2) signo=-signo
    enddo
   endif
!!! Depending on the augmetation spin create the new Det
   if (tmpdet(i).eq.0) then
    tmpdet(i)=spin
   else 
    tmpdet(i)=3
    if (spin.eq.2) signo=-signo
   endif
  endif
  write(tmpchar,"(100I1.1)") (tmpdet(j),j=1,norb)
  write(1,"(A,1000(x,E40.30e3))") trim(tmpchar),(float(signo)*CIDet(j,iDet),j=1,npot)
  do j=1,npot
   outnorm(j)=outnorm(j)+(float(signo)*CIDet(j,iDet))**2
  enddo
 enddo
 close(1)
 do j=1,npot
  write(6,*) "Norms ",innorm(j),outnorm(j)
 enddo
enddo

end program augment

