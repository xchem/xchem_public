!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

subroutine GramSchmidt(nbas,nvec,A,overlap)
implicit none
integer, intent(in) :: nbas,nvec
real*8, intent(inout) :: A(nbas,nvec)
!!! If overlap does not exist, the bas is considered orthonormal (check ort)
real*8, optional, intent(in) :: overlap(nbas,nbas)

real*8 newA(nbas,nvec)
real*8 crossov,norm(nvec)
integer ibas,ivec,jbas,jvec
real*8 ov(nbas,nbas)
logical ort

norm=0.
if (present(overlap)) then
 ov=overlap
 ort=.false.
else
 ort=.true.
 ov=0.
endif


!!! First vector is not touched and the rest are restarted
do ibas=1,nbas
 newA(ibas,1)=A(ibas,1)
enddo
norm(1)=0.
do ibas=1,nbas
 if ( ort ) then
  norm(1)=norm(1)+newA(ibas,1)**2
 else
  do jbas=1,nbas
   norm(1)=norm(1)+newA(ibas,1)*newA(jbas,1)*ov(ibas,jbas)
  enddo
 endif
enddo
!! The rest are 0
do ivec=2,nvec
 do ibas=1,nbas
  newA(ibas,ivec)=0.
 enddo
enddo

! Starting Gram-Schmidt 
do ivec=2,nvec
!! Projecting over the previous orthonormalized orbitals
 do jvec=1,ivec-1
  crossov=0.
  do ibas=1,nbas
   if ( ort ) then
    crossov=crossov+newA(ibas,jvec)*A(ibas,ivec)
   else
    do jbas=1,nbas
     crossov=crossov+newA(ibas,jvec)*A(jbas,ivec)*ov(ibas,jbas)
    enddo
   endif
  enddo
  do ibas=1,nbas
   newA(ibas,ivec)=newA(ibas,ivec)+crossov/norm(jvec) *newA(ibas,jvec)
  enddo
 enddo
 do ibas=1,nbas
  newA(ibas,ivec)=A(ibas,ivec)-newA(ibas,ivec)
 enddo
 norm(ivec)=0.
 do ibas=1,nbas
  if ( ort ) then
   norm(ivec)=norm(ivec)+newA(ibas,ivec)**2
  else
   do jbas=1,nbas
    norm(ivec)=norm(ivec)+newA(ibas,ivec)*newA(jbas,ivec)*ov(ibas,jbas)
   enddo
  endif
 enddo
enddo

!!! Normalizing and saving
do ivec=1,nvec
 IF (norm(ivec).le.0) THEN
   WRITE(6,'(A,I0,A,F16.12)') "WARNING: norm of vector ",ivec," from GS has norm ",norm(ivec)
 ENDIF
 do ibas=1,nbas
  A(ibas,ivec)=newA(ibas,ivec)/sqrt(norm(ivec))
 enddo
enddo

return
end   
