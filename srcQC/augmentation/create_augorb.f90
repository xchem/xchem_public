!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program create_augorb
implicit none
integer nsym
integer :: nbas,totbas
real*8, allocatable :: orb(:,:),overlap(:,:)
character*100 fich,typov,typorb
real*8 epsilon1

logical ex
integer isym
integer i,j,lind

write(6,*) "File with the overlap of the basis and type (text,binary)"
read(5,*) fich,typov
inquire(file=fich,exist=ex)
if (ex) then
 write(6,*) "Reading symmetry definitions and basis overlap from ",trim(fich)
else
 stop 'We need a file with the overlap'
endif
if (typov=="text") then
 open(1,file=fich)
else if (typov=="binary") then
 open(1,file=fich,form="unformatted")
else
 stop "The file format should be text or binary"
endif

write(6,*) "File to write the new orbitals and type (MOLCAS,binary,free)"
read(5,*) fich,typorb
if (typorb=="binary") then
 open(2,file=fich,form="unformatted")
else if (typorb=="free") then
 open(2,file=fich)
else if (typorb=="MOLCAS") then
 open(2,file=fich)
else
 stop "The orbital file should be MOLCAS, binary or free"
endif

write(6,*) "Epsilon to remove linear dependencies"
read(5,*) epsilon1

totbas=0
if (typov=="binary") read(1)   nsym
if (typov=="text")   read(1,*) nsym
write(6,*) "Number of symmetries ",nsym
do isym=1,nsym
 if (typov=="text")   read(1,*) nbas
 if (typov=="binary") read(1)   nbas
! write(6,*) "Reading symmetry ",isym,nsym,nbas
 totbas=totbas+nbas
 if (nbas.ne.0) then
  allocate(overlap(nbas,nbas),orb(nbas,nbas))
  do i=1,nbas
   if (typov=="text")   read(1,*) (overlap(i,j),j=1,i)
   if (typov=="binary") read(1)   (overlap(i,j),j=1,i)
  enddo
  do i=1,nbas
   do j=1,i
    overlap(j,i)=overlap(i,j)
   enddo
  enddo


!! The orbital overlap is considered in the augmented basis
  orb=overlap
!!! Obtaining a linearly independent combination for the augmented basis
  call junk(nbas,orb,epsilon1,lind)

  write(6,*) "Number of linear independent orbitals of sym ",isym,lind,nbas
  call GramSchmidt(nbas,lind,orb,overlap)

  if (typorb=="MOLCAS") then
   do i=1,nbas
    if (i.le.lind) then
     write(2,"(A,I1,I3)") "* Linear independent orb ",isym,i
    else
     write(2,"(A,I1,I3)") "* Linear dependent orb ",isym,i
    endif
    write(2,"(4(E18.12e2))") (orb(j,i),j=1,nbas)
   enddo
  endif
  if (typorb=="free") write(2,"(5(x,E20.10e3))") ((orb(j,i),j=1,nbas),i=1,nbas)
  if (typorb=="binary") write(2)                 ((orb(j,i),j=1,nbas),i=1,nbas)
  
  deallocate(orb,overlap)
 endif
enddo

end


subroutine junk(nbas,orb,epsilon1,lind)
implicit none
integer, intent(in) :: nbas
integer, intent(out) :: lind
real*8, intent(in) :: epsilon1
real*8, intent(inout) :: orb(nbas,nbas)
real*8 eig(nbas)

integer i,j
real*8 kk

! write(6,*) "Junking ",nbas
lind=nbas
call diag(orb,nbas,eig)
call order(orb,nbas,eig,.true.)
do i=1,nbas
 if (eig(i).lt.epsilon1) then
  if (eig(i-1).ge.epsilon1) lind=i-1
  do j=1,nbas
   orb(j,i)=0.
  enddo
 endif
enddo

return
end
