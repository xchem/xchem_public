!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program Dyson

implicit none
integer :: nrneu,nrcat0,nrcat,norb,ircat
!!! To create the molden file, the CAS should be defined
integer :: totorb

character*100 fich

integer :: neuDets,catDets
real*8, allocatable :: nCIDet(:,:),cCIDet(:,:)
integer*1, allocatable :: nDetlist(:,:),cDetlist(:,:)

real*8, allocatable :: basorb(:,:)
real*8, allocatable :: Dys(:,:,:)
real*8, allocatable :: dysnorm(:)
integer, allocatable :: orb(:)
real*8, allocatable :: catov(:,:)


integer :: n,i
logical :: ex,molden

integer :: iDet,iroot,ineu,icat,isp
real*8 normCI,normDet
real*8 :: ov


write(6,*) "Number of orbitals in the CAS: active and total (common for neutral and cation)"
read(5,*) norb,totorb
allocate (orb(norb),basorb(totorb,norb) )
if (totorb.eq.0) then
 write(6,*) "The total number of orbitals are not known, the orbital is not gonna be read in the basis set"
 molden=.false.
else
 molden=.true.
 write(6,*) "Identification of the CAS orbitals in the molden file (number)"
 do n=1,norb
  write(6,*) "Orbital ",n," Identification number and file with expansion (molden format)"
  read(5,*) orb(n),fich
  write(6,*) "Reading orbital from ",fich
  inquire(file=fich,exist=ex)
  if (ex) then
   open(1,file=fich)
   do i=1,totorb
    read(1,*) iDet,basorb(i,n)
   enddo
   close(1)
   write(6,*) "Orbital ready"
  else
   write(6,*) "Orbital file does not exist ",trim(fich)
   write(6,*) "Terminatting"
   stop
  endif
 enddo
endif



!! NEUTRAL
write(6,*) "File with the CI vector for the neutral (Slater)"
read(5,*) fich
open(1,file=fich,status="old",iostat=n)
if (n.ne.0) then
 write(6,*) "File with the CI vector for neutral does not exist ",trim(fich)
 stop 'File with the CI vector for neutral is not optional'
endif
read(1,*) nrneu,neuDets,norb
allocate(nDetList(neuDets,norb),nCIDet(nrneu,neuDets))
do iDet=1,neuDets
 read(1,*) fich,(nCIDet(iroot,iDet),iroot=1,nrneu)
 read(fich,"(1000I1.1)") (nDetList(iDet,i),i=1,norb)
enddo
write(6,*) "Neutral ready"

!! CATION
write(6,*) "File with the CI vector for the cation using neutral orb (Slater)"
read(5,*) fich
open(1,file=fich,status="old",iostat=n)
if (n.ne.0) then
 write(6,*) "File with the CI vector for neutral does not exist ",trim(fich)
 stop 'File with the CI vector for neutral is not optional'
endif
read(1,*) nrcat,catDets,i
if (i.ne.norb) then
 write(6,*) "Number of orbitals in cation and neutral must be equal"
 write(6,*) "CATION: ",norb
 write(6,*) "NEUTRAL: ",i
 stop "Number of orbitals in cation and neutral must be equal"
endif
allocate(cDetList(catDets,norb),cCIDet(nrcat,catDets))
do iDet=1,catDets
 read(1,*) fich,(cCIDet(iroot,iDet),iroot=1,nrcat)
 read(fich,"(1000I1.1)") (cDetList(iDet,i),i=1,norb)
enddo
write(6,*) "Cation ready"


!KLK
print*, "Number of desired states for ion with ionic orbitals (overlap needs to be sized accordingly)"
read(5,*) ircat
WRITE(6,'(A,x,I4)'), "ircat",ircat
!KLK

allocate(catov(ircat,nrcat) )
write(6,*) "Checking if there is overlap.dat containing the cionly and CAS cation overlap"
write(6,*) "This will transform the Dyson orbital to the real eigenfunctions of the cation"
inquire(file="overlap.dat",exist=ex)
if (ex) then
 write(6,*) "Reading the overlap"
 open(1,file="overlap.dat")
 do iroot=1,ircat
  read(1,*) (catov(iroot,n),n=1,nrcat)
  ov=0.d0
  do n=1,nrcat
   ov=ov+catov(iroot,n)**2
  enddo     
  write(6,'(A,x,I4,x,A,x,F20.17)'), "State",iroot,"overlaps with nuetral by",ov
 enddo
else
 write(6,*) "Considering overlap unit matrix, i.e. the same orbitals of the cation in the eigenfunction"
 catov=0.d0
 do iroot=1,ircat
  catov(iroot,iroot)=1.d0
 enddo
endif

!!! COMPARING NEUTRAL AND CATION AND TAKING THE DYSON ORBITAL
write(6,*) "Calculating the occupations of the Dyson orbital"
if (molden) then
 open(2,file="dyson.molden")
 write(6,*) "Dyson orbitals are gonna be written in 'dyson.molden'"
endif
open(3,file="dyson_norm.out")
allocate (dysnorm(ircat))
allocate( Dys(2,norb,nrcat) )

open(11,file="dyson.orb")
n=0
do ineu=1,nrneu
 do icat=1,nrcat
  write(6,*) "Working pair roots neutral cation: ",ineu,icat
  call Dysonorb(norb,neuDets,nDetlist,nCIDet(ineu,:),catDets,cDetlist,cCIDet(icat,:),Dys(:,:,icat) )
 enddo
 
 ! Getting the transformation to the new eigenfunctions of the cation (catov)
 ! Projection the cation relaxed eigenfunctions into the neutral orbitals
 do i=1,norb
  do isp=1,2
   do icat=1,ircat !with ionic orbitals
    dysnorm(icat)=0.d0
    do iroot=1,nrcat !with neutral orbitals
     dysnorm(icat)=dysnorm(icat)+catov(icat,iroot)*Dys(isp,i,iroot)
    enddo
   enddo
   do icat=1,ircat
    Dys(isp,i,icat)=dysnorm(icat)
   enddo
  enddo
 enddo
 
 
 do icat=1,ircat
  write(11,"(A6,I3.3,A1,I3.3,A4)") "dyson_",ineu,"-",icat,".orb"
  write(6,*) "Neutral and cation states ",ineu,icat
  dysnorm(icat)=0.d0
  do i=1,norb
   do isp=1,2
    dysnorm(icat)=dysnorm(icat)+Dys(isp,i,icat)**2
   enddo
  enddo
  write(11,*) dysnorm(icat)
  do i=1,norb
   write(11,"(2(x,E20.10e3))") Dys(:,i,icat)
  enddo
  
  write(6,*) "Norm**2 ",dysnorm(icat)
 
  n=n+1
  if (molden) then
   write(6,*) "Writting orbital in molden format ",n 
   call writmolden(norb,totorb,Dys(:,:,icat),n,2,basorb)
  endif
 enddo
 do icat=1,nrcat
  write(3,*) ineu-0.5,icat-0.5,dysnorm(icat)
  write(3,*) ineu-0.5,icat+0.5,dysnorm(icat)
 enddo
 write(3,*) ""
 do icat=1,nrcat
  write(3,*) ineu+0.5,icat-0.5,dysnorm(icat)
  write(3,*) ineu+0.5,icat+0.5,dysnorm(icat)
 enddo
 write(3,*) ""
enddo
  


end program dyson

subroutine Dysonorb(norb,nDets,nList,nCI,cDets,cList,cCI,Dys)
implicit none
integer, intent(in) :: norb
!! NEUTRAL
integer, intent(in) :: nDets
integer*1, intent(in) :: nList(nDets,norb)
real*8, intent(in) :: nCI(nDets)
!! CATION
integer, intent(in) :: cDets
integer*1, intent(in) :: cList(cDets,norb)
real*8, intent(in) :: cCI(cDets)
!!! DYSON
real*8, intent(out) :: Dys(2,norb)

integer :: inDet,icDet,ndif,difforb,isp
integer :: n,csign
real*8 :: kk

Dys=0.d0
do inDet=1,nDets
 do icDet=1,cDets
  ndif=0
  do n=1,norb
   if (cList(icDet,n).ne.nList(inDet,n) ) then
    ndif=ndif+1
    difforb=n
   endif
  enddo
  if (ndif.eq.1) then
   kk=cCI(icDet)*nCI(inDet)

   csign=1
   if (difforb.ne.1) then
    do n=1,difforb-1
     if (cList(icDet,n).eq.1 .or. cList(icDet,n).eq.2) csign=-1*csign
    enddo
   endif
   if (cList(icDet,difforb).eq.0) then
    if (nList(inDet,difforb).eq.1) isp=1
    if (nList(inDet,difforb).eq.2) isp=2
    if (nList(inDet,difforb).eq.3) isp=0  !!! Only happen if they differ in more than one orb
   endif
   if (cList(icDet,difforb).eq.1) then
!!! I have to create the electron for the cation after the alpha
    csign=-1*csign
    if (nList(inDet,difforb).eq.0) isp=0
    if (nList(inDet,difforb).eq.2) isp=0
    if (nList(inDet,difforb).eq.3) isp=2
   endif
   if (cList(icDet,difforb).eq.2) then
!!! I have to create the electron for the cation after before the alpha
    csign=+1*csign
    if (nList(inDet,difforb).eq.0) isp=0
    if (nList(inDet,difforb).eq.1) isp=0
    if (nList(inDet,difforb).eq.3) isp=1
   endif
!!! In principle this cannot happen the electron should be missing in the cat
   if (cList(icDet,difforb).eq.3) then
    if (nList(inDet,difforb).eq.0) isp=0
    if (nList(inDet,difforb).eq.1) isp=0
    if (nList(inDet,difforb).eq.2) isp=0
   endif
   if (isp.ne.0) then
    Dys(isp,difforb)=Dys(isp,difforb)+kk*float(csign)
   else 
    write(6,*) "Warning the cation has more electron that neutral or the difference is more than 1 e!!"
    write(6,*) "NEUTRAL"
    write(6,"(50(x,I1.1))") nList(inDet,:)
    write(6,*) "CATION"
    write(6,"(50(x,I1.1))") cList(icDet,:)
    write(6,*) "Different orbital ",difforb,nList(inDet,difforb),cList(icDet,difforb)
    stop
   endif

  endif
 enddo
enddo

return
end subroutine Dysonorb

subroutine writmolden(norb,totorb,Dys,ndys,un,basis)
implicit none
integer, intent(in) :: norb,totorb,un,ndys
real*8, intent(in) :: Dys(2,norb),basis(totorb,norb)

integer :: i,j
real*8 kk,dysnorm

dysnorm=0.d0
do j=1,norb
 dysnorm=dysnorm+Dys(1,j)**2
enddo
write(un,"(A5,I6,A1)") " Sym=",ndys,"a"
kk=0.d0
write(un,"(A4,F11.4)") "Ene=",kk
write(un,"(A12)")      " Spin=Alpha"
write(un,"(A6,F11.5)") "Occup=",dysnorm
do i=1,totorb
 kk=0.d0
 do j=1,norb
  kk=kk+Dys(1,j)*basis(i,j)
 enddo
 write(un,"(I4,F19.9)") i,kk
enddo

dysnorm=0.d0
do j=1,norb
 dysnorm=dysnorm+Dys(2,j)**2
enddo
write(un,"(A5,I6,A1)") " Sym=",ndys,"a"
kk=0.d0
write(un,"(A4,F11.4)") "Ene=",kk
write(un,"(A11)")      " Spin=Beta"
write(un,"(A6,F11.5)") "Occup=",dysnorm
do i=1,totorb
 kk=0.d0
 do j=1,norb
  kk=kk+Dys(2,j)*basis(i,j)
 enddo
 write(un,"(I4,F19.9)") i,kk
enddo

end subroutine writmolden
