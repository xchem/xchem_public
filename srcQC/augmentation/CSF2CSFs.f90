!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program CSF2CSFs
implicit none
integer npot,nCSF,nblocks,npotblock
real*8, allocatable :: CI(:,:)
character*100 reorder

character*100 fich
integer iroot,iCSF
integer i,nactual
integer, allocatable :: potblock(:)
character*100 way


write(6,*) "File with the total CI vector"
read(5,*) fich,way
write(6,*) trim(fich)," ",trim(way)
call readCSF0(fich,way,1,npot,nCSF)
write(6,*) "Roots :",npot
write(6,*) "CSFs  :",nCSF
allocate(CI(nCSF,npot))
call readCSF(way,1,npot,nCSF,CI)

write(6,*) "Data ready, writting in blocks"

write(6,*) "Do you want to reorder the data (yes or no)?"
read(5,*) reorder
write(6,*) reorder
select case (reorder)
 case("no")
  write(6,*) "No reorder"
 case("manual")
  write(6,*) "Manual reorder"
 case("parent")
  write(6,*) "Reorder by parent"
 case default
  stop 'Reorder: no, manual, parent'
end select

nactual=0
write(6,*) "How many blocks?"
read(5,*) nblocks
do i=1,nblocks
 read(5,*) npotblock,fich
 allocate(potblock(npotblock))

 select case (reorder)
  case("no")
  do iroot=1,npotblock
   potblock(iroot)=iroot+nactual
  enddo
  nactual=nactual+npotblock
 case("manual")
  read(5,*) (potblock(iroot),iroot=1,npotblock)
 case("parent")
  potblock(1)=i
  do iroot=2,npotblock
   potblock(iroot)=potblock(iroot-1)+nblocks
  enddo
 end select

 write(6,"(A,A,1000(x,I3.3))") "Writing in ",trim(fich),(potblock(iroot),iroot=1,npotblock)
 open(1,file=fich,form="unformatted")
 write(1) npotblock,nCSF
 do iroot=1,npotblock
  write(1) (CI(iCSF,potblock(iroot)),iCSF=1,nCSF)
 enddo
 close(1)
 deallocate(potblock)
enddo

end program
