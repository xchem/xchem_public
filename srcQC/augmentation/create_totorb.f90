!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program create_totorb
implicit none
integer nsym1
integer nbas0,nbas1,norb0,nbas,norb
integer, allocatable :: norb1(:)
real*8, allocatable :: orb(:,:),overlap(:,:)
real*8, allocatable :: orb_local(:,:),orb_aug(:,:)
real*8 epsilon1

integer isym

character*100 fich,typ
integer i,j,norb1n
logical ex

!! The overlap basis is used to know the number of basis functions
!! and to orthonormalize
write(6,*) "File with the overlap of the basis and file type (text,binary)"
read(5,*) fich,typ
inquire(file=fich,exist=ex)
if (ex) then
 write(6,*) "Reading overlap from ",trim(fich)

 if ( typ == "text" ) then
  open(1,file=fich)
  read(1,*) i
 else if ( typ == "binary" ) then
  open(1,file=fich,form="unformatted")
  read(1) i
 else
  stop 'Binary or text format'
 endif

 if (i.ne.1) then
  write(6,*) "The program is only working with no symmetry in the localized orbitals"
  stop 'Symmetry is still not implemented'
 endif

else
 write(6,*) "File with overlap is not optional ",trim(fich)
 stop 'Overlap file does not exist '
endif

if (typ=="binary") read(1) nbas
if (typ=="text") read(1,*) nbas

allocate (overlap(nbas,nbas),orb(nbas,nbas))
write(6,*) "Using basis functions ",nbas
do i=1,nbas
 if (typ=="text") read(1,*) (overlap(i,j),j=1,i)
 if (typ=="binary") read(1) (overlap(i,j),j=1,i)
enddo
close(1)
do i=1,nbas
 do j=1,i
  overlap(j,i)=overlap(i,j)
 enddo
enddo
write(6,*) "Overlap basis ready"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!! Let's read the local orbitals
write(6,*) "File with the local orbitals and format (MOLCAS, binary, free)"
read(5,*) fich,typ
inquire(file=fich,exist=ex)
if (ex) then
 write(6,*) "Reading the local orbitals from ",trim(fich)
 if (typ=="binary") then
  open(1,file=fich, form="unformatted")
 else if (typ=="free") then
  open(1,file=fich)
 else if (typ=="MOLCAS") then
  open(1,file=fich)
 else
  stop 'Orbitals format must be MOLCAS, free or binary'
 endif

else
 write(6,*) 'File with augmented orbitals does not exist ',trim(fich)
 stop 'File with augmented orbitals does not exist '
endif
write(6,*) "Number of local basis functions and local orbitals"
read(5,*) nbas0,norb0
if (norb0.gt.nbas0) then
 stop 'The number of orbitals cannot be bigger than the basis'
endif
if (nbas0.gt.nbas) then
 stop 'The local basis should be smaller than the total basis'
endif
allocate(orb_local(nbas0,norb0))
if (typ=="MOLCAS") then
 do i=1,norb0
  read(1,*) 
  read(1,"(4(E18.12e2))") (orb_local(j,i),j=1,nbas0)
 enddo
endif
if (typ=="free") read(1,*) ((orb_local(j,i),j=1,nbas0),i=1,norb0)
if (typ=="binary") read(1) ((orb_local(j,i),j=1,nbas0),i=1,norb0)
close(1)
write(6,*) "Local orbitals ready"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
orb=0.
do i=1,norb0
 do j=1,nbas0
  orb(j,i)=orb_local(j,i)
 enddo
enddo
norb=norb0

!!!!!!!!!!!!!!!!!! Let's read the augmented orbitals by symmetry order
write(6,*) "File with the augmented orbitals and format (MOLCAS,free or binary)"
read(5,*) fich,typ
inquire(file=fich,exist=ex) 
if (ex) then
 write(6,*) "Reading augmented guess orbitals from ",trim(fich)

 if (typ=="MOLCAS") then
  open(1,file=fich)
 else if (typ=="free") then
  open(1,file=fich)
 else if (typ=="binary") then
  open(1,file=fich,form="unformatted")
 else 
  stop 'Orbitals must be MOLCAS, binary or free'
 endif

else
 write(6,*) 'File with augmented orbitals does not exist ',trim(fich)
 stop 'File with augmented orbitals does not exist '
endif
nbas1=nbas-nbas0
write(6,*) "How many symmetry blocks are in the augmented orbitals"
read(5,*) nsym1
allocate (norb1(nsym1))
write(6,*) "Number of orbitals in every symmetry"
read(5,*) norb1(:)
write(6,*) "Considering basis functions ",nbas1

write(6,*) "Fix an epsilon to remove too similar orbitals"
read(5,*) epsilon1

norb=norb0
do isym=1,nsym1
 write(6,*) "Checking block ",isym," with ",norb1(isym)," orbitals"
 allocate(orb_aug(nbas1,norb1(isym)) )
 if (typ=="MOLCAS") then
  do i=1,norb1(isym)
   read(1,*)
   read(1,"(4(E18.12e2))") (orb_aug(j,i),j=1,nbas1)
  enddo
 endif
 if (typ=="free") read(1,*) ((orb_aug(j,i),j=1,nbas1),i=1,norb1(isym))
 if (typ=="binary") read(1) ((orb_aug(j,i),j=1,nbas1),i=1,norb1(isym))
 
!!! Projecting the augmented basis into the localized orbital and removing linear dependencies
 i=norb1(isym)
 call junk(nbas0,norb0,orb_local,nbas1,i,orb_aug, epsilon1,overlap,norb1n)
 write(6,*) "Changing the number of orbitals ",norb1n,i
 do i=1,norb1n
  do j=1,nbas1
   orb(j+nbas0,norb+i)=orb_aug(j,i)
  enddo
 enddo
 norb=norb+norb1n
 write(6,*) "New number of orbitals ",norb
 deallocate(orb_aug)
enddo
allocate(orb_aug(nbas,norb))
do i=1,norb
 do j=1,nbas
  orb_aug(j,i)=orb(j,i)
 enddo
enddo
!!! Orthonormalizing the set of augmented basis with the localized orbitals
call GramSchmidt(nbas,norb,orb_aug,overlap)
orb=0.
do i=1,norb
 do j=1,nbas
  orb(j,i)=orb_aug(j,i)
 enddo
enddo

write(6,*) "File with the output orbitals (it will be created in Orb, MO and bin formats)"
read(5,*) fich
typ=trim(fich)//".Orb"
open(1,file=typ)
do i=1,nbas
 if (i.le.norb0) then
  write(1,"(A,2(x,I4))") "* Local orbital ",i,i
 else if (i.le.norb) then
  write(1,"(A,2(x,I4))") "* Orthonormalized aug orbital ",i,i-norb0
 else
  write(1,"(A)") "* Zero orbital"
 endif
 write(1,"(4(E18.12e2))") (orb(j,i),j=1,nbas)
enddo
close(1)
typ=trim(fich)//".MO"
open(1,file=typ)
write(1,"(5(x,E30.20e3))") ((orb(j,i),j=1,nbas),i=1,nbas)
close(1)
typ=trim(fich)//".bin"
open(1,file=typ,form="unformatted")
write(1) ((orb(j,i),j=1,nbas),i=1,nbas)
close(1)

end

subroutine junk(nbas0,norb0,orb_local,nbas1,norb1,orb_aug, epsilon1,ov,norbf)
implicit none
integer, intent(in) :: nbas0,norb0,nbas1
real*8, intent(in) :: orb_local(nbas0,norb0),epsilon1
real*8, intent(in) :: ov(nbas0+nbas1,nbas0+nbas1)
integer, intent(in) :: norb1
integer, intent(out) :: norbf
real*8, intent(inout) :: orb_aug(nbas1,norb1)

real*8 tmpov(norb0,norb1),tmporb(nbas1,norb1)
real*8 proj(norb1,norb1),eig(norb1)

integer i,j,k,l

!write(6,*) "Entering junk"
tmporb=orb_aug
do i=1,norb0
 do j=1,norb1
  tmpov(i,j)=0.
  do k=1,nbas0
   do l=1,nbas1
    tmpov(i,j)=tmpov(i,j)+orb_local(k,i)*orb_aug(l,j)*ov(k,nbas0+l)
   enddo
  enddo
 enddo
enddo
proj=matmul(transpose(tmpov),tmpov)
call diag(proj,norb1,eig)
orb_aug=0.
l=0
do i=norb1,1,-1
 if (eig(i).lt.(1.-epsilon1)) then
  l=l+1
  do j=1,nbas1
   orb_aug(j,l)=0.
   do k=1,norb1
    orb_aug(j,l)=orb_aug(j,l)+proj(k,i)*tmporb(j,k)
   enddo
  enddo
 else
  write(6,*) "Orbital ",i," rejected with overlap ",eig(i)
 endif
enddo

!write(6,*) "Exiting junk"
norbf=l

return
end

