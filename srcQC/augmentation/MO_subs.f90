!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

subroutine readover(n,ov,fich,way)
implicit none
character*100, intent(in) :: fich,way
integer, intent(out) :: n
real*8,intent(out), allocatable :: ov(:,:)

logical ex
integer i,j

inquire(file=fich,exist=ex)
if (ex) then
 if (way == "binary") then
  open(1,file=fich,form="unformatted")
 else if (way == "text") then
  open(1,file=fich)
 else
  stop "Overlap file must be binary or text"
 endif
else
 stop 'Not overlap file'
endif

if (way=="binary") read(1) i
if (way=="text") read(1,*) i
if (i.ne.1) stop 'No symmetry is allowed'

if (way=="binary") read(1) n
if (way=="text") read(1,*) n
write(6,*) "Contracted basis functions ",n
allocate(ov(n,n) )
if (way=="binary") then
 do i=1,n
  read(1) (ov(i,j),j=1,i)
 enddo
endif
if (way=="text") read(1,*) ((ov(i,j),j=1,i),i=1,n)

do i=1,n
 do j=1,i
  ov(j,i)=ov(i,j)
 enddo
enddo

end subroutine readover

subroutine readMO(n,orb,fich,way)
implicit none
character*100, intent(in) :: fich,way
integer, intent(in) :: n
real*8,intent(out) :: orb(n,n)

logical ex
integer i,j,nsym,nbas

inquire(file=fich,exist=ex)
if (ex) then
 if (way == "binary") then
  open(1,file=fich,form="unformatted")
 else if (way == "MOLCAS") then
  open(1,file=fich)
 else if (way == "free") then
  open(1,file=fich)
 else
  stop "Orbital file must be binary, free or MOLCAS"
 endif
else
 stop 'Not orbital file'
endif

if (way=="binary") THEN
        read(1) nsym
        read(1) nbas
        print*, "DEUBG ",nSym,Nbas
        read(1) ((orb(j,i),j=1,n),i=1,n)
ENDIF
if (way=="free") read(1,*) ((orb(j,i),j=1,n),i=1,n)
if (way=="MOLCAS") then
 do i=1,n
  read(1,*)
  read(1,"(4E18.12e2)") (orb(j,i),j=1,n)
 enddo
endif

end subroutine readMO

subroutine writeMO(n,orb,fich,way)
implicit none
character*100, intent(in) :: fich,way
integer, intent(in) :: n
real*8,intent(in) :: orb(n,n)

logical ex
integer i,j

if (way == "binary") then
 open(1,file=fich,form="unformatted")
else if (way == "MOLCAS") then
 open(1,file=fich)
else if (way == "free") then
 open(1,file=fich)
else
 stop "Orbital file must be binary, free or MOLCAS"
endif

if (way=="binary") then
    write(1) 1
    write(1) n
    write(1) ((orb(j,i),j=1,n),i=1,n)
endif
if (way=="free") write(1,"(1000(x,E30.20e3))") ((orb(j,i),j=1,n),i=1,n)
if (way=="MOLCAS") then
 do i=1,n
  write(1,"(A,I5)") "* Orbital ",i
  write(1,"(4E18.12e2)") (orb(j,i),j=1,n)
 enddo
endif

end subroutine writeMO

subroutine MOov(n,orb1,orb2,overlap,ov)
implicit none
integer, intent(in) :: n
real*8, intent(in) :: orb1(n),orb2(n),overlap(n,n)
real*8, intent(out) :: ov

integer i,j
ov=0.
do i=1,n
 do j=1,n
  ov=ov+overlap(j,i)*orb1(j)*orb2(i)
 enddo
enddo

end subroutine MOov

