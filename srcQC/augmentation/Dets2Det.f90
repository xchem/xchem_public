!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program Dets2Det

use Detfile_

implicit none
integer ntot,nfil,norb,mdet
type(Detfile), allocatable :: Detblocks(:)
integer ifil,iDet,i,j
character*100 fich

integer, allocatable :: tmpdet(:)

logical, allocatable :: Detlist(:,:)
real*8, allocatable :: CIDet(:,:)
integer totDet
logical diffDet,same
real*8 norm

write(6,*) "Number of CI files to mix"
read(5,*) nfil
allocate (Detblocks(nfil))
mdet=0
ntot=0
do ifil=1,nfil
 read(5,*) fich
 open(1,file=fich,status="old",iostat=i)
 if (i.ne.0) stop 'File with the Determinants missing'
 write(6,*) "Reading ",ifil,trim(fich)
 call Detblocks(ifil)%readDetfile(1)
 close(1)
 if (ifil.eq.1) norb=Detblocks(ifil)%norb
 if (norb.ne.Detblocks(ifil)%norb) stop 'Files with different active space'
 mdet=mdet+Detblocks(ifil)%totalDets
 ntot=ntot+Detblocks(ifil)%npot
enddo
write(6,*) "Number of total roots ",ntot
write(6,*) "Maximum Determinants ",mdet

write(6,*) "Allocating supervector"
allocate(Detlist(mdet,2*norb),CIDet(ntot,mdet))

ntot=0
CIDet=0.
totDet=1
!!! First determinant (00000) is included
Detlist(totDet,:)=.false.

do ifil=1,nfil
 write(6,*) "Checking file ",ifil,Detblocks(ifil)%totalDets
 do iDet=1,Detblocks(ifil)%totalDets
  diffdet=.true.
  i=0
  do while (i.le.totDet .and. diffdet) 
   i=i+1
   same=.true.
   do j=1,2*norb
    !MARKUS make gfortran-fit
    !if (Detblocks(ifil)%Detlist(iDet,j).ne.Detlist(i,j)) same=.false.
    if (Detblocks(ifil)%Detlist(iDet,j).neqv.Detlist(i,j)) same=.false.
   enddo
   if (same) diffdet=.false.
  enddo
    
  if (diffdet) then
   totDet=totDet+1
   i=totDet
   Detlist(totDet,:)=Detblocks(ifil)%Detlist(iDet,:)
!   write(6,*) "New one ", Detlist(totDet,:)
!  else
!   write(6,*) "Same "
!   write(6,*) Detlist(i,:)
!   write(6,*) Detblocks(ifil)%Detlist(iDet,:)
  endif
  
  do j=1,Detblocks(ifil)%npot
   CIDet(j+ntot,i)=CIDet(j+ntot,i)+Detblocks(ifil)%CIDet(j,iDet)
  enddo
 enddo
 ntot=ntot+Detblocks(ifil)%npot
 write(6,*) "Done file, Determinants up to now ",totDet
enddo

do i=1,ntot
 norm=0.
 do iDet=1,totDet
  norm=norm+CIDet(i,iDet)**2
 enddo
 write(6,*) "Norm ",i,norm
enddo

write(6,*) "Output file"
read(5,*) fich
write(6,*) fich

allocate(tmpdet(norb))
open(1,file=fich)
write(1,"(3(x,I10.10))") ntot,totDet,norb
do iDet=1,totDet

!!! From logical to integer
 do i=1,norb
  if (Detlist(iDet,2*i).and.Detlist(iDet,2*i-1)) then
   tmpdet(i)=3
  else if (Detlist(iDet,2*i)) then
   tmpdet(i)=2
  else if (Detlist(iDet,2*i-1)) then
   tmpdet(i)=1
  else
   tmpdet(i)=0
  endif
 enddo
   
 write(fich,"(100I1.1)") (tmpdet(i),i=1,norb)
 write(1,"(A,1000(x,E40.30e3))") trim(fich),(CIDet(i,iDet),i=1,ntot)
enddo

end program Dets2Det
