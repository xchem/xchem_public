!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program read_write_orb
implicit none
real*8, allocatable :: orb1(:,:),orb2(:,:),orb3(:)
integer nbas1,nbas2,norb1,norb2
integer nbas3,norb3


character*100 fich
integer i,j,sym

write(6,*) "File with the input orbitals (noaug)"
read(5,*) fich
open(1,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File must exist ",fich
 stop
endif
read(1,*) !INPORB
read(1,*) !INFO
read(1,*) !* Orbitals type
read(1,*) !!! No clue
read(1,*) nbas1
read(1,*) norb1
read(1,*) !Info
read(1,*) !Blank
read(1,*) !#ORB
allocate(orb1(norb1,nbas1) )
write(6,*) "Input orbitals noaug (Number and basis)",norb1,nbas1
write(6,*) "Read from ",fich

write(6,*)"File with the input orbitals (aug) to consider after noaug"
read(5,*) fich
open(2,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File must exist ",fich
 stop
endif
read(2,*) !INPORB
read(2,*) !INFO
read(2,*) !* Orbitals type
read(2,*) !!! No clue
read(2,*) nbas2
read(2,*) norb2
read(2,*) !Info
read(2,*) !Blank
read(2,*) !#ORB
allocate(orb2(norb2,nbas2) )
write(6,*) "Input orbitals aug (Number and basis)",norb2,nbas2
write(6,*) "Read from ",fich



do i=1,norb1
!!! Orbitals from the noaug data
 read(1,*) ! Number of orbital
 read(1,"(4(E18.12e2))") (orb1(i,j),j=1,nbas1)
enddo
close(1)

do i=1,norb2
!!! Orbitals from the aug data
 read(2,*) ! Number of orbital
 read(2,"(4(E18.12e2))") (orb2(i,j),j=1,nbas2)
enddo
close(2)

write(6,*) "Input for template, defines the total number of basis and orbitals"
read(5,*) fich

open(1,file=fich)
read(1,*) !INPORB
read(1,*) !INFO
read(1,*) !* Orbitals type
read(1,*) !!! No clue
read(1,*) nbas3
read(1,*) norb3
read(1,*) !Info
read(1,*) !Blank
read(1,*) !#ORB

write(6,*) "File for the destination orbitals "
read(5,*) fich
write(6,*) "Output orbitals (Number and basis) ",norb3,nbas3
allocate (orb3(nbas3))

open(3,file=fich)
write(3,"(20A)") "#INPORB 1.1"
write(3,"(A5)")  "#INFO"
write(3,"(A15)") "* Homemade orbs"
write(3,"(A24)") "       0       1       2"
write(3,"(I8)") nbas3
write(3,"(I8)") norb3
write(3,"(A1)") "*"
write(3,*)
write(3,"(A4)") "#ORB"

! Mixing
do i=1,norb3
!!! Orbitals from the template file
 read(1,*) ! Number of orbital
 read(1,"(4(E18.12e2))") (orb3(j),j=1,nbas3)
 do j=1,nbas3
  orb3(j)=0.d0
 enddo

 if (nbas3.eq.nbas1+nbas2) then
!!! Adding the two sets
  if (i.le.norb1) then
   write(6,*) "Orbital ",i," taken from set 1 ",i
   do j=1,nbas1
    orb3(j)=orb1(i,j)
   enddo
  else
   write(6,*) "Orbital ",i," taken from set 2 ",i-norb1
   do j=1,nbas2
    orb3(j+nbas1)=orb2(i-norb1,j)
   enddo
  endif
!!
 endif

 if (nbas3.eq.nbas2) then
!!! Substituting the first norb1 orbitals of orb2
  if (i.le.norb1) then
   do j=1,nbas1
    orb3(j)=orb1(i,j)
   enddo
  else 
   do j=1,nbas2
    orb3(j)=orb2(i,j)
   enddo
  endif

 endif

 sym=1
 write(3,"(A9,2(x,I4))") "* ORBITAL",sym,i
 write(3,"(4(E18.12e2))") (orb3(j),j=1,nbas3)
enddo

!!! The rest is a copy of the template file
do
 read(1,"(A)",END=1) fich
 write(3,"(100A)") trim(fich)
enddo

1 continue
 


end program
