!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program desym
implicit none
integer nsym,nbas,nblocks
integer, allocatable :: orb(:),nbs(:),sym(:),sym2(:)
integer n0,nf,nt
real*8, allocatable :: MO(:,:),MO2(:,:)

integer isym,i,j,k
character*100 fich1,fich2
real*8 kk

read(5,*) nbas,nsym
allocate (nbs(nsym),orb(nbas),MO(nbas,nbas),sym(nbas))
nt=0
do isym=1,nsym
 write(6,*) "Reading symmetry ",isym
 read(5,*) nbs(isym),nblocks
 do i=1,nblocks
  read(5,*) n0,nf
  do j=n0,nf
   nt=nt+1
   orb(nt)=j

!!! Checking if the orbital was already used
   if (nt.ne.1) then
    do k=1,nt-1
     if (orb(k).eq.orb(nt)) then
      write(6,*) "Repeated orbital ",nt,orb(nt)
      stop
     endif
    enddo
   endif

  enddo
 enddo
enddo


if (nt.ne.nbas) then
 write(6,*) "Total number of basis ",nbas
 write(6,*) "Total number of symmetry adapted basis ",nt
 stop "Total number of basis is not the same that the symmetry adapted"
endif

read(5,*) fich1,fich2
open(1,file=fich1)
MO=0.
nt=0
n0=0
do isym=1,nsym
 write(6,*) "Symmetry ",isym
 write(6,*) "Orbitals "
 write(6,*) (orb(j),j=n0+1,n0+nbs(isym))
 do i=1,nbs(isym)
  nt=nt+1
  sym(nt)=isym
  read(1,*)
  read(1,"(4e18.12e2)") (MO(orb(j),nt),j=n0+1,n0+nbs(isym))
 enddo
 n0=n0+nbs(isym)
enddo

!!! Removing zero orbitals
allocate(MO2(nbas,nbas),sym2(nbas))
MO2=0.
sym2=0
nt=0
do i=1,nbas
 kk=0.
 do j=1,nbas
  kk=kk+MO(j,i)**2
 enddo
 if (kk.ge.1e-8) then
  nt=nt+1
  sym2(nt)=sym(i)
  do j=1,nbas
   MO2(j,nt)=MO(j,i)
  enddo
 endif
enddo

write(6,*) "Total number of non-zero orbitals ",nt

fich1=trim(fich2)//".Orb"
open(1,file=fich1)
do i=1,nt
 if (i.eq.1) then
  write(1,"(A,x,I5,x,I5,x,a,x,I5)") "* ORBITAL sym ",sym2(i),i,"LI",nt
 else
  write(1,"(A,I5,x,I5)") "* ORBITAL sym ",sym2(i),i
 endif
 write(1,"(4e18.12e2)") (MO2(j,i),j=1,nbas)
enddo
kk=0.
do i=nt+1,nbas
 write(1,"(A,I5)") "* Linear dependent orbital (0) ",i
 write(1,"(4e18.12e2)") (kk,j=1,nbas)
enddo



end
