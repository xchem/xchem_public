!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program cubeFFT
implicit none
integer np(3),nat,npk(3),dir
real*8 x0(3),dx(3,3)
complex*16, allocatable :: wf(:,:,:),wfk(:,:,:)
real*8, allocatable :: kk(:)
complex*16, allocatable :: wfp(:)

integer i,j,k,ii,ij
real*8 norm(5)
real*8 dr(3),dk(3),Ekin,ka(3),ra,Ekindir,kdir
real*8 Epot(2),El(2),x(3)
character*100 fich
logical ex
integer npf

read(5,*) Epot(:)
read(5,*) El(:)
El=El/27.211

read(5,*) fich

inquire(file=fich, exist=ex)
if (ex) then
 open(1,file=fich)
else
 write(6,*) "Cube file it does not exist ",fich
endif
open(2,file="outz.cube")
read(1,*)
write(2,*) 
read(1,*)
write(2,*)
read(1,*) nat,x0(:)
write(2,901) nat,x0(:)

do i=1,3
 read(1,*) np(i),dx(i,:)
 write(2,901) np(i),dx(i,:)
enddo
do i=1,nat
 read(1,"(A)") fich
 write(2,"(A)") trim(fich)
enddo

allocate (wf(np(1),np(2),np(3)))
allocate (kk(np(3)))

write(6,*) "Starting reading cube file"
do i=1,np(1)
 do j=1,np(2)
  read(1,*) (kk(k),k=1,np(3) )
  do k=1,np(3)
   wf(i,j,k)=cmplx(kk(k),0.)
  enddo
 enddo
enddo
write(6,*) "Dyson orbital in memory"
deallocate(kk)

read(5,*) dir


!! Defining dr for later
do ii=1,3
 dr(ii)=0.
 do j=1,3
  dr(ii)=dr(ii)+dx(ii,j)**2
 enddo
 dr(ii)=sqrt(dr(ii))
 norm(1)=norm(1)*dr(ii)
 norm(2)=norm(2)*dr(ii)
enddo

do i=1,3
 write(6,*) "Integrating dir ",dx(i,:)
 allocate (kk(np(i)) )
 call integra(i,np,wf,kk)
 if (i.eq.1) open(1,file="int1.sp")
 if (i.eq.2) open(1,file="int2.sp")
 if (i.eq.3) open(1,file="int3.sp")
 do j=1,np(i)
  ra=float(j-1)*dr(i)
  do k=1,3
   x(k)=x0(k)+float(j-1)*dx(i,k)
  enddo
  write(1,900) ra,kk(j),x(:)
 enddo
 close(1)
 deallocate(kk)
enddo
 
 

norm(1)=0.0
norm(2)=0.0
allocate (kk(np(3)))
do i=1,np(1)
 do j=1,np(2)
  do k=1,np(3)
   norm(1)=norm(1)+abs(wf(i,j,k))**2
   do ii=1,3
    x(ii)=x0(ii)
    x(ii)=x(ii)+float(i-1)*dx(1,ii)
    x(ii)=x(ii)+float(j-1)*dx(2,ii)
    x(ii)=x(ii)+float(k-1)*dx(3,ii)
   enddo
   ra=x(dir)
   wf(i,j,k)=ra*wf(i,j,k)
   kk(k)=dreal(wf(i,j,k))
   norm(2)=norm(2)+abs(wf(i,j,k))**2
  enddo
  write(2,902) (kk(k),k=1,np(3))
 enddo
enddo
deallocate(kk)
do i=1,3
 norm(1)=norm(1)*dr(i)
 norm(2)=norm(2)*dr(i)
enddo
close(2)
   
write(6,*) "Norm in position representation "
write(6,*) "Dys norm ",norm(1)
write(6,*) "dir Dys norm ",norm(2)

!!! IF norm is smaller than a threshold (1e-5) less stop since ionization is negligible
if (norm(1).le.1e-5 .and. norm(2).le.1e-5) then
 write(6,*) 'Dyson norm is negligible'
 write(6,*) "Aborting"
 stop
endif

do i=1,3
 write(6,*) "Integrating dir ",i
 allocate (kk(np(i)) )
 call integra(i,np,wf,kk)
 if (i.eq.1) open(1,file="int1z.sp")
 if (i.eq.2) open(1,file="int2z.sp")
 if (i.eq.3) open(1,file="int3z.sp")
 do j=1,np(i)
  ra=float(j-1)*dr(i)
  do k=1,3
   x(k)=x0(k)+float(j-1)*dx(i,k)
  enddo
  write(1,900) ra,kk(j),x(:)
 enddo
 close(1)
 deallocate(kk)
enddo

!write(6,*) "Writting dir Dys in file space.vtk"
!open(1,file="space.vtk")
!call vtk(1,np,x0,dr,wf,.false.)
!close(1)


do i=1,3
 read(5,*) npk(i)
enddo

npf=2048
write(6,*) "Starting FFT ",npf
do ii=1,3
 dk(ii)=2.*acos(-1.)/float(npf)/dr(ii)
 write(6,*) "Momentum ",ii
 write(6,*) "Dk ",dk(ii)
 write(6,*) "Maximum mom ",npk(ii)/2.*dk(ii)
 write(6,*) "Maximum kinetic energy ",(npk(ii)/2.*dk(ii))**2/2. *27.211
enddo

allocate (wfk(np(1),np(2),npk(3)) )
allocate (wfp(npf) )
write(6,*) "Direction 3"
do i=1,np(1)
 do j=1,np(2)
  wfp=cmplx(0.,0.)
  do k=1,np(3)
   wfp(k+npf/2-np(3)/2)=wf(i,j,k)
  enddo
  call tfft(wfp,npf,npf,1)
  wfk(i,j,npk(3))=wfp(npf)
  do k=1,npk(3)/2
   wfk(i,j,k)=wfp(k)
   wfk(i,j,npk(3)-k)=wfp(npf-k)
  enddo
 enddo
enddo
deallocate (wf)

write(6,*) "Direction 2"
allocate (wf(np(1),npk(2),npk(3)) )
do i=1,np(1)
 do j=1,npk(3)
  wfp=cmplx(0.,0.)
  do k=1,np(2)
   wfp(k+npf/2-np(2)/2)=wfk(i,k,j)
  enddo
  call tfft(wfp,npf,npf,1)
  wf(i,npk(2),j)=wfp(npf)
  do k=1,npk(2)/2
   wf(i,k,j)=wfp(k)
   wf(i,npk(2)-k,j)=wfp(npf-k)
  enddo
 enddo
enddo
deallocate(wfk)

write(6,*) "Direction 1"
allocate(wfk(npk(1),npk(2),npk(3)) )
do i=1,npk(2)
 do j=1,npk(3)
  wfp=cmplx(0.,0.)
  do k=1,np(1)
   wfp(k+npf/2-np(1)/2)=wf(k,i,j)
  enddo
  call tfft(wfp,npf,npf,1)
  wfk(npk(1),i,j)=wfp(npf)
  do k=1,npk(1)/2
   wfk(k,i,j)=wfp(k)
   wfk(npk(1)-k,i,j)=wfp(npf-k)
  enddo
 enddo
enddo
deallocate(wfp,wf)

write(6,*) "FFT done"

do i=1,3
 write(6,*) "Integrating dir ",i
 allocate (kk(npk(i)) )
 call integra(i,npk,wfk,kk)
 if (i.eq.1) open(1,file="int1z.mom")
 if (i.eq.2) open(1,file="int2z.mom")
 if (i.eq.3) open(1,file="int3z.mom")
 do j=npk(i)/2+1,npk(i)
  ra=float(j-1-npk(i))*dk(i)
  write(1,900) ra,kk(j)
 enddo
 do j=1,npk(i)/2
  ra=float(j-1)*dk(i)
  write(1,900) ra,kk(j)
 enddo
 close(1)
 deallocate(kk)
enddo

!write(6,*)" written in mom.vtk"
!open(1,file="mom.vtk")
!call vtk(1,npk,x0,dk,wfk,.true.)
!close(1)

write(6,*) "A huge binary file 'ampl.mom' with the amplitude as function of momentum is gonna be written"
open(1,file="ampl.mom",form="unformatted")
norm(3)=0.
norm(4)=0.
norm(5)=0.
do i=1,npk(1)
 if (i.le.npk(1)/2) then
  ka(1)=float(i-1)
 else
  ka(1)=float(i-1-npk(1))
 endif
 ka(1)=ka(1)*dk(1)

 do j=1,npk(2)
  if (j.le.npk(2)/2) then
   ka(2)=float(j-1)
  else
   ka(2)=float(j-1-npk(2))
  endif
  ka(2)=ka(2)*dk(2)

  do k=1,npk(3)
   if (k.le.npk(3)/2) then
    ka(3)=float(k-1)
   else
    ka(3)=float(k-1-npk(3))
   endif
   ka(3)=ka(3)*dk(3)

   write(1) wfk(i,j,k)

   Ekin=0.
   do ii=1,3
    Ekin=Ekin+ka(ii)**2
   enddo
   Ekin=Ekin/2.

   norm(3)=norm(3)+abs(wfk(i,j,k))**2
   Ekin=Ekin+Epot(2)-Epot(1)
   if (Ekin.ge.El(1) .and. Ekin.le.El(2)) norm(4)=norm(4)+abs(wfk(i,j,k))**2

!!!! TEMPORAL
   kdir=ka(1)
   Ekindir=kdir**2/2.
   Ekindir=Ekindir+Epot(2)-Epot(1)
   if (Ekindir.ge.El(1) .and. Ekindir.le.El(2)) norm(5)=norm(5)+abs(wfk(i,j,k))**2
  enddo
 enddo
enddo

write(6,*) "Norm in momentum space:"
write(6,*) "dir Dys ",norm(3),norm(3)/norm(3)*norm(2)
write(6,*) "Bandwith allowed dir Dys including all directions ",norm(4)/norm(3)*norm(2)
write(6,*) "Bandwith allowed dir Dys in direction ",dir,norm(5)/norm(3)*norm(2)

   

900 format(10000(x(E20.10e3)) )
!!! cube format
901 format(I5,3(x,F11.6))
902 format(6(x,E12.5e2))
end


subroutine integra(dir,np,wf,integ)
implicit none
integer, intent(in) :: dir,np(3)
complex*16, intent(in) :: wf(np(1),np(2),np(3) )
real*8, intent(out) :: integ(np(dir))

integer i,j,k,ii,ij
 
do k=1,np(dir)
 if (dir.eq.1) then
  ii=2
  ij=3
 endif
 if (dir.eq.2) then
  ii=1
  ij=3
 endif
 if (dir.eq.3) then
  ii=1
  ij=2
 endif
 integ(k)=0.
 do i=1,np(ii)
  do j=1,np(ij)
   if (dir.eq.1) integ(k)=integ(k)+abs(wf(k,i,j))**2
   if (dir.eq.2) integ(k)=integ(k)+abs(wf(i,k,j))**2
   if (dir.eq.3) integ(k)=integ(k)+abs(wf(i,j,k))**2
  enddo
 enddo
enddo


end
