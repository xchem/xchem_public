!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM detSplit
    USE detIO
    IMPLICIT none
 !CONTROL VARIABLES
    INTEGER :: stat
    LOGICAL :: exists
 !CMD LINE ARGUMENT
    CHARACTER*200 :: fich
 !STRUCTURE VARIABLES
    TYPE(det_) :: det

    CALL GETARG(1,fich)
    IF (LEN(TRIM(fich)).eq.0) STOP "ERROR determinant file has to be&
      provided as command line argument"
    CALL det%create(TRIM(fich),"b")
    CALL det%split()
END PROGRAM
