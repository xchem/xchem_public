!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program general_mix
use diagonalizations
implicit none
integer n0,nparent,ncommon,naug,npot
integer n1,nf,nprop
character*200 fich,way

real*8, allocatable :: Ham(:,:),ov(:,:),kkv(:),tmp(:,:)

integer i,j,iprop
character*200 pot1,pot2,fprop,dir,props
logical ex,debug
logical orbital
integer mixtype
logical half

inquire(file="debug",exist=debug)

!!MARKUS MOD
write(6,*) "set output directory"
read(5,*) dir 
write(6,*) "set directory containg rassi fragments (typically props.l*)"
read(5,*) props
!!END MARKUS MOD
write(6,*) "Number of localized states"
read(5,*) n0
write(6,*) "Number of parent ions"
read(5,*) nparent
write(6,*) "Number of common states in the parent ion (usually the RAS2)"
read(5,*) ncommon
write(6,*) "First and last orbitals where the rotation happens"
read(5,*) n1,nf
naug=nf-n1+1+ncommon
npot=naug*nparent+n0
write(6,*) "Total augmented orbitals per parent ion ",naug
write(6,*) "Total number of states ",npot

allocate(Ham(npot,npot),ov(npot,npot),kkv(npot))

write(6,*) "Format of the pieces (binary or free)"
read(5,*) way

if (way == "binary") then
 write(6,*) "The files are gonna be read in binary with termination .bin"
else if (way == "free") then
 write(6,*) "The files are gonna be read in free format with termination .out"
else
 stop 'Binary of free'
endif


write(6,*) "Reading pieces with states ",n0+nparent*(ncommon+2)
allocate(tmp(n0+nparent*(ncommon+2),n0+nparent*(ncommon+2)) )

write(6,*) "Order in the input (parent or orbital)"
read(5,*) fich, mixtype
write(6,*) fich, mixtype
select case (fich)
 case("parent")
  orbital=.false.
 case("orbital")
  orbital=.true.
 case default
  stop 'Order must be by parent ions or by orbitals'
end select
select case (mixtype)
 case(1)
  write(6,*) "Considering only files of type Ham_**-**"
 case(2)
  write(6,*) "Considering files of type Ham_**-** and Ham_**"
 case default
  stop 'Mixtype not valid'
end select


do i=n1,nf-1
 do j=i+1,nf
  write(pot1,"(I5)") i
  pot1=adjustl(pot1)
!  if (i.le.9999) write(pot1,"(I4)") i
!  if (i.le.999)  write(pot1,"(I3)") i
!  if (i.le.99)   write(pot1,"(I2)") i
!  if (i.le.9)    write(pot1,"(I1)") i
  write(pot2,"(I5)") j
  pot2=adjustl(pot2)
!  if (j.le.9999) write(pot2,"(I4)") j
!  if (j.le.999)  write(pot2,"(I3)") j
!  if (j.le.99)   write(pot2,"(I2)") j
!  if (j.le.9)    write(pot2,"(I1)") j
  pot1=trim(pot1)
  pot2=trim(pot2)
  write(fich,"(200A)") TRIM(props),"/Ham_",trim(pot1),"-",trim(pot2)
  if (way == "binary") write(fich,"(200A)") trim(fich),".bin"
  if (way == "free")   write(fich,"(200A)") trim(fich),".out"

  
  inquire(file=fich,exist=ex)
  if (debug) write(6,*) "Let's read from ",trim(fich),ex
  if (ex) then
!   write(6,*) "Reading from ",trim(fich)
  if (way=="binary") open(1,file=fich,form="unformatted")
  if (way=="free")   open(1,file=fich)
!!!!! Reading n0+ncommon + 2 augmented (i,j)
   tmp=0.
   call reading(n0,naug,ncommon,nparent,npot,Ham,1,.true.,i-n1+1,j-n1+1,way,n0+nparent*(ncommon+2),tmp,orbital)
   close(1)
  else
   write(6,*) trim(fich)," does not exist"
  endif
  write(fich,"(200A)") TRIM(props),"/overlap_",trim(pot1),"-",trim(pot2)
  if (way == "binary" ) write(fich,"(200A)") trim(fich),".bin"
  if (way == "free" )   write(fich,"(200A)") trim(fich),".out"
  inquire(file=fich,exist=ex)
  if (debug) write(6,*) "Let's read from ",trim(fich),ex
  if (ex) then
   if (way=="binary") open(1,file=fich,form="unformatted")
   if (way=="free")   open(1,file=fich)
!!!!! Reading n0+ncommon + 2 augmented (i,j)
   tmp=0.
   call reading(n0,naug,ncommon,nparent,npot,ov,1,.true.,i-n1+1,j-n1+1,way,n0+nparent*(ncommon+2),tmp,orbital)
   close(1)
  else
   write(6,*) trim(fich)," does not exist"
  endif
 enddo
 write(6,*) "Done ",i,nf-1
enddo
deallocate(tmp)

if (mixtype.eq.2) then
 write(6,*) "Checking if there is a part with only one aug (only Ham)"
 write(6,*) "Reading pieces with states ",n0+nparent*(ncommon+1)
 allocate(tmp(n0+nparent*(ncommon+1),n0+nparent*(ncommon+1)) )

 do i=n1,nf
  write(pot1,"(I5)") i
  pot1=adjustl(pot1)
!  if (i.le.9999) write(pot1,"(I4)") i
!  if (i.le.999)  write(pot1,"(I3)") i
!  if (i.le.99)   write(pot1,"(I2)") i
!  if (i.le.9)    write(pot1,"(I1)") i
  pot1=trim(pot1)
  write(fich,"(200A)") TRIM(props),"/Ham_",trim(pot1)
  if (way == "binary") write(fich,"(200A)") trim(fich),".bin"
  if (way == "free")   write(fich,"(200A)") trim(fich),".out"

  inquire(file=fich,exist=ex)
  if (debug) write(6,*) "Let's read from ",trim(fich),ex
  if (ex) then
!   write(6,*) "Reading from ",trim(fich)
   if (way=="binary") open(1,file=fich,form="unformatted")
   if (way=="free")   open(1,file=fich)
!!!!! Reading n0+ncommon + 2 augmented (i,j)
   tmp=0.
  write(6,*) "Augmenting ",i,trim(fich)
   call reading1(n0,naug,ncommon,nparent,npot,Ham,1,.true.,i-n1+1,way,n0+nparent*(ncommon+1),tmp,orbital)
   close(1)
  else
   write(6,*) trim(fich)," does not exist"
  endif
!  if (i.eq.n1+1) stop
 enddo
 deallocate(tmp)
endif

write(6,*) "Putting them together"
open(1,file=TRIM(dir)//"/Ham.out")
open(2,file=TRIM(dir)//"/overlap.out")
open(3,file=TRIM(dir)//"/Ham.bin",form="unformatted")
open(4,file=TRIM(dir)//"/overlap.bin",form="unformatted")
write(1,"(I10)") npot
write(2,"(I10)") npot
write(3) npot
write(4) npot
do i=1,npot
 write(1,900) (Ham(i,j),j=1,npot)
 write(2,900) (ov(i,j),j=1,npot)
 write(3) (Ham(i,j),j=1,npot)
 write(4) (ov(i,j),j=1,npot)
enddo
close(1)
close(2)
close(3)
close(4)


write(6,*) "Writting in blocks"

write(fich,*) "Ham"
call writting(npot,Ham,n0,naug,nparent,fich,dir)
write(fich,*) "ov"
call writting(npot,ov,n0,naug,nparent,fich,dir)


write(6,*) "Calculate eigenvalues (yes or no)"
read(5,*) fich
select case(fich)
 case("yes")
!!! Calculating eigenvalues
  write(6,*) "Calculating eigenvalues ..."
  call eig(npot,Ham,ov,kkv)
  write(6,*) " ...save them in eig.out"
  open(1,file=TRIM(dir)//"/eig.out")
  do i=1,npot
   write(1,*) i,kkv(i)
  enddo
  close(1)
  write(6,*) "...also save diagonalizing matrix"
  open(1,file=TRIM(dir)//"/diag.out")
  write(1,*) 1
  write(1,*) npot
  do i=1,npot
    write(1,*) (ov(j,i),j=1,npot)  
  enddo
end select

write(6,*) "Let's mix also the properties"
write(6,*) "Reading pieces with states ",n0+nparent*(ncommon+2)
allocate(tmp(n0+nparent*(ncommon+2),n0+nparent*(ncommon+2)) )

write(6,*) "Number of properties to be mixed"
read(5,*)  nprop

do iprop=1,nprop
 write(6,*) "Property ",iprop
!!!! Ham is gonna be used to store the property
 Ham=0.
 write(6,*) "Starting characters, it will be reading as xxx_*-*"
 read(5,*) fprop,half
!!!!!!!!!!!!!!
 write(6,*) "Doing ",trim(fprop)
!!!!!!!!!!!!!!!
 do i=n1,nf-1
  do j=i+1,nf
   write(pot1,"(I5)") i
   pot1=adjustl(pot1)
   write(pot2,"(I5)") j
   pot2=adjustl(pot2)
   pot1=trim(pot1)
   pot2=trim(pot2)
!!!!!!!
   write(fich,"(200A)") TRIM(props),"/",trim(fprop),"_",trim(pot1),"-",trim(pot2)

   if (way == "binary") write(fich,"(200A)") trim(fich),".bin"
   if (way == "free")   write(fich,"(200A)") trim(fich),".out"

  
   inquire(file=fich,exist=ex)
   if (debug) write(6,*) "Let's read from ",trim(fich),ex
   if (ex) then
    if (way=="binary") open(1,file=fich,form="unformatted")
    if (way=="free")   open(1,file=fich)
!!!!! Reading n0+ncommon + 2 augmented (i,j)
    tmp=0.
    !call reading(n0,naug,ncommon,nparent,npot,Ham,1,.false.,i-n1+1,j-n1+1,way,n0+nparent*(ncommon+2),tmp,orbital)
    call reading(n0,naug,ncommon,nparent,npot,Ham,1,half,i-n1+1,j-n1+1,way,n0+nparent*(ncommon+2),tmp,orbital)
    close(1)
   else
    write(6,*) trim(fich)," does not exist"
   endif
  enddo
  write(6,*) "Done ",trim(fprop),i,nf-1
 enddo

 write(fich,"(200A)") TRIM(dir),"/",trim(fprop),".out"
 open(1,file=fich)
 write(fich,"(200A)") TRIM(dir),"/",trim(fprop),".bin"
 open(2,file=fich,form="unformatted")
 write(1,"(I10)") npot
 write(2) npot
 do i=1,npot
  write(1,900) (Ham(i,j),j=1,npot)
  write(2) (Ham(i,j),j=1,npot)
 enddo
 close(1)
 close(2)
 call writting(npot,Ham,n0,naug,nparent,fprop,dir)

enddo

900 format(1000000(x,E30.20e3))
end

subroutine reading(n0,naug,ncommon,nparent,npot,tot,un,half,aug1,aug2,way,n,tmp,orbital)
implicit none
integer, intent(in) :: n0,naug,ncommon,nparent,npot,aug1,aug2,un,n
logical,intent (in) :: half,orbital
character*100, intent(in) :: way
real*8, intent(inout) :: tot(npot,npot)

real*8, intent(inout) :: tmp(n,n)
integer ii,ij,i,j,iparent,jparent
integer iroot
real*8 tmp2(n,n)

   if (way=="binary") THEN
        read(1) 
        read(1) i
   ENDIF
   if (way=="free") THEN   
    read(1,*) 
    read(1,*) i
   ENDIF
   if (i.ne.n) then
    write(6,*) "Wrong number of potentials on the piece "
    write(6,*) "Expected: ",n
    write(6,*) "In the piece: ",i
    stop
   endif

do ii=1,n
 if (half) then
  if (way=="binary") read(1) (tmp(ii,ij),ij=1,ii)
  if (way=="free")   read(1,*) (tmp(ii,ij),ij=1,ii)
 else
  if (way=="binary") read(1) (tmp(ii,ij),ij=1,n)
  if (way=="free")   read(1,*) (tmp(ii,ij),ij=1,n)
 endif
enddo

if (half) then
 do ii=1,n
  do ij=1,ii
   tmp(ij,ii)=tmp(ii,ij)
  enddo
 enddo
endif

do ii=1,n
 do ij=1,n
  if (isnan(tmp(ii,ij)) ) stop 'There are NaN in your data'
 enddo
enddo

!!! The output must be by parent so reoredering if you are ordering by orbitals
if (orbital) then
 tmp2=tmp

!!! The part of n0
 if (n0.ge.1) then
  do i=1,n0
   do ii=1,ncommon+2
    do ij=1,nparent
     tmp(i,n0+(ij-1)*(ncommon+2)+ii)=tmp2(i,n0+(ii-1)*(nparent)+ij)
     tmp(n0+(ij-1)*(ncommon+2)+ii,i)=tmp2(n0+(ii-1)*(nparent)+ij,i)
    enddo
   enddo
  enddo
 endif

!!! The augmented states
 do i=1,ncommon+2
  do j=1,nparent
   do ii=1,ncommon+2
    do ij=1,nparent
     tmp(n0+(j-1)*(ncommon+2)+i,n0+(ij-1)*(ncommon+2)+ii)= &
     tmp2(n0+(i-1)*(nparent)+j,n0+(ii-1)*(nparent)+ij)
    enddo
   enddo
  enddo
 enddo
endif

if (.false.) then

 write(6,*) "Checking file with augmented ",aug1,aug2
 write(6,*) "Number of common augmentations ",ncommon
 write(6,*) "Number of local states ",n0
 do i=1,n
  if (i.le.n0) then
   ii=i
   iroot=i
   iparent=0
  endif
  iparent=int(float((i-n0-1)/(ncommon+2)) )+1
!!! Local root
  ii=i-(iparent-1)*(ncommon+2)-n0
  iroot=ii
  if (iroot.eq.ncommon+1) j=aug1+ncommon
  if (iroot.eq.ncommon+2) j=aug2+ncommon
!!! Global root
  ii=n0+(iparent-1)*naug+j
  write(6,*) "Pot ",i," parent ",iparent," augmented ",j, "state ",ii
 enddo
endif
  

!!! Copy this part into the total matrix)
do i=1,n
 do j=1,n
!  write(6,*)
!  write(6,*) i,j,aug1,aug2,n0,ncommon
  ! Assignation
  if (i.le.n0) ii=i
  if (j.le.n0) ij=j
  
  if (i.gt.n0) then
   iparent=int(float((i-n0-1)/(ncommon+2)) )+1
   iroot=i-(iparent-1)*(ncommon+2)-n0
   ii=iroot
   if (iroot.eq.ncommon+1) ii=aug1+ncommon
   if (iroot.eq.ncommon+2) ii=aug2+ncommon
   ii=n0+(iparent-1)*naug+ii
  endif
  if (j.gt.n0) then
   iparent=int(float((j-n0-1)/(ncommon+2)) )+1
   iroot=j-(iparent-1)*(ncommon+2)-n0
   ij=iroot
   if (iroot.eq.ncommon+1) ij=aug1+ncommon
   if (iroot.eq.ncommon+2) ij=aug2+ncommon
   ij=n0+(iparent-1)*naug+ij
  endif

  if (ii.gt.npot.or.ij.gt.npot) then
   write(6,*) "Trying to write out of limits ",ii,ij
   write(6,*) "Augmenting ",aug1,aug2
   stop "Problem in the creation of Ham matrix"
  endif
  tot(ii,ij)=tmp(i,j)
 enddo
enddo
  

end

subroutine reading1(n0,naug,ncommon,nparent,npot,tot,un,half,aug1,way,n,tmp,orbital)
implicit none
integer, intent(in) :: n0,naug,ncommon,nparent,npot,aug1,un,n
logical,intent (in) :: half,orbital
character*100, intent(in) :: way
real*8, intent(inout) :: tot(npot,npot)

real*8, intent(inout) :: tmp(n,n)
integer ii,ij,i,j,iparent,jparent
integer iroot
real*8 tmp2(n,n)

   if (way=="binary") THEN
        read(1)
        read(1) i
   ENDIF
   if (way=="free") THEN   
        read(1,*) 
        read(1,*) i
   ENDIF
   if (i.lt.n) then
    write(6,*) "Wrong number of potentials on the piece "
    write(6,*) "Expected: ",n
    write(6,*) "In the piece: ",i
    stop
   else if (i.gt.n .and. aug1.eq.1) then
    write(6,*) "Number of potentials in the piece: ",i
    write(6,*) "Reading the first ",n
   endif

do ii=1,n
 if (half) then
  if (way=="binary") read(1) (tmp(ii,ij),ij=1,ii)
  if (way=="free")   read(1,*) (tmp(ii,ij),ij=1,ii)
 else
  if (way=="binary") read(1) (tmp(ii,ij),ij=1,n)
  if (way=="free")   read(1,*) (tmp(ii,ij),ij=1,n)
 endif
enddo

if (half) then
 do ii=1,n
  do ij=1,ii
   tmp(ij,ii)=tmp(ii,ij)
  enddo
 enddo
endif

do ii=1,n
 do ij=1,n
  if (isnan(tmp(ii,ij)) ) stop 'There are NaN in your data'
 enddo
enddo

!!! The output must be by parent so reoredering if you are ordering by orbitals
if (orbital) then
 tmp2=tmp

!!! The part of n0
 if (n0.ge.1) then
  do i=1,n0
   do ii=1,ncommon+1
    do ij=1,nparent
     tmp(i,n0+(ij-1)*(ncommon+1)+ii)=tmp2(i,n0+(ii-1)*(nparent)+ij)
     tmp(n0+(ij-1)*(ncommon+1)+ii,i)=tmp2(n0+(ii-1)*(nparent)+ij,i)
    enddo
   enddo
  enddo
 endif

!!! The augmented states
 do i=1,ncommon+1
  do j=1,nparent
   do ii=1,ncommon+1
    do ij=1,nparent
     tmp(n0+(j-1)*(ncommon+1)+i,n0+(ij-1)*(ncommon+1)+ii)= &
     tmp2(n0+(i-1)*(nparent)+j,n0+(ii-1)*(nparent)+ij)
    enddo
   enddo
  enddo
 enddo
endif

!!! Copy this part into the total matrix)
do i=1,n
 do j=1,n
!  write(6,*)
!  write(6,*) i,j,aug1,aug2,n0,ncommon
  ! Assignation
  if (i.le.n0) ii=i
  if (j.le.n0) ij=j
  
  if (i.gt.n0) then
   iparent=int(float((i-n0-1)/(ncommon+1)) )+1
   iroot=i-(iparent-1)*(ncommon+1)-n0
   ii=iroot
   if (iroot.eq.ncommon+1) ii=aug1+ncommon
   ii=n0+(iparent-1)*naug+ii
  endif
  if (j.gt.n0) then
   iparent=int(float((j-n0-1)/(ncommon+1)) )+1
   iroot=j-(iparent-1)*(ncommon+1)-n0
   ij=iroot
   if (iroot.eq.ncommon+1) ij=aug1+ncommon
   ij=n0+(iparent-1)*naug+ij
  endif

  tot(ii,ij)=tmp(i,j)
  if (ii.gt.npot.or.ij.gt.npot) then
   write(6,*) "Trying to write out of limits ",ii,ij
   write(6,*) "Augmenting ",aug1
   write(6,"(3(x,I3),3(x,F20.10),4(x,I6))") & 
   i,j,aug1,tmp2(i,j),tmp(i,j),tot(ii,ij),ii,ij
   stop "Problem in the creation of Ham matrix"
  endif

 enddo
enddo
  

end

subroutine writting(npot,mat,n0,naug,nparent,fich0,dir)
integer, intent(in) :: npot,n0,naug,nparent
character*200, intent(in) :: fich0,dir
real*8, intent(in) :: mat(npot,npot)

integer i,j,prev1,prev2,ipar,jpar
character*200 fich,fich1

write(6,*) "Localized states ",trim(fich0)

write(fich1,"(A10)") "_00-00.out"
fich=trim(fich0)//fich1
fich=trim(dir)//"/"//trim(adjustl(fich))

open(1,file=fich)
write(1,*) n0,n0
do i=1,n0
 write(1,900) (mat(i,j),j=1,n0)
enddo
close(1)
do jpar=1,nparent
 write(fich1,"(A4,I2.2,A4)") "_00-",jpar,".out"
 fich=trim(fich0)//fich1
 fich=trim(dir)//"/"//trim(adjustl(fich))
 open(1,file=fich)
 write(1,*) n0,naug
 prev2=n0+(jpar-1)*naug
 do i=1,n0
  write(1,900) (mat(i,j+prev2),j=1,naug)
 enddo
 close(1)
enddo

do ipar=1,nparent
 write(6,*) "Augmented states parent ",ipar

 prev1=n0+(ipar-1)*naug
 do jpar=ipar,nparent
  write(fich1,"(A1,I2.2,A1,I2.2,A4)") "_",ipar,"-",jpar,".out"
  fich=trim(fich0)//fich1
  fich=trim(dir)//"/"//trim(adjustl(fich))
!  write(6,"(A)") fich
  open(1,file=fich)
  write(1,*) naug,naug
  prev2=n0+(jpar-1)*naug
  do i=1,naug
   write(1,900) (mat(i+prev1,j+prev2),j=1,naug)
  enddo
  close(1)
 enddo

enddo

 

900 format(1000000(E30.20e3))
write(6,'(A)') "Happy"
end
