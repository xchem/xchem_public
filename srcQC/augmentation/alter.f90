!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program alter_orb
implicit none
integer nbas,norb,nalter
character*100 fich
real*8, allocatable :: MO(:,:)
integer alt1,alt2
logical ex


integer i,j
real*8 kk
character*100 forma,typ

write(6,*) "Number of basis functions and file with the original orbitals"
read(5,*) nbas,fich
write(6,*) "Considering basis ",nbas
allocate( MO(nbas,nbas))

inquire (file=fich,exist=ex)
if (ex) then
 write(6,*) "Reading from ",trim(fich)
else
 write(6,*) "Original orbitals not found ",trim(fich)
 stop "Orbital file not found"
endif

write(6,*) "File format (MOLCAS, binary, free)"
read(5,*) forma

typ="read"
call readwriteorb(typ,fich,nbas,MO,forma,nbas)
write(6,*) "Orbitals ready"

write(6,*) "Number of exchanges "
read(5,*) nalter
do i=1,nalter
 write(6,*) "Orbitals to exchange ",i
 read(5,*) alt1,alt2
 if (alt1.lt.0.or.alt1.gt.nbas) then
  write(6,*) "The orbital ",alt1," does not exist"
  stop
 endif
 if (alt2.lt.0.or.alt2.gt.nbas) then
  write(6,*) "The orbital ",alt2," does not exist"
  stop
 endif
 if (alt1.ne.alt2) then
  do j=1,nbas
   kk=MO(j,alt1)
   MO(j,alt1)=MO(j,alt2)
   MO(j,alt2)=kk
  enddo
 endif
 write(6,*) "Orbitals exchanged ",alt1,alt2
enddo

write(6,*) "Number of orbitals not zero that are gonna be written"
read(5,*) norb
write(6,*) "Writting new orbitals ",norb
write(6,*) "Writting as zero orbitals bigger than ",norb

write(6,*) "Output file and format (MOLCAS, binary,free)"
read(5,*) fich,forma
typ="write"
call readwriteorb(typ,fich,nbas,MO,forma,norb)


end

subroutine readwriteorb(typ,fich,nbas,MO,forma,norb)
implicit none
integer, intent(in) :: nbas,norb
character*100, intent(in) :: typ,fich,forma
real*8, intent(inout) :: MO(nbas,nbas)

integer i,j

if (typ == "read") then
 write(6,*) "Reading orbitals from ",trim(fich)
 write(6,*) "The format is ",trim(forma)
else if (typ == "write") then
 write(6,*) "Writting orbitals on ",trim(fich)
 write(6,*) "The used format is ",trim(forma)
else
 write(6,*) "Selected file ",trim(fich)
 write(6,*) "Selected format ",trim(forma)
 write(6,*) trim(typ)
 stop 'Read or write?'
endif

if (typ == "write" .and. norb.lt.nbas) then
 do i=norb+1,nbas
  do j=1,nbas
   MO(j,i)=0.
  enddo
 enddo
endif

if (forma == "MOLCAS") then
 open(1,file=fich)
 do i=1,nbas
  if (typ == "read") read(1,*)
  if (typ == "write") write(1,"(A10,I6)") "* ORBITAL ",i
  if (typ == "read") read(1,"(4E18.12e2)") (MO(j,i),j=1,nbas)
  if (typ == "write") write(1,"(4E18.12e2)") (MO(j,i),j=1,nbas)
 enddo
 close(1)
else if (forma == "binary") then
 open(1,file=fich,form="unformatted")
 if (typ == "read") then 
        read(1)
        read(1)
        read(1) ((MO(j,i),j=1,nbas),i=1,nbas)
 elseif (typ == "write") then
        write(1) 1
        write(1) nbas
        write(1) ((MO(j,i),j=1,nbas),i=1,nbas)
 endif
 close(1)

else if (forma == "free" ) then
 open(1,file=fich)
 if (typ == "read") read(1,*) ((MO(j,i),j=1,nbas),i=1,nbas)
 if (typ == "write") write(1,"(2x,5e20.12)") ((MO(j,i),j=1,nbas),i=1,nbas)
 close(1)


else
 stop 'Format not known'
endif

write(6,*) "Orbitals exchanged succesfully"
end
