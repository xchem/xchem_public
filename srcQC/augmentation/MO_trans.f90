!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program MO_trans
implicit none
integer nbas,norb
real*8, allocatable :: orb(:,:)

character*100 fich,typ
integer i,j
logical ex

write(6,*) "Number of basis functions"
read(5,*) nbas
allocate (orb(nbas,nbas))

write(6,*) "Input file with the orbitals and type (free, MOLCAS, binary)"
read(5,*) fich,typ
call readMO(nbas,orb,fich,typ)


write(6,*) "Output format and type"
read(5,*) fich,typ
call writeMO(nbas,orb,fich,typ)

end
