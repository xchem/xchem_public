!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program eigvalues
use diagonalizations
implicit none
integer n1,n2,nprop
real*8, allocatable :: Ham(:,:),ov(:,:),eigv(:)
real*8 thrs
integer nli

character*100 fich,way

integer i,j,iprop
logical ex

write(6,*) "Hamiltonian file and type (triangular or square)"
read(5,*) fich,way
call readmat0(1,fich,way,n2)
allocate(Ham(n2,n2))
call readmat1(1,way,n2,Ham)

write(6,*) "Overlap file and type (triangular or square or ON)"
read(5,*) fich,way
if (way.eq."ON") then
 allocate(ov(n2,n2))
 ov=0.d0
 do i=1,n2
  ov(i,i)=1.d0
 enddo
 n1=n2
else    
 call readmat0(1,fich,way,n1)
 allocate(ov(n1,n1))
 call readmat1(1,way,n1,ov)
endif


write(6,*) "File for the eigenfunctions"
read(5,*) fich

if (n1.ne.n2) then
 stop 'Marices overlap and Hamiltonian with different dimensions'
endif

allocate(eigv(n1))
write(6,*) "Select a new threshold for linear dependencies"
read(5,*) thrs
call eig(n1,Ham,ov,eigv,thrs)


open(1,file=fich)
do i=1,n1
 write(1,*) i,eigv(i)
enddo
do i=1,n1
 write(1,*) "Eigenvalue ",i,eigv(i)
 write(1,*) Ham(:,i)
enddo

write(6,*) "Number of properties to reevaluate"
read(5,*) nprop
do iprop=1,nprop
 write(6,*) "Property file and type (triangular or square) ",iprop
 read(5,*) fich
 call readmat0(1,fich,way,n2)
 if (n2.ne.n1) stop 'Property file must have the same size than Hamiltonian'
 call readmat1(1,way,n2,ov)
 ov=matmul(transpose(Ham),ov)
 ov=matmul(ov,Ham)
 write(6,*) "File to write the property"
 read(5,*) fich
 open(1,file=fich)
 write(1,*) n1
 do i=1,n1
  write(1,"(1000000(x,E20.10e3))") ov(i,:)
 enddo
 close(1)
enddo

end

subroutine readmat0(un,fich,way,n)
implicit none
integer, intent(in) :: un
character*100, intent(in) :: fich,way
integer, intent(out) :: n

integer i,nf
logical ex
character*100 line
integer ntokens

inquire(file=fich,exist=ex)
if (ex) then
 open(un,file=fich)
 select case(way)
  case("triangular")
   read(un,"(A)") line
   read(line,*) n
  case("square")
   read(un,"(A)") line
   nf=ntokens(line)
   if (nf.eq.1) then
    read(line,*) n
    write(6,*) "Square matrix with ",n
    i=n
   else if (nf.eq.2) then
    read(line,*) n,i
    write(6,*) "You ask for square but there are two dimensions ",n,i
    write(6,*) "Must be equal"
   else
    stop
   endif
   if (i.ne.n) then
    write(6,*) "Square matrix has different rows and columns ",trim(fich)
    stop 'Matrix is not square'
   endif
  case default 
   stop 'Matrix must be triangular or square'
 end select
else
 write(6,*) "File with matrix does not exist ",trim(fich)
 stop 'Matrix file is missing'
endif
end subroutine

subroutine readmat1(un,way,n,mat)
implicit none
integer, intent(in) :: un,n
character*100, intent(in) :: way
real*8, intent(out) :: mat(n,n)

integer i,j

select case(way)
 case("triangular")
  do i=1,n
   read(un,*) (mat(i,j),j=1,i)
  enddo
  do i=1,n
   do j=1,i
    mat(j,i)=mat(i,j)
   enddo
  enddo
 case("square")
  do i=1,n
   read(un,*) (mat(i,j),j=1,n)
  enddo
end select

close(un)

end subroutine



integer function ntokens(line)
character,intent(in):: line*(*)
integer i, n, toks

i = 1;
n = len_trim(line)
toks = 0
ntokens = 0
do while(i <= n)
   do while(line(i:i) == ' ') 
     i = i + 1
     if (n < i) return
   enddo
   toks = toks + 1
   ntokens = toks
   do
     i = i + 1
     if (n < i) return
     if (line(i:i) == ' ') exit
   enddo
enddo
end function ntokens 
