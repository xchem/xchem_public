!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PRogram Det2CSF

use CSFtools
use Configuration_

implicit none
integer :: norb,norb0
type(Configuration) outCSFs

integer npot,nCSF,nfich
character*100 fich,tmpDet,fichout

integer totalDets,mdet
real*8, allocatable :: CIDet(:,:),CICSF(:,:)
integer*1, allocatable :: Detlist(:,:),Detlist2(:,:)

integer :: iCSF,iroot,i,ifich
real*8, allocatable :: innorm(:),outnorm(:)
real*8 :: redthr=+1.e-15
real*8 Detnorm
character*100 way

write(6,*) "File with the guga table"
read(5,*) fich 
write(6,*) fich
open(1,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File does not exist ",trim(fich)
 stop 'File with the guga table is compulsory'
endif
write(6,*) "Reading guga table"
mdet=0
read(1,*) nCSF,norb
call outCSFs%initConfiguration(norb,nCSF)
call outCSFs%readit(1,mdet)
close(1)
write(6,*) "Number of Determinants to compare ",mdet

write(6,*) "Number of Det files with the same guga"
read(5,*) nfich
write(6,*) nfich


do ifich=1,nfich
 write(6,*) "Files with the Det and CI vectors for file and way ",ifich
 read(5,*) fich,fichout,way
 write(6,"(3(x,A))") trim(fich),trim(fichout),trim(way)

 open(1,file=fich,status="old",iostat=i)
 if (i.ne.0) then
  write(6,*) "File does not exist ",trim(fich)
  stop 'File with CI vector is compulsory'
 endif
 read(1,*) npot,totalDets,norb0
 write(6,*) "Determinants in the CI ",totalDets," ",trim(fich)," ",trim(fichout)
!!! Creating list of Determinants adapted to the out GUGA table
!allocate (Detlist2(totalDets,norb) )
!allocate(Detlist(totalDets,norb0),CIDet(npot,totalDets) )

!! Getting the CI in the CICSF vector temporally
 allocate(CICSF(npot,totalDets),Detlist2(totalDets,norb0))
 i=0
 do iCSF=1,totalDets
  read(1,*) tmpDet,(CICSF(iroot,iCSF),iroot=1,npot)
  read(tmpDet,"(100I1.1)") (Detlist2(iCSF,iroot),iroot=1,norb0)
  Detnorm=0.
  do iroot=1,npot
   Detnorm=Detnorm+CICSF(iroot,iCSF)**2
  enddo
  if (Detnorm.ge.redthr) i=i+1
 enddo
 close(1)
 write(6,*) "Reduction of Determinants ",totalDets,i
 allocate(CIDet(npot,i),Detlist(i,norb0))
 i=0
 do iCSF=1,totalDets
   Detnorm=0.
  do iroot=1,npot
   Detnorm=Detnorm+CICSF(iroot,iCSF)**2
  enddo
  if (Detnorm.ge.redthr) then
   i=i+1
   CIDet(:,i)=CICSF(:,iCSF)
   Detlist(i,:)=Detlist2(iCSF,:)
  endif
 enddo
 totalDets=i
 deallocate(CICSF,Detlist2)

!!!!! Orbital check
 allocate(Detlist2(totalDets,norb))
 if (norb.eq.norb0) then
  write(6,*) "GUGA table and CI have the same number of orbitals ... copying"
  Detlist2=Detlist
 else
  if (norb.gt.norb0) then
   write(6,*) "GUGA table contains more active orbitals than the CI ... enlarging"
   do iCSF=1,totalDets
    do i=1,norb0
     Detlist2(iCSF,i)=Detlist(iCSF,i)
    enddo
    do i=norb0+1,norb
     Detlist2(iCSF,i)=0
    enddo
   enddo
  endif
  if (norb.lt.norb0) then
   write(6,*) "GUGA table contains less active orbitals than the CI ... aborting"
   stop "GUGA table contains less active orbitals than the CI ... aborting"
  endif
 endif
!!!!!!!!!!!!!!!!!

 allocate(innorm(npot),outnorm(npot))
 do iroot=1,npot
  innorm(iroot)=0.
  do iCSF=1,totalDets
   innorm(iroot)=innorm(iroot)+CIDet(iroot,iCSF)**2
  enddo
 enddo

 allocate (CICSF(npot,outCSFs%nCSF))
 CICSF=0.
 write(6,*) "Transforming"
 call outCSFs%checkCSFs(npot,Detlist2,totalDets,CIDet,CICSF)
 write(6,*) "Transformation done"
 
 do iroot=1,npot
  outnorm(iroot)=0.
  do iCSF=1,nCSF
   outnorm(iroot)=outnorm(iroot)+CICSF(iroot,iCSF)**2
  enddo
  write(6,*) "Norm ",iroot,innorm(iroot),outnorm(iroot)
  if ((innorm(iroot)-outnorm(iroot))**2.ge.0.0001) then
   write(6,*) "Warning root ",iroot
   write(6,*) "Norms (Det,CSF) ",innorm(iroot),outnorm(iroot)
  endif
 enddo

 call writeCSF(way,fichout,1,npot,nCSF,CICSF)
 deallocate(innorm,outnorm,CICSF,Detlist2,CIDet,Detlist)
enddo

end program Det2CSF
