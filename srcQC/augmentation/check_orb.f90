!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program checkorb
implicit none
integer nbas,norb
real*8, allocatable :: ov(:,:),MO(:,:)

character*100 fich,way
logical ex
integer i,j,k,l
real*8 kk,thrs

write(6,*) "Number of basis and file with the basis overlap"
read(5,*) nbas, fich
allocate(ov(nbas,nbas))
inquire(file=fich,exist=ex)
if (ex) then
 open(1,file=fich)
 do i=1,nbas
  read(1,*) (ov(j,i),j=1,i)
 enddo
 do i=1,nbas
  do j=1,i
   ov(i,j)=ov(j,i)
  enddo
 enddo
endif

write(6,*) "Number of orbitals, file with the orbitals and format"
write(6,*) "0 orbitals consider the size of the basis"
write(6,*) "Format can be MOLCAS of free"
read(5,*) norb, fich, way
if (norb.eq.0) norb=nbas
allocate(MO(nbas,norb))
inquire(file=fich,exist=ex)
if (ex) then
 open(1,file=fich)
 if (way == "free") then
  read(1,*) ((MO(j,i),j=1,nbas),i=1,norb)
 else if (way == "MOLCAS") then
  do i=1,norb
   read(1,*) 
   read(1,"(4E18.12e2)") (MO(j,i),j=1,nbas)
  enddo
 else
  stop "Format not known, only MOLCAS of free"
 endif
endif

read(5,*) thrs

do i=1,norb
 do j=1,norb
  kk=0.
  do k=1,nbas
   do l=1,nbas
    kk=kk+MO(k,i)*MO(l,j)*ov(k,l)
   enddo
  enddo
  if (i.eq.j) then
   if ((kk-1.d0)**2.gt.thrs) write(6,*) "Warning ",i,kk
  else
   if (kk**2.gt.thrs) write(6,*) "Warning ",i,j,kk
  endif
 enddo
enddo

write(6,*) "If there is no warning, everything is fine"

end
