!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program overlap

use Configuration_

implicit none
integer :: norb1,norb2
type(Configuration), allocatable :: inCSFs,outCSFs

integer, allocatable :: nroots
character*100, allocatable :: fichCI
character*100 fichDet

integer totalDets
real*8, allocatable :: CIDet(:,:),CICSF(:,:),CIDet2(:,:)
integer*1, allocatable :: Detlist(:,:),Detlist2(:,:)

integer :: n,mdet

integer :: iCSF,i,j,iroot

write(6,*) "Number of orbitals of the input"
read(5,*) norb1
write(6,*) "Number of roots, CSFs  and files with amplitudes"
read(5,*)  nroots,iCSF,fichCI,fichDet
write(6,*) "Reading Determinants from ",trim(fichDet)
call inCSFs%initConfiguration(norb1,iCSF)

mdet=0
open(1,file=fichDet)
call inCSFs%readit(1,mdet)
close(1)
write(6,*) "Input ready, Roots ",nroots

!!! Allocating supervector for the CI in Slater Determinants
write(6,*) "Allocating supervector of Determinants ",mdet
allocate(Detlist2(mdet,norb1),CIDet2(nroots,mdet) )
CIDet2=0.d0

!! Reading CI data
totalDets=0
write(6,*) "Reading CI vector from ",trim(fichCI)
open(1,file=fichCI)
allocate( CICSF(nroots,inCSFs%nCSF) )
do iCSF=1,inCSFs%nCSF
 read(1,*) (CICSF(iroot,iCSF),iroot=1,nroots)
enddo
close(1)
call inCSFs%checkDets(mdet,nroots,Detlist2,totalDets,CIDet2, &
 CICSF,nroots,0)
deallocate(CICSF)

!!! Allocating the real size of CIDet and copying data
write(6,*) "Allocating real number of different Determinants ",totalDets
allocate (CIDet(nroots,totalDets),Detlist(totalDets,norb1) )
open(1,file="CIDet1.out")
do i=1,totalDets
 do j=1,norb1
  Detlist(i,j)=Detlist2(i,j)
 enddo
 do j=1,nroots
  CIDet(j,i)=CIDet2(j,i)
 enddo
 !!! Lets use fichDet as a temporal character array to save the Determinant
 write(fichDet,"(50(I1.1))") (DetList(i,j),j=1,norb1)
 write(1,"(A,x,100(x,E20.10e3))") trim(fichDet),(CIDet(j,i),j=1,nroots)
enddo
close(1)


write(6,*) "Number of orbitals in the output CI" 
write(6,*) "Important to remember that the orbitals must be the same and in the same order that in the input"
read(5,*) norb2
if (norb1.gt.norb1) then
 write(6,*) "The number of orbitals of the input are bigger than the output, overlap makes no sense"
 stop
endif
write(6,*) "Number of the output CSFs and files for the outpu CI and new Determinant amplitudes"
read(5,*)  iCSF,fichCI,fichDet
write(6,*) "Reading output determinants from ",trim(fichDet)
call outCSFs%initConfiguration(norb2,iCSF)

mdet=0
open(1,file=fichDet)
call outCSFs%readit(1,mdet)
close(1)

write(6,*) "Let's create the new CI vector in CSFs"

!!! Reallocating Detlist
deallocate (CIDet2,Detlist2)
allocate (Detlist(totalDets,norb2) )
 
allocate( CICSF(nroots,outCSFs%nCSF) )
CICSF=0.d0
do j=1,totalDets
 do n=1,norb1
  Detlist2(j,n)=Detlist(j,n)
 enddo
 if (norb2.gt.norb1) then
  do n=norb1,norb2
   Detlist2(j,n)=0
  enddo
 endif
  

! Overlapping with CSFs 
 call outCSFs%checkCSFs(nroots,Detlist2,totalDets,CIDet,CICSF )
enddo
write(6,*) "Overlap done, writting outputs"
open(1,file=fichCI)
do iCSF=1,outCSFs%nCSF
 write(1,"(1000(x,E20.10e3))") (CICSF(n,iCSF),n=1,nroots)
enddo
  
     
end program overlap
