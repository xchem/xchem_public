!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program localtoaug
! Program to transform the localized part of an orbital into
! expansion of monocentric functions to represent it at large distances
implicit none

interface
 subroutine readover(n,ov,fich,way)
 implicit none
 character*100, intent(in) :: fich,way
 integer, intent(out) :: n
 real*8,intent(out), allocatable :: ov(:,:)
 end subroutine readover
end interface

integer nbas,nbas0,norb0,nmon
real*8, allocatable :: orb(:,:),ov(:,:)
real*8, allocatable :: x2(:,:),y2(:,:),z2(:,:)

real*8, allocatable :: V2(:,:),G2(:,:),b(:,:)
real*8, allocatable :: corr(:,:)

character*100 fich,way

integer i,j,k,l
real*8 kk,kk1

write(6,*) "File with the overlap and type"
read(5,*) fich,way
call readover(nbas,ov,fich,way)

allocate (orb(nbas,nbas),x2(nbas,nbas),y2(nbas,nbas),z2(nbas,nbas))
write(6,*) "File with the orbitals and type"
read(5,*) fich,way
call readMO(nbas,orb,fich,way)

write(6,*) "File with the x2 property in the basis of Gaussian"
read(5,*) fich
call getprop(nbas,fich,x2)

write(6,*) "File with the y2 property in the basis of Gaussian"
read(5,*) fich
call getprop(nbas,fich,y2)

write(6,*) "File with the z2 property in the basis of Gaussian"
read(5,*) fich
call getprop(nbas,fich,z2)

write(6,*) "Number of localized basis and orbitals"
read(5,*) nbas0,norb0

nmon=nbas-nbas0
write(6,*) "Gaussian monocentric functions ",nmon
!!! Calculate V2, R2 matrix between the monocentroic Gaussians and localized orbitals
allocate (V2(nmon,norb0))
do i=1,nmon
 do j=1,norb0
  V2(i,j)=0.
  do k=1,nbas0
   kk=x2(i+nbas0,k)**2+y2(i+nbas0,k)**2+z2(i+nbas0,k)**2
   V2(i,j)=V2(i,j)+kk*orb(k,j)
  enddo
 enddo
enddo

!! Calculate G2, R2 matrix between monocentric Gaussians
allocate(G2(nmon,nmon))
do i=1,nmon
 do j=1,nmon
  kk=x2(i+nbas0,j+nbas0)**2+y2(i+nbas0,j+nbas0)**2+z2(i+nbas0,j+nbas0)**2
  G2(i,j)=kk
 enddo
enddo

!!! I wanna know how much of the localized orbital "i" is contained into the
!!! monocentric part of the augmented orbital "j"
allocate (b(norb0,nmon))
do i=1,norb0
 do j=1,nmon
  b(i,j)=0.
  do k=1,nbas0
   do l=nbas0+1,nbas
    b(i,j)=b(i,j) -orb(k,i)*ov(k,l)*orb(l,j+norb0)
   enddo
  enddo
 enddo
enddo

!! Calculate correction as: G2^{-1} * V2 * b
allocate(corr(nmon,nmon))
corr=matmul(V2,b)
write(6,*) "Inverting matrix ",nmon
call inv(G2,nmon)
write(6,*) "Inversion done"
corr=matmul(G2,corr)

do i=1,norb0
 orb(:,i)=0.
enddo
do i=1,nbas0
 orb(i,:)=0.
enddo

do i=1,nmon
 do j=1,nmon
  orb(j+nbas0,i+norb0)=orb(j+nbas0,i+norb0) &
                      +corr(j,i)
 enddo
enddo

write(6,*) "Final corrected orbitals, created in binary type"
write(way,"(A)") "binary"
read(5,*) fich
call writeMO(nbas,orb,fich,way)

!!! Checking corrected orbitals
do i=1,nbas
 kk=0.
 kk1=0.
 do j=1,nbas
  do k=1,nbas
   if (j.le.nbas0.and.k.le.nbas0) then
    kk1=kk1+orb(j,i)*orb(k,i)*ov(j,k)
   endif
   kk=kk+orb(j,i)*orb(k,i)*ov(j,k)
  enddo
 enddo
 write(6,*) "Orbital, local norm, monocentric norm ",i,kk1,kk
enddo
 

end


subroutine getprop(n,fich,prop)
implicit none
integer, intent(in) :: n
character*100, intent(in) :: fich
real*8, intent(out) :: prop(n,n)

logical ex
integer i,j

inquire(file=fich,exist=ex)
if (ex) then
 open(1,file=fich)
 do i=1,n
  read(1,*) (prop(i,j),j=1,i)
 enddo
 close(1)
 do i=1,n
  do j=1,i
   prop(j,i)=prop(i,j)
  enddo
 enddo
else
 stop "File with prop does not exist"
endif

return
end

subroutine inv(A,n)
implicit none
integer, intent(in) :: n
real*8, intent(inout) :: A(n,n)

!!! Inversion!!!
real*8 eig(n),U(n,n)
integer i,j

U=A
call diag(U,n,eig)
!!! A= U eig U*
!!! A^-1 = U eig^-1 U*
do i=1,n
 do j=1,n
  A(j,i)=U(j,i)*1./eig(i)
 enddo
enddo

A=matmul(A,transpose(U))

end subroutine inv


