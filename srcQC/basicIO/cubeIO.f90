!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE cubeIO
    IMPLICIT none
    PRIVATE
    PUBLIC OPERATOR(+)
    PUBLIC OPERATOR(*)
    PUBLIC cube_
    TYPE cube_
        PRIVATE 
        LOGICAL :: created=.FALSE.
        CHARACTER*200 :: fileName
        INTEGER :: nAtom
        REAL*8 :: originX,originY,originZ
        REAL*8, ALLOCATABLE :: coordX(:)
        REAL*8, ALLOCATABLE :: coordY(:)
        REAL*8, ALLOCATABLE :: coordZ(:)
        INTEGER :: nX,nY,nZ
        REAL*8 :: dX(3),dY(3),dZ(3)
        REAL*8, ALLOCATABLE :: cubeMatrix(:,:,:)
        
        CONTAINS
            PROCEDURE :: createFromFile
            PROCEDURE :: createEmpty
            PROCEDURE :: writeCube
            PROCEDURE :: angleIntegrate
            GENERIC :: create => createFromFile,createEmpty
    END TYPE

    INTERFACE OPERATOR (*)
        PROCEDURE multiplyScalarCube
        PROCEDURE multiplyCubeCube
    END INTERFACE OPERATOR (*)

    INTERFACE OPERATOR (+)
        PROCEDURE addCube
    END INTERFACE OPERATOR (+)

    CONTAINS

        FUNCTION addCube(a,b) 
            IMPLICIT none
            TYPE(cube_), INTENT(in) :: a,b
            TYPE(cube_) :: addCube
            CALL addCube%create(a)
            addCube%cubeMatrix=a%cubeMatrix+b%cubeMatrix
        END FUNCTION

        FUNCTION multiplyScalarCube(a,b) 
            IMPLICIT none
            REAL*8, INTENT(in) :: a
            TYPE(cube_), INTENT(in) :: b
            TYPE(cube_) :: multiplyScalarCube
            CALL multiplyScalarCube%create(b)
            multiplyScalarCube%cubeMatrix=a*b%cubeMatrix
        END FUNCTION

        FUNCTION multiplyCubeCube(a,b)
            IMPLICIT none
            TYPE(cube_),INTENT(in) :: a,b
            TYPE(cube_) :: multiplyCubeCube
            CALL multiplyCubeCube%create(a)
            multiplyCubeCube%cubeMatrix=a%cubeMatrix*b%cubeMatrix
        END FUNCTION 

        SUBROUTINE createFromFile(this,fich,unIN)
            IMPLICIT none
            CLASS(cube_) :: this
            INTEGER, INTENT(in), OPTIONAL :: unIN
            INTEGER :: un,stat
            CHARACTER(len=*), INTENT(in) :: fich
            REAL*8 :: kk
            INTEGER :: iX,iY,iZ,iAtom
            un=1
            IF (PRESENT(unIN)) un=unIN
            OPEN(un,FILE=TRIM(fich),IOSTAT=stat)
            IF (stat.ne.0) THEN
                STOP "Error in reading or opening cube file"
            ENDIF
            READ(un,*)
            READ(un,*)
            READ(un,*) this%nAtom,this%originX,this%originY,this%originZ
            this%nAtom=ABS(this%nAtom)
            READ(un,*) this%nX,this%dx(1),this%dx(2),this%dx(3)
            READ(un,*) this%nY,this%dy(1),this%dy(2),this%dy(3)
            READ(un,*) this%nZ,this%dz(1),this%dz(2),this%dz(3)
            ALLOCATE(this%coordX(this%nAtom))
            ALLOCATE(this%coordY(this%nAtom))
            ALLOCATE(this%coordZ(this%nAtom))
            ALLOCATE(this%cubeMatrix(this%nZ,this%nY,this%nX))
            DO iAtom=1,this%nAtom
                READ(un,*) iX,kk,this%coordX(iAtom),&
                  this%coordY(iAtom),&
                  this%coordZ(iAtom)
            ENDDO
            READ(un,*) (((this%cubeMatrix(iZ,iY,iX),iZ=1,this%nZ),iY=1,this%nY),iX=1,this%nX)
            this%created=.TRUE.
            CLOSE(un)
        END SUBROUTINE 

        SUBROUTINE createEmpty(this,mould)
            CLASS(cube_) :: this
            CLASS(cube_), INTENT(in) :: mould
            this%fileName="-cloned"
            this%nAtom=mould%nAtom
            this%nX=mould%nX
            this%nY=mould%nY
            this%nZ=mould%nZ
            this%dX=mould%dX
            this%dY=mould%dY
            this%dZ=mould%dZ
            this%originX=mould%originX
            this%originY=mould%originY
            this%originZ=mould%originZ
            ALLOCATE(this%coordX(this%nAtom))
            ALLOCATE(this%coordY(this%nAtom))
            ALLOCATE(this%coordZ(this%nAtom))
            this%coordX=mould%coordX
            this%coordY=mould%coordY
            this%coordZ=mould%coordZ
            ALLOCATE(this%cubeMatrix(this%nZ,this%nY,this%nX))
            this%cubeMatrix=0.d0
            this%created=.TRUE.
        END SUBROUTINE

        SUBROUTINE writeCube(this,fich,unIn)
            CLASS(cube_) :: this
            CHARACTER(len=*), INTENT(in) :: fich
            INTEGER, INTENT(in), OPTIONAL :: unIn
            INTEGER :: un
            INTEGER :: iAtom
            INTEGER :: iX,iY,iZ
            IF(PRESENT(unIN)) un=unin
            un=1
            OPEN(un,FILE=TRIM(fich))
            WRITE(un,'(A)') "Molden file for one electron density"
            WRITE(un,'(A)') "written by Markus' cubeIO class"
            WRITE(un,'(I5,3(F12.6))') this%nAtom,this%originX,this%originY,this%originZ
            WRITE(un,'(I5,3(F12.6))') this%nX,this%dx(1),this%dx(2),this%dx(3)
            WRITE(un,'(I5,3(F12.6))') this%nY,this%dy(1),this%dy(2),this%dy(3)
            WRITE(un,'(I5,3(F12.6))') this%nZ,this%dz(1),this%dz(2),this%dz(3)
            DO iAtom=1,this%nAtom
                WRITE(un,'(I5,4(F12.6))') iAtom,0.0,this%coordX(iAtom),this%coordY(iAtom),this%coordZ(iAtom)
            ENDDO
            WRITE(un,'(6(x,E12.6))') (((this%cubeMatrix(iZ,iY,iX),iZ=1,this%nZ),iY=1,this%nY),iX=1,this%nX) 
            CLOSE(un)
        END SUBROUTINE

        !SUBROUTINE angleIntegrate(this,r,rmaxIn)
        !    CLASS (cube_) :: this
        !    REAL*8, INTENT(out), ALLOCATABLE :: r
        !    REAL*8, INTENT(in), OPTIONAL :: rmaxIn
        !    REAL*8 :: rmax,dr
        !    INTEGER :: iX,iY,iZ
        !    IF (PRESENT(rmaxIn)) rmax=rmaxIn                     
        !    DO iX
        !    ENDDO 
END MODULE
