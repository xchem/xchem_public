!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE basisIO
    IMPLICIT none
    PRIVATE
    PUBLIC basis_
    TYPE basis_
        INTEGER :: nExponent
        INTEGER :: nContraction
        REAL*8,ALLOCATABLE :: exponent(:)
        REAL*8,ALLOCATABLE :: contractionCoef(:,:)
        CONTAINS
            !CREATOR/DESTROYER
            PROCEDURE :: createFromFile
            PROCEDURE :: createEmpty
            PROCEDURE :: destroy
            !GETTERS
            PROCEDURE :: getExponentVector
            PROCEDURE :: getContractionMatrix
            PROCEDURE :: getNContraction
            PROCEDURE :: getNPrimitive
            !SETTERS
            PROCEDURE :: setExponentVector
            PROCEDURE :: setContractionMatrix
            !OUTPUT
            PROCEDURE :: writeBasis
            GENERIC :: create => createFromFile,createEmpty
    END TYPE

    CONTAINS
        SUBROUTINE createFromFile(this,file)
            IMPLICIT none
            CLASS(basis_) :: this
            CHARACTER(len=*),INTENT(in) :: file
            INTEGER :: iostat,iExp,iCon
            LOGICAL :: exist
            INQUIRE(file=file,exist=exist)
            IF (.NOT.exist) STOP "ERROR: basis file does not exist" 
            OPEN(1,file=file,status="old")
            READ(1,*,iostat=iostat) this%nExponent,this%nContraction
            IF (iostat.ne.0) STOP "ERROR in reading basis file"
            IF (this%nContraction.gt.this%nExponent) THEN
                STOP "ERROR number of contractions cant be greater &
                  than number of exponents"
            ENDIF
            ALLOCATE(this%exponent(this%nExponent))
            ALLOCATE(this%contractionCoef(this%nExponent,this%nContraction))
            DO iExp=1,this%nExponent    
                READ(1,*,iostat=iostat) this%exponent(iExp)
                IF(iostat.ne.0) STOP "ERROR in reading exponents"
            ENDDO
            DO iExp=1,this%nExponent    
                READ(1,*,iostat=iostat) (this%contractionCoef(iExp,iCon),iCon=1,this%nContraction)
                IF(iostat.ne.0) STOP "ERROR in reading contraction coefficients"
            ENDDO
            CLOSE(1)
        END SUBROUTINE
        SUBROUTINE createEmpty(this,nPrimitive,nContraction)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER, INTENT(in) :: nPrimitive,nContraction
            this%nExponent=nPrimitive
            this%nContraction=nContraction
            ALLOCATE(this%exponent(this%nExponent))
            ALLOCATE(this%contractionCoef(this%nExponent,this%nContraction))
            this%exponent=0.d0
            this%contractionCoef=0.d0
        END SUBROUTINE
        FUNCTION getNPrimitive(this) RESULT(nPrimitive)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER :: nPrimitive
            nPrimitive=this%nExponent
        END FUNCTION
        FUNCTION getNContraction(this) RESULT(nContraction)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER :: nContraction
            nContraction=this%nContraction
        END FUNCTION
        SUBROUTINE getExponentVector(this,nExp,arr)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER,INTENT(in) :: nExp
            REAL*8 :: arr(nExp)
            arr=this%exponent
        END SUBROUTINE
        SUBROUTINE getContractionMatrix(this,nExp,nCon,arr)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER, INTENT(in) :: nCon,nExp
            REAL*8 :: arr(nExp,nCon)
            arr=this%contractionCoef
        END SUBROUTINE
        SUBROUTINE setExponentVector(this,nExp,arr)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER,INTENT(in) :: nExp
            REAL*8 :: arr(nExp)
            this%exponent=arr
        END SUBROUTINE
        SUBROUTINE setContractionMatrix(this,nExp,nCon,arr)
            IMPLICIT none
            CLASS(basis_) :: this
            INTEGER, INTENT(in) :: nCon,nExp
            REAL*8 :: arr(nExp,nCon)
            this%contractionCoef=arr
        END SUBROUTINE
        SUBROUTINE writeBasis(this,fich)
            IMPLICIT none
            CLASS(basis_) :: this
            CHARACTER(len=*), INTENT(in) :: fich
            INTEGER :: iExp,iCon
            OPEN(1,file=TRIM(fich),STATUS="replace")
            WRITE(1,*) this%nExponent,this%nContraction
            DO iExp=1,this%nExponent    
                WRITE(1,'(E32.24)') this%exponent(iExp)
            ENDDO
            DO iExp=1,this%nExponent    
                WRITE(1,'(3(x,E32.24))') (this%contractionCoef(iExp,iCon),iCon=1,this%nContraction)
            ENDDO
        END SUBROUTINE
        SUBROUTINE destroy(this)
            IMPLICIT none
            CLASS(basis_) :: this
            DEALLOCATE(this%exponent)
            DEALLOCATE(this%contractionCoef)
        END SUBROUTINE
END MODULE

