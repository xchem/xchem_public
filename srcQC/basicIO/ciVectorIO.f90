!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE ciVectorIO
!CLASS FOR OBJECT THAT CONTAIN AND MANIPULATE
!SETS OF CI VECTORS
    IMPLICIT none
    PRIVATE
    PUBLIC ciVector_
    TYPE ciVector_
        PRIVATE
        INTEGER :: nState
        INTEGER :: nCSF
        REAL*8, ALLOCATABLE :: ciCoef(:,:)
        LOGICAL :: alive=.FALSE.
        CONTAINS
            PROCEDURE :: readCI
            PROCEDURE :: writeCI
            PROCEDURE :: initializeEmpty
            PROCEDURE :: getNCSF 
            PROCEDURE :: getNState
            PROCEDURE :: getCiVector
            PROCEDURE :: setCiVector
            PROCEDURE :: getCiVectorCoef
            PROCEDURE :: destroy
            PROCEDURE :: normalize
    END TYPE
    
    CONTAINS
        SUBROUTINE readCI(this,fich,sorte)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            LOGICAL :: bin,exists
            CHARACTER*100 :: token
            INTEGER :: iCSF,iState,nCSF,nState,stat
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            INQUIRE(file=TRIM(fich),EXIST=exists)
            IF (.NOT.exists) STOP "ERROR: ci vector file does not exist"
            IF(bin) THEN
                OPEN(1,file=fich,status="old",form="unformatted",IOSTAT=stat)
                IF (stat.ne.0) THEN
                    STOP "ERROR: binary ciVector File could not be opened"
                ELSE
                    WRITE(6,'(A,A,A,I2)') "Opened binary ci Vector file file: ",TRIM(fich)," in unit ",1
                ENDIF
                READ(1) nState,nCSF
                this%nState=nState
                this%nCSF=nCSF
                ALLOCATE(this%ciCoef(nCSF,nState))
                DO iState=1,nState
                    READ(1) (this%ciCoef(iCSF,iState),iCSF=1,nCSF)
                ENDDO
            ELSE
                OPEN(1,file=fich,status="old",IOSTAT=stat)
                IF (stat.ne.0) THEN
                    STOP "ERROR: formatted ciVector File could not be opened"
                ELSE
                    WRITE(6,'(A,A,A,I2)') "Openend formatted ci Vector file file: ",TRIM(fich)," in unit ",1
                ENDIF
                READ(1,*) nState,nCSF
                this%nState=nState
                this%nCSF=nCSF
                ALLOCATE(this%ciCoef(nCSF,nState))
                DO iState=1,nState
                    !READ(1,'(10(x,E30.20e3))') (this%ciCoef(iCSF,iState),iCSF=1,nCSF)
                    READ(1,*) (this%ciCoef(iCSF,iState),iCSF=1,nCSF)
                ENDDO
            ENDIF 
            this%alive=.TRUE.
            CLOSE(1)
        END SUBROUTINE
        SUBROUTINE writeCI(this,fich,sorte)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            LOGICAL :: bin
            CHARACTER*100 :: token
            INTEGER :: iCSF,iState,nCSF,nState
            IF (.NOT.this%alive) STOP "An basis basis cannot be written before being read or initialized"
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            IF(bin) THEN
                OPEN(1,file=fich,status="replace",form="unformatted")
                nState=this%nState
                nCSF=this%nCSF
                WRITE(1) nState,nCSF
                DO iState=1,nState
                    WRITE(1) (this%ciCoef(iCSF,iState),iCSF=1,nCSF)
                ENDDO
            ELSE
                OPEN(1,file=fich,status="replace")
                nState=this%nState
                nCSF=this%nCSF
                WRITE(1,'(I5.5,x,I10.10)') nState,nCSF
                DO iState=1,nState
                    WRITE(1,'(10(x,E30.20e3))') (this%ciCoef(iCSF,iState),iCSF=1,nCSF)
                ENDDO
            ENDIF 
            CLOSE(1)
        END SUBROUTINE
        SUBROUTINE initializeEmpty(this,nCSF,nState)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(in) :: nCSF,nState
            IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
            this%nState=nState 
            this%nCSF=nCSF
            ALLOCATE(this%ciCoef(nCSF,nState))
            this%ciCoef=0.d0
            this%alive=.TRUE.
        END SUBROUTINE
        SUBROUTINE getNCSF(this,nCSF)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(out) :: nCSF
            IF (.NOT.this%alive) STOP "Information cannot be read from an basis before being read or initialized"
            nCSF=this%nCSF         
        END SUBROUTINE
        SUBROUTINE getNState(this,nState)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(out) :: nState
            IF (.NOT.this%alive) STOP "Information cannot be read from an basis before being read or initialized"
            nState=this%nState
        END SUBROUTINE
        SUBROUTINE getCiVector(this,state,ciVec)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(in) :: state
            REAL*8 , ALLOCATABLE,INTENT(inout) :: ciVec(:)
            IF (.NOT.this%alive) STOP "Information cannot be read from an basis before being read or initialized"
            IF ((state.gt.this%nState).OR.(state.le.0)) STOP "This state does not exist"
            IF (ALLOCATED(ciVec)) THEN 
                IF (SIZE(ciVec,1).ne.this%nCSF) THEN
                    DEALLOCATE(ciVec)
                    ALLOCATE(ciVec(this%nCSF)) 
                ENDIF
            ELSE
                ALLOCATE(ciVec(this%nCSF)) 
            ENDIF
            ciVec=this%ciCoef(:,state)
        END SUBROUTINE
        SUBROUTINE getCiVectorCoef(this,state,csf,coef)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(in) :: state
            INTEGER, INTENT(in) :: csf
            REAL*8 ,INTENT(out) :: coef
            IF (.NOT.this%alive) STOP "Information cannot be read from an basis before being read or initialized"
            IF ((state.gt.this%nState).OR.(state.le.0)) STOP "This state does not exist"
            !DEBUG
            !print*, "you mad?"
            coef=this%ciCoef(csf,state)
        END SUBROUTINE
        SUBROUTINE setCiVector(this,state,ciVec)
            IMPLICIT none
            CLASS(ciVector_) :: this 
            INTEGER, INTENT(in) :: state
            REAL*8 , ALLOCATABLE,INTENT(in) :: ciVec(:)
            IF (.NOT.this%alive) STOP "Information cannot be read from an basis before being read or initialized"
            IF (.NOT.ALLOCATED(ciVec)) STOP "Only an allocated array can be written to a symmetry"
            IF ((state.gt.this%nState).OR.(state.le.0)) STOP "This state does not exist"
            IF (SIZE(ciVec).NE.this%nCSF) STOP "Dimension of CI array and CI object matrix disagree"           
            this%ciCoef(:,state)=ciVec
        END SUBROUTINE
        SUBROUTINE normalize(this)
            IMPLICIT none 
            CLASS(ciVector_) :: this
            INTEGER :: iState,iCSF
            REAL*8 :: norm
            DO iState=1,this%nState
                norm=0.d0
                DO iCSF=1,this%nCSF
                    norm=norm+this%ciCoef(iCSF,iState)**2
                ENDDO
                WRITE(6,'(A,x,I4,x,A,x,F10.5)') "State",iState,"norm:",norm
                norm=sqrt(norm)
                this%ciCoef(:,iState)=this%ciCoef(:,iState)/norm
            ENDDO
        END SUBROUTINE 
        SUBROUTINE destroy(this)
            IMPLICIT none
            class(ciVector_) :: this
            this%alive=.FALSE.
            DEALLOCATE(this%ciCoef)
        END SUBROUTINE
END MODULE
