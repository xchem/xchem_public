!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module hdf5IO
    implicit none
    private
    public hdf5_
    type hdf5_
        !Class variables
        integer :: nRoots
        integer :: nCSFs
        real*8, allocatable :: CIMatrix(:,:)
        contains
            !Class methods
            procedure, private :: hdf5_getCIVectors
            procedure, private :: hdf5_getNRoots
            procedure, private :: hdf5_getNCSFs
            procedure, private :: hdf5_load
            !Generic
            generic, public :: open            => hdf5_open
            generic, public :: getCIVectors    => hdf5_getCIVectors
            generic, public :: getNRoots       => hdf5_getNRoots
            generic, public :: getNCSFs        => hdf5_getNCSFs
    end type 

    contains

    subroutine hdf5_open(this, file)
        class(hdf5_) :: this
        character(len=*), intent(in) :: file
         
    end subroutine

    logical function hdf5_getNRoots(this) result (nRoots) 
        class(hdf5_), intent(in) :: this
        nRoots = this.nRoots
    end function getNRoots

    logical function hdf5_getNCSFs(this) result (nCSFs) 
        class(hdf5_), intent(in) :: this
        nCSFs = this.nCSFs
    end function getNCSFs

    subroutine hdf5_getCIVectors(this, CIMatrix)
        class(hdf5_), intent(in) :: this

    end subroutine

end module hdf5
