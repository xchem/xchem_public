!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module Configuration_

use CSFprop_
USE searchInsert
USE sortingAlgorithms

implicit none

type, public :: Configuration
 integer :: norb,nCSF,nDet
 integer :: maxSpinKey
 integer, allocatable :: detIndex(:,:)
 type(CSFprop), allocatable :: CSF(:)

 contains
  procedure :: initConfiguration
  procedure :: readit
  procedure :: checkDets
  procedure :: checkCSFs
end type Configuration

contains

subroutine initConfiguration(this, norbitals, nCSFs)
 class(Configuration) :: this
 integer norbitals,nCSFs



 this%norb=norbitals
 this%nCSF=nCSFs
! write(6,*) "Init ",norbitals,nCSFs
 allocate(this%CSF(nCSFs))

end subroutine initConfiguration

subroutine readit(this,un,maxdet,indexAnalysisIN)
 implicit none
 class(Configuration) :: this
 integer, intent(in) :: un
 integer :: maxdet

 integer :: nDeterminants
 integer*1 :: dettocheck(this%nOrb)
 character(len=this%norb), allocatable :: tmpchar(:)
 character(len=this%norb) :: tmpchar2
 integer, allocatable :: ampl(:,:)
 
 integer :: iDet,nKey,iCSF,jOrb,iOrb,insertionPoint,i,jCSF
 integer*8 :: key,occKey,spinKey,spinCount,maxSpinKey,maxOccKey
 !integer*8,allocatable  :: tempIndex(:,:)
 integer*8 :: occKeys(this%nCSF),csfOrder(this%nCSF)
 integer*8,allocatable  :: tempKey(:)
 integer*8,allocatable  :: tempVal(:)

 logical,optional :: indexAnalysisIN
 logical :: indexAnalysis

 real*8 norm,kk
 integer :: cc1,cc2,cr1,cr2,shifts
 real*8 :: secs
 character*8 date
 character*10 time

 indexAnalysis=.FALSE.
 IF (present(indexAnalysisIN)) THEN
    indexAnalysis=indexAnalysisIN
 ENDIF

 WRITE(6,'(A)') "Read Guga table"   
 maxdet=0
 shifts=0
 maxSpinKey=0
 maxOccKey=0
 do iCSF=1,this%nCSF
  read(un,*) tmpchar2, nDeterminants
  call this%CSF(iCSF)%initCSFprop(nDeterminants,this%norb)
  maxdet=maxdet+nDeterminants
  do iOrb=1,this%norb
   this%CSF(iCSF)%CSFname(iOrb:iOrb)=tmpchar2(iOrb:iOrb)
  enddo
  allocate (tmpchar(nDeterminants))
  allocate (ampl(2,nDeterminants))
  read(un,*) (ampl(1,iDet),ampl(2,iDet),tmpchar(iDet),iDet=1,nDeterminants)
  call this%CSF(iCSF)%translation(tmpchar,ampl)
!! Checking that the transformation is unitary
  norm=0.d0
  do iDet=1,nDeterminants
   call this%CSF(iCSF)%extractDet(iDet,Dettocheck,kk)
   if (iDet.eq.1) then
    occKey=0
    DO iOrb=1,this%nOrb
        IF (dettocheck(iOrb).eq.3) THEN
            occKey=occKey+2*3**(iOrb-1)
        ELSEIF (dettocheck(iOrb).eq.2.OR.dettocheck(iOrb).eq.1) THEN
            occKey=occKey+3**(iOrb-1)
        ENDIF
        occKeys(iCSF)=occKey
        csfOrder(iCSF)=iCSF
    ENDDO
    IF (occKey.gt.maxOccKey) THEN    
     maxOccKey=occKey
    ENDIF
   endif
   spinKey=0
   spinCount=0
   DO iOrb=1,this%nOrb
       IF (dettocheck(iOrb).eq.1) THEN
           spinKey=spinKey+2**spinCount
           spinCount=spinCount+1
       ELSEIF (dettocheck(iOrb).eq.2) THEN
           spinCount=spinCount+1
       ENDIF
   ENDDO
   if (spinKey.gt.maxSpinKey) THEN
    maxSpinKey=spinKey
   endif
   kk=float(ampl(1,iDet))/float(ampl(2,iDet))
   if (kk.ge.0) then
    kk=sqrt(kk)
   else
    kk=-sqrt(-kk)
   endif
   norm=norm+kk**2
  enddo
  if (norm.le..99 .or. norm.ge.1.01) then
   write(6,*) "Transformation from CSF ",iCSF, "is not unitary ",norm
   write(6,*) ampl(1,:)
   write(6,*) ampl(2,:)
   stop
  endif
  deallocate(tmpchar,ampl)
 enddo
  
 WRITE(6,'(A,I0)') "Identify Determinants, maximum:",maxdet   
 IF (.NOT.indexAnalysis) THEN
    RETURN
 ENDIF
 WRITE(6,'(A,I0)') "Maximum spin key=",maxSpinKey
 WRITE(6,'(A,I0)') "Maximum occ key=",maxOccKey
 WRITE(6,'(A)') "Sort occKeys" 
 CALL QSortInt(this%nCSF,occKeys,csfOrder)
 WRITE(6,'(A)') "Sort done" 

 nKey=0
 !allocate(tempIndex(2,maxdet))
 allocate(tempKey(maxdet))
 allocate(tempVal(maxdet))
 !tempIndex=0
 tempKey=0
 tempVal=0
 call system_clock(cc1,cr1)
 do iCSF=1,this%nCSF
    IF (mod(iCSF,10000).eq.0) THEN
        call system_clock(cc2,cr2)
        secs=2*REAL(cc2-cc1)/REAL(cr1+cr2)
        WRITE(6,'(A,F14.8,A,I0,A,I0,A,I0)') "time (secs) ",secs," so far ",&
          iCSF," CSFs done. Dets so far:",nKey," shifts ",shifts
    ENDIF
    !if (iCSF.eq.1) then
    ! call date_and_time(date,time)
    ! write(6,"(A,2(x,A))") "Starting the transformation ",date,time
    ! write(6,"(4(x,A10),x,A8,x,A10)") "Percentage","actualCSF","Diff Dets","Total CSF","date","time"
    !else
    ! do i=1,100
    !  call date_and_time(date,time)
    !  if (float(iCSF-1).lt. i*.01*this%nCSF.and. float(iCSF).ge. i*.01*this%nCSF) then
    !   write(6,"(4(x,I10),2(x,A))") i,iCSF,nKey,this%nCSF,date,time
    !   exit
    !  endif
    ! enddo
    !endif
    !print*, "DEBUG iCSF,nDet",iCSF,this%CSF(iCSF)%nDet
    !occKey=0
    !call this%CSF(iCSF)%extractDet(1,Dettocheck,kk)
    !DO iOrb=1,this%nOrb
    !    jOrb=this%nOrb-iOrb
    !    IF (dettocheck(iOrb).eq.3) THEN
    !        occKey=occKey+2*3**jOrb
    !    ELSEIF (dettocheck(iOrb).eq.2.OR.dettocheck(iOrb).eq.1) THEN
    !        occKey=occKey+3**jOrb
    !    ENDIF
    !ENDDO
    !occKey=maxOccKey-occKey

    jCSF=csfOrder(iCSF)
    occKey=occKeys(iCSF)
    !CALL this%CSF(jCSF)%extractDet(1,Dettocheck,kk)
    !DO iOrb=1,this%nOrb
    !    IF (dettocheck(iOrb).eq.3) THEN
    !        occKey=occKey+2*3**(iOrb-1)
    !    ELSEIF (dettocheck(iOrb).eq.2.OR.dettocheck(iOrb).eq.1) THEN
    !        occKey=occKey+3**(iOrb-1)
    !    ENDIF
    !ENDDO
    do iDet=1,this%CSF(jCSF)%nDet
        call this%CSF(jCSF)%extractDet(iDet,Dettocheck,kk)
        spinKey=0
        spinCount=0
        DO iOrb=1,this%nOrb
            IF (dettocheck(iOrb).eq.1) THEN
                spinKey=spinKey+2**spinCount
                spinCount=spinCount+1
            ELSEIF (dettocheck(iOrb).eq.2) THEN
                spinCount=spinCount+1
            ENDIF
        ENDDO
        key=(maxSpinKey+1)*occKey+spinKey
        !key=0
        !DO iOrb=1,this%nOrb
        !    key=key+dettocheck(iOrb)*4**(iOrb-1)
        !ENDDO
        IF (nKey.gt.0) THEN
          CALL binarySearch(key,nKey,tempKey(1:nKey),insertionPoint)
          IF (insertionPoint.gt.0) THEN
              CALL insert(key,insertionPoint,nKey,maxdet,tempKey)
              CALL insert(nKey+1,insertionPoint,nKey,maxdet,tempVal)
              shifts=shifts+nKey-insertionPoint
              nKey=nKey+1
          ENDIF
        ELSE
          nKey=nKey+1
          tempKey(1)=key
          tempVal(1)=nKey
        ENDIF
    enddo
 enddo

 WRITE(6,'(A,I0)') "Number of unique determinants identified: ",nKey
 WRITE(6,'(A,I0)') "Number of shifts carried out: ",shifts
 call system_clock(cc2,cr2)
 secs=2*REAL(cc2-cc1)/REAL(cr1+cr2)
 WRITE(6,'(A,F14.8,A,I0,A,I0,A,I0)') "time (secs) ",secs," so far ",&
   iCSF," CSFs done. Dets so far:",nKey," shifts ",shifts
 ALLOCATE(this%detIndex(nKey,2))
 this%detIndex(:,1)=tempKey(1:nKey)
 this%detIndex(:,2)=tempVal(1:nKey)
 DEALLOCATE(tempKey) 
 DEALLOCATE(tempVal) 
 this%maxSpinKey=maxSpinKey
 this%nDet=nKey
 maxdet=nKey
end subroutine readit

subroutine checkDets(this,mdet,ntot,Detlist,totalDets,CIDet, &
 CSFDet,nroots,prevroots)
implicit none
class(Configuration) :: this
integer ntot,mdet,totalDets
integer*1 :: Detlist(mDet,this%norb)
integer*1 :: Dettocheck(this%norb)
real*8 :: CIDet(ntot,mdet),CSFDet(nroots,this%nCSF)

integer, intent(in) :: nroots,prevroots

integer :: iCSF,j,k,iDet
logical ex,diffDet
real*8 :: ampl
real*8 kk

character*8 date
character*10 time

!MARKUS VARS FOR EFFICIENT SEARCH
INTEGER :: spinKey,occKey,spinCount,maxSpinKey
INTEGER :: key,nKey,insertionPoint 
INTEGER :: iOrb

totalDets=0
detlist=-1
maxSpinKey=this%maxSpinKey
do iCSF=1,this%nCSF
! write(6,*) "Checking CSF ",iCSF,this%nCSF
 !!!! Getting from CSFprop the determinants of the iCSF
 if (iCSF.eq.1) then
  call date_and_time(date,time)
  write(6,"(A,2(x,A))") "Starting the transformation ",date,time
  write(6,"(4(x,A10),x,A8,x,A10)") "Percentage","actualCSF","Diff Dets","Total CSF","date","time"
 else
  do j=1,10
   call date_and_time(date,time)
   if (float(iCSF-1).lt. j*.1*this%nCSF.and. float(iCSF).ge. j*.1*this%nCSF) then
    write(6,"(4(x,I10),2(x,A))") j*10,iCSF,totalDets,this%nCSF,date,time
   endif
  enddo
 endif
 do j=1,this%CSF(iCSF)%nDet
 !MARKUS IMPLENT EFFICIENT SEACRCH FOR EXISTING DETS
  ! Taking the Determinant to check
  call this%CSF(iCSF)%extractDet(j,Dettocheck,ampl)
  if (j.eq.1) then
    occKey=0
    DO iOrb=1,this%nOrb
        IF (dettocheck(iOrb).eq.3) THEN
            occKey=occKey+2*3**(iOrb-1)
        ELSEIF (dettocheck(iOrb).eq.2.OR.dettocheck(iOrb).eq.1) THEN
            occKey=occKey+3**(iOrb-1)
        ENDIF
    ENDDO
  endif
  spinKey=0
  spinCount=0
  DO iOrb=1,this%nOrb
      IF (dettocheck(iOrb).eq.1) THEN
          spinKey=spinKey+2**spinCount
          spinCount=spinCount+1
      ELSEIF (dettocheck(iOrb).eq.2) THEN
          spinCount=spinCount+1
      ENDIF
  ENDDO
  key=(maxSpinKey+1)*occKey+spinKey
  CALL binarySearch(key,this%nDet,this%detIndex(:,1),insertionPoint)
  IF (insertionPoint.gt.0) THEN
      STOP "ERROR Unindexed Determinant encountered. Weird!"
  ELSEIF (insertionPoint.eq.0) THEN
      WRITE(6,*) "XCHEM_WARNING insertionPoint 0, check the results carefully"
  ELSE
      iDet=this%detIndex(ABS(insertionPoint),2)
  ENDIF

  IF (Detlist(iDet,1).eq.-1) THEN 
    totalDets=totalDets+1
    DO k=1,this%nOrb
      Detlist(iDet,k)=dettocheck(k)
    ENDDO
  ENDIF
  do k=1,ntot
    CIDet(prevroots+k,iDet)=CIDet(prevroots+k,iDet)+&
      ampl*CSFDet(k,iCSF)
  enddo
  !!! Increasing the number of different total Determinants
  !if (diffDet) then
  ! totalDets=totalDets+1
  ! iDet=totalDets
  ! do k=1,this%norb
  !  Detlist(totalDets,k)=Dettocheck(k)
  ! enddo
! !  write(6,"(A14,2(x,I7.6),x,50(I1.1))") "!!!NEW DET!!!!",iDet,ampl,Detlist(iDet,:)
! ! else
! !  write(6,"(A14,2(x,I7.6),x,50(I1.1))") "!!!OLD DET!!!!",totalDets,ampl,Dettocheck
! !  write(6,"(A14,x,I6.6,x,50(I1.1))") "The same that ",iDet,Detlist(iDet,:)
  !endif
  !!WRITE(6,'(<this%nOrb>(I1),x,I)') dettocheck,iDet
  !do k=1,ntot
  ! CIDet(prevroots+k,iDet)=CIDet(prevroots+k,iDet)+ &
  ! ampl*CSFDet(k,iCSF)
  !enddo

 enddo
enddo
if (totalDets.ne.this%nDet) then
    STOP "ERROR some expected determinants were not encountered"
endif
end subroutine checkDets

subroutine checkCSFs(this,nroots,Detlist,totalDets,CIDet,CICSF)
implicit none
class(Configuration) :: this
integer, intent(in) :: nroots,totalDets
integer*1, intent(in) :: Detlist(totalDets,this%norb)
real*8, intent(in) :: CIDet(nroots,totalDets)
real*8 :: CICSF(nroots,this%nCSF)

integer*1 :: Dettocheck(this%norb)
real*8 :: ampl

integer :: iCSF,j,k,iDet,iroot,norb
logical sameoc

!MARKUS VARS
INTEGER :: detIndex(totalDets,2)
INTEGER :: key,nKey
INTEGER :: iOrb,insertionPoint

character*8 date
character*10 time

norb=this%norb

!MARKUS SPEED UP EFFICIENT SEARCH
detIndex=0
DO iDet=1,totalDets
    key=0
    DO iOrb=1,nOrb  
        key=key+Detlist(iDet,iOrb)*4**(iOrb-1)
    ENDDO
    IF (iDet.eq.1) THEN
        CALL insertKeyValue(key,iDet,0,iDet-1,totalDets,detIndex)
    ELSE
        CALL binarySearch(key,iDet-1,detIndex(1:iDet-1,1),insertionPoint)
        IF (insertionPoint.lt.0) THEN
            WRITE(6,'(A,I0)') "ERROR following determinat found twice:",detIndex(ABS(insertionPoint),2)
            STOP "ERROR Doubly appearing determinant in Detlist"
        ELSE
            CALL insertKeyValue(key,iDet,insertionPoint,iDet-1,totalDets,detIndex)
        ENDIF
    ENDIF
ENDDO
!print*, "DetIndex"
!DO iDet=1,totalDets
!    print*,detIndex(iDet,1),detIndex(iDet,2)
!ENDDO

do iCSF=1,this%nCSF
 if (iCSF.eq.1) then
  call date_and_time(date,time)
  write(6,"(A,2(x,A))") "Starting the transformation ",date,time
  write(6,"(3(x,A10),x,A8,x,A10)") "Percentage","actualCSF","Total CSF","date","time"
 else
  do j=1,10
   call date_and_time(date,time)
   if (float(iCSF-1).lt. j*.1*this%nCSF.and. float(iCSF).ge. j*.1*this%nCSF) then
    write(6,"(3(x,I10),2(x,A))") j*10,iCSF,this%nCSF,date,time
   endif
  enddo
 endif

 !!!! Getting from CSFprop the determinants of the iCSF
 do j=1,this%CSF(iCSF)%nDet
!  ! Taking the Determinant to check
  call this%CSF(iCSF)%extractDet(j,Dettocheck,ampl)
  key=0
  DO iOrb=1,nOrb  
      key=key+Dettocheck(iOrb)*4**(iOrb-1)
  ENDDO
  CALL binarySearch(key,totalDets,detIndex,insertionPoint)
  IF (insertionPoint.lt.0) THEN
    iDet=detIndex(ABS(insertionPoint),2)
    do iroot=1,nroots
     CICSF(iroot,iCSF)=CICSF(iroot,iCSF)+ampl*CIDet(iroot,iDet)
    enddo
  ENDIF
 enddo
enddo

!END MARKUS

!!MARKUS COMMENT ALL THIS TO IMPLEMENT EFFICIENT SEARCH ABOVE
! do j=1,this%CSF(iCSF)%nDet
!!  ! Taking the Determinant to check
!  call this%CSF(iCSF)%extractDet(j,Dettocheck,ampl)
!!  write(6,"(A21,3(x,I6.6),x,50(I1.1))") "Checking Determinant ",iCSF,j,this%CSF(iCSF)%nDet,Dettocheck
!!  write(6,"(F10.5,x,300(I1.1))") ampl,Dettocheck
!!~  do k=1,this%norb
!!~   Dettochek(k)=this%CSF(iCSF)%Dets(j,k)
!!~  enddo
!  ! Comparing the j determinant of the iCSF with the list of the main program
!  !$OMP PARALLEL &
!  !$OMP DEFAULT (none) &
!  !$OMP PRIVATE (iDet,sameoc,k,iroot) &
!  !$OMP SHARED (Detlist,Dettocheck,totalDets,ampl,norb,CICSF,nroots,iCSF,CIDet) 
!  !$OMP DO
!  do iDet=1,totalDets
!   sameoc=.true.
!   k=0
!   do while (sameoc.and.k.lt.norb)
!    k=k+1
!    ! write(6,"(A15,I6.6,x50(I1.1))") "Comparing list ",iDet,DetList(iDet,:)
!    if (Detlist(iDet,k).ne.Dettocheck(k)) sameoc=.false.
!   enddo
!   !!! Including the value of the CSF
!   if (sameoc) then 
!    do iroot=1,nroots
!     CICSF(iroot,iCSF)=CICSF(iroot,iCSF)+ampl*CIDet(iroot,iDet)
!    enddo
!!   else
!!    write(6,*) "Warning a configuration is not included in the CSF list"
!!    write(6,"(100I1.1)") (Detlist(iDet,k),k=1,this%norb)
!   endif
!  enddo
!  !$OMP END DO NOWAIT
!  !$OMP END PARALLEL
! enddo
!enddo
 
end subroutine checkCSFs

end module Configuration_
