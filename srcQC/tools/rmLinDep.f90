!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM orthoNormalize
    USE orbitalIO
    USE oneIntIO
    USE orbitalToolKit
    IMPLICIT none
    CHARACTER*200 :: inputFile
    LOGICAL :: exists
    INTEGER :: stat
    
    CHARACTER*200 :: orbitalFile1
    CHARACTER*200 :: orbitalFile2
    CHARACTER*200 :: ovFile
    CHARACTER*200 :: linIndepOrbitalFile   
   
    TYPE(orbital_) :: orbIn1
    TYPE(orbital_) :: orbIn2
    TYPE(orbital_) :: orbOut
    TYPE(oneInt_) :: ov

    INTEGER :: nOrb       
    INTEGER :: nBas
    INTEGER :: nSym
    INTEGER :: nVirt
    INTEGER, ALLOCATABLE :: symSize(:)
    REAL*8, ALLOCATABLE :: orbVec(:)
    REAL*8, ALLOCATABLE :: orbMat(:,:)
    REAL*8, ALLOCATABLE :: ovBasMat(:,:)
    REAL*8, ALLOCATABLE :: ovOrbMat(:,:)
    REAL*8, ALLOCATABLE :: newOrb(:,:)
    REAL*8, ALLOCATABLE :: virtOrb(:,:)
    REAL*8, ALLOCATABLE :: ovVirt(:,:)
    REAL*8, ALLOCATABLE :: ovNewVirt(:,:)
    REAL*8 :: thr

    INTEGER :: inac(8),ras2(8)

    INTEGER :: iSym
    INTEGER :: iBas
    INTEGER :: iOrb

    NAMELIST /INPUT/ orbitalFile1,&
      orbitalFile2,&
      ovFile,&
      linIndepOrbitalFile,&
      inac,&
      ras2,&
      thr
  
    inac=-1
    ras2=-1
    thr=1e-8
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
  
    CALL ov%readOneInt(ovFile,"f",.TRUE.,1,"o")
    CALL ov%getNSym(nSym)
    CALL ov%getSymSize(symSize)

    WRITE(6,'(A,I0)') "Number of symmetries= ",nSym
    CALL orbIn1%readOrbital(orbitalFile1,"f","pro")
    CALL orbIn2%readOrbital(orbitalFile2,"f","pro")
    CALL orbOut%initializeEmpty(symSize)

    DO iSym=1,nSym
        WRITE(6,'(A,I0)') "Symmetry: ",iSym
        nBas=symSize(iSym)
        nOrb=ras2(iSym)
        nVirt=nBas-nOrb-inac(iSym)
        IF (nOrb.eq.0) THEN
            DO iOrb=1,nBas
                CALL orbIn1%getOrbital(orbVec,iOrb,iSym)
                CALL orbOut%setOrbitalVec(orbVec,iOrb,iSym)
            ENDDO
            CYCLE
        ENDIF
        ALLOCATE(orbMat(nBas,nOrb*2))
        ALLOCATE(ovOrbMat(nOrb*2,nOrb*2))
        ALLOCATE(newOrb(nBas,nOrb))
        ALLOCATE(virtOrb(nBas,nVirt))
        ALLOCATE(ovVirt(nVirt,nVirt))
        ALLOCATE(ovNewVirt(nOrb,nVirt))
        orbMat=0.d0
        ovBasMat=0.d0
        newOrb=0.d0
        virtOrb=0.d0
        ovNewVirt=0.d0
        ovVirt=0.d0
        DO iOrb=1,nOrb
            CALL orbIn1%getOrbital(orbVec,inac(iSym)+iOrb,iSym)
            orbMat(:,iOrb)=orbVec
        ENDDO
        DO iOrb=1,nOrb
            CALL orbIn2%getOrbital(orbVec,inac(iSym)+iOrb,iSym)
            orbMat(:,iOrb+nOrb)=orbVec
        ENDDO
        DO iOrb=1,nVirt
            CALL orbIn1%getOrbital(orbVec,inac(iSym)+nOrb+iOrb,iSym)
            virtOrb(:,iOrb)=orbVec
        ENDDO
        CALL ov%getSym(iSym,ovBasMat)
        WRITE(6,'(A,I0,A)') "Overlap between active Orbitals of sym ",iSym," before removal of linear dependencies"
        CALL overlaps(nOrb*2,nBas,orbMat,ovOrbMat,ovBasMat)
        WRITE(6,'(A)') "Do Gram Schmidit of active orbitals of 1st set and 2nd set to remove components common to both from those of 2nd set."
        CALL gramSchmidt(nOrb*2,nBas,orbMat,ovBasMat)
        WRITE(6,'(A,I0,A)') "Overlap between active Orbitals of sym ",iSym," after removal of linear dependencies"
        CALL overlaps(nOrb*2,nBas,orbMat,ovOrbMat,ovBasMat)
        DO iOrb=1,nOrb
            newOrb(:,iOrb)=orbMat(:,iOrb+nOrb)
        ENDDO
        WRITE(6,'(A,I0,A)') "Overlap between virtual Orbitals of sym ",iSym," before removal of linear dependencies"
        CALL overlaps(nVirt,nBas,virtOrb,ovVirt,ovBasMat)
        WRITE(6,'(A,I0,A)') "Overlap between ON Orbitals of sym ",iSym," from 2nd orbital set with virtual Orbitals from first"
        CALL overlaps2(nBas,nOrb,nVirt,newOrb,virtOrb,ovNewVirt,ovBasMat)
        CALL rmLinDepOrbOrb(nBas,nOrb,nVirt,newOrb,virtOrb,ovNewVirt,thr)
        !CALL rmLinDep(nOrb,nBas,orbMat,ovOrbMat,thr)
        !WRITE(6,'(A)') "Overlap between Orbitals after removal of linear dependencies"
        !CALL overlaps(nOrb,nBas,orbMat,ovOrbMat,ovBasMat)

        WRITE(6,'(A)') "DEBUG!!"
        DO iOrb=1,inac(iSym)
            CALL orbIn1%getOrbital(orbVec,iOrb,iSym)
            CALL orbOut%setOrbitalVec(orbVec,iOrb,iSym)
        ENDDO
        DO iOrb=1,2*nOrb
            orbVec=orbMat(:,iOrb)
            CALL orbOut%setOrbitalVec(orbVec,iOrb+inac(iSym),iSym)
        ENDDO
        DO iOrb=1,nVirt-nOrb
            orbVec=virtOrb(:,iOrb+nOrb)
            CALL orbOut%setOrbitalVec(orbVec,iOrb+inac(iSym)+2*nOrb,iSym)
        ENDDO
        DEALLOCATE(newOrb)
        DEALLOCATE(virtOrb)
        DEALLOCATE(orbMat)
        DEALLOCATE(ovOrbMat)
        DEALLOCATE(ovNewVirt)
        DEALLOCATE(ovVirt)
    ENDDO
    CALL orbOut%writeOrbital(TRIM(linIndepOrbitalFile)//".RasOrb","f")
    CALL orbOut%writeOrbital(TRIM(linIndepOrbitalFile),"f","pro")
END PROGRAM
