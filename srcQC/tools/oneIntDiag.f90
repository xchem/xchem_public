!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntDiag
    USE diagonalizations
    USE oneIntIO
    TYPE(oneInt_) :: oneInt
    CHARACTER*100 :: fich
    REAL*8, ALLOCATABLE :: eVal(:)
    REAL*8, ALLOCATABLE :: oneIntMat(:,:)
    REAL*8, ALLOCATABLE :: U(:,:)
    INTEGER :: nOrb,iOrb,stat,i,j
    REAL*8 :: val

    WRITE(6,'(A)') "Specify orbital file:"
    READ(5,*) fich
    CALL oneInt%readOneInt(TRIM(fich),"b",3)
    CALL oneInt%getSymSize(nOrb)
    ALLOCATE(eVal(nOrb))
    ALLOCATE(U(nOrb,nOrb))
    ALLOCATE(oneIntMat(nOrb,nOrb))
    eVal=0.d0
    U=0.d0
    oneIntMat=0.d0
    DO 
        CALL oneInt%getRecord(i,j,val,stat)
        IF (stat.ne.0) EXIT
        oneIntMat(i,j)=val
    ENDDO
    WRITE(6,'(A)') "eigenvalues"
    U=oneIntMat
    CALL diag(U,nOrb,eVal)
    DO iOrb=1,nOrb
        WRITE(6,'(F18.12)') eVal(iOrb)
    ENDDO
END PROGRAM

