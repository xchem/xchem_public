!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM wcBIN
!CODE TO COUNT LINES IN BINARY FILE
        IMPLICIT none
        INTEGER :: n,n0
        CHARACTER*300 :: fich,fich2,sorte
        LOGICAL :: exists
        INTEGER :: stat,i,j,k,l
        REAL*8 :: val
        LOGICAL :: bin
        bin=.true.
        fich=" "
        sorte=" "
        n=0
        n0=0
        CALL GETARG(1,fich)
        !CALL GETARG(2,sorte)
        IF (LEN(TRIM(sorte)).gt.0) THEN
            IF (sorte(1:1).eq."f") bin=.FALSE.
        ENDIF
        IF (LEN(TRIM(fich)).eq.0) STOP "ERROR no file provided"
        INQUIRE(FILE=TRIM(fich),EXIST=exists)
        IF (.NOT.exists) STOP "ERROR file does not exist"
        OPEN(1,FILE=TRIM(fich),FORM="unformatted",STATUS="old") 
        !WRITE(fich2,'(A,A)'),TRIM(fich),".new"
        !IF (bin) THEN
        !    OPEN(2,file=TRIM(fich2),FORM="unformatted",STATUS="replace") 
        !ELSE
        !    OPEN(2,file=TRIM(fich2),STATUS="replace") 
        !ENDIF
        READ(1) i
        !IF (bin) THEN
        !    WRITE(2) i
        !ELSE
        !    WRITE(2,*) i
        !ENDIF
        DO
                READ(1,IOSTAT=stat) i,j,k,l,val
                IF (IS_IOSTAT_END(stat)) EXIT
                n=n+1 
                !IF (ABS(val).lt.1e-18) THEN 
                !    n0=n0+1 
                !ELSE
                !    IF (bin) THEN
                !        WRITE(2) i,j,k,l,val
                !    ELSE 
                !        WRITE(2,'(4(x,I5),x,E18.12)') i,j,k,l,val
                !    ENDIF          
                !ENDIF
                IF (mod(n,1000000).eq.0) write(6,'(A,I0)') "records so far= ",n
        ENDDO  
        WRITE(6,'(A,I0,A,I0)') "number if records: ",n," of which are zero ",n0
END PROGRAM
