!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM showIntegrals
    IMPLICIT none
    INTEGER :: i,j,k,l
    INTEGER :: stat
    INTEGER :: iInt
    INTEGER :: maxIndex,nOrbitals,nIntegrals
    INTEGER :: iOrb,jOrb
    REAL*8 :: integral
    REAL*8, ALLOCATABLE :: mat(:,:)
    CHARACTER*200 :: fich
    CHARACTER*200 :: fich2,fmt
    CHARACTER*1 :: mFmt
    CHARACTER*1 :: shapeMat

    WRITE(6,'(A)') 'File with integrals:'
    READ(5,*) fich
    WRITE(6,'(A)') 'Matrix format (s)parse or (a)rray'
    READ(5,'(A1)') mFmt

    OPEN(1,FILE=TRIM(fich),FORM='unformatted')

    IF (mFmt.eq."s") THEN
        READ(1) nOrbitals
        WRITE(6,'(A,I)') "Orbital indeces: ",nOrbitals

        WRITE(6,'(A)') 'Maximum index to be considered:'
        READ(5,*) maxIndex

        WRITE(6,'(A)') "Writing Orbitals accoring to these specifications:"

        DO 
            READ(1,IOSTAT=stat) i,j,k,l,integral
            IF (stat.ne.0) EXIT
            IF (i.gt.maxIndex.OR.j.gt.maxIndex.OR.k.gt.maxIndex.OR.l.gt.maxIndex) CYCLE
            WRITE(6,'(4(x,I4),x,E24.16)') i,j,k,l,integral
        ENDDO
    ELSE 
        WRITE(6,'(A)') "Shape of matrix: (s)quare or (t)riangular"
        READ(5,*) shapeMat
        WRITE(6,'(A)') "Where to write to"
        READ(5,*) fich2
        OPEN(10,file=trim(fich2),status="replace")
        READ(1)
        READ(1) nOrbitals
        WRITE(6,'(A,I0)') "Size of matrix=",nOrbitals
        ALLOCATE(mat(nOrbitals,nOrbitals))
        mat=0.d0
        DO iOrb=1,nOrbitals
            IF (shapeMat.eq."s") THEN
                READ(1) (mat(jOrb,iOrb),jOrb=1,nOrbitals)
            ELSE
                READ(1) (mat(jOrb,iOrb),jOrb=1,iOrb)
            ENDIF
        ENDDO
        WRITE(fmt,'(A,I0,A)') "(",nOrbitals,"(x,E30.20e3))"
        WRITE(10,*) 1
        WRITE(10,*) nOrbitals
        DO iOrb=1,nOrbitals
            WRITE (10,fmt) (mat(jOrb,iOrb),jOrb=1,nOrbitals)
        ENDDO
    ENDIF
END PROGRAM 
