!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM orbitalFmt
    USE orbitalIO
    IMPLICIT none

!CONTROL VARS
    CHARACTER*200 :: inputFile
    INTEGER :: stat
    LOGICAL :: exists

!INPUT VARS 
    CHARACTER*200 :: inOrbitalFile
    CHARACTER*200 :: outOrbitalFile
    CHARACTER*200 :: inFormat
    CHARACTER*200 :: outFormat
    INTEGER :: firstOrb
    INTEGER :: lastOrb
    INTEGER :: firstBas
    INTEGER :: lastBas

!STRUCT VARS
    TYPE (orbital_) :: orb
    INTEGER :: iOrb,iBas,jOrb
    REAL*8, ALLOCATABLE :: orbVec(:)
    REAL*8, ALLOCATABLE :: orbMat(:,:)

    NAMELIST /INPUT/ inOrbitalFile,&
      outOrbitalFile,&
      inFormat,&
      outFormat,&
      firstOrb,&
      lastOrb,&
      firstBas,&
      lastBas

    inOrbitalFile=""
    outOrbitalFile=""
    inFormat=""
    outFormat=""
    firstOrb=-1
    lastOrb=-1
    firstBas=-1
    lastBas=-1

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    
    CALL orb%readOrbital(TRIM(inOrbitalFile),TRIM(inFormat)) 

    ALLOCATE(orbMat(lastBas-firstBas+1,lastOrb-firstOrb+1))
    orbMat=0.d0

    jOrb=0
    OPEN(1,FILE=TRIM(outOrbitalFile),STATUS="replace")
    DO iOrb=firstOrb,lastOrb
        jOrb=jOrb+1
        CALL orb%getOrbital(orbVec,iOrb)
        orbMat(:,jOrb)=orbVec(firstBas:lastBas)
        !print*, iOrb
    ENDDO
    DO iBas=1,lastBas-firstBas+1
        WRITE(1,'(3(x,E18.12))') (orbMat(iBas,iOrb),iOrb=1,lastOrb-firstOrb+1)
    ENDDO
    CLOSE(1)
END PROGRAM
