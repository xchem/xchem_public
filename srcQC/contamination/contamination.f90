!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
    USE orbitalIO
    USE diagonalizations 
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(orbital_) :: orb
    CHARACTER*200 :: inputFile
   
!INPUT VARIABLE
    CHARACTER*200 :: monMonFile
    CHARACTER*200 :: monLocFile
    CHARACTER*200 :: locLocFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: newOrbitalFile
    CHARACTER*200 :: method
    INTEGER :: nLocBas
    INTEGER :: nMonBasTotal
    INTEGER :: nMonBas
    INTEGER :: nLocOrb
    INTEGER :: nDifOrb
    INTEGER :: nOrb
    INTEGER :: nBasTotal
    INTEGER :: occupied
    REAL*8 :: thr
!STUCTURAL VARIABLES
    REAl*8, ALLOCATABLE :: locLocArr(:,:)
    REAl*8, ALLOCATABLE :: monMonArr(:,:)
    REAL*8, ALLOCATABLE :: monLocArr(:,:)
    REAL*8, ALLOCATABLE :: oM(:,:)
    REAL*8, ALLOCATABLE :: contam(:,:)
    REAL*8, ALLOCATABLE :: D(:,:),lambda(:,:),eVal(:)
    REAL*8, ALLOCATABLE :: ULX(:,:)

    INTEGER :: nMonBasLI
    INTEGER, ALLOCATABLE :: basisIndex(:)

!TEMP VARIABLE

    INTEGER :: counter
    INTEGER :: iBas,jBas,kBas
    INTEGER :: iOrb,jOrb
    CHARACTER*200 :: token
    REAL*8 :: val,norm
    REAL*8, ALLOCATABLE :: tempVec(:)
    REAL*8, ALLOCATABLE :: b(:)
    REAL*8, ALLOCATABLE :: x(:)
    REAL*8, ALLOCATABLE :: test(:)

    NAMELIST /INPUT/    monMonFile ,&
        monLocFile ,&
        locLocFile ,&
        orbitalFile ,&
        nLocBas, &
        nLocOrb,&
        nOrb,&
        newOrbitalFile,&
        method,&
        thr
  
    method="lstSq"
    thr=1e-8

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

   

    IF (nLocOrb.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
    nDifOrb=nOrb-nLocOrb


    OPEN(2,file=TRIM(monMonFile),STATUS="old")
    nMonBas=0
    DO 
        READ(2,*) token,token,token,iBas
        IF (nMonBas.eq.0) THEN
            kBas=iBas
        ELSE
            IF (kBas.ne.iBas) EXIT
        ENDIF
        READ(2,*) token,token,token,jBas
        READ(2,*)
        nMonBas=nMonBas+1
    ENDDO
    WRITE(6,'(A,I0)') "Number of monocentric relevant basis functions=",nMonBas
    ALLOCATE(basisIndex(jBas))
    basisIndex=0
    REWIND(2)
    counter=0
    DO 
        counter=counter+1
        IF (counter.gt.nMonBas) EXIT
        READ(2,*) token,token,token,iBas
        READ(2,*) token,token,token,jBas
        READ(2,*)
        basisIndex(jBas)=counter
    ENDDO

    ALLOCATE(monMonArr(nMonBas,nMonBas))
    ALLOCATE(monLocArr(nLocBas,nMonBas))
    ALLOCATE(locLocArr(nLocBas,nLocBas))
    ALLOCATE(contam(nLocBas,nMonBas))
    ALLOCATE(test(nMonBas))
    ALLOCATE(b(nMonBas))
    ALLOCATE(x(nMonBas))
    monMonArr=0.0
    monLocArr=0.0
    REWIND(2)
    DO 
        READ(2,*,IOSTAT=stat) token,token,token,iBas
        IF (stat.ne.0) EXIT
        READ(2,*) token,token,token,jBas
        READ(2,*) token,token,val
        monMonArr(basisIndex(iBas),basisIndex(jBas))=val
    ENDDO
    CLOSE(2)
    WRITE(6,'(A)') "monMon Integrals Read"

    OPEN(2,file=TRIM(monLocFile),STATUS="old")
    DO
        READ(2,*,IOSTAT=stat) token,token,token,iBas
        IF (stat.ne.0) EXIT
        READ(2,*) token,token,token,jBas
        READ(2,*) token,token,token,token,val
        monLocArr(iBas,basisIndex(jBas))=val
    ENDDO
    CLOSE(2)

    WRITE(6,'(A)') "locLoc Integrals Read"

    OPEN(2,file=TRIM(locLocFile),STATUS="old")
    DO
        READ(2,*,IOSTAT=stat) token,token,token,iBas
        IF (stat.ne.0) EXIT
        READ(2,*) token,token,token,jBas
        READ(2,*) token,token,token,token,val
        locLocArr(iBas,jBas)=val
        IF (iBas.ne.jBas) locLocArr(jBas,iBas)=val
    ENDDO
    CLOSE(2)

    WRITE(6,'(A)') "monLoc Integrals Read"
    WRITE(6,'(A,E18.12)'),"largest val in monLoc Partial Integrals= ",maxval(abs(monLocArr))
    WRITE(6,'(A,E18.12)'),"largest val in monMon Partial Integrals= ",maxval(abs(monMonArr))
    WRITE(6,'(A,E18.12)'),"largest val in locMon Partial Integrals= ",maxval(abs(locLocArr))


    IF (TRIM(method).eq."lstSq") THEN
        WRITE(6,'(A)') "Express r>Rmin locs in mons using least square fit to ||Ax-b||=0"
        DO iBas=1,nLocBas
            WRITE(6,'(A,I0)') "Local Basis Fct=",iBas
            b=monLocArr(iBas,:)
            x=MATMUL(monMonArr,b)
            norm=SQRT(DOT_PRODUCT(b,x))
            WRITE(6,'(A,E18.12)') "    Norm of PCG: ",locLocArr(iBas,iBas)
            WRITE(6,'(A,E18.12)') "    Norm of projection on MCGs: ",norm
            !CALL linSolver_dgesv(nMonBas,monMonArr,b,x) 
            IF (norm.ge.1e-14) THEN
                CALL linSolver_dgelsd(nMonBas,monMonArr,b,x) 
                contam(iBas,:)=x
                x=MATMUL(monMonArr,x)
                test=x-b
                norm=SQRT(DOT_PRODUCT(x,MATMUL(monMonArr,x)))
                WRITE(6,'(A,E18.12)') "    Of this can be expressed in MCGs: ",norm
                norm=SQRT(DOT_PRODUCT(test,MATMUL(monMonArr,test)))
                WRITE(6,'(A,E18.12)') "    Leaving unaccounted for (||Ax-b||): ",norm
            ELSE
                WRITE(6,'(A)') "Negligible!"
                contam(iBas,:)=0.d0
            ENDIF
            WRITE(6,'(A)') ""
        ENDDO
    ELSEIF (TRIM(method).eq."diag") THEN    
        WRITE(6,'(A)') "Express r>Rmin locs by diagonalizing <m_i|m_j>"
        ALLOCATE(D(nMonBas,nMonBas))
        ALLOCATE(lambda(nMonBas,nMonBas))
        ALLOCATE(ULX(nMonBas,nMonBas))
        ALLOCATE(eVal(nMonBas))
        D=monMonArr
        lambda=0.d0
        eVal=0.d0
        CALL diag(D,nMonBas,eVal)
        thr=thr*MAXVAL(eVal)
        nMonBasLI=0
        DO iBas=1,nMonBas
            IF (eVal(iBas).gt.thr) THEN
                nMonBasLI=nMonBasLI+1
                lambda(iBas,iBas)=1/eVal(iBas)
            ELSE
                D(:,iBas)=0.d0
            ENDIF
        ENDDO
        WRITE(6,'(A,I0)') "Rank reductiont to ",nMonBasLI
        ULX=MATMUL(MATMUL(D,lambda),TRANSPOSE(D)) 
        DO iBas=1,nLocBas
            WRITE(6,'(A,I0)') "Local Basis Fct=",iBas
            b=monLocArr(iBas,:)
            x=MATMUL(monMonArr,b)
            norm=SQRT(DOT_PRODUCT(b,x))
            WRITE(6,'(A,E18.12)') "    Norm of PCG: ",locLocArr(iBas,iBas)
            WRITE(6,'(A,E18.12)') "    Norm of projection on MCGs: ",norm
            x=MATMUL(b,ULX)
            contam(iBas,:)=x
            x=MATMUL(monMonArr,x)
            test=x-b
            norm=SQRT(DOT_PRODUCT(x,MATMUL(monMonArr,x)))
            WRITE(6,'(A,E18.12)') "    Of this can be expressed in MCGs: ",norm
            norm=SQRT(DOT_PRODUCT(test,MATMUL(monMonArr,test)))
            WRITE(6,'(A,E18.12)') "    Leaving unaccounted for (||Ax-b||): ",norm
            WRITE(6,'(A)') ""
        ENDDO
    ENDIF
   
    


    WRITE(6,'(A)') "Loading orbitals" 
    CALL orb%readOrbital(orbitalFile,"b") 
    CALL orb%getNOrb(nBasTotal)
    WRITE(6,'(A,I0)') "Basis fcts in orbital file: ",nBasTotal
    nMonBasTotal=nBasTotal-nLocBas
    ALLOCATE(oM(nBasTotal,nBasTotal))
    DO iOrb=1,nOrb
        CALL orb%getOrbital(tempVec,iOrb)
        oM(:,iOrb)=tempVec
    ENDDO
    CALL orb%destroy()

    WRITE(6,'(A)') "Transform protruding local part as far as possible"
    DO iOrb=nLocOrb+1,nOrb
        WRITE(6,'(A,I0)') "Working on orbital ",iOrb
        DO iBas=1,nLocBas 
            DO jBas=nLocBas+1,SIZE(basisIndex)
                IF (basisIndex(jBas).gt.0) THEN
                    oM(jBas,iOrb)=oM(jBas,iOrb)+&
                        oM(iBas,iOrb)*contam(iBas,basisIndex(jBas))
                ENDIF
            ENDDO
        ENDDO
    ENDDO

    CALL orb%initializeEmpty(nBasTotal)
    CALL orb%setSym(1,oM)
    CALL orb%writeOrbital(TRIM(newOrbitalFile)//".RasOrb","f")
    CALL orb%writeOrbital(TRIM(newOrbitalFile)//".bin","b")
END PROGRAM
