# XCHEM - The *ab initio* Solution for Multichannel Scattering Problems

## Prerequisits
The following are needed to run the XCHEM code:

* __python3__ with modules 
	* __pyparsing__ (There may be some problems with blank lines depending on the python3 version used, removing these lines solve the problems)
	* __six__
	* __h5py__
    * (optional - for automatic plots) __matplotlib__
* __ifort__ version 15.0.2 +
* __MKL__
* (optional - for deferred calculations) __Slurm Workload Manager__ 

## Installation

To run the XCHEM code two main pieces of software must be installed: the XCHEM code itself and  a customized version of [OpenMOLCAS](https://gitlab.com/Molcas/OpenMolcas); the open source version of the quantum chemistry package (QCP) MOLCAS.

### XCHEM

Clone the XCHEM repository by executing the following command 

```
git clone git@gitlab.com:xchem/xchem_public.git
```

In the local directory two sets of source code need to be compiled. The code interfacing with MOLCAS, located in the directory srcQC, and the code for the computation of scattering states (using the GABS basis), located in srcScat. To compile these navigate to the respective directories and execute 

```
make all
```

### OpenMOLCAS

Clone the XCHEM-Fork of OpenMOLCAS by executing the following command 

```
git clone -b xchemqc https://gitlab.com/xchem/OpenMolcas.git
```
Configure OpenMOLCAS by executing 

```
./configure-cmake -compiler intel -omp -opt normal -mkl <MKLPATH>
```

and compile by executing


```
make all
```

in the build directory created during configuration. For a comprehensive guide see the instructions on the [OpenMOLCAS website](https://gitlab.com/Molcas/OpenMolcas). *Important:* OpenMOLCAS must be compiled with hdf5 support. 

## Input Files
To run the complete XCHEM code the following input files need to be provided. A more detailed explanation of the input paramters and expected sturcture of these files is given below. Examples for all these files are included for the case of molecular Hydrogen in the tutorial directory. 

*  __InputFile__ Defines which modules of the xchem code to run and with which paramters.

* __GatewayFile__ File defining the polycentric gaussian basis as well as the molecular geometry, written in the same format as an input for MOLCAS' Gateway module.

* __OrbitalFile__ File containing the active space polycentric gaussian orbitals. Neutral states will be created by augmenting with an additional electron in these orbitals as well as in a set of monocentric gaussian orbitals created by the code. Normally these are obtained by carrying out a CASSCF or RASSCF calculation, for the relevant system. They must be provided in Molcas' INPORB format.

## Running XCHEM
Once the input files are set up, exectuion of XCHEM is done via the python code pyxchem.py in the directory pyxchem. Execute

```
python3 <PATHTOXCHEM>/pyxchem/pyxchem.py <INPUTFILE>
```

For information about valid command line parameters execute

```
python3 <PATHTOXCHEM>/pyxchem/pyxchem.py --help
```

For a list of modules in XCHEM execute

```
python3 <PATHTOXCHEM>/pyxchem/pyxchem.py --info
```

For more detailed information about a module execute 

```
python3 <PATHTOXCHEM>/pyxchem/pyxchem.py --info <MODULE_NAME>
```


### Deferring expensive calculations

Some of the modules of XCHEM may become quite costly depending on the size of the system in question. In these cases it is possible to provide the relevant modules in the input file with the paramter 
```
defer=T
```
Rather than running the entire calculation, the program will execute only the computationally cheap parts, and prepare the other parts for submission to multiple nodes. Submission is then done by providing pyxchem.py with either the flag

```
--submission all
```

or

```
--submission <MODULE_NAME>
```

In the latter case only one module may be submitted at a time, and the order of the modules must be respected.

Submission may be controlled via the flag

```
--remote_dir '<DIRECTORY_PATTERN>'
```

where DIRECTORY_PATTERN should be a path defining the unique scratch space of each job. Environment variables are expanded at run time of the job and may be used to ensure uniqueness of the scratch directories. Use either $XCHEM_JOBID or something like $SLURM_JOBID.

Furthermore, the flag

```
--submission_flags '<FLAG_STRING>'
```

may be used to specifiy the parameters passed to sbatch on submission. E.g. "--cpus-per-task=16 --qos=fast"

## Examples
### Hydrogen (small)
To run this example 

* copy the files in tutorials/hydrogen_small/ to a directory from which to work from.
* open the file h2.input and modify the paramters __xchem__ to the path where you installed XCHEM (alternatively you may delete this line and set the environment variable XCHEM).
* execute:
```
python3 pyxchem.py h2.input
```
*  When the calculation is finished execute:
```
head -10 xchem_data/ccm/CloseCoupling/1A_1A/BoxStates/H_Eigenvalues
```
if everything worked correctly the output should look like

```
         758         723
    1               -1.15374938236163849
    2               -0.91190064019635919
    3               -0.77354940430129648
    4               -0.70052919424206261
    5               -0.68271666575501744
    6               -0.68271666575500611
    7               -0.62711754028684430
    8               -0.61958016495944046
    9               -0.61925039087404243
```
showing the energies of the energies of the energetically lowest nine eigenstates of the close coupling hamiltonian.


