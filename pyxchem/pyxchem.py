#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

#!/usr/bin/env python3
import os, argparse, sys
from steps import env, integrals, basis, orbitals, rdm, qci, ccm, bsplines, boxstates, scatteringstates, dta, preptdse, plot, mfpad, beta
from parsing import xchem_input  
from xchem_error import *


print('')
print('XCHEM - The Ab Initio solution of molecular scattering problems')
print('')

cmd_line_args = argparse.ArgumentParser()
cmd_line_args.add_argument('input_file', nargs='?', help = 'Specifies the input file' )
cmd_line_args.add_argument('--submit', nargs=1, help = 'Specify one and only one module to submit to several nodes', choices=['env','orbitals','integrals','rdm','qci','ccm','bsplines','boxstates','scatteringstates','dta','preptdse','mfpad','beta','all'])
cmd_line_args.add_argument('--info', nargs=1, help = 'Show information about the specified modules', choices=['env','integrals','orbitals','rdm','qci','ccm','bsplines','boxstates','scatteringstates','dta','preptdse','basis','plot','mfpad','beta'] )
cmd_line_args.add_argument('--remote_dir', nargs=1, default=[os.path.join(os.getcwd(), 'XCHEM_REMOTE','$XCHEM_JOBID')], help = 'Scratch directory pattern for submitted jobs. E.g. <PATH_TO_NODE>/$SLURM_JOBID. Evironment variables will be evaluated on the node. You may also use $XCHEM_JOBID.' )
cmd_line_args.add_argument('--submission_flags', nargs='?', help = 'string to be passed to queue system. E.g. for slurm: --qos=... --time=...  --cpus-per-task etc.')
cmd_line_args.add_argument('--only_inputs', action="store_true", help = 'Only inputs are created, nothing is submitted')

if len(sys.argv) == 1:
	cmd_line_args.print_help(sys.stderr)
	sys.exit(1)

args = cmd_line_args.parse_args()

def getStep( step_name, param_dict, info = False ):	
	if ( step_name == 'env' ):
		step = env.Env( param_dict, info = info )
	elif ( step_name == 'basis' ):
		step = basis.Basis( param_dict, info = info )
	elif ( step_name == 'integrals' ):
		step = integrals.Integrals( param_dict, info = info )
	elif ( step_name == 'orbitals' ):
		step = orbitals.Orbitals( param_dict, info = info )
	elif ( step_name == 'rdm' ):
		step = rdm.Rdm( param_dict, info = info )
	elif ( step_name == 'qci' ):
		step = qci.Qci( param_dict, info = info )
	elif ( step_name == 'bsplines' ):
		step = bsplines.Bsplines( param_dict, info = info ) 
	elif( step_name == 'ccm' ):
		step = ccm.Ccm( param_dict, info = info ) 
	elif( step_name == 'boxstates' ):	
		step = boxstates.Boxstates( param_dict, info = info )
	elif (step_name == 'scatteringstates'):
		step = scatteringstates.Scatteringstates( param_dict, info = info ) 
	elif (step_name == 'dta'):
		step = dta.Dta( param_dict, info = info ) 
	elif (step_name == 'mfpad'):
		step = mfpad.Mfpad( param_dict, info = info ) 
	elif (step_name == 'beta'):
		step = beta.Beta( param_dict, info = info ) 
	elif (step_name == 'preptdse'):
		step = preptdse.Preptdse( param_dict, info = info )
	elif (step_name == 'plot'):
		step = plot.Plot( param_dict, info=info)
	elif (step_name == 'exit'):
		print("Encountered &exit keyword. Stopping execution")
		if args.only_inputs:
			with open("tosend.sh","a") as f:
				f.write('"\n')
				f.write("for ijob in $listofjobs; do \n")
				f.write("if [ \"$ijob\" == \"wait\" ]; then wait\n")
				f.write("else chmod +x $ijob;srun -n 1 -o $ijob.log $ijob &\n")
				f.write("fi;done\n")
				f.write("wait\n")
		sys.exit()
	elif ( step_name == 'nowait'):
		step = None
	else:
		raise XchemError('Unknwon Modul name {}'.format(step_name))
	return(step)

# Print info if requested
if args.info:
	print('Printing Information: ')
	for step_name in args.info:
		getStep( step_name, {}, info = True )

# Set up environment variables
if args.input_file:
	input_list = xchem_input.parse( args.input_file ) 
	try:
		i = [ step_name == 'env' for step_name,_ in input_list].index(True)
	except:
		i = -1
	if i >= 0:
		step = env.Env( input_list[i][1] )
		del input_list[i]
	else:
		step = env.Env( {} )
else:
	step = env.Env( {} )
step.run()


# Submit step if submission flag is provided
if args.submit:
	only_inputs=False
	if args.only_inputs:
		only_inputs=True
		with open("tosend.sh","w") as f:
			f.write("#!/bin/bash\n")
			f.write('listofjobs="')
	to_be_submitted = args.submit[0]
	submit_all = to_be_submitted == 'all'
	if args.submission_flags:
		submission_flags = args.submission_flags
	else:
		submission_flags = ' '
	ids_in = []
	ids_out = []
	nowait = False
	step_uids = {}
	for step_name, param_dict in input_list :
		if submit_all or step_name in [to_be_submitted,'nowait','exit']	:
			if step_name == 'nowait':
				nowait = True
			else:
				if only_inputs and nowait==False:
					with open("tosend.sh","a") as f:
						f.write("wait\n")
				step = getStep( step_name, {} )
				if step_name in step_uids:
					step_uids[step_name] += 1
				else:
					step_uids[step_name] = 0
				if 'defer' in step.input_dict:
					if nowait:
						ids_tmp = step.submit( 	args.remote_dir[0], submission_flags, step_uids[step_name], ids_in, only_inputs )
						ids_out += ids_tmp
					else:
						ids_in = ids_out.copy()
						ids_out = step.submit( 	args.remote_dir[0], submission_flags, step_uids[step_name], ids_in, only_inputs )
				nowait = False
	if only_inputs:
		with open("tosend.sh","a") as f:
			f.write('"\n')
			f.write("for ijob in $listofjobs; do \n")
			f.write("if [ \"$ijob\" == \"wait\" ]; then wait\n")
			f.write("else chmod +x $ijob;srun -n 1 -o $ijob.log $ijob &\n")
			f.write("fi;done\n")
			f.write("wait\n")
	sys.exit()

# Main loop
step_uids = {}
for step_name, param_dict in input_list :
	step = getStep( step_name, param_dict )
	if step_name in step_uids:
		step_uids[step_name] += 1
	else:
		step_uids[step_name] = 0
	if step is not None:
		step.run(step_uids[step_name])
