#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

#A set of methods returning input for different molcas modules
import h5_extractors
import os
from basis_set import BasisSet, ghost_basis_creator, twoexp_ghost_basis_creator, multighost_basis_creator

def gateway_input( h5_file=None, sdip = None, monocentric = False, input_dict = {}, symmetry=None, exp1 = None , exp2 = None, blockbasis = False ):
	if input_dict['gateway_desym'] != None:
		fich=input_dict['gatewayFile']
		if h5_file != None:
			if "desym" in h5_file:
				fich=input_dict['gateway_desym']
		fich=os.path.join(os.environ['XCHEM_SUBMIT'],fich)
		with open(fich,"r") as f:
			input_string=f.read()
		if "expert" not in input_string.lower():
			raise XchemError("STOP XCHEM, using gateway files wihtout expert mode ")
	else:
		input_string = "\n&GATEWAY"
		input_string += "\nTitle=xchem"
		input_string += "\nexpert"
		multighost = input_dict['multighost']
		if symmetry: input_string += "\nsymmetry={}".format(symmetry)
		if h5_file:
			basis_sets = h5_extractors.gateway( h5_file )
			for basis_set in basis_sets:
				input_string += basis_set.getAsString()
	if (monocentric):
#VICENT MODIFICATION
		multighost=input_dict['multighost']
		if (multighost):
			ghost_basis_sets = multighost_basis_creator( input_dict )  
		else:
			if (exp1):
				ghost_basis_sets = twoexp_ghost_basis_creator( input_dict, exp1, exp2 )
			elif (blockbasis):
				ghost_basis_sets = twoexp_ghost_basis_creator( input_dict, blockbasis = True )
			else:
				ghost_basis_sets = ghost_basis_creator( input_dict )  
#END OF VICENT MODIFICATION
		for basis_set in ghost_basis_sets:
			input_string += basis_set.getAsString()
	return(input_string)

#VICENT MODIFICATION
#def twoexp_gateway_input( exp, lexp, h5_file=None, sdip = None, monocentric = False, input_dict = {}, symmetry=None):
#	input_string = "\n&GATEWAY"
#	input_string += "\nTitle=xchem"
#	input_string += "\nexpert"
#	multighost = input_dict['multighost']
#	if symmetry: input_string += "\nsymmetry={}".format(symmetry)
#	if h5_file:
#		basis_sets = h5_extractors.gateway( h5_file )
#		for basis_set in basis_sets:
#			input_string += basis_set.getAsString()
#	if (monocentric):
#		ghost_basis_sets = twoexp_ghost_basis_creator( input_dict, kexp, lexp )  
#		for basis_set in ghost_basis_sets:
#			input_string += basis_set.getAsString()
#	return(input_string)
#END OF VICENT MODIFICATION

def seward_input( oneo = False, noguess = True, mult = None, options='' ):
	input_string = "\n&SEWARD"
	if (oneo): input_string += "\noneo"
	if (noguess): input_string += "\nnogu"
	if (mult): input_string  += "\nmult={}".format(mult)
	if (options):
		for option in options.split(','):
			input_string  += "\n{}".format(option)
	return(input_string)

def guessorb_input(sthr = 1e-5, tthr = 1e12 ):
	input_string = "\n&GUESSORB"
	input_string += "\nsthr={: 11.4e}".format(sthr)
	input_string += "\ntthr={: 11.4e}".format(tthr)
	return(input_string)

def scf_input():
	input_string = "\n&SCF"
	return(input_string)

def expbas_input(desy = True, noex=True ):
	input_string = "\n&EXPBAS"
	if (desy): input_string += "\ndesy" 
	if (noex): input_string += "\nnoex" 
	return(input_string)

def rasscf_input(input_dict, symm, ciro, spin, lumo = True, prsd = False, prwf = 0.05, desym = False, cion = False, expand_ras3 = 0, tigh = None, cimx = None, expand_nact = None, hexs = False, options=''):
	inac = input_dict['inac']
	ras1 = input_dict['ras1']
	ras2 = input_dict['ras2']
	ras3 = input_dict['ras3']
	nact = input_dict['nact']
	if expand_nact:
		nact = [ i + j for i,j in zip(nact, expand_nact) ]

	input_string = "\n&RASSCF"
	nact = " ".join([str(i) for i in nact ])
	if (desym):
		ras2 = str(sum(ras2))
		if (inac): inac = str(sum(inac))
		if (ras1): ras1 = str(sum(ras1))
		if (ras3):
			ras3 = str(sum(ras3) + expand_ras3)
		else:
			ras3 = str(expand_ras3)
		
	else:
		ras2 = " ".join([str(i) for i in ras2 ])
		if (inac): inac = " ".join([str(i) for i in inac ])
		if (ras1): ras1 = " ".join([str(i) for i in ras1 ])
		if (ras3): ras3 = " ".join([str(i) for i in ras3 ])

	input_string += "\nprwf={}".format(prwf)
	if (lumo): input_string += "\nlumo"
	if (prsd): input_string += "\nprsd"
	if (cion): input_string += "\ncion"
	if (tigh): input_string += "\ntigh=" + str(tigh) + " " + str(tigh)
	if (cimx): input_string += "\ncimx=" + str(cimx) 

	input_string += "\nsymm=" + str(symm)
	input_string += "\nspin=" + str(spin)
	input_string += "\nnact=" + nact
	input_string += "\nciro=" + str(ciro) + " " + str(ciro) + " 1"
	input_string += "\nras2=" + ras2
	if (inac): input_string += "\ninac=" + inac 
	if (ras1): input_string += "\nras1=" + ras1
	if (ras3): input_string += "\nras3=" + ras3
	if hexs:
		input_string += "\nhexs=1;1"
	if (options):
		for option in options.split(','):
			input_string  += "\n{}".format(option)
	return(input_string)
	
def rassi_input(onel = False, mein = False, orbi = False, cipr = False, operator = None, order = 1, return_nprop = True, return_file_list = True, files=None ):
	input_string = "\n&RASSI"
	if (onel): input_string += "\nonel"
	if (mein): input_string += "\nmein"
	if (orbi): input_string += "\norbi"
	if (cipr): input_string += "\ncipr"
	if files is not None:
		input_string += '\nfile'
		input_string += '\n{}'.format(len(files))
		for f in files:
			input_string += '\n{}'.format(f)
	if (return_file_list): file_list = []
	if (operator): 
		nprop_total = int(order*(order**2 + 6*order + 11)/6)
		input_string += '\nprop={}'.format(nprop_total)
		counter = 0
		for iorder in range(1,order +1):
			nprop = int(( iorder + 1 ) * ( iorder +2 ) / 2)
			for iprop in range(1, nprop +1):
				counter += 1
				input_string += "\n'{}  {}'  {}".format(operator, iorder, iprop)
				if return_file_list: file_list.append('prop{:03d}_c{:03d}.out'.format(counter,iprop))
	if operator and return_nprop:
		return(input_string, file_list)
	else:
		return(input_string)
