#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

a = [(0,0),(1,1),(2,2),(3,3)]
print(a)

for num,(x,_) in enumerate(a):
	print("coord {} has x={}".format(num,x))
	#print("coord {} has y={}".format(num,y))
	print("")

a[0] = "Hola Mundo"
a[1] = a
print(a)

b = (1,2,3)
print(b)
try:
	b[0] = 0
except:
	print("Cant change tuple")
print(b)


d = {"nombre":"Vicent", "edad":24, "origen":"valencia", "nombre" : "Pedro"}
print(d)

for key,val in d.items():
	print(key,val)

nombre = d.pop('nombre')
print(d)
print(nombre)
