#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class Coche:
	def __init__(self, color = None, edad = None, marca = None):
		self.color = color
		if type(edad) is int:
			self.edad = edad
		else:
			print("Could not instantiate car with non integer age")
		self.marca = marca

	def repintar( self, new_color ):
		self.color = new_color

class Furgoneta(Coche):
	def __init__(self, color= None, edad=None, marca=None, capacidad = None):
		super(Furgoneta).__init__(color, edad, marca)
		self.capacidad=capacidad
	def caragar( self, peso ):
		if pese  > capacidad:
			print("ni puedo")


c = Coche( color = "red", marca = "vw")
print(c.color)
c.repintar("blue") 
print(c.color)
