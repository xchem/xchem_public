#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class Determinant:
	def __init__( self, file_name ):
		with open( file_name, 'r') as f:
			meta_data = f.readline()
		self.file_name = file_name
		meta_data = meta_data.split()
		self.num_roots = int(meta_data[0])
		self.num_dets = int(meta_data[1])
		self.num_orbs = int(meta_data[2])
		#self.energies = []
		#for root in range(self.num_roots):	
		#	self.energies.append( float( meta_data[ root + 3 ]) )

	def root_energies( self, output_file_name=None, index_shift = 0 ):
		for iroot, energy in enumerate(self.energies):
			file_name = '{}_{:03d}.out'.format(output_file_name, iroot + 1 + index_shift)	
			with open(file_name, 'w') as f:
				f.write('{:.20e}'.format(energy))
