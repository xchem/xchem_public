#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os,re

def compressTwoElectronIntegrals_input( input_dict,
									    molcas_integral_file = None, 
									    compressed_integral_file = None, 
									    dontCompress = False, 
									    printBasInfo = False):
	lMonocentric = input_dict['lMonocentric']
	kMonocentric = input_dict['kMonocentric']
	
	input_string = '&INPUT\n'
	input_string += 'petiteBasisList="petiteBasis"\n'
	input_string += 'blockInfoFile="basis_block.raw.info"\n'
	if molcas_integral_file: input_string += 'twoElectronIntegralsMolcas="{}"\n'.format(molcas_integral_file)
	if compressed_integral_file: input_string += 'twoElectronIntegralsCompressed="{}"\n'.format(compressed_integral_file)
	if dontCompress:
		input_string += 'dontCompress=T\n'
	else:
		input_string += 'dontCompress=F\n'
	if printBasInfo:
		input_string += 'printBasInfo=T\n'
	else:
		input_string += 'printBasInfo=F\n'
	if len(lMonocentric) == 1 and  len(kMonocentric) == 1:
		input_string += 'lMax={}\n'.format(lMonocentric[0])
		input_string += 'kMax={}\n'.format(kMonocentric[0])
	else:
		input_string += 'lkMonocentricFile="lk.dat"\n'
		if not os.path.isfile( 'lk.dat' ):
			with open('lk.dat','w') as f:
				f.write('{}\n'.format(len(lMonocentric)))
				for l,k in zip(lMonocentric,kMonocentric):
					f.write('{} {}\n'.format(l,k))
	input_string += '&END\n'
	return(input_string)

def csf2det_input(ci_file, guga_file, det_file, ascii=True):
	if (ascii):
		input_string = '"{}" ascii\n"{}"\n"{}" ascii\n'.format(ci_file, guga_file, det_file)
	else:
		input_string = '"{}" ascii\n"{}"\n"{}" binary\n'.format(ci_file, guga_file, det_file)
	return(input_string)

def det2csf_input(guga_file, det_file, ci_file, ascii=True):
	if isinstance(det_file,str):
		det_file2=[ det_file ]
	else:
		det_file2=det_file.copy()
	if isinstance(ci_file,str):
		ci_file2=[ ci_file ]
	else:
		ci_file2=ci_file.copy()
	nfich=min(len(det_file2),len(ci_file2))
	if len(ci_file2) != len(det_file2):
		print("XCHEM_WARNING Problem in det2csf:")
		print("XCHEM_WARNING det_file2 ",det_file2)
		print("XCHEM_WARNING ci_file2 ",ci_file2)
		print("XCHEM_WARNING Considering ",nfich)
	if (ascii):
		input_string = '"{}"\n {}\n'.format(guga_file, nfich)
		for i in range(nfich):
			input_string+='"{}" "{}" ascii\n'.format(det_file2[i],ci_file2[i])
	return(input_string)

def csfs2csf_input(ciroParentStates,norb,nameSourceStates,result_file):
	nstates = len(ciroParentStates) * norb + len(nameSourceStates)
	input_string = '{}\n'.format(nstates)
	if nameSourceStates:
		for i,_ in enumerate(nameSourceStates):
			input_string += '"{}.ras3.SRC.csf"\n'.format(i+1)
	for iorb in range(1, norb+1):
		for i,_ in enumerate(ciroParentStates):
			input_string += '"{}.aug_{:02d}.PI.csf"\n'.format(i+1,iorb)
	input_string += '{}\n'.format(result_file)
	return(input_string)

def augment_input(det_file, input_dict, spin):
	ras1 = input_dict['ras1']
	ras2 = input_dict['ras2']
	ras3 = input_dict['ras3']
	norb = 2
	if ras1: norb += sum(ras1)
	if ras2: norb += sum(ras2)
	if ras3: norb += sum(ras3)
	input_string = '"{}"\n{}\n{}\n'.format(det_file, spin, norb)
	return(input_string)

def twoexpconc_input(datadir, nofjob, expslist):
	input_string = '"{}"\n'.format(datadir)
	input_string += str(nofjob)+'\n'
	input_string += str(len(expslist))+'\n'
	for i in range(len(expslist)):
		input_string += expslist[i][:1] + ' ' + expslist[i][2:] +'\n'
	return(input_string)

def trd1_element_input(trd_file, det_file, iState, jState, norb):
	input_string = '&INPUT\n'
	input_string += 'trd1File="{}"\n'.format(trd_file)
	input_string += 'detFile="{}"\n'.format(det_file)
	input_string += 'state1={}\n'.format(iState)
	input_string += 'state2={}\n'.format(jState)
	input_string += 'occupied={}\n'.format(norb)
	input_string += '&END\n'
	return( input_string )

def trd2_element_input(trd_file, det_file, iState, jState, norb):
	input_string = '&INPUT\n'
	input_string += 'trd2File="{}"\n'.format(trd_file)
	input_string += 'detFile="{}"\n'.format(det_file)
	input_string += 'state1={}\n'.format(iState)
	input_string += 'state2={}\n'.format(jState)
	input_string += 'occupied={}\n'.format(norb)
	input_string += '&END\n'
	return( input_string )

def trd1_build_input(trd_file_list,trd_file):
	input_string = '&INPUT\n'
	input_string += 'trd1ElementFileList="{}"\n'.format(trd_file_list)
	input_string += 'trd1File="{}"\n'.format(trd_file)
	input_string += '&END\n'
	return( input_string )

def trd2_build_input(trd_file_list,trd_file):
	input_string = '&INPUT\n'
	input_string += 'trd2ElementFileList="{}"\n'.format(trd_file_list)
	input_string += 'trd2File="{}"\n'.format(trd_file)
	input_string += '&END\n'
	return( input_string )

def block_info_input( input_dict, meta_dict, raw_info_file ):
	input_string = '{} {}\n'.format(len(meta_dict['irreps']), meta_dict['group'].lower())
	input_string += '{}\n'.format( ' '.join(meta_dict['irreps']).lower() )
	if input_dict['inac']:
		input_string += ' '.join( [ ' '.join( [str(sym+1) for i in range(norb)] ) for sym,norb in enumerate(input_dict['inac']) ] ) + ' '
	if input_dict['ras1']:
		input_string += ' '.join( [ ' '.join( [str(sym+1) for i in range(norb)] ) for sym,norb in enumerate(input_dict['ras1']) ] ) + ' '
	if input_dict['ras2']:
		input_string += ' '.join( [ ' '.join( [str(sym+1) for i in range(norb)] ) for sym,norb in enumerate(input_dict['ras2']) ] ) + ' '
	if input_dict['ras3']:
		input_string += ' '.join( [ ' '.join( [str(sym+1) for i in range(norb)] ) for sym,norb in enumerate(input_dict['ras3']) ] ) + '\n'
	else:
		input_string += '\n'
	
	blocks = []
	nblocks = 0
	with open( raw_info_file, 'r')  as f:
		for line in f:
			nblocks += 1
			blocks.append(tuple(line.split()))
	input_string += '{}\n'.format(nblocks)

	for nfct,label,l,m,k,ghost in blocks:
		if meta_dict['x_bas_sym']:
			input_string += '{} {} {} {} {} {} {}\n'.format(nfct, label, l, m, k, ghost, meta_dict['x_bas_sym'][label])
		else:
			input_string += '{} {} {} {} {} {} {}\n'.format(nfct, label, l, m, k, ghost, 1)
	return(input_string)

def linDepKill_input( input_dict, block_info, inporb = 'INPORB'):
	input_string = '&INPUT\n'
	input_string += 'ovFile="ov.int"\n'
	input_string += 'locOrbFile="{}"\n'.format(inporb)
	input_string += 'linDepThr={}\n'.format(input_dict['linDepThr'])
	norb = 0
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	lMonocentric = input_dict['lMonocentric']
	kMonocentric = input_dict['kMonocentric']
	input_string += 'nOrbLoc={}\n'.format(norb)
	input_string += 'linIndepOrbFile="mpcg"\n'
	with open( 'basis_block.info', 'w') as f:
		f.write( block_info )
	input_string += 'blockInfoFile="basis_block.info"\n'
	if len(lMonocentric) == 1 and  len(kMonocentric) == 1:
		input_string += 'lMax={}\n'.format(lMonocentric[0])
		input_string += 'kMax={}\n'.format(kMonocentric[0])
	else:
		input_string += 'lkMonocentricFile="lk.dat"\n'
		if not os.path.isfile( 'lk.dat' ):
			with open('lk.dat','w') as f:
				f.write('{}\n'.format(len(lMonocentric)))
				for l,k in zip(lMonocentric,kMonocentric):
					f.write('{} {}\n'.format(l,k))
	if input_dict['multighost']:
		input_string += 'multiGhost=T\n'
	else:
		input_string += 'multiGhost=F\n'
	input_string += 'orbSymFile="orbs.sym"\n'
	input_string += 'binaryLocOrb=F\n'
	input_string += '&END\n'
	return(input_string)

def ijkl2iqks_input( input_dict, meta_dict, ntask = None):
	norb = 0
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	input_string = '&INPUT\n'	
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'integralFile="ham2E.compressed.int"\n'
	input_string += 'outegralFile="iqks.int"\n'
	input_string += 'occupied={}\n'.format(meta_dict['number_diffuse_orbitals'])
	input_string += 'local={}\n'.format(norb)
	if ntask:
		input_string += 'suffixFile="ij.suffix'+str(ntask)+'"\n'
	else:
		input_string += 'suffixFile="ij.suffix"\n'
	input_string += '&END\n'
	return(input_string)

def ijkl2ijrs_input( input_dict, meta_dict, ntask = None):
	norb = 0
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	input_string = '&INPUT\n'	
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'integralFile="ham2E.compressed.int"\n'
	input_string += 'outegralFile="ijrs.int"\n'
	input_string += 'occupied={}\n'.format(meta_dict['number_diffuse_orbitals'])
	input_string += 'local={}\n'.format(norb)
	if ntask:
		input_string += 'suffixFile="ij.suffix'+str(ntask)+'"\n'
	else:
		input_string += 'suffixFile="ij.suffix"\n'
	input_string += '&END\n'
	return(input_string)

def iqks2pqrs_input( input_dict, meta_dict, ntask=None ):
	norb = 0
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	input_string = '&INPUT\n'	
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'iqksFile="iqks.int"\n'
	input_string += 'ijrsFile="ijrs.int"\n'
	if ntask is not None:
		input_string += 'outegralFile="pqrs.int'+str(ntask)+'"\n'
	else:
		input_string += 'outegralFile="pqrs.int"\n'
	input_string += 'occupied={}\n'.format(meta_dict['number_diffuse_orbitals'])
	input_string += 'local={}\n'.format(norb)
	input_string += '&END\n'
	return(input_string)

def pqrsConcatenate_input():
	input_string = '&INPUT\n'
	input_string += 'orbitalFile="pqrs.int"\n'
	input_string += 'outputFile="pqrs.int"\n'
	input_string += 'IndexesFile="i.index"\n'
	input_string += '&END\n'
	return(input_string)


def ijkl2pqrs_input( input_dict, meta_dict, suf, intfile, ntask=None, path=None, readprev=True ):
	norb = 0
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	input_string = '&INPUT\n'	
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'integralFile="'+intfile+'"\n'
	input_string += 'basisFile="basisHam2E.dat.'+suf+'"\n'
	if ntask is not None:
		if path is None:
			input_string += 'outegralFile="pqrs.int'+str(ntask)+'"\n'
		else:
			input_string += 'outegralFile="'+path+'/pqrs.int'+str(ntask)+'"\n'
	else:
		input_string += 'outegralFile="pqrs.int"\n'
	input_string += 'occupied={}\n'.format(meta_dict['number_diffuse_orbitals'])
	input_string += 'local={}\n'.format(norb)
	input_string += 'rmint=.true.\n'
	if readprev:
		input_string += 'readprev=.true.\n'
	else:
		input_string += 'readprev=.false.\n'
	input_string += '&END\n'
	return(input_string)

def oneIntBas2Orb_input( meta_dict ):
	input_string = '&INPUT\n'	
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'oneElectronOperatorBasisFiles="ham1E.int-ov.int-dip1.int-dip2.int-dip3.int-vel1.int-vel2.int-vel3.int-kin.int"\n'
	input_string += 'oneElectronOperatorOrbitalFiles="ham1E.orb.int-ov.orb.int-dip1.orb.int-dip2.orb.int-dip3.orb.int-vel1.orb.int-vel2.orb.int-vel3.orb.int-kin.orb.int"\n'
	input_string += 'occupied={}\n'.format(meta_dict['number_diffuse_orbitals'])
	input_string += 'hermitian="h-h-h-h-h-a-a-a-h"\n'
	input_string += '&END\n'
	return( input_string )

def twoIntOrb2OperatorMatrix_input( input_dict, meta_dict ):
	ninac = 0
	if input_dict['inac']:
		ninac =   sum(input_dict['inac'])
	if input_dict['nameSourceStates']:
		nsource = sum(meta_dict['numSourceStates'])
	else:
		nsource = 0
	input_string = '&INPUT\n'
	input_string += 'twoIntRDMFile="trd2"\n'
	input_string += 'twoIntFile="pqrs.int"\n'
	input_string += 'outputFile="ham2E.state.int"\n'
	input_string += 'nInactive={}\n'.format(ninac)
	input_string += 'nSource={}\n'.format(nsource)
	input_string += 'nParent={}\n'.format(sum(meta_dict['numParentIons']))
	input_string += '&END\n'
	return( input_string ) 

def oneIntOrb2OperatorMatrix_input( input_dict, operator, meta_dict ):
	ninac = 0
	if input_dict['inac']:
		ninac =   sum(input_dict['inac'])
	if input_dict['nameSourceStates']:
		nsource = sum(meta_dict['numSourceStates'])
	else:
		nsource = 0
	input_string = '&INPUT\n'
	input_string += 'oneIntRDMFile="trd1"\n'
	input_string += 'oneIntFile="{}.orb.int"\n'.format(operator)
	input_string += 'outputFile="{}.state.int"\n'.format(operator)
	input_string += 'nInactive={}\n'.format(ninac)
	input_string += 'nSource={}\n'.format(nsource)
	input_string += 'nParent={}\n'.format(sum(meta_dict['numParentIons']))
	input_string += '&END\n'
	return( input_string ) 

def subtractDiagonal_input( diagonal, operator ):
	input_string = '&INPUT\n'
	input_string += 'oneIntFile="{}.state.int"\n'.format(operator)
	input_string += 'ovFile="ov.state.int"\n'
	input_string += 'diagonal={}'.format( diagonal )
	input_string += '&END\n'
	return( input_string )

def symSplit_input( input_dict, meta_dict ):
	input_string = '&INPUT\n'
	input_string += 'orbSymFile="orbs.sym"\n'
	input_string += 'parentSyms="{}"\n'.format('-'.join(list(map(str,input_dict['symmParentStates']))))
	input_string += 'nParentStatesBySym="{}"\n'.format('-'.join(list(map(str,meta_dict['numParentIons']))))
	if input_dict['nameSourceStates']:
		input_string += 'sourceSyms="{}"\n'.format('-'.join(list(map(str,input_dict['symmSourceStates']))))
		input_string += 'nSourceStatesBySym="{}"\n'.format('-'.join(list(map(str,meta_dict['numSourceStates']))))
	else:
		input_string += 'sourceSyms=" "\n'
		input_string += 'nSourceStatesBySym=" "\n'
	ninac = 0
	if input_dict['inac']:
		ninac = sum(input_dict['inac'])
	input_string += 'inactive={}\n'.format(ninac)
	input_string += 'blockInfoFile="basis_block.info"\n'
	input_string += 'multiplicityAugmented={}\n'.format(input_dict['spinAugmentedStates'])
	input_string += 'multiplicityParent="{}"\n'.format('-'.join(list(map(str,input_dict['spinParentStates']))))
	if input_dict['forceC1']:
		input_string += 'forceC1=T\n'
	else:
		input_string += 'forceC1=F\n'
	input_string += '&END\n'
	return( input_string )

def GABS_interface_sym_input( input_dict, meta_dict ):
	norb = 0
	ninac = 0
	lMonocentric = input_dict['lMonocentric']
	kMonocentric = input_dict['kMonocentric']
	if input_dict['inac']:
		norb += sum(input_dict['inac']) 
		ninac = sum(input_dict['inac'])
	if input_dict['ras1']:
		norb += sum(input_dict['ras1']) 
	if input_dict['ras2']:
		norb += sum(input_dict['ras2']) 
	if input_dict['ras3']:
		norb += sum(input_dict['ras3']) 
	input_string = '&INPUT\n'
	###
	if len(lMonocentric) == 1 and  len(kMonocentric) == 1:
		input_string += 'lMax={}\n'.format(lMonocentric[0])
		input_string += 'kMax={}\n'.format(kMonocentric[0])
	else:
		input_string += 'lkMonocentricFile="lk.dat"\n'
		if not os.path.isfile( 'lk.dat' ):
			with open('lk.dat','w') as f:
				f.write('{}\n'.format(len(lMonocentric)))
				for l,k in zip(lMonocentric,kMonocentric):
					f.write('{} {}\n'.format(l,k))
	input_string += 'basisFile="bas"\n'
	input_string += 'orbitalFile="mpcg.bin"\n'
	input_string += 'blockInfoFile="basis_block.info"\n'
	input_string += 'multiplicity={}\n'.format(input_dict['spinAugmentedStates'])
	input_string += 'nLocOrb={}\n'.format(norb)
	input_string += 'nInactive={}\n'.format(ninac)
	input_string += 'nLocBas={}\n'.format(meta_dict['number_local_basis_functions'])
	if input_dict['forceC1']:
		input_string += 'forceC1=T\n'
	else:
		input_string += 'forceC1=F\n'
	if input_dict['multighost']:
		input_string += 'multiGhost=T\n'
	else:
		input_string += 'multiGhost=F\n'
	if input_dict['twoexp']:
		input_string += 'twoexp=T\n'
	else:
		input_string += 'twoexp=F\n'

	input_string += 'orbSymFile="orbs.sym"\n'
	input_string += '&END\n'
	return( input_string )

def computeEvals_input():
	input_string = '&INPUT\n'
	input_string += 'opFile="Ham.state.int"\n'
	input_string += 'ovFile="ov.state.int"\n'
	input_string += 'eValFile="ham.eVals"\n'
	input_string += '&END\n'
	return( input_string )

def sumOneIntTwoIntNuclear_input( meta_dict ):
	with  open( 'nuclearRepulsion', 'w') as f:
		f.write('{}'.format(meta_dict['nuclear_repulsion']))
	input_string = '&INPUT\n'
	input_string += 'oneElFile="ham1E.state.int"\n'
	input_string += 'twoElFile="ham2E.state.int"\n'
	input_string += 'ovFile="ov.state.int"\n'
	input_string += 'nucRepFile="nuclearRepulsion"\n'
	input_string += 'totHamFile="Ham.state.int"\n'	
	input_string += '&END\n'
	return( input_string )

def SAE_input( input_dict ):
	input_string = 'StoreDirectory={}\n'.format('.')
	input_string += 'BasisFile=BasisMixed\n'
	input_string += 'AbsorptionFile=AbsorptionPotential'
	return(input_string)

def Xchem_input( input_dict ):
	input_string = 'StorageDir={}\n'.format( '.' )
	xchem_input_kws = [
		'numBsDropBeginning',
		'numBsDropEnding',
		'conditionNumber',
		'conditionBsMethod',
		'conditionBsThreshold',
		'asymptoticCorrection',
		'loadLocStates',
		'scattConditionNumber',
		]
	for kw in xchem_input_kws:
		if kw in input_dict:
			input_string += '{}={}\n'.format(kw,input_dict[kw])
	return( input_string )

def BasisMixed_input( input_dict ):
	input_string = 'BasisKind=GaussianBSpline\n'
	input_string += 'RadialBasisFile=GaussianBSplineBasis\n'
	input_string += 'BasisDir=GABS\n'
	if input_dict['plotBasis']:
		input_string += 'PlotBasis=T\n'
		input_string += 'nPlot={}'.format(input_dict['nPlot'])
	else:
		input_string += 'PlotBasis=F'
	return( input_string )

def GaussianBSplineBasis_input( input_dict ):
	input_string = 'BSplineBasisFile=BSplineBasis\n'
	input_string += 'GaussianBasisFile=GaussianBasis\n'
	input_string += 'NBSplineSkip={}\n'.format( input_dict['nBSplineSkip'] )
	input_string += 'MatrixElementThreshold={}\n'.format( input_dict['matrixElementThreshold'] )
	input_string += 'GaussianBSplineOverlapThreshold={}\n'.format( input_dict['gaussianBSplineOverlapThreshold'] )
	input_string += 'GaussianProjectorThreshold={}'.format( input_dict['gaussianProjectorThreshold'] )
	return( input_string )

def BSplineBasis_input( input_dict ):
	input_string = 'Method=UniformGrid\n'
	input_string += 'NumberNodes={}\n'.format( input_dict['numberNodes'] )
	input_string += 'SplineOrder={}\n'.format( input_dict['splineOrder'] )
	input_string += 'Rmin={}\n'.format( input_dict['rmin'] ) 
	input_string += 'Rmax={}'.format( input_dict['rmax'] )	
	return( input_string )

def GaussianBasis_input( input_dict ) :
	input_string = 'NumberExponents={}\n'.format( input_dict['nexp'])
	lmax = max( [ l + 2*k for l,k in zip(input_dict['lMonocentric'],input_dict['kMonocentric']) ] ) 
	input_string += 'MaxAngularMomentum={}\n'.format( lmax )
	input_string += 'MinReciprocalConditionNumber=1.d-6\n'
	input_string += 'Generation=EvenTempered\n'
	input_string += 'Alpha0={}\n'.format( input_dict['alpha0'])
	input_string += 'Beta={}\n'.format( input_dict['beta'])
	input_string += 'ExponentsFile=GaussExponents'	
	return( input_string )

def AbsorptionPotential_input( input_dict ):
	if 'cap' in input_dict:
		if len(input_dict['cap']) == 0:
			cap_string = ''
		else:
			cap_string = capString( input_dict['cap'] )
	else:
		cap_string = ''
	return(cap_string)


def BasicMatrixElements_input( ):
	input_string = '-gif SAE'	
	return(input_string)

def ConvertQCI_input( input_dict, meta_dict , folder):	
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -qcdir {}'.format( str(folder) )
	input_string += ' -qcroot {}'.format( os.environ['XCHEM_DATA_DIR'] )
	return(input_string)

def BuildConditioningMatrices_input( input_dict, meta_dict ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -ketsym {}'.format(input_dict['brasym'])
	input_string += ' -esdir {}'.format('.')
	return( input_string )
 
def BuildSymmetricElectronicSpace_input( input_dict, meta_dict, op, axis=None, gauge=None ):
	with open('.ccf.{}'.format(meta_dict['uid']), 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf.{}'.format(meta_dict['uid'])
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -esdir {}'.format('.')
	input_string += ' -op {}'.format( op )
	if input_dict['force_ccm']: input_string += ' -force'
	if axis: input_string += ' -axis {}'.format( axis ) 
	if gauge: input_string += ' -gau {}'.format( gauge )
	return( input_string )

def BuildConditionedSymmetricElectronicSpace_input( input_dict, meta_dict, op, axis=None, gauge=None ):
	with open('.ccf.{}'.format(meta_dict['uid']), 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf.{}'.format(meta_dict['uid'])
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -ketsym {}'.format(input_dict['brasym'])
	input_string += ' -esdir {}'.format('.')
	input_string += ' -op {}'.format( op ) 
	if input_dict['force_ccm']: input_string += ' -force'
	if axis: input_string += ' -axis {}'.format( axis ) 
	if gauge: input_string += ' -gau {}'.format( gauge )
	return( input_string )

def DiagonalizeHamiltonian_input( input_dict, meta_dict, DipVel=None, DipLen=None, x=None, y=None, z=None, CAP=None ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -cn {}'.format(input_dict['conditionNumber'])
	input_string += ' -ccfile .ccf'
	input_string += ' -esdir {}'.format('.')
	input_string += ' -sym {}'.format(input_dict['brasym'])
	if (DipLen):
		input_string += ' -l t'
	if (DipVel):
		input_string += ' -v t'
	if (CAP):
		input_string += ' -cap t'
	if (x):
		input_string += ' -x t'
	if (y):
		input_string += ' -y t'
	if (z):
		input_string += ' -z t'
	return( input_string )	

def DiagonalizeOverlap_input( input_dict, meta_dict, ion = None ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	if (input_dict['cond']): input_string += ' -cond'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -esdir {}'.format('.')
	input_string += ' -sym {}'.format(input_dict['brasym'])
	if ion:
		input_string += ' -ion {}A.{} '.format(spin_PI(input_dict, meta_dict, ion), ion)
	return( input_string )	

def TransformOperators_input( input_dict, meta_dict, op, nPIs, gauge = None, axes = None ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	if (input_dict['cond']): input_string += ' -cond'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -cn {}'.format(input_dict['conditionNumber'])
	input_string += ' -esdir {}'.format('.')
	input_string += ' -sym {}'.format(input_dict['brasym'])
	input_string += ' -boxi {}'.format(input_dict['boxi'])
	input_string += ' -npi {}'.format(nPIs)
	input_string += ' -op {}'.format(op)
	if axes: input_string += ' -axis {}'.format( axes ) 
	if gauge: input_string += ' -gau {}'.format( gauge )
	return( input_string )	


def TransformMatBlocks_input( input_dict, meta_dict, op, braion = None, ketion = None, gauge = None, axes = None ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	if (input_dict['cond']): input_string += ' -cond'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -cn {}'.format(input_dict['conditionNumber'])
	input_string += ' -esdir {}'.format('.')
	input_string += ' -sym {}'.format(input_dict['brasym'])
	input_string += ' -op {}'.format(op)
	if braion: input_string += ' -braion {}A.{}'.format(spin_PI(input_dict, meta_dict, braion), braion)
	if ketion: input_string += ' -ketion {}A.{}'.format(spin_PI(input_dict, meta_dict, ketion), ketion)
	if axes: input_string += ' -axis {}'.format( axes ) 
	if gauge: input_string += ' -gau {}'.format( gauge )
	return( input_string )	

def ComputeScatteringStatesll_input( input_dict,meta_dict, concatenate = False ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict,meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -sym {}'.format(input_dict['brasym'])
	if input_dict['ne']: input_string += ' -ne {}'.format(input_dict['ne'])
	if input_dict['emin']!=None: input_string += ' -emin {}'.format(input_dict['emin'])
	if input_dict['emax']!=None: input_string += ' -emax {}'.format(input_dict['emax'])
	if input_dict['mode']: input_string += ' -mode {}'.format(input_dict['mode'])
	if input_dict['dEmax']: input_string += ' -demax {}'.format(input_dict['dEmax'])
	if input_dict['thrmin']: input_string += ' -thrmin {}'.format(input_dict['thrmin'])
	if input_dict['thrmax']: input_string += ' -thrmax {}'.format(input_dict['thrmax'])
	if input_dict['scatLabel']!=None: input_string += ' -scatlabel {}'.format(input_dict['scatLabel'][0])
	if input_dict['pqnmin']: input_string += ' -pqnmin {}'.format(input_dict['pqnmin'])
	if input_dict['pqnmax']: input_string += ' -pqnmax {}'.format(input_dict['pqnmax'])
	if input_dict['dpqnmax']: input_string += ' -dpqnmax {}'.format(input_dict['dpqnmax'])
	if input_dict['dPhmax']: input_string += ' -dphmax {}'.format(input_dict['dPhmax'])
	if input_dict['verbous']: input_string += ' -verbous'
	if input_dict['overwrite']: input_string += ' --overwrite'
	return( input_string )	

def ComputeMultichannelScatteringStates_input( input_dict,meta_dict, concatenate = False ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict,meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -esdir {}'.format('.')
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -ne {}'.format(input_dict['ne'])
	input_string += ' -emin {}'.format(input_dict['emin'])
	input_string += ' -emax {}'.format(input_dict['emax'])
	input_string += ' -cm {}'.format(input_dict['cm'])
	if input_dict['cond']:
		input_string += ' -cond'
	if concatenate:
		input_string += ' -concatenate'
	return( input_string )	

def ComputeDipoleTransitionAmplitudes_input( input_dict, meta_dict, axis, gauge, concatenate = False ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -axis {}'.format( axis ) 
	input_string += ' -gau {}'.format( gauge )
	input_string += ' -esdir {}'.format('.')
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -braemin {}'.format( input_dict['emin'] )
	input_string += ' -braemax {}'.format( input_dict['emax'] )
	input_string += ' -ketemin {}'.format( input_dict['emin'] )
	input_string += ' -ketemax {}'.format( input_dict['emax'] )
	input_string += ' -boxi {}'.format( abs(int(input_dict['boxi'])) )
	input_string += ' -trans {}'.format( input_dict['trans'] )
	input_string += ' -cm {}'.format(input_dict['cm'])
#	if input_dict['sinasymp']:
#		input_string += ' -sinasymp'
#	if input_dict['cond']:
#		input_string += ' -cond'
	if concatenate:
		input_string += ' -concatenate'
	return(input_string)

def ComputePhotoelectronSpectrumDTA_input( input_dict, meta_dict, axis, gauge):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	if input_dict['scatLabel']!=None: input_string += ' -scatlabel {}'.format(input_dict['scatLabel'])
	input_string += ' -ccfile .ccf'
	input_string += ' -axis {}'.format( axis )
	input_string += ' -gau {}'.format( gauge )
	input_string += ' -sym {}'.format(input_dict['brasym'])
	input_string += ' -boxi {}'.format( int(input_dict['boxi']) )
	input_string += ' -od ./DipoleTransitions/'
	return( input_string )

def ComputePhotoelectronSpectrum_input( input_dict, meta_dict, axis, gauge, propfile = False ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	if input_dict['scatLabel']!=None: input_string += ' -scatlabel {}'.format(input_dict['scatLabel'])
	input_string += ' -ccfile .ccf'
	input_string += ' -axis {}'.format( axis )
	if propfile:
	 	input_string += ' -psi {}'.format(input_dict['propfile'])
 		input_string += ' -od ./AngularDistribution/'
	else:
		input_string += ' -gau {}'.format( gauge )
		input_string += ' -sym {}'.format(input_dict['brasym'])
		input_string += ' -boxi {}'.format( int(input_dict['boxi']) )
		input_string += ' -od ./DipoleTransitions/'
	return( input_string )

def ComputeBetaParameter_input( input_dict, meta_dict, gauge):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	if input_dict['scatLabel']!=None: input_string += ' -scatlabel {}'.format(input_dict['scatLabel'])
	input_string += ' -ccfile .ccf'
	input_string += ' -gau {}'.format( gauge )
	input_string += ' -sym {}'.format(input_dict['brasym'])
	input_string += ' -boxi {}'.format( int(input_dict['boxi']) )
	input_string += ' -od ./DipoleTransitions/'
	return( input_string )

def ComputeConConDipoleTransitions_input( input_dict, meta_dict, axis, gauge ):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = ' -pif Xchem'
	input_string += ' -gif SAE'	
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	if (len(input_dict['scatLabel'][:]) > 1):
		buf=""
		for i in range(len(input_dict['scatLabel'][:])-1):
			buf=buf+input_dict['scatLabel'][i]+':'
		buf=buf+input_dict['scatLabel'][len(input_dict['scatLabel'][:])-1]
		input_string += ' -scatlabellist {}'.format( buf )
	else:
		input_string += ' -scatlabellist {}'.format( input_dict['scatLabel'][0] )
	nPIs = 0
	for i in range(len(meta_dict['numParentIons'])):
		nPIs += meta_dict['numParentIons'][i]
	input_string += ' -npi {}'.format( nPIs ) 
	if input_dict['cond']:
		input_string += ' -cond'
	input_string += ' -ccfile .ccf'
	input_string += ' -op Dipole'
	input_string += ' -axis {}'.format( axis ) 
	input_string += ' -gau {}'.format( gauge )
	input_string += ' -sym {}'.format(input_dict['brasym'])
	input_string += ' -od ./DipoleTransitions/'
	return( input_string )

def PrepareMatricesForTDSE_input( input_dict, meta_dict, DipLen = None, DipVel = None, x = None, y = None, z = None):
	with open('.ccf', 'w') as ccf:
		ccf_string  = create_CCF_from_CCChannels( input_dict, meta_dict )	
		ccf.write(ccf_string)
	input_string = '-gif SAE'
	input_string += ' -pif Xchem'
	input_string += ' -mult {}'.format( input_dict['spinAugmentedStates'] )
	input_string += ' -ccfile .ccf'
	input_string += ' -esdir {}'.format('.')
	input_string += ' -brasym {}'.format(input_dict['brasym'])
	input_string += ' -ketsym {}'.format(input_dict['brasym'])
	if input_dict['cond']:
		input_string += ' -cond'
	if (DipLen):
		input_string += ' -l t'
	if (DipVel):
		input_string += ' -v t'
	if (x):
		input_string += ' -x t'
	if (y):
		input_string += ' -y t'
	if (z):
		input_string += ' -z t'
	return( input_string )	

def create_CCF_from_CCChannels( input_dict, meta_dict ):
	ccf_string = 'Group = C1\n'
	if meta_dict['PIcharge'] is None:
		ccf_string += 'Charge = {}\n'.format( meta_dict['total_nuclear_charge'] - nelectrons(input_dict) )
	else:
		ccf_string += 'Charge = {}\n'.format( meta_dict['PIcharge'] )
	#ccf_string += 'Charge = {}\n'.format( meta_dict['total_nuclear_charge'] - nelectrons(input_dict) )
	ccf_string += 'Lmax = {}\n'.format( Lmax(input_dict) )
	ccf_string += 'Nelectrons = {}\n'.format( nelectrons(input_dict) + 1 )
	ccf_string += '[{}A]{{\n'.format( input_dict['spinAugmentedStates'] )
	for channel_raw in input_dict['closeCouplingChannels'].split(';'):
		channel = re.split( r'[\(\)]', channel_raw.strip() )
		PI_id = int(channel[0])
		partial_channels = channel[1].split(',')
		ccf_string += '{}A.{} ('.format(spin_PI(input_dict, meta_dict, PI_id), PI_id)
		npartial_channels = len(partial_channels)
		for i,partial_channel in enumerate( partial_channels ):
			l,m = list(map(int,partial_channel.split()))
			if i + 1  == npartial_channels:
				ccf_string += '{} {}'.format(l,m)
			else:
				ccf_string += '{} {},'.format(l,m)
		ccf_string += ')\n'
	ccf_string += '}\n'
	return(ccf_string)

def capString( string ) :
	string = string.split(',')
	cap_string = ''
	for cap in string:
		cap = list(map(float,cap.split()))
		if len(cap) == 3:
			cap_string += ' '.join(list(map(str,cap[0:2]))) + ' 0.0 ' + str(cap[-1])  + ' 1\n'
		elif len(cap) == 4:
			cap_string += ' '.join(list(map(str,cap))) + ' 1\n'
		else:
			raise XchemError('Could not parse input provided for kewyword cap')
	return(cap_string)

def Lmax( input_dict ):
	lMonocentric = input_dict['lMonocentric']
	kMonocentric = input_dict['kMonocentric']
	if len(lMonocentric) == 1 and  len(kMonocentric) == 1:
	 	L = lMonocentric[0] + 2* kMonocentric[0]
	else:	
		L = -1
		for l,k in zip(lMonocentric,kMonocentric):
			if l + 2*k  > L:
				L  = l + 2*k
	return(L)

def nelectrons( input_dict ):
	N = input_dict['nact'][0]
	if input_dict['inac']:
		N += 2 * sum(input_dict['inac'])
	return(N)	

def spin_PI( input_dict, meta_dict, index ):
	l = []
	for e,i in enumerate(meta_dict['numParentIons']):
		l = l + [input_dict['spinParentStates'][e]] * i
	return( l[index-1] )

def num_orb_active( input_dict ):
	norb = 0
	norb += sum(input_dict['ras2'])
	if input_dict['ras1']:
		norb += sum(input_dict['ras1'])
	if input_dict['ras3']:
		norb += sum(input_dict['ras3'])
	return(norb)

def num_orb_occ( input_dict ):
	norb = 0
	norb += sum(input_dict['ras2'])
	if input_dict['inac']:
		norb += sum(input_dict['inac'])
	if input_dict['ras1']:
		norb += sum(input_dict['ras1'])
	if input_dict['ras3']:
		norb += sum(input_dict['ras3'])
	return(norb)
