#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

#from h5_extract import H5Extractor
import h5py
import numpy as np
import pyparsing as pp
from xchem_error import *
from collections import defaultdict, Counter
from basis_set import BasisSet
from civector import CIVector
import itertools

def sfs_energies( h5_file_name, output_file_name ):
	with h5py.File( h5_file_name, 'r') as h5:
		data = h5['SFS_ENERGIES']
		nroots = h5['SFS_ENERGIES'].shape[0]
		for iroot in range(nroots):
			file_name = '{}_{:03d}.out'.format(output_file_name, iroot + 1)
			with open(file_name, 'w') as f:
				f.write('{:.20e}'.format(data[iroot]))

def load_ci_vector_to_h5( h5_file_in, ci_file, h5_file_out):
	ci = CIVector(ci_file)
	with h5py.File( h5_file_in, 'r' ) as h5_in, h5py.File(h5_file_out, 'w') as h5_out:
		for key,value in h5_in.attrs.items():
			h5_out.attrs[key] = value
		for key in h5_in:
			h5_in.copy(key, h5_out)
		del h5_out['ROOT_ENERGIES']
		del h5_out['CI_VECTORS']
		h5_out['CI_VECTORS'] = ci.ci_vectors

def total_nuclear_charge( h5_file_name ):
	with h5py.File( h5_file_name, 'r') as h5:	
		if 'DESYM_CENTER_CHARGES' in h5:
			charge = sum(h5['DESYM_CENTER_CHARGES'][:])
		elif 'CENTER_CHARGES' in h5:
			charge = sum(h5['CENTER_CHARGES'][:])
		else:
			raise XchemError('Could not extract total nuclear charge from file {}'.format(h5_file_name))
	return(charge)

def ci_vectors( h5_file_name, output_file_name=None ):
	with h5py.File( h5_file_name, 'r' ) as h5:
		data = h5['CI_VECTORS']
		if output_file_name:
			nRoot = data.shape[0]
			nCSF = data.shape[1]
			with open(output_file_name, 'w') as f:
				f.write('{:05d} {:010d}\n'.format(nRoot, nCSF))
				for iRoot in range(nRoot):
					for iCSF in range(nCSF):
						f.write('{:.20e} '.format(data[iRoot,iCSF]))
					f.write('\n')
		else:
			return(data)

def number_basis_functions( h5_file_name ):
	with h5py.File( h5_file_name, 'r')  as h5:
		if 'DESYM_BASIS_FUNCTION_IDS' in h5:
			num_bas_fcts = h5['DESYM_BASIS_FUNCTION_IDS'][:].shape[0]
		elif 'BASIS_FUNCTION_IDS' in h5:
			num_bas_fcts = h5['BASIS_FUNCTION_IDS'][:].shape[0]
		else:
			raise XchemError('Could not extract number of basis functions from file {}'.format(h5_file_name))
		return num_bas_fcts

def permanent_dipole( h5_file_name ):
	with h5py.File( h5_file_name, 'r' ) as h5:
		q = h5['CENTER_CHARGES'][:].reshape((-1,1))
		r = h5['CENTER_COORDINATES'][:]
		dipole = np.sum( q * r, axis = 0 ) 
		return( dipole.tolist() ) 

def nuclear_repulsion( h5_file_name ):
	with h5py.File( h5_file_name, 'r' ) as h5:
		q = h5['CENTER_CHARGES'][:]
		r = h5['CENTER_COORDINATES'][:]
		nuc_rep = 0.0
		for iatom, jatom in itertools.combinations( range(q.size), r=2 ): 
			nuc_rep = nuc_rep + q[iatom]*q[jatom] / np.sqrt( np.sum( np.square( r[iatom,:] - r[jatom,:] )))
		kk=h5.attrs['POTNUC']
		if abs(kk-nuc_rep)>=1e-5:
			print("XCHEM_WARNING Nuclear repulsion are different")	
			print("XCHEM_WARNING Calculated nuclear repulsion ",nuc_rep)
			print("XCHEM_WARNING In the h5 file ",kk)
			print("Using ",kk)
		nuc_rep=kk
		return( nuc_rep )

# Helper function to extrcat label from string contained in h5.
def _getLabel( label_string ):
	atomLabel = pp.Word(pp.alphanums)
	generator = pp.Word(pp.alphanums)
	whitespace = pp.Word(' ')
	label = atomLabel.setResultsName('atom') +  pp.Optional( pp.Suppress(pp.Literal(':')) + generator.setResultsName('generator') )
	r = label.parseString( label_string )
	if 'generator' in r:
		return (r['atom'],r['generator'])
	else:
		return (r['atom'],None)

def lastshell( file_name ):
	with h5py.File( file_name, "r" ) as h5:
		for i,label in enumerate(h5['CENTER_LABELS'][:]):
			atom,_ = _getLabel(label.decode('UTF-8')) 
			if 'X' in atom:
				first_ghost = i+1
				break
		kk1=h5['BASIS_FUNCTION_IDS'][:,0] < first_ghost
#               print(kk1)
		kk=[]
		for i,j in enumerate(kk1):
			if j:
				kk.append(i)
#               print(kk)
		kk2=h5['BASIS_FUNCTION_IDS'][kk,2]
#               print(kk2)
		kk3=np.ediff1d(kk2)
#               print(kk3)
		kk4=np.sum(kk3 != 0)
#               print(kk4)
		lastshell = np.sum(kk4)+1

	#	lastshell = np.sum(np.ediff1d(h5['BASIS_FUNCTION_IDS'][h5['BASIS_FUNCTION_IDS'][:,0] < first_ghost,2]) != 0) + 1 
	return(lastshell.item())

def gateway( file_name ):
	# Open h5 File
	with h5py.File( file_name , 'r' )  as h5:
		# IDs specefying for each basis functions: [atom_id, l, contraction_id].
		primitive_ids = h5['PRIMITIVE_IDS'][:]
		# Exponents and contraction coefficients for all basis functions.
		# Ordered as [[exp_1(atom_1,l=0),coff_{1,1}(atom_1,l=0)], 
		#             [exp_2(atom_1,l=0),coff_{1,2}(atom_1,l=0)],
		#								...
		#             [exp_1(atom_1,l=0),coff_{2,1}(atom_1,l=0)],
		#								...
		#             [exp_1(atom_1,l=1),coff_{1,1}(atom_1,l=1)],
		#								...
		#             [exp_1(atom_2,l=0),coff_{1,1}(atom_2,l=0)],
		#								...
		#             [exp_nexp(atom_natom,l=lmax),coff_{ncoef,nexp}(atom_natom,l=lmax)]].
		primitives = h5['PRIMITIVES'][:]
		# Charges of the atoms.
		if 'DESYM_CENTER_CHARGES' in h5:
			desym_center_charges = h5['DESYM_CENTER_CHARGES'][:]
		elif 'CENTER_CHARGES' in h5:
			desym_center_charges = h5['CENTER_CHARGES'][:]
		else:
			raise XchemError('Could not extract center charges from file {}'.format(file_name))
		# Molecular geometry.
		if 'DESYM_CENTER_COORDINATES' in h5:
			desym_center_coordinates = h5['DESYM_CENTER_COORDINATES'][:]
		elif 'CENTER_COORDINATES' in h5:
			desym_center_coordinates = h5['CENTER_COORDINATES'][:]
		else:
			raise XchemError('Could not extract center coordinates from file {}'.format(file_name))
		# Labels (inlcuding information about how the atom related to the symmetry of the system).
		if 'DESYM_CENTER_LABELS' in h5:
			desym_center_labels = h5['DESYM_CENTER_LABELS'][:]
		elif 'CENTER_LABELS' in h5:
			 desym_center_labels = h5['CENTER_LABELS'][:]
		else:
			raise XchemError('Could not extract center labels from file {}'.format(file_name))
		# Basis function ids. Include information about whether or not the basis for a particular atom is
		# cartesian or spherical.
		if 'DESYM_BASIS_FUNCTION_IDS' in h5:
			desym_basis_function_ids = h5['DESYM_BASIS_FUNCTION_IDS'][:]
		elif 'BASIS_FUNCTION_IDS' in h5:
			desym_basis_function_ids = h5['BASIS_FUNCTION_IDS'][:]
		else:
			raise XchemError('Could not extract basis function ids from file {}'.format(file_name))

	# Extract atom labels.
	atoms = []
	for l in desym_center_labels:
		atom,_ = _getLabel(l.decode('UTF-8')) 
		atoms.append(atom)

	# Dictionary to keep track of which atoms have already been 
	# accounted for.
	unique_atoms = {}
	# List for basis sets. Will be the output.
	basis_sets = []
	start_ind_bas = 0 
	for i,atom in enumerate(atoms):
		if atom in unique_atoms: 
			# If atom has already been seen skip to next.
			start_ind_bas += unique_atoms[atom]
			continue
		#
		# Initialize new basis set.
		basis_set = BasisSet(atom.strip('0123456789'))

		# Identify maximum l for current atom.
		l_max = np.amax( primitive_ids[ primitive_ids[:,0] == i + 1, 1] )
		# Idenfify which l are expressed in sphericals in which in cartesians
		ls = desym_basis_function_ids[ desym_basis_function_ids[:,0] == i+1 ,2]
		for l in range(l_max+1):
			if l in ls:
				basis_set.spherical.append( BasisSet.AOLabels(l) )
			elif -l in ls:
				basis_set.cartesian.append( BasisSet.AOLabels(abs(l)) )
			else:
				raise XchemError('Error in establishing if {} functions for atom {} are cartesian or spherical'.format(l,atom))
		# Counter the number of exponent-coefficient-pairs (=nexp * ncoef) for each l.
		l_counter = Counter( primitive_ids[ primitive_ids[:,0] == i + 1, 1]  )
		# Extract the contractions id for current atom's basis functions.
		bas = primitive_ids[ primitive_ids[:,0] == i + 1, 2]
		#
		start_ind_atom, end_ind_atom, nBas = 0, 0, 0
		# For each l, find the number of exponents/contractions
		for l in range(l_max + 1 ):
			end_ind_atom += l_counter[l]
			nBas_l = np.amax( bas[start_ind_atom:end_ind_atom] )
			nExp = (end_ind_atom - start_ind_atom) / nBas_l
			start_ind_atom = end_ind_atom
			# Number of exponent-coefficient-pairs must be  divisible by 
			# number of contractions (and yields the number of exponents).
			if ( int(nExp) - nExp != 0 ):
				raise XchemBasisError
			else:
				nExp = int(nExp)
			# Append information for current l to current atom's basis
			basis_set.number_exponents.append(nExp)
			basis_set.number_contractions.append(nBas_l)
			basis_set.exponents.append( primitives[ start_ind_bas: start_ind_bas + nExp , 0 ] )
			basis_set.contractions.append( np.transpose( primitives[start_ind_bas: start_ind_bas + nExp * nBas_l, 1 ].reshape((-1,nExp))))
			start_ind_bas +=  nBas_l * nExp
			nBas += nBas_l * nExp
		# Add atom to list of seen atoms.
		unique_atoms[atom] = nBas
		# Set charge and geometry information.
		basis_set.charge = desym_center_charges[i]
		temp = desym_center_coordinates [ [ i for i in range(len(atoms)) if atoms[i] == atom],:]
		basis_set.natom = temp.shape[0]
		for iatom in range(basis_set.natom):
			basis_set.geometry.append( temp[iatom,:] )
			basis_set.atom_labels.append( atom + str(iatom) )
		basis_sets.append(basis_set)
	return( basis_sets )
