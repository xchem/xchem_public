#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

# Methods to parse input file and check for constistency
import pyparsing as pp

def parse( input_file ):
	# acceptable module names:
	module_names=[
		'env',
		'integrals',
		'rdm',
		'qci',
		'scatt',
		'orbitals',
		'basis',
		'ccm',
		'bsplines',
		'boxstates',
		'scatteringstates',
		'dta',
		'mfpad',
		'beta',
		'preptdse',
		'plot',
		'exit',
		'nowait'
		]

	# Define grammar for input Files.
	pp.ParserElement.setDefaultWhitespaceChars(' \t')
	white_space = pp.Suppress( pp.ZeroOrMore( pp.White(' ') ) )
	trailing_space = pp.Suppress( pp.restOfLine ) + pp.Suppress( pp.lineEnd )

	# Set up lines that may be ignored (commented or blank)
	blank_line = white_space + pp.lineEnd 
	comment_line = pp.Optional( white_space ) + ( pp.Literal("#") | pp.Literal("*") | pp.Literal("//") ) + trailing_space
	ignored_lines = pp.Suppress( pp.ZeroOrMore( blank_line | comment_line )  )

	# Keywords may contain numbers, letters and underscores
	word = pp.Word(pp.alphanums + '_')
	# Fields may contain numbers, letters and special characters: -._/
	field = pp.Word(pp.alphanums + '-._/,;()="' )
	# Fields are separated by a space
	field_separator = pp.Literal(' ')

	# Modules are as in molcas given as '&<MODULENAME>'. 
	# The <MODULENAME> may contain letters
	xchem_module_name = white_space + pp.Suppress( pp.Literal('&') ) + word + trailing_space

	# Set up a key-value pair as <KEY> = <VALUE[0]>, [ <VALUE[1]>, <VALUE[2]>, ... ]
	xchem_module_parameter_key = white_space + word + white_space
	xchem_module_parameter_value = white_space + pp.Group( pp.OneOrMore( field + pp.Suppress( pp.Optional( field_separator ) ) ) ) + trailing_space
	xchem_module_paramter = pp.Group( xchem_module_parameter_key.setResultsName('key') + \
							pp.Suppress( pp.Literal('=') ) + \
							xchem_module_parameter_value.setResultsName('value') + \
							ignored_lines )

	# Set up a module as a <MODULE NAME> \n <KEY-VALUE[0]> \n ...
	xchem_module = pp.Group( xchem_module_name.setResultsName('module') + ignored_lines + pp.Group( pp.ZeroOrMore( xchem_module_paramter ) ).setResultsName('parameters') )

	# Set up input as multiple modules
	xchem_input = ignored_lines + pp.OneOrMore( xchem_module )

	# Open and read input file.
	file_handler = open( input_file ) 
	input_string = file_handler.read()

	# Use grammer to parse input file.
	input_parsed = xchem_input.parseString( input_string )
	
	# Transform input data into a dictionaries containing the parameters and values for 
	# each requested model.
	input_list = []
	for input_element in input_parsed:
		module_name = input_element['module'][0].lower()
		if (module_name not in module_names):
			raise ValueError('Unknown xchem module: ' + module_name )
		parameter_dict = {}
		for parameter in  input_element['parameters']:
			parameter_dict[parameter['key'][0]] = parameter['value'][0].asList()
		input_list.append((module_name, parameter_dict))

	# Print and return input data.
	print('Input provided:')
	print('--------------------')
	for module, parameters in input_list:
		print('&' + module)
		for key, value in parameters.items():
			print('    ' + key + '=' + ' '.join(value) )
	print('--------------------')
	return input_list
