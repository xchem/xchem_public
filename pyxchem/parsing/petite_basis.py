#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class PetiteBasisParser:
	def __init__(self):
		self._petite = False
		self._petite_finished = False
		self._result = ""
	
	def __call__(self, line):
		if self._petite_finished: return
		if len(line)==0: return 
		if self._petite: 
			if 'Basis set specifications' in line:
				self._petite_finished = True
				return
			self._result += line + '\n'
		if 'Basis Label        Type   Center' in line:
			self._petite = True

	@property
	def result(self):
		return self._result

	def write(self, file_name):
		handle = open( file_name, mode = 'w' )
		for line in self._result:
			handle.write(line)
		handle.close()
