#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

def symmetry_parse( string ):
	lst = [ c.strip().lower() for c in string.split('\n') ]
	lst = list(filter(('').__ne__, lst ))
	sym=""
	for key in lst:
		if 'symm' in key:
			sym=lst[lst.index(key)+1]
		if 'group' in key:
			sym=lst[lst.index(key)+1]
	if sym != "c1" and sym != "e" and sym != "nosym" and sym != "":
		return(sym)
	else:
		return(None)

class SymmetryParser:
	def __init__(self):
		self.result = {'generators':None}
	def __call__(self, line):
		if 'Character Table for' in line:
			self.result['group'] = line.split()[-1] 
		if 'Symmetry species   ' in line:
			self.result['irreps'] = line.split()[2:]
		if 'Found SYMMETRY generators:' in line:
			self.result['generators']= ' '.join(line.split()[3:])
