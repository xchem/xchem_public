#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class GugaParser:
	def __init__(self,file_name):
		self._file_name = file_name
		self._started = False
		self._lastcsf = False
		self._stop = False
		self._ndet = 0
		self._icsf = 0
		self._dets = []
		self._conf = None
		self._det_symbols={"2":"#","a":"+","b":"-","0":"."}
		self._translate_dict = {}
		for c in '|sqrt()/':
			self._translate_dict[ord(c)] = ' '

	def __call__(self, line):
		if self._stop:
			pass
		else:
			line = line.strip()
			if line: 
				if self._started:
					if 'sqrt' in line:
						self._process_det(line)
					else:
						if self._conf:
							self._print_csf_in_dets()
						if self._lastcsf:
							self._stop = True
							self.guga_handel.close()
						else:
							self._process_csf(line)
				elif 'conf/sym' in line:
					self._started = True
					self.guga_handel = open(self._file_name,'w')
				elif 'Number of CSFs' in line:
					self._ncsf = int(line.split()[-1])

	def _process_csf(self, line):
		csf = line.split()
		self._icsf = int(csf[0])
		self._conf = ''.join(csf[1:-2])
		self._ndet = 0
		self._dets = []
		if self._icsf == self._ncsf:
			self._lastcsf = True 

	def _process_det(self, line):
		self._ndet += 1
		det = line.translate(self._translate_dict).split()
		det[3] = self._replace_det_symbols(det[3])
		self._dets.append( '{}{} {} {} '.format(det[0],det[1],det[2],det[3]))

	def _print_csf_in_dets(self):
		if self._icsf == 1:
			norb = len(self._conf)
			self.guga_handel.write('{} {}\n'.format(self._ncsf, norb ))
		self.guga_handel.write('{} {} {}\n'.format(self._conf, self._ndet, self._icsf))
		self.guga_handel.write(' '.join(self._dets) + '\n')
	
	def _replace_det_symbols(self, det):
		for k,v in self._det_symbols.items():
			det = det.replace(k,v)
		return(det)
