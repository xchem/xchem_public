#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class MultipoleParser:
	def __init__(self, num_parent_ions):
		self._counter = 0
		self._nPI = num_parent_ions
		self._result = ''

	def __call__(self, line):
		if self._counter % (self._nPI+1) == 0:
			component = component_list[int(self._counter/(self._nPI + 1))+1]
			self._result += '{}\n'.format(component)
		else:
			self._result += '{}\n'.format(line)
		self._counter +=  1

	def write(self, file_name):
		with open( file_name, mode = 'w' ) as f:
			f.write(self._result)

component_list={}
component_list[1]="X"
component_list[2]="Y"
component_list[3]="Z"
component_list[4]="XX"
component_list[5]="XY"
component_list[6]="XZ"
component_list[7]="YY"
component_list[8]="YZ"
component_list[9]="ZZ"
component_list[10]="XXX"
component_list[11]="XXY"
component_list[12]="XXZ"
component_list[13]="XYY"
component_list[14]="XYZ"
component_list[15]="XZZ"
component_list[16]="YYY"
component_list[17]="YYZ"
component_list[18]="YZZ"
component_list[19]="ZZZ"
component_list[20]="XXXX"
component_list[21]="XXXY"
component_list[22]="XXXZ"
component_list[23]="XXYY"
component_list[24]="XXYZ"
component_list[25]="XXZZ"
component_list[26]="XYYY"
component_list[27]="XYYZ"
component_list[28]="XYZZ"
component_list[29]="XZZZ"
component_list[30]="YYYY"
component_list[31]="YYYZ"
component_list[32]="YYZZ"
component_list[33]="YZZZ"
component_list[34]="ZZZZ"
component_list[35]="XXXXX"
component_list[36]="XXXXY"
component_list[37]="XXXXZ"
component_list[38]="XXXYY"
component_list[39]="XXXYZ"
component_list[40]="XXXZZ"
component_list[41]="XXYYY"
component_list[42]="XXYYZ"
component_list[43]="XXYZZ"
component_list[44]="XXZZZ"
component_list[45]="XYYYY"
component_list[46]="XYYYZ"
component_list[47]="XYYZZ"
component_list[48]="XYZZZ"
component_list[49]="XZZZZ"
component_list[50]="YYYYY"
component_list[51]="YYYYZ"
component_list[52]="YYYZZ"
component_list[53]="YYZZZ"
component_list[54]="YZZZZ"
component_list[55]="ZZZZZ"
component_list[56]="XXXXXX"
component_list[57]="XXXXXY"
component_list[58]="XXXXXZ"
component_list[59]="XXXXYY"
component_list[60]="XXXXYZ"
component_list[61]="XXXXZZ"
component_list[62]="XXXYYY"
component_list[63]="XXXYYZ"
component_list[64]="XXXYZZ"
component_list[65]="XXXZZZ"
component_list[66]="XXYYYY"
component_list[67]="XXYYYZ"
component_list[68]="XXYYZZ"
component_list[69]="XXYZZZ"
component_list[70]="XXZZZZ"
component_list[71]="XYYYYY"
component_list[72]="XYYYYZ"
component_list[73]="XYYYZZ"
component_list[74]="XYYZZZ"
component_list[75]="XYZZZZ"
component_list[76]="XZZZZZ"
component_list[77]="YYYYYY"
component_list[78]="YYYYYZ"
component_list[79]="YYYYZZ"
component_list[80]="YYYZZZ"
component_list[81]="YYZZZZ"
component_list[82]="YZZZZZ"
component_list[83]="ZZZZZZ"
