#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import numpy as np
class CIVector:
	def __init__(self, file_name = None):
		if file_name is not None:
			self.readFromFile( file_name )

	def readFromFile(self, file_name):
		with open(file_name, "r") as f:
			icsf = 0
			iroot = 0
			for i,line in enumerate(f):
				if i==0:
					self.nroot, self.ncsf = map(int,line.split())
					self.ci_vectors = np.zeros((self.nroot, self.ncsf))
				else:
					ci_chunk = list(map(float,line.split()))	
					self.ci_vectors[iroot,icsf:icsf+len(ci_chunk)] = ci_chunk
					icsf += len(ci_chunk)
					if icsf == self.ncsf:
						icsf = 0
						iroot += 1
					if icsf > self.ncsf:
						pass
