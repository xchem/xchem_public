#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import subprocess, shlex
import os,sys
from glob import glob
from xchem_error import *
import osutils

class XchemManager:
	def __init__(self, input_cache = '.xchem_input'):
		if 'XCHEM_DEBUG' in os.environ:
			if os.environ['XCHEM_DEBUG'] == '1': 
				self.xchem = os.path.join(os.environ['XCHEM'], 'bin_d')
			else:
				self.xchem = os.path.join(os.environ['XCHEM'], 'bin')
		else:
			self.xchem = os.path.join(os.environ['XCHEM'], 'bin')
		self._input_cache = input_cache
		self._piped_programs = ['CSF2Det.exe', 'Det2CSF.exe', 'augment.exe', 'CSFs2CSF.exe' ]


	@property
	def input_cache( self ):
		return(self._input_cache)
	
	@input_cache.setter
	def input_cache( self, value ):
		self._input_cache = value

	def run(self, program, xchem_file_input = None, xchem_cmd_line_input = None, parsefct = None, xchem_file_input_name = None, in_files = {}, wait = True ):
		sys.stdout.flush()
		if (xchem_file_input):
			if (xchem_file_input_name):
				if (type(xchem_file_input_name) == list ):
					for content, name in zip(xchem_file_input, xchem_file_input_name):
						handle = open( name, mode = 'w' )
						handle.write( content )
						handle.close()
				else:
					handle = open( xchem_file_input_name , mode = 'w' ) 
					handle.write(xchem_file_input)
					handle.close()
			else:	
				handle = open( self._input_cache , mode = 'w' ) 
				handle.write(xchem_file_input)
				handle.close()

		if os.path.isfile(os.path.join(self.xchem, program)):
			pass
		elif os.path.isfile(os.path.join(self.xchem, program + '.exe')):
			program += '.exe'
		else:
			raise XchemError('Binary for {} could not be found'.format(program))

		for directory in in_files:
			for pattern in in_files[directory]:
				osutils.lnregexls( directory, '.', pattern )

		if program in self._piped_programs:
			p0 = subprocess.Popen( ['cat',self._input_cache], stdout=subprocess.PIPE )  
			p = subprocess.Popen( [os.path.join(self.xchem, program)],
							       stdin = p0.stdout, stdout=subprocess.PIPE )
			for line in iter(p.stdout.readline, b''):
				preprocessed_line = line.decode('UTF-8').strip('\n')
				if os.environ['XCHEM_PRINT'] == '2' or 'XCHEM_WARNING' in preprocessed_line:
					print(preprocessed_line)	
			if wait:
				rc = p.wait() 
		else:
			cmd = os.path.join(self.xchem, program)
			if xchem_file_input:
				cmd += " {}".format(self._input_cache)
			if xchem_cmd_line_input:
				cmd += " {}".format(xchem_cmd_line_input)
			cmd = shlex.split(cmd)

			p = subprocess.Popen( cmd, stdout=subprocess.PIPE )
			for line in iter(p.stdout.readline, b''):
				preprocessed_line = line.decode('UTF-8').strip('\n')
				if os.environ['XCHEM_PRINT'] == '2' or 'XCHEM_WARNING' in preprocessed_line:
					print(preprocessed_line)
				if parsefct:
					parsefct(preprocessed_line.strip())
			if wait:
				rc = p.wait()
		if wait:
			if rc != 0:
				raise XchemError()
			return(rc)
		else:
			return(p)
			
	def task(self, program, xchem_file_input = None, xchem_cmd_line_input = None, parsefct = None, xchem_file_input_name = None, iterate_over=None, parallel = False):
		task_dict = {}
		task_dict['package'] = 'xchem'
		task_dict['program'] = program
		task_dict['xchem_file_input'] = xchem_file_input
		task_dict['xchem_cmd_line_input'] = xchem_cmd_line_input
		task_dict['parsefct'] = parsefct
		task_dict['xchem_file_input_name'] = xchem_file_input_name
		task_dict['iterate_over'] = iterate_over
		task_dict['parallel'] = parallel
		return(task_dict)
