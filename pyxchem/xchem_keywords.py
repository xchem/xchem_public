#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import XchemKeyword

#
gatewayFile = XchemKeyword( name = 'gatewayFile', optional = False, data_type = 'str', is_file = True)
description = '''File defining molecular geometry and the polycentric Gaussian basis sets. It should be prepared as if an input for Molcas' Gateway module.'''
gatewayFile.description = description	
#
gateway_desym = XchemKeyword( name = 'gateway_desym', optional = True, data_type = 'str', is_file = True, default = None)
description = '''File defining molecular geometry and the polycentric Gaussian basis sets. It should be prepared as if an input for Molcas' Gateway module with no symmetry and in expert mode.'''
gateway_desym.description = description	
#
lMonocentric = XchemKeyword( name = 'lMonocentric', optional = False, data_type = 'int', array = True)
description = '''The maximum value of l in the definition of the monocentric gaussians as: G_{\\alpha,klm}(x)=N*r^{l+2k}exp(-\\alpha * r^2)X_{l,m}(\\theta,\\phi)'''
lMonocentric.description = description
#
kMonocentric = XchemKeyword( name = 'kMonocentric', optional = False, data_type = 'int', array = True)
description = '''The maximum value of k in the definition of the monocentric gaussians as: G_{\\alpha,klm}(x)=N*r^{l+2k}exp(-\\alpha * r^2)X_{l,m}(\\theta,\\phi)'''
kMonocentric.description = description
#
multighost = XchemKeyword( name = 'multighost', optional = True, data_type = 'bool', default = False )
description = '''If True: Use different ghost atoms for monocentric gaussian basis functions of different l. This can only be use if the default paramters for alpha0, beta and nexp are used. If False: Use only one ghost atom for all l. The identity matrix will be used for the contraction matrix'''
multighost.description = description
#STARTS VICENT MODIFICATION
twoexp = XchemKeyword( name = 'twoexp', optional = True, data_type = 'bool', default = True )
description = '''If true: The calculation of the integrals is done by pairs of exponents of the monocentric basis.'''
twoexp.description = description
saveTwoElInt = XchemKeyword( name = 'saveTwoElInt', optional = True, data_type = 'bool', default = True )
description = '''If the twoexp keyword is added and the saveTwoElInt is False, the integrals are nor saved and the calculation is faster'''
saveTwoElInt.description = description
#ENDS VICENT MODIFICATION
#JGV
pqrs_temporal = XchemKeyword( name = 'pqrs_temporal', optional = True, data_type = 'bool', default = True )
description = '''Only makes sense with defer, if False, the pqrs integrals are created directly in integrals/tmp instead in the remote_dir directory'''
pqrs_temporal.description = description
## END
alpha0 = XchemKeyword( name = 'alpha0', optional = True, data_type = 'float', default = 0.01 )
description = '''Value to be used in the expression alpha_i = alpha0 * beta^{i-1} for the even tempered monocentric gaussian set. It is not recommended to change this other than for testing purposes.'''
alpha0.description = description
#
beta = XchemKeyword( name = 'beta', optional = True, data_type = 'float', default = 1.46 )
description = '''Value to be used in the expression alpha_i = alpha0 * beta^{i-1} for the even tempered monocentric gaussian set. It is not recommended to change this other than for testing purposes.'''
beta.description = description
#
nexp = XchemKeyword( name = 'nexp', optional = True, data_type = 'int', default = 22 )
description = '''Number of primitive monocentric gaussians to be used'''
nexp.description = description
#
linDepThr = XchemKeyword( name = 'linDepThr', optional = True, data_type = 'float', default = 1.0e-8 )
description = ''' Threshold to remove linear dependencies in the set of diffuse orbitals'''
linDepThr.description = description
#
defer = XchemKeyword( name = 'defer', optional = True, data_type = 'bool', default = False )
description = ''' Defer expensive part of computation to be submitted across multiple nodes. To submit execute pyxchem --submit <module>'''
defer.description = description
#
orbitalFile = XchemKeyword( name = 'orbitalFile' , data_type = 'str')
description = '''File containing orbitals to be used for generation of all parent ion states. These need to in Molcas' INPORB format'''
orbitalFile.description = description
#
inac = XchemKeyword( name = 'inac' , data_type = 'int', optional = True, array = True )
description='''Nr of inactive orbitals in each symmetry. An anaolgous keyword exists in Molcas Rasscf module.'''
inac.description = description
#
ras1 = XchemKeyword( name = 'ras1' , data_type = 'int', optional = True, array = True)
description='''Nr of ras1 orbitals in each symmetry.  An anaolgous keyword exists in Molcas Rasscf module.'''
ras1.description = description
#
ras2 = XchemKeyword( name = 'ras2' , data_type = 'int', array = True )
description='''Nr of ras2 orbitals in each symmetry.  An anaolgous keyword exists in Molcas Rasscf module.'''
ras2.description = description
#
ras3 = XchemKeyword( name = 'ras3' , data_type = 'int', optional = True, array = True )
description='''Nr of ras3 orbitals in each symmetry.  An anaolgous keyword exists in Molcas Rasscf module.'''
ras3.description = description
#
nact					= XchemKeyword( name = 'nact' , data_type = 'int', array = True )
description='''Expects either one or three integer numbers. If one is given it defines the number of electrons in the ras2 orbitals. If three are given they correspond to nr of electrons in ras2 orbitals, nr of holes in ras1 orbitals and nr of electrons in ras3 orbitals. An anaolgous keyword exists in Molcas Rasscf module.'''
nact.description = description
#
symmParentStates		= XchemKeyword( name = 'symmParentStates' , data_type = 'int', array = True )	
description='''The symmetries for which parent ions are to be included. The is similar to the symm keyword in Molcas Rasscf. However multiple (repeated) symmetries may be defined'''
symmParentStates.description = description
#
ciroParentStates		= XchemKeyword( name = 'ciroParentStates' , data_type = 'str', array=True )	
description='''Nr of roots to be calculated for each symmetry requested in symmParentStates, if an integer is given. '''
description += ''' If a string is given it will be interpreted as the path to a file containing determinants.'''
ciroParentStates.description = description
#
spinParentStates		= XchemKeyword( name = 'spinParentStates' , data_type = 'int', array=True )
description='''Multiplicity of the states requested in symmParentStates.'''
spinParentStates.description = description
#
spinAugmentedStates		= XchemKeyword( name = 'spinAugmentedStates' , data_type = 'int' )
description='''Multiplicity of all augmented states'''
spinAugmentedStates.description = description
#
spinAugmentingElectron	= XchemKeyword( name = 'spinAugmentingElectron' , data_type = 'str', array=True)		
description='''Spin of augmenting electron (alpha or beta)'''
spinAugmentingElectron.description = description
#
nameSourceStates		= XchemKeyword( name = 'nameSourceStates' , data_type = 'str', optional=True, array=True, default=[] )	
description='''Label used to defined groups of sources states.'''
nameSourceStates.description = description
#
symmSourceStates		= XchemKeyword( name = 'symmSourceStates' , data_type = 'int', optional=True, array=True )
description='''Symmetry for each group of source states, defined in nameSourceStates.'''
symmSourceStates.description = description
#
ciroSourceStates		= XchemKeyword( name = 'ciroSourceStates' , data_type = 'str', optional=True, array=True, default = None )
description='''Nr of roots to be calculated for each group of srouce states symmParentStates, if an integer is given. '''
description += ''' If a string is given it will be interpreted as the path to a file containing determinants.'''
ciroSourceStates.description = description
#
createSourceFromParent	= XchemKeyword( name = 'createSourceFromParent' , data_type = 'bool', optional = True, array = True )
description='''For each group defined in nameSourceStates specify if the roots are to be calculated using the parent ion orbitals (T/F)'''
createSourceFromParent.description = description
#
maxMultipoleOrder = XchemKeyword( name = 'maxMultipoleOrder' , data_type = 'int', optional = True, default = 6 )
description='''Define order of multipoles to be used in multipoles expansion of Hamiltonian including B-spline orbitals'''
maxMultipoleOrder.description = description
#
forceC1 = XchemKeyword( name = 'forceC1', data_type = 'bool', optional = True, default = True )
description = '''In creating the interface with the scattering code all augmented states are artificially treated as being of symmetry C1. This can be useful in the definiction of the close coupling input'''
forceC1.description = description
#
rmin			= XchemKeyword( name = 'rmin', data_type = 'float' )
description = 'Defines the radius at which the B-Splines begin.'
rmin.description = description
#
rmax			= XchemKeyword( name = 'rmax', data_type = 'float' )
description = 'Defines the radius at which the B-Splines end.'
rmax.description = description
#
numberNodes 	= XchemKeyword( name = 'numberNodes', data_type = 'int' )
description = 'Define the number of nodes for the construction of the B-spline part of the GABS basis.'
numberNodes.description = description
#
splineOrder    = XchemKeyword( name = 'splineOrder', data_type = 'int', optional = True, default = 7 )
description = 'Define the order of the B-splines.'
splineOrder.description = description
#
matrixElementThreshold = XchemKeyword( name = 'matrixElementThreshold', data_type = 'float', optional = True, default = 1e-24 )
description = ''
matrixElementThreshold.description = description
#
gaussianBSplineOverlapThreshold = XchemKeyword( name = 'gaussianBSplineOverlapThreshold' , data_type = 'float', optional = True, default = 1e-16 )
description = ''
gaussianBSplineOverlapThreshold.description = description
#
gaussianProjectorThreshold = XchemKeyword( name = 'gaussianProjectorThreshold', data_type = 'float', optional = True, default = 1e-8 )
description = ''
gaussianProjectorThreshold.description = description
#
nBSplineSkip = XchemKeyword( name = 'nBSplineSkip', data_type = 'int', optional = True, default =3)
description = ''
nBSplineSkip.description = description
#
plotBasis = XchemKeyword( name = 'plotBasis', data_type = 'bool', optional = True, default = False )
description = 'Plot the GABS basis.'
plotBasis.description = description
#
force_ccm = XchemKeyword( name = 'force_ccm', data_type = 'bool', optional = True, default = False )
description = 'Force computation of ccms'
force_ccm.description = description
#
nPlot = XchemKeyword( name = 'nPlot', data_type = 'int', optional = True, default = 1000 )
description = 'Number of radial datapoints (rmin <= r <= rmax) used if plotBasis=T.'
nPlot.description = description
#
closeCouplingChannels = XchemKeyword( name = 'closeCouplingChannels', data_type = 'custom', array = True, optional = True)
description = 'Define channels to be included in close coupling expansion. Format: PI_1 (l_1 m_1, l_2 m_2, ...); PI_2 ( l_1 m_1,...); ..., where PI_i is an integer defining the parent ion according to the ordere they were defined in in ciroParentStates. I.e. if ciroParentStates=3 2 3 1 and symmParentStates=1 2 3 4, then PI_i ranges from 1 to 9. with PI_i = 1..3 corresponding to the three states of symmetry 1, PI_i = 4,5 to the states of symmetry 2 etc.'
closeCouplingChannels.description = description
#
brasym = XchemKeyword( name = 'brasym', data_type = 'str', optional = True, default = 'A' ) 
#
ketsym = XchemKeyword( name = 'ketsym', data_type = 'str', optional = True ) 
#
operators = XchemKeyword( name = 'operators', data_type = 'str', optional = True, default=['S','H','Dipole'], array = True)
description = 'Set operators whose matrix elements in the close coupling basis are to be computed: S for Overlap, H for Hamiltonian and Dipole for Dipole.'
operators.description = description
#
axes = XchemKeyword( name = 'axes', data_type = 'str', optional = True, array = True, default=['x','y','z'])
description = 'If "Dipole" is included in operators, set the components to be computed.'
axes.description = description
#
gauges = XchemKeyword( name = 'gauges', data_type = 'str', optional = True, array = True, default=['l','v'])
description = 'If "Dipole" is included in operators, set the gauge in which to compute it: (l)ength, (v)elocity.'
gauges.description = description
#
cond  = XchemKeyword( name = 'cond', data_type = 'bool', optional = True, default = False )
description = 'If cond=T use the conditinod matrices corresponding to the method set in conditionBsMethod.'
cond.description = description
#
checkDep  = XchemKeyword( name = 'checkDep', data_type = 'bool', optional = True, default = True )
description = 'If True checks if there are linear dependences up to a threshold(checkThreshold)'
checkDep.description = description
#
checkThreshold = XchemKeyword( name = 'checkThreshold', data_type = 'float', optional = True, default = 1e-3)
description = '''Threshold used to check if there are linear dependences comparing the Ground State obained with the boxstates module and with the obtained in the Quantum Chemistry part.'''
checkThreshold.description = description
#
numBsDropBeginning = XchemKeyword( name = 'numBsDropBeginning', data_type = 'int', optional = True, default = 0 )
description = '''Number of B-splines to be removed at the beginning of the box. (applied if using conditionBsMethod=RM)'''
numBsDropBeginning.description = description
#
numBsDropEnding = XchemKeyword( name = 'numBsDropEnding', data_type = 'int', optional = True, default = 0 )
description = '''Number of B-splines to be removed at the end of the box. (applied if using conditionBsMethod=RM)'''
numBsDropEnding.description = description
#
conditionNumber = XchemKeyword( name = 'conditionNumber', data_type = 'float', optional = True, default = 1e-5)
description = '''Threshold used to remove overlap matrix linear dependencies for hamiltonian diagonalization.'''
conditionNumber.description = description
#
conditionBsMethod = XchemKeyword( name = 'conditionBsMethod', data_type = 'str', optional = True, default = 'LI' )
description = 'Set method for the basis conditioning: RM removes the first and last few B-splines as set in numBsDropBeginning and numBsDropEnding. LI builds a linear combination of B-splines linear independent to the diffuse Gaussian. LIO builds a linear combination of B-splines and Gaussian orthogonal to the diffuse Gaussian.'
conditionNumber.description = description
#
conditionBsThreshold = XchemKeyword( name = 'conditionBsThreshold', data_type = 'str', optional = True, default = 1e-6 )
description = 'Threshold for conditioning the B-splines (if conditionBsMethod=LI[O]).'
conditionBsThreshold.description = description
#
loadLocStates = XchemKeyword( name = 'loadLocStates', data_type = 'bool', optional = True, default = True ) 
description = 'If T(rue) the source states are included. If (F)alse the close coupling matrix included states resulting from augmentation of a parent ion.'
loadLocStates.description = description
#
emin = XchemKeyword( name = 'emin', data_type = 'float', optional = True, default = None )
description = 'Set the minimum of the energy range in which scattering states are to be computed.'
emin.description = description
#
emax = XchemKeyword( name = 'emax', data_type = 'float', optional = True, default = None )
description = 'Set the maximum of the energy range in which scattering states are to be computed'
emax.description = description
#
pqnmin = XchemKeyword( name = 'pqnmin', data_type = 'float', optional = True, default = None )
description = 'Set the minimum principal quantum number for the calculation of the Rydberg states.'
pqnmin.description = description
#
pqnmax = XchemKeyword( name = 'pqnmax', data_type = 'float', optional = True, default = None )
description = 'Set the maximum principal quantum number for the calculation of the Rydberg states.'
pqnmax.description = description
#
dpqnmax = XchemKeyword( name = 'dpqnmax', data_type = 'float', optional = True, default = None )
description = 'Set the maximum difference of the principal quantum number between consecutive Rydberg states.'
dpqnmax.description = description
#
dPhmax = XchemKeyword( name = 'dPhmax', data_type = 'float', optional = True, default = None )
description = 'Set the maximum difference of phase shift between consecutive scattering states.'
dPhmax.description = description
#
dEmax = XchemKeyword( name = 'dEmax', data_type = 'float', optional = True, default = None )
description = 'Set the maximum energy difference between the computed scattering states.'
dEmax.description = description
#
thrmin = XchemKeyword( name = 'thrmin', data_type = 'int', optional = True, default = None )
description = 'Minimum ionization threshold to start computing the scattering states (lower bound).'
thrmin.description = description
#
thrmax = XchemKeyword( name = 'thrmax', data_type = 'int', optional = True, default = None )
description = 'Maximum ionization threshold for computing the scattering states (upper bound).'
thrmax.description = description
#
ne = XchemKeyword( name = 'ne', data_type = 'float', optional = True, default = None )
description = 'Set the the numner of scattering states to be calculated in the interval defined by emin and emax.'
ne.description = description
#
scattConditionNumber = XchemKeyword( name = 'scattConditionNumber', data_type = 'float', optional = True, default = 1e-10 )
description = 'Threshold used to remove overlap matrix linear dependencies for the scattering states calculation. We can gain more flexibility relaxing this threshold.'
scattConditionNumber.description = description
#
mode = XchemKeyword( name = 'mode', data_type = 'str', optional = True, default = 'uniform')
description = 'It defines the energy grid for computing the scattering states. The possible variables are: uniform, info, rydberg, refine and resolve. Just one has to be specified on each calculation.'
mode.description = description
#
scatLabel = XchemKeyword( name = 'scatLabel', data_type = 'str', optional = True, default = '1',array=True)
description = 'Label of a set of scattering states  when using spec method (1 by default)'
scatLabel.description = description
#
cm = XchemKeyword( name = 'cm', data_type = 'str', optional = True, default = 'spec')
description = 'Chose method used for computation of scattering states. Valid choices are: hsle (for LU decomp), invm (matrix inversion), spec (matrix inversion in box state basis). See manual for details.'
cm.description = description
#
transBlocks  = XchemKeyword( name = 'transBlocks', data_type = 'bool', optional = True, default = False )
description = 'Transform all operators by blocks, not just de H and S'
transBlocks.description = description
#
overwrite  = XchemKeyword( name = 'overwrite', data_type = 'bool', optional = True, default = False )
description = 'If overwrite=T recompute scattering states from scratch.'
overwrite.description = description
#
verbous  = XchemKeyword( name = 'verbous', data_type = 'bool', optional = True, default = False )
description = 'If verbous=T more information is printed.'
verbous.description = description
#
trans = XchemKeyword( name = 'trans', data_type = 'str', optional = True, default = 'cb') 
description = 'Define the kind of transition whose dipole transition elements are to be computed: bb (box-box), bc (box-continuum), cb (continuum-box) or cc (continnum-continuum).'
trans.description = description
#
braemin = XchemKeyword( name = 'braemin', data_type = 'float', optional = True, default = -1.0e99)
description = 'Define the minimum of the energy range for which to compute the dipole transition elements (for trans=[cc,cb])'
braemin.description = description
#
braemax = XchemKeyword( name = 'braemax', data_type = 'float', optional = True, default = 1.0e99)
description = 'Define the maximum of the energy range for which to compute the dipole transition elements (for trans=[cc,cb])'
braemax.description = description
#
ketemin = XchemKeyword( name = 'ketemin', data_type = 'float', optional = True, default = -1.0e99)
description = 'Define the minimum of the energy range for which to compute the dipole transition elements (for trans=[cc,bc])'
ketemin.description = description
#
ketemax = XchemKeyword( name = 'ketemax', data_type = 'float', optional = True, default = 1.0e99)
description = 'Define the maximum of the energy range for which to compute the dipole transition elements (for trans=[cc,bc])'
ketemax.description = description
#
boxi = XchemKeyword( name = 'boxi', data_type = 'int', optional = True, default = 1 )
description = 'For boundstate transitions: define ordinal of the bound state for which to compute dipole transition elements (e.g. 1 for least energetic eigenstate of Hamiltonian). If you want the complete matrices between all the box states, set boxi to -1.'
boxi.description = description
#
sinasymp = XchemKeyword( name = 'sinasymp', data_type = 'bool', optional = True, default = False)
sinasymp.description = description
#
cap = XchemKeyword( name = 'cap', data_type = 'custom', optional = True, array = True, default = '' )
description = 'Define complex absorbing potentials to be used. A cap is defined as V(r)= z \theta(r-Ro) (r-Ro)^n. Each cap may be specified by three or four numbers. '\
			  + 'If three numbers are given they corrspond to: order of polynomial growth of cap with radius n, starting radius of cap Ro and the imaginary '\
			  + 'component of the strength of the cap Im(z). If a fourth number is given it corresponds to Re(z), which may be use to accalerate the wavepacket.'
cap.description = description
#
sewardOptions = XchemKeyword( name = 'sewardOptions', data_type = 'custom', optional = True, default = '', array=True)
description = 'Provide extra options to seward (for example R2PO etc. )'
sewardOptions.description = description
#
rasscfOptions = XchemKeyword( name = 'rasscfOptions', data_type = 'custom', optional = True, default = '', array=True)
rasscfOptions.description = 'Provide extra options to rasscf (for example SDAV)'
#
gugaDir = XchemKeyword( name = 'gugaDir', data_type = 'str', optional = True, default = '')
description = 'Specify a directory to read guga tables from ($PWD/<gugaDir> and <gugaDir> are tried in this order). This may be useful to avoid recalculating guga tables if working with '\
			  + 'the same active space at different geometries. Note that if a guga file with the correct name is found, its accuracy will not be checked. '\
			  + 'It is up to the user to make sure the guga tables correspond to the correct active space.'
gugaDir.description = description
#
justRot = XchemKeyword( name = 'justRot', data_type = 'bool', optional = True, default = False)
description = "If set to TRUE the overlap blocks won't be diagonalized again and just the Operators specified will be rotated again"
justRot.description = description
#
coreHoleParentIon = XchemKeyword( name = 'coreHoleParentIon', data_type = 'int', optional = True, default = [], array = True )
description = 'Specify which of the requested sets of parent ion states, are to be used to compute core hole states. This is done via MOLCAS hexs keyword.'
coreHoleParentIon.description = description
#
coreHoleSource = XchemKeyword( name = 'coreHoleSource', data_type = 'int', optional = True, default = [], array = True )
description = 'Specify which of the requested sets of source states, are to be used to compute core hole states. This is done via MOLCAS hexs keyword.'
coreHoleSource.description = description
#
propfile = XchemKeyword( name = 'propfile', data_type = 'str', optional = True, default = '')
description = 'Specify the wavefunction output after the propagation over the box states. It has to be in the BoxStates folder'
propfile.description = description
#
time = XchemKeyword( name = 'time', data_type = 'float', optional = True, default = 0)
description = 'The time of the propagation to compute the angular distribution (required).'
time.description = description

PIcharge = XchemKeyword( name="PIcharge", data_type='float', optional = True, default= None)
description='Parent Ion charge, overwrites the calculated one using the number of inactive orbitals and active electrons'
PIcharge.description = description
