#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
import xchem_keywords
from xchem_input_generators import *
from osutils import *
import itertools

class Ccm(XchemModule):
	def __init__(self, input_dict, info = False ):
		super().__init__(input_dict, 'ccm', info = info )
	
		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.ketsym )
		self.add_keyword( xchem_keywords.axes )
		self.add_keyword( xchem_keywords.gauges )
		self.add_keyword( xchem_keywords.force_ccm )
		self.add_keyword( xchem_keywords.cond )
		self.add_keyword( xchem_keywords.operators )
		self.add_keyword( xchem_keywords.defer )
		self.add_keyword( xchem_keywords.numBsDropBeginning )
		self.add_keyword( xchem_keywords.numBsDropEnding )
		self.add_keyword( xchem_keywords.conditionNumber )
		self.add_keyword( xchem_keywords.conditionBsMethod )
		self.add_keyword( xchem_keywords.conditionBsThreshold )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )
		if (os.environ['rmLock']):
			for dirpath, dirs, files in os.walk(self.DataDir):
				for fname in files:
					if (fname.endswith('.lock')):
						file2rm = os.path.join(dirpath,fname)
						os.remove( os.path.join( self.DataDir, file2rm ) )

		qci='qci'
		data_dirs = self.load_previous_meta_data([str(qci),'bsplines'])
		self.meta_dict['uid'] = uid

		Xchem = Xchem_input( self.input_dict )
		SAE = SAE_input( self.input_dict ) 

		self.log_message('Build Close Coupling Matrix')
		if self.input_dict['defer']:
			self.job_list.add_job( work_in_place = True )
		for op in self.input_dict['operators']:
			if op != 'Dipole':
				BuildSymmetricElectronicSpace = BuildSymmetricElectronicSpace_input( self.input_dict, self.meta_dict, op)
				BuildConditionedSymmetricElectronicSpace = BuildConditionedSymmetricElectronicSpace_input( self.input_dict, self.meta_dict, op)
				if self.input_dict['defer']:
					if self.meta_dict['uid'] == 0:
						task = self.xchem.task('BuildSymmetricElectronicSpace',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = BuildSymmetricElectronicSpace)
					else:
						task = self.xchem.task('BuildSymmetricElectronicSpace',
										xchem_cmd_line_input = BuildSymmetricElectronicSpace)
					self.job_list.add_task( task )
#					if self.input_dict['cond']:
#						task = self.xchem.task('BuildConditionedSymmetricElectronicSpace',
#										xchem_file_input=[SAE,Xchem],
#										xchem_file_input_name=['SAE','Xchem'],
#										xchem_cmd_line_input = BuildConditionedSymmetricElectronicSpace)
#						self.job_list.add_task( task )
								 
				else:
					self.xchem.run('BuildSymmetricElectronicSpace',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = BuildSymmetricElectronicSpace)
#					if self.input_dict['cond']:
#						self.xchem.run('BuildConditionedSymmetricElectronicSpace',
#										xchem_file_input=[SAE,Xchem],
#										xchem_file_input_name=['SAE','Xchem'],
#										xchem_cmd_line_input = BuildConditionedSymmetricElectronicSpace)
			else:
				for axis, gauge in itertools.product( self.input_dict['axes'],self.input_dict['gauges'] ):
					BuildSymmetricElectronicSpace = BuildSymmetricElectronicSpace_input( self.input_dict, self.meta_dict, op, axis=axis, gauge=gauge)
					BuildConditionedSymmetricElectronicSpace = BuildConditionedSymmetricElectronicSpace_input( self.input_dict, self.meta_dict, op, axis=axis, gauge=gauge)
					if self.input_dict['defer']:
						if self.meta_dict['uid'] == 0:
							task = self.xchem.task('BuildSymmetricElectronicSpace',
											xchem_file_input=[SAE,Xchem],
											xchem_file_input_name=['SAE','Xchem'],
											xchem_cmd_line_input = BuildSymmetricElectronicSpace)
						else:
							task = self.xchem.task('BuildSymmetricElectronicSpace',
											xchem_cmd_line_input = BuildSymmetricElectronicSpace)
						self.job_list.add_task( task )
#						if self.input_dict['cond']:
#							task = self.xchem.task('BuildConditionedSymmetricElectronicSpace',
#											xchem_file_input=[SAE,Xchem],
#											xchem_file_input_name=['SAE','Xchem'],
#											xchem_cmd_line_input = BuildConditionedSymmetricElectronicSpace)
#							self.job_list.add_task( task )
					else:
						self.xchem.run('BuildSymmetricElectronicSpace',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = BuildSymmetricElectronicSpace)
#						if self.input_dict['cond']:
#							self.xchem.run('BuildConditionedSymmetricElectronicSpace',
#											xchem_file_input=[SAE,Xchem],
#											xchem_file_input_name=['SAE','Xchem'],
#											xchem_cmd_line_input = BuildConditionedSymmetricElectronicSpace)
		if self.input_dict['defer']:
			self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
		self.dump_meta_data()
