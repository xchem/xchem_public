#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
import xchem_keywords
from xchem_input_generators import *
from osutils import *
import itertools

class Bsplines(XchemModule):
	def __init__(self, input_dict, info = False):
		super().__init__(input_dict, 'bsplines', data_dir = 'ccm',  info = info)

		self.add_keyword( xchem_keywords.rmin )
		self.add_keyword( xchem_keywords.rmax )
		self.add_keyword( xchem_keywords.numberNodes )
		self.add_keyword( xchem_keywords.splineOrder )
		self.add_keyword( xchem_keywords.matrixElementThreshold )
		self.add_keyword( xchem_keywords.gaussianBSplineOverlapThreshold )
		self.add_keyword( xchem_keywords.gaussianProjectorThreshold )
		self.add_keyword( xchem_keywords.plotBasis )
		self.add_keyword( xchem_keywords.nPlot )
		self.add_keyword( xchem_keywords.nBSplineSkip )
		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.defer )
		self.add_keyword( xchem_keywords.cap )
		self.add_keyword( xchem_keywords.numBsDropBeginning )
		self.add_keyword( xchem_keywords.numBsDropEnding )
		self.add_keyword( xchem_keywords.conditionNumber )
		self.add_keyword( xchem_keywords.conditionBsMethod )
		self.add_keyword( xchem_keywords.conditionBsThreshold )
		self.add_keyword( xchem_keywords.cond )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.ketsym )
		self.add_keyword( xchem_keywords.PIcharge )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )
		if (os.environ['rmLock']):
			for dirpath, dirs, files in os.walk(self.DataDir):
				for fname in files:
					if (fname.endswith('.lock')):
						file2rm = os.path.join(dirpath,fname)
						os.remove( os.path.join( self.DataDir, file2rm ) )
		
		qci='qci'
		self.load_previous_meta_data(['integrals',str(qci)])
## JGV putting the PIcharge as meta_data if exists
		if self.input_dict['PIcharge']:
			self.meta_dict['PIcharge']=self.input_dict['PIcharge']
		else:
			self.meta_dict['PIcharge']=None
		ConvertQCI = ConvertQCI_input( self.input_dict, self.meta_dict, str(qci) )


		Xchem = Xchem_input( self.input_dict )
		SAE = SAE_input( self.input_dict ) 

		BasisMixed = BasisMixed_input( self.input_dict )
		GaussianBSplineBasis = GaussianBSplineBasis_input( self.input_dict )
		BSplineBasis =BSplineBasis_input( self.input_dict )
		GaussianBasis = GaussianBasis_input( self.input_dict )
		AbsorptionPotential = AbsorptionPotential_input( self.input_dict)

		self.log_message( 'Computing Basic Matrix Elements' )
		BasicMatrixElements = BasicMatrixElements_input( )
		if self.input_dict['defer']:
			self.job_list.add_job( work_in_place = True )
			task = self.xchem.task('BasicMatrixElements', xchem_file_input = [SAE, BasisMixed, GaussianBSplineBasis, BSplineBasis, GaussianBasis,AbsorptionPotential],
						   xchem_file_input_name = ['SAE', 'BasisMixed', 'GaussianBSplineBasis', 'BSplineBasis','GaussianBasis','AbsorptionPotential'], 
						   xchem_cmd_line_input = BasicMatrixElements)
			self.job_list.add_task(task)
		else:
			self.xchem.run('BasicMatrixElements', xchem_file_input = [SAE, BasisMixed, GaussianBSplineBasis, BSplineBasis, GaussianBasis,AbsorptionPotential],
						   xchem_file_input_name = ['SAE', 'BasisMixed', 'GaussianBSplineBasis', 'BSplineBasis','GaussianBasis','AbsorptionPotential'], 
						   xchem_cmd_line_input = BasicMatrixElements)

		self.log_message( 'Converting QC data' )
	
		if self.input_dict['defer']:
			task = self.xchem.task('ConvertQCI', xchem_file_input=[SAE,Xchem],
						  xchem_file_input_name=['SAE','Xchem'],
						  xchem_cmd_line_input = ConvertQCI)
			self.job_list.add_task(task)
		else:
			self.xchem.run('ConvertQCI', xchem_file_input=[SAE,Xchem],
						  xchem_file_input_name=['SAE','Xchem'],
						  xchem_cmd_line_input = ConvertQCI)
		BuildConditioningMatrices = BuildConditioningMatrices_input(self.input_dict, self.meta_dict)
		if self.input_dict['cond']:
			if self.input_dict['defer']:
				task = self.xchem.task('BuildConditioningMatrices', xchem_file_input=[SAE,Xchem],
							  xchem_file_input_name=['SAE','Xchem'],
							  xchem_cmd_line_input = BuildConditioningMatrices)
				self.job_list.add_task(task)
			
			else:
				self.xchem.run('BuildConditioningMatrices', xchem_file_input=[SAE,Xchem],
							  xchem_file_input_name=['SAE','Xchem'],
							  xchem_cmd_line_input = BuildConditioningMatrices)
		if self.input_dict['defer']:
			self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
		
		self.dump_meta_data()
