#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_input_generators import *
from textwrap import wrap
import numpy as np
import xchem_keywords
import matplotlib as mpl
mpl.use('Agg')
global nomatplotlib 
nomatplotlib = False
try:
	import matplotlib.pyplot as plt
except:
	nomatplotlib = True

class Plot(XchemModule):
	@classmethod
	def loadtxtvarnumcols(cls, filename, comments):
		with open(filename, 'r') as fhandler:
			data = [ list(map(float,line.split())) for line in fhandler.readlines() if line.lstrip()[0] not in comments ]
		maxlen = max(map(len,data))
		data = [ l + [0.0] * (maxlen-len(l)) for l in data  ]
		return(np.array( data ))

	def __init__(self, input_dict, info = False ):
		super().__init__(input_dict, 'plot', data_dir = 'plot', info = info)
		self.add_keyword( xchem_keywords.cm ) 
		self.add_keyword( xchem_keywords.scatLabel ) 
		self._check_keywords()
		self.PIE = []

	def plotEvals(self,infile,outfile):
		evals = np.loadtxt( infile, skiprows = 1 )
		GSEnergy = evals[0,1]
		nEvals,_ = evals.shape
		nframes = 1
		if np.floor( nEvals / 100 ) != 0:
		    nframes += 1
		    nEvalsSmall = int( nEvals / 100 )
		else:
		    nEvalsSmall = None
		if np.floor( nEvals / 10 ) != 0:
		    nframes += 1
		    nEvalsMedium = int( nEvals / 10 )
		else:
		    nEvalsMedium = None
		fig,_ = plt.subplots(figsize=(10,10) )
		fig.suptitle( '\n'.join(wrap('Eigen Energies, from: {}'.format( infile ), 60)))
		iframe = 1
		if nEvalsSmall is not None:
		    ax = plt.subplot(nframes,1,iframe)
		    ax.scatter( evals[:nEvalsSmall,0], evals[:nEvalsSmall,1], marker = 'X')
		    plt.ylabel('Energy (a.u.)')
		    iframe += 1
		
		if nEvalsMedium is not None:
		    ax = plt.subplot(nframes,1,iframe)
		    ax.scatter( evals[:nEvalsMedium,0], evals[:nEvalsMedium,1], marker = 'X')
		    plt.ylabel('Energy (a.u.)')
		    iframe += 1
		ax = plt.subplot(nframes,1,iframe)
		ax.scatter( evals[:,0], evals[:,1], marker = 'X')
		plt.ylabel('Energy (a.u.)')
		plt.xlabel('Eigenvalue ordinal')
		fig.savefig( outfile )
		return GSEnergy

	def plotQEvals(self, infile, outfile):
		evals = np.loadtxt( infile, skiprows = 1 )
		nEvals,_ = evals.shape
		e0 = np.min( evals[:,1] )
		gamma0 = np.min( evals[:,2] ) 
		fig,_ = plt.subplots(figsize=(10,10) )
		fig.suptitle( '\n'.join(wrap('Quenched Eigen Energies, from: {}'.format( infile ), 60)))

		ax = plt.subplot(3,1,1)
		ax.scatter( evals[:,1], evals[:,2], marker = 'X')
		ax.axis([e0,2*max(self.PIE)-min(self.PIE),gamma0 * 0.01, abs(gamma0*0.001)])
		for i,PI in enumerate(self.PIE):
			plt.text( PI, abs(gamma0*0.002), 'Ionization thesh. {}'.format(i), color = 'k' )
			plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Imag(Energy) (a.u.)')

		ax = plt.subplot(3,1,2)
		ax.scatter( evals[:,1], evals[:,2], marker = 'X')
		ax.axis([e0,2*max(self.PIE)-min(self.PIE),gamma0 * 0.1, abs(gamma0*0.01)])
		for PI in self.PIE:
			plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Imag(Energy) (a.u.)')

		ax = plt.subplot(3,1,3)
		ax.scatter( evals[:,1], evals[:,2], marker = 'X')
		ax.axis([e0,2*max(self.PIE)-min(self.PIE),gamma0 * 1.0, abs(gamma0*0.1)])
		for PI in self.PIE:
			plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Imag(Energy) (a.u.)')
		plt.xlabel('Real(Energy) (a.u.)')
		fig.savefig( outfile )
	
	def plotPhase_new(self, infile, outfile):
		phases = []
		nvalues = 0
		with open(infile) as inf:
			for line in inf:
				phases.append(line.split())
				nvalues += 1
		nchannels = int(phases[nvalues-1][1])
#		nvalues, nchannels = phases.shape 
#		total = np.zeros( shape= (nvalues, 1) )
		fig,_ = plt.subplots(figsize=(10,10) )
#		fig.suptitle( '\n'.join(wrap('Scattering phase, from: {}'.format( infile ), 60)))
		ax = plt.subplot(1,1,1)
		phases_channels = np.zeros((nvalues,nchannels))
		energies = np.zeros((nvalues))
		total = np.zeros((nvalues))
		for val in range(nvalues):
			energies[val] = float(phases[val][0] )
			total[val] = float(phases[val][2] )
			for channel in range(int(phases[val][1])):
				phases_channels[val,channel] = float(phases[val][channel+2] )
		for channel in range(nchannels):
			ax.plot(energies[:],phases_channels[:,channel], label = 'channel {}'.format(channel+1))
		ax.plot(energies[:],total[:], label = 'total')
		ax.legend()
		ax.axis([np.min(energies[:]),np.max(energies[:]),np.min(total[:]),np.max(total[:])])
		for i,PI in enumerate(self.PIE):
			if PI > np.min(energies[:]) and PI < np.max(energies[:]):
				plt.text( PI, np.mean(total), 'Ionization thesh. {}'.format(i), color = 'k' )
				plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Rel.Scattering Phase (dimensionless)')
		plt.xlabel('Energy (a.u.)')
		fig.savefig( outfile ) 


	def plotPhase(self, infile, outfile):
		phases = np.loadtxt( infile, comments='#' )
		phases = phases[phases[:,0].argsort()]
		nvalues, nchannels = phases.shape 
		total = np.zeros( shape= (nvalues, 1) )
		fig,_ = plt.subplots(figsize=(10,10) )
		fig.suptitle( '\n'.join(wrap('Scattering phase, from: {}'.format( infile ), 60)))
		ax = plt.subplot(1,1,1)
		for channel in range(1,nchannels):
			total += phases[:,channel].reshape((-1,1))
			ax.plot(phases[:,0],phases[:,channel], label = 'channel {}'.format(channel))
		ax.plot(phases[:,0],total, label = 'total')
		ax.legend()
		ax.axis([np.min(phases[:,0]),np.max(phases[:,0]),np.min(phases[:,1:]),np.max(total)])
		for i,PI in enumerate(self.PIE):
			if PI > np.min(phases[:,0]) and PI < np.max(phases[:,0]):
				plt.text( PI, np.mean(total), 'Ionization thesh. {}'.format(i), color = 'k' )
				plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Rel.Scattering Phase (dimensionless)')
		plt.xlabel('Energy (a.u.)')
		fig.savefig( outfile ) 

	def plotDta(self, infile, outfile, gauge):
		dtas = Plot.loadtxtvarnumcols( infile, comments='#')
		dtas = dtas[dtas[:,1].argsort()]
		nvalues, nchannels = dtas.shape 
		nchannels -= 2
		total = np.zeros( shape= (nvalues, 1) )
		fig,_ = plt.subplots(figsize=(10,10) )
		fig.suptitle( '\n'.join(wrap('Cross Sections, from: {}'.format( infile ), 60)))
		ax = plt.subplot(1,1,1)
		c = 137.035
		auToMb = 5.292**2
		for channel in range(nchannels):
			if gauge == 'Len':
				cs = np.multiply( np.square(dtas[:,channel + 2]), dtas[:,1] - dtas[:,0] ) * auToMb * 4 * np.pi**2 / c 
			else:
				cs = np.divide( np.square(dtas[:,channel + 2]), dtas[:,1] - dtas[:,0] ) * auToMb * 4 * np.pi**2 / c 
			total += cs.reshape((-1,1))
			ax.plot(dtas[:,1], cs, label = 'channel {}'.format(channel+1))
		ax.plot(dtas[:,1], total, label = 'total')
		ax.legend()
		ax.axis([np.min(dtas[:,1]),np.max(dtas[:,1]),np.min(dtas[:,2:]),np.max(total)])
		for i,PI in enumerate(self.PIE):
			if PI > np.min(dtas[:,1]) and PI < np.max(dtas[:,1]):
				plt.text( PI, np.mean(total), 'Ionization thesh. {}'.format(i), color = 'k' )
				plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Cross section (Mbn)')
		plt.xlabel('Energy (a.u)')
		fig.savefig( outfile ) 

	def plotDta_new(self, infile, outfile, gauge, GS_en):
		dips = []
		nvalues = 0
		with open(infile) as inf:
			inf.readline()
			for line in inf:
				dips.append(line.split())
				nvalues += 1
		nchannels = 2*int(dips[nvalues-1][1])
		total = np.zeros((nvalues))
		fig,_ = plt.subplots(figsize=(10,10) )
		fig.suptitle( '\n'.join(wrap('Cross Sections, from: {}'.format( infile ), 60)))
		ax = plt.subplot(1,1,1)
		c = 137.035
		auToMb = 5.292**2
		energies = np.zeros((nvalues))
		dips_channels = np.zeros((nvalues,nchannels))
		for val in range(nvalues):
			energies[val] = float(dips[val][0])
			for channel in range(2*int(dips[val][1])):
				dips_channels[val,channel] = float(dips[val][channel+2] )
		for channel in range(nchannels):
			if gauge == 'Len':
				cst = np.multiply( np.square(dips_channels[:,channel]), energies[:] - GS_en ) * auToMb * 4 * np.pi**2 / c 
			else:
				cst = np.divide( np.square(dips_channels[:,channel]), energies[:] - GS_en ) * auToMb * 4 * np.pi**2 / c 
			total[:] += cst[:]
			if (channel % 2) != 0:
				cs += cst
				ax.plot(energies[:], cs, label = 'channel {}'.format(int((channel+1)/2)))
				cs = 0.0
			else:
				cs = cst
		ax.plot(energies[:], total[:], label = 'total')
		ax.legend()
		ax.axis([np.min(energies[:]),np.max(energies[:]),np.min(total[:]),np.max(total)])
		for i,PI in enumerate(self.PIE):
			if PI > np.min(energies[:]) and PI < np.max(energies[:]):
				plt.text( PI, np.mean(total), 'Ionization thesh. {}'.format(i), color = 'k' )
				plt.axvline( PI, color="k", ls='--' )
		plt.ylabel('Cross section (Mbn)')
		plt.xlabel('Energy (a.u)')
		fig.savefig( outfile ) 


	@prolog_and_epilog
	def run( self, uid = 0 ):
		if nomatplotlib:
			raise XchemError('Module matplotlib not present. Could not produce plots')
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )

		qci='qci'
		path_dict = self.load_previous_meta_data(['bsplines',str(qci)])
		found_path=False
		for i in range(1,10):
			path = os.path.join( path_dict['bsplines'], 'CloseCoupling','{}A_{}A'.format(i,i))
			if os.path.isdir( path ):
				found_path=True
				break
		if not found_path:
			raise XchemError('No data found to be plotted. Defer keyword used? Modules with plottable results are "boxstates", "scatteringstates" and "dta".')

		# Load ionization thresholds
		PI_energy_files = osutils.regexls( path_dict[str(qci)], 'En_.*\.out' )
		for PI_energy_file in PI_energy_files:
			self.PIE.append(np.loadtxt( os.path.join(path_dict[str(qci)], PI_energy_file) ))

		# Plot eigen energies if present.
		GSEnergy = 0.0
		if os.path.isdir( os.path.join(path, 'BoxStates') ):
			eigen_values_file = os.path.join(path, 'BoxStates', 'H_Eigenvalues')
			if os.path.isfile( os.path.join( eigen_values_file )):
				GSEnergy = self.plotEvals( eigen_values_file, "H_Eigenvalues.eps" )
				GSEnergy = self.plotEvals( eigen_values_file, "H_Eigenvalues.png" )
			qeigen_values_files = osutils.regexls( os.path.join(path, 'BoxStates'), 'QH.*Eigenvalues' )
			for qeigen_values_file in qeigen_values_files:
				self.plotQEvals( os.path.join(path, 'BoxStates', qeigen_values_file), "{}.eps".format(qeigen_values_file) )
				self.plotQEvals( os.path.join(path, 'BoxStates', qeigen_values_file), "{}.png".format(qeigen_values_file) )
				
		# Plot phases
		scat_phase_paths = []
		if self.input_dict['cm'] != 'spec':
			if os.path.isdir( os.path.join(path, 'ScatteringStates') ):
				hsle_path = os.path.join(path, 'ScatteringStates', 'HSLE','EigenPhases')
				if os.path.isdir( hsle_path ):
					scat_phase_paths.append( ('hsle', hsle_path) ) 
				spec_path = os.path.join(path, 'ScatteringStates', 'SPEC','EigenPhases')
				if os.path.isdir( spec_path ):
					scat_phase_paths.append( ('spec', spec_path) ) 
	
			for method, scat_phase_path in scat_phase_paths:
				eigen_phase_files = osutils.regexls( scat_phase_path, '^eigenphases_.*')
				for eigen_phase_file in eigen_phase_files:	
					self.plotPhase( os.path.join( scat_phase_path, eigen_phase_file), '{}.{}.eps'.format(eigen_phase_file, method))
					self.plotPhase( os.path.join( scat_phase_path, eigen_phase_file), '{}.{}.png'.format(eigen_phase_file, method))
		else:
			if os.path.isdir( os.path.join(path, 'ScatteringStates') ):
				spec_path = os.path.join(path, 'ScatteringStates', 'SPEC')
			for i in self.input_dict['scatLabel']:
				self.plotPhase_new( os.path.join( spec_path, 'scat'+i+'.dat'), 'SPEC_EigenPhases_'+i+'.eps')
				self.plotPhase_new( os.path.join( spec_path, 'scat'+i+'.dat'), 'SPEC_EigenPhases_'+i+'.png')
			
		# Plot cross sections (individual and partial)
		gauges = ['Len', 'Vel']
		if self.input_dict['cm'] != 'spec':
			if os.path.isdir( os.path.join(path, 'DipoleTransitions', 'CB')):
				dta_path = os.path.join(path, 'DipoleTransitions', 'CB')
				for method,_ in scat_phase_paths:
					for g in gauges:
						for r in orientations:
							dat_values_files = osutils.regexls( dta_path, 'Dipole{}_{}Box.*min.*max.*{}'.format(g,r,method.upper()))
							for dat_values_file in dat_values_files:
								self.plotDta( os.path.join( dta_path, dat_values_file), '{}.eps'.format(dat_values_file), g)
								self.plotDta( os.path.join( dta_path, dat_values_file), '{}.png'.format(dat_values_file), g)
		else:
			orientations = ['x','y','z']
			path = os.path.join( path_dict['bsplines'], 'DipoleTransitions')
			for g in gauges:
				for r in orientations:
					dat_values_files = osutils.regexls( path, 'DipoleTransAmp{}_{}_FromBoxStat*'.format(g,r))
					for dat_values_file in dat_values_files:
						self.plotDta_new( os.path.join( path, dat_values_file), '{}.eps'.format(dat_values_file), g, GSEnergy)
						self.plotDta_new( os.path.join( path, dat_values_file), '{}.png'.format(dat_values_file), g, GSEnergy)
