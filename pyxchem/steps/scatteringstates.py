#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
from xchem_input_generators import *
import xchem_keywords
from osutils import *
import itertools

class Scatteringstates(XchemModule):
	def __init__(self, input_dict, info = False ):
		super().__init__(input_dict, 'scatteringstates', data_dir='ccm', info = info)
	

		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.cond )
		self.add_keyword( xchem_keywords.thrmin )
		self.add_keyword( xchem_keywords.thrmax )
		self.add_keyword( xchem_keywords.dEmax )
		self.add_keyword( xchem_keywords.pqnmin )
		self.add_keyword( xchem_keywords.pqnmax )
		self.add_keyword( xchem_keywords.dpqnmax )
		self.add_keyword( xchem_keywords.scatLabel )
		self.add_keyword( xchem_keywords.dPhmax )
		self.add_keyword( xchem_keywords.emin )
		self.add_keyword( xchem_keywords.emax )
		self.add_keyword( xchem_keywords.ne )
		self.add_keyword( xchem_keywords.cm )
		self.add_keyword( xchem_keywords.scattConditionNumber  )
		self.add_keyword( xchem_keywords.mode )
		self.add_keyword( xchem_keywords.verbous )
		self.add_keyword( xchem_keywords.overwrite )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.numBsDropBeginning )
		self.add_keyword( xchem_keywords.numBsDropEnding )
		self.add_keyword( xchem_keywords.conditionNumber )
		self.add_keyword( xchem_keywords.loadLocStates )
		self.add_keyword( xchem_keywords.conditionBsThreshold )
		self.add_keyword( xchem_keywords.conditionBsMethod )
		self.add_keyword( xchem_keywords.defer )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )
		if (os.environ['rmLock']):
			for dirpath, dirs, files in os.walk(self.DataDir):
				for fname in files:
					if (fname.endswith('.lock')):
						file2rm = os.path.join(dirpath,fname)
						os.remove( os.path.join( self.DataDir, file2rm ) )
		qci='qci'

		if self.input_dict['cm'] != 'spec':
			data_dirs = self.load_previous_meta_data([str(qci),'bsplines','ccm'])

			Xchem = Xchem_input( self.input_dict )
			SAE = SAE_input( self.input_dict ) 

			self.log_message('Compute Scattering States')
			ComputeMultichannelScatteringStates = ComputeMultichannelScatteringStates_input( self.input_dict, self.meta_dict )
			ComputeMultichannelScatteringStates_concatenate = ComputeMultichannelScatteringStates_input( self.input_dict, self.meta_dict, concatenate = True )
			if self.input_dict['defer']:
				self.job_list.add_job( work_in_place = True )
				task = self.xchem.task('ComputeMultichannelScatteringStates',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeMultichannelScatteringStates )
				self.job_list.add_task( task )
				task = self.xchem.task('ComputeMultichannelScatteringStates',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeMultichannelScatteringStates_concatenate )
				self.job_list.add_task( task )
			else:
				self.xchem.run('ComputeMultichannelScatteringStates',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeMultichannelScatteringStates )
				self.xchem.run('ComputeMultichannelScatteringStates',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeMultichannelScatteringStates_concatenate )

			if self.input_dict['defer']:
				self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
			self.dump_meta_data()
		else:
			data_dirs = self.load_previous_meta_data([str(qci),'bsplines','ccm','boxstates'])
			Xchem = Xchem_input( self.input_dict )
			SAE = SAE_input( self.input_dict ) 

			self.log_message('Compute Scattering States')
			ComputeScatteringStatesll = ComputeScatteringStatesll_input( self.input_dict, self.meta_dict )
			if self.input_dict['defer']:
				self.job_list.add_job( work_in_place = True )
				task = self.xchem.task('ComputeScatteringStatesll',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeScatteringStatesll )
				self.job_list.add_task( task )
			else:
				print(ComputeScatteringStatesll)
				self.xchem.run('ComputeScatteringStatesll',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = ComputeScatteringStatesll )
			if self.input_dict['defer']:
				self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
			self.dump_meta_data()
