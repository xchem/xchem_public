#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
import xchem_keywords
from xchem_input_generators import *
from osutils import *
import itertools
import submission_tools

class Boxstates(XchemModule):
	def __init__(self, input_dict, info = False):
		super().__init__(input_dict, 'boxstates', data_dir='ccm', info = info)
	

		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.numBsDropBeginning )
		self.add_keyword( xchem_keywords.numBsDropEnding )
		self.add_keyword( xchem_keywords.checkThreshold )
		self.add_keyword( xchem_keywords.checkDep )
		self.add_keyword( xchem_keywords.conditionNumber )
		self.add_keyword( xchem_keywords.conditionBsMethod )
		self.add_keyword( xchem_keywords.conditionBsThreshold )
		self.add_keyword( xchem_keywords.loadLocStates )
		self.add_keyword( xchem_keywords.justRot )
		self.add_keyword( xchem_keywords.boxi )
		self.add_keyword( xchem_keywords.transBlocks )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.cond )
		self.add_keyword( xchem_keywords.operators )
		self.add_keyword( xchem_keywords.axes )
		self.add_keyword( xchem_keywords.gauges )
		self.add_keyword( xchem_keywords.defer )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )
		if (os.environ['rmLock']):
			for dirpath, dirs, files in os.walk(self.DataDir):
				for fname in files:
					if (fname.endswith('.lock')):
						file2rm = os.path.join(dirpath,fname)
						os.remove( os.path.join( self.DataDir, file2rm ) )
		qci='qci'
		data_dirs = self.load_previous_meta_data([str(qci),'bsplines','ccm'])

		Xchem = Xchem_input( self.input_dict )
		SAE = SAE_input( self.input_dict ) 
		SAE_File = open( os.path.join(self.DataDir,'SAE'),'w')
		SAE_File.write(SAE)
		SAE_File.close()
		Xchem_File = open( os.path.join(self.DataDir,'Xchem'),'w')
		Xchem_File.write(Xchem)
		Xchem_File.close()
		nPIs = 0
		for i in range(len(self.meta_dict['numParentIons'])):
			nPIs += self.meta_dict['numParentIons'][i]
		if (not self.input_dict['justRot']): 
			new_job = submission_tools.need_new_job( nPIs +1, int(os.environ['XCHEM_MAX_NUM_NODES']))
			nofjob=0
			self.log_message('Diagonalizing the Overlap by blocks')
			if self.input_dict['defer']:
				if new_job[nofjob]:
					self.job_list.add_job( work_in_place = True )
					DiagonalizeOverlap = DiagonalizeOverlap_input( self.input_dict, self.meta_dict )
				task = self.xchem.task('DiagonalizeOverlap',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = DiagonalizeOverlap )
				self.job_list.add_task( task )
				nofjob = nofjob + 1
				for i in range(nPIs):
					if new_job[nofjob]:
						self.job_list.add_job( work_in_place = True )
						DiagonalizeOverlap = DiagonalizeOverlap_input( self.input_dict, self.meta_dict, i + 1)
					task = self.xchem.task('DiagonalizeOverlap',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = DiagonalizeOverlap )
					self.job_list.add_task( task )
					nofjob = nofjob + 1
			else:
				DiagonalizeOverlap = DiagonalizeOverlap_input( self.input_dict, self.meta_dict )
				self.xchem.run('DiagonalizeOverlap',
				xchem_file_input=[SAE,Xchem],
				xchem_file_input_name=['SAE','Xchem'],
				xchem_cmd_line_input = DiagonalizeOverlap )
				for i in range(nPIs):
					DiagonalizeOverlap = DiagonalizeOverlap_input( self.input_dict, self.meta_dict, i + 1)
					self.xchem.run('DiagonalizeOverlap',
							xchem_file_input=[SAE,Xchem],
							xchem_file_input_name=['SAE','Xchem'],
							xchem_cmd_line_input = DiagonalizeOverlap )
		self.log_message('Transforming the Blocks for the different operators')
		if (self.input_dict['defer'] and not self.input_dict['justRot']):
			self.job_list.next_block()	
		DipLen = False 
		DipVel = False 
		CAP = False
		x = False 
		y = False 
		z = False
		nofjob = 0
		new_job = submission_tools.need_new_job((nPIs +1)* (nPIs +1), int(os.environ['XCHEM_MAX_NUM_NODES']))
		if ('H' in self.input_dict['operators']):
			op = 'H'
			if self.input_dict['defer']:
				if new_job[nofjob]:
					self.job_list.add_job( work_in_place = True )
				TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op )
				task = self.xchem.task('TransformMatBlocks',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = TransformMatBlocks )
				self.job_list.add_task( task )
				nofjob = nofjob + 1
				for i in range(nPIs):
					if new_job[nofjob]:
						self.job_list.add_job( work_in_place = True )
					TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion=i+1 )
					task = self.xchem.task('TransformMatBlocks',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = TransformMatBlocks )
					self.job_list.add_task( task )
					nofjob = nofjob + 1
					for j in range(nPIs):
						if new_job[nofjob]:
							self.job_list.add_job( work_in_place = True )
						TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion = i+1, ketion = j+1 )
						task = self.xchem.task('TransformMatBlocks',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = TransformMatBlocks )
						self.job_list.add_task( task )
						nofjob = nofjob + 1
				for j in range(nPIs):
					if new_job[nofjob]:
						self.job_list.add_job( work_in_place = True )
					TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, ketion = j+1 )
					task = self.xchem.task('TransformMatBlocks',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = TransformMatBlocks )
					self.job_list.add_task( task )
					nofjob = nofjob + 1
			else:
				TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op )
				self.xchem.run('TransformMatBlocks',
				xchem_file_input=[SAE,Xchem],
				xchem_file_input_name=['SAE','Xchem'],
				xchem_cmd_line_input = TransformMatBlocks )
				for i in range(nPIs):
					TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion=i+1 )
					self.xchem.run('TransformMatBlocks',
					xchem_file_input=[SAE,Xchem],
					xchem_file_input_name=['SAE','Xchem'],
					xchem_cmd_line_input = TransformMatBlocks )
					for j in range(nPIs):
						TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion = i+1, ketion = j+1 )
						self.xchem.run('TransformMatBlocks',
						xchem_file_input_name=['SAE','Xchem'],
						xchem_cmd_line_input = TransformMatBlocks )
				for j in range(nPIs):
					TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, ketion = j+1 )
					self.xchem.run('TransformMatBlocks',
					xchem_file_input=[SAE,Xchem],
					xchem_file_input_name=['SAE','Xchem'],
					xchem_cmd_line_input = TransformMatBlocks )
			self.log_message('Diagonalizing the Hamiltonian.')
			DiagonalizeHamiltonian = DiagonalizeHamiltonian_input( self.input_dict, self.meta_dict )
			if not self.input_dict['justRot']:
				if self.input_dict['defer']:
					self.job_list.next_block()
					self.job_list.add_job( work_in_place = True )
					task = self.xchem.task('DiagonalizeHamiltonian',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = DiagonalizeHamiltonian )
					self.job_list.add_task( task )
				else:
					self.xchem.run('DiagonalizeHamiltonian',
					xchem_file_input=[SAE,Xchem],
					xchem_file_input_name=['SAE','Xchem'],
					xchem_cmd_line_input = DiagonalizeHamiltonian )
		if (self.input_dict['checkDep'] and not self.input_dict['defer'] and not self.input_dict['justRot']):
			f=open(os.path.join( data_dirs[str(qci)],'ham.eVals'),'r')
			qci_gs = float(f.readline().split()[1])
			f.close()
			f=open(os.path.join( self.DataDir,'CloseCoupling',str(self.input_dict['spinAugmentedStates']) +str(self.input_dict['brasym'])+'_'+str(self.input_dict['spinAugmentedStates']) +str(self.input_dict['brasym']),'BoxStates','H_Eigenvalues'),'r')
			f.readline()
			scat_gs = float(f.readline().split()[1])
			f.close()
			if (abs(qci_gs-scat_gs) > float(self.input_dict['checkThreshold'])): 
				raise XchemError('The Ground State computed with the boxstates module differs from the one obtained in the quantum chemistry part more than the checkThreshold. It seems that there are linear dependences, so you must increase the conditionNumber')
		if self.input_dict['defer'] and not self.input_dict['justRot']: self.job_list.next_block()
		op_not_H_S = self.input_dict['operators']
		if ('H' in op_not_H_S): op_not_H_S.remove('H') 
		if ('S' in op_not_H_S): op_not_H_S.remove('S') 
		if (self.input_dict['transBlocks']): 
			nofjob = 0
			if ( 'Dipole' in self.input_dict['operators']):
				new_job = submission_tools.need_new_job((nPIs +1)* (nPIs +1)*(len(self.input_dict['operators'])-1 + len(self.input_dict['gauges'])*len(self.input_dict['axes'])), int(os.environ['XCHEM_MAX_NUM_NODES']))
			else:
				new_job = submission_tools.need_new_job((nPIs +1)* (nPIs +1)*(len(self.input_dict['operators'])), int(os.environ['XCHEM_MAX_NUM_NODES']))
			for op in self.input_dict['operators']:
				if (op == 'CAP'): CAP = True
				if op == 'S': continue
				if op == 'H': continue
				if op == 'Dipole':
					for axes in self.input_dict['axes']:
						if (axes == 'x'): x = True
						if (axes == 'y'): y = True
						if (axes == 'z'): z = True
						for gauge in self.input_dict['gauges']:
							if (gauge == 'l'): DipLen = True
							if (gauge == 'v'): DipVel = True
							if self.input_dict['defer']:
								if new_job[nofjob]:
									self.job_list.add_job( work_in_place = True )
								TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge )
								task = self.xchem.task('TransformMatBlocks',
												xchem_file_input=[SAE,Xchem],
												xchem_file_input_name=['SAE','Xchem'],
												xchem_cmd_line_input = TransformMatBlocks )
								self.job_list.add_task( task )
								nofjob = nofjob + 1
								for i in range(nPIs):
									if new_job[nofjob]:
										self.job_list.add_job( work_in_place = True )
									TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, braion=i+1 )
									task = self.xchem.task('TransformMatBlocks',
													xchem_file_input=[SAE,Xchem],
													xchem_file_input_name=['SAE','Xchem'],
													xchem_cmd_line_input = TransformMatBlocks )
									self.job_list.add_task( task )
									nofjob = nofjob + 1
									for j in range(nPIs):
										if new_job[nofjob]:
											self.job_list.add_job( work_in_place = True )
										TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, braion = i+1, ketion = j+1 )
										task = self.xchem.task('TransformMatBlocks',
														xchem_file_input=[SAE,Xchem],
														xchem_file_input_name=['SAE','Xchem'],
														xchem_cmd_line_input = TransformMatBlocks )
										self.job_list.add_task( task )
										nofjob = nofjob + 1
								for j in range(nPIs):
									if new_job[nofjob]:
										self.job_list.add_job( work_in_place = True )
									TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, ketion = j+1 )
									task = self.xchem.task('TransformMatBlocks',
													xchem_file_input=[SAE,Xchem],
													xchem_file_input_name=['SAE','Xchem'],
													xchem_cmd_line_input = TransformMatBlocks )
									self.job_list.add_task( task )
									nofjob = nofjob + 1
							else:
								TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge )
								self.xchem.run('TransformMatBlocks',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = TransformMatBlocks )
								for i in range(nPIs):
									TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, braion=i+1 )
									self.xchem.run('TransformMatBlocks',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = TransformMatBlocks )
									for j in range(nPIs):
										TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, braion = i+1, ketion = j+1 )
										self.xchem.run('TransformMatBlocks',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = TransformMatBlocks )
								for j in range(nPIs):
									TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, axes=axes, gauge=gauge, ketion = j+1 )
									self.xchem.run('TransformMatBlocks',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = TransformMatBlocks )
								self.job_list.add_job( work_in_place = True )
				else:
					if self.input_dict['defer']:
						if new_job[nofjob]:
							self.job_list.add_job( work_in_place = True )
						TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op )
						task = self.xchem.task('TransformMatBlocks',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = TransformMatBlocks )
						self.job_list.add_task( task )
						nofjob = nofjob + 1
						for i in range(nPIs):
							if new_job[nofjob]:
								self.job_list.add_job( work_in_place = True )
							TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion=i+1 )
							task = self.xchem.task('TransformMatBlocks',
											xchem_file_input=[SAE,Xchem],
											xchem_file_input_name=['SAE','Xchem'],
											xchem_cmd_line_input = TransformMatBlocks )
							self.job_list.add_task( task )
							nofjob = nofjob + 1
							for j in range(nPIs):
								if new_job[nofjob]:
									self.job_list.add_job( work_in_place = True )
								TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion = i+1, ketion = j+1 )
								task = self.xchem.task('TransformMatBlocks',
												xchem_file_input=[SAE,Xchem],
												xchem_file_input_name=['SAE','Xchem'],
												xchem_cmd_line_input = TransformMatBlocks )
								self.job_list.add_task( task )
								nofjob = nofjob + 1
						for j in range(nPIs):
							if new_job[nofjob]:
								self.job_list.add_job( work_in_place = True )
							TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, ketion = j+1 )
							task = self.xchem.task('TransformMatBlocks',
											xchem_file_input=[SAE,Xchem],
											xchem_file_input_name=['SAE','Xchem'],
											xchem_cmd_line_input = TransformMatBlocks )
							self.job_list.add_task( task )
							nofjob = nofjob + 1
					else:
						TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op )
						self.xchem.run('TransformMatBlocks',
						xchem_file_input=[SAE,Xchem],
						xchem_file_input_name=['SAE','Xchem'],
						xchem_cmd_line_input = TransformMatBlocks )
						for i in range(nPIs):
							TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion=i+1 )
							self.xchem.run('TransformMatBlocks',
							xchem_file_input=[SAE,Xchem],
							xchem_file_input_name=['SAE','Xchem'],
							xchem_cmd_line_input = TransformMatBlocks )
							for j in range(nPIs):
								TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, braion = i+1, ketion = j+1 )
								self.xchem.run('TransformMatBlocks',
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = TransformMatBlocks )
						for j in range(nPIs):
							TransformMatBlocks = TransformMatBlocks_input( self.input_dict, self.meta_dict, op, ketion = j+1 )
							self.xchem.run('TransformMatBlocks',
							xchem_file_input=[SAE,Xchem],
							xchem_file_input_name=['SAE','Xchem'],
							xchem_cmd_line_input = TransformMatBlocks )
		else:
			self.log_message('Rotating the TDSE matrices and the information for the scattering part.')
			nofjob = 0
			if ( 'Dipole' in self.input_dict['operators']):
				new_job = submission_tools.need_new_job(len(self.input_dict['operators'])-1 + len(self.input_dict['gauges'])*len(self.input_dict['axes']), int(os.environ['XCHEM_MAX_NUM_NODES']))
			else:
				new_job = submission_tools.need_new_job(len(self.input_dict['operators']), int(os.environ['XCHEM_MAX_NUM_NODES']))
			for op in op_not_H_S:
				if (op == 'Dipole'):
					for gauge in self.input_dict['gauges']:
						for axes in self.input_dict['axes']:
							TransformOperators = TransformOperators_input( self.input_dict, self.meta_dict, op, nPIs, axes=axes, gauge=gauge )
							if self.input_dict['defer']:
								if new_job[nofjob]:
									self.job_list.add_job( work_in_place = True )
								task = self.xchem.task('TransformOperators',
												xchem_file_input=[SAE,Xchem],
												xchem_file_input_name=['SAE','Xchem'],
												xchem_cmd_line_input = TransformOperators )
								self.job_list.add_task( task )
								nofjob = nofjob + 1
							else:
								self.xchem.run('TransformOperators',
								xchem_file_input=[SAE,Xchem],
								xchem_file_input_name=['SAE','Xchem'],
								xchem_cmd_line_input = TransformOperators )
				else:
					TransformOperators = TransformOperators_input( self.input_dict, self.meta_dict, op, nPIs )
					if self.input_dict['defer']:
						if new_job[nofjob]:
							self.job_list.add_job( work_in_place = True )
						task = self.xchem.task('TransformOperators',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = TransformOperators )
						self.job_list.add_task( task )
						nofjob = nofjob + 1
					else:
						self.xchem.run('TransformOperators',
						xchem_file_input=[SAE,Xchem],
						xchem_file_input_name=['SAE','Xchem'],
						xchem_cmd_line_input = TransformOperators )
		if self.input_dict['defer']:
			self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid=uid )
		self.dump_meta_data()
