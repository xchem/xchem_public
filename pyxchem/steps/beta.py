#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
import xchem_keywords
from xchem_input_generators import *
from osutils import *
import itertools
import submission_tools

class Beta(XchemModule):
	def __init__(self, input_dict, info = False ):
		super().__init__(input_dict, 'beta', data_dir='ccm', info = info )
	
		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.propfile )
		self.add_keyword( xchem_keywords.time )
		self.add_keyword( xchem_keywords.defer )
		self.add_keyword( xchem_keywords.axes ) 
		self.add_keyword( xchem_keywords.gauges ) 
		self.add_keyword( xchem_keywords.scatLabel )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )

		try:
			self.load_previous_meta_data(['rdm_qci'])
			data_dirs = self.load_previous_meta_data(['qci','bsplines','ccm','boxstates','scatteringstates'])
		except:
			data_dirs = self.load_previous_meta_data(['rdm','bsplines','ccm','boxstates','scatteringstates'])

		Xchem = Xchem_input( self.input_dict )
		SAE = SAE_input( self.input_dict ) 

		self.log_message('Compute Beta Asymmetry Parameter')
		if len(self.input_dict['axes']) < 3:
			self.log_message("Beta Assymetry paramter can't be computed (x, y and z direction must be included)")
		else:
			if self.input_dict['defer']:
				self.job_list.add_job( work_in_place = True )
			for gauge in self.input_dict['gauges']:
			        self.log_message('Compute Beta Asymmetry Parameter')
			        ComputeBetaParameter = ComputeBetaParameter_input( self.input_dict, self.meta_dict, gauge=gauge )
			        if self.input_dict['defer']:
                			task = self.xchem.task('ComputeBetaParameter',
                							xchem_file_input=[SAE,Xchem],
                							xchem_file_input_name=['SAE','Xchem'],
                							xchem_cmd_line_input = ComputeBetaParameter )
                			self.job_list.add_task(task)
			        else:
                			self.xchem.run('ComputeBetaParameter',
                							xchem_file_input=[SAE,Xchem],
                							xchem_file_input_name=['SAE','Xchem'],
                							xchem_cmd_line_input = ComputeBetaParameter )
			        if self.input_dict['defer']:
                			self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
		self.dump_meta_data()
