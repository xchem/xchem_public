#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os, shutil
from xchem_abstract import *
import xchem_keywords
import subprocess
from xchem_error import *
from molcas_input_generators import *
from xchem_input_generators import *
from molcas_manager import MolcasManager
from xchem_manager import XchemManager
from parsing import lin_dep_kill_output
import osutils
from orbital import Orbital

class Orbitals(XchemModule):
	def __init__( self, input_dict, info = False):
		super().__init__(input_dict, 'orbitals', info = info )
		#
		self.add_keyword( xchem_keywords.orbitalFile  )
		self.add_keyword( xchem_keywords.inac  )
		self.add_keyword( xchem_keywords.ras1  )
		self.add_keyword( xchem_keywords.ras2  )
		self.add_keyword( xchem_keywords.ras3  )
		self.add_keyword( xchem_keywords.defer ) 
		self.add_keyword( xchem_keywords.linDepThr )	

		self._check_keywords()
		self.MOLCAS = MolcasManager( WorkDir= self.WorkDir, 
									 MOLCAS_OUTPUT = self.WorkDir)
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.WorkDir )
		osutils.mkdir( self.DataDir )
		os.chdir( self.WorkDir )

		data_dirs = self.load_previous_meta_data(['basis'])

		# PART1 Creates diffuse orbitals
		for f in [ self.SymProject + s for s in ['.OneInt','.RunFile']]:
			osutils.symlink( os.path.join(data_dirs['basis'], f), f)

		if self.meta_dict['group'].lower() == 'c1':
			self.log_message( 'Provided input orbitals do not require desymmetrization.') 
			shutil.copyfile( os.path.join(os.environ['XCHEM_SUBMIT'], self.input_dict['orbitalFile']), self.SymProject + '.DeSymOrb' )
		else:
			orbital = Orbital(os.path.join( os.environ['XCHEM_SUBMIT'], self.input_dict['orbitalFile']))
			orbital.reorder( self.input_dict )
			orbital.set_occ( 0.0 )
			orbital.write( 'INPORB' )
			self.log_message( 'Run Molcas to desymmetrize orbitals') 
			expbas = expbas_input( desy = True, noex=True )
			self.MOLCAS.run( expbas, Project = self.SymProject )

		self.log_message('Create diffuse orbitals from MCG and PCF bases')
		osutils.symlink( os.path.join(data_dirs['basis'], 'ov.int'), 'ov.int')
		block_info = block_info_input( self.input_dict, self.meta_dict, os.path.join(data_dirs['basis'], 'basis_block.raw.info') ) 
		linDepKill = linDepKill_input( self.input_dict, block_info, inporb = self.SymProject + '.DeSymOrb' ) 
		lin_dep_kill_output_parser =  lin_dep_kill_output.LinDepKillOutputParser()
		self.xchem.run('linDepKill',xchem_file_input=linDepKill, parsefct = lin_dep_kill_output_parser)
		shutil.move( 'orbs.sym', os.path.join(self.DataDir, 'orbs.sym'))
		shutil.move( self.SymProject + '.DeSymOrb', os.path.join(self.DataDir, self.SymProject + '.DeSymOrb'))
		self.meta_dict['number_diffuse_orbitals'] = lin_dep_kill_output_parser.number_diffuse_orbitals
		self.log_message('Number of diffuse orbitals = {}'.format(self.meta_dict['number_diffuse_orbitals']))
		
		self.log_message('Transform one electron basis integrals to orbital integrals')
		oneIntBas2Orb = oneIntBas2Orb_input( self.meta_dict )
		if self.input_dict['defer']:
			in_files = {data_dirs['basis'] : [r'(ov|dip.|vel.|kin|ham1E)\.int'], self.DataDir : [r'(mpcg\.bin)'] }
			self.job_list.add_job(in_files = in_files, out_files = {self.DataDir :[ r'\.orb\.int$']})
			task = self.xchem.task('oneIntBas2Orb_new', oneIntBas2Orb)
			self.job_list.add_task( task )
		else:
			self.xchem.run('oneIntBas2Orb_new',oneIntBas2Orb, in_files = {data_dirs['basis'] : [r'(ov|dip.|vel.|kin|ham1E)\.int']})
			osutils.mvregexls( self.WorkDir, self.DataDir, 'orb.int$') 
		shutil.move('basis_block.info', os.path.join( self.DataDir, 'basis_block.info'))
		shutil.copyfile('mpcg.bin', os.path.join( self.DataDir, 'mpcg.bin'))

		# PART2 Creates pqrs
		if self.input_dict['saveTwoElInt']:
			new_data_dirs = self.load_previous_meta_data(['integrals'])
			data_dirs.update(new_data_dirs)
			self.log_message('Transform two electron basis integrals to orbital integrals')
			self.log_message('compute [iq||ks] from [ij||kl] ',priority=2)
			ijkl2iqks = ijkl2iqks_input( self.input_dict, self.meta_dict )
			if self.input_dict['defer']:
				self.job_list.add_job(in_files = {data_dirs['integrals'] : [r'ham2E\.compressed\.int\..*','ij\.suffix'], self.DataDir : ['mpcg\.bin'] }, out_files = {self.WorkDir : [r'iqks\.int'] })
				task = self.xchem.task('ijkl2iqks', ijkl2iqks )
				self.job_list.add_task(task)
			else:
				self.xchem.run('ijkl2iqks', ijkl2iqks, in_files = {data_dirs['integrals'] : [r'ham2E\.compressed\.int\..*','ij\.suffix']} )
			#
			self.log_message('compute [ij||rs] from [ij||kl] ',priority=2)
			ijkl2ijrs = ijkl2ijrs_input( self.input_dict, self.meta_dict )
			if self.input_dict['defer']:
				self.job_list.add_job(in_files = {data_dirs['integrals'] : [r'ham2E\.compressed\.int\..*','ij\.suffix'], self.DataDir : ['mpcg\.bin'] }, out_files = {self.WorkDir : [r'ijrs\.int'] })	
				task = self.xchem.task('ijkl2ijrs', ijkl2ijrs )
				self.job_list.add_task(task)
			else:
				self.xchem.run('ijkl2ijrs', ijkl2ijrs)
			#
			if self.input_dict['defer']:
				self.job_list.next_block()
	
			self.log_message('compute [pq||rs] from [iq||ks] and [ij||rs]',priority=2)
			iqks2pqrs = iqks2pqrs_input( self.input_dict, self.meta_dict ) 
			if self.input_dict['defer']:
				self.job_list.add_job(in_files = { self.WorkDir : ['(iqks|ijrs)\.int'], self.DataDir : ['mpcg\.bin'] }, out_files = {self.DataDir : [r'pqrs\.int'] })	
				task = self.xchem.task('iqks2pqrs', iqks2pqrs)
				self.job_list.add_task(task)
			else:
				self.xchem.run('iqks2pqrs', iqks2pqrs)
				shutil.move( 'pqrs.int', os.path.join(self.DataDir, 'pqrs.int' ))
	
			if self.input_dict['defer']:
				self.job_list.next_block()
	
	
			if self.input_dict['defer']: self.job_list.dump( os.path.join( self.DataDir, 'orbitals.submission.json'), uid = uid )
			self.dump_meta_data()
		else:
			self.log_message('The transformation of two electron basis integrals to orbital integrals will be done in the integrals module')
			if self.input_dict['defer']: self.job_list.dump( os.path.join( self.DataDir, 'orbitals.submission.json'), uid = uid )
			self.dump_meta_data()
			
