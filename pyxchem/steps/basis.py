#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os
import xchem_abstract
import xchem_keywords
import osutils
from xchem_error import XchemError
from molcas_input_generators import *
from molcas_manager import MolcasManager
from xchem_manager import XchemManager
from xchem_input_generators import compressTwoElectronIntegrals_input
from parsing.symmetry import symmetry_parse, SymmetryParser
from parsing import petite_basis, x_irrep, diffuse_blocks
import shutil
import h5_extractors

class Basis(xchem_abstract.XchemModule):
	def __init__( self, input_dict, info = False ):
		super().__init__(input_dict, 'basis', info = info)

		self.add_keyword( xchem_keywords.gatewayFile )
		self.add_keyword( xchem_keywords.gateway_desym )
		self.add_keyword( xchem_keywords.sewardOptions )
		self.add_keyword( xchem_keywords.lMonocentric )
		self.add_keyword( xchem_keywords.kMonocentric )
		self.add_keyword( xchem_keywords.multighost )
		self.add_keyword( xchem_keywords.alpha0 )
		self.add_keyword( xchem_keywords.beta )
		self.add_keyword( xchem_keywords.nexp )
		self.add_keyword( xchem_keywords.twoexp )
		self.add_keyword( xchem_keywords.saveTwoElInt )

		self._check_keywords()
		self.MOLCAS = MolcasManager( WorkDir= self.WorkDir, 
									 MOLCAS_OUTPUT = self.WorkDir)
		self.xchem = XchemManager()

	def _module_specific_checks(self):
		if self.input_dict['multighost']:
			if self.input_dict['twoexp'] or not self.input_dict['saveTwoElInt']:
				raise XchemError('If multighost is True, twoexp must be False and saveTwoElInt must be True.')
		else:
			if not self.input_dict['saveTwoElInt'] and not self.input_dict['twoexp']:
				raise XchemError('Cannot set saveTwoElInt to False without also setting twoexp to True.')

	@xchem_abstract.prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		osutils.mkdir( self.WorkDir )
		os.chdir( self.WorkDir )

		gateway = open( os.path.join(os.environ['XCHEM_SUBMIT'],self.input_dict['gatewayFile']) ).read()
		symmetry_keyword = symmetry_parse( gateway )
		self.meta_dict['symmetry_keyword'] = symmetry_keyword

		seward = seward_input( oneo = False, mult = 6, options = self.input_dict['sewardOptions'] )
		guessorb = guessorb_input( )
		xchem_kill_guessorb = open(os.path.join(self.WorkDir,'xchem'),'w+')
		symmetry_parser = SymmetryParser()

		self.log_message('Run Molcas to create runfiles and extract information about PCG basis')
		self.MOLCAS.run( gateway + seward , Project = self.SymProject, parsefct = symmetry_parser )
		self.MOLCAS.run(  guessorb, Project = self.SymProject, parsefct = symmetry_parser, acceptable_rcs=['all'] )
		self.meta_dict.update( symmetry_parser.result )
		if self.meta_dict['symmetry_keyword'] is None:
			self.meta_dict['symmetry_keyword'] = symmetry_parser.result['generators']
		else:
			symmetry_parser.result['generators'] = self.meta_dict['symmetry_keyword']

		number_local_basis_functions = h5_extractors.number_basis_functions( self.SymProject + '.guessorb.h5' )
		total_nuclear_charge = h5_extractors.total_nuclear_charge( self.SymProject + '.guessorb.h5' )
		self.meta_dict['number_local_basis_functions'] = number_local_basis_functions
		self.meta_dict['total_nuclear_charge'] = total_nuclear_charge
		self.log_message('Number of local basis function = {}'.format(number_local_basis_functions) )
		self.log_message('Total nuclear charge = {}'.format(total_nuclear_charge) )

		for f in [ self.SymProject + s for s in ['.guessorb.h5','.OneInt','.OrdInt','.RunFile']]:
			shutil.copy( f, os.path.join(self.DataDir, f))


		self.log_message('Run Molcas to extract symmetry of MCG bases')
#VICENT MODIFICATION
		if self.input_dict['twoexp']:
			gateway = gateway_input( monocentric = True ,
									 input_dict = self.input_dict,
									 symmetry = self.meta_dict['symmetry_keyword'],
									 blockbasis = True )
		else:
#END OF VICENT MODIFICATION
			gateway = gateway_input( monocentric = True, input_dict = self.input_dict, symmetry = self.meta_dict['symmetry_keyword'] )
		x_irrep_parser = x_irrep.XIrrepParser()
		self.MOLCAS.run( gateway, Project = self.SymProject, parsefct =x_irrep_parser )
		self.meta_dict['x_bas_sym'] = x_irrep_parser.result
		
#VICENT MODIFICATION
		if self.input_dict['twoexp']:
			gateway = gateway_input( self.SymProject + '.guessorb.h5', 
									 sdip = True, 
									 monocentric = True ,
									 input_dict = self.input_dict,
									 blockbasis = True )
		else:
#END OF VICENT MODIFICATION
			gateway = gateway_input( self.SymProject + '.guessorb.h5', 
									 sdip = True, 
									 monocentric = True ,
									 input_dict = self.input_dict)
		seward = seward_input( oneo = True, options = self.input_dict['sewardOptions'] )
		guessorb = guessorb_input( sthr = 10.0, tthr = 10.0 )
#VICENT MODIFICATION
		if self.input_dict['twoexp']:
			parser = diffuse_blocks.DiffusenBlockParser('basis_block.raw.info')
			self.log_message('Run Molcas to compute one electron integrals for PCG and MCG bases')
			self.MOLCAS.run( gateway + seward,
							 Project = self.DesymProject,
							 parsefct = parser,
							 dump_integrals1 = True)
			self.MOLCAS.run( guessorb,
							 Project = self.DesymProject,
							 parsefct = parser,
							 acceptable_rcs = ["all"])

			for f in [ self.DesymProject + '.guessorb.h5', 'basis_block.raw.info' ]:
				shutil.move(f, os.path.join(self.DataDir,f))
		else:
#END OF VICENT MODIFICATION
			parser = petite_basis.PetiteBasisParser()
			self.log_message('Run Molcas to compute one electron integrals for PCG and MCG bases')
			self.MOLCAS.run( gateway + seward,
							 Project = self.DesymProject,
							 parsefct = parser,
							 dump_integrals1 = True)
			self.MOLCAS.run( guessorb,
							 Project = self.DesymProject,
							 parsefct = parser,
							 acceptable_rcs = ["all"] )
			parser.write( 'petiteBasis' )
			compress= compressTwoElectronIntegrals_input( self.input_dict, dontCompress = True )
			self.xchem.run('compressTwoElectronIntegrals',compress)

			for f in ['petiteBasis', self.DesymProject + '.guessorb.h5', 'basis_block.raw.info' ]:
				shutil.move(f, os.path.join(self.DataDir,f))
		for f in [ '{}.int'.format(s) for s in ['dip1','dip2','dip3','vel1','vel2','vel3','ham1E','ov','kin'] ]:
			shutil.move(f, os.path.join(self.DataDir,f))

		self.dump_meta_data()
