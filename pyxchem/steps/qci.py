#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os, shutil, sys
from xchem_abstract import *
import subprocess
import xchem_keywords
from xchem_error import *
from molcas_input_generators import *
from xchem_input_generators import *
from molcas_manager import MolcasManager
from xchem_manager import XchemManager
from parsing import guga,multipole,lin_dep_kill_output
import h5_extractors
import osutils
from basis_set import BasisSet, ghost_basis_creator, multighost_basis_creator
import itertools
import submission_tools
from determinant import Determinant

class Qci(XchemModule):
	def __init__( self, input_dict, info = False):
		super().__init__(input_dict, 'qci' , info = info)
		#
		self.add_keyword( xchem_keywords.defer ) 

		self._check_keywords()
		self.MOLCAS = MolcasManager( WorkDir= self.WorkDir, 
									 MOLCAS_OUTPUT = self.WorkDir)
		self.xchem = XchemManager()


	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.WorkDir )
		osutils.mkdir( self.DataDir )
		os.chdir( self.WorkDir )
		try:
			data_dirs = self.load_previous_meta_data(['basis','orbitals','integrals','rdm'])
		except:
			raise XchemError('The qci module can only be used after the rdm module.')
		operators = [('ham1E','T', '1.0', None ), ('ov','T', nelectrons(self.input_dict) + 1 , None), ('kin','T','1.0', None)]
		for i in range(1,4):
			operators.append( ('dip{}'.format(i),'T','1.0', self.meta_dict['permanent_dipole'][i-1] ))	
			operators.append( ('vel{}'.format(i),'F','1.0', None ))	


		osutils.lnregexls( data_dirs['orbitals'], self.WorkDir, 'orb.int$')
		osutils.lnregexls( data_dirs['rdm'], self.WorkDir, 'trd*')
		osutils.cpregexls( data_dirs['rdm'], self.DataDir, 'En_*')
		osutils.cpregexls( data_dirs['rdm'], self.DataDir, 'multipoles.dat')
		osutils.cpregexls( data_dirs['rdm'], self.DataDir, 'velocities.dat')
		if self.input_dict['saveTwoElInt']:
			for f in ['pqrs.int', 'orbs.sym']:
				osutils.symlink( os.path.join( data_dirs['orbitals'], f) , f)
		else:
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'orbs.sym') , 'orbs.sym')
				osutils.symlink( os.path.join( data_dirs['integrals'], 'pqrs.int') , 'pqrs.int')
		self.log_message('Compute operator matrix elements of two electron hamiltonian from orbital integrals and two electron reduced density matrix')
		twoIntOrb2OperatorMatrix = twoIntOrb2OperatorMatrix_input( self.input_dict, self.meta_dict )
		if self.input_dict['defer']:
			if self.input_dict['saveTwoElInt']:
				in_files = {
					data_dirs['rdm'] : [r'trd1',r'trd2'],
					self.WorkDir : [r'nuclearRepulsion',r'.*bas','lk\.dat'],
					data_dirs['orbitals'] : [r'pqrs\.int', r'\.orb\.int$', r'basis_block\.info', r'orbs\.sym',r'mpcg\.bin']
					}
				out_files = {
					self.DataDir :[ r'(ov|Ham|dip.|vel.|kin)_\d{3}-\d{3}\.out','(symmetry|interface.*|ham\.eVals)']
					}
			else:
				in_files = {
					data_dirs['rdm'] : [r'trd1',r'trd2'],
					self.WorkDir : [r'nuclearRepulsion',r'.*bas','lk\.dat'],
					data_dirs['integrals'] : [r'pqrs\.int'], data_dirs['orbitals'] : [ r'\.orb\.int$', r'basis_block\.info', r'orbs\.sym',r'mpcg\.bin']
					}
				out_files = {
					self.DataDir :[ r'(ov|Ham|dip.|vel.|kin)_\d{3}-\d{3}\.out','(symmetry|interface.*|ham\.eVals)']
					}
			self.job_list.add_job( in_files = in_files, out_files = out_files )
			task = self.xchem.task('twoIntOrb2OperatorMatrix_new',  twoIntOrb2OperatorMatrix)
			self.job_list.add_task( task )
		else:
			self.xchem.run('twoIntOrb2OperatorMatrix_new',  twoIntOrb2OperatorMatrix)

		
		self.log_message('Compute operator matrix elements of one electron operators from orbital integrals and two electron reduced density matrix')
		for name, hermitian, scale_factor, dipole in operators :
			self.log_message('Consider operator: {}'.format(name),priority=2)
			oneIntOrb2OperatorMatrix = oneIntOrb2OperatorMatrix_input( self.input_dict, name, self.meta_dict )
			if self.input_dict['defer']:
				task = self.xchem.task('oneIntOrb2OperatorMatrix_new',  
							    xchem_file_input = oneIntOrb2OperatorMatrix, 
								xchem_cmd_line_input = '{} {}'.format(scale_factor, hermitian))
				self.job_list.add_task( task ) 
			else:
				task = self.xchem.run('oneIntOrb2OperatorMatrix_new',  
							    xchem_file_input = oneIntOrb2OperatorMatrix, 
								xchem_cmd_line_input = '{} {}'.format(scale_factor, hermitian))
			if dipole!=None:
				subtractDiagonal = subtractDiagonal_input( dipole, name )
				if self.input_dict['defer']:
					task = self.xchem.task('subtractDiagonal',subtractDiagonal)
					self.job_list.add_task( task )
				else:
					self.xchem.run('subtractDiagonal',subtractDiagonal)

		self.log_message('Compute complete Hamiltonian')
		sumOneIntTwoIntNuclear = sumOneIntTwoIntNuclear_input( self.meta_dict )
		if self.input_dict['defer']:
			task = self.xchem.task('sumOneIntTwoIntNuclear', sumOneIntTwoIntNuclear)
			self.job_list.add_task( task )
		else:
			self.xchem.run('sumOneIntTwoIntNuclear', sumOneIntTwoIntNuclear)
		operators[0] = ('Ham',None,None,None )
		self.out_files = []
		self.log_message('Break up total operator matrices into blocks according to symmetry')
		symSplit = symSplit_input(self.input_dict, self.meta_dict)
		for name,_,_,_ in operators:
			self.log_message('Consider operator: {}'.format(name),priority=2)
			if name == 'ov':
				xchem_cmd_line_input = '{}.state.int T'.format(name)
			else:
				xchem_cmd_line_input = '{}.state.int'.format(name)
			if self.input_dict['defer']:
				task = self.xchem.task('symSplit',xchem_file_input = symSplit, xchem_cmd_line_input = xchem_cmd_line_input)
				self.job_list.add_task( task )
			else:
				self.xchem.run('symSplit',xchem_file_input = symSplit, xchem_cmd_line_input = xchem_cmd_line_input)
			self.add_out_files( '{}_*-*.out'.format(name)  )
		self.add_out_files('symmetry')

		self.log_message('Create interface(s) for computations with BSplines')
#VICENT MODIFICATION
		if self.input_dict['multighost']:
			ghost_bases = multighost_basis_creator( self.input_dict )
		else:
			ghost_bases = ghost_basis_creator( self.input_dict )
#END OF VICENT MODIFICATION
		for basis in ghost_bases:
			basis.writeGaussians()
		GABS_interface_sym = GABS_interface_sym_input(self.input_dict, self.meta_dict)
		for iirrep in range(1,len(self.meta_dict['irreps']) +1 ):
			if  self.input_dict['defer']:
				task = self.xchem.task( 'GABS_interface_sym', 
								xchem_file_input = GABS_interface_sym,
								xchem_cmd_line_input= str(iirrep) )
				self.job_list.add_task( task )
			else:
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'mpcg.bin'), 'mpcg.bin')
				osutils.symlink( os.path.join( data_dirs['orbitals'], 'basis_block.info'), 'basis_block.info')
				self.xchem.run( 'GABS_interface_sym', 
								xchem_file_input = GABS_interface_sym,
								xchem_cmd_line_input= str(iirrep) )
		self.add_out_files( 'interface*' )

		self.log_message('Diagonalize Hamiltonian')
		computeEvals = computeEvals_input()
		if  self.input_dict['defer']:
			task = self.xchem.task('computeEvals',computeEvals)
			self.job_list.add_task( task )
		else:
			self.xchem.run('computeEvals',computeEvals)
		self.add_out_files( 'ham.eVals' )
		self.mv_out_files()
		if self.input_dict['defer']: 
			self.job_list.dump( os.path.join( self.DataDir, 'qci.submission.json'), uid = uid )
		self.dump_meta_data()
