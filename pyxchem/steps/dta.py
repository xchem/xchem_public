#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_abstract import *
from xchem_manager import XchemManager
import xchem_keywords
from xchem_input_generators import *
from osutils import *
import itertools

class Dta(XchemModule):
	def __init__(self, input_dict, info = False ):
		super().__init__(input_dict, 'dta', data_dir='ccm', info = info )
	
		self.add_keyword( xchem_keywords.closeCouplingChannels )
		self.add_keyword( xchem_keywords.brasym )
		self.add_keyword( xchem_keywords.axes ) 
		self.add_keyword( xchem_keywords.scatLabel ) 
		self.add_keyword( xchem_keywords.gauges ) 
		self.add_keyword( xchem_keywords.cm ) 
		self.add_keyword( xchem_keywords.boxi ) 
		self.add_keyword( xchem_keywords.defer )
		self.add_keyword( xchem_keywords.trans )

		self._check_keywords()
		self.xchem = XchemManager()

	@prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.DataDir )
		os.chdir( self.DataDir )
		if (os.environ['rmLock']):
			for dirpath, dirs, files in os.walk(self.DataDir):
				for fname in files:
					if (fname.endswith('.lock')):
						file2rm = os.path.join(dirpath,fname)
						os.remove( os.path.join( self.DataDir, file2rm ) )
		if self.input_dict['cm'] != 'spec':
			if self.input_dict['trans'] == 'cc':
				data_dirs = self.load_previous_meta_data(['qci','bsplines','ccm','scatteringstates'])
			elif self.input_dict['trans'] == 'bb':
				data_dirs = self.load_previous_meta_data(['qci','bsplines','ccm','boxstates'])
			else:
				data_dirs = self.load_previous_meta_data(['qci','bsplines','ccm','boxstates','scatteringstates'])
	
			Xchem = Xchem_input( self.input_dict )
			SAE = SAE_input( self.input_dict ) 
	
			self.log_message('Compute Dipole Transition Amplitdutes')
			if self.input_dict['defer']:
				self.job_list.add_job( work_in_place = True )
			for axis,gauge in itertools.product( self.input_dict['axes'], self.input_dict['gauges']):
				ComputeDipoleTransitionAmplitudes = ComputeDipoleTransitionAmplitudes_input ( self.input_dict, self.meta_dict,axis=axis, gauge=gauge )
				ComputeDipoleTransitionAmplitudes_concatenate = ComputeDipoleTransitionAmplitudes_input( self.input_dict ,self.meta_dict,axis=axis, gauge=gauge, concatenate = True )
				if self.input_dict['defer']:
					task = self.xchem.task('ComputeDipoleTransitionAmplitudes',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = ComputeDipoleTransitionAmplitudes )
					self.job_list.add_task(task)
					task = self.xchem.task('ComputeDipoleTransitionAmplitudes',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = ComputeDipoleTransitionAmplitudes_concatenate )
					self.job_list.add_task(task)
				else:
					self.xchem.run('ComputeDipoleTransitionAmplitudes',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = ComputeDipoleTransitionAmplitudes )
					self.xchem.run('ComputeDipoleTransitionAmplitudes',
									xchem_file_input=[SAE,Xchem],
									xchem_file_input_name=['SAE','Xchem'],
									xchem_cmd_line_input = ComputeDipoleTransitionAmplitudes_concatenate )
	
			if self.input_dict['defer']:
				self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
			self.dump_meta_data()
		else:
			data_dirs = self.load_previous_meta_data(['qci','bsplines','ccm','boxstates','scatteringstates'])
	
			Xchem = Xchem_input( self.input_dict )
			SAE = SAE_input( self.input_dict ) 
	
			self.log_message('Compute Dipole Transition Amplitdutes')
			if self.input_dict['defer']:
				self.job_list.add_job( work_in_place = True )
			if (self.input_dict['trans'] == 'cc'):
				for axis,gauge in itertools.product( self.input_dict['axes'], self.input_dict['gauges']):
					ComputeConConDipoleTransitions = ComputeConConDipoleTransitions_input( self.input_dict, self.meta_dict,axis=axis, gauge=gauge )
					if self.input_dict['defer']:
						task = self.xchem.task('ComputeConConDipoleTransitions',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = ComputeConConDipoleTransitions )
						self.job_list.add_task(task)
					else:
						self.xchem.run('ComputeConConDipoleTransitions',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = ComputeConConDipoleTransitions )
			else:
				for axis,gauge in itertools.product( self.input_dict['axes'], self.input_dict['gauges']):
					ComputePhotoelectronSpectrumDTA = ComputePhotoelectronSpectrumDTA_input( self.input_dict, self.meta_dict,axis=axis, gauge=gauge )
					if self.input_dict['defer']:
						task = self.xchem.task('ComputeProjections',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = ComputePhotoelectronSpectrumDTA )
						self.job_list.add_task(task)
					else:
						self.xchem.run('ComputeProjections',
										xchem_file_input=[SAE,Xchem],
										xchem_file_input_name=['SAE','Xchem'],
										xchem_cmd_line_input = ComputePhotoelectronSpectrumDTA )
			if self.input_dict['defer']:
				self.job_list.dump( os.path.join( self.DataDir, '{}.submission.json'.format(self.name)), uid = uid )
			self.dump_meta_data()
