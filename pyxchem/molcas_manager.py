#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

# Method to run molcas from and variably
# save, discard or parse output

import subprocess
import os,sys
import osutils
from collections import deque
from xchem_error import *

class MolcasManager:
	def __init__(self, input_cache = '.molcas_input', WorkDir = None, MOLCAS_OUTPUT = None, MOLCAS_PRINT = 3):
		self._input_cache = input_cache
		if WorkDir:
			self._WorkDir = WorkDir
			os.environ['WorkDir'] = WorkDir
		if MOLCAS_OUTPUT:
			self._MOLCAS_OUTPUT = MOLCAS_OUTPUT
			os.environ['MOLCAS_OUTPUT'] = MOLCAS_OUTPUT
		self._MOLCAS_PRINT = MOLCAS_PRINT
		os.environ['MOLCAS_PRINT'] = str(MOLCAS_PRINT)

	@property
	def WorkDir( self ):
		return self._WorkDir
	@WorkDir.setter
	def WorkDir( self, value ):
		os.environ['WorkDir'] = value
		self._WorkDir = value

	@property
	def MOLCAS_OUTPUT( self ):
		return self._WorkDir
	@MOLCAS_OUTPUT.setter
	def MOLCAS_OUTPUT( self, value ):
		os.environ['MOLCAS_OUTPUT'] = value
		self._MOLCAS_OUTPUT = value

	@property
	def MOLCAS_PRINT( self ):
		return self._WorkDir
	@MOLCAS_PRINT.setter
	def MOLCAS_PRINT( self, value ):
		os.environ['MOLCAS_OUTPUT'] = value
		self._MOLCAS_OUTPUT = value

	def task(self, molcas_input, Project = 'xchem', dump_integrals = False, dump_integrals1 = False, shell_limit = None, parallel = False ):	
		task_dict = {}
		task_dict['Project'] = Project
		task_dict['package'] = 'molcas'
		task_dict['molcas_input'] = molcas_input
		task_dict['dump_integrals'] = dump_integrals
		task_dict['dump_integrals1'] = dump_integrals1
		task_dict['shell_limit'] = shell_limit
		task_dict['parallel'] = parallel
		return(task_dict)

#VICENT MODIFICATION				
	def run(self, 
#END OF VICENT MODIFICATION				
			molcas_input,
			Project = 'xchem',
			output_file = None,
			xchem_print = None,
			parsefct = None,
			dump_integrals = False,
			dump_integrals1 = False,
			shell_limit = None,
			guga_only = False,
			wait= True,
			acceptable_rcs = [0]): 		

		# Flush stdout to make sure output appears in the correct order.
		sys.stdout.flush()

		os.environ['Project'] = Project
		handle = open( self._input_cache , mode = 'w' ) 
		handle.write(molcas_input)
		handle.close()
		output_buffer = deque(maxlen=50)

		if dump_integrals or dump_integrals1:
			if dump_integrals:
				osutils.echo( os.path.join(self.WorkDir,'prIntegral') ,content='1')
			if dump_integrals1:
				osutils.echo( os.path.join(self.WorkDir,'prIntegral') ,content='1 \n 1')
		else:
			osutils.rm( os.path.join(self.WorkDir,'prIntegral'))


		if guga_only:
			osutils.echo( os.path.join(self.WorkDir,'gugaOnly'),content='1')
		else:
			osutils.rm( os.path.join(self.WorkDir,'gugaOnly'))

#VICENT MODIFICATION				dd
		if (shell_limit):
			#nshell,ishell,jshell = shell_limit
			#if ishell == 0 or jshell == 0:
			if 'exp1' in shell_limit and 'exp2' in shell_limit:
				lvals = shell_limit['lMonocentric']
				kvals = shell_limit['kMonocentric']
				lkvals = str(len(lvals)) + " " + str(max(lvals)) + "\n"
				LMax = 0
				for i in range(len(shell_limit['lMonocentric'])):
					if (lvals[i] + 2*kvals[i]) > LMax:
						LMax = lvals[i] + 2*kvals[i]
					lkvals += str(lvals[i]) + " " + str(kvals[i]) + "\n"
				osutils.echo( os.path.join(self.WorkDir,'lastshell.dat'),content=str(shell_limit['lastshell']) + "\n" + str(-shell_limit['exp1']) + " " + str(-shell_limit['exp2']) + "\n" + str(LMax) + "\n" +str(shell_limit['nexp']) + "\n" + str(lkvals))
			#else:
			elif 'shell1' in shell_limit and 'shell2' in shell_limit:
				osutils.echo( os.path.join(self.WorkDir,'lastshell.dat'),content=str(shell_limit['lastshell']) + "\n" + str(shell_limit['shell1']) + " " + str(shell_limit['shell2']) )
#END OF VICENT MODIFICATION	

		else: 
			osutils.rm( os.path.join(self.WorkDir,'lastshell.dat'))

		p = subprocess.Popen(['pymolcas', self._input_cache], stdout=subprocess.PIPE )
		for line in iter(p.stdout.readline, b''):	
			preprocessed_line = line.decode('UTF-8')
			output_buffer.append( preprocessed_line.strip('\n'))
			if parsefct:
				parsefct(preprocessed_line.strip())
			if os.environ['XCHEM_PRINT'] == '2':
				print(preprocessed_line.strip('\n'))
		if wait:
			rc = p.wait()         
			if rc not in acceptable_rcs and acceptable_rcs[0] != "all":
				print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
				print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
				print('LAST 50 LINES OF OUTPUT OF MOLCAS CALC THAT CAUSED CRASH')
				print('TO SEE MORE RERUN WITH xchem_print=2')
				print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
				print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
				print('\n'.join(output_buffer))
				print("Out rc ",rc,acceptable_rcs)
				print(os.system("df -h; du -h"))
				raise XchemMolcasError()
			return(rc)
		else:
			return(p)
