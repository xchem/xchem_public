#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import glob,os
import osutils
import sys
import subprocess, shlex
import time
from xchem_error import *

class Job:
	header = '#!/bin/bash'
	#export necessary environment variables
	def __init__(self, in_files = [], out_files = [], order = 0, indentifier = None, tasks=None, work_in_place = False ):
		self.order = order
		self.identifier = indentifier
		self.in_files = in_files
		self.out_files = out_files
		self.work_in_place = work_in_place
		if tasks:
			self.tasks = tasks
		else:
			self.tasks = []
	def add_task(self, task ):
		self.tasks.append( task ) 
	def get_dict(self):
		dct = { 
			'order':self.order,
			'indentifier':self.identifier,
			'tasks':self.tasks,
			'in_files':self.in_files,
			'out_files':self.out_files,
			'work_in_place':self.work_in_place
		}
		return dct
	#Move this function to xchem_abstract and pass it just the Job, that way all the intersting variables will be around

class JobList:
	def __init__(self, file_name = None ):
		self.jobs = []
		self.order = 0
		if file_name:
			self._load( file_name )
		self.nsent=0
	
	def next_block(self):
		self.order += 1
	
	def add_task(self, *args, **kwargs ):
		self.jobs[-1].add_task( *args, **kwargs)	

	def add_job(self,  in_files = [], out_files = [], indentifier = None, tasks=None, work_in_place = False ):	
		self.jobs.append( Job( in_files =in_files, out_files = out_files, indentifier = indentifier, tasks=tasks, order = self.order, work_in_place = work_in_place ) )

	def dump(self, file_name, uid):
		lst = []
		enumerated_file_name = file_name + '.{}'.format(uid)
		for job in self.jobs:
			lst.append( job.get_dict() )
		osutils.write_data_to_json( enumerated_file_name, lst )

	def _load(self, file_name):
		lst = osutils.read_data_from_json( file_name )
		for job_dict in lst:
			self.jobs.append(Job(**job_dict))

def launch( job_string, flags = ' ', depends_on_jobids = [], script_file_name = '.xchemscript', only_inputs=False):
	with open( script_file_name, 'w') as f:
		f.write( job_string )
	f=open("tosend.sh","a")
	if depends_on_jobids:
# jgv flags += ' --dependency=afterok:{}'.format( ':'.join(list(map(str,depends_on_jobids))))	
		flags += ' --dependency=afterany:{}'.format( ':'.join(list(map(str,depends_on_jobids))))	
	print('Submit script = {}, with flags = {}'.format(script_file_name, flags))
	if only_inputs:
		if depends_on_jobids==[1]:
			f.write("wait\n")
		f.write(script_file_name+"\n")
	else:
		cmd = 'sbatch {} {} '.format(flags, script_file_name)
		print(cmd)
		cmd = shlex.split( cmd )
	if only_inputs:
		f.close()
		return(0)
	else:
		p = subprocess.Popen( cmd, stdout=subprocess.PIPE )
		jobid = -1
		for line in iter(p.stdout.readline, b''):
			if len( line.decode('ascii').split()) > 3:
				if line.decode('ascii').split()[-4] == 'Submitted':
					try:
						jobid = int(line.decode('ascii').split()[-1])
					except:
						raise XchemInternalError('Could not extract jobid in job submission. Necessary to handle dependencies')
					break
		if jobid == -1:
			raise XchemInternalError('Could not extract jobid in job submission. Necessary to handle dependencies')
		rc = p.wait()
		if rc == 0:
			print("Submitted job {}".format(jobid))
		else:
			raise XchemInternalError('Error in jobsubmission')
		return(jobid)

class ProcList:
	def __init__(self):
		self._list = []
	def add(self, p):
		self._list.append( p )
	def waitForFreeProc(self, ncpu, dead_time = 1.0 ):
		ncpu = int(ncpu)
		procs_done = []
		if len(self._list) >= ncpu:
			while len(procs_done) == 0:
				procs_done = [ (i,p.poll()) for i,p in enumerate(self._list) if p.poll() is not None ]
				time.sleep(dead_time)	
			for i,rc in reversed(procs_done):
				if rc != 0:
					raise XchemError()
				del self._list[i]

	def waitForAllProcs( self, dead_time = 1.0 ):
		procs_running= [1]
		while len(procs_running) > 0 :
			procs_running = [ p for p in self._list if p.poll() is None ]
			rcs = [ p.poll() for p in self._list if p.poll() is not None ]
			if any( rc != 0 for rc in rcs ):
				raise XchemError()
			time.sleep(dead_time)	
		return( [] )

	def num_procs(self):
		return(len(self._list))

def split_seq(n, size):
    seq = range(n)
    newseq = []
    splitsize = 1.0/size*len(seq)
    for i in range(size):
        newseq.append([int(round(i*splitsize)),int(round((i+1)*splitsize))-1])
    return newseq

def which_job(limit_seq,task):
    result = [ i for i,limits in enumerate(limit_seq) if task >= limits[0] and task <= limits[1]]
    return(result[0])

def need_new_job(tasks,nodes):
    out = []
    limit_seq = split_seq(tasks, nodes)
    prev_job = -1
    for i in range(tasks):  
        job = which_job(limit_seq,i)
        if job == prev_job:
            out.append(False)
        else:
            out.append(True)
            prev_job = job 
    return(out)
