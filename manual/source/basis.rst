.. _basis:

******
&basis
******

This module has two main purposes: on the one hand it computes the one electron integrals between all relevant Gaussian basis functions (including both monocentric and polycentric Gaussians). On the other hand it extracts a number of parameters that are required for subsequent modules. The computational effort of the module is small relative to the an entire computation.

Parameters
##########

*  **gatewayFile** One of the two files that need to be prepared prior to the execution of XCHEM (apart from the input file itself. The other is the orbital file required for the orbitals module). Specifies the name of the file containing the polycentric bases to be used (such as for example cc-pVQZ) and the nuclear geometry. The file needs to be a valid input to the Gateway module of Molcas (see Molcas documentation for more details). 
*  **lMonocentric** Specifies the maximum value for :math:`l` to be used in the monocentric Gaussian basis set. If one value is given all values :math:`0\cdots l` will be included with equal maximum values for :math:`k` (see kMonocentric keyword). If more than one value is given, the user may specify the maximum value of :math:`k` for each :math:`l`. If providing more than one value the values must be consecutive and starting at :math:`0`.
*  **kMonocentric** Specifies the maximum value for :math:`k` to be used in the monocentric Gaussian basis set. If one value is given it will be applied used for all value of :math:`l` small or equal to the provided kMonocentric. Otherwise each field defines the maximum value of :math:`k` for the corresponding field provided to lMonocentric.
*  **nexp** Number of exponents to use in the even tempered monocentric basis. Default: nexp=22
*  **alpha0** The value of :math:`\alpha_0` to be used in defining the monocentric Gaussian exponents via the formula :math:`\alpha_i = \alpha_0 \beta^{i-1}`. Default: alpha0 = 0.01
*  **beta** The value of *beta* to be used in defining the monocentric Gaussian exponents via the formula :math:`\alpha_i = \alpha_0 \beta^{i-1}`. Default: beta=1.46
*  **multighost** If set to true different optimised basis sets, based on the default values for nexp, alpha0 and beta, are used for monocentric Gaussian basis sets of different angular momenta. Default: True
*  **sewardOptions** May be used to provide one or several options to be provided to Molcas' Seward module (used to calculate the Gaussian integrals).

