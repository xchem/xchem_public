.. _dta:

****
&dta
****

Parameters
##########

This module computes the dipole transition amplitudes between bound and scattering states. They may be used directly to compute the (partial) photo ionisation cross sections, or in a time dependent calculation, to correctly describe the impact of a laser field. 

* **closeCouplingChannels**  Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the photo ionisation cross section. 
* **trans** Choose which kind of transition amplitudes to calculation. Valid options: cb (scattering to bound), bc (bound to scattering), bb (bound to bound), and cc (scattering to scattering). Default= vb 
* **boxi** If trans is set up to include bound states, defines which bound state (in order of increasing eigen energies) to use in calculating the transition amplitudes. (If trans=bb, the transition amplitudes between the chooses one and all other box eigen states will be computed). Default: 1
* **braemin** If trans=cb, defines the energy of the least energetic, available scattering states to be included in the calculation. :math:`\mathrm{Default}=-\infty`.
* **braemax** If trans=cb, defines the energy of the most energetic, available scattering states to be included in the calculation. :math:`\mathrm{Default}=\infty`. 
* **ketemin** If trans=bc, defines the energy of the least energetic, available scattering states to be included in the calculation. :math:`\mathrm{Default}=-\infty`.
* **ketemax** If trans=bc, defines the energy of the most energetic, available scattering states to be included in the calculation. :math:`\mathrm{Default}=\infty`. 
* **gauges** Analogous to the same keyword of the module ccm. 
* **axes** Analogous to the same keyword of the module ccm.
* **cm** If trans is set up to include scattering states, select the method that was used to compute scattering states to be used in calculating the transition amplitudes. Default = hsle.
* **scattConditionNumber** If trans is set up to include scattering states, select the threshold that was used to compute scattering states to be used in calculating the transition amplitudes. Default= :math:`10^{-10}`.
* **sinasymp**
* **operators**
* **loadLocStates**
* **numBsDropEnding**
* **conditionBsThreshold**
* **numBsDropBeginning**
* **conditionBsMethod**
* **cond**
* **brasym**

