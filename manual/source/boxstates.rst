.. _boxstates:

**********
&boxstates
**********

Request the diagonalisation of the close coupling (quenched) Hamiltonian. 

Parameters
##########

* **closeCouplingChannels** Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the life times and or energies of the eigen states. 
* **loadLocStates** If true the source states are included in the diagonalisation. Default: True.
* **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
* **brasym**
* **conditionNumber**
* **numBsDropBeginning**
* **conditionBsThreshold**
* **numBsDropEnding**
* **cond**
* **conditionBsMethod**
