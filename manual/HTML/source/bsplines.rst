.. _bsplines:

*********
&bsplines
*********

This module calculates the integrals between B-splines and monocentric Gaussian basis functions. Furthermore it converts the results from the rdm modules to be treatable as a scattering problem. The parameters of this module must be chose with care as all subsequent modules depend on them, making changes potentially costly. 

Parameters
##########

* **rmin** Defines the radius in atomic units from the origin at which the B-spline part of the GABs basis starts. This should be chose so that the polycentric Gaussian  orbitals used to describe the parent ions and source states only protrude beyond it to a negligible extent.
* **rmax** Defines the radius in atomic units at which the B-spline basis ends. In order to obtain correct scattering states, it should be ensured that at least a few oscillations of the electron being ejected can be represented in the resulting box. 
* **numberNodes** Defines the number of nodes used to create the B-spline basis. A reasonable first guess is two nodes per atomic unit, though higher B-spline densities may be necessary, depending on the kinetic energy of the ejected electron (larger kinetic energies require larger densities). 
* **closeCouplingChannels** Defines the close coupling expansion (i.e. ejected electrons of which angular momenta should "leave behind" which parent ions). The format for this is :math:`\mathrm{ccm}=\Phi_1 (l^1_1\;m^1_1,\cdots,l^1_{n_1}\;m^1_{n_1});\cdots;\Phi_i (l^i_1\;m^i_1,\cdots,l^i_{n_i}\;m^i_{n_i})`, where :math:`\Phi_i` is the :math:`i^{\mathrm{th}}` parent ion defined via ciroParentStates. For example :math:`\mathrm{ccm}=1 (0\;0,2\;0); 2(2\;0),3 (1\;-1,1\;1); 4(1\;-1, 1\;1)` in combination with :math:`\mathrm{ciroParentStates}=2\;2` (used in the rdm module) would couple the the first state of the first group of states to electrons with angular momenta :math:`l=0,2` both with :math:`m=0`, the second state of the first group of states to electrons with :math:`l=0` and :math:`m=0`, and both states of the second group of states to electrons with :math:`l=1` and :math:`m=\pm 1`.
* **cap** Define complex absorbing potentials (CAP) to be included in the calculation. CAPs may be useful for subsequent time dependent calculations to avoid reflections. Furthermore the complex eigenvalues of the quenched Hamiltonians may give direct insight into the life times of auto ionising states. In general the CAPs are of the shape: :math:`V_{n,r_0,z}(r)=zr^n\theta(r-r_0)`. Each CAP to be included may thus be defined via three or four numbers. If three numbers are given they correspond to: order of polynomial growths :math:`n`, starting point :math:`r_0`, and strength :math:`\mathrm{Im}(z)`. If a fourth number is given it corresponds to :math:`\mathrm{Re}(z)`, which may be used to accelerate the wave packet once it reaches the absorber. To include multiple CAPs, sets of parameters may be provided and should be separated by commas. 

* **splineOrder** The order of the B-splines in the GABs basis. The default value is normally appropriate. Default: 7
* **matrixElementThreshold** Minimum overlap between primitive Gaussian functions and BSplines. Default: :math:`10^{-24}`
* **gaussianBSplineOverlapThreshold** Minimum overlap between preconditioned Gaussian functions and BSplines. Default: :math:`10^{-16}`
* **gaussianProjectorThreshold** Minimum deviation from 1 of the eigenvalues of the projector on the preconditioned gaussian basis expressed in the BSpline basis required to consider the linear combination of BSpline independent from the preconditioned Gaussian basis. Default: :math:`10^{-8}`
* **plotBasis** If true, plots of the B-spline basis functions will be produced. 
* **nPlot** Radial grid points to use for plotting of B-splines. Default: 1000.
* **nBSplineSkip** Define the first :math:`n` B-splines to be neglected, this is used to ensure that the wave functions is smooth at rmin. The default value is normally appropriate. Default: 3
* **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
