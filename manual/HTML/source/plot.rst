.. _plot:

*****
&plot
*****

This module produces some first guess plots to analyse: the eigen energies of the (quenched) Hamiltonian, the scattering phases, and the dipole transition amplitudes. **Note**: this module requires the python package matplotlib.




 
