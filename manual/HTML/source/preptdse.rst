.. _preptdse:

*********
&preptdse
*********

This module writes all computed operators (S, H, Dipole, etc) between basis functions in different matrices. This module is useful in case one wants to either perform his/her own calculation to obtain the different matrices necessary to compute time dependent calculations that propagate in the eigen states of the Hamiltonian, or propagate directly in the basis functions. **Note**: The :ref:`boxstates` module produces the matrices necessary to propagate in the eigen states of the Hamiltonian. This module **does not** perform propagations. 

Parameters
##########
* **closeCouplingChannels** Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the time dependent analysis of photoionization processes.
* **gauges** Analogous to the same keyword of the module ccm.
* **axes** Analogous to the same keyword of the module ccm.
* **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
* **loadLocStates** 
* **cond**
* **numBsDropBeginning**
* **numBsDropEnding**
* **conditionNumber**
* **conditionBsThreshold**
* **conditionBsMethod**
* **brasym**


