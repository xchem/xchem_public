************
Installation
************

To run the XCHEM code two main pieces of software must be installed: the XCHEM code itself and  a customised version of OpenMolcas; the open source version of the quantum chemistry package Molcas. This chapter outlines the steps to install the former, as well as how to patch the source code of the latter.

Prerequisites
#############

In order to compile and run XCHEM and OpenMolcas several pieces of software must be present:

#. ifort version :math:`>` 15.0 +
#. MKL (or lapack)
#. hdf5
#. python version :math:`>` 3.0 +

Also the following python modules must be installed:

#. pyparsing
#. h5py
#. six

Furthermore, XCHEM in its current state, is designed to be run on machines using the `Slurm Workload Manager <https://www.schedmd.com/>`_ and several scripts are designed to send jobs using it. It should be noted that, with the exception of very small systems, XCHEM is likely to prove too time consuming if run on a single node.

XCHEM
#####

Clone the XCHEM repository by executing the following command:

::

	git clone git@github.com:jesusG-V/xchemQC.git

In the cloned directory two separate source directories appear, corresponding to XCHEM-QC and XCHEM-SCAT. The source files in both must be compiled by executing

::

	make all

in each.

OpenMolcas
##########

Clone the version of OpenMolcas adapted for use as part of XCHEM by executing 

::

	git clone -b xchemqc git@gitlab.com:mklk1988/OpenMolcas.git

Afterwards the steps for installing OpenMolcas correspond to those of installing a standard `OpenMolcas <https://gitlab.com/Molcas/OpenMolcas>`_ distribution. Note that OpenMolcas **must** be installed with hdf5 support. 

