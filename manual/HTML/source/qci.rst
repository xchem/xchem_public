.. _qci:

****
&qci
****

All relevant operator matrix elements are computed (this is equivalent to the Rassi module of Molcas).

Parameters
##########

*  **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.

