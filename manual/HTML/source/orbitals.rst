.. _orbitals:

*********
&orbitals
*********

This module uses the provided polycentric Gaussian orbitals, to create an orthogonal set of monocentric (diffuse) Gaussian basis functions. Subsequently it transforms all integrals from basis functions to orbitals (this is equivalent to the Motra module of Molcas). If using large values for lMonocentric and kMonocentric this latter part of the calculation may take a couple of hours on a single node.

Parameters
##########

*  **orbitalFile** One of the two files that need to be prepared prior to the execution of XCHEM (apart from the input file itself, the other is the gateway file required for the *basis* module). Specifies the name of the file containing the polycentric Gaussian orbitals to be used to represent both the parent ions and source states. The file must be in Molcas' INPORB format.
*  **inac** Number of inactive orbitals in each irreducible representation of the point group, used to specify the molecule (see Molcas documentation for more details). Default: all 0
*  **ras2** Number of active orbitals in each irreducible representation of the point group, used to specify the molecule. All possible excitations are considered within the ras2 space (see Molcas documentation for more details). 
*  **ras1** Number of ras1 orbitals in each irreducible representation of the point group, used to specify the molecule. Only excitation up to a maximum number of holes are permitted in this space (see Molcas documentation for more details). Default: all 0
*  **ras3** Number of ras3 orbitals in each irreducible representation of the point group, used to specify the molecule. Only excitation up to a maximum number of electrons are permitted in this space (see Molcas documentation for more details). Default: all 0
*  **linDepThr** Threshold to be used in creating the orthogonal monocentric Gaussian orbitals. Default: :math:`10^{-8}`
*  **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.

