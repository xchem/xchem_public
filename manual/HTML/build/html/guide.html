
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Guide to Parameters &#8212; Xchem  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Bibliography" href="references.html" />
    <link rel="prev" title="&amp;preptdse" href="preptdse.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="guide-to-parameters">
<h1>Guide to Parameters<a class="headerlink" href="#guide-to-parameters" title="Permalink to this headline">¶</a></h1>
<div class="section" id="choosing-the-bases">
<h2>Choosing the Bases<a class="headerlink" href="#choosing-the-bases" title="Permalink to this headline">¶</a></h2>
<p>As pointed out previously the basis may be seen as composed of polycentric Gaussians, monocentric Gaussians and B-splines. Making good choices for these basis sets is crucial for obtaining good results in acceptable computational time. The following three paragraphs outline the considerations one should make for each of these basis sets.</p>
<div class="section" id="polycentric-gaussians">
<span id="choosepcg"></span><h3>Polycentric Gaussians<a class="headerlink" href="#polycentric-gaussians" title="Permalink to this headline">¶</a></h3>
<p>The polycentric Gaussians take care of the description of the short range structure of parent ion states and source states. As such a basis set should be chosen that is flexible enough to give a good description of those states. Care should be taken not to include very diffuse polycentric Gaussians, as that would require increasing <span class="math notranslate nohighlight">\(R_0\)</span> which in turn requires the use of a large monocentric Gaussian basis.</p>
<p>As a method to check the viability of the PCGs used, it is recommendable to plot or otherwise inspect the radial behaviour of the active space orbitals constructed from PCGs, as exemplified in figures <a class="reference internal" href="#rad1"><span class="std std-numref">Fig. 3</span></a> and <a class="reference internal" href="#rad2"><span class="std std-numref">Fig. 4</span></a>.</p>
</div>
</div>
<div class="section" id="monocentric-gaussians">
<h2>Monocentric Gaussians<a class="headerlink" href="#monocentric-gaussians" title="Permalink to this headline">¶</a></h2>
<p>For most purposes the default values that define the exponents used in the monocentric Gaussian basis, need not be modified. The key parameters in choosing the monocentric Gaussian basis are <em>lMonocentric and *kMonocentric</em>. In general smaller values of <span class="math notranslate nohighlight">\(l\)</span> require larger values of <span class="math notranslate nohighlight">\(k\)</span> to ensure the same radial flexibility. Therefore for applications requiring a small maximum value for <span class="math notranslate nohighlight">\(l\leq3\)</span> (e.g. small molecules with high symmetry), it is usually sufficient to choose the same maximum value for <span class="math notranslate nohighlight">\(k\)</span> for every <span class="math notranslate nohighlight">\(l\)</span> (see <a class="reference internal" href="basis.html#basis"><span class="std std-ref">&amp;basis</span></a> for details). For more complex systems it may be useful to choose large values of <span class="math notranslate nohighlight">\(k\)</span> (e.g. <span class="math notranslate nohighlight">\(3\)</span>) for small <span class="math notranslate nohighlight">\(l\)</span> (e.g. <span class="math notranslate nohighlight">\(\leq2\)</span>) and smaller values for <span class="math notranslate nohighlight">\(k\)</span> (e.g. <span class="math notranslate nohighlight">\(0\)</span>) for larger l <span class="math notranslate nohighlight">\(l\geq 4\)</span>. Furthermore the need for higher angular momenta increases for scattering states of larger energies; in figure <a class="reference internal" href="#artifacts"><span class="std std-numref">Fig. 5</span></a> the unphysical slow oscillations around the correct cross sections are a consequence of insufficient <em>lMonocentric</em> at the affected energies.</p>
</div>
<div class="section" id="b-splines">
<h2>B-splines<a class="headerlink" href="#b-splines" title="Permalink to this headline">¶</a></h2>
<p>The key parameters to set up the B-spline basis are <em>rmin</em>, <em>rmax</em> and <em>numberNodes</em>.</p>
<div class="section" id="rmin">
<h3>rmin<a class="headerlink" href="#rmin" title="Permalink to this headline">¶</a></h3>
<p>Finding a good choice for <em>rmin</em> follows essentially the path outlined in section <a class="reference internal" href="#choosepcg"><span class="std std-ref">Polycentric Gaussians</span></a>. Negligible overlap between the B-splines and active space orbitals, used to express the source and parent ion states, must be ensured. Figure <a class="reference internal" href="#rad2"><span class="std std-numref">Fig. 4</span></a> and <a class="reference internal" href="#rad1"><span class="std std-numref">Fig. 3</span></a> show how a choice for <em>rmin</em>, guaranteing neglible overlap, may be made.</p>
</div>
<div class="section" id="rmax">
<h3>rmax<a class="headerlink" href="#rmax" title="Permalink to this headline">¶</a></h3>
<p><em>rmax</em> must be chose large enough to avoid the following complication: computing scattering states just above an ionisation threshold corresponds to ejection of electrons with a very long wavelength. For an accurate representation of the scattering states of a given energy it must be ensured that at least a few oscillations of the electronic wave function fit into the box demarcated by <em>rmax</em>. Furthermore, if using a complex absober, <em>rmax</em> may need to be increased to accomodate it.</p>
</div>
<div class="section" id="numbernodes">
<h3>numberNodes<a class="headerlink" href="#numbernodes" title="Permalink to this headline">¶</a></h3>
<p>The number of nodes determines the density of B-splines. It should be ensured that the chose B-spline basis is capable of representing the oscillations of an electron ejected at the maximum kinetic energy included in the calculation; in figure <a class="reference internal" href="#artifacts"><span class="std std-numref">Fig. 5</span></a> the unphysical fast oscillations around the correct cross sections are a consequence of insufficient <em>numberNodes</em> at the affected energies.</p>
<div class="figure align-default" id="id1">
<span id="rad1"></span><img alt="_images/rad1.png" src="_images/rad1.png" />
<p class="caption"><span class="caption-number">Fig. 3 </span><span class="caption-text">Radial behaviour of active space orbitals of a calculation for NO:math:<cite>_2</cite>. In choosing the PCGs, the active space, and <span class="math notranslate nohighlight">\(R_0\)</span> a compromise should be aimed for that allows choosing <span class="math notranslate nohighlight">\(R_0\)</span>, so that no significant part of the active PCG orbitals is allowed to overlap with B-Splines. In this example <span class="math notranslate nohighlight">\(R_0=7\)</span> would be a reasonable choice, given that more than 99.9% of each of the active space orbitals is contained to within a radius of <span class="math notranslate nohighlight">\(7\)</span> atomic units.</span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</div>
<div class="figure align-default" id="id2">
<span id="rad2"></span><img alt="_images/orbs_pyrazine.png" src="_images/orbs_pyrazine.png" />
<p class="caption"><span class="caption-number">Fig. 4 </span><span class="caption-text">ISO surface of an active space orbital of pyrazine cropped to different box sizes. The interior of the manifold contains 99.9% of the depicted orbital. Left: the manifold cropped to a box of 8 a.u.; it is fully contained in the box. Right: the manifold cropped to a box of 7 a.u.; protruding sections are cut away. A similar analysis of all active space orbitals, would allow choosing an appropriate value for <span class="math notranslate nohighlight">\(R_0\)</span>.</span><a class="headerlink" href="#id2" title="Permalink to this image">¶</a></p>
</div>
<div class="figure align-default" id="id3">
<span id="artifacts"></span><img alt="_images/h.jpg" src="_images/h.jpg" />
<p class="caption"><span class="caption-number">Fig. 5 </span><span class="caption-text">Comparison of photoionization cross section of atomic hydrogen obtained analytically (black) and with XCHEM (coloured). The effects of poorly chosen parameters for the basis sets manifest themselves in different ways: In sufficient angular momentum in the monocentric Gaussian basis leads to slow unphysical oscillations around the correct solution, it can be seen clearly that increasing <span class="math notranslate nohighlight">\(l\)</span> remedies this behaviour. Insufficient density of B-splines leads to jagged, fast oscillation before incorrectly predicting zero cross section. Increasing the B-spline density would yield smooth curves.</span><a class="headerlink" href="#id3" title="Permalink to this image">¶</a></p>
</div>
</div>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Structure and Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="howto.html">Using XCHEM</a></li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1"><a class="reference internal" href="bsplines.html">&amp;bsplines</a></li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Guide to Parameters</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#choosing-the-bases">Choosing the Bases</a></li>
<li class="toctree-l2"><a class="reference internal" href="#monocentric-gaussians">Monocentric Gaussians</a></li>
<li class="toctree-l2"><a class="reference internal" href="#b-splines">B-splines</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="preptdse.html" title="previous chapter">&amp;preptdse</a></li>
      <li>Next: <a href="references.html" title="next chapter">Bibliography</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.2.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.8</a>
      
      |
      <a href="_sources/guide.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>