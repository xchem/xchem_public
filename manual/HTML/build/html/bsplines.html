
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>&amp;bsplines &#8212; Xchem  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="&amp;ccm" href="ccm.html" />
    <link rel="prev" title="&amp;qci" href="qci.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="bsplines">
<span id="id1"></span><h1>&amp;bsplines<a class="headerlink" href="#bsplines" title="Permalink to this headline">¶</a></h1>
<p>This module calculates the integrals between B-splines and monocentric Gaussian basis functions. Furthermore it converts the results from the rdm modules to be treatable as a scattering problem. The parameters of this module must be chose with care as all subsequent modules depend on them, making changes potentially costly.</p>
<div class="section" id="parameters">
<h2>Parameters<a class="headerlink" href="#parameters" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li><p><strong>rmin</strong> Defines the radius in atomic units from the origin at which the B-spline part of the GABs basis starts. This should be chose so that the polycentric Gaussian  orbitals used to describe the parent ions and source states only protrude beyond it to a negligible extent.</p></li>
<li><p><strong>rmax</strong> Defines the radius in atomic units at which the B-spline basis ends. In order to obtain correct scattering states, it should be ensured that at least a few oscillations of the electron being ejected can be represented in the resulting box.</p></li>
<li><p><strong>numberNodes</strong> Defines the number of nodes used to create the B-spline basis. A reasonable first guess is two nodes per atomic unit, though higher B-spline densities may be necessary, depending on the kinetic energy of the ejected electron (larger kinetic energies require larger densities).</p></li>
<li><p><strong>closeCouplingChannels</strong> Defines the close coupling expansion (i.e. ejected electrons of which angular momenta should “leave behind” which parent ions). The format for this is <span class="math notranslate nohighlight">\(\mathrm{ccm}=\Phi_1 (l^1_1\;m^1_1,\cdots,l^1_{n_1}\;m^1_{n_1});\cdots;\Phi_i (l^i_1\;m^i_1,\cdots,l^i_{n_i}\;m^i_{n_i})\)</span>, where <span class="math notranslate nohighlight">\(\Phi_i\)</span> is the <span class="math notranslate nohighlight">\(i^{\mathrm{th}}\)</span> parent ion defined via ciroParentStates. For example <span class="math notranslate nohighlight">\(\mathrm{ccm}=1 (0\;0,2\;0); 2(2\;0),3 (1\;-1,1\;1); 4(1\;-1, 1\;1)\)</span> in combination with <span class="math notranslate nohighlight">\(\mathrm{ciroParentStates}=2\;2\)</span> (used in the rdm module) would couple the the first state of the first group of states to electrons with angular momenta <span class="math notranslate nohighlight">\(l=0,2\)</span> both with <span class="math notranslate nohighlight">\(m=0\)</span>, the second state of the first group of states to electrons with <span class="math notranslate nohighlight">\(l=0\)</span> and <span class="math notranslate nohighlight">\(m=0\)</span>, and both states of the second group of states to electrons with <span class="math notranslate nohighlight">\(l=1\)</span> and <span class="math notranslate nohighlight">\(m=\pm 1\)</span>.</p></li>
<li><p><strong>cap</strong> Define complex absorbing potentials (CAP) to be included in the calculation. CAPs may be useful for subsequent time dependent calculations to avoid reflections. Furthermore the complex eigenvalues of the quenched Hamiltonians may give direct insight into the life times of auto ionising states. In general the CAPs are of the shape: <span class="math notranslate nohighlight">\(V_{n,r_0,z}(r)=zr^n\theta(r-r_0)\)</span>. Each CAP to be included may thus be defined via three or four numbers. If three numbers are given they correspond to: order of polynomial growths <span class="math notranslate nohighlight">\(n\)</span>, starting point <span class="math notranslate nohighlight">\(r_0\)</span>, and strength <span class="math notranslate nohighlight">\(\mathrm{Im}(z)\)</span>. If a fourth number is given it corresponds to <span class="math notranslate nohighlight">\(\mathrm{Re}(z)\)</span>, which may be used to accelerate the wave packet once it reaches the absorber. To include multiple CAPs, sets of parameters may be provided and should be separated by commas.</p></li>
<li><p><strong>splineOrder</strong> The order of the B-splines in the GABs basis. The default value is normally appropriate. Default: 7</p></li>
<li><p><strong>matrixElementThreshold</strong> Minimum overlap between primitive Gaussian functions and BSplines. Default: <span class="math notranslate nohighlight">\(10^{-24}\)</span></p></li>
<li><p><strong>gaussianBSplineOverlapThreshold</strong> Minimum overlap between preconditioned Gaussian functions and BSplines. Default: <span class="math notranslate nohighlight">\(10^{-16}\)</span></p></li>
<li><p><strong>gaussianProjectorThreshold</strong> Minimum deviation from 1 of the eigenvalues of the projector on the preconditioned gaussian basis expressed in the BSpline basis required to consider the linear combination of BSpline independent from the preconditioned Gaussian basis. Default: <span class="math notranslate nohighlight">\(10^{-8}\)</span></p></li>
<li><p><strong>plotBasis</strong> If true, plots of the B-spline basis functions will be produced.</p></li>
<li><p><strong>nPlot</strong> Radial grid points to use for plotting of B-splines. Default: 1000.</p></li>
<li><p><strong>nBSplineSkip</strong> Define the first <span class="math notranslate nohighlight">\(n\)</span> B-splines to be neglected, this is used to ensure that the wave functions is smooth at rmin. The default value is normally appropriate. Default: 3</p></li>
<li><p><strong>defer</strong> Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.</p></li>
</ul>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Structure and Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="howto.html">Using XCHEM</a></li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">&amp;bsplines</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#parameters">Parameters</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1"><a class="reference internal" href="guide.html">Guide to Parameters</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="qci.html" title="previous chapter">&amp;qci</a></li>
      <li>Next: <a href="ccm.html" title="next chapter">&amp;ccm</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.2.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.8</a>
      
      |
      <a href="_sources/bsplines.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>