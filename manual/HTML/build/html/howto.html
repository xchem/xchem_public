
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Using XCHEM &#8212; Xchem  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Manual" href="manual.html" />
    <link rel="prev" title="Installation" href="installation.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="using-xchem">
<h1>Using XCHEM<a class="headerlink" href="#using-xchem" title="Permalink to this headline">¶</a></h1>
<div class="section" id="executing-xchem">
<h2>Executing XCHEM<a class="headerlink" href="#executing-xchem" title="Permalink to this headline">¶</a></h2>
<p>To run XCHEM in the most straightforward way requires only execution of the following command:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&lt;</span><span class="n">PATH_TO_XCHEM_INSTALLATION</span><span class="o">&gt;/</span><span class="n">pyxchem</span><span class="o">/</span><span class="n">pyxchem</span><span class="o">.</span><span class="n">py</span> <span class="o">&lt;</span><span class="n">INPUT_FILE</span><span class="o">&gt;</span>
</pre></div>
</div>
<p>The following three sections outline the general structure of the input file, the structure of the results, as well as the modifications that need to be made to the workflow if working on a cluster.</p>
</div>
<div class="section" id="structure-of-the-input-file">
<span id="input-file"></span><h2>Structure of the input file<a class="headerlink" href="#structure-of-the-input-file" title="Permalink to this headline">¶</a></h2>
<p>The input file defines all steps that will be carried out by an XCHEM calculation. The input file requests different modules by including</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&amp;&lt;</span><span class="n">MODULE_NAME</span><span class="o">&gt;</span>
<span class="n">param1</span><span class="o">=</span><span class="n">val</span>
<span class="n">param2</span><span class="o">=</span><span class="n">val1</span> <span class="n">val2</span>
<span class="c1"># lines starting with #,* or ! are treated as comments.</span>
</pre></div>
</div>
<p>where the parameters following the module name, modify the behaviour of the given module. For a detailed list of available modules and their parameters, see chapter <a class="reference internal" href="manual.html#manual"><span class="std std-ref">Manual</span></a>.</p>
<div class="section" id="meta-modules">
<h3>Meta modules<a class="headerlink" href="#meta-modules" title="Permalink to this headline">¶</a></h3>
<p>The modules</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&amp;</span><span class="n">exit</span>
</pre></div>
</div>
<p>and</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&amp;</span><span class="n">nowait</span>
</pre></div>
</div>
<p>may be used to modify the flow of program. If the former is encountered at any point, the calculation halts. The latter may be used to specify modules to be run in parallel if working on a cluster. For more details see section <a class="reference internal" href="#on-clusters"><span class="std std-ref">Working on a cluster</span></a>.</p>
</div>
</div>
<div class="section" id="structure-of-the-produced-data">
<span id="output"></span><h2>Structure of the produced data<a class="headerlink" href="#structure-of-the-produced-data" title="Permalink to this headline">¶</a></h2>
<p>By default all data produced by the XCHEM calculation is saved in the directory:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span>$PWD/XCHEM_data/*
</pre></div>
</div>
<p>In it the modules save those results that are relevant for subsequent calculations in different sub-directories. Data produced during the execution, that is not relevant for subsequent modules is saved in</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span>$PWD/XCHEM_data/&lt;MODULE_DIR&gt;/tmp
</pre></div>
</div>
<p>These directories may be deleted upon completion of each individual module. It is recommendable to do so, as these may become quite large.</p>
</div>
<div class="section" id="working-on-a-cluster">
<span id="on-clusters"></span><h2>Working on a cluster<a class="headerlink" href="#working-on-a-cluster" title="Permalink to this headline">¶</a></h2>
<p>While it is possible to treat small systems, with a limited range of angular momenta on a single node, it is likely that larger calculations using XCHEM need to be run on a cluster. <strong>The current distribution assumes availability of the slurm workload manager.</strong> XCHEM facilitates this by allowing expensive calculations to be executed in two steps: a first cheap step (referred to as setup, see section <a class="reference internal" href="#setup"><span class="std std-ref">Setup</span></a> run locally, and a second more expensive step (referred to as submission, see section <a class="reference internal" href="#submission"><span class="std std-ref">Submission</span></a>) distributed over several nodes. The parameter <strong>defer</strong>, available to all but the basis module is used to control which parts of the calculation to submit.</p>
<div class="section" id="the-defer-parameter">
<h3>The “defer” parameter<a class="headerlink" href="#the-defer-parameter" title="Permalink to this headline">¶</a></h3>
<p>If defer is set to true in some module, the expensive parts of that module and all subsequent modules are not run immediately in their entirety, but rather are prepared for later submission. <strong>Note</strong>: Mixing defer=T and defer=F should be done only with great care, as a module run with defer=F, assumes that all modules whose data it depends on have finished in their entirety.</p>
</div>
<div class="section" id="setup">
<span id="id1"></span><h3>Setup<a class="headerlink" href="#setup" title="Permalink to this headline">¶</a></h3>
<p>The execution of the setup step is identical to a simple, local run of XCHEM:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&lt;</span><span class="n">PATH_TO_XCHEM_INSTALLATION</span><span class="o">&gt;/</span><span class="n">pyxchem</span><span class="o">/</span><span class="n">pyxchem</span><span class="o">.</span><span class="n">py</span> <span class="o">&lt;</span><span class="n">INPUT_FILE</span><span class="o">&gt;</span>
</pre></div>
</div>
<p>Afterwards, the directory structured described in section <a class="reference internal" href="#output"><span class="std std-ref">Structure of the produced data</span></a> will have already been created.</p>
</div>
<div class="section" id="submission">
<span id="id2"></span><h3>Submission<a class="headerlink" href="#submission" title="Permalink to this headline">¶</a></h3>
<p>To submit one or all modules, marked as deferred execute:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&lt;</span><span class="n">PATH_TO_XCHEM_INSTALLATION</span><span class="o">&gt;/</span><span class="n">pyxchem</span><span class="o">/</span><span class="n">pyxchem</span><span class="o">.</span><span class="n">py</span> <span class="o">&lt;</span><span class="n">INPUT_FILE</span><span class="o">&gt;</span> <span class="o">--</span><span class="n">submit</span> <span class="o">&lt;</span><span class="n">MODULE</span><span class="o">&gt;</span> <span class="o">--</span><span class="n">remote_dir</span> <span class="s1">&#39;&lt;REMOTE_PATH&gt;&#39;</span> <span class="o">--</span><span class="n">submission_flags</span> <span class="s1">&#39;&lt;FLAGS&gt;&#39;</span>
</pre></div>
</div>
<p>This will trigger the submission of a series of jobs with dependencies among them set to ensure that they run in the correct order. <strong>NOTE: The input file must not be modified between the setup and the submission step</strong>. The command line parameters have the following functions:</p>
<ol class="arabic simple">
<li><p><span class="math notranslate nohighlight">\(&lt;\)</span> <strong>MODULE</strong> <span class="math notranslate nohighlight">\(&gt;\)</span> Chose which module to submit. “all” may be used to submit all modules marked for submission.</p></li>
<li><p><span class="math notranslate nohighlight">\(&lt;\)</span> <strong>REMOTE_PATH</strong> <span class="math notranslate nohighlight">\(&gt;\)</span> On most clusters each node has a local scratch space that should be used for read and write operations during the execution of some jobs. This flag may be used to specify this directory. <strong>Note:</strong> the path for each job should be unique, for this purpose the path should be given in single inverted comas to avoid premature expansion of environment variables such as $SLURM_JOBID.</p></li>
<li><p><span class="math notranslate nohighlight">\(&lt;\)</span> <strong>FLAGS</strong> <span class="math notranslate nohighlight">\(&gt;\)</span> Flags to be passed to sbatch, to specify number of processors, quality if services, requested computational time etc.</p></li>
</ol>
</div>
<div class="section" id="only-inputs">
<h3>Only_inputs<a class="headerlink" href="#only-inputs" title="Permalink to this headline">¶</a></h3>
<p>It writes all the inputs of the module specified and a script to execute them. This adds more flexibility to run the different jobs.</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">&lt;</span><span class="n">PATH_TO_XCHEM_INSTALLATION</span><span class="o">&gt;/</span><span class="n">pyxchem</span><span class="o">/</span><span class="n">pyxchem</span><span class="o">.</span><span class="n">py</span> <span class="o">&lt;</span><span class="n">INPUT_FILE</span><span class="o">&gt;</span> <span class="o">--</span><span class="n">submit</span> <span class="o">&lt;</span><span class="n">MODULE</span><span class="o">&gt;</span> <span class="o">--</span><span class="n">only_inputs</span>
</pre></div>
</div>
</div>
<div class="section" id="nowait">
<h3>&amp;nowait<a class="headerlink" href="#nowait" title="Permalink to this headline">¶</a></h3>
<p>The &amp;nowait meta module may be used to permit the concurrent execution of several modules. If placed between two modules the jobs corresponding to the second module will not wait for completion of those associated with the first module. This is primarily useful to request simultaneous execution of the ccm module with different close coupling expansions or simultaneous execution of the scatteringstates module with different energy ranges (see chapter <a class="reference internal" href="manual.html#manual"><span class="std std-ref">Manual</span></a> for more details on the modules).</p>
</div>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Structure and Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Using XCHEM</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#executing-xchem">Executing XCHEM</a></li>
<li class="toctree-l2"><a class="reference internal" href="#structure-of-the-input-file">Structure of the input file</a></li>
<li class="toctree-l2"><a class="reference internal" href="#structure-of-the-produced-data">Structure of the produced data</a></li>
<li class="toctree-l2"><a class="reference internal" href="#working-on-a-cluster">Working on a cluster</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1"><a class="reference internal" href="bsplines.html">&amp;bsplines</a></li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1"><a class="reference internal" href="guide.html">Guide to Parameters</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="installation.html" title="previous chapter">Installation</a></li>
      <li>Next: <a href="manual.html" title="next chapter">Manual</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.2.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.8</a>
      
      |
      <a href="_sources/howto.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>