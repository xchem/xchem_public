
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Overview &#8212; Xchem  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Structure and Theory" href="theory.html" />
    <link rel="prev" title="Welcome to Xchem’s documentation!" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="overview">
<h1>Overview<a class="headerlink" href="#overview" title="Permalink to this headline">¶</a></h1>
<p>The XCHEM code comprises a set of tools allowing the computation of scattering states of complex molecules, with the goal of studying photoionization processes ejecting no more than one electron from such systems. Currently <em>complex molecules</em> refers to multi electronic diatomic systems (for instance N<sub>2</sub> and O<sub>2</sub>), and small poly-atomic systems (for instance Water). Though future releases of this code will increase these limitations on the size of the molecules, which are due primarily to requirements made on computational resources.</p>
<div class="section" id="core-ideas">
<h2>Core Ideas<a class="headerlink" href="#core-ideas" title="Permalink to this headline">¶</a></h2>
<p>The philosophy of the XCHEM code may be thought of as the combination of a few of the core aspects used to generate said scattering states. To understand these aspects we recall that, using the close coupling formalism of scattering theory, we may express a scattering state <span class="math notranslate nohighlight">\(\Psi_{\alpha E}\)</span> of a molecular system (with N<sub>e</sub> electrons) as (for details see <a class="bibtex reference internal" href="references.html#marante2014" id="id1">[1]</a><a class="bibtex reference internal" href="references.html#marante2017" id="id2">[2]</a><a class="bibtex reference internal" href="references.html#marante2017-2" id="id3">[3]</a> and references therein)</p>
<div class="math notranslate nohighlight" id="equation-xchemcore">
<span class="eqno">(1)<a class="headerlink" href="#equation-xchemcore" title="Permalink to this equation">¶</a></span>\[\begin{split}     \Psi^{-}_{\alpha E}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e})&amp;=&amp;\sum_i c_{i,\alpha E}\aleph_{i}+\sum_{\beta,i}N_{\alpha i}\hat{\mathcal{A}}\Upsilon_{\beta}c_{\beta i,\alpha E}\quad\mathrm{where}\\\end{split}\]</div>
<div class="math notranslate nohighlight" id="equation-channelfct">
<span class="eqno">(2)<a class="headerlink" href="#equation-channelfct" title="Permalink to this equation">¶</a></span>\[\begin{split}     \Upsilon_{\alpha}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1};\hat{\mathbf{r}}_{N_e},\zeta_{N_e})&amp;=&amp;^{2S+1}\left[\Phi_{a}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1})\otimes\chi(\zeta_{N_e})\right]_{\Sigma}X_{l,m}(\hat{\mathbf{r}}_{N_e})\\\end{split}\]</div>
<p>where <span class="math notranslate nohighlight">\(\aleph_i\)</span> denote <em>short range states</em> with electrons in bound orbitals, <span class="math notranslate nohighlight">\(\Upsilon_{\alpha}\)</span> denote the <em>channel functions</em> built from cationic states coupled to the spin and orbital angular momentum of the ejected electron, <span class="math notranslate nohighlight">\(\phi_i\)</span> denotes the radial component of the ejected electron and <span class="math notranslate nohighlight">\(\alpha\)</span> is the channel index specifying the cationic state <span class="math notranslate nohighlight">\(\Phi_{a}\)</span>, the quantum numbers <span class="math notranslate nohighlight">\(l\)</span> and <span class="math notranslate nohighlight">\(m\)</span> of the ejected electron and the total and projected spin angular momenta <span class="math notranslate nohighlight">\(S\)</span> and <span class="math notranslate nohighlight">\(\Sigma\)</span>. Of the terms in equations <a class="reference internal" href="#equation-xchemcore">(1)</a> and <a class="reference internal" href="#equation-channelfct">(2)</a>, <span class="math notranslate nohighlight">\(\phi_{i}\)</span> is the only term inherently dependent on the oscillatory behaviour of the continuum electron.</p>
<p>The short range states <span class="math notranslate nohighlight">\(\aleph_i\)</span>, as well as the cationic state <span class="math notranslate nohighlight">\(\Phi_{\alpha}\)</span>, contained in the definition of the channel functions, are described exclusively in terms of electrons in bound orbitals. Therefore they may be computed with the standard tools of quantum chemistry (QC), computationally implemented in quantum chemistry packages such as Molcas <a class="bibtex reference internal" href="references.html#aquilante2016molcas" id="id4">[4]</a> and Molpro <a class="bibtex reference internal" href="references.html#molpro-brief" id="id5">[5]</a>. These (and others) have in common, that the electronic wavefunction is expressed in terms of orbitals expanded in sets of Gaussian basis functions centred at the atomic sites of the molecular system; the so called polycentric Gaussian (PCG) basis sets.</p>
<p>In contrast to that, a similar approach is poorly suited for the <span class="math notranslate nohighlight">\(\phi_i(r_{N_e})\)</span>-term of equation <a class="reference internal" href="#equation-xchemcore">(1)</a> due to problems associated with PCGs in expressing the long range behaviour of continuum electrons <a class="bibtex reference internal" href="references.html#marante2014" id="id6">[1]</a><a class="bibtex reference internal" href="references.html#marante2017" id="id7">[2]</a>. Consequently a hybrid set of B-Spline basis functions and an auxiliary set of monocentric Gaussian (MCG) basis functions, centred at the centre of mass of the the molecule, is used for the description of the continuum electron. This basis set is referred to as a GABS Basis <a class="bibtex reference internal" href="references.html#marante2014" id="id8">[1]</a>.</p>
<p>This approach yields a key advantage inherent in the design of the GABS and PCG basis sets, namely a vanishing overlap between B-Splines and PCGs. This significantly simplifies the evaluation of integrals allowing for the application of the XCHEM code to rather complex systems. This combination of using PCG-based QCPs (for the evaluation of <span class="math notranslate nohighlight">\(\aleph_i\)</span> and <span class="math notranslate nohighlight">\(\Phi_{\alpha}\)</span>) and interfacing them with a GABS based approach (for the representation of <span class="math notranslate nohighlight">\(\phi_{i}\)</span>) to construct physically admissible scattering states <a class="footnote-reference brackets" href="#id10" id="id9">1</a> is the approach defining the XCHEM code. More details about the components comprising the XCHEM code may be found in the next chapter.</p>
<dl class="footnote brackets">
<dt class="label" id="id10"><span class="brackets"><a class="fn-backref" href="#id9">1</a></span></dt>
<dd><p>obtained as solutions to the generalized eigenvalue problem <span class="math notranslate nohighlight">\((\mathbf{H}-\mathbf{S}E)\mathbf{c}=0\)</span> (where <span class="math notranslate nohighlight">\(\mathbf{H}\)</span> and <span class="math notranslate nohighlight">\(\mathbf{S}\)</span> are the Hamiltonian and overlap matrices states obtained by augmenting the <span class="math notranslate nohighlight">\(\Phi_a\)</span> of equation <a class="reference internal" href="#equation-channelfct">(2)</a> with an additional electron), with physicality of the solution being ensured by enforcing correct asymptotic behaviour <a class="bibtex reference internal" href="references.html#marante2017" id="id11">[2]</a></p>
</dd>
</dl>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Overview</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#core-ideas">Core Ideas</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Structure and Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="howto.html">Using XCHEM</a></li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1"><a class="reference internal" href="bsplines.html">&amp;bsplines</a></li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1"><a class="reference internal" href="guide.html">Guide to Parameters</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Welcome to Xchem’s documentation!</a></li>
      <li>Next: <a href="theory.html" title="next chapter">Structure and Theory</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.2.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.8</a>
      
      |
      <a href="_sources/overview.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>