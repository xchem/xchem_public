
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />

    <title>Structure and Theory &#8212; Xchem  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Installation" href="installation.html" />
    <link rel="prev" title="Overview" href="overview.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="structure-and-theory">
<h1>Structure and Theory<a class="headerlink" href="#structure-and-theory" title="Permalink to this headline">¶</a></h1>
<p>At the highest level the XCHEM code may be understood as separated into two components dealing with QC and scattering theory. The two components shall be referred to as XCHEM-QC and XCHEM-SCAT, respectively. In the following paragraph the theoretical background underlying each component is outlined to help understand the specifications that need to be made in the input files. The outlines are deliberately concise, and for more details the reader is referred to the references given in the text.</p>
<section id="xchem-qc">
<h2>XCHEM-QC<a class="headerlink" href="#xchem-qc" title="Permalink to this headline">¶</a></h2>
<p>XCHEM-QC carries out all those calculations, that do not involve B-spline basis functions and therefore frequently allow the use of QCPs. Broadly speaking, XCHEM-QC starts from a set of molecular orbitals obtained in a CASSCF or RASSCF calculation, used to describe a set of short range states <span class="math notranslate nohighlight">\(\aleph\)</span> and parent ion (PI) states <span class="math notranslate nohighlight">\(\Phi_a\)</span> which are subsequently augmented with an additional electron in some orbital <span class="math notranslate nohighlight">\(\varphi_i\)</span> (giving neutral states if working with singly charged PIs, as is frequently the case). Operator matrix elements of the augmented states for one (<span class="math notranslate nohighlight">\(\hat{\mathcal{O}}^1\)</span>) and two (<span class="math notranslate nohighlight">\(\hat{\mathcal{O}}^2\)</span>) electron operators are then computed via</p>
<div class="math notranslate nohighlight" id="equation-oneelopmat">
<span class="eqno">(3)<a class="headerlink" href="#equation-oneelopmat" title="Permalink to this equation">¶</a></span>\[\begin{split}     \mathcal{O}^{1}_{IJ} &amp;=&amp; \sum_{ij} D_{ij}^{IJ} \int d\mathbf{r}\varphi_i(\mathbf{r})\hat{\mathcal{O}}^1\varphi_j(\mathbf{r})\\\end{split}\]</div>
<div class="math notranslate nohighlight" id="equation-twoelopmat">
<span class="eqno">(4)<a class="headerlink" href="#equation-twoelopmat" title="Permalink to this equation">¶</a></span>\[\begin{split}     \mathcal{O}^{2}_{IJ} &amp;=&amp; \sum_{ijkl} d_{ijkl}^{IJ}\int d\mathbf{r}_1d\mathbf{r}_2\varphi_i(\mathbf{r}_1)\varphi_j(\mathbf{r}_1)\hat{\mathcal{O}}^2\varphi_k(\mathbf{r}_2)\varphi_l(\mathbf{r}_2)\\\end{split}\]</div>
<p>where <span class="math notranslate nohighlight">\(D\)</span> and <span class="math notranslate nohighlight">\(d\)</span> are the one and two electron reduced density matrices, respectively. The set of orbitals <span class="math notranslate nohighlight">\(\varphi_i\}\)</span> used to augment the PIs in, comprises the active space orbitals (ASO) <span class="math notranslate nohighlight">\(\varphi^{a}\)</span> used to compute the PIs (expanded in a standard set of PCG basis functions <span class="math notranslate nohighlight">\(b^P\)</span>), as well as a set of diffuse orbitals (DO) <span class="math notranslate nohighlight">\(\varphi^{d}\)</span> expanded in the auxiliary MCG basis <span class="math notranslate nohighlight">\(b^{M}\)</span> and orthogonal to <span class="math notranslate nohighlight">\(\varphi^{a}\)</span>.</p>
<p>Computation of the integrals appearing in equations <a class="reference internal" href="#equation-oneelopmat">(3)</a> and <a class="reference internal" href="#equation-twoelopmat">(4)</a> requires knowledge of the integrals of PCG and MCG basis functions. These are computed using Molcas’ Seward module. The basis coefficients of <span class="math notranslate nohighlight">\(\varphi^{a}\)</span> are known and those of <span class="math notranslate nohighlight">\(\varphi^{d}\)</span> are computed ensuring orthonormality.</p>
<p>Computation of the reduced density matrices requires knowledge of the CI vectors of <span class="math notranslate nohighlight">\(\aleph\)</span> and <span class="math notranslate nohighlight">\(\hat{a}_i^{\dagger}\Phi_a\)</span>, where <span class="math notranslate nohighlight">\(\hat{a}_i^{\dagger}\)</span> creates an electron in orbital <span class="math notranslate nohighlight">\(\varphi_i\)</span> (being either an ASO or a DO). For <span class="math notranslate nohighlight">\(\aleph\)</span> these are known. For <span class="math notranslate nohighlight">\(\hat{a}_i^{\dagger}\Phi_a\)</span>, the expansion of <span class="math notranslate nohighlight">\(\Phi_a\)</span> in terms of configuration state functions (CSF) is converted to an expansion in terms of Slater determinants by using the graphical unitary group approach (the relevant GUGA tables may be computed with OpenMOLCAS’ RASSCF module). Application of <span class="math notranslate nohighlight">\(\hat{a}_i\)</span> to Slater determinants is trivial and states of the required spin may be obtained be reverting to a CSF description using the appropriate GUGA table for the augmented system.</p>
<p>The operators whose matrix elements need to be computed in this way, are the one and two electron Hamiltonian, the overlap as well as the electrical dipole moment <span class="math notranslate nohighlight">\(\mathbf{r}\)</span> (length gauge) and canonical momentum <span class="math notranslate nohighlight">\(\mathbf{p}\)</span> (velocity gauge).</p>
</section>
<section id="xchem-scat">
<h2>XCHEM-SCAT<a class="headerlink" href="#xchem-scat" title="Permalink to this headline">¶</a></h2>
<p>The XCHEM-SCAT code introduces the B-spline part of the GABS basis, to be able to describe the long range oscillatory behaviour of continuum electrons. The GABS basis is characterized by two key parameters: <strong>a)</strong> the radius <span class="math notranslate nohighlight">\(R_0\)</span> with all Bspline <span class="math notranslate nohighlight">\(B(r&lt;R_0)=0\)</span> and chosen so that <span class="math notranslate nohighlight">\(|\varphi^a(r&gt;R_0)|&lt;&lt;1\)</span>, and <strong>b)</strong> the radius <span class="math notranslate nohighlight">\(R_1\)</span> such that <span class="math notranslate nohighlight">\(R_0&lt;R_1\)</span> and <span class="math notranslate nohighlight">\(|\varphi^d(r&gt;R_1)|&lt;&lt;1\)</span>. Figure <a class="reference internal" href="#gabs3d"><span class="std std-numref">Fig. 1</span></a> schematically visualizes the key features of all three types of basis functions present for an XCHEM calculations (PCG, MCG and BSpline). The lack of overlap between PCGs and B-splines means that the computation of the operator matrix elements for two augmented states of which at least one is augmented in a B-spline orbital can be simplified according to <span id="id1">[<a class="reference internal" href="references.html#id3" title="C. Marante, M. Klinker, I. Corral, J. González-Vázquez, L. Argenti, and F. Martín. Hybrid-basis close-coupling interface to quantum chemistry packages for the treatment of ionization problems. Journal of Chemical Theory and Computation, 13(2):499–514, 2017. doi:10.1021/acs.jctc.6b00907.">2</a>]</span></p>
<div class="math notranslate nohighlight">
\[\mathcal{O}=\langle\hat{\mathcal{A}}\Upsilon|\hat{\mathcal{O}}|\hat{\mathcal{A}}\Upsilon\rangle=\langle\Upsilon|\hat{\mathcal{O}}|\Upsilon\rangle.\]</div>
<p>From the matrix elements one may then obtain scattering states (by imposing the asymptotically correct boundary conditions for photoionization problems) and compute observable quantities such as the photo ionization cross sections or use the box eigenstates as a basis for the solution of the time dependent Schrödinger Equation.</p>
<figure class="align-default" id="id2">
<span id="gabs3d"></span><img alt="_images/GABS3D.png" src="_images/GABS3D.png" />
<figcaption>
<p><span class="caption-number">Fig. 1 </span><span class="caption-text">Schematic depiction of relevant basis functions. The PCG for the description of <span class="math notranslate nohighlight">\(\Phi_a\)</span> and <span class="math notranslate nohighlight">\(\aleph\)</span> are shown in blue and black radially extending no further than <span class="math notranslate nohighlight">\(R_0\)</span>. The MCGs are show in magenta and are assumed to be negligible beyond <span class="math notranslate nohighlight">\(R_1\)</span>. The B-splines are shown in cyan and extend from <span class="math notranslate nohighlight">\(R_0\)</span> to the end of the box. Clearly visible is the absence of a radial region in which both B-splines and PCGs are non-zero.</span><a class="headerlink" href="#id2" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">Overview</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Structure and Theory</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#xchem-qc">XCHEM-QC</a></li>
<li class="toctree-l2"><a class="reference internal" href="#xchem-scat">XCHEM-SCAT</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="howto.html">Using XCHEM</a></li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1"><a class="reference internal" href="bsplines.html">&amp;bsplines</a></li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1"><a class="reference internal" href="guide.html">Guide to Parameters</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="overview.html" title="previous chapter">Overview</a></li>
      <li>Next: <a href="installation.html" title="next chapter">Installation</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 4.3.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/theory.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>