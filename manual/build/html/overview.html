
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />

    <title>Overview &#8212; Xchem  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Structure and Theory" href="theory.html" />
    <link rel="prev" title="Welcome to Xchem’s documentation!" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="overview">
<h1>Overview<a class="headerlink" href="#overview" title="Permalink to this headline">¶</a></h1>
<p>The XCHEM code comprises a set of tools allowing the computation of scattering states of complex molecules, with the goal of studying photoionization processes ejecting no more than one electron from such systems. Currently <em>complex molecules</em> refers to multi electronic diatomic systems (for instance N<sub>2</sub> and O<sub>2</sub>), and small poly-atomic systems (for instance Water). Though future releases of this code will increase these limitations on the size of the molecules, which are due primarily to requirements made on computational resources.</p>
<section id="core-ideas">
<h2>Core Ideas<a class="headerlink" href="#core-ideas" title="Permalink to this headline">¶</a></h2>
<p>The philosophy of the XCHEM code may be thought of as the combination of a few of the core aspects used to generate said scattering states. To understand these aspects we recall that, using the close coupling formalism of scattering theory, we may express a scattering state <span class="math notranslate nohighlight">\(\Psi_{\alpha E}\)</span> of a molecular system (with N<sub>e</sub> electrons) as (for details see <span id="id1">[<a class="reference internal" href="references.html#id2" title="C. Marante, L. Argenti, and F. Martín. Hybrid Gaussian B-spline basis for the electronic continuum: Photoionization of atomic Hydrogen. Physical Review A, 90(1):012506, 2014. doi:10.1103/PhysRevA.90.012506.">1</a>, <a class="reference internal" href="references.html#id3" title="C. Marante, M. Klinker, I. Corral, J. González-Vázquez, L. Argenti, and F. Martín. Hybrid-basis close-coupling interface to quantum chemistry packages for the treatment of ionization problems. Journal of Chemical Theory and Computation, 13(2):499–514, 2017. doi:10.1021/acs.jctc.6b00907.">2</a>, <a class="reference internal" href="references.html#id4" title="C. Marante, M. Klinker, T. Kjellsson, E. Lindroth, J. González-Vázquez, Argenti, L., and F. Martín. Photoionization of Ne using the XCHEM approach: Total and partial cross sections and resonance parameters above the $2s^2$ $2p^5$ threshold. Physical Review A, pages 022507, 2017. doi:10.1103/PhysRevA.96.022507.">3</a>]</span> and references therein)</p>
<div class="math notranslate nohighlight" id="equation-xchemcore">
<span class="eqno">(1)<a class="headerlink" href="#equation-xchemcore" title="Permalink to this equation">¶</a></span>\[\begin{split}     \Psi^{-}_{\alpha E}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e})&amp;=&amp;\sum_i c_{i,\alpha E}\aleph_{i}+\sum_{\beta,i}N_{\alpha i}\hat{\mathcal{A}}\Upsilon_{\beta}c_{\beta i,\alpha E}\quad\mathrm{where}\\\end{split}\]</div>
<div class="math notranslate nohighlight" id="equation-channelfct">
<span class="eqno">(2)<a class="headerlink" href="#equation-channelfct" title="Permalink to this equation">¶</a></span>\[\begin{split}     \Upsilon_{\alpha}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1};\hat{\mathbf{r}}_{N_e},\zeta_{N_e})&amp;=&amp;^{2S+1}\left[\Phi_{a}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1})\otimes\chi(\zeta_{N_e})\right]_{\Sigma}X_{l,m}(\hat{\mathbf{r}}_{N_e})\\\end{split}\]</div>
<p>where <span class="math notranslate nohighlight">\(\aleph_i\)</span> denote <em>short range states</em> with electrons in bound orbitals, <span class="math notranslate nohighlight">\(\Upsilon_{\alpha}\)</span> denote the <em>channel functions</em> built from cationic states coupled to the spin and orbital angular momentum of the ejected electron, <span class="math notranslate nohighlight">\(\phi_i\)</span> denotes the radial component of the ejected electron and <span class="math notranslate nohighlight">\(\alpha\)</span> is the channel index specifying the cationic state <span class="math notranslate nohighlight">\(\Phi_{a}\)</span>, the quantum numbers <span class="math notranslate nohighlight">\(l\)</span> and <span class="math notranslate nohighlight">\(m\)</span> of the ejected electron and the total and projected spin angular momenta <span class="math notranslate nohighlight">\(S\)</span> and <span class="math notranslate nohighlight">\(\Sigma\)</span>. Of the terms in equations <a class="reference internal" href="#equation-xchemcore">(1)</a> and <a class="reference internal" href="#equation-channelfct">(2)</a>, <span class="math notranslate nohighlight">\(\phi_{i}\)</span> is the only term inherently dependent on the oscillatory behaviour of the continuum electron.</p>
<p>The short range states <span class="math notranslate nohighlight">\(\aleph_i\)</span>, as well as the cationic state <span class="math notranslate nohighlight">\(\Phi_{\alpha}\)</span>, contained in the definition of the channel functions, are described exclusively in terms of electrons in bound orbitals. Therefore they may be computed with the standard tools of quantum chemistry (QC), computationally implemented in quantum chemistry packages such as Molcas <span id="id2">[<a class="reference internal" href="references.html#id5" title="Francesco Aquilante, Jochen Autschbach, Rebecca K Carlson, Liviu F Chibotaru, Mickaël G Delcey, Luca De Vico, Nicolas Ferré, Luis Manuel Frutos, Laura Gagliardi, Marco Garavelli, and others. Molcas 8: new capabilities for multiconfigurational quantum chemical calculations across the periodic table. Journal of computational chemistry, 37(5):506–541, 2016. doi:10.1002/jcc.24221.">4</a>]</span> and Molpro <span id="id3">[<a class="reference internal" href="references.html#id8" title="H.-J. Werner, P. J. Knowles, G. Knizia, F. R. Manby, M. Schütz, and others. Molpro, version 2015.1, a package of ab initio programs. 2015. see http://www.molpro.net.">5</a>]</span>. These (and others) have in common, that the electronic wavefunction is expressed in terms of orbitals expanded in sets of Gaussian basis functions centred at the atomic sites of the molecular system; the so called polycentric Gaussian (PCG) basis sets.</p>
<p>In contrast to that, a similar approach is poorly suited for the <span class="math notranslate nohighlight">\(\phi_i(r_{N_e})\)</span>-term of equation <a class="reference internal" href="#equation-xchemcore">(1)</a> due to problems associated with PCGs in expressing the long range behaviour of continuum electrons <span id="id4">[<a class="reference internal" href="references.html#id2" title="C. Marante, L. Argenti, and F. Martín. Hybrid Gaussian B-spline basis for the electronic continuum: Photoionization of atomic Hydrogen. Physical Review A, 90(1):012506, 2014. doi:10.1103/PhysRevA.90.012506.">1</a>, <a class="reference internal" href="references.html#id3" title="C. Marante, M. Klinker, I. Corral, J. González-Vázquez, L. Argenti, and F. Martín. Hybrid-basis close-coupling interface to quantum chemistry packages for the treatment of ionization problems. Journal of Chemical Theory and Computation, 13(2):499–514, 2017. doi:10.1021/acs.jctc.6b00907.">2</a>]</span>. Consequently a hybrid set of B-Spline basis functions and an auxiliary set of monocentric Gaussian (MCG) basis functions, centred at the centre of mass of the the molecule, is used for the description of the continuum electron. This basis set is referred to as a GABS Basis <span id="id5">[<a class="reference internal" href="references.html#id2" title="C. Marante, L. Argenti, and F. Martín. Hybrid Gaussian B-spline basis for the electronic continuum: Photoionization of atomic Hydrogen. Physical Review A, 90(1):012506, 2014. doi:10.1103/PhysRevA.90.012506.">1</a>]</span>.</p>
<p>This approach yields a key advantage inherent in the design of the GABS and PCG basis sets, namely a vanishing overlap between B-Splines and PCGs. This significantly simplifies the evaluation of integrals allowing for the application of the XCHEM code to rather complex systems. This combination of using PCG-based QCPs (for the evaluation of <span class="math notranslate nohighlight">\(\aleph_i\)</span> and <span class="math notranslate nohighlight">\(\Phi_{\alpha}\)</span>) and interfacing them with a GABS based approach (for the representation of <span class="math notranslate nohighlight">\(\phi_{i}\)</span>) to construct physically admissible scattering states <a class="footnote-reference brackets" href="#id7" id="id6" role="doc-noteref"><span class="fn-bracket">[</span>1<span class="fn-bracket">]</span></a> is the approach defining the XCHEM code. More details about the components comprising the XCHEM code may be found in the next chapter.</p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="id7" role="note">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#id6">1</a><span class="fn-bracket">]</span></span>
<p>obtained as solutions to the generalized eigenvalue problem <span class="math notranslate nohighlight">\((\mathbf{H}-\mathbf{S}E)\mathbf{c}=0\)</span> (where <span class="math notranslate nohighlight">\(\mathbf{H}\)</span> and <span class="math notranslate nohighlight">\(\mathbf{S}\)</span> are the Hamiltonian and overlap matrices states obtained by augmenting the <span class="math notranslate nohighlight">\(\Phi_a\)</span> of equation <a class="reference internal" href="#equation-channelfct">(2)</a> with an additional electron), with physicality of the solution being ensured by enforcing correct asymptotic behaviour <span id="id8">[<a class="reference internal" href="references.html#id3" title="C. Marante, M. Klinker, I. Corral, J. González-Vázquez, L. Argenti, and F. Martín. Hybrid-basis close-coupling interface to quantum chemistry packages for the treatment of ionization problems. Journal of Chemical Theory and Computation, 13(2):499–514, 2017. doi:10.1021/acs.jctc.6b00907.">2</a>]</span></p>
</aside>
</aside>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Xchem</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Overview</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#core-ideas">Core Ideas</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Structure and Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="howto.html">Using XCHEM</a></li>
<li class="toctree-l1"><a class="reference internal" href="manual.html">Manual</a></li>
<li class="toctree-l1"><a class="reference internal" href="basis.html">&amp;basis</a></li>
<li class="toctree-l1"><a class="reference internal" href="orbitals.html">&amp;orbitals</a></li>
<li class="toctree-l1"><a class="reference internal" href="integrals.html">&amp;integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="rdm.html">&amp;rdm</a></li>
<li class="toctree-l1"><a class="reference internal" href="qci.html">&amp;qci</a></li>
<li class="toctree-l1"><a class="reference internal" href="bsplines.html">&amp;bsplines</a></li>
<li class="toctree-l1"><a class="reference internal" href="ccm.html">&amp;ccm</a></li>
<li class="toctree-l1"><a class="reference internal" href="boxstates.html">&amp;boxstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="scatstates.html">&amp;scatteringstates</a></li>
<li class="toctree-l1"><a class="reference internal" href="dta.html">&amp;dta</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">&amp;plot</a></li>
<li class="toctree-l1"><a class="reference internal" href="preptdse.html">&amp;preptdse</a></li>
<li class="toctree-l1"><a class="reference internal" href="guide.html">Guide to Parameters</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">Bibliography</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Welcome to Xchem’s documentation!</a></li>
      <li>Next: <a href="theory.html" title="next chapter">Structure and Theory</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2020, Markus Klinker and Vicent J. Borràs.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 4.3.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/overview.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>