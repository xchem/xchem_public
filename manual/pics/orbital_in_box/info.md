# INFO

* Boxes used: 7 and 8 Angstrom
* Orbital density included: 99.9 %
* Orbital 6.1 symmetry in D2h (A~g nitrogen lone pair) from pyrazine
