\chapter{Using XCHEM}

\section{Executing XCHEM}
To run XCHEM in the most straightforward way requires only execution of the following command:

\begin{lstlisting}
	<PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE>
\end{lstlisting}
 
 The following three sections outline the general structure of the input file, the structure of the results, as well as the modifications that need to be made to the workflow if working on a cluster.

\section{Structure of the input file}\label{sec:input_file}
The input file defines all steps that will be carried out by an XCHEM calculation. The input file requests different modules by including 

\begin{lstlisting}
	&<MODULE_NAME>
	param1=val
	param2=val1 val2 
	# lines starting with #,* or ! are treated as comments.
	
\end{lstlisting}

where the parameters following the module name, modify the behaviour of the given module. For a detailed list of available modules and their parameters, see chapter~\ref{ch:manual}. 
\subsection{Meta modules}
The modules  
\begin{lstlisting}
	&exit
\end{lstlisting}

and 

\begin{lstlisting}
	&nowait
\end{lstlisting}

may be used to modify the flow of program. If the former is encountered at any point, the calculation halts. The latter may be used to specify modules to be run in parallel if working on a cluster. For more details see section~\ref{sec:on_clusters}.

\section{Structure of the produced data}\label{sec:output}
By default all data produced by the XCHEM calculation is saved in the directory:

\begin{lstlisting}
	$PWD/XCHEM_data/*
\end{lstlisting}

In it the modules save those results that are relevant for subsequent calculations in different sub-directories. Data produced during the execution, that is not relevant for subsequent modules is saved in 

\begin{lstlisting}
	$PWD/XCHEM_data/<MODULE_DIR>/tmp
\end{lstlisting}

These directories may be deleted upon completion of each individual module. It is recommendable to do so, as these may become quite large. 

\section{Working on a cluster}\label{sec:on_clusters}
While it is possible to treat small systems, with a limited range of angular momenta on a single node, it is likely that larger calculations using XCHEM need to be run on a cluster. \textbf{The current distribution assumes availability of the slurm workload manager.} XCHEM facilitates this by allowing expensive calculations to be executed in two steps: a first cheap step (referred to as setup, see section~\ref{sec:setup}) run locally, and a second more expensive step (referred to as submission, see section~\ref{sec:submission}) distributed over several nodes. The parameter \textbf{defer}, available to all but the basis module is used to control which parts of the calculation to submit.

\subsection{The "defer" parameter}
If defer is set to true in some module, the expensive parts of that module and all subsequent modules are not run immediately in their entirety, but rather are prepared for later submission. \textbf{Note}: Mixing defer=T and defer=F should be done only with great care, as a module run with defer=F, assumes that all modules whose data it depends on have finished in their entirety.
\subsection{Setup}\label{sec:setup}
The execution of the setup step is identical to a simple, local run of XCHEM:
\begin{lstlisting}
	<PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE>
\end{lstlisting}
Afterwards, the directory structured described in section~\ref{sec:output} will have already been created. 
\subsection{Submission}\label{sec:submission}
To submit one or all modules, marked as deferred execute:

\begin{lstlisting}
	<PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE> --submit <MODULE> --remote_dir '<REMOTE_PATH>' --submission_flags '<FLAGS>'
\end{lstlisting}

This will trigger the submission of a series of jobs with dependencies among them set to ensure that they run in the correct order. \textbf{NOTE: The input file must not be modified between the setup and the submission step}. The command line parameters have the following functions:

\begin{enumerate}
\item[$<$\textbf{MODULE}$>$] Chose which module to submit. "all" may be used to submit all modules marked for submission.
\item[$<$\textbf{REMOTE\_PATH}$>$] On most clusters each node has a local scratch space that should be used for read and write operations during the execution of some jobs. This flag may be used to specify this directory. \textbf{Note:} the path for each job should be unique, for this purpose the path should be given in single inverted comas to avoid premature expansion of environment variables such as \$SLURM\_JOBID.
\item[$<$\textbf{FLAGS}$>$] Flags to be passed to sbatch, to specify number of processors, quality if services, requested computational time etc.
\end{enumerate}

\subsection{\&nowait}
The \&nowait meta module may be used to permit the concurrent execution of several modules. If placed between two modules the jobs corresponding to the second module will not wait for completion of those associated with the first module. This is primarily useful to request simultaneous execution of the ccm module with different close coupling expansions or simultaneous execution of the scatteringstates module with different energy ranges (see chapter~\ref{ch:manual} for more details on the modules). 

