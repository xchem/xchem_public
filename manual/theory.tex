\chapter{Structure and Theory}

At the highest level the XCHEM code may be understood as separated into two components dealing with QC and scattering theory. The two components shall be referred to as XCHEM-QC and XCHEM-SCAT, respectively. Running XCHEM-QC corresponds to all steps up to (not including) Part 3 of figure~\ref{fig:xchemFlow}, while XCHEM-SCAT corresponds to Part 3. In the following paragraph the theoretical background underlying each component is outlined to help understand the specifications that need to be made in the input files. The outlines are deliberately concise, and for more details the reader is referred to the references given in the text.

\section{XCHEM-QC}

XCHEM-QC carries out all those calculations, that do not involve B-spline basis functions and therefore frequently allow the use of QCPs. Broadly speaking, XCHEM-QC starts from a set of molecular orbitals obtained in a CASSCF or RASSCF calculation, used to describe a set of short range states $\aleph$ and parent ion (PI) states $\Phi_a$ which are subsequently augmented with an additional electron in some orbital $\varphi_i$ (giving neutral states if working with singly charged PIs, as is frequently the case). Operator matrix elements of the augmented states for one ($\hat{\mathcal{O}}^1$) and two ($\hat{\mathcal{O}}^2$) electron operators are then computed via

\begin{eqnarray}
	\mathcal{O}^{1}_{IJ} &=& \sum_{ij} D_{ij}^{IJ} \int d\mathbf{r}\varphi_i(\mathbf{r})\hat{\mathcal{O}}^1\varphi_j(\mathbf{r}) \label{eq:oneElOpMat}\\
	\mathcal{O}^{2}_{IJ} &=& \sum_{ijkl} d_{ijkl}^{IJ}\int d\mathbf{r}_1d\mathbf{r}_2\varphi_i(\mathbf{r}_1)\varphi_j(\mathbf{r}_1)\hat{\mathcal{O}}^2\varphi_k(\mathbf{r}_2)\varphi_l(\mathbf{r}_2)\label{eq:twoElOpMat}
\end{eqnarray}

where $D$ and $d$ are the one and two electron reduced density matrices, respectively. The set of orbitals $\{\varphi_i\}$ used to augment the PIs in, comprises the active space orbitals (ASO) $\varphi^{a}$ used to compute the PIs (expanded in a standard set of PCG basis functions $b^P$), as well as a set of diffuse orbitals (DO) $\varphi^{d}$ expanded in the auxiliary MCG basis $b^{M}$ and orthogonal to $\varphi^{a}$.

Computation of the integrals appearing in equations~\ref{eq:oneElOpMat} and~\ref{eq:twoElOpMat} requires knowledge of the integrals of PCG and MCG basis functions. These are computed using Molcas' Seward module. The basis coefficients of $\varphi^{a}$ are known and those of $\varphi^{d}$ are computed ensuring orthonormality. 

Computation of the reduced density matrices requires knowledge of the CI vectors of $\aleph$ and $\hat{a}_i^{\dagger}\Phi_a$, where $\hat{a}_i^{\dagger}$ creates an electron in orbital $\varphi_i$ (being either an ASO or a DO). For $\aleph$ these are known. For $\hat{a}_i^{\dagger}\Phi_a$, the expansion of $\Phi_a$ in terms of configuration state functions (CSF) is converted to an expansion in terms of Slater determinants by using the graphical unitary group approach (the relevant GUGA tables may be computed with OpenMOLCAS' RASSCF module). Application of $\hat{a}_i$ to Slater determinants is trivial and states of the required spin may be obtained be reverting to a CSF description using the appropriate GUGA table for the augmented system.

The operators whose matrix elements need to be computed in this way, are the one and two electron Hamiltonian, the overlap as well as the electrical dipole moment $\mathbf{r}$ (length gauge) and canonical momentum $\mathbf{p}$ (velocity gauge).

\section{XCHEM-SCAT}

The XCHEM-SCAT code introduces the B-spline part of the GABS basis, to be able to describe the long range oscillatory behaviour of continuum electrons. The GABS basis is characterized by two key parameters:
\begin{enumerate*}
\item[\textbf{a)}] the radius $R_0$ with all Bspline $B(r<R_0)=0$ and chosen so that $|\varphi^a(r>R_0)|<<1$, and
\item[\textbf{b)}] the radius $R_1$ such that $R_0<R_1$ and $|\varphi^d(r>R_1)|<<1$
\end{enumerate*}. 
Figure~\ref{fig:GABS3d} schematically visualizes the key features of all three types of basis functions present for an XCHEM calculations (PCG, MCG and BSpline). The lack of overlap between PCGs and B-splines means that the computation of the operator matrix elements for two augmented states of which at least one is augmented in a B-spline orbital can be simplified according to\cite{Marante2017}

\begin{equation}
	\mathcal{O}=\braket{\hat{\mathcal{A}}\Upsilon|\hat{\mathcal{O}}|\hat{\mathcal{A}}\Upsilon}=\braket{\Upsilon|\hat{\mathcal{O}}|\Upsilon}.
\end{equation}

From the matrix elements one may then obtain scattering states (by imposing the asymptotically correct boundary conditions for photoionization problems) and compute observable quantities such as the photo ionization cross sections or use the box eigenstates as a basis for the solution of the time dependent Schr\"odinger Equation.

\begin{figure}[!tbh]
\centering
\includegraphics[width=0.7\textwidth]{pics/GABS3D}
\caption{Schematic depiction of relevant basis functions. The PCG for the description of $\Phi_a$ and $\aleph$ are shown in blue and black radially extending no further than $R_0$. The MCGs are show in magenta and are assumed to be negligible beyond $R_1$. The B-splines are shown in cyan and extend from $R_0$ to the end of the box. Clearly visible is the absence of a radial region in which both B-splines and PCGs are non-zero.}\label{fig:GABS3d}
\end{figure}

