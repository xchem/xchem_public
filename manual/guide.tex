\chapter{Guide to Parameters}
\section{Choosing the Bases}
As pointed out previously the basis may be seen as composed of polycentric Gaussians, monocentric Gaussians and B-splines. Making good choices for these basis sets is crucial for obtaining good results in acceptable computational time. The following three paragraphs outline the considerations one should make for each of these basis sets.
\subsection{Polycentric Gaussians}\label{sec:choosepcg}
The polycentric Gaussians take care of the description of the short range structure of parent ion states and source states. As such a basis set should be chosen that is flexible enough to give a good description of those states. Care should be taken not to include very diffuse polycentric Gaussians, as that would require increasing $R_0$ which in turn requires the use of a large monocentric Gaussian basis. 

As a method to check the viability of the PCGs used, it is recommendable to plot or otherwise inspect the radial behaviour of the active space orbitals constructed from PCGs, as exemplified in figures~\ref{fig:rad1} and~\ref{fig:rad2}.

\subsection{Monocentric Gaussians}
For most purposes the default values that define the exponents used in the monocentric Gaussian basis, need not be modified. The key parameters in choosing the monocentric Gaussian basis are \textit{lMonocentric} and \textit{kMonocentric}. In general smaller values of $l$ require larger values of $k$ to ensure the same radial flexibility. Therefore for applications requiring a small maximum value for $l\leq3$ (e.g. small molecules with high symmetry), it is usually sufficient to choose the same maximum value for $k$ for every $l$ (see section~\ref{sec:basis} for details). For more complex systems it may be useful to choose large values of $k$ (e.g. $3$) for small $l$ (e.g. $\leq2$) and smaller values for $k$ (e.g. $0$) for larger l $l\geq 4$. Furthermore the need for higher angular momenta increases for scattering states of larger energies; in figure~\ref{fig:artifacts} the unphysical slow oscillations around the correct cross sections are a consequence of insufficient \textit{lMonocentric} at the affected energies. 

\subsection{B-splines}
The key parameters to set up the B-spline basis are \textit{rmin}, \textit{rmax} and \textit{numberNodes}. 
\subsubsection{rmin}
Finding a good choice for \textit{rmin} follows essentially the path outlined in section~\ref{sec:choosepcg}. Negligible overlap between the B-splines and active space orbitals, used to express the source and parent ion states, must be ensured. Figure~\ref{fig:rad2} and~\ref{fig:rad1} show how a choice for \textit{rmin}, guaranteing neglible overlap, may be made.
\subsubsection{rmax}
\textit{rmax} must be chose large enough to avoid the following complication: computing scattering states just above an ionisation threshold corresponds to ejection of electrons with a very long wavelength. For an accurate representation of the scattering states of a given energy it must be ensured that at least a few oscillations of the electronic wave function fit into the box demarcated by \textit{rmax}. Furthermore, if using a complex absober, \textit{rmax} may need to be increased to accomodate it.
\subsubsection{numberNodes} The number of nodes determines the density of B-splines. It should be ensured that the chose B-spline basis is capable of representing the oscillations of an electron ejected at the maximum kinetic energy included in the calculation; in figure~\ref{fig:artifacts} the unphysical fast oscillations around the correct cross sections are a consequence of insufficient \textit{numberNodes} at the affected energies.


\begin{figure}[!tbh]
\centering
\includegraphics[width=0.7\textwidth]{pics/rad1}
\caption{Radial behaviour of active space orbitals of a calculation for NO$_2$. In choosing the PCGs, the active space, and $R_0$ a compromise should be aimed for that allows choosing $R_0$, so that no significant part of the active PCG orbitals is allowed to overlap with B-Splines. In this example $R_0=7$ would be a reasonable choice, given that more than 99.9\% of each of the active space orbitals is contained to within a radius of $7$ atomic units.}\label{fig:rad1}
\end{figure}

\begin{figure}[!tbh]
\centering
\includegraphics[width=0.7\textwidth]{pics/orbs_pyrazine}
\caption{ISO surface of an active space orbital of pyrazine cropped to different box sizes. The interior of the manifold contains 99.9\% of the depicted orbital. Left: the manifold cropped to a box of 8 a.u.; it is fully contained in the box. Right: the manifold cropped to a box of 7 a.u.; protruding sections are cut away. A similar analysis of all active space orbitals, would allow choosing an appropriate value for $R_0$}\label{fig:rad2}
\end{figure}

\begin{figure}[!tbh]
\centering
\includegraphics[width=0.7\textwidth]{pics/h}
\caption{Comparison of photoionization cross section of atomic hydrogen obtained analytically (black) and with XCHEM (coloured). The effects of poorly chosen parameters for the basis sets manifest themselves in different ways: In sufficient angular momentum in the monocentric Gaussian basis leads to slow unphysical oscillations around the correct solution, it can be seen clearly that increasing $l$ remedies this behaviour. Insufficient density of B-splines leads to jagged, fast oscillation before incorrectly predicting zero cross section. Increasing the B-spline density would yield smooth curves.}\label{fig:artifacts}
\end{figure}

