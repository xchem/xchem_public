\chapter{Manual}\label{ch:manual}
This chapter outlines the modules and their parameters that may be included in the input file for an XCHEM calculation. The structure of the input file was outlined in~\ref{sec:input_file}. Figure~\ref{fig:flow} shows the dependency of the different modules on each other. 

\begin{figure}[!tbh]
\centering
\includegraphics[width=1.0\textwidth]{pics/flow}
\caption{Flow chart showing the dependencies among the different modules making up XCHEM. The dependency of \textit{scatteringstates} on \textit{boxstates} (dotted arrow) may be ignored if the parameters
 \textit{cm} in \textit{scatteringstates} is set to \textit{hsle}. The \textit{plot} module will produce plots, as long as at least one of the dependencies indicated by the blue arrows is fulfilled.}\label{fig:flow}
\end{figure}

\section{\&basis} \label{sec:basis}
This module has two main purposes: on the one hand it computes the one electron integrals between all relevant Gaussian basis functions (including both monocentric and polycentric Gaussians). On the other hand it extracts a number of parameters that are required for subsequent modules. The computational effort of the module is small relative to the an entire computation.

\subsection{Parameters}
\begin{enumerate}
\item[\textbf{gatewayFile}] One of the two files that need to be prepared prior to the execution of XCHEM (apart from the input file itself. The other is the orbital file required for the orbitals module). Specifies the name of the file containing the polycentric bases to be used (such as for example cc-pVQZ) and the nuclear geometry. The file needs to be a valid input to the Gateway module of Molcas (see Molcas documentation for more details). 
\item[\textbf{lMonocentric}] Specifies the maximum value for $l$ to be used in the monocentric Gaussian basis set. If one value is given all values $0\cdots l$ will be included with equal maximum values for $k$ (see kMonocentric keyword). If more than one value is given, the user may specify the maximum value of $k$ for each $l$. If providing more than one value the values must be consecutive and starting at $0$. 
\item[\textbf{kMonocentric}]
Specifies the maximum value for $k$ to be used in the monocentric Gaussian basis set. If one value is given it will be applied used for all value of $l$ small or equal to the provided kMonocentric. Otherwise each field defines the maximum value of $k$ for the corresponding field provided to lMonocentric.
\item[\textbf{nexp}] Number of exponents to use in the even tempered monocentric basis. Default: nexp=22
\item[\textbf{alpha0}] The value of $\alpha_0$ to be used in defining the monocentric Gaussian exponents via the formula $\alpha_i = \alpha_0 \beta^{i-1}$. Default: alpha0 = 0.01
\item[\textbf{beta}] The value of $beta$ to be used in defining the monocentric Gaussian exponents via the formula $\alpha_i = \alpha_0 \beta^{i-1}$. Default: beta=1.46
\item[\textbf{multighost}] If set to true different optimised basis sets, based on the default values for nexp, alpha0 and beta, are used for monocentric Gaussian basis sets of different angular momenta. Default: True
\item[\textbf{sewardOptions}] May be used to provide one or several options to be provided to Molcas' Seward module (used to calculate the Gaussian integrals).
\end{enumerate}

\section{\&integrals}
This module calculates the relevant two electron integrals for monocentric and polycentric Gaussian basis functions. In many cases this module will account for a considerable percentage of total computational time. Its cost grows rather quickly for increasing values of kMonocentric and lMonocentric. Running the calculation on a single node it is not recommendable to have values larger than $3$ and $2$ for kMonocentric and lMonocentric, respectively. Across multiple nodes value of lMonocentric$=6$ and kMonocentric$=1$ are possible but may take several days even if run on architectures with $10$ nodes of $16$ processors each. 

\subsection{Parameters}
\begin{enumerate}
\item[\textbf{defer}] Defer as much of the calculation as possible, to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&orbitals}
This module uses the provided polycentric Gaussian orbitals, to create an orthogonal set of monocentric (diffuse) Gaussian basis functions. Subsequently it transforms all integrals from basis functions to orbitals (this is equivalent to the Motra module of Molcas). If using large values for lMonocentric and kMonocentric this latter part of the calculation may take a couple of hours on a single node.
\subsection{Parameters}
\begin{enumerate}
\item[\textbf{orbitalFile}] One of the two files that need to be prepared prior to the execution of XCHEM (apart from the input file itself, the other is the gateway file required for the \textit{basis} module). Specifies the name of the file containing the polycentric Gaussian orbitals to be used to represent both the parent ions and source states. The file must be in Molcas' INPORB format.
\item[\textbf{inac}] Number of inactive orbitals in each irreducible representation of the point group, used to specify the molecule (see Molcas documentation for more details). Default: all 0
\item[\textbf{ras2}] Number of active orbitals in each irreducible representation of the point group, used to specify the molecule. All possible excitations are considered within the ras2 space (see Molcas documentation for more details). 
\item[\textbf{ras1}] Number of ras1 orbitals in each irreducible representation of the point group, used to specify the molecule. Only excitation up to a maximum number of holes are permitted in this space (see Molcas documentation for more details). Default: all 0
\item[\textbf{ras3}] Number of ras3 orbitals in each irreducible representation of the point group, used to specify the molecule. Only excitation up to a maximum number of electrons are permitted in this space (see Molcas documentation for more details). Default: all 0
\item[\textbf{linDepThr}] Threshold to be used in creating the orthogonal monocentric Gaussian orbitals. Default: $10^{-8}$
\item[\textbf{defer}] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}
\subsection{\&rdm}
The \textit{rdm} (reduced density matrix) module computes the one and two electron density matrices between all source states and states resulting from augmentation of parent ions with an additional electron, in either an active polycentric Gaussian orbital or a diffuse monocentric Gaussian orbital. Subsequently all relevant operator matrix elements are computed (this is equivalent to the Rassi module of Molcas).

\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{nact} ] Define the number of electrons in the system. The keyword accepts up to three numbers. The first specifies the number of electrons in the ras2 space. The second and third specify the number of holes and electrons in the ras1 and ras3 spaces, respectively. 
\item[ \textbf{symmParentStates} ] Define groups of parent ions by specifying their symmetry. The same symmetry may appear multiple times to specify, for example, two sets of states in $B_{1u}$ symmetry with different multiplicities.
\item[ \textbf{spinParentStates} ] Define the multiplicity for each group defined in symmParentStates.

\item[ \textbf{ciroParentStates} ] Specify which or how many states to include for each group of states. If an integer $i$ is provided, Molcas will be used to create the CI vectors corresponding to the $i$ energetically lowest states of given symmetry and multiplicity. It will do so without re-optimising the orbitals. If a string $s$ is given the code will try to read the CI vectors (expressed in determinants) from the file $s$ (see section~\ref{sec:determinantfiles} for details on the format and how to create such files). 

\item[ \textbf{coreHoleParentIon} ] Use only in combination with providing integers to ciroParentStates. For each group specify if in calculating the CI vectors Molcas' hexs keyword should be applied. Useful for the treatment of core hole states (see Molcas documentation for more details). Default: False
\item[ \textbf{spinAugmentingElectron} ] Specify the spin of the electron to be augmented with as either "alpha" or "beta".
\item[ \textbf{spinAugmentedStates} ] Specify the multiplicity of ALL source states. Note that as currently treatment of spin orbit coupling is not included, there is no need to specify source states of different multiplicity as they have no chance of coupling. 

%\item[ nameSourceStates ] 
\item[ \textbf{symmSourceStates} ] Equivalent to symmParentStates, but used to specify short range states with one more electron compared to the parent ion states. Default: None
\item[ \textbf{ciroSourceStates} ] Equivalent to ciroParentStates, but used to specify short range states with one more electron compared to the parent ion states. Default: None
\item[ \textbf{coreHoleSource} ] Equivalent to coreHoleParentIon, but used to specify short range states with one more electron compared to the parent ion states. Default: False

\item[ \textbf{gugaDir} ] The computation of guga tables may be expensive for active spaces that contain in excess of $10^5$ configuration state functions. As the structure of the guga table only depends on the active space, and not on the geometry, orbitals, or choices of states, the name of a directory may be specified, containing the relevant files of guga tables. Typically this would be used if treating the same system for multiple geometries. In that case the file "*.guga" should be save from the rdm/tmp directory and the path be provided to this keyword. Default: None
\item[ \textbf{forceC1} ] Treat all augmented states to be of C1 symmetry. This does not mean the states orbitals lose their symmetry, only that in calculating matrix elements, their symmetry is not considered. At present it is not recommended to change this keyword from its default value. Default: True
\item[ \textbf{maxMultipoleOrder} ] Define the maximum multiple order to be computed for later use in the multipole expansion carried out for the computation of the Hamiltonian matrix including B-spline orbitals. Default: 6.
\item[ \textbf{defer} ] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&bsplines}
This module calculates the integrals between B-splines and monocentric Gaussian basis functions. Furthermore it converts the results from the rdm modules to be treatable as a scattering problem. The parameters of this module must be chose with care as all subsequent modules depend on them, making changes potentially costly. 

\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{ rmin }] Defines the radius in atomic units from the origin at which the B-spline part of the GABs basis starts. This should be chose so that the polycentric Gaussian  orbitals used to describe the parent ions and source states only protrude beyond it to a negligible extent.
\item[ \textbf{ rmax }] Defines the radius in atomic units at which the B-spline basis ends. In order to obtain correct scattering states, it should be ensured that at least a few oscillations of the electron being ejected can be represented in the resulting box. 
\item[ \textbf{ numberNodes }] Defines the number of nodes used to create the B-spline basis. A reasonable first guess is two nodes per atomic unit, though higher B-spline densities may be necessary, depending on the kinetic energy of the ejected electron (larger kinetic energies require larger densities). 
\item[ \textbf{ closeCouplingChannels }]
Defines the close coupling expansion (i.e. ejected electrons of which angular momenta should "leave behind" which parent ions). The format for this is $\mathrm{ccm}=\Phi_1 (l^1_1\;m^1_1,\cdots,l^1_{n_1}\;m^1_{n_1});\cdots;\Phi_i (l^i_1\;m^i_1,\cdots,l^i_{n_i}\;m^i_{n_i})$, where $\Phi_i$ is the $i^{\mathrm{th}}$ parent ion defined via ciroParentStates. For example $\mathrm{ccm}=1 (0\;0,2\;0); 2(2\;0),3 (1\;-1,1\;1); 4(1\;-1, 1\;1)$ in combination with $\mathrm{ciroParentStates}=2\;2$ (used in the rdm module) would couple the the first state of the first group of states to electrons with angular momenta $l=0,2$ both with $m=0$, the second state of the first group of states to electrons with $l=0$ and $m=0$, and both states of the second group of states to electrons with $l=1$ and $m=\pm 1$.

\item[ \textbf{ cap }] Define complex absorbing potentials (CAP) to be included in the calculation. CAPs may be useful for subsequent time dependent calculations to avoid reflections. Furthermore the complex eigenvalues of the quenched Hamiltonians may give direct insight into the life times of auto ionising states. In general the CAPs are of the shape: $V_{n,r_0,z}(r)=zr^n\theta(r-r_0)$. Each CAP to be included may thus be defined via three or four numbers. If three numbers are given they correspond to: order of polynomial growths $n$, starting point $r_0$, and strength $\mathrm{Im}(z)$. If a fourth number is given it corresponds to $\mathrm{Re}(z)$, which may be used to accelerate the wave packet once it reaches the absorber. To include multiple CAPs, sets of parameters may be provided and should be separated by commas. 

\item[ \textbf{ splineOrder }] The order of the B-splines in the GABs basis. The default value is normally appropriate. Default: 7
\item[ \textbf{ matrixElementThreshold }] Minimum overlap between primitive Gaussian functions and BSplines. Default: $10^{-24}$
\item[ \textbf{ gaussianBSplineOverlapThreshold }] Minimum overlap between preconditioned Gaussian functions and BSplines. Default: $10^{-16}$
\item[ \textbf{ gaussianProjectorThreshold }] Minimum deviation from 1 of the eigenvalues of the projector on the preconditioned gaussian basis expressed in the BSpline basis required to consider the linear combination of BSpline independent from the preconditioned Gaussian basis. Default: $10^{-8}$
\item[ \textbf{ plotBasis }] If true, plots of the B-spline basis functions will be produced. 
\item[ \textbf{ nPlot }] Radial grid points to use for plotting of B-splines. Default: 1000.
\item[ \textbf{ nBSplineSkip }] Define the first $n$ B-splines to be neglected, this is used to ensure that the wave functions is smooth at rmin. The default value is normally appropriate. Default: 3
\item[ \textbf{ defer }] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}


\section{\&ccm}  This module computes the matrix elements between the source states and all possible augmented states (i.e. all states obtained by augmenting the parent ions with another electron of angular momenta included in the close coupling expansion, in either Gaussian or B-spline orbitals). 

\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{ closeCouplingChannels }] Identical to the same keyword in the module \textit{bsplines}; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, as the ccm module may become quite costly for large close coupling expansions. 
\item[ \textbf{ operators }] Specify which matrix elements to compute. Valid choices are H (for the Hamiltonian), S (for the Overlap), CAP (for the CAPs, if included) and Dipole. Default: H S Dipole
\item[ \textbf{ axes }]
If the dipoles are required, select which components should be calculated. Valid choices are x, y, and z. Default: x y z.
%\item[ \textbf{ brasym }]

%\item[ \textbf{ cond }]
%\item[ \textbf{ ketsym }]
\item[ \textbf{ gauges }] If the dipoles are requested, select which gauges to compute them in. Valid choices are v (for velocity) and l (for length). Default: l v.
\item[ \textbf{ defer }] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&boxstates} 
Request the diagonalisation of the close coupling (quenched) Hamiltonian. 

\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{ closeCouplingChannels }]Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the life times and or energies of the eigen states. 
\item[ \textbf{ loadLocStates }] If true the source states are included in the diagonalisation. Default: True.
%\item[ \textbf{ brasym }]
%\item[ \textbf{ conditionNumber }]
%\item[ \textbf{ numBsDropBeginning }]
%\item[ \textbf{ conditionBsThreshold }]
%\item[ \textbf{ numBsDropEnding }]
%\item[ \textbf{ cond }]
%\item[ \textbf{ conditionBsMethod }]
\item[ \textbf{ defer }] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&scatteringstates} 
This module computes scattering states of the system by fitting to the asymptotically correct solution. It also compute the relative scattering phases by diagonalising the S matrix. 

\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{ closeCouplingChannels }] Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the scattering states. 
\item[ \textbf{ emin }] Choose the lowest absolute energy in atomic units, of the scattering states to be computed.
\item[ \textbf{ emax }] Choose the highest absolute energy in atomic units, of the scattering states to be computed. \textbf{WARNING:} emin and emax \textbf{may not} lie on either side of an ionisation threshold, i.e. an energy at which some channels change from closed to open. The module should be requested multiple times with different values for emin and emax for the analysis of scattering states between different pairs of ionisation thresholds.
\item[ \textbf{ ne }] Number of scattering states to compute between emin and emax.
\item[ \textbf{ cm }] Method used to compute the scattering states, i.e. finding $M$ non-trivial solutions to the equation 
\begin{equation} \label{eq:SENum}
	(\mathbf{H}-E\mathbf{S})\Psi = 0,
\end{equation}
where $M$ is the number of open channels at energy $E$. Valid choices are: hsle (solve homogeneous system using LU decomoposition $\mathbf{H}-E\mathbf{S}=\mathbf{L}\mathbf{U}$), invm (write~\ref{eq:SENum} as $[\mathbf{A}|\mathbf{B}]=\left[\frac{\tilde{\Psi}}{-\mathbf{I}}\right]$ and invert $\mathbf{A}$ to obtain $\Psi = \mathbf{A}^{-1}\mathbf{B}$), and spec (invert $A$ of invm method more efficiently by writing $\mathbf{A} = \mathbf{\Phi}(\mathbf{E}-E)\mathbf{\Phi}^{\dagger}$), where $\mathbf{\Phi}$ and $\mathbf{E}$ are the matrix of box eigenstates and diagonal matrix of box eigenvalues, respectively. Default: hsle. 
\item[ \textbf{ scattConditionNumber }] Conditioning number used to remove linear dependencies in calculating scattering states. Default= $10^{-10}$
\item[ \textbf{ loadLocStates }] See module boxstates.
%\item[ \textbf{ cond }]
%\item[ \textbf{ numBsDropBeginning }]
%\item[ \textbf{ conditionBsThreshold }]
%\item[ \textbf{ conditionBsMethod }]
%\item[ \textbf{ numBsDropEnding }]
%\item[ \textbf{ conditionNumber }]
%\item[ \textbf{ brasym }]
\item[ \textbf{ defer }] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&dta} 

\subsection{Parameters}
This module computes the dipole transition amplitudes between bound and scattering states. They may be used directly to compute the (partial) photo ionisation cross sections, or in a time dependent calculation, to correctly describe the impact of a laser field. 
\begin{enumerate}
\item[ \textbf{ closeCouplingChannels }]  Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the photo ionisation cross section. 
\item[ \textbf{ trans }] Choose which kind of transition amplitudes to calculation. Valid options: cb (scattering to bound), bc (bound to scattering), bb (bound to bound), and cc (scattering to scattering). Default= vb 
\item[ \textbf{ boxi }] If trans is set up to include bound states, defines which bound state (in order of increasing eigen energies) to use in calculating the transition amplitudes. (If trans=bb, the transition amplitudes between the chooses one and all other box eigen states will be computed). Default: 1
\item[ \textbf{ braemin }] If trans=cb, defines the energy of the least energetic, available scattering states to be included in the calculation. $\mathrm{Default}=-\infty$.
\item[ \textbf{ braemax }] If trans=cb, defines the energy of the most energetic, available scattering states to be included in the calculation. $\mathrm{Default}=\infty$. 
\item[ \textbf{ ketemin }] If trans=bc, defines the energy of the least energetic, available scattering states to be included in the calculation. $\mathrm{Default}=-\infty$.
\item[ \textbf{ ketemax }] If trans=bc, defines the energy of the most energetic, available scattering states to be included in the calculation. $\mathrm{Default}=\infty$. 
%\item[ \textbf{ operators }]
\item[ \textbf{ gauges }] Analogous to the same keyword of the module ccm. 
\item[ \textbf{ axes }] Analogous to the same keyword of the module ccm.
\item[ \textbf{ cm }] If trans is set up to include scattering states, select the method that was used to compute scattering states to be used in calculating the transition amplitudes. Default = hsle.
\item[ \textbf{ scattConditionNumber }] If trans is set up to include scattering states, select the threshold that was used to compute scattering states to be used in calculating the transition amplitudes. Default= $10^{-10}$.
%\item[ \textbf{ sinasymp }]
%\item[ \textbf{ loadLocStates }]
%\item[ \textbf{ numBsDropEnding }]
%\item[ \textbf{ conditionBsThreshold }]
%\item[ \textbf{ numBsDropBeginning }]
%\item[ \textbf{ conditionBsMethod }]
%\item[ \textbf{ cond }]
%\item[ \textbf{ brasym }]

\end{enumerate}

\section{\&preptdse} 
This modules transforms the close coupling matrix of the dipole operator to the basis in which the Hamiltonian is diagonal. This resulting matrices may be used in time dependent calculations that propagate in the eigen states of the Hamiltonian. \textbf{Note}: This module \textbf{does not} perform propagations, it only prepares the matrices to facilitate their use in a propagation. 
\subsection{Parameters}
\begin{enumerate}
\item[ \textbf{ closeCouplingChannels }] Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the time dependent analysis of photoionization processes.
\item[ \textbf{ gauges }] Analogous to the same keyword of the module ccm.
\item[ \textbf{ axes }] Analogous to the same keyword of the module ccm.
%\item[ \textbf{ loadLocStates }] 
%\item[ \textbf{ cond }]
%\item[ \textbf{ numBsDropBeginning }]
%\item[ \textbf{ numBsDropEnding }]
%\item[ \textbf{ conditionNumber }]
%\item[ \textbf{ conditionBsThreshold }]
%\item[ \textbf{ conditionBsMethod }]
%\item[ \textbf{ brasym }]
\item[ \textbf{ defer }] Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
\end{enumerate}

\section{\&plot} This module produces some first guess plots to analyse: the eigen energies of the (quenched) Hamiltonian, the scattering phases, and the dipole transition amplitudes. \textbf{Note}: this module requires the python package matplotlib.




 
