\chapter{The XCHEM code}

The \XCHEM code can be viewed as split into two parts: on the one hand \XCHEMQC using (modified) QCPs to generate all those terms of equation~\ref{eq:xchemCore} that do not need B-Splines, and on the other hand {\footnotesize\textsc{XCHEM-SCAT}}, which, using the results from {\footnotesize\textsc{XCHEM-QC}}, adds the B-Splines part of the GABS basis to complete the creation of augmented states and thus allow the computation of molecular scattering states. These to sub-codes run independently, and as such the following two sections will outline the structure of either code. The respective roles of the \XCHEMQC and \XCHEMSCAT are shown in figure~\ref{fig:xchemFlow}, with parts 1 and 2 comprising \XCHEMQC and part 3 amounting to \XCHEMSCAT. The following provides an overview over these two sub-codes. Furthermore in each of these sections a paragraph is dedicated to technical details regarding the implementation of the code, structure of the generated output data, as well as prerequisites necessary for the successful execution.


\section{XCHEM-QC}
The \XCHEMQC code generates the cat-ionic states using the State Average Complete Active Space Self Consistent Field (SA-CASSCF) methodology, and makes the first step to generating the scattering states by augmenting these cationic states with an additional electron. The augmentation is done in both, PCG orbitals included in the active space used to compute the cationic states, as well as in a set of diffuse orbitals generated using the MCGs contained in the GABS basis at hand. The user must specify the molecular geometry, the details of the active space involved, the PCG basis set, the specifications of the MCGs (maximum values for quantum numbers $l$ and $k$), and a \emph{wf}-file generated with Molpro containing the orbitals the user wishes to express the cationic states in. 

Running the \XCHEMQC involves the execution of the four scripts shown in the flow diagram in figure~\ref{fig:xchemQCFlow}a, creating the folder structure shown in figure~\ref{fig:xchemQCFlow}b. All four scripts runInt.sh, runCas.sh, runOrb.sh and runFinish.sh may be found in the directory XCHEM-QC. They call all necessary executables (located in XCHEM-QC/bin whose source code is in XCHEM-QC/src) and parsing scripts (located in XCHEM-QC/parse). The remaining directories found in the XCHEM-QC directory contain MCG basis sets (XCHEM-QC/ghostLibrary), group tables (XCHEM-QC/data), necessary templates (XCHEM-QC/templates) and documentation (XCHEM-QC/man). After execution of the aforementioned scripts, the data relevant for the subsequent execution of the \XCHEMSCAT code is located in the orb\_*/ directory (see figure~\ref{fig:xchemQCFlow}b).

\subsection*{Technical Details}
The programs executed by the different run scripts fall into two categories: programs included in the Molcas or Molpro QCP (some of which have been modified) as well as programs developed from scratch interfacing with said QCPs. All code is written in Fortran (including the QCPs), while some additional parsing is done by Python. Furthermore a patch needs to be applied to both the Molcas and Molpro source codes to accommodate the features of \XCHEMQC (the patch files are stored in XCHEM-QC/QCPPatch). As such in order to run \XCHEMQC the user must have access to:

\begin{description}
    [align=left,style=nextline,leftmargin=*,labelsep=\parindent,font=\normalfont]
	\item[\textsc{ifort15}] Fortran Compiler necessary for compilation of both QCP packages, and newly developed code. 
	\item[\textsc{Molcas8.0}] The QCP Molcas is required for many of the QC calculations. Furthermore, as modification to its source code are necessary, the user must be in possession of a license to Molcas8.0
	\item[\textsc{Molpro2010.1}] The QCP Molpro is needed for its capability to carry out SA-CASSCF calculation with a state average over states of different symmetries (the same is not available in Molcas). 
	\item[\textsc{python2.7}] Necessary for several parsing scripts of QCP output. 
	\item[\textsc{lapack}] Package of linear algebra subroutines.
\end{description}

\begin{figure}[!tbh]
	\centering
	\includegraphics[width=\textwidth]{pics/QC-Folders}
	\caption{The main steps involved in the execution of \XCHEMQC. A) The four steps necessary to produce the data required by the \XCHEMSCAT code. The first two steps may be run concurrently and generate the CI-vectors of the augmented states (runCas.sh) and the basis integrals including the MCG basis functions (runInt.sh), respectively. The third step (runOrb.sh) creates the operator matrix elementes of the augmented states using the results from the previous two steps, while the final step computes (runFinish.sh) prepares the data for treatment with the \XCHEMSCAT code.	B) Structure of the folders created be the four steps of A). The cas\_*, int\_* and orb\_* directories get created by the runCas, runInt and runOrb scripts, respectively and their names encode information about the chosen active space and the angular momenta used in the MCG basis.}\label{fig:xchemQCFlow}
\end{figure}


\section{XCHEM-SCAT}
Upon the successful execution of {\footnotesize\textsc{XCHEM-QC}}, the generated data may be used by \XCHEMSCAT to calculate quantities relevant to molecular photoionization such as, for example, photoionization cross sections. The user must set parameters that define which channels are to be included in the close coupling expansion (each channel is classified by a cationic state and the angular momentum of the ejected electron, see equation~\ref{eq:channelFct}), details of the B-Spline part of the GABS basis to be used (such as number and order of the B-Splines and the size of the box) as well as ranges of energies in which the scattering states (and subsequent, if desired, photoionization cross sections) are to be computed. Furthermore the user must specify the directory where the \XCHEMQC data is stored, as well as the directories where the \XCHEMSCAT output is to be stored. The executables of all these program may be found in XCHEM-SCAT/bin\_ifort/ (the relevant source files are stored in XCHEM-SCAT/programs/*/src)

The structure of this program, from the most general point of view is shown in figure~\ref{fig:xchemScatFlow}, outlining the dependencies of the various codes that must be executed. The ostensibly complex structure of the resulting work-flow is aided by several scripts automatizing many of the steps. These scripts are stored in XCHEM-SCAT/programs/run.

\subsection*{Technical Details}
The \XCHEMSCAT code was developed from scratch entirely in Fortran. As such its only prerequisites are the Fortran compiler ifort and the linear algebra package lapack, both of which are contained in the prerequisite of \XCHEMQC also. Therefore successful execution of \XCHEMQC should guarantee that the system is also prepared to run \XCHEMSCAT successfully.

\begin{figure}[!tbh]
	\centering
	\includegraphics[width=\textwidth]{pics/scatFlow}
	\caption{Flow diagram for the \XCHEMSCAT code. Orange boxes indicate input files (IF). The GABS IF defines the GABS basis to be used, while the XCHEM IF specifies the channels of the close couplings expansion as well as parameters on the computational methods to be used. The matrix elements of all augmented states (augmented in PCG, MCG or B-Spline orbitals) are computed by the BuildSymmetricElectronicSpace program, which can subsequently be used to evaluate scattering states (ComputeMultichannelScatteringStates) and photoionization cross sections (ComputeDipoleTransitionAmplitudes). Sevaral optional programs are available; such as inclusion of a complex absorbing potential (CAP) (BuildConditionedMatrices), computations of the box eigenstates (ComputeSymmetricElectronicSpaceBoxEigenstates, this necessary for some methods available to compute the scattering states) and verification of the virial theorem to test quality of the data.}\label{fig:xchemScatFlow}
\end{figure}


\section{Authorship}
Of the provided codes, we \textbf{do not} claim ownership over the following programs:
\subsection*{XCHEM-QC}
\begin{itemize}
	\item XCHEM-QC/src/misc/sortingAlgorithms.f90
	\item XCHEM-QC/src/misc/diag-lapack.f90
\end{itemize}
\subsection*{XCHEM-SCAT}
\begin{itemize}
	\item XCHEM-SCAT/programs/share/src/dgamma.f
	\item XCHEM-SCAT/programs/share/src/dpsort.f
	\item XCHEM-SCAT/programs/share/src/ipsort.f
	\item XCHEM-SCAT/programs/share/src/xerror.f
	\item XCHEM-SCAT/programs/share/src/dquadpack.f
	\item XCHEM-SCAT/programs/share/src/rcwf.f90
\end{itemize}