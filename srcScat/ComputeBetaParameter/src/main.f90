!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program ProgramComputeBetaParameter

  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING

  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleString
  use ModuleIO

  use ModuleElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleParentIons

  use ModuleConstants
  use ModuleDiagonalize
  use ModuleScatteringStates

  implicit none

  enum, bind(c)
     enumerator :: MODE_PRINT_INFO
     enumerator :: MODE_UNIFORM
     enumerator :: MODE_RYDBERG
     enumerator :: MODE_RESOLVE
     enumerator :: MODE_REFINE 
  end enum

  !.. Run-time parameters
  !..
  character(len=:), allocatable :: ProgramInputFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: CloseCouplingConfigFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: SymLabel
  character(len=:), allocatable :: OutDipFilex
  character(len=:), allocatable :: OutDipFiley
  character(len=:), allocatable :: OutDipFilez
  character(len=:), allocatable :: OutDir

  character(len=:), allocatable :: StorageDir
  character(len=:), allocatable :: scatLabel
  character(len=:), allocatable :: Gauge
  type(ClassElectronicSpace)    :: Space
  character(len=:), allocatable :: SymStorageDir
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace

  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: nChannels

  integer :: i, ich, ipwc, lw, iBox
  integer :: uid, iBuf
  real(kind(1d0))               :: IonCharge

  real   (kind(1d0)), allocatable :: PWCThr(:)
  integer           , allocatable :: PWClan(:)

  !> Linked-list with the scattering states at several energies
  type(ClassScatteringStateList)          :: ScatStateList
  type(ClassScatteringState)    , pointer :: ScatState

  complex(kind(1d0)), allocatable :: zPsi(:)
  complex(kind(1d0)), allocatable :: zAmpx(:,:),zAmpy(:,:),zAmpz(:,:)
  
  character(len=:), allocatable :: ccscat_dir
  integer                      :: iEn
  character(len=:),allocatable :: ChanName
  real(kind(1d0))              :: Energy, Rmax,beta,PHIBra,PhiKet
  real(kind(1d0))              :: IonEnergy, ElectronEnergy
  real(kind(1d0)), allocatable :: EnergyGrid(:)
  real(kind(1d0)), allocatable :: ThresholdList(:)
  integer        , allocatable :: nThresholdChannels(:), ThresholdChannelList(:,:)
  integer                      :: nThresholds,  iThr, nBoxStates,DipBra,DipKet
  integer                      :: iOpen, iOpenPwc, nEnergies, nOpen
  integer        , allocatable :: listOpen(:), vnOpen(:)
  integer        , allocatable :: listOpenPI(:)
  real(kind(1d0)), allocatable :: Egrid(:)
  integer, parameter           :: J_MAX = 100

  character(len=1000) :: sBuffer, FileName
  integer                      :: iOpenPIKet, iOpenPIBra, lket, lbra, jmax, ichBra, ichKet, iOpenPI
  integer                      :: j, mu, mbra, mket, iOpenBraConjg, iOpenKetConjg, iOpenBra, iOpenKet
  logical                      :: BraConjgIsPresent, KetConjgIsPresent
  real(kind(1d0))              :: F0, F1, F2, sigmaKet, sigmaBra, PICharge
  complex(kind(1d0))           :: zF3, zF4, zF5, zF6, tmpzF4x,tmpzF4y,tmpzF4z,A0,A2,zF4x,zF4y,zF4z,zF5x,zF5y,zF5z, G0
  integer                      :: BoxStateIndex
  complex(kind(1d0))           :: xConjKet,yConjKet,zConjKet,xConjBra,yConjBra,zConjBra
  !.. External functions
  real(kind(1d0)), external :: CoulombPhase 

  logical kk

  call GetRunTimeParameters(    &
              ProgramInputFile, &
              NameDirQCResults, &
       CloseCouplingConfigFile, &
              Multiplicity    , &
              SymLabel        , &
              OutDipFilex     , &
              OutDipFiley     , &
              OutDipFilez     , &
              BoxStateIndex   , &
              scatLabel       , &
              Gauge           , &
              OutDir          )


  call ParseProgramInputFile( ProgramInputFile, StorageDir )
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  call GetSymmetricElectronicSpace( Space, SymLabel, SymSpace )
  call SymSpace.GetThresholdSequence( &
       nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList )
  allocate( SymStorageDir, source = GetBlockStorageDir( SymSpace, SymSpace ) )
  allocate(ccscat_dir, source = SymStorageDir//"/ScatteringStates/SPEC/")
  PICharge = SymSpace.GetPICharge()

  call ScatStateList%Init(ccscat_dir)

  !.. Reconstruct the list of thresholds and print it on screen
  call ScatStateList.Load( scatLabel )
  call ScatStateList%GetEnergyList( Egrid )
  nEnergies  = size(Egrid,1)
  call ScatStateList%GetnOpenList(  vnOpen )
  call ScatStateList%GetNBoxStates(nBoxStates)
  allocate(zPsi( nBoxStates ))
  write(OUTPUT_UNIT,'(a,i0)') 'The Number of BoxStates is : ',nBoxStates

  !.. Save PWA to disk
  
  call Execute_Command_Line(" mkdir -p "//OutDir)
  allocate(zAmpx(size(Egrid),maxval(vnOpen(:))),zAmpy(size(Egrid),maxval(vnOpen(:))),zAmpz(size(Egrid),maxval(vnOpen(:))))
  call LoadPWA( zAmpx, vnOpen, Egrid, OutDir, OutDipFilex )
  call LoadPWA( zAmpy, vnOpen, Egrid, OutDir, OutDipFiley )
  call LoadPWA( zAmpz, vnOpen, Egrid, OutDir, OutDipFilez )


  do iThr = 1, nThresholds

     !*** UGLY
     sBuffer=SymSpace%GetPWCLabel(ThresholdChannelList(1,iThr))
     i=index(sBuffer,"X")
     sBuffer(i:)=" "
     FileName=trim(adjustl(OutDir))//"/"//"beta_"//trim(adjustl(Gauge))//"_"//trim(adjustl(scatLabel))//"_"//trim(adjustl(sBuffer))
     !***
     open(newunit=uid        , &
          file=trim(FileName), &
          form="formatted"   , &
          status="unknown"   )
     
     do iEn = 1, size( Egrid )

        A0 = Z0
        A2 = Z0
        G0 = Z0
        Energy =  Egrid( iEn )
        if(Energy < ThresholdList( iThr )) cycle

        call GetOpenChannels(       &
             nThresholds          , &
             ThresholdList        , &
             nThresholdChannels   , &
             ThresholdChannelList , &
             Energy, nOpen, listOpen, iThr, listOpenPI )

        jmax=0
        do iOpenPiBra = 1, nThresholdChannels( iThr )
           ichBra = listOpen( listOpenPI( iOpenPiBra ) ) 
           lbra = SymSpace%GetChannelL ( ichBra )
           jmax=max(jmax,lbra)
        enddo
        do lbra = 0,jmax
          do mBra = -lBra,lBra
             tmpzF4x = Z0
             tmpzF4y = Z0
             tmpzF4z = Z0
             do iOpenPiBra = 1, nThresholdChannels( iThr )
                iOpenBra = listOpenPI( iOpenPiBra )
                ichBra   = listOpen( iOpenBra   )
                if (lBra .eq. SymSpace%GetChannelL ( ichBra ) .and. mBra .eq. SymSpace%GetChannelM ( ichBra ) ) then
                   tmpzF4x = zAmpx( iEn, iOpenBra )
                   tmpzF4y = zAmpy( iEn, iOpenBra )
                   tmpzF4z = zAmpz( iEn, iOpenBra )
                   exit
                endif
             enddo                
             do i=1, nThresholdChannels( iThr )
                BraConjgIsPresent = &
                     SymSpace%GetChannelL ( listOpen( listOpenPI(i) ) ) ==  lbra .and. &
                     SymSpace%GetChannelM ( listOpen( listOpenPI(i) ) ) == -mbra 
                if(BraConjgIsPresent)then 
                   xConjBra = zAmpx( iEn,listOpenPI (i)) 
                   yConjBra = zAmpy( iEn,listOpenPI (i)) 
                   zConjBra = zAmpz( iEn,listOpenPI (i)) 
                   exit
                endif
             enddo
             if (.not. BraConjgIsPresent) xConjBra = 0.d0
             if (.not. BraConjgIsPresent) yConjBra = 0.d0
             if (.not. BraConjgIsPresent) zConjBra = 0.d0
             do lKet =  0,jmax
                do mKet = -lKet,lKet
                   zF5x = Z0
                   zF5y = Z0
                   zF5z = Z0
                   zF4x = tmpzF4x
                   zF4y = tmpzF4y
                   zF4z = tmpzF4z
                   do iOpenPiKet = 1, nThresholdChannels( iThr )
                      iOpenKet = listOpenPI( iOpenPiKet )
                      !
                      ichKet = listOpen( iOpenKet ) 
                      if (lKet .eq. SymSpace%GetChannelL ( ichKet ) .and. mKet .eq. SymSpace%GetChannelM ( ichKet ) ) then
                          zF5x = zAmpx( iEn, iOpenKet )
                          zF5y = zAmpy( iEn, iOpenKet )
                          zF5z = zAmpz( iEn, iOpenKet )
                          exit
                      endif
                   enddo
                   do i=1, nThresholdChannels( iThr )
                      KetConjgIsPresent = &
                           SymSpace%GetChannelL ( listOpen( listOpenPI(i) ) ) ==  lket .and. &
                           SymSpace%GetChannelM ( listOpen( listOpenPI(i) ) ) == -mket 
                      if(KetConjgIsPresent)then
                         xConjKet = zAmpx( iEn,listOpenPI (i)) 
                         yConjKet = zAmpy( iEn,listOpenPI (i)) 
                         zConjKet = zAmpz( iEn,listOpenPI (i))
                         exit 
                      endif
                   enddo
                   if (.not. KetConjgIsPresent) xConjKet = 0.d0
                   if (.not. KetConjgIsPresent) yConjKet = 0.d0
                   if (.not. KetConjgIsPresent) zConjKet = 0.d0
                   do dipbra = -1,1
                     sigmaBra = CoulombPhase( lBra, PICharge, Energy - ThresholdList( iThr ) )
                     zF4 = (exp(Zi*(lbra*(PI/2.d0)-sigmaBra)))
                     zF4 = zF4*dble(1-abs(dipbra)+(abs(dipbra)/sqrt(2.0)))*(zsgn(mBra)/sqrt(2.d0))*(-dipbra*conjg(zF4x) - Zi*dipbra*bm(mBra)*conjg(xConjBra)-Zi*abs(dipbra)*conjg(zF4y)+abs(dipbra)*bm(mBra)*conjg(yConjBra) + dble(1-abs(dipbra))*conjg(zF4z)+Zi*bm(mBra)*dble(1-abs(dipbra))*conjg(zConjBra))
                       zF4 = conjg(zF4) 
                     do dipket = -1,1
                       sigmaKet = CoulombPhase( lKet, PICharge, Energy - ThresholdList( iThr ) )
                       zF5 = ( exp(Zi*(lKet*(PI/2.d0)-sigmaKet)))
                       zF5 = zF5*dble(1-abs(dipket)+(abs(dipket)/sqrt(2.0)))*(zsgn(mket)/sqrt(2.d0))*(-dipket*conjg(zF5x) - Zi*dipket*bm(mket)*conjg(xConjKet)-Zi*abs(dipket)*conjg(zF5y)+abs(dipket)*bm(mket)*conjg(yConjKet) + dble(1-abs(dipket))*conjg(zF5z)+Zi*bm(mket)*dble(1-abs(dipket))*conjg(zConjKet))

                       F2 = CGC( lbra, 2, lket, 0, 0 ) * CGC( lket, 2, lbra, mket, mbra-mket )*CGC(1,2,1,dipbra,dipket-dipbra)
                       F1 = CGC( lbra, 0, lket, 0, 0 ) * CGC( lket, 0, lbra, mket, mbra-mket )*CGC(1,0,1,dipbra,dipket-dipbra)
                       if (mbra+dipbra .ne. mket+dipket) F2=0.d0
                       if (mbra+dipbra .ne. mket+dipket) F1=0.d0
                       A0 = A0 + F1 * zF4 * zF5 
                       A2 = A2 + F2 * zF4 * zF5
 
                     enddo
                   enddo
                enddo
             enddo
          enddo
        enddo
        beta = 5*CGC(1,2,1,0,0)*(real(A2)/real(A0))
        write(uid,*) Energy, beta

     enddo

     close(uid)

  enddo

  stop

contains
  
  real(kind(1d0)) function dpar( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(mod(abs(i),2)==1)res=-1.d0
  end function dpar

  real(kind(1d0)) function bm( i ) result( res )
    integer, intent(in) :: i
    res = 1.d0
    if (i .eq. 0) res = 0.d0
  end function bm

  complex(kind(1d0)) function zsgn( i ) result( res )
    !.. Returns 1 if i>=0 and -i * (-1)^{i} if i<0
    integer, intent(in) :: i
    if(i<0) then
      if(mod(abs(i),2)/=0) then
        res=Zi
      else
        res=-Zi
      endif
      return
    elseif (i>0) then
       res=Z1
       return
    elseif (i==0) then
       res = sqrt(2.d0)
    endif
  end function zsgn

  real(kind(1d0)) function dsgn( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(i<0)res=-1.d0
  end function dsgn

  real(kind(1d0)) function dsgnpow( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(i<0.and.mod(abs(i),2)==1)res=-1.d0
  end function dsgnpow

  function UniformGrid(xmi,xma,n) result(grid)
    real(kind(1d0)), intent(in) :: xmi, xma
    integer        , intent(in) :: n
    real(kind(1d0)), allocatable :: grid(:)
    integer :: i
    allocate(grid,source=[(xmi+(xma-xmi)/dble(n-1)*dble(i-1),i=1,n)])
  end function UniformGrid


  !> Reads the run time parameters specified in the command line.
  !! If an optional inherently positive parameters is not specified
  !! on the command line, it is initialized to a negative number
  !! to signal that it should not be used to evaluate the energy grid.
  !<
  subroutine GetRunTimeParameters( &
       ProgramInputFile, &
       NameDirQCResults, &
       ConfigFile      , &
       Multiplicity    , &
       SymLabel        , &
       OutDipFilex     , &
       OutDipFiley     , &
       OutDipFilez     , &
       BoxStateIndex   , &
       ScatLabel       , &
       Gauge           , &
       OutDir          )
    !
    use ModuleErrorHandling
    use ModuleCommandLineParameterList
    use ModuleString    
    implicit none
    !
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    character(len=:), allocatable, intent(out) :: ConfigFile
    character(len=:), allocatable, intent(out) :: scatLabel
    integer                      , intent(out) :: Multiplicity
    integer                      , intent(out) :: BoxStateIndex
    character(len=:), allocatable, intent(out) :: SymLabel
    character(len=:), allocatable, intent(out) :: OutDipFilex
    character(len=:), allocatable, intent(out) :: OutDipFiley
    character(len=:), allocatable, intent(out) :: OutDipFilez
    character(len=:), allocatable, intent(out) :: OutDir
    character(len=:), allocatable, intent(out) :: Gauge
    !
    type( ClassCommandLineParameterList ) :: List
    !
    character(len=*), parameter :: PROGRAM_DESCRIPTION=&
         "Compute multichannel scattering states"
    character(len=512) :: strnBuf, pif
    !
    call List.SetDescription(PROGRAM_DESCRIPTION)
    call List.Add( "--help" , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " "    , "required" )
    call List.Add( "-label" , "subdir label" , ""            , "optional" )
    call List.Add( "-ccfile"  , "Config File"  , "CCFile"      , "optional" )
    call List%Add( "-boxi"  , "Box state index", 1           , "optional" )
    call List%Add( "-gau"   , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-mult"  , "Total Multiplicity", 1        , "required" )
    call List.Add( "-sym"   , "SymLabel"         , "A"       , "required" )
    call List.Add( "-od"    , "OutDir"           , "out"     , "required" )
    call List.Add( "-scatLabel" , "label of the set of scattering states"  ,  "1", "optional" )
    !
    call List.Parse()
    !
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if


    call List%Get( "-gau"    , strnBuf )
    allocate( Gauge          , source = trim( strnBuf )    )
    call List%Get( "-boxi"   , BoxStateIndex )
    call List%Get( "-pif"    , pif  )
    allocate( ProgramInputFile       , source = trim( pif )    )
    call List.Get( "-label",  strnBuf  )
    allocate(NameDirQCResults,source=trim(adjustl(strnBuf)))
    call List.Get( "-ccfile",  strnBuf  )
    allocate(ConfigFile,source=trim(adjustl(strnBuf)))
    call List%Get( "-mult"   , Multiplicity )
    call List.Get( "-sym"  ,  strnBuf  )
    allocate( SymLabel, source=trim(adjustl(strnBuf)))
    call List.Get( "-od",  strnBuf  )
    allocate( OutDir, source=trim(adjustl(strnBuf)))
    call List.Get( "-scatLabel",  strnBuf  )
    allocate(scatLabel,source=trim(adjustl(strnBuf)))
    !
    call List.Free()

    if (trim(Gauge) .eq. 'l') then
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Len_x_FromBoxState_'),BoxStateIndex
      allocate( OutDipFilex, source=trim(strnBuf))
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Len_y_FromBoxState_'),BoxStateIndex
      allocate( OutDipFiley, source=trim(strnBuf))
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Len_z_FromBoxState_'),BoxStateIndex
      allocate( OutDipFilez, source=trim(strnBuf))
    elseif (trim(Gauge) .eq. 'v' ) then    
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Vel_x_FromBoxState_'),BoxStateIndex
      allocate( OutDipFilex, source=trim(strnBuf))
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Vel_y_FromBoxState_'),BoxStateIndex
      allocate( OutDipFiley, source=trim(strnBuf))
      write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Vel_z_FromBoxState_'),BoxStateIndex
      allocate( OutDipFilez, source=trim(strnBuf))
    else
      call Assert('The Dipole gauge is not well specified')
    endif
    !.. Must add check on the input
    !
    write(OUTPUT_UNIT,"(a)" ) "Run time parameters :"
    write(OUTPUT_UNIT,"(a)" ) "Subdir label    : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)" ) "CC Config File  : "//ConfigFile
    write(OUTPUT_UNIT,"(a)" ) "Spectral SymLabel   : "//SymLabel
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile


  subroutine LoadPWA( zAmp, vnOpen, Egrid, InDir, fname )
    complex(kind(1d0)), intent(inout) :: zAmp(:,:)
    integer           , intent(in) :: vnOpen(:)
    real(kind(1d0))   ,allocatable :: tmpReal1(:),tmpReal2(:)
    real(kind(1d0))   , intent(in) :: Egrid(:)
    character(len=*)  , intent(in)  :: InDir
    character(len=*) , optional , intent(in) :: fname
    real(kind(1d0)) :: tmpreal
    integer  :: uid, iostat, iEn, nOpen, ich,i,tmpint
    if (present(fname) ) then
      open( newunit=uid, File=trim(OutDir)//'/'//trim(fname), form="formatted", status="old" )
      print*, trim(OutDir)//trim(fname)
    else
      open( newunit=uid, File=trim(OutDir)//'zAmp', form="formatted", status="old" )
    endif
    read(uid,*)
    do iEn=1,size(Egrid)
       nOpen=vnOpen(iEn)
!       read(uid,*) tmpreal, tmpreal, (zAmp(iEn,ich),ich=1,nOpen)
!       read(uid,"(e24.16,x,i6,*(x,e24.16))") tmpreal, tmpint, (zAmp(iEn,ich),ich=1,nOpen)
       allocate(tmpReal1(nOpen),tmpReal2(nOpen))
       read(uid,*) tmpreal, tmpreal, ((tmpReal1(ich),tmpReal2(ich)),ich=1,nOpen)
       do ich=1,nOpen
         zAmp(iEn,ich) = cmplx(tmpReal1(ich),tmpReal2(ich))
       enddo
       deallocate(tmpReal1,tmpReal2)

    enddo
    close(uid)

  end subroutine LoadPWA
  subroutine SavePWA( zAmp, vnOpen, Egrid, OutDir, fname )
    complex(kind(1d0)), intent(in) :: zAmp(:,:)
    integer           , intent(in) :: vnOpen(:)
    real(kind(1d0))   , intent(in) :: Egrid(:)
    character(len=*)  , intent(in) :: OutDir
    character(len=*) , optional , intent(in) :: fname

    integer  :: uid, iostat, iEn, nOpen, ich
    if (present(fname) ) then
      open( newunit=uid, File=trim(OutDir)//'/'//trim(fname), form="formatted", status="replace" )
      print*, trim(OutDir)//trim(fname)
    else
      open( newunit=uid, File=trim(OutDir)//'zAmp', form="formatted", status="replace" )
    endif
    write(uid,*)"# ",size(Egrid)
    do iEn=1,size(Egrid)
       nOpen=vnOpen(iEn)
       write(uid,"(e24.16,x,i6,*(x,e24.16))") Egrid(iEn), nOpen, (zAmp(iEn,ich),ich=1,nOpen)
    enddo
    close(uid)

  end subroutine SavePWA

  subroutine getopenchannels( nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList , &
       E, nOpen, listOpen, nPI, listOpenPI )

    integer             , intent(in) :: nThresholds
    real(kind(1d0))     , intent(in) :: ThresholdList(:)
    integer             , intent(in) :: nThresholdChannels(:)
    integer             , intent(in) :: ThresholdChannelList(:,:)
    real(kind(1d0))     , intent(in) :: E
    integer             , intent(out):: nOpen
    integer, allocatable, intent(inout):: listOpen(:)
    integer             , intent(in)   :: nPI
    integer, allocatable, intent(inout):: listOpenPI(:)

    integer :: ithr, ich, iOpen

    nOpen=0
    do ithr = 1, nThresholds
       if( ThresholdList(ithr) <= E ) nOpen = nOpen + nThresholdChannels(ithr)
    enddo
    call realloc(listOpen,nOpen)
    call realloc(listOpenPI,nThresholdChannels(nPI))
    listOpenPI=0

    iOpen=0
    iOpenPI=0
    do ithr = 1, nThresholds
       if( ThresholdList(ithr) > E )exit
       do ich = 1, nThresholdChannels(ithr)
          iOpen=iOpen+1
          listOpen(iOpen) = ThresholdChannelList( ich, ithr )
          if(ithr==nPI)then
             iOpenPI=iOpenPI+1
             listOpenPI(iOpenPI) = iOpen
          endif
       enddo
    enddo

  end subroutine getopenchannels



end program ProgramComputeBetaParameter


