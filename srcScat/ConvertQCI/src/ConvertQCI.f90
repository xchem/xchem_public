!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  
!!  This program converts the quantum-chemistry interface (QCI) to an independent local format
!!
program ConvertQCI

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleIO
  use ModuleString
  use ModuleGroups
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleElectronicSpace
  use ModuleGeneralInputFile
  use ModuleBasis
  use ModuleBasicMatrixElements
  use ModuleMatrix
  use ModuleAbsorptionPotential
  use ModuleParentIons
  use symba
  use ModulePartialWaveChannel

  implicit none

  interface
     subroutine FetchMultipolesData( MultipolesFile, NumChannels, AngMom, BraIndex, KetIndex, CartLabel, MultipoleValues )
       !
       character(len=*),               intent(in) :: MultipolesFile
       integer,                        intent(in) :: NumChannels
       integer,                        intent(in) :: AngMom
       integer,                        intent(in) :: BraIndex
       integer,                        intent(in) :: KetIndex
       character(len=32), allocatable, intent(out) :: CartLabel(:)
       real(kind(1d0)),   allocatable, intent(out) :: MultipoleValues(:)
       !
     end subroutine FetchMultipolesData
  end interface


  type :: ClassMultipoleBlockVec
     type(ClassMatrix), allocatable :: Block(:,:)
  end type ClassMultipoleBlockVec


  character(len=*), parameter :: DictionaryFileName = "symmetry"
  character(len=*), parameter :: MultipoleFileName  = "multipoles.dat"
  character(len=*), parameter :: VelocityFileName  = "velocities.dat"
  character(len=*), parameter :: PIKinEnergyFileName = "kinetic.parent.dat"
  character(len=*), parameter :: InterfaceFileRootName = "interface"
  character(len=*), parameter :: HamiltonianFileRootName = "Ham_"
  character(len=*), parameter :: KinEnergyFileRootName = "kin_"
  character(len=*), parameter :: OverlapFileRootName = "ov_"
  character(len=*), parameter :: DipoleLenFileRootName = "dip"
  character(len=*), parameter :: DipoleVelFileRootName = "vel"
  character(len=*), parameter :: FileExtension = ".out"
  logical         , parameter :: FormattedBlock = .true.



  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: QCInterfaceDir
  character(len=:), allocatable :: NameDirQCResults


  !.. Config file parameters
  character(len=:), allocatable :: StorageDir

  !.. Local variables
  type(ClassBasis)              :: Basis
  type(ClassBasicMatrixElements):: BasicMatrixElements
  type(ClassAbsorptionPotential):: AbsPot
  !
  type(ClassElectronicSpace)    :: Space
  type(ClassShortRangeSymOrbitals) :: SRSOrbSet


  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  integer                       :: iIrrep


  integer                       :: uidDict, iostat
  character(len=:), allocatable :: DictionaryFile
  character(len=:), allocatable :: Line

  integer                           :: NParentIons
  integer                           :: iPion, uidPion
  real(kind(1d0))                   :: PionEnergy
  type(ClassParentIon), allocatable :: ParentIonVec(:)
  character(len=16)   , allocatable :: ParentIonTag(:)
  character(len=:)    , allocatable :: PionFile
  character(len=16)                 :: PionStrn


  integer                           :: uidMult
  integer                           :: uidVel
  integer                           :: ichar
  character(len=:)    , allocatable :: MultFile
  character(len=:)    , allocatable :: VelFile
  character(len=:)    , allocatable :: KinEnergyFile
  character(len=:)    , allocatable :: CartesianStrn
  character(len=128)                :: ReadMonStrn
  type(ClassMultipole)              :: Multipoles
  type(ClassMultipole)              :: Velocities
  type(ClassKineticEnergy)          :: KinEnerg
  integer                           :: iPionBra, iPionKet
  real(kind(1d0))                   :: CartMultVal
  real(kind(1d0))                   :: CartVelVal

  character(len=:)    , allocatable :: InterfaceFileName

  character(len=:), allocatable :: SAEstoredir
  character(len=:), allocatable :: SAEBasisFile

  integer :: RankT, DeltaL
  type(ClassMatrix)  :: GeneralMatrix
  type(ClassMatrix), allocatable :: GABSOverlap(:,:)
  type(ClassMatrix), allocatable :: GABSHamiltonian(:,:)
  type(ClassMatrix), allocatable :: GABSKinEnergy(:,:)
  type(ClassMatrix), allocatable :: GABSDipoleLen(:,:)
  type(ClassMatrix), allocatable :: GABSDipoleVel(:,:)
  !> Complex absorber potential. The first index run over
  !! the number of potentials defined in the absorption
  !! potential configuration file. The second and the
  !! third define the operator matrix.
  type(ClassMatrix), allocatable :: GABSCAP(:,:,:)
  integer :: iCAP


  type(ClassMultipoleBlockVec), pointer :: GABSMultipole(:)


  logical :: exist

  integer :: l, lMult
  integer :: lBra, lKet
  real(kind(1d0)) :: AngularFactor

  integer :: NumExponents

  integer, allocatable :: NRowMin(:), NRowMax(:)

  integer :: NCCch, iCCch, iCCchBra, iCCchKet
  character(len=64), allocatable :: CCchSym(:), CCchTag(:), CCchPWC(:)
  character(len=:), allocatable :: FileName
  
  logical :: OnlyPrepareData

  integer :: Lmax
  integer :: LmaxSpace

  !.. Read run-time parameters, among which there is the group and the symmetry
  call GetRunTimeParameters(      &
       GeneralInputFile,          &
       ProgramInputFile,          &
       Multiplicity,              &
       CloseCouplingConfigFile,   &
       QCInterfaceDir,            &
       NameDirQCResults           )
  !
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "QC Interface Dir    : "//QCInterfaceDir
  write(OUTPUT_UNIT,"(a)" ) "General Input File  : "//GeneralInputFile
  write(OUTPUT_UNIT,"(a)" ) "Program Input File  : "//ProgramInputFile
  write(OUTPUT_UNIT,"(a)" ) "Close Coupling File : "//CloseCouplingConfigFile


  call ParseProgramInputFile( ProgramInputFile, &
       StorageDir )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir


  call Space%SetMultiplicity( Multiplicity )
  !call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(StorageDir) )
  OnlyPrepareData = .true.
  call Space%ParseConfigFile( CloseCouplingConfigFile, OnlyPrepareData )


  !.. Prepare the single-particle blocks for all the irreps of the group
  !..
  Group => Space%GetGroup()


  != PART ONE = Copy parent-ion data

  !.. Now the program parses the file with the correspondance between the PWCs from QC
  !   and the parent-ion identity as well as the total symmetry. "FetchLine" will be used to
  !   get the next non-empty line, once purged from comments.
  !..
  allocate( DictionaryFile, source= AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//DictionaryFileName)
  call OpenFile( DictionaryFile, uidDict, "read", "formatted" )
  !
  !.. The first valid line must contain the name of the group.
  !   If the name does not match the one specified in the CC config file, it complains.
  !..
  call FetchLine( uidDict, Line )
  if( Line .isnt. Group%GetName() ) call Assert( &
       " Group "//Line//" in "//DictionaryFile//&
       " does not match "//Group%GetName()//&
       " in "//CloseCouplingConfigFile ) 


  !.. The second valid name must contain the number of parent ions available in the interface
  !..
  call FetchLine( uidDict, Line )
  read(Line,*,iostat=iostat) NParentIons
  if(iostat/=0 .or. NParentIons <1) call Assert(" Number of parent ions incorrectly specified in "//DictionaryFile )


  !.. Read the tags of the parent ions and their quantum numbers.
  !   Read from the QCI the parent ion properties (now: just the energy)
  !   Save the parent ion data in storage
  !..
  allocate(ParentIonVec(NParentIons))
  allocate(ParentIonTag(NParentIons))
  ParentIonTag=" "
  do iPion = 1, NParentIons
     call FetchLine( uidDict, Line )
     read( Line, * ) ParentIonTag(iPion), PionStrn
     ParentIonTag(iPion)=adjustl(ParentIonTag(iPion))
     allocate(PionFile,source=AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//"En_"//trim(ParentIonTag(iPion))//".out")
     call OpenFile(PionFile,uidPion,"read","formatted")
     read(uidPion,*)PionEnergy
     close(uidPion)
     deallocate(PionFile)
     call ParentIonVec(iPion)%init(Group,PionStrn,Space%GetPionCharge(),PionEnergy)
     call ParentIonVec(iPion)%SaveData(Space%GetStorageDir())
  enddo


  LmaxSpace = Space%GetLMax()
  Lmax = Space%GetMaxLinCC()
  write(OUTPUT_UNIT,"(a,i0)" ) "Max L defined in the Close-Coupling : ", Lmax
  
  !.. For each pair of parent ions, read from the QCI the list of non-zero cartesian multipoles between them
  !  (remember that up to D2h cartesian monomials are irreducibles), then transform it to Spherical and save
  !   the result to storage
  !..
  allocate(KinEnergyFile,source=AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//PIKinEnergyFileName)
  allocate(MultFile,source=AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//MultipoleFileName)
  allocate(VelFile,source=AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//VelocityFileName)
  !
  do iPionBra = 1, NParentIons
     do iPionKet = 1, NParentIons

        ! Part of the kinetic energy
        call KinEnerg%Init( ParentIonVec(iPionBra), ParentIonVec(iPionKet) )
        call KinEnerg%FetchKinEnergy( KinEnergyFile, iPionBra, iPionKet )
        call KinEnerg%Save( Space%GetStorageDir() )
        
        ! Part of the multipoles
        call Multipoles%Init( 2*Space%GetLmax(), ParentIonVec(iPionBra), ParentIonVec(iPionKet) )
        !
        call Multipoles%FetchCartesianStrn( CartesianStrn )
        call SetStringToUppercase( CartesianStrn )
        !

        call OpenFile( MultFile, uidMult, "read", "formatted" )
        !
        MultFileCycle : do 

           call FetchLine( uidMult, Line )
           if(len_trim(Line)==0)exit
           read(Line,*) ReadMonStrn
           call SetStringToUppercase( ReadMonStrn )
           ichar = index( " "//CartesianStrn//" ", " "//trim(adjustl(ReadMonStrn))//" " )
           if(ichar<1)then
              do iPion =1, NParentIons
                 call FetchLine(uidMult, Line)
              enddo
              cycle MultFileCycle
           endif
           do iPion = 1, iPionBra
              call FetchLine( uidMult, Line )
           enddo
           read(Line,*) ( CartMultVal, iPion=1, iPionKet )
           do iPion = iPionBra+1, NParentIons
              call FetchLine( uidMult, Line )
           enddo

           call Multipoles%SetCartEl( trim(adjustl(ReadMonStrn)), CartMultVal )

        enddo MultFileCycle
        deallocate( CartesianStrn )
        close( uidMult )

        call Multipoles%ConvertCartesianToSpherical()
        call Multipoles%Save( Space%GetStorageDir() )
        !
        !XCHEMQC add velocity part
        call Velocities%Init( 2*Space%GetLmax(), ParentIonVec(iPionBra), ParentIonVec(iPionKet) )
        !
        call Multipoles%FetchCartesianStrn( CartesianStrn )
        call SetStringToUppercase( CartesianStrn )
        !
        call OpenFile( VelFile, uidVel, "read", "formatted" )

        VelFileCycle : do 

           call FetchLine( uidVel, Line )
           if(len_trim(Line)==0)exit
           read(Line,*) ReadMonStrn
           call SetStringToUppercase( ReadMonStrn )
           ichar = index( " "//CartesianStrn//" ", " "//trim(adjustl(ReadMonStrn))//" " )
           if(ichar<1)then
              do iPion =1, NParentIons
                 call FetchLine(uidVel, Line)
              enddo
              cycle VelFileCycle
           endif
           do iPion = 1, iPionBra
              call FetchLine( uidVel, Line )
           enddo
           read(Line,*) ( CartVelVal, iPion=1, iPionKet )
           do iPion = iPionBra+1, NParentIons
              call FetchLine( uidVel, Line )
           enddo

           call Velocities%SetCartEl( trim(adjustl(ReadMonStrn)), CartVelVal )

        enddo VelFileCycle
        deallocate( CartesianStrn )
        close( uidVel )
        call Velocities%ConvertCartesianToSpherical()
        call Velocities%Save( Space%GetStorageDir(), "velo" )
        !END XCHEMQC 
        !
     enddo
  enddo


  != PART TWO = Copy symmetric diffuse orbital data and build diffuse - Bspline matrix elements


  !.. Parse the configuration file with the parameters
  !   necessary to initialize the single-active electron basis
  !   and matrix elements
  !..
  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 


  !.. Initialises the GABS basis
  !..
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call Basis%Init( SAEBasisFile )
  call Basis%SetRoot( SAEstoredir )

  if ( Basis%GetGaussianLMax() /= LmaxSpace ) then
     write(output_unit,*) 'LmaxGABS =', Basis%GetGaussianLMax()
     write(output_unit,*) 'LmaxSpace =', LmaxSpace
     call Assert( &
          "The maximum L in the GABS Gaussians is different "//&
          "from the maximum possible L defined in the CC file.")
  end if
  !.. Load basic matrix elements
  write(OUTPUT_UNIT,"(a)") " Load matrices between basis functions"
  call BasicMatrixElements%Init( Basis )
  call BasicMatrixElements%Read( Basis%GetDir() )

  !.. Load the absorption potentials
  call AbsPot%Parse( AbsorptionPotentialFile )
  call AbsPot%Init( Basis )
  call AbsPot%Read( Basis%GetDir() )


  NumExponents = Basis%GetGaussianNumExponents()

  allocate(GABSOverlap     ( 0:0, 0:Lmax ))
  allocate(GABSHamiltonian ( 0:0, 0:Lmax ))
  allocate(GABSKinEnergy   ( 0:0, 0:Lmax ))
  allocate(GABSCAP( AbsPot%NumberOfTerms(), 0:0, 0:Lmax ))
  do l = 0, Lmax

     !.. Computes the conversion factor from radial integral to
     !   reduced matrix element, the two being related by
     !$$
     !   ***
     !   \langle f \ell \| O_{T} \| g \ell' \rangle = \sqrt{2\ell'+1} ( C_{\ell' 0, T 0}^{\ell 0} )^{-1} \langle f \ell | O_{T} | g \ell' \rangle
     !$$
     AngularFactor = sqrt(dble(2*l+1)) 

     call BasicMatrixElements%FetchMetricMatrix( GABSOverlap( 0, l ) )
     call Basis%RemoveGaussianRowNormalization( GABSOverlap( 0, l ) )
     !.. Convert the radial integral to the reduced matrix element
     call GABSOverlap(0,l)%Multiply(AngularFactor)
     
     call BasicMatrixElements%FetchHamiltonianMatrix( GABSHamiltonian(0,l), l, Space%GetPionCharge() )
     call Basis%RemoveGaussianRowNormalization( GABSHamiltonian(0,l) )
     call GABSHamiltonian(0,l)%Multiply(AngularFactor)

     call BasicMatrixElements%FetchKineticMatrix( GABSKinEnergy(0,l), l )
     call Basis%RemoveGaussianRowNormalization( GABSKinEnergy(0,l) )
     call GABSKinEnergy(0,l)%Multiply(AngularFactor)

     !
     do iCAP = 1, AbsPot%NumberOfTerms()
        call AbsPot%Fetch( iCAP, GABSCAP(iCAP,0,l), l, REP_LEVEL_TOTAL )
        call Basis%RemoveGaussianRowNormalization( GABSCAP(iCAP,0,l) )
        call GABSCAP(iCAP,0,l)%Multiply(AngularFactor)
        
     end do
     
  enddo


  RankT = 1
  allocate(GABSDipoleLen(-RankT:RankT,0:Lmax))
  allocate(GABSDipoleVel(-RankT:RankT,0:Lmax))
  do l = 0, Lmax
     do DeltaL = -RankT, RankT, 2
        
        !.. Already loads the reduced matrix element
        !..
        call BasicMatrixElements%FetchGeneralLengthMatrix( GABSDipoleLen( DeltaL, l ), l+DeltaL, l )
        call Basis%RemoveGaussianRowNormalization( GABSDipoleLen( DeltaL, l ) )
        if(l+DeltaL>Lmax) GABSDipoleLen( DeltaL, l ) = 0.d0
        call BasicMatrixElements%FetchGeneralVelocityMatrix( GABSDipoleVel( DeltaL, l ), l+DeltaL, l )
        call Basis%RemoveGaussianRowNormalization( GABSDipoleVel( DeltaL, l ) )
        if(l+DeltaL>Lmax) GABSDipoleVel( DeltaL, l ) = 0.d0

     enddo
  enddo


  !.. Computes the reduced blocks for the multipoles, excluding the monopole (l = 0)
  allocate( GABSMultipole( 1: 2*Lmax ) )
  do lMult = 1, 2*Lmax
     
     allocate( GABSMultipole(lMult)%Block( -lMult:lMult, 0:Lmax ) )
     call BasicMatrixElements%FetchMultipoleMatrix( lMult, GeneralMatrix )
     call Basis%RemoveGaussianRowNormalization( GeneralMatrix )

     do lKet = 0, Lmax
        do DeltaL = -lMult, lMult, 2

           GABSMultipole(lMult)%Block( DeltaL, lKet ) = GeneralMatrix

           lBra = lKet + DeltaL
           if( lBra > Lmax .or. lBra < 0 )then
              GABSMultipole(lMult)%Block( DeltaL, lKet ) = 0.d0
              cycle
           endif
           
           AngularFactor = sqrt(dble((2*lKet+1)*(2*lMult+1))/(4.d0*PI)) * CGC( lKet, lMult, lBra, 0, 0 )
           call GABSMultipole(lMult)%Block( DeltaL, lKet )%Multiply( AngularFactor )

        enddo
     enddo
     
  enddo


  allocate(NRowMin(0:Lmax))
  allocate(NRowMax(0:Lmax))
  do l = 0, Lmax
     call Basis%GetLRowInterval( l, NRowMin(l), NRowMax(l) )
  enddo

  !..  Cycle over the irrep of the diffuse orbitals
  !   (in general, they do not have a definite value for Xlm)
  !..
  irrepv => Group%GetIrrepList()
  do iIrrep = 1, size( irrepv )

     !.. Check if iIrrep is represented in the interface. If not, move forward.
     !..
     if ( allocated(InterfaceFileName) ) deallocate( InterfaceFileName )
     allocate( InterfaceFileName, source = &
          AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
          InterfaceFileRootName//irrepv(iIrrep)%GetName() )

     INQUIRE( file = InterfaceFileName, exist = exist )
     if( .not. exist )cycle

     !.. Copies the symmetric interface from the interface to the storage
     call SRSOrbSet%AcquireInterface( Space%GetStorageDir(), InterfaceFileName )
     !.. Initialise short-range symmetric orbitals
     call SRSOrbSet%init( irrepv( iIrrep ), Space%GetStorageDir(), Space%GetLmax() )
     call SRSOrbSet%ComputeFullTransfMat( NumExponents )


     !.. Scalar operators (Overlap, Hamiltonian and CAP).
     call ComputeAndSaveBlock( Lmax, Space, Basis, GABSOverlap    , "S", 0, irrepv( iIrrep ), SRSOrbSet, FormattedBlock )

     call ComputeAndSaveBlock( Lmax, Space, Basis, GABSHamiltonian, "H", 0, irrepv( iIrrep ), SRSOrbSet, FormattedBlock )

     call ComputeAndSaveBlock( Lmax, Space, Basis, GABSKinEnergy  , "K", 0, irrepv( iIrrep ), SRSOrbSet, FormattedBlock )

     !.. Complex absorber.
     do iCAP = 1, AbsPot%NumberOfTerms()
        call ComputeAndSaveBlock( Lmax, Space, Basis, GABSCAP(iCAP,:,:), "CAP"//AlphabeticNumber(iCAP)//"_", 0,&
          irrepv( iIrrep ), SRSOrbSet, FormattedBlock )
     end do


     !.. Dipole Operator Length Gauge.
     call ComputeAndSaveBlock( Lmax, Space, Basis, GABSDipoleLen, "DipoleLen", 1, irrepv( iIrrep ), SRSOrbSet, FormattedBlock )

     !.. Dipole Operator Velocity Gauge.
     call ComputeAndSaveBlock( Lmax, Space, Basis, GABSDipoleVel, "DipoleVel", 1, irrepv( iIrrep ), SRSOrbSet, FormattedBlock )

     !.. Multipoles Operators.
     do lMult = 1, 2*Lmax
        call ComputeAndSaveBlock( Lmax, Space, Basis, GABSMultipole(lMult)%Block, "Multipole", lMult,&
          irrepv( iIrrep ), SRSOrbSet, FormattedBlock )
     enddo
     
  enddo


  != PART THREE = Copy close-coupling quantum-chemistry blocks from QCI to Storage


  !.. Read the labels of the close-coupling components
  call FetchLine( uidDict, Line )
  !.. Read the number of close-coupling channels
  read(Line,*,iostat=iostat) NCCch 
  if(iostat/=0)call Assert(" Invalid number of close-coupling channels in "//DictionaryFile )
  allocate( CCchTag( NCCch ) )
  allocate( CCchSym( NCCch ) )
  allocate( CCchPWC( NCCch ) )
  do iCCch = 1, NCCch
     call FetchLine( uidDict, Line )
     read(Line,*,iostat=iostat) CCchTag( iCCch ), CCchSym( iCCch ), CCchPWC( iCCch )
     CCchTag( iCCch ) = adjustl(CCchTag( iCCch ))
     CCchSym( iCCch ) = adjustl(CCchSym( iCCch ))
     CCchPWC( iCCch ) = adjustl(CCchPWC( iCCch ))
     if(iostat/=0)call Assert(" Invalid close-coupling channel labels in "//DictionaryFile )
  enddo
  close( uidDict )

  
  !.. Copy close-coupling blocks
  !XCHEMQC Can this be parallelized?
  do iCCchBra = 1, NCCch
     do iCCchKet = 1, NCCch
       
        if( trim(CCchSym( iCCchBra )) .is. trim(CCchSym( iCCchKet )) )then 
           if ( allocated(FileName) ) deallocate( FileName )
           allocate( FileName, source = &
                AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
                HamiltonianFileRootName//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
           call Space%AcquireHamiltonian( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchPWC( iCCchKet ), FileName )

           if ( allocated(FileName) ) deallocate( FileName )
           allocate( FileName, source = &
                AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
                KinEnergyFileRootName//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
           call Space%AcquireKinEnergy( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchPWC( iCCchKet ), FileName )
           
           if ( allocated(FileName) ) deallocate( FileName )
           allocate( FileName, source = &
                AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
                OverlapFileRootName//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
           call Space%AcquireOverlap( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchPWC( iCCchKet ), FileName )
           
        endif


        !*** CONVERT THE CODE TO A MORE COMPACT FORM, SUCH AS THE ONE OUTLINED IN THE FOLLOWING COMMENTED SEGMENT
        !
!!$        allocate( DirName, source = AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults) )
!!$        iDip2M(1:3)=(/1,-1,0/)
!!$        do iDip = 1, 3
!!$           
!!$           allocate(FileExt,source=AlphabeticNumber(iDip)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension)
!!$
!!$           allocate(FileNameLen, source=DirName//DipoleLenFileRootName//FileExt)
!!$           call Space%AcquireDipoleLen( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ), CCchPWC( iCCchKet ), FileNameLen, iDip2M(iDip) )
!!$
!!$           allocate(FileNameVel, source=DirName//DipoleVelFileRootName//FileExt)
!!$           call Space%AcquireDipoleVel( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ), CCchPWC( iCCchKet ), FileNameVel, iDip2M(iDip) )
!!$
!!$           deallocate(FileExt,FileNameLen,FileNameVel)
!!$
!!$        enddo


        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleLenFileRootName//AlphabeticNumber(3)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleLen( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
          CCchPWC( iCCchKet ), FileName, 0 )

        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleLenFileRootName//AlphabeticNumber(2)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleLen( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
        CCchPWC( iCCchKet ), FileName, -1 )

        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleLenFileRootName//AlphabeticNumber(1)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleLen( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
          CCchPWC( iCCchKet ), FileName, 1 )

        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleVelFileRootName//AlphabeticNumber(3)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleVel( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
          CCchPWC( iCCchKet ), FileName, 0 )

        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleVelFileRootName//AlphabeticNumber(2)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleVel( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
          CCchPWC( iCCchKet ), FileName, -1 )

        if ( allocated(FileName) ) deallocate( FileName )
        allocate( FileName, source = &
             AddSlash(QCInterfaceDir)//AddSlash(NameDirQCResults)//&
             DipoleVelFileRootName//AlphabeticNumber(1)//"_"//trim(CCchTag(iCCchBra))//"-"//trim(CCchTag(iCCchKet))//FileExtension )
        call Space%AcquireDipoleVel( CCchSym( iCCchBra ), CCchPWC( iCCchBra ), CCchSym( iCCchKet ),&
          CCchPWC( iCCchKet ), FileName, 1 )

     enddo
  enddo

  write(output_unit,'(a)') "Done"    


contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile,          &
       ProgramInputFile,          &
       CurrentMultiplicity,       &
       CloseCouplingConfigFile,   &
       QCInterfaceDir,            &
       NameDirQCResults           )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    !> [GeneralInputFile](@ref GeneralInputFile)  
    character(len=:), allocatable, intent(out) :: GeneralInputFile
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    integer,                       intent(out) :: CurrentMultiplicity
    character(len=:), allocatable, intent(out) :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out) :: QCInterfaceDir
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=200) :: gif, pif, ccfile, qcdir, qcroot
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Build the single-ionization close-coupling space "//&
         "for an arbitrary atom or molecule using input from a QC code."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-qcroot" , "Name of the root folder with all the QC interfaces ",  " ", "required" )
    call List%Add( "-qcdir"  , "Name of the subfolder with the QC interface for a given nucl. config.",  " ", "required" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif"    , gif  )
    call List%Get( "-pif"    , pif  )
    call List%Get( "-mult"   , CurrentMultiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-qcroot" , qcroot  )
    call List%Get( "-qcdir"  , qcdir  )

    !
    call List%Free()
    !
    allocate( GeneralInputFile, source = trim( gif ) )
    allocate( ProgramInputFile, source = trim( pif ) )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( QCInterfaceDir,   source = trim( qcroot ) )
    allocate( NameDirQCResults, source = trim( qcdir ) )
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile, &
       StorageDir )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*) , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/"     , "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"        , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile



  subroutine SAE_Block_Transformation( CorrectMat, NewXlmToCartMat, CartCoeff, Block_Diffuse_BSplineXlm )
    type(ClassMatrix) , intent(in)   :: CorrectMat
    complex(kind(1d0)), intent(in)   :: NewXlmToCartMat(:,:)
    real(kind(1d0))   , intent(in)   :: CartCoeff(:,:)
    class(ClassMatrix) , intent(out) :: Block_Diffuse_BSplineXlm

    type(ClassMatrix) :: MatGABS
    type(ClassMatrix) :: Array1

    MatGABS = CorrectMat
    Array1 = dble(NewXlmToCartMat)
    call Array1%Multiply( MatGABS, 'Right', 'N' )
    call MatGABS%Free()
    Block_Diffuse_BSplineXlm = CartCoeff
    call Block_Diffuse_BSplineXlm%Transpose()
    call Block_Diffuse_BSplineXlm%Multiply( Array1, 'Right', 'N' )
    call Array1%Free()

  end subroutine SAE_Block_Transformation




  subroutine ComputeAndSaveBlock( Lmax, Space, Basis, GABSMat, LabelOp, lOp, Irrep, SRSOrbSet, FormattedBlock )

    use symba

    integer                          , intent(in) :: Lmax
    Class(ClassElectronicSpace)      , intent(in) :: Space
    Class(ClassBasis)                , intent(in) :: Basis
    Class(ClassMatrix)               , intent(in) :: GABSMat(-lOp:lOp,0:Lmax)
    character(len=*)                 , intent(in) :: LabelOp
    integer                          , intent(in) :: lOp
    Class(ClassIrrep)                , intent(in) :: Irrep
    Class(ClassShortRangeSymOrbitals), intent(in) :: SRSOrbSet
    logical                          , intent(in) :: FormattedBlock

    real(kind(1d0)), allocatable :: CartCoeff(:,:)

    integer :: mOp, iXlm, lKet, mKet, lBra, mBra, lBraMin, lBraMax
    type(ClassXlm) :: OpXlm, Xlm, BraXlm
    type(ClassXlmSymmetricSet) :: XlmSymSet
    logical :: firstCycle
    real(kind(1d0)) :: AngularFactor
    complex(kind(1d0)), allocatable :: NewXlmToCartMat(:,:)
    integer :: NumFunGABS, NumSkippedBs, NumGaussianGABS, NumExponents
    type(ClassMatrix)  :: CopyGABSMat
    type(ClassMatrix)  :: CorrectMat
    type(ClassMatrix)  :: DeltaBlock_Diffuse_BSplineXlm
    type(ClassMatrix)  :: DeltaBlock_BSplineXlm_BSplineXlm
    type(ClassMatrix)  :: Block_Diffuse_BSplineXlm
    type(ClassMatrix)  :: Block_BSplineXlm_BSplineXlm
    type(ClassDiffuseBsplineXlmBlock) :: DBSBlock
    type(ClassBsplineXlmBsplineXlmBlock) :: BSBSBlock

    call SRSOrbSet%FetchCartesianCoeff( CartCoeff )

    NumFunGABS      = Basis%GetNFun()
    NumSkippedBs    = Basis%GetNBSplineSkip()
    NumGaussianGABS = Basis%GetNFun("G")
    NumExponents    = Basis%GetGaussianNumExponents()

    do mOp = -lOp, lOp

       call OpXlm%init( lOp, mOp )
       call XlmSymSet%init(  Lmax, Irrep *  OpXlm%GetIrrep( Irrep%GetGroup() ) )
       do iXlm = 1, XlmSymSet%GetNXlm()

          Xlm = XlmSymSet%GetXlm( iXlm )
          lKet = Xlm%Getl()
          mKet = Xlm%Getm()

          firstCycle = .TRUE.

          lBraMin = abs(lKet-lOp)
          lBraMax = min(lKet+lOp,Lmax)
          do lBra = lBraMin, lBraMax, 2
             do mBra = -lBra, lBra

                AngularFactor = TripleXlmIntegral( lBra, mBra, lOp, mOp, lKet, mKet ) * &
                     sqrt( 4 * PI / dble( ( 2 * lKet + 1 ) * ( 2 * lOp + 1 ) ) ) / CGC(lKet,lOp,lBra,0,0)
                if( abs(AngularFactor) < tiny(1.d0) ) cycle

                call SRSOrbSet%FetchklmMask( NewXlmToCartMat, lBra, mBra, NumExponents )

                !.. Transform and save the GABS block
                CopyGABSMat = GABSMat(lBra-lKet,lKet)
                call CopyGABSMat%Multiply( AngularFactor )
                call CopyGABSMat%GetSubMatrix( NRowMin(lBra), NRowMax(lBra), &
                     NumGaussianGABS + NumSkippedBs + 1, NumFunGABS, CorrectMat )
                call SAE_Block_Transformation( CorrectMat, NewXlmToCartMat, CartCoeff, DeltaBlock_Diffuse_BSplineXlm )
                !.. B-spline B-spline block doesn't need to be transformed.
                call CopyGABSMat%GetSubMatrix( NumGaussianGABS + NumSkippedBs + 1, NumFunGABS, &
                     NumGaussianGABS + NumSkippedBs + 1, NumFunGABS, DeltaBlock_BSplineXlm_BSplineXlm )
                call BraXlm%Init( lBra, mBra )

                if(firstCycle)then
                   Block_Diffuse_BSplineXlm = DeltaBlock_Diffuse_BSplineXlm
                   Block_BSplineXlm_BSplineXlm = DeltaBlock_BSplineXlm_BSplineXlm
                   firstCycle=.FALSE.
                   call BSBSBlock%init( BraXlm, OpXlm, LabelOp, Xlm, Block_BSplineXlm_BSplineXlm )
                   call BSBSBlock%save( Space%GetStorageDir(), FormattedBlock )
                   cycle
                end if
                call Block_Diffuse_BSplineXlm%Add( DeltaBlock_Diffuse_BSplineXlm )
                Block_BSplineXlm_BSplineXlm = DeltaBlock_BSplineXlm_BSplineXlm
                
                call BSBSBlock%init( BraXlm, OpXlm, LabelOp, Xlm, Block_BSplineXlm_BSplineXlm )
                call BSBSBlock%save( Space%GetStorageDir(), FormattedBlock )

             enddo
          enddo

          if( .not.firstCycle )then
             call DBSBlock%init( Irrep, OpXlm, LabelOp, Xlm, Block_Diffuse_BSplineXlm )
             call DBSBlock%save( Space%GetStorageDir(), FormattedBlock )
          end if

       enddo

    enddo

  end subroutine ComputeAndSaveBlock






end program ConvertQCI


  subroutine FetchMultipolesData( MultipolesFile, NumChannels, AngMom, BraIndex, KetIndex, CartLabel, MultipoleValues )
    !
    use ModuleIO
    use ModuleErrorHandling

    character(len=*),               intent(in) :: MultipolesFile
    integer,                        intent(in) :: NumChannels
    integer,                        intent(in) :: AngMom
    integer,                        intent(in) :: BraIndex
    integer,                        intent(in) :: KetIndex
    character(len=32), allocatable, intent(out) :: CartLabel(:)
    real(kind(1d0)),   allocatable, intent(out) :: MultipoleValues(:)
    !
    integer :: uid, l, NumCartLabel, Counter, iostat
    character(len=32) :: Strn
    logical :: ReadFile
    real(kind(1d0)) :: Val
    !
    ReadFile = .true.
    !
    NumCartLabel = (AngMom+2)*(AngMom+1)/2
    !
    allocate( CartLabel(NumCartLabel) )
    allocate( MultipoleValues(NumCartLabel) )
    MultipoleValues = 0.d0
    !
    call OpenFile( MultipolesFile, uid, "READ", "FORMATTED" )
    !
    Counter = 0
    !
    do while( ReadFile )
       !
       read(uid,*,IOSTAT=iostat) Strn
       l = LEN_TRIM( Strn )
       !
       if ( iostat < 0 ) then
          call BuildCartLabel ( AngMom, CartLabel )
          call ErrorMessage( "The cartesian multipoles do not reach 2*Lmax, then those that are not present are considered zero." )
          exit
       else
          if( l == AngMom ) then
             !
             Counter = Counter + 1
             CartLabel(Counter) = Strn
             Strn = " "
             call ReadBlock( uid, NumChannels, BraIndex, KetIndex, Val )
             MultipoleValues(Counter) = Val
             if ( Counter == NumCartLabel ) exit
             !
          else
             !
             call SkipBlock( uid, NumChannels )
             !
          end if
          !
       end if
       !
    end do
    !
    close( uid )
    !
    !
  contains
    !
    !
    subroutine BuildCartLabel ( AngMom, CartLabel )
      !
      integer,           intent(in)  :: AngMom
      character(len=32), intent(out) :: CartLabel(:)
      !
      integer :: i, n, l, TotComb, IntVal, Counter, ValI, ValJ, ValK
      character(len=512) :: Strn
      integer, allocatable :: IntArray(:,:), NumberIJK(:,:), NumberIndIJK(:,:)
      logical :: AreDiff
      integer, parameter :: NumCart = 3
      character(len=8), parameter :: CartI = "x"
      character(len=8), parameter :: CartJ = "y"
      character(len=8), parameter :: CartK = "z"
      !
      TotComb = NumCart**AngMom
      allocate( IntArray(TotComb,AngMom))
      IntArray = 0
      !
      do l = 1, AngMom
         do n = 1, TotComb
            !
            if ( (mod(n,NumCart**l) > 0 ) .and. &
                 (mod(n,NumCart**l) <= NumCart**(l-1)) ) then
               IntVal = 1
            elseif ( (mod(n,NumCart**l) > NumCart**(l-1)) .and. &
                 (mod(n,NumCart**l) <= 2*NumCart**(l-1)) ) then
               IntVal = 2
            else
               IntVal = 3
            end if
            !
            IntArray(n,l) = IntVal
            !
         end do
      end do
      !
      !
      allocate( NumberIJK(TotComb,3) )
      NumberIJK = 0
      ! Extract the combinations which are different, disregarding permutations.
      do n = 1, TotComb
         !
         ValI = 0
         ValJ = 0
         ValK = 0
         !
         do l = 1, AngMom
            !
            IntVal = IntArray(n,l)
            if ( IntVal == 1 ) ValI = ValI + 1
            if ( IntVal == 2 ) ValJ = ValJ + 1
            if ( IntVal == 3 ) ValK = ValK + 1
            !
            NumberIJK(n,1) = ValI
            NumberIJK(n,2) = ValJ
            NumberIJK(n,3) = ValK
            !
         end do
      end do
      !
      allocate( NumberIndIJK(size(CartLabel),3) )
      NumberIndIJK = 0
      NumberIndIJK(1,:) = NumberIJK(1,:)
      Counter = 1
      do n = 2, TotComb
         !
         AreDiff = .true.
         !
         do i = 1, size(CartLabel)
            !
            !
            if ( (NumberIJK(n,1) == NumberIndIJK(i,1)) .and. &
                 (NumberIJK(n,2) == NumberIndIJK(i,2)) .and. &
                 (NumberIJK(n,3) == NumberIndIJK(i,3)) ) then
               AreDiff = .false.
               exit
            end if
            !
         end do
         !
         if ( AreDiff ) then
            Counter = Counter + 1
            NumberIndIJK(Counter,:) = NumberIJK(n,:)
         end if
         !
      end do
      !
      !
      do n = 1, size(CartLabel)
         !
         Strn = ' '
         !
         do i = 1, NumberIndIJK(n,1)
            Strn = trim(adjustl(Strn))//trim(adjustl(CartI))
         end do
         !
         do i = 1, NumberIndIJK(n,2)
            Strn = trim(adjustl(Strn))//trim(adjustl(CartJ))
         end do
         !
         do i = 1, NumberIndIJK(n,3)
            Strn = trim(adjustl(Strn))//trim(adjustl(CartK))
         end do
         !
         CartLabel(n) = trim(adjustl(Strn))
         !
      end do
      !
    end subroutine BuildCartLabel
    !
    subroutine SkipBlock( uid, NumChannels )
      integer, intent(in) :: uid, NumChannels
      integer :: i
      do i = 1, NumChannels
         read(uid,*)
      end do
    end subroutine SkipBlock
    !
    subroutine ReadBlock( uid, NumChannels, BraIndex, KetIndex, Val )
      !
      integer,         intent(in)  :: uid, NumChannels, BraIndex, KetIndex
      real(kind(1d0)), intent(out) :: Val
      integer :: i, j
      real(kind(1d0)), allocatable :: LineVal(:)
      !
      allocate( LineVal(NumChannels) )
      !
      do i = 1, NumChannels
         if( i == BraIndex ) then
            read(uid,*) ( LineVal(j), j=1,NumChannels )
         else
            read(uid,*)
         end if
      end do
      !
      Val = LineVal(KetIndex)
      deallocate( LineVal )
      !
    end subroutine ReadBlock
    !
  end subroutine FetchMultipolesData


