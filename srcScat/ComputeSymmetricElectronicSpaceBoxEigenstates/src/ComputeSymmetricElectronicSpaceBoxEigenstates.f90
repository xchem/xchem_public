!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!
program ComputeSymmetricElectronicSpaceBoxEigenstates

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleGeneralInputFile
  use ModuleAbsorptionPotential

  implicit none


  logical         , parameter :: BOX_ONLY = .TRUE.
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  logical         , parameter :: REMOVE_BIG_EIGENVALUES = .FALSE.


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  logical                       :: OnlyQC
  logical                       :: OnlyLoc
  logical                       :: OnlyPoly
  logical                       :: OnlyDiagQH
  logical                       :: DecoupleChannels
  logical                       :: AnalyzeEvcts

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod


  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: OverlapMat, HamiltonianMat, CAP
  type(ClassSESSESBlock)        :: QCOverlapMat
  type(ClassMatrix)             :: OvMat, HamMat, CAPMat, QCOvMat
  type(ClassComplexMatrix)      :: QHMat, CompOvMat
  type(ClassSpectralResolution) :: OvSpecRes, HamSpecRes, CAPSpecRes
  type(ClassComplexSpectralResolution) :: QHSpecRes
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  type(ClassAbsorptionPotential):: AbsPot

  character(len=:), allocatable :: DipoleAxis
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: ConditionLabel
  integer :: i
  real(kind(1d0)) :: MaxAllowedEigVal
  logical :: MatIsSymmetric

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       BraSymLabel            ,   &
       KetSymLabel            ,   &
       UseConditionedBlocks   ,   &
       UsePerfectProjection   ,   &
       OnlyQC                 ,   &
       OnlyLoc                ,   &
       OnlyPoly               ,   &
       OnlyDiagQH             ,   &
       DecoupleChannels       ,   &
       AnalyzeEvcts)


  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "SAE General Input File  : "//GeneralInputFile
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File : ", ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ", CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 
  write(OUTPUT_UNIT,"(a,L)" )"Only QC, polycentric and monocentric Gaussian :     ", OnlyQC 
  write(OUTPUT_UNIT,"(a,L)" )"Only localized :     ", OnlyLoc 
  write(OUTPUT_UNIT,"(a,L)" )"Only polycentric Gaussian :     ", OnlyPoly
  write(OUTPUT_UNIT,"(a,L)" )"Only digonalize the Quenched Hamiltonian :", OnlyDiagQH



  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod




  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OverlapLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )


  if ( SET_FORMATTED_WRITE) call OverlapMat%SetFormattedWrite()
  if ( BOX_ONLY .and. &
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly) call OverlapMat%SetBoxOnly()
  !

  write(output_unit,"(a)") 'Loading overlap ...'
  !
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly) then
     if ( UseConditionedBlocks ) then
        Group  => Space%GetGroup()
        irrepv => Group%GetIrrepList()
        Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
        !.. Auxiliar conditioner only with the purpose to get the label.
        call Conditioner%Init(     &
             ConditionBsMethod   , &
             ConditionBsThreshold, &
             NumBsDropBeginning  , &
             NumBsDropEnding     , &
             irrepv(1), Xlm      , &
             UsePerfectProjection )
        allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis, ConditionLabel , LoadLocStates=LoadLocStates )
     else
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis, LoadLocStates = LoadLocStates )
     end if
     call OverlapMat%Assemble( LoadLocStates, OvMat )
  elseif( OnlyQC ) then
     call OverlapMat%LoadQC( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
     call OverlapMat%AssembleQC( LoadLocStates, OvMat )
  elseif( OnlyLoc ) then
     call OverlapMat%LoadLoc( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
     call OverlapMat%AssembleLoc( LoadLocStates, OvMat )
  elseif( OnlyPoly ) then
     call OverlapMat%LoadPoly( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
     call OverlapMat%AssemblePoly( LoadLocStates, OvMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          OverlapLabel//' operator matrix.' )
  end if
  !
  allocate( SymStorageDir, source = OverlapMat%GetStorageDir() )
  !call OvMat%writeColumnLabels( GetColumnLabelsFileName(SymStorageDir,OverlapLabel,ConditionLabel) )
  !
  call OverlapMat%Free()
  MatIsSymmetric = OvMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'S is symmetric: ', MatIsSymmetric
  !
  
  if ( .not.OnlyDiagQH ) then
     write(output_unit,"(a)") 'Diagonalizing overlap ...'
     call OvMat%Diagonalize( OvSpecRes )
     !
     if ( OnlyQC ) then
        call OvSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,OverlapLabel,ConditionLabel)//QCLabel,1 )
        call OvSpecRes%Write( GetSpectrumFileName(SymStorageDir,OverlapLabel,ConditionLabel)//QCLabel )
     elseif( OnlyLoc ) then
        call OvSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,OverlapLabel,ConditionLabel)//LocLabel,1 )
        call OvSpecRes%Write( GetSpectrumFileName(SymStorageDir,OverlapLabel,ConditionLabel)//LocLabel )
     elseif( OnlyPoly ) then
        call OvSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,OverlapLabel,ConditionLabel)//PolyLabel,1 )
        call OvSpecRes%Write( GetSpectrumFileName(SymStorageDir,OverlapLabel,ConditionLabel)//PolyLabel )
     else
    !    call OvSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,OverlapLabel,ConditionLabel),1 )
        call OvSpecRes%Write( GetSpectrumFileName(SymStorageDir,OverlapLabel,ConditionLabel) )
     end if
     call OvSpecRes%Free()
     !
  end if


  if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
  if ( BOX_ONLY .and.&
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly ) call HamiltonianMat%SetBoxOnly()
  !
  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  if ( DecoupleChannels ) then
    write(6,'(A)') "XCHEMQC decouple channels"
    call HamiltonianMat%SetDecoupleChannels( ) 
  endif 
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly ) then
     if ( UseConditionedBlocks ) then
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, ConditionLabel,&
          LoadLocStates=LoadLocStates )
     else
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, LoadLocStates=LoadLocStates )
     end if
     call HamiltonianMat%Assemble( LoadLocStates, HamMat )
  elseif ( OnlyQC ) then
     call HamiltonianMat%LoadQC( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssembleQC( LoadLocStates, HamMat )
  elseif ( OnlyLoc ) then
     call HamiltonianMat%LoadLoc( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssembleLoc( LoadLocStates, HamMat )
  elseif ( OnlyPoly ) then
     call HamiltonianMat%LoadPoly( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssemblePoly( LoadLocStates, HamMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          HamiltonianLabel//' operator matrix.' )
  end if
  !
  call HamiltonianMat%Free()
  MatIsSymmetric = HamMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'H is symmetric: ', MatIsSymmetric
  !

  if ( .not.OnlyDiagQH ) then
     !
     if ( REMOVE_BIG_EIGENVALUES ) then
        if ( SET_FORMATTED_WRITE) call QCOverlapMat%SetFormattedWrite()
        call QCOverlapMat%LoadQC( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
        call QCOverlapMat%AssembleQC( LoadLocStates, QCOvMat )
        call QCOverlapMat%Free()
        MaxAllowedEigVal = QCOvMat%GetMaxEigVal()
     end if
     !
     !
     write(output_unit,"(a)") 'Diagonalizing Hamiltonian ...'
     if ( REMOVE_BIG_EIGENVALUES ) then
        call HamMat%Diagonalize( OvMat, HamSpecRes, ConditionNumber, MaxAllowedEigVal )
     else
        call HamMat%Diagonalize( OvMat, HamSpecRes, ConditionNumber )
     end if
     !
     if ( DecoupleChannels ) then
        if ( OnlyQC ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//&
             QCLabel//"_decouple",1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//QCLabel//"_decouple" )
        elseif ( OnlyLoc ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//&
             LocLabel//"_decouple",1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//LocLabel//"_decouple" )
        elseif ( OnlyPoly ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//&
             PolyLabel//"_decouple",1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//PolyLabel//"_decouple" )
        else
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//&
             "_decouple",1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//"_decouple" )
           if (AnalyzeEvcts) call HamSpecRes%AnalyzeEigenVectors( HamMat, OvMat,&
             GetEigenVecAnalFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//"_decouple" )
        end if
     else
        if ( OnlyQC ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//QCLabel,1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//QCLabel )
        elseif ( OnlyLoc ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//LocLabel,1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//LocLabel )
        elseif ( OnlyPoly ) then
           call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//PolyLabel,1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
        else
           !call HamSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel,ConditionLabel),1 )
           call HamSpecRes%Write( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel) )
           if (AnalyzeEvcts) call HamSpecRes%AnalyzeEigenVectors( HamMat, OvMat, GetEigenVecAnalFileName(SymStorageDir,&
             HamiltonianLabel,ConditionLabel) )
        end if
     endif
     call HamSpecRes%Free()
     !
  end if


  !.. Complex absorber part.
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly ) then
     !
     CompOvMat = OvMat
     !.. Load the absorption potential parameters
     call SAEGeneralInputData%ParseFile( GeneralInputFile )
     call AbsPot%Parse( AbsorptionPotentialFile )
     !
     do i = 1, AbsPot%NumberOfTerms()
        !
        if ( SET_FORMATTED_WRITE) call CAP%SetFormattedWrite()
        if ( BOX_ONLY  ) call CAP%SetBoxOnly()
        write(output_unit,"(a)") 'Loading CAP ...'
        if ( UseConditionedBlocks ) then
           call CAP%Load( BraSymSpace, KetSymSpace, GetFullCAPLabel(i), DipoleAxis, ConditionLabel, LoadLocStates=LoadLocStates )
        else
           call CAP%Load( BraSymSpace, KetSymSpace, GetFullCAPLabel(i), DipoleAxis, LoadLocStates=LoadLocStates )
        end if
        call CAP%Assemble( LoadLocStates, CAPMat )
        call CAP%Free()
        !
        if ( .not.OnlyDiagQH ) then
           write(output_unit,"(a)") 'Diagonalizing CAP '//AlphabeticNumber(i)//' ...'
           call CAPMat%Diagonalize( OvMat, CAPSpecRes, ConditionNumber )
           !
     !      call CAPSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,GetFullCAPLabel(i),ConditionLabel),1 )
           call CAPSpecRes%Write( GetSpectrumFileName(SymStorageDir,GetFullCAPLabel(i),ConditionLabel) )
           call CAPSpecRes%Free()
        end if
        !
        !
        !.. Quenched Hamiltonian part
        QHMat = CAPMat
        call CAPMat%Free()
        call QHMat%Multiply( AbsPot%Coeff(i) )
        call QHMat%Add( HamMat )
        write(output_unit,"(a)") 'Diagonalizing QH '//AlphabeticNumber(i)//' ...'
        call QHMat%Diagonalize( CompOvMat, QHSpecRes, ConditionNumber )
        !call QHMat%Free()
        !
        if ( DecoupleChannels ) then
            call QHSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,GetFullQHLabel(i),ConditionLabel)//&
              "_decouple",1 )
            call QHSpecRes%Write( GetSpectrumFileName(SymStorageDir,GetFullQHLabel(i),ConditionLabel)//"_decouple" )
            if (AnalyzeEvcts) call QHSpecRes%AnalyzeEigenVectors(QHMat, CompOvMat, GetEigenVecAnalFileName(SymStorageDir,&
              GetFullQHLabel(i),ConditionLabel)//"_decouple" )
        else
      !      call QHSpecRes%WriteEigenvalues( GetEigenvaluesFileName(SymStorageDir,GetFullQHLabel(i),ConditionLabel),1 )
            call QHSpecRes%Write( GetSpectrumFileName(SymStorageDir,GetFullQHLabel(i),ConditionLabel) )
            if (AnalyzeEvcts) call QHSpecRes%AnalyzeEigenVectors(QHMat, CompOvMat, GetEigenVecAnalFileName(SymStorageDir,&
              GetFullQHLabel(i),ConditionLabel) )
        endif
        call QHMat%Free()
        call QHSpecRes%Free()
        !
     end do

  end if


  write(output_unit,'(a)') "Done"
 


contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       BraSymLabel               , &
       KetSymLabel               , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       OnlyDiagQH                , &
       DecoupleChannels          , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    logical,                       intent(out)   :: UseConditionedBlocks
    logical,                       intent(out)   :: UsePerfectProjection
    logical,                       intent(out)   :: OnlyQC
    logical,                       intent(out)   :: OnlyLoc
    logical,                       intent(out)   :: OnlyPoly  
    logical,                       intent(out)   :: OnlyDiagQH  
    logical,                       intent(out)   :: DecoupleChannels
    logical,                       intent(out)   :: AnalyzeEvcts
  !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: gif, pif, ccfile, qcdir, brasym, ketsym
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"      , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-pp"  , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-onlyqc"  , "Only diagonalize the quantum chemistry sub-matrices." )
    call List%Add( "-onlyloc"  , "Only diagonalize the localized sub-matrices." )
    call List%Add( "-onlypoly"  , "Only diagonalize the localized sub-matrices." )
    call List%Add( "-onlydiagQH"  , "Only diagonalize the Quenched Hamiltonian." )
    call List%Add( "-DecoupleChannels"  , "Do not load the couplings between different channels in diagonalizing Hamiltonian." )
    call List%Add( "-AnalyzeEvcts"  , &
      "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-gif" , gif  )
    call List%Get( "-pif" , pif  )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-esdir" , qcdir  )
    UseConditionedBlocks = List%Present( "-cond" )
    UsePerfectProjection = List%Present( "-pp" )
    OnlyQC               = List%Present( "-onlyqc" )
    OnlyLoc              = List%Present( "-onlyloc" )
    OnlyPoly             = List%Present( "-onlypoly" )
    OnlyDiagQH           = List%Present( "-onlydiagQH" )
    DecoupleChannels     = List%Present( "-DecoupleChannels" ) 
    AnalyzeEvcts         = List%Present( "-AnalyzeEvcts" ) 
    !
    allocate( GeneralInputFile, source = trim( gif ) )
    allocate( ProgramInputFile, source = trim( pif ) )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults, source = trim( qcdir ) )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    !
    call List%Free()
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    if ( OnlyQC .and. OnlyLoc ) then
       call Assert( '"onlyqc" and "onlyloc" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyQC .and. OnlyPoly ) then
       call Assert( '"onlyqc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyLoc .and. OnlyPoly ) then
       call Assert( '"onlyloc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"     , ConditionNumber  )
    call List%Get( "LoadLocStates"       , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile






end program ComputeSymmetricElectronicSpaceBoxEigenstates
