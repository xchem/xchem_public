!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
program ComputeMultichannelScatteringStates
 
 
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleErrorHandling
  use ModuleGeneralInputFile
  use ModuleBasis
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleIO


  implicit none

  !> If .true./.false. the close-coupling blocks will be read or
  !! write with/without format.
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  !

  !.. Run time parameters
  !> [GeneralInputFile](@ref GeneralInputFile)  
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  real(kind(1d0))               :: Emin = -huge(1d0)
  real(kind(1d0))               :: Emax = huge(1d0)
  integer                       :: NumEnergies
  character(len=:), allocatable :: CompMethod 
  logical                       :: PlotScattStates
  logical                       :: ConcatenateEigenPhases
  logical                       :: RemoveScattStates
  logical                       :: FeshbachMethod
  
  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber
  real(kind(1d0))               :: ScattConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  !.. Rest of variables.
  !> Radial basis class ([ModuleBasis](@ref ModuleBasis.f90)).  
  type(ClassBasis)              :: Basis
  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: OverlapMat, HamiltonianMat
  type(ClassMatrix)             :: OvMat, HamMat, QCFullConditioner, Overlap, Hamiltonian 
  type(ClassScatteringStatesVec) :: ScattStatesVec
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  !
  real(kind(1d0)), allocatable  :: EnergyVec(:)
  !
  character(len=:), allocatable :: DipoleAxis
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: ConditionLabel
  character(len=:), allocatable :: SAEstoredir
  character(len=:), allocatable :: SAEBasisFile
  !
  !> Number of rows of the full quantum chemistry operator matrices.
  integer  :: NumFunQCBra
  !> Number of columns of the full quantum chemistry operator matrices.
  integer  :: NumFunQCKet
  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: NumPWC
  !> Minimum number of B-splines among all channels that do not 
  !! overlap with the diffuse Gaussian orbitals.
  integer :: NumBsNotOverlap

  call GetRunTimeParameters(    &
       GeneralInputFile       , &
       ProgramInputFile       , &
       Multiplicity           , &
       CloseCouplingConfigFile, &
       NameDirQCResults       , &    
       BraSymLabel            , &
       KetSymLabel            , &
       Emin                   , &
       Emax                   , &
       NumEnergies            , &
       CompMethod             , &
       PlotScattStates        , &
       UseConditionedBlocks   , &
       UsePerfectProjection   , &
       ConcatenateEigenPhases , &
       RemoveScattStates      , &
       FeshbachMethod         )
  
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(2a)" ) "SAE General Input File : ", GeneralInputFile
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File : ", ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ", CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  if ( Emin > -huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,f12.6)" )"Minimum Energy :        ", Emin
  end if
  if ( Emax < huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,f12.6)" )"Maximum Energy :        ", Emax
  end if
  if ( .not.ConcatenateEigenPhases ) then
     write(OUTPUT_UNIT,"(a,i0)" )"Number of Energies :    ", NumEnergies
  end if
  write(OUTPUT_UNIT,"(2a)" ) "Computational Method :  ", CompMethod
  write(OUTPUT_UNIT,"(a,L)" )"Plot scattering states :", PlotScattStates
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 
  write(OUTPUT_UNIT,"(a,L)" )"Concatenate eigenphases :     ", ConcatenateEigenPhases 
  write(OUTPUT_UNIT,"(a,L)" )"Remove the computed scattering states in the energy range set :     ", RemoveScattStates 
  write(OUTPUT_UNIT,"(a,L)" )"Use the Feshbach partition method :     ", FeshbachMethod 



  call ParseProgramInputFile( &
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      , &
       ScattConditionNumber , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for scattering :      ", ScattConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod



  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 

  !.. Initialize the basis
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call Basis%Init( SAEBasisFile )
  call Basis%SetRoot( SAEstoredir )


  
  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
  
  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OverlapLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )
  

  if ( SET_FORMATTED_WRITE) call OverlapMat%SetFormattedWrite()
  !
  write(output_unit,"(a)") 'Loading overlap ...'
  !
  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          UsePerfectProjection )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
     if ( .not.(ConcatenateEigenPhases .or. &
          RemoveScattStates            .or. &
          PlotScattStates               ) ) &
          call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis, ConditionLabel )
  else
     if ( .not.(ConcatenateEigenPhases .or. &
          RemoveScattStates            .or. &
          PlotScattStates               ) ) &
          call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
  end if
  !
  allocate( SymStorageDir, source = GetBlockStorageDir(BraSymSpace,KetSymSpace) )
  !
  
  !.. Only removes the scattering states and eigenphases
  if ( RemoveScattStates ) then
     !
     call RemoveComputedFiles( &
          SymStorageDir       , &
          CompMethod          , &
          Emin, Emax          , &
          ScattConditionNumber, &
          ConditionLabel      , &
          FeshbachMethod      )
     !
     stop
     !
  end if

  !.. Only concatenates the eigenphases
  if ( ConcatenateEigenPhases ) then
     !
     call BuildEigenphasesFile( &
          SymStorageDir       , &
          CompMethod          , &
          Emin, Emax          , &
          ScattConditionNumber, &
          ConditionLabel      , &
          FeshbachMethod      )
     !
     stop
     !
  end if


  !.. Only plots the scattering states
  if ( PlotScattStates ) then
     !
     write(OUTPUT_UNIT,"(a)" ) 'Plotting scattering states ...'
     call PlotScatteringStates( &
          SymStorageDir       , &
          KetSymSpace         , &
          Basis               , &
          CompMethod          , &
          Emin, Emax          , &
          ScattConditionNumber, &
          ConditionLabel      )
     !
     stop
     !
  end if


  !
  call OverlapMat%Assemble( LoadLocStates, OvMat )
  call OverlapMat%GetNumFunQC( LoadLocStates, NumFunQCBra, NumFunQCKet )
  NumBsNotOverlap = OverlapMat%GetMinNumNotOvBs()
  if ( NumFunQCBra /= NumFunQCKet ) Call Assert( &
       'The overlap matrix must be squared.' )
  !
  call OverlapMat%Free()




  if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
  !
  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  if ( UseConditionedBlocks ) then
     call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, ConditionLabel )
  else
     call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
  end if
  !
  call HamiltonianMat%Assemble( LoadLocStates, HamMat )
  call HamiltonianMat%Free()


  write(output_unit,"(a,2i0)") 'Original dimension of S', OvMat%NRows(), OvMat%NColumns()
  write(output_unit,"(a,2i0)") 'Original dimension of H', HamMat%NRows(), HamMat%NColumns()


  !.. For S and H operators the Bra and Ket symmetric spaces are the same.
  NumPWC = BraSymSpace%GetNumPWC()
  

  !.. Condition the QC part plus the B-splines overlapping with
  ! it, to apply the HSLE and INVM methods.
  if ( CONDITION_QC ) then
     if ( (CompMethod .is. HSLEMethodLabel) .or. &
          (CompMethod .is. InvMatMethodLabel) ) then
        write(output_unit,"(a)") 'Conditioning QC part ...'
        call ComputeQCConditioner( OvMat, OvMat%NRows()-NumPWC, ScattConditionNumber, QCFullConditioner )
!!$        call ComputeQCConditioner( OvMat, OvMat%NRows()-NumPWC*NumBsNotOverlap, ScattConditionNumber, QCFullConditioner )
!!$        call ComputeQCConditioner( OvMat, NumFunQCBra, ConditionNumber, QCFullConditioner )
        call ApplyQCConditioner( OvMat , QCFullConditioner )
        call ApplyQCConditioner( HamMat, QCFullConditioner )
        write(output_unit,"(a,2i0)") 'Dimension of S after QC conditioning', OvMat%NRows(), OvMat%NColumns()
        write(output_unit,"(a,2i0)") 'Dimension of H after QC conditioning', HamMat%NRows(), HamMat%NColumns()
     end if
  end if

  !.. Remove the last B-splines from the rows
  call OvMat%GetSubMatrix(        &
       1, OvMat%NRows() - NumPWC, &
       1, OvMat%NColumns()      , &
       Overlap           )
  call OvMat%Free()
  call HamMat%GetSubMatrix(        &
       1, HamMat%NRows() - NumPWC, &
       1, HamMat%NColumns()      , &
       Hamiltonian        )
  call HamMat%Free()

  write(output_unit,"(a,2i0)") 'Dimension of S for scattering', Overlap%NRows(), Overlap%NColumns()
  write(output_unit,"(a,2i0)") 'Dimension of H for scattering', Hamiltonian%NRows(), Hamiltonian%NColumns()

  !.. Build energy array
  call BuildGrid( NumEnergies, Emin, Emax, EnergyVec )

  
  call ScattStatesVec%Init( &
       Basis, BraSymSpace, SymStorageDir, NumBsNotOverlap, &
       CompMethod, EnergyVec, ScattConditionNumber, ConditionLabel )
  !
  if ( CONDITION_QC ) call ScattStatesVec%SetConditionerQC( QCFullConditioner )
  call QCFullConditioner%Free()
  !
  call ScattStatesVec%ComputeScatteringStates( Overlap, Hamiltonian, FeshbachMethod )
  !
  call Overlap%Free()
  call Hamiltonian%Free()
  deallocate( EnergyVec )
  

  write(output_unit,'(a)') 'Done'

contains

  
  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &    
       BraSymLabel               , &
       KetSymLabel               , &
       Emin                      , &
       Emax                      , &
       NumEnergies               , &
       CompMethod                , &
       PlotScattStates           , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       ConcatenateEigenPhases    , &
       RemoveScattStates         , &
       FeshbachMethod            )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    !> [GeneralInputFile](@ref GeneralInputFile)  
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer                      , intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    real(kind(1d0))              , intent(inout) :: Emin
    real(kind(1d0))              , intent(inout) :: Emax
    integer                      , intent(out)   :: NumEnergies
    character(len=:), allocatable, intent(out)   :: CompMethod
    logical                      , intent(out)   :: PlotScattStates
    logical                      , intent(out)   :: UseConditionedBlocks
    logical                      , intent(out)   :: UsePerfectProjection
    logical                      , intent(out)   :: ConcatenateEigenPhases
    logical                      , intent(out)   :: RemoveScattStates
    logical                      , intent(out)   :: FeshbachMethod
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: pif, brasym, ketsym, ccfile, qcdir, cm, gif
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Computes and saves the single ionization "//&
         "multichannel scattering states defined by the Close-Coupling "//&
         "expansion, as well as the eigenphases." 
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-Emin"   , "Minimum Energy"    , 1.d0, "optional" )
    call List%Add( "-Emax"   , "Maximum Energy"    , 1.d0, "optional" )
    call List%Add( "-NE"     , "Number of Energies", 100, "optional" )
    call List%Add( "-cm"     , "Computational Method",  " ", "required" )
    call List%Add( "-plot"  , "Plot scattering states" )
    call List%Add( "-cond"   , "Use the conditioned blocks" )
    call List%Add( "-pp"     , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-concatenate"     , "Concatenate all the available eigenphases within "//&
         "the energy range specified, for the kind of conditioning and method set." )
    call List%Add( "-remove"     , "Remove all the computed scattering states in the energy range set." )
    call List%Add( "-feshbach"     , "Use Feshbach partition method." )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif"    , gif )
    call List%Get( "-pif"    , pif  )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-esdir"  , qcdir  )
    if ( List%Present("-Emin") ) then
       call List%Get( "-Emin"   , Emin )
    end if
    if ( List%Present("-Emax") ) then
       call List%Get( "-Emax"   , Emax )
    end if
    if ( List%Present("-NE") ) then
       call List%Get( "-NE"     , NumEnergies )
    end if
    call List%Get( "-cm"     , cm  )
    PlotScattStates        = List%Present( "-plot" )
    UseConditionedBlocks   = List%Present( "-cond" )
    UsePerfectProjection   = List%Present( "-pp" )
    ConcatenateEigenPhases = List%Present( "-concatenate" )
    RemoveScattStates      = List%Present( "-remove" )
    FeshbachMethod         = List%Present( "-feshbach" )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    !
    if ( ConcatenateEigenPhases .and. RemoveScattStates ) call Assert( &
         'The options: "-concatenate" and "-remove" cannot be set at the same time.' )
    !
    if ( .not.(ConcatenateEigenPhases .or. PlotScattStates) ) then
       if ( .not.List%Present("-Emin") ) call Assert( &
            'The minimum energy: option "-Emin", must be present.' )
       if ( .not.List%Present("-Emax") ) call Assert( &
            'The maximum energy: option "-Emax", must be present.' )
       if (.not.RemoveScattStates ) then
          if ( .not.List%Present("-NE") ) call Assert( &
               'The number of energies: option "-NE", must be present.' )
       end if
    end if
    !
    call List%Free()
    !
    allocate( GeneralInputFile       , source = trim( gif )    )
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    allocate( CompMethod             , source = trim(cm)       )
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    call SetStringToUppercase( CompMethod )
    call CheckScatteringMethod( CompMethod )
    !
  end subroutine GetRunTimeParameters




  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            , &
       ScattConditionNumber       , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    real(kind(1d0))              , intent(out) :: ScattConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    call List%Add( "ScattConditionNumber", 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"      , ConditionNumber  )
    call List%Get( "ScattConditionNumber" , ScattConditionNumber  )
    call List%Get( "LoadLocStates" , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"   , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"      , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold" , ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"    , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionNumber )
    call CheckThreshold( ScattConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    call CheckLastBsIsPresent( ConditionBsMethod, NumBsDropEnding )
    !
  end subroutine ParseProgramInputFile




  subroutine RemoveComputedFiles( &
       SymStorageDir            , &
       CompMethod               , &
       Emin, Emax               , &
       ScattConditionNumber     , &
       ConditionLabel           , &
       FeshbachMethod           )
    !
    character(len=*)                    , intent(in)    :: SymStorageDir
    character(len=*)                    , intent(in)    :: CompMethod
    real(kind(1d0))                     , intent(in)    :: Emin
    real(kind(1d0))                     , intent(in)    :: Emax
    real(kind(1d0))                     , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable       , intent(inout) :: ConditionLabel
    logical                             , intent(in)    :: FeshbachMethod
    !
    character(len=256), allocatable :: FilesVec(:)
    !
    if ( FeshbachMethod ) then
       call GetIncomingScatteringFeshbachFilesVec( &
            SymStorageDir        , &
            CompMethod           , &
            Emin, Emax           , &
            ScattConditionNumber , &  
            ConditionLabel       , &
            FilesVec             )
    else
       call GetIncomingScatteringFilesVec( &
            SymStorageDir        , &
            CompMethod           , &
            Emin, Emax           , &
            ScattConditionNumber , &  
            ConditionLabel       , &
            FilesVec             )
    end if
    call RemoveFilesInList( FilesVec )
    deallocate( FilesVec )
    !
    call GetSinScatteringFilesVec( &
         SymStorageDir        , &
         CompMethod           , &
         Emin, Emax           , &
         ScattConditionNumber , &  
         ConditionLabel       , &
         FilesVec             )
    call RemoveFilesInList( FilesVec )
    deallocate( FilesVec )
    !
    call GetScatteringChannelsFilesVec( &
         SymStorageDir        , &
         CompMethod           , &
         Emin, Emax           , &
         ScattConditionNumber , &  
         ConditionLabel       , &
         FilesVec             )
    call RemoveFilesInList( FilesVec )
    deallocate( FilesVec )
    !
    if ( FeshbachMethod ) then
       call GetEigenphasesFeshbachFilesVec( &
            SymStorageDir         , &
            CompMethod            , &
            Emin, Emax            , &
            ScattConditionNumber  , &  
            ConditionLabel        , &
            FilesVec              )
    else
       call GetEigenphasesFilesVec( &
            SymStorageDir         , &
            CompMethod            , &
            Emin, Emax            , &
            ScattConditionNumber  , &  
            ConditionLabel        , &
            FilesVec              )
    end if
    call RemoveFilesInList( FilesVec )
    deallocate( FilesVec )
    !
  end subroutine RemoveComputedFiles




  subroutine BuildEigenphasesFile( &
       SymStorageDir             , &
       CompMethod                , &
       Emin, Emax                , &
       ScattConditionNumber      , &
       ConditionLabel            , &
       FeshbachMethod            )
    !
    character(len=*)                    , intent(in)    :: SymStorageDir
    character(len=*)                    , intent(in)    :: CompMethod
    real(kind(1d0))                     , intent(in)    :: Emin
    real(kind(1d0))                     , intent(in)    :: Emax
    real(kind(1d0))                     , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable       , intent(inout) :: ConditionLabel
    logical                             , intent(in)    :: FeshbachMethod
    !
    character(len=256), allocatable :: FilesVec(:)
    character(len=:), allocatable :: FileNameConcatenatedEigenphases, FileNameConcatenatedTotalPhase
    real(kind(1d0)) ,allocatable :: Eigenphases(:)
    integer :: uid, uidTot, uidSingle, i, j, k, NChannels
    real(kind(1d0)) :: Energy
    !
    integer  , parameter :: MaxNChannels = 1000
    !
    !
    if ( FeshbachMethod ) then
       !
       call GetEigenphasesFeshbachFilesVec( &
            SymStorageDir         , &
            CompMethod            , &
            Emin, Emax            , &
            ScattConditionNumber  , &  
            ConditionLabel        , &
            FilesVec              )
       !
       call GetConcatenatedEigenphasesFeshbachFileName( &
            SymStorageDir                     , &
            CompMethod                        , &
            Emin, Emax                        , &
            ScattConditionNumber              , &  
            ConditionLabel                    , &
            FileNameConcatenatedEigenphases   )
       !
       call GetConcatenatedTotalPhaseFeshbachFileName( &
            SymStorageDir                    , &
            CompMethod                       , &
            Emin, Emax                       , &
            ScattConditionNumber             , &  
            ConditionLabel                   , &
            FileNameConcatenatedTotalPhase   )
       !
    else
       !
       call GetEigenphasesFilesVec( &
            SymStorageDir         , &
            CompMethod            , &
            Emin, Emax            , &
            ScattConditionNumber  , &  
            ConditionLabel        , &
            FilesVec              )
       !
       call GetConcatenatedEigenphasesFileName( &
            SymStorageDir                     , &
            CompMethod                        , &
            Emin, Emax                        , &
            ScattConditionNumber              , &  
            ConditionLabel                    , &
            FileNameConcatenatedEigenphases   )
       !
       call GetConcatenatedTotalPhaseFileName( &
            SymStorageDir                    , &
            CompMethod                       , &
            Emin, Emax                       , &
            ScattConditionNumber             , &  
            ConditionLabel                   , &
            FileNameConcatenatedTotalPhase   )
       !
    end if
    !
    !
    call OpenFile( FileNameConcatenatedEigenphases, uid,  'write', 'formatted' )
    write(uid,'(3(a,5x))') "#  Energy","N Open Channels", "Eigenphases"
    call OpenFile( FileNameConcatenatedTotalPhase, uidTot,  'write', 'formatted' )
    write(uidTot,'(2(a,5x))') "#  Energy","Total phase-shift"
    !
    !
    allocate( Eigenphases(MaxNChannels) )
    Eigenphases = 0.d0
    !
    do j = 1, size(FilesVec)
       !
       call OpenFile( trim(adjustl(FilesVec(j))), uidSingle, 'read', 'formatted' )
       !
       read(uidSingle,*) Energy, NChannels, (Eigenphases(i),i=1,NChannels)
       !
       close( uidSingle )
       !
       write(uidTot,"(f32.16)",ADVANCE="NO") Energy
       write(uidTot,"(f32.16)",ADVANCE="YES") sum(Eigenphases(1:NChannels))
       write(uid,"(f32.16)",ADVANCE="NO") Energy
       write(uid,"(i0)",ADVANCE="NO") NChannels
       !
       do k = 1, NChannels
          if ( k == NChannels ) then
             write(uid,"(f32.16)",ADVANCE="YES") Eigenphases(k) 
          else
             write(uid,"(f32.16)",ADVANCE="NO") Eigenphases(k) 
          end if
       end do
       !
    end do
    !
    close( uid )
    close( uidTot )
    !
  end subroutine BuildEigenphasesFile



  subroutine PlotScatteringStates( &
       SymStorageDir       , &
       KetSymSpace         , &
       Basis               , &
       CompMethod          , &
       Emin, Emax          , &
       ScattConditionNumber, &
       ConditionLabel      )
    !
    character(len=*)                    , intent(in)    :: SymStorageDir
    class(ClassSymmetricElectronicSpace), intent(in)    :: KetSymSpace
    class(ClassBasis)                   , intent(in)    :: Basis
    character(len=*)                    , intent(in)    :: CompMethod
    real(kind(1d0))                     , intent(in)    :: Emin
    real(kind(1d0))                     , intent(in)    :: Emax
    real(kind(1d0))                     , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable       , intent(inout) :: ConditionLabel
    !
    character(len=:), allocatable :: KetStatesDir, PlotScattStatesDir, KetStatesFileName
    character(len=256), allocatable :: ScatteringFilesVec(:) 
    integer :: NumKet, i
    type(ClassComplexSpectralResolution) :: KetSpecRes
    type(ClassComplexMatrix) :: CompKetStates
    !

    allocate( KetStatesDir, source = GetBlockStorageDir(KetSymSpace,KetSymSpace) )
    allocate( PlotScattStatesDir, source = GetPlotScattStateDirName(SymStorageDir,CompMethod) )
    !
    call GetIncomingScatteringFilesVec( &
         KetStatesDir            , &
         CompMethod              , &
         Emin, Emax        , &
         ScattConditionNumber    , &  
         ConditionLabel          , &
         ScatteringFilesVec   )
    !
    NumKet = size(ScatteringFilesVec)
    !
    do i = 1, NumKet
       allocate( KetStatesFileName, source = trim(adjustl(ScatteringFilesVec(i))) )
       call KetSpecRes%Read( KetStatesFileName )
       deallocate( KetStatesFileName )
       call KetSpecRes%Fetch( CompKetStates )
       call KetSpecRes%Free()
       write(*,*) CompKetStates%NRows(), CompKetStates%NColumns()


    end do





    !
  end subroutine PlotScatteringStates




end program ComputeMultichannelScatteringStates
