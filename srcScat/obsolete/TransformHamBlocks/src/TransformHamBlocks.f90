!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!

program TransformHamBlocks

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleGeneralInputFile

  implicit none

  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: StateSymLabel
  character(len=:), allocatable :: BraIonLabel
  character(len=:), allocatable :: KetIonLabel
  logical                       :: AnalyzeEvcts

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber

  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace
  type(ClassSESSESBlock)        :: TransMat
  type(ClassMatrix)             :: HamMat
  type(ClassSESSESBlock)        :: HamBlock
  type(ClassMatrix) :: HamTrans
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)

  real(kind(1d0)), allocatable  :: vEval(:)
  real(kind(1d0)), allocatable  :: vfact(:)
  integer                       :: nLinDepBra
  integer                       :: nLinDepKet
  type(ClassMatrix)             :: BraEvec
  type(ClassMatrix)             :: KetEvec

  type(ClassSpectralResolution) :: OvSpecResBra
  type(ClassSpectralResolution) :: OvSpecResKet
  type(ClassSpectralResolution) :: HamSpecRes


  character(len=:), allocatable :: SymStorageDir
  character(len=1024) :: sBuffer

  !*** MAYBE INCONSISTENT: THERE SEEM TO BE NO ROLE FOR CONDITIONLABEL. CHECK
  character(len=:), allocatable :: ConditionLabel
  integer         :: i, BraIonIndex, KetIonIndex
  real(kind(1d0)) :: MaxAllowedEigVal
  logical         :: MatIsSymmetric

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       StateSymLabel          ,   &
       BraIonLabel            ,   &
       KetIonLabel            ,   &
       AnalyzeEvcts           )

  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      )

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectronicSpace( Space, StateSymLabel, SymSpace )

  if ( SET_FORMATTED_WRITE ) call HamBlock%SetFormattedWrite()
  call HamBlock%SetBoxOnly()

  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  !.. diagonalization of the source + polycentric augemented states

  BraIonIndex = SymSpace%GetPIIndex( BraIonLabel )
  KetIonIndex = SymSpace%GetPIIndex( KetIonLabel )
  write(*,"(a,i0)") "BraIonIndex: ",BraIonIndex
  write(*,"(a,i0)") "KetIonIndex: ",KetIonIndex
  call HamBlock%LoadMonoIonPoly( SymSpace, SymSpace, HamiltonianLabel, BraIonIndex, KetIonIndex )

  call HamBlock%AssembleChCh( HamMat, BraIonIndex, KetIonIndex )

  sBuffer = HamBlock%GetStorageDir()
  allocate( SymStorageDir, source = trim( adjustl( sBuffer ) ) )

  MatIsSymmetric = HamMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'H is symmetric: ', MatIsSymmetric


  !.. Compute tranformation matrix for the bra
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//BraIonLabel
  call OvSpecResBra%Read( sBuffer, FORM_IO )
  call OvSpecResBra%Fetch( vEval )
  nLinDepBra=0
  do i=1,size(vEval)
     if(vEval(i) >= ConditionNumber)exit
     nLinDepBra = i
  enddo
  if(minval(vEval(nLinDepBra+1:))<ConditionNumber) call Assert("Eigenvalues are not ordered")
  allocate(vfact(size(vEval)-nLinDepBra))
  vfact=1.d0/sqrt(vEval(nLinDepBra+1:))
  call OvSpecResBra%Fetch( BraEvec )
  call BraEvec%RemoveColumns( nLinDepBra, "START" )
  call BraEvec%ScaleColumns( vfact )
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//"LinInd_"//BraIonLabel
  call BraEvec%Write( sbuffer, FORM_IO )
  call HamMat%Multiply( BraEvec, "Left" , "T" )
  call OvSpecResBra%Free()
  call BraEvec%Free()
  deallocate(vfact)

  !.. Compute tranformation matrix for the ket
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//KetIonLabel
  call OvSpecResKet%Read( sBuffer, FORM_IO )
  call OvSpecResKet%Fetch( vEval )
  nLinDepKet=0
  do i=1,size(vEval)
     if(vEval(i) >= ConditionNumber)exit
     nLinDepKet = i
  enddo
  if(minval(vEval(nLinDepKet+1:))<ConditionNumber) call Assert("Eigenvalues are not ordered")
  allocate(vfact(size(vEval)-nLinDepKet))
  vfact=1.d0/sqrt(vEval(nLinDepKet+1:))
  call OvSpecResKet%Fetch( KetEvec )
  call KetEvec%RemoveColumns( nLinDepKet, "START" )
  call KetEvec%ScaleColumns( vfact )
  call HamMat%Multiply( KetEvec, "Right", "N" )
  call OvSpecResKet%Free()
  call KetEvec%Free()

  sBuffer = GetOperatorFileName(SymStorageDir,HamiltonianLabel,"_transformed")//"_"//BraIonLabel//"_"//KetIonLabel
  call HamMat%Write(sBuffer, FORM_IO )

  if( BraIonIndex == KetIonIndex )then
     call HamMat%Diagonalize( HamSpecRes )
     sBuffer = GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel)//"_"//BraIonLabel
     call HamSpecRes%WriteEigenvalues( sBuffer, 1 )
  endif

contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       StateSymLabel             , &
       BraIonLabel               , &
       KetIonLabel               , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: StateSymLabel
    character(len=:), allocatable, intent(inout) :: BraIonLabel
    character(len=:), allocatable, intent(inout) :: KetIonLabel
    logical,                       intent(out)   :: AnalyzeEvcts
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"   , "Program Input File",  " ", "required" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile", "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-sym"   , "Name of the Irreducible Representation",  " ", "required" )
    call List%Add( "-braion"   , "Name of the BraIon (polycentric part, if not specified)",  " ", "optional" )
    call List%Add( "-ketion"   , "Name of the KetIon (polycentric part, if not specified)",  " ", "optional" )
    call List%Add( "-AnalyzeEvcts"  , &
         "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-gif"   , sBuffer  )
    allocate( GeneralInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-pif"   , sBuffer  )
    allocate( ProgramInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-mult"  , Multiplicity )
    !
    call List%Get( "-ccfile", sBuffer  )
    allocate( CloseCouplingConfigFile, source = trim( sBuffer ) )
    !
    call List%Get( "-esdir" , sBuffer   )
    allocate( NameDirQCResults, source = trim( sBuffer ) )
    !
    AnalyzeEvcts = List%Present( "-AnalyzeEvcts" ) 
    !
    call List%Get( "-sym", sBuffer )
    allocate( StateSymLabel, source = trim(sBuffer) )
    !
    if(List%Present("-braion"))then
       call List%Get( "-braion", sBuffer )
       allocate( BraIonLabel, source = trim(sBuffer) )
    else
       allocate( BraIonLabel, source = "Polycentric" )
    endif
    if(List%Present("-ketion"))then
       call List%Get( "-ketion", sBuffer )
       allocate( KetIonLabel, source = trim(sBuffer) )
    else
       allocate( KetIonLabel, source = "Polycentric" )
    endif
    !
    call List%Free()
    !
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "Read run time parameters :"
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "SAE General Input File  : "//GeneralInputFile
    write(OUTPUT_UNIT,"(2a)"  ) "Program Input File      : ", ProgramInputFile
    write(OUTPUT_UNIT,"(a,i0)") "Multiplicity            : ", Multiplicity
    write(OUTPUT_UNIT,"(2a)"  ) "Close Coupling File     : ", CloseCouplingConfigFile
    write(OUTPUT_UNIT,"(a)"   ) "Nuclear Config Dir Name : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)"   ) "Symmetry                : "//StateSymLabel
    write(OUTPUT_UNIT,"(a)"   ) "BraBlock                : "//BraIonLabel
    write(OUTPUT_UNIT,"(a)"   ) "KetBlock                : "//KetIonLabel
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"     , ConditionNumber  )
    !
    call CheckThreshold( ConditionNumber )
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
    !
  end subroutine ParseProgramInputFile



end program TransformHamBlocks
