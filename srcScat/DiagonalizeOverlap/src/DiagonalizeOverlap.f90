!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!
program DiagonalizeOverlap

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleHDF5
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleGeneralInputFile

  implicit none

  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"

  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: StateSymLabel
  character(len=:), allocatable :: IonLabel
  logical                       :: UseConditionedBlocks

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir

  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace
  type(ClassSESSESBlock)        :: OverlapMat
  type(ClassSESSESBlock)        :: QCOverlapMat
  type(ClassConditionerBlock)   :: Conditioner
  character(len=:), allocatable :: ConditionLabel
  type(ClassMatrix)             :: OvMat
  type(ClassSpectralResolution) :: OvSpecRes
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  character(len=:), allocatable :: SymStorageDir
  character(len=1024) :: sBuffer

  integer         :: i, IonIndex
  real(kind(1d0)) :: MaxAllowedEigVal
  logical         :: MatIsSymmetric

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       StateSymLabel          ,   &
       UseConditionedBlocks   ,   &
       IonLabel               )

  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectronicSpace( Space, StateSymLabel, SymSpace )
  print*, UseConditionedBlocks
  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel)
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          .FALSE. )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
  endif

  if ( SET_FORMATTED_WRITE) call OverlapMat%SetFormattedWrite()
  call OverlapMat%SetBoxOnly()

  write(output_unit,"(a)") 'Loading overlap ...'
  !
  !.. diagonalization of the source + polycentric augemented states

  IonIndex = SymSpace%GetPIIndex( IonLabel )
  write(*,"(a,i0)") "IonIndex: ",IonIndex

!  if(IonIndex == 0)then
!     call OverlapMat%LoadPoly   ( SymSpace, SymSpace, OverlapLabel )
!  else
!     call OverlapMat%LoadMonoIon( SymSpace, SymSpace, OverlapLabel, IonIndex, IonIndex )
!  endif
  if (UseConditionedBlocks) then
    call OverlapMat%LoadMonoIonPolyCondition( SymSpace, SymSpace, OverlapLabel, IonIndex, IonIndex, Conditioner )
  else
    call OverlapMat%LoadMonoIonPoly( SymSpace, SymSpace, OverlapLabel, IonIndex, IonIndex )
  endif
  call OverlapMat%AssembleChCh( OvMat, IonIndex, IonIndex )

  sBuffer = OverlapMat%GetStorageDir()
  call OverlapMat%Free()

  allocate( SymStorageDir, source = trim( adjustl( sBuffer ) ) )
  sBuffer = GetColumnLabelsFileName( SymStorageDir, OverlapLabel )//"_"//IonLabel 
  call OvMat%writeColumnLabels( trim(sBuffer) )
  !
  MatIsSymmetric = OvMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'S is symmetric: ', MatIsSymmetric

  write(output_unit,"(a)") 'Diagonalizing overlap ...'
  call OvMat%Diagonalize( OvSpecRes )
  call OvMat%Free()
  !*** ADD ORDER EIGENVALUES
  !
  sBuffer = GetEigenvaluesFileName(SymStorageDir,OverlapLabel )//"_"//IonLabel
  call OvSpecRes%WriteEigenvalues( sBuffer, 1 )
  !
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//IonLabel
  call OvSpecRes%Write( sBuffer, FORM_IO )
  call OvSpecRes%Free()

contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       StateSymLabel             , &
       UseConditionedBlocks      , &
       IonLabel                  )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    logical,                       intent(out)   :: UseConditionedBlocks
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: StateSymLabel
    character(len=:), allocatable, intent(inout) :: IonLabel
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"   , "Program Input File",  " ", "required" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-ccfile", "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-sym"   , "Name of the Irreducible Representation",  " ", "required" )
    call List%Add( "-ion"   , "Name of the Ion (polycentric part, if not specified)",  " ", "optional" )
    !
    call List%Parse()
    UseConditionedBlocks = List%Present( "-cond" )
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-gif"   , sBuffer  )
    allocate( GeneralInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-pif"   , sBuffer  )
    allocate( ProgramInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-mult"  , Multiplicity )
    !
    call List%Get( "-ccfile", sBuffer  )
    allocate( CloseCouplingConfigFile, source = trim( sBuffer ) )
    !
    call List%Get( "-esdir" , sBuffer   )
    allocate( NameDirQCResults, source = trim( sBuffer ) )
    !
    call List%Get( "-sym", sBuffer )
    allocate( StateSymLabel, source = trim(sBuffer) )
    !
    if(List%Present("-ion"))then
       call List%Get( "-ion", sBuffer )
       allocate( IonLabel, source = trim(sBuffer) )
    else
       allocate( IonLabel, source = "Polycentric" )
    endif
    !
    call List%Free()
    !
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "Read run time parameters :"
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "SAE General Input File  : "//GeneralInputFile
    write(OUTPUT_UNIT,"(2a)"  ) "Program Input File      : ", ProgramInputFile
    write(OUTPUT_UNIT,"(a,i0)") "Multiplicity            : ", Multiplicity
    write(OUTPUT_UNIT,"(2a)"  ) "Close Coupling File     : ", CloseCouplingConfigFile
    write(OUTPUT_UNIT,"(a)"   ) "Nuclear Config Dir Name : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)"   ) "Symmetry                : "//StateSymLabel
    write(OUTPUT_UNIT,"(a)"   ) "Block                   : "//IonLabel
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , sBuffer   )
    allocate( ConditionBsMethod, source = trim(adjustl(sBuffer)) )
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    !
  end subroutine ParseProgramInputFile



end program DiagonalizeOverlap
