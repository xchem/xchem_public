!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!

program TransformMatBlocks

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleElectronicSpace
  use ModuleGeneralInputFile
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleHDF5
  use ModuleBasis
  use ModuleBSpline
  use ModuleGaussianBSpline
  use ModuleSymmetricElectronicSpaceSpectralMethods

  implicit none

  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: StateSymLabel
  character(len=:), allocatable :: BraIonLabel
  character(len=:), allocatable :: KetIonLabel
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: DipoleAxis
  logical                       :: UseConditionedBlocks
  logical                       :: AnalyzeEvcts

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber

  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace
  type(ClassSESSESBlock)        :: TransMat
  type(ClassMatrix)             :: OpMat
  type(ClassMatrix)             :: LastBsMat
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassMatrix)             :: LastBsBoxDerMat
  type(ClassSESSESBlock)        :: OpBlock
  type(ClassSESSESBlock)        :: MatLastBsBlock
  type(ClassBSpline)            :: BSplineBasis
  type(ClassBasis)              :: GABSBasis
  type(ClassMatrix) :: OpTrans
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm

  real(kind(1d0)), allocatable  :: vEval(:)
  real(kind(1d0)), allocatable  :: vfact(:)
  integer                       :: nLinDepBra
  integer                       :: nLinDepKet
  type(ClassMatrix)             :: BraEvec
  type(ClassMatrix)             :: KetEvec

  type(ClassSpectralResolution) :: OvSpecResBra
  type(ClassSpectralResolution) :: OvSpecResKet
  type(ClassSpectralResolution) :: OpSpecRes
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  integer, allocatable          :: BsRemovedInf(:)
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: BSplineBasisFile
  character(len=:), allocatable :: SAEBasisFile
  character(len=:), allocatable :: SAEstoredir
  character(len=1024) :: sBuffer

  !*** MAYBE INCONSISTENT: THERE SEEM TO BE NO ROLE FOR CONDITIONLABEL. CHECK
  character(len=:), allocatable :: ConditionLabel
  integer         :: i, k, BraIonIndex, KetIonIndex, NumPWC, uid
  real(kind(1d0)) :: MaxAllowedEigVal, LastBsBoxDer, LastBsVal, LastBsDer
  logical         :: MatIsSymmetric

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       StateSymLabel          ,   &
       UseConditionedBlocks   ,   &
       BraIonLabel            ,   &
       KetIonLabel            ,   &
       OperatorLabel          ,   &
       ConditionNumber        ,   &
       DipoleAxis             ,   &
       AnalyzeEvcts           )

  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       BSplineBasisFile     , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
  
  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 


  !.. Initialises the GABS basis
  !..
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call GABSBasis%Init( SAEBasisFile )

  !..The same symmetries are allowed for the overlap and the 
  !  different matrices, so this subroutine can be called only once.
  call GetSymmetricElectronicSpace( Space, StateSymLabel, SymSpace )
  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          .FALSE. )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
  endif

  if ( SET_FORMATTED_WRITE ) call OpBlock%SetFormattedWrite()
  call OpBlock%SetBoxOnly()

  if ( OperatorLabel .is. CAPLabel ) then
    deallocate( OperatorLabel )
    allocate ( OperatorLabel, source = GetFullCAPLabel(1) )
  end if

  write(output_unit,"(a)") 'Loading Matrix ...'
  !
  !.. diagonalization of the source + polycentric augemented states

  BraIonIndex = SymSpace%GetPIIndex( BraIonLabel )
  KetIonIndex = SymSpace%GetPIIndex( KetIonLabel )
  write(*,"(a,i0)") "BraIonIndex: ",BraIonIndex
  write(*,"(a,i0)") "KetIonIndex: ",KetIonIndex
  if (UseConditionedBlocks) then
    call OpBlock%LoadMonoIonPolyCondition( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, Conditioner, BsRemovedInf = BsRemovedInf,Axis=DipoleAxis )
  else
    call OpBlock%LoadMonoIonPoly( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, DipoleAxis )
  endif
  call OpBlock%AssembleChCh( OpMat, BraIonIndex, KetIonIndex )

  sBuffer = OpBlock%GetStorageDir()
  allocate( SymStorageDir, source = trim( adjustl( sBuffer ) ) )

!  MatIsSymmetric = OpMat%IsSymmetric(1.d-10)
!  write(output_unit,*) 'Matrix is symmetric: ', MatIsSymmetric



  !.. Compute tranformation matrix for the bra
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//BraIonLabel
  call OvSpecResBra%Read( sBuffer, FORM_IO )
  call OvSpecResBra%Fetch( vEval )
  nLinDepBra=0
  do i=1,size(vEval)
     if(vEval(i)/vEVal(size(vEval)) >=ConditionNumber)exit
     nLinDepBra = i
  enddo
  if(minval(vEval(nLinDepBra+1:))<ConditionNumber) call Assert("Eigenvalues are not ordered")
  allocate(vfact(size(vEval)-nLinDepBra))
  vfact=1.d0/sqrt(vEval(nLinDepBra+1:))
  call OvSpecResBra%Fetch( BraEvec )
  call BraEvec%RemoveColumns( nLinDepBra, "START" )
  call BraEvec%ScaleColumns( vfact )
  if ( (OperatorLabel .eq. HamiltonianLabel) .and. &
       (BraIonIndex   .ne. 0               ) .and. & 
       (KetIonIndex   .ne. 0               )) then
! Computing and writting the S and the H of the LastBs in the new LInd basis   
    if ( SET_FORMATTED_WRITE ) call MatLastBsBlock%SetFormattedWrite()
!!We are considering that the last bspline is not modified by the conditioning
!procedure. Must be always true.
    if (UseConditionedBlocks) then
       call MatLastBsBlock%GetMonoIonPolyLastBs( SymSpace, SymSpace, OverlapLabel, BraIonIndex, KetIonIndex, 'KET', LastBsMat, Conditioner = Conditioner )
    else
       call MatLastBsBlock%GetMonoIonPolyLastBs( SymSpace, SymSpace, OverlapLabel, BraIonIndex, KetIonIndex, 'KET', LastBsMat )
    endif
    call LastBsMat%Multiply( BraEvec, "Left" , "T" )
    sBuffer = GetOperatorFileName(SymStorageDir,OverlapLabel,"_LastBs")//"_"//BraIonLabel//"_"//KetIonLabel
    call LastBsMat%Write( sbuffer, FORM_IO )
    if (UseConditionedBlocks) then
       call MatLastBsBlock%GetMonoIonPolyLastBs( SymSpace, SymSpace, HamiltonianLabel, BraIonIndex, KetIonIndex, 'KET', LastBsMat, Conditioner = Conditioner )
    else
       call MatLastBsBlock%GetMonoIonPolyLastBs( SymSpace, SymSpace, HamiltonianLabel, BraIonIndex, KetIonIndex, 'KET', LastBsMat )
    endif
    call LastBsMat%Multiply( BraEvec, "Left" , "T" )
    sBuffer = GetOperatorFileName(SymStorageDir,HamiltonianLabel,"_LastBs")//"_"//BraIonLabel//"_"//KetIonLabel
    call LastBsMat%Write( sbuffer, FORM_IO )
  endif
! Computing and writting the derivative at the end of the box for the linear
! independent basis.

  if ( ( BraIonIndex > 0 )                       .and.  &
       ( BraIonIndex .eq. KetIonIndex )          .and.  &
       ( OperatorLabel .eq. HamiltonianLabel ) ) then
    call BSplineBasis%Init( BSplineBasisFile )
    LastBsBoxDer = BSplineBasis%Eval( GABSBasis%GetRMax(), BSplineBasis%GetNBSplines() - 1 ,1 )

    LastBsDer = BSplineBasis%Eval( GABSBasis%GetRMax(), BSplineBasis%GetNBSplines() ,1 )
    LastBsVal = BSplineBasis%Eval( GABSBasis%GetRMax(), BSplineBasis%GetNBSplines() )
    NumPWC = SymSpace%GetNumPWC( BraIonIndex )
    call LastBsBoxDerMat%InitFull(NumPWC,BraEvec%NColumns())
    k = 0
    if (.not. UseConditionedBlocks) allocate(BsRemovedInf(NumPWC))
    if (.not. UseConditionedBlocks) BsRemovedInf=0
    do i = NumPWC,1,-1
      LastBsBoxDerMat%A(i,:) = BraEvec%A(BraEvec%NRows()-k,:)*LastBsBoxDer
      k = k + BSplineBasis%GetNBSplines()-GABSBasis%GetNBSplineSkip()-1-BsRemovedInf(i)
    enddo
    sBuffer = GetOperatorFileName(SymStorageDir,OverlapLabel,"_LinInd_DerRMax")//"_"//BraIonLabel//"_"//KetIonLabel
    call LastBsBoxDerMat%Write( sbuffer, FORM_IO )
    sBuffer = GetOperatorFileName(SymStorageDir,OverlapLabel,"_LastBsDer")
    open (Newunit = uid, file = sBuffer, status = 'replace' )
    write(uid,*) LastBsDer, GABSBasis%GetRMax()
    close(uid)
    sBuffer = GetOperatorFileName(SymStorageDir,OverlapLabel,"_LastBsVal")
    open (Newunit = uid, file = sBuffer, status = 'replace' )
    write(uid,*) LastBsVal, GABSBasis%GetRMax()
    close(uid)
    sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//"LinInd_"//BraIonLabel
    call BraEvec%Write( sbuffer, FORM_IO )
  endif

  call OpMat%Multiply( BraEvec, "Left" , "T" )
  call OvSpecResBra%Free()
  call BraEvec%Free()
  deallocate(vfact)

  !.. Compute tranformation matrix for the ket
  sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//KetIonLabel
  call OvSpecResKet%Read( sBuffer, FORM_IO )
  call OvSpecResKet%Fetch( vEval )
  nLinDepKet=0
  do i=1,size(vEval)
     if(vEval(i)/vEVal(size(vEval)) >= ConditionNumber)exit
     nLinDepKet = i
  enddo
  if(minval(vEval(nLinDepKet+1:))<ConditionNumber) call Assert("Eigenvalues are not ordered")
  allocate(vfact(size(vEval)-nLinDepKet))
  vfact=1.d0/sqrt(vEval(nLinDepKet+1:))
  call OvSpecResKet%Fetch( KetEvec )
  call KetEvec%RemoveColumns( nLinDepKet, "START" )
  call KetEvec%ScaleColumns( vfact )
  call OpMat%Multiply( KetEvec, "Right", "N" )
  call OvSpecResKet%Free()
  call KetEvec%Free()
  if (allocated(DipoleAxis)) then
    write(sBuffer, '(A)') '_'//trim(DipoleAxis)
    deallocate(DipoleAxis)
    allocate(DipoleAxis, Source = trim(sBuffer))
  else
    allocate(DipoleAxis, source = '' )
  endif
  sBuffer = GetOperatorFileName(SymStorageDir,OperatorLabel,DipoleAxis//"_transformed")//"_"//BraIonLabel//"_"//KetIonLabel
  call OpMat%Write(sBuffer, FORM_IO )

  if( BraIonIndex == KetIonIndex .and. OperatorLabel .eq. HamiltonianLabel )then
     call OpMat%Diagonalize( OpSpecRes )
     sBuffer = GetEigenvaluesFileName(SymStorageDir,OperatorLabel)//"_"//BraIonLabel
     call OpSpecRes%WriteEigenvalues( sBuffer, 1 )
  endif

contains


  !> Reads the run time parameters from the command line.

  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       StateSymLabel             , &
       UseConditionedBlocks      , &
       BraIonLabel               , &
       KetIonLabel               , &
       OperatorLabel             , &
       conditionNumber           , &
       DipoleAxis                , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    real*8                       , intent(out)   :: conditionNumber
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: StateSymLabel
    logical,                       intent(out)   :: UseConditionedBlocks
    character(len=:), allocatable, intent(inout) :: BraIonLabel
    character(len=:), allocatable, intent(inout) :: KetIonLabel
    character(len=:), allocatable, intent(out)   :: OperatorLabel
    character(len=:), allocatable, intent(out)   :: DipoleAxis
    character(len=:), allocatable                :: Gauge
    character(len=:), allocatable                :: AuxOp
    logical,                       intent(out)   :: AnalyzeEvcts
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"   , "Program Input File",  " ", "required" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile", "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-sym"   , "Name of the Irreducible Representation",  " ", "required" )
    call List%Add( "-braion"   , "Name of the BraIon (polycentric part, if not specified)",  " ", "optional" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-ketion"   , "Name of the KetIon (polycentric part, if not specified)",  " ", "optional" )
    call List%Add( "-cn"     , "Condition number used in the transfornation", 1.d0          , "required" )
    call List%Add( "-op"     , "Name of the Operator",  " ", "required" )
    call List%Add( "-gau"    , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-axis"   , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-AnalyzeEvcts"  , &
         "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    UseConditionedBlocks = List%Present( "-cond" )
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-gif"   , sBuffer  )
    allocate( GeneralInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-op"     , sbuffer           )
    !
    allocate( OperatorLabel          , source = trim( sbuffer )     )
    !
    call CheckOperator( OperatorLabel )
    !
    if(List%Present("-gau"))then
       call List%Get( "-gau", sbuffer )
       allocate( Gauge, source = trim(sbuffer) )
       call CheckGauge( Gauge )
    end if
    !
    if(List%Present("-axis"))then
       call List%Get( "-axis", sbuffer )
       allocate( DipoleAxis, source = trim(sbuffer) )
       call CheckAxis( DipoleAxis )
    end if
    !
    call List%Get( "-pif"   , sBuffer  )
    allocate( ProgramInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-mult"  , Multiplicity )
    !
    call List%Get( "-ccfile", sBuffer  )
    allocate( CloseCouplingConfigFile, source = trim( sBuffer ) )
    !
    call List%Get( "-esdir" , sBuffer   )
    allocate( NameDirQCResults, source = trim( sBuffer ) )
    !
    AnalyzeEvcts = List%Present( "-AnalyzeEvcts" ) 
    !
    call List%Get( "-cn"     , ConditionNumber  )
    !
    call CheckThreshold( ConditionNumber )
    !
    call List%Get( "-sym", sBuffer )
    allocate( StateSymLabel, source = trim(sBuffer) )
    !
    if(List%Present("-braion"))then
       call List%Get( "-braion", sBuffer )
       allocate( BraIonLabel, source = trim(sBuffer) )
    else
       allocate( BraIonLabel, source = "Polycentric" )
    endif
    if(List%Present("-ketion"))then
       call List%Get( "-ketion", sBuffer )
       allocate( KetIonLabel, source = trim(sBuffer) )
    else
       allocate( KetIonLabel, source = "Polycentric" )
    endif
    !
    call List%Free()
    !
    !..Check dipole
    if ( OperatorLabel .is. DipoleLabel ) then
       if ( .not.allocated(Gauge) ) call Assert( 'For '//DipoleLabel//' operator the gauge must be present.' )
       if ( .not.allocated(DipoleAxis) ) call Assert( 'For '//DipoleLabel//' operator the orientation must be present.' )
       allocate( AuxOp, source = OperatorLabel//Gauge )
       deallocate( OperatorLabel )
       allocate( OperatorLabel, source = AuxOp )
       deallocate( AuxOp )
    end if

    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "Read run time parameters :"
    write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
    write(OUTPUT_UNIT,"(a)"   ) "SAE General Input File  : "//GeneralInputFile
    write(OUTPUT_UNIT,"(2a)"  ) "Program Input File      : ", ProgramInputFile
    write(OUTPUT_UNIT,"(a,i0)") "Multiplicity            : ", Multiplicity
    write(OUTPUT_UNIT,"(2a)"  ) "Close Coupling File     : ", CloseCouplingConfigFile
    write(OUTPUT_UNIT,"(a)"   ) "Nuclear Config Dir Name : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)"   ) "Symmetry                : "//StateSymLabel
    write(OUTPUT_UNIT,"(a)"   ) "BraBlock                : "//BraIonLabel
    write(OUTPUT_UNIT,"(a)"   ) "KetBlock                : "//KetIonLabel
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       BSplineBasisFile           , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    character(len=:), allocatable, intent(out) :: BSplineBasisFile
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: sBuffer, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "."    , "optional" )
    call List%Add( "BSplineBasisFile"    , "BSplineBasis", "optional" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"                , sBuffer   )
    allocate( StorageDir      , source = trim(adjustl(sBuffer)) )
    !
    call List%Get( "BSplineBasisFile"          , sBuffer   )
    allocate( BSplineBasisFile, source = trim(adjustl(sBuffer)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , sBuffer   )
    allocate( ConditionBsMethod, source = trim(adjustl(sBuffer)) )
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    !
  end subroutine ParseProgramInputFile



end program TransformMatBlocks
