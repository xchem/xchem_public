!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> Computes the partial widths: \f$\Gamma_{\alpha}\f$, 
!!(\f$\Gamma=\Sum_{\alpha}\Gamma_{\alpha}), associated to a 
!!resonance coupled with different scattering channels:
!!\f$\Psi_{\alpha,E}^{-}\f$.
program ComputePartialWidths
 
 
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleErrorHandling
  use ModuleGeneralInputFile
  use ModuleBasis
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleIO


  implicit none
  

  !> Real tolerance for some consistency checks.
  real(kind(1d0)), parameter :: DEFINED_TOLERANCE = 1.d-6
  !
  !> If .true./.false. the close-coupling blocks will be read or
  !! write with/without format.
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  !
  !> This method evaluates the scattering S matrix for the
  !! complex energies corresponding to the resonances, and
  !! extracts the partial widths from the matrix components.
  character(len=*), parameter :: ComplexEnergyScatteringMatrixLabel = "SMAT"
  !> This method consists in computing the transition from the 
  !! Quenched Hamiltonian states describing the resonances,
  !! whose complex eigenenergies has to be close to those passed 
  !! in a file, to the different scattering channels. The area
  !! under the Loretzian profile corresponds to the branching ratio.
  character(len=*), parameter :: QuenchedHamStatesTransitionLabel   = 'QUENCHED'
  !> This method consists in computing the eigenphases around
  !! the resonance, and from it the partial widths can be extracted
  !! using the Macek equation:
  !!\f$2(E-E_{r}))\Sigma_{m=1}^{N}\Gamma_{m}\cot{\delta_{m}^{0}-\delta_{n}(E)}, n=1, ..., N\f$.
  !! Then a fitting is needed to extract the background 
  !! contribution of the eigenphases: \f$\delta_{m}^{0}\f$.
  character(len=*), parameter :: EigenPhaseLabel   = 'EIGENPHASE'
  !> This method is based in the Feshbach partitioning of the
  !! wavefunctions space into a P and Q subspaces mutually
  !! orthogonal: PQ = QP = 0 (P+Q=1). The Q subspace comprehend
  !! the resonances meanwhile the P subspace the non-resonant
  !! continuum.
  character(len=*), parameter :: FeshbachMethodLabel   = 'FESHBACH'
  !> Name of the directory in which the partial widths results
  !! will be stored.
  character(len=*), parameter :: PartialWidthsDirLabel   = 'PartialWidths'

  !.. Run time parameters
  !> [GeneralInputFile](@ref GeneralInputFile)  
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  real(kind(1d0))               :: Emin = -huge(1d0)
  real(kind(1d0))               :: Emax = huge(1d0)
  !> Method used to compute the scattering states.
  character(len=:), allocatable :: CompMethod
  !> Method used to compute the partial widths.
  character(len=:), allocatable :: CompPartialWidthsMethod 
  !> If present at run time, transforms the scattering states into a different
  !! symmetry representation, given a set of coefficients in this
  !! file. I.e.: to pass from D2h channels to spherically
  !! symmetric channels in atoms.
  character(len=:), allocatable :: TransformScattStatesFile
  !> File containing a set of complex energies to compute the 
  !!scattering states, instead of using those computed in a
  !!real energy domain.
  character(len=:), allocatable :: ComplexEnergiesFile
  !> First Quenched Hamiltonian to be considered (1 for first)
  integer :: MinIndexQH = 1
  !> Last Quenched Hamiltonian to be considered (1 for first)
  integer :: MaxIndexQH = 1

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber
  real(kind(1d0))               :: ScattConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  !.. Rest of variables.
  !> Radial basis class ([ModuleBasis](@ref ModuleBasis.f90)).  
  type(ClassBasis)              :: Basis
  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: OverlapMat, HamiltonianMat
  type(ClassMatrix)             :: OvMat, HamMat, QCFullConditioner, Overlap, Hamiltonian 
  type(ClassComplexMatrix)      :: CompHamMat, NewCompHamMat
  type(ClassScatteringStatesVec) :: ScattStatesVec
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  !
  complex(kind(1d0)), allocatable  :: EnergyVec(:)
  real(kind(1d0))   , allocatable  :: RealEnergyVec(:)
  !
  character(len=:), allocatable :: DipoleAxis
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: ConditionLabel
  character(len=:), allocatable :: SAEstoredir
  character(len=:), allocatable :: SAEBasisFile
  !
  !> Number of rows of the full quantum chemistry operator matrices.
  integer  :: NumFunQCBra
  !> Number of columns of the full quantum chemistry operator matrices.
  integer  :: NumFunQCKet
  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: NumPWC
  !> Minimum number of B-splines among all channels that do not 
  !! overlap with the diffuse Gaussian orbitals.
  integer :: NumBsNotOverlap
  integer :: NumEnergies
  !> ClassComplexmatrix containing the transformation matrix
  !! from the old symmetry representation tp the new one.
  type(ClassComplexMatrix) :: TransfMat
  !> Vector with the names of the computed scattering states
  !! in the range [Emin,Emax].
  character(len=256), allocatable :: ScatteringFilesVec(:)
  !> Vector with the names of the computed non-resonant
  !! scattering states in the range [Emin,Emax].
  character(len=256), allocatable :: ScatteringFeshbachFilesVec(:)
  !> File name of the scattering state.
  character(len=:), allocatable :: ScattStatesFileName
  !> File name of the non-resonant scattering state.
  character(len=:), allocatable :: ScattStatesFeshbachFileName
  integer :: i, j, k, n
  !> ClassComplexSpectralResolution containing the scattering states.
  type(ClassComplexSpectralResolution) :: ScattSpecRes
  !> Energy vector whose dimension coincides with the number 
  !! of open channels, storing in all the entries the same energy,
  !! that at which the scattering states were computed. Although
  !! real, for convenience was stored in complex format.
  complex(kind(1d0)), allocatable :: ScattEnergyVec(:)
  !> Energy vector whose dimension coincides with the number 
  !! of open channels, storing in all the entries the same energy,
  !! that at which the non-resonant scattering states were computed. Although
  !! real, for convenience was stored in complex format.
  complex(kind(1d0)), allocatable :: ScattFeshbachEnergyVec(:)
  !> ClassComplexMatrix storing the scattering states computed 
  !! for a certain energy.
  type(ClassComplexMatrix) :: CompScattStates
  !> ClassComplexMatrix storing the non-resonant scattering states computed 
  !! for a certain energy.
  type(ClassComplexMatrix) :: CompFeshbachScattStates
  !> File name of the Quenched Hamiltonian spectrum.
  character(len=:), allocatable :: QHFileName
  !> ClassComplexSpectralResolution containing the Quanched
  !! Hamiltonian spectrum in the box.
  type(ClassComplexSpectralResolution) :: QHSpecRes
  !> Spectrum of the Hamiltonian coming from the
  !! Feshbach partition.
  type(ClassSpectralResolution) :: FeshbachHSpecRes
  !> ClassComplexMatrix storing the Quenched Hamiltonian 
  !! right eigenvectors corresponding to the resonances
  !! of interest.
  type(ClassComplexMatrix) :: ResonanceRightEigVec
  !> ClassMatrix storing the Feshbach Hamiltonian 
  !! eigenvectors corresponding to the resonances
  !! of interest.
  type(ClassMatrix) :: ResonanceEigVec
  !> ClassComplexMatrix storing the Quenched Hamiltonian 
  !! right eigenvectors corresponding to the resonances
  !! of interest, redimension to be compatible with the 
  !! scattering states, zero entries need to be added for
  !! the B-splines that do not vanish at the end of the box.
  type(ClassComplexMatrix) :: NewResonanceRightEigVec
  !> Quanched Hamiltonian eigenvalues corresponding to the
  !! resonances of interest.
  complex(kind(1d0)), allocatable :: ResonanceEigValues(:)
  !> Feshbach Hamiltonian eigenvalues corresponding to the
  !! resonances of interest.
  real(kind(1d0)), allocatable :: RealResonanceEigValues(:)
  !> Full path of the partial widths results directory.
  character(len=:), allocatable :: PartialWidthsDir
  !> Vector of file names corresponding to each resonance.
  character(len=256), allocatable :: PartialWidthsFilenameVec(:)
  !> Vector of file names corresponding to each resonance.
  character(len=256), allocatable :: PartialWidthsFreeContFilenameVec(:)
  !> Vector of file names corresponding to each resonance.
  character(len=256), allocatable :: DirectPartialWidthsFilenameVec(:)
  !> Vector of unit numbers associated with files.
  integer, allocatable :: UidVec(:)
  !> Vector of unit numbers associated with files.
  integer, allocatable :: UidVecFreeCont(:)
  !> Vector of unit numbers associated with files.
  integer, allocatable :: UidVecPWidth(:)
  !
  type(ClassComplexMatrix) :: FreeResCompScattStates, ResStateMat, AuxCompScattStates, SMat, InvDeltaE
  real(kind(1d0)) :: Val
  type(ClassMatrix) :: EigenPhases
  type(ClassComplexMatrix) :: EigenVec
  logical :: FirstEnergy
  integer :: NOpenChannels, uid
  character(len=:), allocatable :: EigenPhasesFileName
  !> File name of the Hamiltonian spectrum with the
  !! Feshbach partition.
  character(len=:), allocatable :: FeshbachHamFileName
  


  call GetRunTimeParameters(    &
       GeneralInputFile       , &
       ProgramInputFile       , &
       Multiplicity           , &
       CloseCouplingConfigFile, &
       NameDirQCResults       , &    
       BraSymLabel            , &
       KetSymLabel            , &
       Emin                   , &
       Emax                   , &
       CompMethod             , &
       CompPartialWidthsMethod, &
       UseConditionedBlocks   , &
       UsePerfectProjection   , &
       TransformScattStatesFile, &
       ComplexEnergiesFile     , &
       MinIndexQH              , &
       MaxIndexQH              )
  
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(2a)" ) "SAE General Input File : ", GeneralInputFile
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File : ", ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ", CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  if ( Emin > -huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,f12.6)" )"Minimum Energy :        ", Emin
  end if
  if ( Emax < huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,f12.6)" )"Maximum Energy :        ", Emax
  end if
  write(OUTPUT_UNIT,"(2a)" ) "Computational Method for the scattering states :  ", CompMethod
  write(OUTPUT_UNIT,"(2a)" ) "Computational Method for the partial widths :  ", CompPartialWidthsMethod
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 
  if ( allocated(TransformScattStatesFile) ) then
     write(OUTPUT_UNIT,"(a,a)" )"Transform scattering states : ",   TransformScattStatesFile
  end if
  if ( allocated(ComplexEnergiesFile) ) then
     write(OUTPUT_UNIT,"(a,a)" )"File containing the complex energies : ",   ComplexEnergiesFile
  end if
  write(OUTPUT_UNIT,"(a,i0)" )"Minimum Qhenched Hamiltonian index :", MinIndexQH
  write(OUTPUT_UNIT,"(a,i0)" )"Maximum Qhenched Hamiltonian index :", MaxIndexQH


  
  call ReadTransformationMatrix( TransformScattStatesFile, TransfMat )

  !.. Reads the resonance energies.
  if ( CompPartialWidthsMethod .is. QuenchedHamStatesTransitionLabel ) then
     call ReadComplexEnergies( ComplexEnergiesFile, EnergyVec )
  elseif ( CompPartialWidthsMethod .is. FeshbachMethodLabel ) then
     call ReadRealEnergies( ComplexEnergiesFile, RealEnergyVec )
  end if


  call ParseProgramInputFile( &
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      , &
       ScattConditionNumber , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D25.16)" ) "Condition number for diagonalization :      ", ConditionNumber
  write(OUTPUT_UNIT,"(a,D25.16)" ) "Condition number for scattering :      ", ScattConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D25.16)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod



  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 

  !.. Initialize the basis
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call Basis%Init( SAEBasisFile )
  call Basis%SetRoot( SAEstoredir )


  
  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
  
  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OverlapLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )
  
  allocate( SymStorageDir, source = GetBlockStorageDir(BraSymSpace,KetSymSpace) )

  allocate( PartialWidthsDir, source = &
       addslash(SymStorageDir)//&
       addslash(PartialWidthsDirLabel) )
  !
  call execute_command_line("mkdir -p "//PartialWidthsDir )  
  !


  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          UsePerfectProjection )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
  end if




  if ( CompPartialWidthsMethod .is. ComplexEnergyScatteringMatrixLabel ) then
     
     call Assert( 'The "ComplexEnergyScatteringMatrix" method has not been implemented yet.' )

     !.. Overlap Block
     if ( SET_FORMATTED_WRITE) call OverlapMat%SetFormattedWrite()
     !
     write(output_unit,"(a)") 'Loading overlap ...'
     !
     if ( UseConditionedBlocks ) then
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis, ConditionLabel )
     else
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, DipoleAxis )
     end if
     !
     !
     call OverlapMat%Assemble( LoadLocStates, OvMat )
     call OverlapMat%GetNumFunQC( LoadLocStates, NumFunQCBra, NumFunQCKet )
     NumBsNotOverlap = OverlapMat%GetMinNumNotOvBs()
     if ( NumFunQCBra /= NumFunQCKet ) Call Assert( &
          'The overlap matrix must be squared.' )
     !
     call OverlapMat%Free()

     !
  !.. Hamiltonian Block
  if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
  !
  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  if ( UseConditionedBlocks ) then
     call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, ConditionLabel )
  else
     call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
  end if
  !
  call HamiltonianMat%Assemble( LoadLocStates, HamMat )
  write(output_unit,"(a,2i0)") 'Original dimension of H', HamMat%NRows(), HamMat%NColumns()
  call HamiltonianMat%Free()
  
     
     !
     write(output_unit,"(a,2i0)") 'Original dimension of S', OvMat%NRows(), OvMat%NColumns()
     write(output_unit,"(a,2i0)") 'Original dimension of H', HamMat%NRows(), HamMat%NColumns()
     
     
     !.. For S and H operators the Bra and Ket symmetric spaces are the same.
     NumPWC = BraSymSpace%GetNumPWC()
     !
     !.. Condition the QC part plus the B-splines overlapping with
     ! it, to apply the HSLE and INVM methods.
     if ( CONDITION_QC ) then
        if ( (CompMethod .is. HSLEMethodLabel) .or. &
             (CompMethod .is. InvMatMethodLabel) ) then
           write(output_unit,"(a)") 'Conditioning QC part ...'
           call ComputeQCConditioner( OvMat, OvMat%NRows()-NumPWC, ScattConditionNumber, QCFullConditioner )
!!$        call ComputeQCConditioner( OvMat, OvMat%NRows()-NumPWC*NumBsNotOverlap, ScattConditionNumber, QCFullConditioner )
!!$        call ComputeQCConditioner( OvMat, NumFunQCBra, ConditionNumber, QCFullConditioner )
           call ApplyQCConditioner( OvMat , QCFullConditioner )
           call ApplyQCConditioner( HamMat, QCFullConditioner )
           write(output_unit,"(a,2i0)") 'Dimension of S after QC conditioning', OvMat%NRows(), OvMat%NColumns()
           write(output_unit,"(a,2i0)") 'Dimension of H after QC conditioning', HamMat%NRows(), HamMat%NColumns()
        end if
     end if
     
     
     !.. Remove the last B-splines from the rows
     call OvMat%GetSubMatrix(        &
          1, OvMat%NRows() - NumPWC, &
          1, OvMat%NColumns()      , &
          Overlap           )
     call HamMat%GetSubMatrix(        &
          1, HamMat%NRows() - NumPWC, &
          1, HamMat%NColumns()      , &
          Hamiltonian        )
     
     write(output_unit,"(a,2i0)") 'Dimension of S for scattering', Overlap%NRows(), Overlap%NColumns()
     write(output_unit,"(a,2i0)") 'Dimension of H for scattering', Hamiltonian%NRows(), Hamiltonian%NColumns()

     
     
     call ScattStatesVec%Init( &
          Basis, BraSymSpace, SymStorageDir, NumBsNotOverlap, &
          CompMethod, dble(EnergyVec), ScattConditionNumber, ConditionLabel )
     !
     if ( CONDITION_QC ) call ScattStatesVec%SetConditionerQC( QCFullConditioner )
     call QCFullConditioner%Free()
     !
     call ScattStatesVec%ComputeScatteringStates( Overlap, Hamiltonian, .false. )
     !
     call Overlap%Free()
     call Hamiltonian%Free()
     deallocate( EnergyVec )
     

     !.. OvMat and HamMat available  


     
  elseif ( CompPartialWidthsMethod .is. QuenchedHamStatesTransitionLabel ) then
     !
     !
     call GetIncomingScatteringFilesVec( &
          SymStorageDir            , &
          CompMethod              , &
          Emin, Emax        , &
          ScattConditionNumber    , &  
          ConditionLabel          , &
          ScatteringFilesVec   )
     !
     !
     do n = MinIndexQH, MaxIndexQH
        !
        !
        allocate( QHFileName, source = GetSpectrumFileName(SymStorageDir,GetFullQHLabel(n),ConditionLabel) )
        call QHSpecRes%Read( QHFileName )
        call ExtractResonances( &
             EnergyVec        , &
             QHSpecRes        , & 
             ResonanceEigValues, &
             ResonanceRightEigVec )
        !
        call RenormalizeRightEigVec( ResonanceRightEigVec )
!!$        call RenormalizeRightEigVecAsHermitian( ResonanceRightEigVec )
        !
        !
        call CreatePartialWidthsFilenameVec( &
             ResonanceEigValues            , &
             PartialWidthsDir              , &
             CompPartialWidthsMethod       , &
             n                             , &
             PartialWidthsFilenameVec      )
        call GetUnitNumberVectorForWritting( PartialWidthsFilenameVec, UidVec )
        call WriteStringUnitNumberVector( &
             UidVec, &
             '#  Energy  '//&
             '|<\Psi_{\alpha,E}^{-1}|\Phi_{res}>|^2  '//&
             '|<\Psi_{\beta,E}^{-1}|\Phi_{res}>|^2 ...' )
        !
        !
        call CreatePartialWidthsFreeContFilenameVec( &
             ResonanceEigValues                    , &
             PartialWidthsDir                      , &
             CompPartialWidthsMethod               , &
             n                                     , &
             PartialWidthsFreeContFilenameVec      )
        call GetUnitNumberVectorForWritting( PartialWidthsFreeContFilenameVec, UidVecFreeCont )
        call WriteStringUnitNumberVector( &
             UidVecFreeCont, &
             '#  Energy  '//&
             '|<\Psi_{\alpha,E}^{-1}|\Phi_{res}>|^2  '//&
             '|<\Psi_{\beta,E}^{-1}|\Phi_{res}>|^2 ...' )
        !
        !
        call CreateDirectPartialWidthsFilenameVec( &
             ResonanceEigValues                  , &
             PartialWidthsDir                    , &
             CompPartialWidthsMethod             , &
             n                                   , &
             DirectPartialWidthsFilenameVec      )
        call GetUnitNumberVectorForWritting( DirectPartialWidthsFilenameVec, UidVecPWidth )
        call WriteStringUnitNumberVector( &
             UidVecPWidth, &
             '#  Energy  '//&
             '\Gamma_{\alpha}  '//&
             '\Gamma_{\beta} ...' )
        !
        !
        !
        NumEnergies = size( ScatteringFilesVec )    
        !
        do i = 1, NumEnergies
           !
           allocate( ScattStatesFileName, source = trim(adjustl(ScatteringFilesVec(i))) )
           call ScattSpecRes%Read( ScattStatesFileName )
           deallocate( ScattStatesFileName )
           !
           call TransformScatteringStates( ScattSpecRes,TransfMat )
           !
           call ScattSpecRes%Fetch( ScattEnergyVec )
           call ScattSpecRes%Fetch( CompScattStates )
           call ScattSpecRes%Free()
           !
           call RedimensionResonanceStates( &
                ResonanceRightEigVec      , &
                CompScattStates           , &
                NewResonanceRightEigVec   )
           !
!!$        !***
!!$        do j = NewResonanceRightEigVec%NRows()-3000, NewResonanceRightEigVec%NRows()
!!$           call NewResonanceRightEigVec%SetElement(j,1,Z0)
!!$        end do
!!$        !***
           !
           !
           FreeResCompScattStates = CompScattStates
           call CompScattStates%TransposeConjugate()
           !
           call CompScattStates%Multiply( NewResonanceRightEigVec, 'Right', 'N' )
           !
           call NewResonanceRightEigVec%Transpose( ResStateMat )
           call ResStateMat%Multiply( FreeResCompScattStates, 'Right', 'N' )
           
           
           
!!$        call ResStateMat%Multiply( CompScattStates, 'Right', 'N' )
!!$        call ResStateMat%Multiply( NewResonanceRightEigVec, 'Left', 'N' )
!!$        call ResStateMat%Multiply( -Z1 )
!!$        !.. Scattering states without the resonant contribution.
!!$        call FreeResCompScattStates%Add( ResStateMat )
!!$        call ResStateMat%Free()
!!$        call FreeResCompScattStates%TransposeConjugate()
!!$        !
!!$        CompHamMat = HamMat
!!$        call CompHamMat%Multiply( FreeResCompScattStates, 'Left', 'N' )
!!$        call CompHamMat%Multiply( NewResonanceRightEigVec, 'Right', 'N' )
!!$        !
!!$        call FreeResCompScattStates%Multiply( NewResonanceRightEigVec, 'Right', 'N' )
           !
           !
           call NewResonanceRightEigVec%Free()
           !
           do j = 1, size(UidVec)
              !..All the energies in ScattEnergyVec are the same.
              write(UidVec(j),'(f25.15)',advance='NO') dble(ScattEnergyVec(1))
              write(UidVecFreeCont(j),'(f25.16)',advance='NO') dble(ScattEnergyVec(1))
              write(UidVecPWidth(j),'(f25.16)',advance='NO') dble(ScattEnergyVec(1))
              do k = 1, CompScattStates%NRows()
                 if ( k == CompScattStates%NRows() ) then 
                    write(UidVec(j),'(f25.16)',advance='YES') abs(CompScattStates%Element(k,j))**2!abs(ResStateMat%Element(j,k)*CompScattStates%Element(k,j))
                    write(UidVecFreeCont(j),'(f25.16)',advance='YES') abs(FreeResCompScattStates%Element(k,j))**2
                    write(UidVecPWidth(j),'(f25.16)',advance='YES') 2.d0*PI*abs(CompHamMat%Element(k,j))**2
                 else
                    write(UidVec(j),'(f25.16)',advance='NO') abs(CompScattStates%Element(k,j))**2!abs(ResStateMat%Element(j,k)*CompScattStates%Element(k,j))
                    write(UidVecFreeCont(j),'(f25.16)',advance='NO') abs(FreeResCompScattStates%Element(k,j))**2
                    write(UidVecPWidth(j),'(f25.16)',advance='NO') 2.d0*PI*abs(CompHamMat%Element(k,j))**2
                 end if
              end do
           end do
           !
           call CompScattStates%Free()
           call FreeResCompScattStates%Free()
           call ResStateMat%Free()
           call NewResonanceRightEigVec%Free()
           !
        end do
        !
        deallocate( QHFileName )
        call QHSpecRes%Free()
        deallocate( ResonanceEigValues )
        call ResonanceRightEigVec%Free()
        deallocate( PartialWidthsFilenameVec )
        deallocate( PartialWidthsFreeContFilenameVec )
        deallocate( DirectPartialWidthsFilenameVec )
        !
        call CloseUnitNumberVector( UidVec )
        deallocate( UidVec )
        call CloseUnitNumberVector( UidVecFreeCont )
        deallocate( UidVecFreeCont )
        call CloseUnitNumberVector( UidVecPWidth )
        deallocate( UidVecPWidth )
        !
        !
     end do
     !
     !
  elseif ( CompPartialWidthsMethod .is. EigenPhaseLabel ) then
     !
     !
     call GetIncomingScatteringFilesVec( &
          SymStorageDir            , &
          CompMethod              , &
          Emin, Emax        , &
          ScattConditionNumber    , &  
          ConditionLabel          , &
          ScatteringFilesVec   )
     !
     NumEnergies = size( ScatteringFilesVec )    
     !
     allocate( EigenPhasesFileName, source = &
          AddSlash(PartialWidthsDir)//&
          RootEigenphasesLabel//&
          GetEnergyRangeLabel(Emin,Emax) )
     !
     call OpenFile( EigenPhasesFileName, uid, 'write', 'formatted' )
     write(uid,'(a)') '# Energy, Number of Channels, eigenphase_{1}, eigenphase_{2}, ...'
     !
     do i = 1, NumEnergies
        !
        allocate( ScattStatesFileName, source = trim(adjustl(ScatteringFilesVec(i))) )
        call ScattSpecRes%Read( ScattStatesFileName )
        deallocate( ScattStatesFileName )
        !
        call TransformScatteringStates( ScattSpecRes,TransfMat )
        !
        call ScattSpecRes%Fetch( ScattEnergyVec )
        call ScattSpecRes%Fetch( CompScattStates )
        call ScattSpecRes%Free()
        !
        call CompScattStates%TransposeConjugate( AuxCompScattStates )
        call AuxCompScattStates%Transpose( SMat )
        !
        call CompScattStates%Multiply( AuxCompScattStates, 'Left', 'N' )
        call CompScattStates%Inverse( InvDeltaE )
        call CompScattStates%Free()
        !
        call SMat%Multiply( AuxCompScattStates, 'Left', 'N' )
        call AuxCompScattStates%Free()
        call SMat%Multiply( InvDeltaE, 'Left', 'N' )
        call InvDeltaE%Free()
        if ( .not.SMat%IsUnitary( DEFINED_TOLERANCE ) ) then
           write(output_unit,*) 'E = ', dble(ScattEnergyVec(1))
           call ErrorMessage( 'The S scattering matrix is not unitary.' )
        end if
        !
        if ( i == 1 ) then
           FirstEnergy = .true.
        else
           FirstEnergy = .false.
        end if
        !.. Compute eigenphases.
        call ComputeEigenPhases( SMat, EigenPhases, EigenVec, FirstEnergy )
        !
        NOpenChannels = EigenPhases%NRows()
        write(uid,"(f32.16)",ADVANCE="NO") dble(ScattEnergyVec(1))
        write(uid,"(i0)",ADVANCE="NO") NOpenChannels
        do j = 1, NOpenChannels
           if ( j == NOpenChannels ) then
              write(uid,"(f32.16)",ADVANCE="YES") EigenPhases%Element(j,1) 
           else
              write(uid,"(f32.16)",ADVANCE="NO") EigenPhases%Element(j,1)  
           end if
        end do
        !
        call SMat%Free()
        deallocate( ScattEnergyVec )
        !
     end do
     !
     close( uid )
     deallocate( EigenPhasesFileName )
     !
     !
  elseif ( CompPartialWidthsMethod .is. FeshbachMethodLabel ) then
     !
     !
     call GetIncomingScatteringFilesVec( &
          SymStorageDir            , &
          CompMethod              , &
          Emin, Emax        , &
          ScattConditionNumber    , &  
          ConditionLabel          , &
          ScatteringFilesVec   )
     !
     call GetIncomingScatteringFeshbachFilesVec( &
          SymStorageDir            , &
          CompMethod              , &
          Emin, Emax        , &
          ScattConditionNumber    , &  
          ConditionLabel          , &
          ScatteringFeshbachFilesVec   )
     !
     !
     !.. Hamiltonian Block
     if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
     !
     write(output_unit,"(a)") 'Loading Hamiltonian ...'
     !
     if ( UseConditionedBlocks ) then
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, ConditionLabel )
     else
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     end if
     !
     call HamiltonianMat%Assemble( LoadLocStates, HamMat )
     write(output_unit,"(a,2i0)") 'Original dimension of H', HamMat%NRows(), HamMat%NColumns()
     call HamiltonianMat%Free()
     !
     write(output_unit,"(a,2i0)") 'Original dimension of H', HamMat%NRows(), HamMat%NColumns()
     !
     CompHamMat = HamMat
     call HamMat%Free()
     !
     !
     allocate( FeshbachHamFileName, source = GetSpectrumFileName(SymStorageDir,GetFullFeshbachHLabel(),ConditionLabel) )
     call FeshbachHSpecRes%Read( FeshbachHamFileName )
     call ExtractRealResonances(  &
          RealEnergyVec         , &
          FeshbachHSpecRes      , & 
          RealResonanceEigValues, &
          ResonanceEigVec       )
     !
     !..For hermitian matrices right and left are eigenvectors are
     ! complex conjugate of the other. Here the name is just to use
     ! an already defined variable.
     ResonanceRightEigVec = ResonanceEigVec
     !
     call CreatePartialWidthsFilenameVecReal( &
          RealResonanceEigValues            , &
          PartialWidthsDir                  , &
          CompPartialWidthsMethod           , &
          PartialWidthsFilenameVec          )
     call GetUnitNumberVectorForWritting( PartialWidthsFilenameVec, UidVec )
     call WriteStringUnitNumberVector( &
          UidVec, &
          '#  Energy  '//&
          '|<\Psi_{\alpha,E}^{-1}|\Phi_{res}>|^2  '//&
          '|<\Psi_{\beta,E}^{-1}|\Phi_{res}>|^2 ...' )
     !
     !
     call CreateDirectPartialWidthsFilenameVecReal( &
          RealResonanceEigValues                  , &
          PartialWidthsDir                        , &
          CompPartialWidthsMethod                 , &
          DirectPartialWidthsFilenameVec          )
     call GetUnitNumberVectorForWritting( DirectPartialWidthsFilenameVec, UidVecPWidth )
     call WriteStringUnitNumberVector( &
          UidVecPWidth, &
          '#  Energy  '//&
          '\Gamma_{\alpha}  '//&
          '\Gamma_{\beta} ...' )
     !
     !
     NumEnergies = size( ScatteringFilesVec )    
     !
     do i = 1, NumEnergies
        !
        !.. Scattering states with the resonant component.
        allocate( ScattStatesFileName, source = trim(adjustl(ScatteringFilesVec(i))) )
        call ScattSpecRes%Read( ScattStatesFileName )
        deallocate( ScattStatesFileName )
        !
        call TransformScatteringStates( ScattSpecRes,TransfMat )
        !
        call ScattSpecRes%Fetch( ScattEnergyVec )
        call ScattSpecRes%Fetch( CompScattStates )
        call ScattSpecRes%Free()
        !
        !.. Scattering states without the resonant component.
        allocate( ScattStatesFeshbachFileName, source = trim(adjustl(ScatteringFeshbachFilesVec(i))) )
        call ScattSpecRes%Read( ScattStatesFeshbachFileName )
        deallocate( ScattStatesFeshbachFileName )
        !
        call TransformScatteringStates( ScattSpecRes,TransfMat )
        !
        call ScattSpecRes%Fetch( ScattFeshbachEnergyVec )
        call ScattSpecRes%Fetch( CompFeshbachScattStates )
        call ScattSpecRes%Free()
        !
        !
        if ( i == 1 ) then
           !
           call RedimensionResonanceStates( &
                ResonanceRightEigVec      , &
                CompScattStates           , &
                NewResonanceRightEigVec   )
           call ResonanceRightEigVec%Free()
           !
           call RedimensionResonanceStates( &
                CompHamMat                , &
                CompScattStates           , &
                NewCompHamMat             )
           call CompHamMat%Free()
           !
        end if
        !
        !
        call CompScattStates%TransposeConjugate()
        call CompScattStates%Multiply( NewResonanceRightEigVec, 'Right', 'N' )
        !
        call CompFeshbachScattStates%TransposeConjugate()
        call CompFeshbachScattStates%Multiply( NewCompHamMat, 'Right', 'N' )
        call CompFeshbachScattStates%Multiply( NewResonanceRightEigVec, 'Right', 'N' )
        
        !
        !
        do j = 1, size(UidVec)
           !..All the energies in ScattEnergyVec are the same.
           write(UidVec(j),'(f25.16)',advance='NO') dble(ScattEnergyVec(1))
           write(UidVecPWidth(j),'(f25.16)',advance='NO') dble(ScattEnergyVec(1))
           do k = 1, CompScattStates%NRows()
              if ( k == CompScattStates%NRows() ) then 
                 write(UidVec(j),'(f25.16)',advance='YES') abs(CompScattStates%Element(k,j))**2
                 write(UidVecPWidth(j),'(f25.16)',advance='YES') 2.d0*PI*abs(CompFeshbachScattStates%Element(k,j))**2
              else
                 write(UidVec(j),'(f25.16)',advance='NO') abs(CompScattStates%Element(k,j))**2
                 write(UidVecPWidth(j),'(f25.16)',advance='NO') 2.d0*PI*abs(CompFeshbachScattStates%Element(k,j))**2
              end if
           end do
        end do
        !
        call CompScattStates%Free()
        call CompFeshbachScattStates%Free()
        !
        !
     end do
     !
     call NewResonanceRightEigVec%Free()
     !
     !
  else
     !
     call Assert( 'Neither of the implemented methods have been selected.' )
     !
  end if




contains

  
  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &    
       BraSymLabel               , &
       KetSymLabel               , &
       Emin                      , &
       Emax                      , &
       CompMethod                , &
       CompPartialWidthsMethod   , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       TransformScattStatesFile  , &
       ComplexEnergiesFile       , &
       MinIndexQH                , &
       MaxIndexQH                )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    !> [GeneralInputFile](@ref GeneralInputFile)  
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer                      , intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    real(kind(1d0))              , intent(inout) :: Emin
    real(kind(1d0))              , intent(inout) :: Emax
    character(len=:), allocatable, intent(out)   :: CompMethod
    character(len=:), allocatable, intent(out)   :: CompPartialWidthsMethod
    logical                      , intent(out)   :: UseConditionedBlocks
    logical                      , intent(out)   :: UsePerfectProjection
    !> If present at run time, transforms the scattering states into a different
    !! symmetry representation, given a set of coefficients in this
    !! file. I.e.: to pass from D2h channels to spherically
    !! symmetric channels in atoms.
    character(len=:), allocatable, intent(inout) :: TransformScattStatesFile
    !> File containing a set of complex energies to compute the 
    !!scattering states, instead of using those computed in a
    !!real energy domain.
    character(len=:), allocatable, intent(inout) :: ComplexEnergiesFile
    integer                      , intent(inout) :: MinIndexQH
    integer                      , intent(inout) :: MaxIndexQH
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: pif, brasym, ketsym, ccfile, qcdir, cm, gif, transfscattfile, complexenergfile, cmpw
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Computes the "//&
         "partial widths associated to the decay of the  "//&
         "resonances to the different scattering channels."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-Emin"   , "Minimum Energy"    , 1.d0, "optional" )
    call List%Add( "-Emax"   , "Maximum Energy"    , 1.d0, "optional" )
    call List%Add( "-cm"     , "Computational Method for the scattering states",  " ", "required" )
    call List%Add( "-cmpw"   , "Computational Method for the partial widths, either "//&
         EigenPhaseLabel//", "//&
         ComplexEnergyScatteringMatrixLabel//" or "//&
         QuenchedHamStatesTransitionLabel,  " ", "required" )
    call List%Add( "-cond"   , "Use the conditioned blocks" )
    call List%Add( "-pp"     , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-transfscattfile"  ,&
      "Name of the File with the coefficients to transform the scattering states",  " ", "optional" )
    call List%Add( "-complexenergfile"  , "Name of the File with the complex energies",  " ", "required" )
    call List%Add( "-firstQH"   , "First Quenched Hamiltonian to be fetched", 1, "optional" )
    call List%Add( "-lastQH"   , "Last Quenched Hamiltonian to be fetched", 1, "optional" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif"    , gif )
    call List%Get( "-pif"    , pif  )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-esdir"  , qcdir  )
    call List%Get( "-complexenergfile", complexenergfile )
    !
    if ( List%Present("-lastQH") ) then
       call List%Get( "-lastQH"   , MaxIndexQH )
    end if
    !
    if ( List%Present("-firstQH") ) then
       call List%Get( "-firstQH"   , MinIndexQH )
    end if
    !
    if ( List%Present("-Emin") ) then
       call List%Get( "-Emin"   , Emin )
    end if
    !
    if ( List%Present("-Emax") ) then
       call List%Get( "-Emax"   , Emax )
    end if
    !
    call List%Get( "-cm"     , cm   )
    call List%Get( "-cmpw"   , cmpw )
    !
    if ( List%Present("-transfscattfile") ) then
       call List%Get( "-transfscattfile", transfscattfile )
       allocate( TransformScattStatesFile, source = trim( transfscattfile ) )
    end if
    !
    UseConditionedBlocks   = List%Present( "-cond" )
    UsePerfectProjection   = List%Present( "-pp" )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    !
    !
    if ( .not.allocated(ComplexEnergiesFile) ) then
       if ( .not.List%Present("-Emin") ) call Assert( &
            'The minimum energy: option "-Emin", must be present.' )
       if ( .not.List%Present("-Emax") ) call Assert( &
            'The maximum energy: option "-Emax", must be present.' )
    end if
    !
    call List%Free()
    !
    allocate( GeneralInputFile       , source = trim( gif )    )
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    allocate( CompMethod             , source = trim(cm)       )
    allocate( CompPartialWidthsMethod, source = trim(cmpw)     )
    allocate( ComplexEnergiesFile, source = trim( complexenergfile ) )
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    call SetStringToUppercase( CompMethod )
    call CheckScatteringMethod( CompMethod )
    call CheckPartialWidthsMethod( CompPartialWidthsMethod )
    if ( MaxIndexQH < MinIndexQH ) call Assert( &
         'The last QH index must be >0 than the first QH index.' )
    !
  end subroutine GetRunTimeParameters




  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            , &
       ScattConditionNumber       , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    real(kind(1d0))              , intent(out) :: ScattConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    call List%Add( "ScattConditionNumber", 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"      , ConditionNumber  )
    call List%Get( "ScattConditionNumber" , ScattConditionNumber  )
    call List%Get( "LoadLocStates" , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"   , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"      , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold" , ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"    , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionNumber )
    call CheckThreshold( ScattConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    call CheckLastBsIsPresent( ConditionBsMethod, NumBsDropEnding )
    !
  end subroutine ParseProgramInputFile



  !>.. Reads the complex energies stored in the file.
  subroutine ReadComplexEnergies( FileName, CompEnVec )
    !> File containing the complex energies.
    character(len=*)               , intent(in)  :: FileName
    !> Vector with the complex energies.
    complex(kind(1d0)), allocatable, intent(out) :: CompEnVec(:)
    !
    integer :: uid, iostat, NumEn, i
    real(kind(1d0)) :: RealEn, ImagEn
    character(len=:), allocatable :: Line
    !
    call OpenFile( FileName, uid, 'read', 'formatted' )
    !
    call FetchLine( uid, Line )
    !.. The first uncommented line has the vector dimension
    read(Line,*) NumEn
    !
    allocate( CompEnVec(NumEn) )
    CompEnVec = Z0
    !
    do i = 1, NumEn
       read(uid,*) RealEn, ImagEn
       CompEnVec(i) = cmplx(RealEn,ImagEn,kind(1d0))
    end do
    !
    close( uid )
    !
  end subroutine ReadComplexEnergies


  !>.. Reads the real energies stored in the file.
  subroutine ReadRealEnergies( FileName, EnVec )
    !> File containing the complex energies.
    character(len=*)               , intent(in)  :: FileName
    !> Vector with the real energies.
    real(kind(1d0)), allocatable   , intent(out) :: EnVec(:)
    !
    integer :: uid, iostat, NumEn, i
    real(kind(1d0)) :: RealEn
    character(len=:), allocatable :: Line
    !
    call OpenFile( FileName, uid, 'read', 'formatted' )
    !
    call FetchLine( uid, Line )
    !.. The first uncommented line has the vector dimension
    read(Line,*) NumEn
    !
    allocate( EnVec(NumEn) )
    EnVec = 0.d0
    !
    do i = 1, NumEn
       read(uid,*) RealEn
       EnVec(i) = RealEn
    end do
    !
    close( uid )
    !
  end subroutine ReadRealEnergies


  !> Checks the validity of the name passed to the partial
  !! widths method.
  subroutine CheckPartialWidthsMethod( CompPartialWidthsMethod )
    !> Method used to compute the partial widths.
    character(len=*), intent(in) :: CompPartialWidthsMethod
    if ( (CompPartialWidthsMethod .isnt. ComplexEnergyScatteringMatrixLabel) .and. &
         (CompPartialWidthsMethod .isnt. QuenchedHamStatesTransitionLabel)   .and. & 
         (CompPartialWidthsMethod .isnt. FeshbachMethodLabel)   .and. & 
         (CompPartialWidthsMethod .isnt. EigenPhaseLabel) ) &
         call Assert( 'The method introduced for '//&
         'the determination of the partial widths '//&
         'is not valid, it has to be either '//&
         EigenPhaseLabel//', '//&
         ComplexEnergyScatteringMatrixLabel//' or '//&
         QuenchedHamStatesTransitionLabel//'.' )
    !
  end subroutine CheckPartialWidthsMethod


  !> Given a vector of complex energies, extracts from the spectral
  !! resolution of the Qenched Hamiltonian, the closest eigenvalues
  !! and the corresponding right eigenvectors.
  subroutine ExtractResonances( &
       EnergyVec        , &
       QHSpecRes        , & 
       ResonanceEigValues, &
       ResonanceRightEigVec )
    !
    !> Vector of complex energies used as a reference.
    complex(kind(1d0))                   , intent(in)    :: EnergyVec(:)
    !> Quanched Hamiltonian spectral resolution
    class(ClassComplexSpectralResolution), intent(inout) :: QHSpecRes
    !> Closest Quenched Hamiltonian eigenvalues to the reference
    !! energies.
    complex(kind(1d0)), allocatable      , intent(out)   :: ResonanceEigValues(:)
    !> ClassComplexMatrix storing the rigth eigenvectors 
    !! corresponding to the resonances energy.
    class(ClassComplexMatrix)            , intent(out)   :: ResonanceRightEigVec
    !
    type(ClassComplexMatrix) :: QHRightEigVec
    complex(kind(1d0)), allocatable :: QHEigValues(:)
    !..Gives the absolute indeces of the resonances energy in the 
    ! Quenched Hamiltonain eigenvalues vector.
    integer, allocatable :: ResonanceIndices(:)
    !
    call QHSpecRes%Fetch( QHEigValues )
    call QHSpecRes%Fetch( QHRightEigVec )
    call QHSpecRes%Free()
    !
    call FindClosestNumbers( &
         EnergyVec         , &
         QHEigValues       , &
         ResonanceEigValues, &
         ResonanceIndices  )
    !
    call QHRightEigVec%FetchMatrixByIndices( &
         ResonanceIndices     , &
         'columns'            , &
         ResonanceRightEigVec )
    !
  end subroutine ExtractResonances



  !> Given a vector of real energies, extracts from the spectral
  !! resolution of a Hamiltonian, the closest eigenvalues
  !! and the corresponding eigenvectors.
  subroutine ExtractRealResonances( &
       RealEnergyVec              , &
       HSpecRes                   , & 
       RealResonanceEigValues     , &
       ResonanceEigVec            )
    !
    !> Vector of real energies used as a reference.
    real(kind(1d0))               , intent(in)    :: RealEnergyVec(:)
    !> Hamiltonian spectral resolution
    class(ClassSpectralResolution), intent(inout) :: HSpecRes
    !> Closest Hamiltonian eigenvalues to the reference
    !! energies.
    real(kind(1d0)), allocatable  , intent(out)   :: RealResonanceEigValues(:)
    !> ClassMatrix storing the eigenvectors 
    !! corresponding to the resonances energy.
    class(ClassMatrix)            , intent(out)   :: ResonanceEigVec
    !
    type(ClassMatrix) :: HEigVec
    real(kind(1d0)), allocatable :: HEigValues(:)
    !..Gives the absolute indeces of the resonances energy in the 
    ! Quenched Hamiltonain eigenvalues vector.
    integer, allocatable :: ResonanceIndices(:)
    !
    call HSpecRes%Fetch( HEigValues )
    call HSpecRes%Fetch( HEigVec )
    call HSpecRes%Free()
    !
    call FindRealClosestNumbers(     &
         RealEnergyVec         , &
         HEigValues            , &
         RealResonanceEigValues, &
         ResonanceIndices  )
    !
    call HEigVec%FetchMatrixByIndices( &
         ResonanceIndices     , &
         'columns'            , &
         ResonanceEigVec )
    !
  end subroutine ExtractRealResonances



  !> Given a vector of complex numbers, finds the closest values
  !! in another vector, and gives also the indices it correspond to.
  subroutine FindClosestNumbers( &
       RefCompVec              , &
       TargetCompVec           , &
       ClosestCompVec          , &
       Indices                 )
    !> Vector of complex numbers used as a reference.
    complex(kind(1d0))             , intent(in)  :: RefCompVec(:)
    !> Vector of complex numbers from which the closest
    !! numbers will be extracted.
    complex(kind(1d0))             , intent(in)  :: TargetCompVec(:)
    !> Vector with the closest numbers to the reference ones.
    complex(kind(1d0)), allocatable, intent(out) :: ClosestCompVec(:)
    !> Vector with the indices of the closest numbers in the
    !! original vector.
    integer, allocatable           , intent(out) :: Indices(:)
    !
    integer :: NumVal, i, j
    real(kind(1d0)) :: ModDiff, NewModDiff
    real(kind(1d0)) :: NewRealDiff, NewImagDiff
    real(kind(1d0)) :: RealRefNum, ImagRefNum
    real(kind(1d0)) :: RealTargetNum, ImagTargetNum
    logical, parameter :: PrintInfo = .true.
    !
    NumVal = size( RefCompVec )
    allocate( ClosestCompVec(NumVal) )
    ClosestCompVec = Z0
    allocate( Indices(NumVal) )
    Indices = 0
    !
    !
    do i = 1, NumVal
       !
       RealRefNum = dble(RefCompVec(i))
       ImagRefNum = aimag(RefCompVec(i))
       ModDiff = huge(1d0)
       !
       do j = 1, size(TargetCompVec)
          !
          RealTargetNum = dble(TargetCompVec(j))
          ImagTargetNum = aimag(TargetCompVec(j))
          !
          NewRealDiff = abs(RealRefNum - RealTargetNum)
          NewImagDiff = abs(ImagRefNum - ImagTargetNum)
          NewModDiff = sqrt( NewRealDiff**2 + NewImagDiff**2 )
          !
          if ( NewModDiff < ModDiff) then
             ModDiff = NewModDiff
             ClosestCompVec(i) = TargetCompVec(j)
             Indices(i) = j
          end if
          !
       end do
       !
    end do
    !
    if ( PrintInfo ) then
       write(output_unit,'(a)') 'Closest values found: '
       write(output_unit,'(a)') 'Reference, Found, Index'
       do i = 1, NumVal
          write(output_unit,'(2f25.16,a,2f25.16,a,i0)') RefCompVec(i),',', ClosestCompVec(i),',', Indices(i)
       end do
    end if
    !
  end subroutine FindClosestNumbers



  !> Given a vector of real numbers, finds the closest values
  !! in another vector, and gives also the indices it correspond to.
  subroutine FindRealClosestNumbers( &
       RefCompVec              , &
       TargetCompVec           , &
       ClosestCompVec          , &
       Indices                 )
    !> Vector of real numbers used as a reference.
    real(kind(1d0))             , intent(in)  :: RefCompVec(:)
    !> Vector of real numbers from which the closest
    !! numbers will be extracted.
    real(kind(1d0))             , intent(in)  :: TargetCompVec(:)
    !> Vector with the closest numbers to the reference ones.
    real(kind(1d0)), allocatable, intent(out) :: ClosestCompVec(:)
    !> Vector with the indices of the closest numbers in the
    !! original vector.
    integer, allocatable        , intent(out) :: Indices(:)
    !
    integer :: NumVal, i, j
    real(kind(1d0)) :: ModDiff, NewModDiff
    real(kind(1d0)) :: NewRealDiff
    real(kind(1d0)) :: RealRefNum
    real(kind(1d0)) :: RealTargetNum
    logical, parameter :: PrintInfo = .true.
    !
    NumVal = size( RefCompVec )
    allocate( ClosestCompVec(NumVal) )
    ClosestCompVec = 0.d0
    allocate( Indices(NumVal) )
    Indices = 0
    !
    !
    do i = 1, NumVal
       !
       RealRefNum = RefCompVec(i)
       ModDiff = huge(1d0)
       !
       do j = 1, size(TargetCompVec)
          !
          RealTargetNum = TargetCompVec(j)
          !
          NewModDiff = abs(RealRefNum - RealTargetNum)
          !
          if ( NewModDiff < ModDiff) then
             ModDiff = NewModDiff
             ClosestCompVec(i) = TargetCompVec(j)
             Indices(i) = j
          end if
          !
       end do
       !
    end do
    !
    if ( PrintInfo ) then
       write(output_unit,'(a)') 'Closest values found: '
       write(output_unit,'(a)') 'Reference, Found, Index'
       do i = 1, NumVal
          write(output_unit,'(f25.16,a,f25.16,a,i0)') RefCompVec(i),',', ClosestCompVec(i),',', Indices(i)
       end do
    end if
    !
  end subroutine FindRealClosestNumbers

 

  !> Redimensions the resonance states to be compatible with the 
  !! scattering states, zero entries need to be added for
  !! the B-splines that do not vanish at the end of the box.
  subroutine RedimensionResonanceStates( &
       ResonanceRightEigVec            , &
       CompScattStates                 , &
       NewResonanceRightEigVec         )
    !> Original resonance state.
    class(ClassComplexMatrix), intent(in) :: ResonanceRightEigVec
    !> Scattering states.
    class(ClassComplexMatrix), intent(in) :: CompScattStates
    !> Redimension resonance state.
    class(ClassComplexMatrix), intent(out) :: NewResonanceRightEigVec
    !
    integer :: NewNumRows, NumRows, NumCols, i, j
    complex(kind(1d0)) :: Val
    !
    NewNumRows = CompScattStates%NRows()
    NumRows = ResonanceRightEigVec%NRows()
    NumCols = ResonanceRightEigVec%NColumns()
    !
    call NewResonanceRightEigVec%InitFull( NewNumRows, NumCols )
    !
    do j = 1, NumCols
       do i = 1, NumRows
          Val = ResonanceRightEigVec%Element(i,j)
          call NewResonanceRightEigVec%SetElement( i, j, Val )
       end do
    end do
    !
  end subroutine RedimensionResonanceStates



  !> Creates a vector of file names, each corresponding with
  !! a complex energy resonance.
  subroutine CreatePartialWidthsFilenameVec( &
       ResonanceEigValues                  , &
       PartialWidthsDir                    , &
       CompPartialWidthsMethod             , &
       Index                               , &
       PartialWidthsFilenameVec            )
    !> Resonances energy vector.
    complex(kind(1d0))             , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Computational method for the partial width calculation.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    integer                        , intent(in)  :: Index
    !> Vector of file names corresponding to each resonance.
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn, ImagStrn
    character(len=*), parameter :: FileLabel = 'ResContCoupling2'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       write(ImagStrn,*) aimag(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            FileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))//&
            ','//trim(adjustl(ImagStrn))//&
            '_'//'QH_'//AlphabeticNumber(Index)
       RealStrn = ' '
       ImagStrn = ' '
    end do
    !
  end subroutine CreatePartialWidthsFilenameVec



  !> Creates a vector of file names, each corresponding with
  !! a real energy resonance.
  subroutine CreatePartialWidthsFilenameVecReal( &
       ResonanceEigValues                      , &
       PartialWidthsDir                        , &
       CompPartialWidthsMethod                 , &
       PartialWidthsFilenameVec                )
    !> Resonances energy vector.
    real(kind(1d0))                , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Vector of file names corresponding to each resonance.
    !> Computational method for the partial width calculation.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn
    character(len=*), parameter :: FileLabel = 'ResContCoupling2'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            FileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))
       RealStrn = ' '
    end do
    !
  end subroutine CreatePartialWidthsFilenameVecReal



  !> Creates a vector of file names, each corresponding to a complex energy
  !! resonance coupled with a non-resonant continuum.
  subroutine CreatePartialWidthsFreeContFilenameVec( &
       ResonanceEigValues                  , &
       PartialWidthsDir                    , &
       CompPartialWidthsMethod             , &
       Index                               , &
       PartialWidthsFilenameVec            )
    !> Resonances energy vector.
    complex(kind(1d0))             , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Computational methods for coputing the partial widths.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    integer                        , intent(in)  :: Index
    !> Vector of file names corresponding to each resonance.
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn, ImagStrn
    character(len=*), parameter :: FreeContFileLabel = 'ResFreeContCoupling2'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       write(ImagStrn,*) aimag(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            FreeContFileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))//&
            ','//trim(adjustl(ImagStrn))//&
            '_'//'QH_'//AlphabeticNumber(Index)
       RealStrn = ' '
       ImagStrn = ' '
    end do
    !
  end subroutine CreatePartialWidthsFreeContFilenameVec



  !> Creates a vector of file names, each corresponding to a real energy
  !! resonance coupled with a non-resonant continuum.
  subroutine CreatePartialWidthsFreeContFilenameVecReal( &
       ResonanceEigValues                  , &
       PartialWidthsDir                    , &
       CompPartialWidthsMethod             , &
       PartialWidthsFilenameVec            )
    !> Resonances energy vector.
    real(kind(1d0))                , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Computational methods for coputing the partial widths.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    !> Vector of file names corresponding to each resonance.
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn
    character(len=*), parameter :: FreeContFileLabel = 'ResFreeContCoupling2'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            FreeContFileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))
       RealStrn = ' '
    end do
    !
  end subroutine CreatePartialWidthsFreeContFilenameVecReal



  !> Creates a vector of file names, each storing the partial
  !! width corresponding to de decay of a complex energy resonance
  !! to a scattering state with definite energy.
  subroutine CreateDirectPartialWidthsFilenameVec( &
       ResonanceEigValues                  , &
       PartialWidthsDir                    , &
       CompPartialWidthsMethod             , &
       Index                               , &
       PartialWidthsFilenameVec            )
    !> Resonances energy vector.
    complex(kind(1d0))             , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Computational method for the partial width calculation.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    integer                        , intent(in)  :: Index
    !> Vector of file names corresponding to each resonance.
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn, ImagStrn
    character(len=*), parameter :: DirectFileLabel = 'DirectPartialWidth'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       write(ImagStrn,*) aimag(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            DirectFileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))//&
            ','//trim(adjustl(ImagStrn))//&
            '_'//'QH_'//AlphabeticNumber(Index)
       RealStrn = ' '
       ImagStrn = ' '
    end do
    !
  end subroutine CreateDirectPartialWidthsFilenameVec



  !> Creates a vector of file names, each storing the partial
  !! width corresponding to de decay of a real energy resonance
  !! to a scattering state with definite energy.
  subroutine CreateDirectPartialWidthsFilenameVecReal( &
       ResonanceEigValues                  , &
       PartialWidthsDir                    , &
       CompPartialWidthsMethod             , &
       PartialWidthsFilenameVec            )
    !> Resonances energy vector.
    real(kind(1d0))                , intent(in)  :: ResonanceEigValues(:)
    !> Full path to the partial widths directory.
    character(len=*)               , intent(in)  :: PartialWidthsDir
    !> Computational method for the partial width calculation.
    character(len=*)               , intent(in)  :: CompPartialWidthsMethod
    !> Vector of file names corresponding to each resonance.
    character(len=256), allocatable, intent(out) :: PartialWidthsFilenameVec(:)
    !
    integer :: i, NumRes
    character(len=256) :: RealStrn
    character(len=*), parameter :: DirectFileLabel = 'DirectPartialWidth'
    !
    NumRes = size(ResonanceEigValues)
    allocate( PartialWidthsFilenameVec(NumRes) )
    !
    do i = 1, NumRes
       write(RealStrn,*) dble(ResonanceEigValues(i))
       PartialWidthsFilenameVec(i) = &
            addslash(PartialWidthsDir)//&
            DirectFileLabel//&
            '_'//CompPartialWidthsMethod//&
            '_'//trim(adjustl(RealStrn))
       RealStrn = ' '
    end do
    !
  end subroutine CreateDirectPartialWidthsFilenameVecReal
  


  !> Normalizes the right eigenvectors again, because maybe
  !! they are not well normalized due to the tolerances defined
  !! in previous programs.
  subroutine RenormalizeRightEigVec( RightEigVec )
    !
    class(ClassComplexMatrix), intent(inout) :: RightEigVec
    !
    complex(kind(1d0)), allocatable :: CR(:,:)
    !
    call RightEigVec%FetchMatrix( CR )
    call ReScaleRightEigVectors( CR )
    RightEigVec = CR
    !
  end subroutine RenormalizeRightEigVec


  !> Normalizes the right eigenvectors as if it comes
  !! from an hermitian operator.
  subroutine RenormalizeRightEigVecAsHermitian( RightEigVec )
    !
    class(ClassComplexMatrix), intent(inout) :: RightEigVec
    !
    complex(kind(1d0)), allocatable :: CR(:,:)
    !
    call RightEigVec%FetchMatrix( CR )
    call NormalizedAsHermitian( CR )
    RightEigVec = CR
    !
  end subroutine RenormalizeRightEigVecAsHermitian



end program ComputePartialWidths
