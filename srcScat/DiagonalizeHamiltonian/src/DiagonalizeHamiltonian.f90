!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

!m fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!

program DiagonalizeHamiltonian

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleHDF5
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleGeneralInputFile

  implicit none

  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: StateSymLabel
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: DipoleAxis
  logical                       :: AnalyzeEvcts
  character*200, dimension(7)   :: InfMats2Rot
  integer                       :: nMats2Rot
  !.. Config file parameters
  character(len=:), allocatable  :: StorageDir
!  real(kind(1d0))               :: ConditionNumber

  type(ClassElectronicSpace)     :: Space
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace
  type(ClassSESSESBlock)         :: BlockDir
  type(ClassMatrix)              :: HamMat
  type(ClassMatrix)              :: Mat
  type(ClassMatrix)              :: tmpMat
  type(ClassMatrix)              :: KetEvec
  type(ClassMatrix), allocatable :: HamEvecsBlocks(:)

  type(ClassGroup), pointer      :: Group

  real(kind(1d0)), allocatable  :: vEval(:),vfact(:)
  type(ClassMatrix)             :: HamEvec




  type(ClassSpectralResolution) :: HamSpecRes,OvSpecResKet


  character(len=:), allocatable :: SymStorageDir
  character(len=1024) :: sBuffer
  character(len=128)  :: BraIonLabel
  character(len=128)  :: KetIonLabel
  
  !*** MAYBE INCONSISTENT: THERE SEEM TO BE NO ROLE FOR CONDITIONLABEL. CHECK
  character(len=:), allocatable :: ConditionLabel
  integer         :: i,j, BraIonIndex, KetIonIndex, nIons
  integer         :: TotNCols, TotNRows
  integer         :: RowBeg, RowEnd
  integer         :: ColBeg, ColEnd
  integer         :: StartRow, RowPWC, nPoly
  logical         :: MatIsSymmetric
  integer         :: nbas, linIndepCounter,nLinDepKet
  real(kind(1d0))               :: ConditionNumber
  !.. Read run-time parameters, among which there is the group and the symmetry
!*********************************
  character(10) :: time
!*********************************
  
  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       StateSymLabel          ,   &
       InfMats2Rot            ,   &
       nMats2Rot              ,   &
       ConditionNumber        ,   &
       AnalyzeEvcts           )

  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           )

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.

  call GetSymmetricElectronicSpace( Space, StateSymLabel, SymSpace )

  !Reading the Hamiltonian blocks, diagonalizing full Hamiltonian and writting
  !the results.
  sBuffer = GetBlockStorageDir(SymSpace,SymSpace)
  allocate( SymStorageDir, source = trim( adjustl( sBuffer ) ) )
  call GetMatFromBlocks(SymStorageDir,SymSpace,'H',HamMat)
  write(*,*) 'Diagonalizing the Hamiltonian'
  call HamMat%Diagonalize(HamSpecRes)
  call HamMat%Free()
  
  !Computing and writting the Hamiltoian between the eigenvectors and the last 
  call GetMatFromBlocks(SymStorageDir,SymSpace,'H_LastBs',Mat,LastBs=.TRUE.)
  nPoly = HamSpecRes%NEval() - Mat%NRows()
  call Mat%AddRows( nPoly, 'START' ) 
  call Mat%Transpose()
  call Mat%Multiply(HamSpecRes%EigenVectors, 'Right', 'N' ) 
  sBuffer = GetOperatorFileName(SymStorageDir,'H_EigenVectors_LastBs')
  call WriteMat(trim(sBuffer),Mat,FORM_IO)

  !Computing and writting the Overlap between the eigenvectors and the last 
  call GetMatFromBlocks(SymStorageDir,SymSpace,'S_LastBs',Mat,LastBs=.TRUE.)
  call Mat%AddRows( nPoly, 'START' ) 
  call Mat%Transpose()
  call Mat%Multiply(HamSpecRes%EigenVectors, 'Right', 'N' ) 
  sBuffer = GetOperatorFileName(SymStorageDir,'S_EigenVectors_LastBs')
  call WriteMat(trim(sBuffer),Mat,FORM_IO)

  !Computing and writting the dervative of the eigenvectors at the end of the
  !box.
  Mat%A = 0.d0
  StartRow = nPoly + 1
  RowPWC = 1
  nIons = SymSpace%GetNumChannels()
  do i = 1,nIons
    call GetOneBlockMat(SymStorageDir,SymSpace,'S_LinInd_DerRMax',i,i,tmpMat)
    call DGEMM('N','N',tmpMat%NRows(),Mat%NColumns(),tmpMat%NColumns(),1.d0,tmpMat%A,tmpMat%NRows(),HamSpecRes%EigenVectors(StartRow,1),size(HamSpecRes%EigenVectors,1),0.d0,Mat%A(RowPWC,1),Mat%NRows())
    StartRow = StartRow + tmpMat%NColumns()
    RowPWC = RowPWC + tmpMat%NRows() 
  enddo
  sBuffer = GetOperatorFileName(SymStorageDir,'Box_EigenStates_DerRMax')
  call WriteMat(trim(sBuffer),Mat,FORM_IO)
  
  sBuffer = GetEigenvaluesFileName(SymStorageDir,HamiltonianLabel)
  call HamSpecRes%WriteEigenvalues( sBuffer, 1 )
  call HamEvec%InitFull(HamSpecRes%Size(),HamSpecRes%Size())
  call HamSpecRes%Fetch(HamEvec%A)
  allocate(HamEvecsBlocks(nIons+1))
  linIndepCounter=0
  nbas=0
  do KetIonIndex=0,nIons
    if (KetIonIndex.eq.0) then
      write(KetIonLabel,'(A)') 'Polycentric'
    else
      KetIonLabel = SymSpace%GetPILabel( KetIonIndex ) 
    endif
    sBuffer = GetSpectrumFileName(SymStorageDir,OverlapLabel )//"_"//KetIonLabel
    call OvSpecResKet%Read( sBuffer, FORM_IO )
    call OvSpecResKet%Fetch( vEval )
    nLinDepKet=0
    do i=1,size(vEval)
       if(vEval(i)/vEVal(size(vEval)) >= ConditionNumber)exit
       nLinDepKet = i
    enddo
    if(minval(vEval(nLinDepKet+1:))<ConditionNumber) call Assert("Eigenvalues are not ordered")
    allocate(vfact(size(vEval)-nLinDepKet))
    vfact=1.d0/sqrt(vEval(nLinDepKet+1:))
    nbas=nbas+size(vEval)
    call OvSpecResKet%Fetch( KetEvec )
    call OvSpecResKet%Free()
    call KetEvec%RemoveColumns( nLinDepKet, "START" )
    call KetEvec%ScaleColumns( vfact )
    call HamEvec%GetSubmatrix(linIndepCounter+1,linIndepCounter+KetEvec%NColumns(),1,HamEvec%NColumns(),HamEvecsBlocks(KetIonIndex+1))
    linIndepCounter=linIndepCounter+size(vEval)-nLinDepKet
    call HamEvecsBlocks(KetIonIndex+1)%Multiply( KetEvec, "Left", "N" )
    call KetEvec%Free()
    deallocate(vfact)
  enddo
  call HamEvec%Free()
  call HamEvec%InitFull(nBas,HamSpecRes%Size())
  rowBeg=1
  do KetIonIndex=0,nIons
    call HamEvec%ComposeFromBlocks(rowBeg,rowBeg+HamEvecsBlocks(KetIonIndex+1)%NRows()-1,1,HamSpecRes%Size(),HamEvecsBlocks(KetIonIndex+1))
    rowBeg = rowBeg + HamEvecsBlocks(KetIonIndex+1)%NRows()
  enddo 
  call HamSpecRes%SetEigenVectors( HamEvec )
  sBuffer = GetSpectrumFileName(SymStorageDir,HamiltonianLabel)
  call HamSpecRes%Write( sBuffer, FORM_IO )
 
  call HamSpecRes%Free()
contains

subroutine diag(A,npot,eig,evctIn)
implicit none
integer*8, intent(in) :: npot
logical, intent(in),optional :: evctIn
real*8,intent(inout) :: A(npot,npot)
real*8, intent(out) :: eig(npot)
logical :: evct
character*1 :: jobz

integer :: info,lwork
real*8 :: work(1)
real*8, allocatable :: work2(:)

evct=.TRUE.
IF (present(evctIn)) THEN
    evct=evctIn
ENDIF

lwork=-1
info=0

IF (evct) THEN
    jobz="V"
ELSE
    jobz="N"
ENDIF

call dsyev(jobz, "U", npot, A, npot, eig, WORK,LWORK,info)
lwork=int(work(1))
allocate (work2(lwork))
call dsyev(jobz, "U", npot, A, npot, eig, WORK2,LWORK,info)
deallocate (work2)

return
end subroutine
  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       StateSymLabel             , &
       InfMats2Rot               , &
       nMats2Rot                 , &
       conditionNumber           , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    real*8                       , intent(out)   :: conditionNumber
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: StateSymLabel
    character*200, dimension(7),   intent(out)   :: InfMats2Rot
    integer,                       intent(out)   :: nMats2Rot
    logical,                       intent(out)   :: AnalyzeEvcts
    character(len=:), allocatable                :: AuxOp
    !
    logical CAP,DipVel,DipLen,x,y,z
    integer i,j
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"   , "Program Input File",  " ", "required" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile", "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-sym"   , "Name of the Irreducible Representation",  " ", "required" )
    call List%Add( "-cap"    , "CAP true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-l"    , "Dipole Gauge Length (l) true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-cn"     , "Condition number used in the transfornation", 1.d0          , "required" )
    call List%Add( "-v"    , "Dipole Gauge Length (v) true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-x"    , "Dipole Orientation x: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-y"    , "Dipole Orientation y: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-z"    , "Dipole Orientation x: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-AnalyzeEvcts"  , &
         "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-cn"     , ConditionNumber  )
    !
    call CheckThreshold( ConditionNumber )
    !
    call List%Get( "-gif"   , sBuffer  )
    allocate( GeneralInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-pif"   , sBuffer  )
    allocate( ProgramInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-cap"   , sBuffer  )
    CAP = .FALSE.
    if (sBuffer .eq. 't' ) CAP = .TRUE.
    !
    call List%Get( "-v"   , sBuffer  )
    DipVel = .FALSE.
    if (sBuffer .eq. 't' ) DipVel = .TRUE.
    !
    call List%Get( "-l"   , sBuffer  )
    DipLen = .FALSE.
    if (sBuffer .eq. 't' ) DipLen = .TRUE.
    !
    call List%Get( "-x"   , sBuffer  )
    x = .FALSE.
    if (sBuffer .eq. 't' ) x = .TRUE.
    !
    call List%Get( "-y"   , sBuffer  )
    y = .FALSE.
    if (sBuffer .eq. 't' ) y = .TRUE.
    !
    call List%Get( "-z"   , sBuffer  )
    z = .FALSE.
    if (sBuffer .eq. 't' ) z = .TRUE.
    !
    call List%Get( "-mult"  , Multiplicity )
    !
    call List%Get( "-ccfile", sBuffer  )
    allocate( CloseCouplingConfigFile, source = trim( sBuffer ) )
    !
    call List%Get( "-esdir" , sBuffer   )
    allocate( NameDirQCResults, source = trim( sBuffer ) )
    !
    AnalyzeEvcts = List%Present( "-AnalyzeEvcts" ) 
    !
    call List%Get( "-sym", sBuffer )
    allocate( StateSymLabel, source = trim(sBuffer) )
    !
    !
    call List%Free()
    !
    !..Check dipole
    i = 0
    j = 0
    if (x) i = i + 1
    if (y) i = i + 1
    if (z) i = i + 1
    if (DipLen) j = j + i
    if (DipVel) j = j + i
    if (CAP)    j = j + 1

    nMats2Rot = j
    write(InfMats2Rot,'(a)') ""
    i = 0
    if (DipLen) then
      if (x) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleLen_x'
      endif
      if (y) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleLen_y'
      endif
      if (z) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleLen_z'
      endif
    endif 
    if (DipVel) then
      if (x) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleVel_x'
      endif
      if (y) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleVel_y'
      endif
      if (z) then
        i = i + 1
        write(InfMats2Rot(i),'(a)') 'DipoleVel_z'
      endif
    endif 
    if (CAP) then
      i = i + 1
      write(InfMats2Rot(i),'(a)') 'CAP1_'
    endif
    !
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "Read run time parameters :"
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "SAE General Input File  : "//GeneralInputFile
    write(OUTPUT_UNIT,"(2a)"  ) "Program Input File      : ", ProgramInputFile
    write(OUTPUT_UNIT,"(a,i0)") "Multiplicity            : ", Multiplicity
    write(OUTPUT_UNIT,"(2a)"  ) "Close Coupling File     : ", CloseCouplingConfigFile
    write(OUTPUT_UNIT,"(a)"   ) "Nuclear Config Dir Name : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
    write(OUTPUT_UNIT,"(a)"   ) "Symmetry                : "//StateSymLabel
    write(OUTPUT_UNIT,"(a)"   ) "Files to Rotate         :"//InfMats2Rot(1:nMats2Rot)
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    !
  end subroutine ParseProgramInputFile

  subroutine GetOneBlockMat(SymStorageDir,SymSpace,OperatorLabel,Bra,Ket,Mat)

    character(len=*),                         intent(in) :: OperatorLabel
    character(len=*),                         intent(in) :: SymStorageDir
    type(ClassSymmetricElectronicSpace),      intent(in) :: SymSpace
    type(ClassMatrix),                        intent(out):: Mat

    integer Bra,Ket,nIons
    character*30 BraIonLabel,KetIonLabel

    write(output_unit,"(a)") 'Loading', trim(OperatorLabel),' Block'
    nIons = SymSpace%GetNumChannels()
    BraIonLabel = "Polycentric"
    if ( Bra > 0 ) BraIonLabel = SymSpace%GetPILabel(Bra)
    KetIonLabel = "Polycentric"
    if ( Ket > 0 ) KetIonLabel = SymSpace%GetPILabel(Ket)
    sBuffer = GetOperatorFileName(SymStorageDir,OperatorLabel,"_"//trim(adjustl(BraIonLabel))//"_"//trim(adjustl(KetIonLabel)))
    call Mat%Read( FileName = trim(adjustl(sBuffer)), Form = FORM_IO)
  end subroutine GetOneBlockMat

  subroutine GetMatFromBlocks(SymStorageDir,SymSpace,OperatorLabel,Mat, LastBs)

    character(len=*),                         intent(in) :: OperatorLabel
    character(len=*),                         intent(in) :: SymStorageDir
    type(ClassSymmetricElectronicSpace),      intent(in) :: SymSpace
    logical, optional,                        intent(in) :: LastBs
    type(ClassMatrix),                        intent(out):: Mat
    type(ClassMatrix), allocatable                       :: MatBlock(:,:)

    integer TotNCols,TotNRows,BraIonIndex,KetIonIndex,RowBeg,RowEnd,ColBeg,ColEnd,nIons,sum
    character*30 BraIonLabel,KetIonLabel

    write(output_unit,"(a)") 'Loading', trim(OperatorLabel),' Blocks...'
    nIons = SymSpace%GetNumChannels()
    if (present(LastBs) .and. LastBs ) then
      allocate(MatBlock(nIons,nIons))
      sum = 0
    else
      allocate(MatBlock(nIons+1,nIons+1))
      sum = 1
    endif  

    TotNCols = 0
    TotNRows = 0
    BraIonLabel = "Polycentric"
    do BraIonIndex = 0, nIons
       if (present(LastBs) .and. LastBs .and. BraIonIndex .eq. 0 ) cycle
       if ( BraIonIndex > 0 ) BraIonLabel = SymSpace%GetPILabel(BraIonIndex)
       KetIonLabel = "Polycentric"
       do KetIonIndex = 0, nIons
          if (present(LastBs) .and. LastBs .and. KetIonIndex .eq. 0 ) cycle
          if ( KetIonIndex > 0 ) KetIonLabel = SymSpace%GetPILabel(KetIonIndex)
          sBuffer = GetOperatorFileName(SymStorageDir,OperatorLabel,"_transformed")//"_"//trim(adjustl(BraIonLabel))//"_"//trim(adjustl(KetIonLabel))
          if (present(LastBs) .and. LastBs) &
          sBuffer = GetOperatorFileName(SymStorageDir,OperatorLabel)//"_"//trim(adjustl(BraIonLabel))//"_"//trim(adjustl(KetIonLabel))
          call MatBlock(BraIonIndex + sum,KetIonIndex + sum)%Read( FileName = trim(adjustl(sBuffer)), Form = FORM_IO)
       enddo
       TotNRows = TotNRows + size(MatBlock(BraIonIndex + sum,BraIonIndex + sum)%A,1)
       TotNCols = TotNCols + size(MatBlock(BraIonIndex + sum,BraIonIndex + sum)%A,2)
    enddo


    call Mat%InitFull(TotNRows,TotNCols)
    
    RowBeg = 1
    do BraIonIndex = 0, nIons
       if (present(LastBs) .and. LastBs .and. BraIonIndex .eq. 0 ) cycle
       ColBeg = 1
       RowEnd = RowBeg + size(MatBlock(BraIonIndex + sum,BraIonIndex + sum)%A,1) - 1
       do KetIonIndex = 0, nIons
          if (present(LastBs) .and. LastBs .and. KetIonIndex .eq. 0 ) cycle
          ColEnd = ColBeg + size(MatBlock(BraIonIndex + sum,KetIonIndex + sum)%A,2) - 1
          call Mat%ComposeFromBlocks(RowBeg, RowEnd, ColBeg, ColEnd, MatBlock(BraIonIndex + sum,KetIonIndex + sum) )
          call MatBlock(BraIonIndex + sum,KetIonIndex + sum)%Free()
          ColBeg = ColEnd + 1
       enddo
       RowBeg = RowEnd + 1
    enddo
    deallocate(MatBlock)
  end subroutine GetMatFromBlocks

  subroutine WriteMat(FileName,Mat,Form)
 
    character(len=*)              , intent(in) :: FileName
    character(len=*)              , intent(in) :: Form
    type(ClassMatrix)             , intent(in) :: Mat

    integer uid,i,j,start,end

    open(NewUnit =  uid      , &
         File    =  FileName , &
         Status  =  'replace', &
         form    =  Form       )
    start = 1
    end = size(Mat%A,2)
    if (trim(Form) .eq. 'FORMATTED') then
      write(uid,'(2(x,I0))') size(Mat%A,1),size(Mat%A,2) 
      do i=start,end
        do j=1,size(Mat%A,1)
          write(uid,'(E30.18)') Mat%A(j,i)
        enddo
      enddo 
    else
      write(uid) size(Mat%A,1),size(Mat%A,2) 
      do i=start,end
        do j=1,size(Mat%A,1)
          write(uid) Mat%A(j,i)
        enddo
      enddo
    endif
    close(uid)

  end subroutine WriteMat


end program DiagonalizeHamiltonian
