!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!

program TransformOperators

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleElectronicSpace
  use ModuleGeneralInputFile
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleHDF5
  use ModuleBasis
  use ModuleBSpline
  use ModuleGaussianBSpline
  use ModuleSymmetricElectronicSpaceSpectralMethods

  implicit none

  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: StateSymLabel
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: DipoleAxis
  logical                       :: UseConditionedBlocks
  logical                       :: AnalyzeEvcts

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber

  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace
  type(ClassSESSESBlock)        :: TransMat
  type(ClassMatrix),allocatable :: OpMat(:,:)
  type(ClassMatrix)             :: OpMatTotal
  type(ClassMatrix)             :: LastBsMat
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassMatrix)             :: LastBsBoxDerMat
  type(ClassSESSESBlock)        :: OpBlock
  type(ClassSESSESBlock)        :: MatLastBsBlock
  type(ClassBSpline)            :: BSplineBasis
  type(ClassBasis)              :: GABSBasis
  type(ClassMatrix) :: OpTrans
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm

  real(kind(1d0)), allocatable  :: vEval(:)
  real(kind(1d0)), allocatable  :: vfact(:)
  integer                       :: nLinDepBra
  integer                       :: nLinDepKet
  type(ClassMatrix)             :: BraEvec
  type(ClassMatrix)             :: KetEvec

  type(ClassSpectralResolution) :: OvSpecResBra
  type(ClassSpectralResolution) :: OvSpecResKet
  type(ClassSpectralResolution) :: OpSpecRes
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  integer, allocatable          :: BsRemovedInf(:)
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: BSplineBasisFile
  character(len=:), allocatable :: SAEBasisFile
  type(ClassSpectralResolution) :: HamSpecRes
  character(len=:), allocatable :: SAEstoredir
  character(len=1024) :: sBuffer
  character(len=1024) :: Filename

  !*** MAYBE INCONSISTENT: THERE SEEM TO BE NO ROLE FOR CONDITIONLABEL. CHECK
  character(len=:), allocatable :: ConditionLabel
  integer         :: i, k, BraIonIndex, KetIonIndex, NumPWC, uid,nPIs,nBas,RowStart,ColStart,boxi
  real(kind(1d0)) :: MaxAllowedEigVal, LastBsBoxDer, LastBsVal, LastBsDer
  logical         :: MatIsSymmetric

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       nPIs                   ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       StateSymLabel          ,   &
       UseConditionedBlocks   ,   &
       OperatorLabel          ,   &
       ConditionNumber        ,   &
       DipoleAxis             ,   &
       boxi                   ,   &
       AnalyzeEvcts           )

  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       BSplineBasisFile     , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
  
  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 


  !.. Initialises the GABS basis
  !..
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call GABSBasis%Init( SAEBasisFile )

  !..The same symmetries are allowed for the overlap and the 
  !  different matrices, so this subroutine can be called only once.
  call GetSymmetricElectronicSpace( Space, StateSymLabel, SymSpace )
  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          .FALSE. )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
  endif

  if ( SET_FORMATTED_WRITE ) call OpBlock%SetFormattedWrite()
  call OpBlock%SetBoxOnly()

  if ( OperatorLabel .is. CAPLabel ) then
    deallocate( OperatorLabel )
    allocate ( OperatorLabel, source = GetFullCAPLabel(1) )
  end if

  write(output_unit,"(a)") 'Loading Matrix ...'
  !
  allocate(OpMat(nPIs+1,nPIs+1))
  nBas=0
  do BraIonIndex=0,nPIs
    do KetIonIndex=0,nPIs
      write(*,"(a,i0)") "BraIonIndex: ",BraIonIndex
      write(*,"(a,i0)") "KetIonIndex: ",KetIonIndex
      if (UseConditionedBlocks) then
        call OpBlock%LoadMonoIonPolyCondition( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, Conditioner, BsRemovedInf = BsRemovedInf,Axis=DipoleAxis )
      else
        call OpBlock%LoadMonoIonPoly( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, DipoleAxis )
      endif
      call OpBlock%AssembleChCh( OpMat(BraIonIndex+1,KetIonIndex+1), BraIonIndex, KetIonIndex )
    enddo
    nBas=nBas+OpMat(BraIonIndex+1,1)%nRows()
  enddo
  call OpBlock%Free()
  call OpMatTotal%InitFull(nBas,nBas)
  RowStart=1
  do BraIonIndex=0,nPIs
    ColStart=1
    do KetIonIndex=0,nPIs
      call OpMatTotal%ComposeFromBlocks(RowStart,RowStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nRows()-1,ColStart,ColStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nColumns()-1,OpMat(BraIonIndex+1,KetIonIndex+1))
      ColStart = ColStart + OpMat(BraIonIndex+1,KetIonIndex+1)%nColumns()
      if (KetIonIndex.eq.nPIs) RowStart=RowStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nRows()
      call OpMat(BraIonIndex+1,KetIonIndex+1)%Free()
    enddo
  enddo
  deallocate(OpMat)
  write(*,*) 'Reading the Hamiltonian already diagonalized'
  sBuffer = GetBlockStorageDir(SymSpace,SymSpace)
  allocate( SymStorageDir, source = trim( adjustl( sBuffer ) ) )
  sBuffer = GetSpectrumFileName(SymStorageDir,HamiltonianLabel) 
  call HamSpecRes%Read( sBuffer, FORM_IO )
  call OpMatTotal%Multiply(HamSpecRes%EigenVectors, 'Right', 'N' )
  call OpMatTotal%Multiply(HamSpecRes%EigenVectors, 'Left' , 'T' )
  call HamSpecRes%Free()
  if(OperatorLabel(1:3).eq.'Dip') then
    write(FileName,'(A)') trim(OperatorLabel)//'_'//trim(DipoleAxis)//'.box'
  else
    write(FileName,'(A)') trim(OperatorLabel)//'.box'
  endif
  sBuffer = GetOperatorFileName(SymStorageDir,trim(FileName)) 
  call WriteMat(trim(sBuffer),OpMatTotal,FORM_IO,boxi)
  call OpMatTotal%Free()

contains


  !> Reads the run time parameters from the command line.

  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       nPIs                      , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       StateSymLabel             , &
       UseConditionedBlocks      , &
       OperatorLabel             , &
       conditionNumber           , &
       DipoleAxis                , &
       boxi                      , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    real*8                       , intent(out)   :: conditionNumber
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity,boxi
    integer,                       intent(out)   :: nPIs
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: StateSymLabel
    logical,                       intent(out)   :: UseConditionedBlocks
    character(len=:), allocatable, intent(out)   :: OperatorLabel
    character(len=:), allocatable, intent(out)   :: DipoleAxis
    character(len=:), allocatable                :: Gauge
    character(len=:), allocatable                :: AuxOp
    logical,                       intent(out)   :: AnalyzeEvcts
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Compute the overlap, Hamiltonian, CAP and Quenched "//&
         "Hamiltonian spectrum in the box, "//&
         "for the selected symmetric electronic spaces."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"   , "Program Input File",  " ", "required" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List%Add( "-boxi"    , "The state number you want to rotate the different matrices" ,  1, "optional" )
    call List%Add( "-ccfile", "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-sym"   , "Name of the Irreducible Representation",  " ", "required" )
    call List%Add( "-npi"   , "Number of Parent Ions",  1, "required" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-cn"     , "Condition number used in the transfornation", 1.d0          , "required" )
    call List%Add( "-op"     , "Name of the Operator",  " ", "required" )
    call List%Add( "-gau"    , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-axis"   , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-AnalyzeEvcts"  , &
         "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    UseConditionedBlocks = List%Present( "-cond" )
    !
    call List%Get( "-gif"   , sBuffer  )
    allocate( GeneralInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-op"     , sbuffer           )
    !
    allocate( OperatorLabel          , source = trim( sbuffer )     )
    !
    call CheckOperator( OperatorLabel )
    !
    if(List%Present("-gau"))then
       call List%Get( "-gau", sbuffer )
       allocate( Gauge, source = trim(sbuffer) )
       call CheckGauge( Gauge )
    end if
    !
    if(List%Present("-axis"))then
       call List%Get( "-axis", sbuffer )
       allocate( DipoleAxis, source = trim(sbuffer) )
       call CheckAxis( DipoleAxis )
    end if
    !
    call List%Get( "-pif"   , sBuffer  )
    allocate( ProgramInputFile, source = trim( sBuffer ) )
    !
    call List%Get( "-boxi"  , boxi )
    call List%Get( "-mult"  , Multiplicity )
    call List%Get( "-npi"  , nPIs )
    !
    call List%Get( "-ccfile", sBuffer  )
    allocate( CloseCouplingConfigFile, source = trim( sBuffer ) )
    !
    call List%Get( "-esdir" , sBuffer   )
    allocate( NameDirQCResults, source = trim( sBuffer ) )
    !
    AnalyzeEvcts = List%Present( "-AnalyzeEvcts" ) 
    !
    call List%Get( "-cn"     , ConditionNumber  )
    !
    call CheckThreshold( ConditionNumber )
    !
    call List%Get( "-sym", sBuffer )
    allocate( StateSymLabel, source = trim(sBuffer) )
    !
    call List%Free()
    !
    !..Check dipole
    if ( OperatorLabel .is. DipoleLabel ) then
       if ( .not.allocated(Gauge) ) call Assert( 'For '//DipoleLabel//' operator the gauge must be present.' )
       if ( .not.allocated(DipoleAxis) ) call Assert( 'For '//DipoleLabel//' operator the orientation must be present.' )
       allocate( AuxOp, source = OperatorLabel//Gauge )
       deallocate( OperatorLabel )
       allocate( OperatorLabel, source = AuxOp )
       deallocate( AuxOp )
    end if

    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a)"   ) "Read run time parameters :"
    write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
    write(OUTPUT_UNIT,"(a)"   )
    write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
    write(OUTPUT_UNIT,"(a)"   ) "SAE General Input File  : "//GeneralInputFile
    write(OUTPUT_UNIT,"(2a)"  ) "Program Input File      : ", ProgramInputFile
    write(OUTPUT_UNIT,"(a,i0)") "Multiplicity            : ", Multiplicity
    write(OUTPUT_UNIT,"(2a)"  ) "Close Coupling File     : ", CloseCouplingConfigFile
    write(OUTPUT_UNIT,"(a)"   ) "Nuclear Config Dir Name : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)"   ) "Symmetry                : "//StateSymLabel
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       BSplineBasisFile           , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    character(len=:), allocatable, intent(out) :: BSplineBasisFile
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: sBuffer, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "."    , "optional" )
    call List%Add( "BSplineBasisFile"    , "BSplineBasis", "optional" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"                , sBuffer   )
    allocate( StorageDir      , source = trim(adjustl(sBuffer)) )
    !
    call List%Get( "BSplineBasisFile"          , sBuffer   )
    allocate( BSplineBasisFile, source = trim(adjustl(sBuffer)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , sBuffer   )
    allocate( ConditionBsMethod, source = trim(adjustl(sBuffer)) )
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    !
  end subroutine ParseProgramInputFile

  subroutine WriteMat(FileName,Mat,Form,boxi)
 
    character(len=*)              , intent(in) :: FileName
    character(len=*)              , intent(in) :: Form
    integer, optional             , intent(in) :: boxi
    type(ClassMatrix)             , intent(in) :: Mat

    integer uid,i,j,start,end

    open(NewUnit =  uid      , &
         File    =  FileName , &
         Status  =  'replace', &
         form    =  Form       )
    start = 1
    end = size(Mat%A,2)
    if (present(boxi) .and. boxi .gt. 0) then
      start = boxi
      end = boxi
    elseif (present(boxi) .and. boxi .lt. -1) then
      start = 1
      end = 1 -boxi - 1
    endif
    if (trim(Form) .eq. 'FORMATTED') then
      write(uid,'(2(x,I0))') size(Mat%A,1),end-start+1 
      do i=start,end
        do j=1,size(Mat%A,1)
          write(uid,'(E30.18)') Mat%A(j,i)
        enddo
      enddo 
    else
      write(uid) size(Mat%A,1),size(Mat%A,2) 
      do i=start,end
        do j=1,size(Mat%A,1)
          write(uid) Mat%A(j,i)
        enddo
      enddo
    endif
    close(uid)

  end subroutine WriteMat


end program TransformOperators
