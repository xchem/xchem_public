!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************


! {{{ Detailed description
!!  Author : Luca Argenti
!!           University of Central Florida, 2017
!!
!> \mainpage Program ComputePhotoelectronSpectrum.
!! 
!! Synopsis:
!! ---------
!!
!! ___
!! Description (applicable to atoms only):
!! ------------
!!
! }}}
program ComputeProjections

  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING

  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleString
  use ModuleIO
  use ModuleHDF5

  use ModuleElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleParentIons

  use ModuleConstants
  use ModuleDiagonalize
  use ModuleScatteringStates

  implicit none

  enum, bind(c)
     enumerator :: MODE_PRINT_INFO
     enumerator :: MODE_UNIFORM
     enumerator :: MODE_RYDBERG
     enumerator :: MODE_RESOLVE
     enumerator :: MODE_REFINE 
  end enum

  !.. Run-time parameters
  !..
  character(len=:), allocatable :: ProgramInputFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: CloseCouplingConfigFile
  integer                       :: Multiplicity
  character*200, allocatable    :: scatLabel(:)
  character(len=:), allocatable :: SymLabel
  character(len=:), allocatable :: PsiFile
  character(len=:), allocatable :: OutDipFile
  real(kind(1d0))               :: Time,RealTime
  character(len=:), allocatable :: OutDir

  character(len=:), allocatable :: StorageDir
  type(ClassElectronicSpace)    :: Space
  character(len=:), allocatable :: SymStorageDir
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace

  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: nChannels

  integer :: uid, iBuf, iScat,iStart,iEnd

  integer           , allocatable :: vnOpen(:)

  !> Linked-list with the scattering states at several energies
  type(ClassScatteringStateList)          :: ScatStateList
  type(ClassScatteringState)    , pointer :: ScatState

  complex(kind(1d0)), allocatable :: zPsi(:)
  complex(kind(1d0)), allocatable :: zAmp(:,:)
  character*200                 :: tmpCht
  character(len=:), allocatable :: ccscat_dir
  integer                      :: iEn,BoxStateIndex,nBoxStates,iBox
  logical                      :: Dipole 
  character(len=:),allocatable :: ChanName
  real(kind(1d0)), allocatable :: Egrid(:)

  call GetRunTimeParameters(    &
              ProgramInputFile, &
              NameDirQCResults, &
       CloseCouplingConfigFile, &
              Multiplicity    , &
              SymLabel        , &
              PsiFile         , &
              OutDipFile      , &
              Time            , &
              BoxStateIndex   , &
              Dipole          , &
              scatLabel       , &
              OutDir          )


  call Execute_Command_Line(" mkdir -p "//OutDir)
  call ParseProgramInputFile( ProgramInputFile, StorageDir )
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  call GetSymmetricElectronicSpace( Space, SymLabel, SymSpace )
  allocate( SymStorageDir, source = GetBlockStorageDir( SymSpace, SymSpace ) )
  allocate(ccscat_dir, source = SymStorageDir//"/ScatteringStates/SPEC/")

  call ScatStateList%Init(ccscat_dir)
  do iScat=1,size(scatLabel)
    !.. Reconstruct the list of thresholds and print it on screen
    call ScatStateList%Load( trim(scatLabel(iScat)) )
    call ScatStateList%GetEnergyList( Egrid )
    call ScatStateList%GetnOpenList(  vnOpen )
  !  call ScatStateList%GetPsiMinus( 1, 1, zPsi )
    call ScatStateList%GetNBoxStates(nBoxStates)
    allocate(zPsi( nBoxStates ))
    if (BoxStateIndex .gt. 0) then
      iStart = BoxStateIndex
      iEnd = BoxStateIndex
    elseif (BoxStateIndex .lt. -1) then
      iStart = 1
      iEnd = -BoxStateIndex
    else 
      iStart = 1
      iEnd = nBoxStates
    endif
    do iBox=iStart,iEnd
      write(OUTPUT_UNIT,'(a,i0)') 'The Number of BoxStates is : ',nBoxStates
      zPsi = Z0
   
      RealTime = Time
      
      if (Dipole) then
        call LoadPsi( SymStorageDir//'BoxStates/'//PsiFile, RealTime, zPsi, iBox = iBox  ) 
      else  
        call LoadPsi( PsiFile, RealTime, zPsi ) 
      endif
  
      !.. Compute the partial-wave amplitudes (PWA)
      zPsi=conjg(zPsi)
      call ScatStateList%ProjectOnBra(zPsi,zAmp)
      zAmp=conjg(zAmp)
  
      !.. Save PWA to disk
      write(tmpCht,'(I0)') iBox
      if (Dipole) then
        call SavePWA( zAmp, vnOpen, Egrid, OutDir, OutDipFile//trim(tmpCht)//'_'//trim(scatLabel(iScat)))
      else
        call SavePWA( zAmp, vnOpen, Egrid, OutDir, 'zAmp_'//trim(scatLabel(iScat)) )
      endif
    enddo
    deallocate(zPsi)
  enddo

contains
  

  function UniformGrid(xmi,xma,n) result(grid)
    real(kind(1d0)), intent(in) :: xmi, xma
    integer        , intent(in) :: n
    real(kind(1d0)), allocatable :: grid(:)
    integer :: i
    allocate(grid,source=[(xmi+(xma-xmi)/dble(n-1)*dble(i-1),i=1,n)])
  end function UniformGrid


  !> Reads the run time parameters specified in the command line.
  !! If an optional inherently positive parameters is not specified
  !! on the command line, it is initialized to a negative number
  !! to signal that it should not be used to evaluate the energy grid.
  !<
  subroutine GetRunTimeParameters( &
       ProgramInputFile, &
       NameDirQCResults, &
       ConfigFile      , &
       Multiplicity    , &
       SymLabel        , &
       PsiFile         , &
       OutDipFile      , &
       Time            , &
       BoxStateIndex   , &
       Dipole          , &
       ScatLabel       , &
       OutDir)
    !
    use ModuleErrorHandling
    use ModuleCommandLineParameterList
    use ModuleString    
    implicit none
    !
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    character(len=:), allocatable, intent(out) :: ConfigFile
    logical                      , intent(out) :: Dipole
    integer                      , intent(out) :: Multiplicity
    integer                      , intent(out) :: BoxStateIndex
    character(len=:), allocatable, intent(out) :: SymLabel
    character(len=:), allocatable, intent(out) :: PsiFile
    character*200   , allocatable, intent(out) :: scatLabel(:)
    character(len=:), allocatable, intent(out) :: OutDipFile
    real(kind(1d0)) ,              intent(out) :: Time
    character(len=:), allocatable, intent(out) :: OutDir
    character(len=:), allocatable              ::  scatLabelList
    character(len=:), allocatable              :: Gauge
    character(len=:), allocatable              :: DipoleAxis
    !
    type( ClassCommandLineParameterList ) :: List
    !
    character(len=*), parameter :: PROGRAM_DESCRIPTION=&
         "Compute multichannel scattering states"
    character(len=512) :: strnBuf, pif
    integer i,j,counter
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " "    , "required" )
    call List%Add( "-label" , "subdir label" , ""            , "optional" )
    call List%Add( "-ccfile"  , "Config File"  , "CCFile"      , "optional" )
    call List%Add( "-boxi"  , "Box state index", 1           , "optional" )
    call List%Add( "-gau"   , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-scatlabellist" , "list of labels of the set of scattering statesi (one blank space between labels)"  ,  "1", "optional" )
    call List%Add( "-axis"  , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-mult"  , "Total Multiplicity", 1        , "required" )
    call List%Add( "-sym"   , "SymLabel"         , "A"       , "required" )
    call List%Add( "-psi"   , "WaveFunction File", "coef.out", "optional" )
    call List%Add( "-t"     , "Projection Time"  ,  0.d0     , "optional" )
    call List%Add( "-od"    , "OutDir"           , "out"     , "required" )
    !
    call List.Parse()
    !
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if

    if (List.Present("-axis") .and. List.Present("-gau") ) then
      Dipole = .TRUE.
    elseif (List.Present("-axis") .or. List.Present("-gau") ) then 
      call Assert('Both (-gau and -axis) have to be specified for calculating the Dipole Tranistion Amplitudes')
    else
      Dipole = .FALSE.
    endif

    call List%Get( "-gau"    , strnBuf )
    allocate( Gauge          , source = trim( strnBuf )    )
    call List%Get( "-axis"   , strnBuf )
    allocate( DipoleAxis             , source = trim( strnBuf )   )
    call List%Get( "-boxi"   , BoxStateIndex )
    call List%Get( "-pif"    , pif  )
    allocate( ProgramInputFile       , source = trim( pif )    )
    call List.Get( "-label",  strnBuf  )
    allocate(NameDirQCResults,source=trim(adjustl(strnBuf)))
    call List.Get( "-ccfile",  strnBuf  )
    allocate(ConfigFile,source=trim(adjustl(strnBuf)))
    call List%Get( "-mult"   , Multiplicity )
    call List.Get( "-sym"  ,  strnBuf  )
    allocate( SymLabel, source=trim(adjustl(strnBuf)))
    call List.Get( "-psi"  ,  strnBuf  )
    allocate( PsiFile, source=trim(adjustl(strnBuf)))
    call List.Get( "-t",  Time  )
    call List.Get( "-od",  strnBuf  )
    allocate( OutDir, source=trim(adjustl(strnBuf)))
    call List%Get( "-scatlabellist",  strnBuf  )
    allocate(scatLabelList,source=trim(adjustl(strnBuf)))
    !
    j=0
    do i=1,len(trim(scatLabelList))
      if (scatLabelList(i:i) .eq. ':') j=j+1
    enddo
    allocate(scatLabel(j+1))
    j=1
    counter=1
    do i=1,len(trim(scatLabelList))
      if (scatLabelList(i:i) .eq. ':') then
        scatLabel(counter) = scatLabelList(j:i-1)
        counter = counter + 1
        j = i + 1
      endif
    enddo
    scatLabel(counter) = scatLabelList(j:len(trim(scatLabelList)))
    call List.Free()

    if (Dipole) then
      deallocate(PsiFile)
      if (trim(Gauge) .eq. 'l') then
        allocate( PsiFile, source=trim('Dipole'//'Len_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A)') trim('DipoleTransAmp'//'Len_'//trim(DipoleAxis)//'_FromBoxState_')
        allocate( OutDipFile, source=trim(strnBuf))
      elseif (trim(Gauge) .eq. 'v' ) then    
        allocate( PsiFile, source=trim('Dipole'//'Vel_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A)') trim('DipoleTransAmp'//'Vel_'//trim(DipoleAxis)//'_FromBoxState_')
        allocate( OutDipFile, source=trim(strnBuf))
      else
        call Assert('The Dipole gauge is not well specified')
      endif
    endif
    !.. Must add check on the input
    !
    write(OUTPUT_UNIT,"(a)" ) "Run time parameters :"
    write(OUTPUT_UNIT,"(a)" ) "Subdir label    : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)" ) "CC Config File  : "//ConfigFile
    write(OUTPUT_UNIT,"(a)" ) "Spectral SymLabel   : "//SymLabel
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile

  subroutine LoadPsi( PsiFile, RealTime, zPsi, iBox )
    character(len=*)  , intent(in)    :: PsiFile
    real(kind(1d0))   , intent(inout) :: RealTime
    complex(kind(1d0)), intent(inout) :: zPsi(:)
    integer, optional , intent(in)    :: iBox
    integer  :: uid, iostat, iTime, i, j
    real(kind(1d0)) :: t, norm, val
    real(kind(1d0)), allocatable      :: realvals(:), imagvals(:)


    open( newunit=uid, File=PsiFile, form="formatted", status="old" )
    if (present(iBox) ) then
      read(uid,*) t
      do i = 1,iBox-1
        do j = 1,t
          read(uid,*)
        enddo
      enddo
        do j = 1,t
          read(uid,*) val
          zPsi(j) = (1.d0,0)*val
        enddo
    else
      allocate(realvals(size(zPsi,1)),imagvals(size(zPsi,1)))
      itime=0
      do
         read(uid,*,iostat=iostat) t
         if(iostat/=0)exit
         if(t>RealTime)exit
         itime=itime+1
      enddo
      rewind(uid)
      do i=1,itime-1
         read(uid,*)
      enddo
      read(uid,*,iostat=iostat) RealTime, norm, (realvals(i),imagvals(i),i=1,size(zPsi,1))
      zPsi = realvals + imagvals*(0.d0,1.d0)
      if(iostat/=0) zPsi(i)=Z0
    endif
    close(uid)

  end subroutine LoadPsi


  subroutine SavePWA( zAmp, vnOpen, Egrid, OutDir, fname )
    complex(kind(1d0)), intent(in) :: zAmp(:,:)
    integer           , intent(in) :: vnOpen(:)
    real(kind(1d0))   , intent(in) :: Egrid(:)
    character(len=*)  , intent(in) :: OutDir
    character(len=*) , optional , intent(in) :: fname

    integer  :: uid, iostat, iEn, nOpen, ich
    open( newunit=uid, File=trim(OutDir)//'/'//trim(fname), form="formatted", status="replace" )
    write(uid,*)"# ",size(Egrid)
    do iEn=1,size(Egrid)
       nOpen=vnOpen(iEn)
       write(uid,"(e24.16,x,i6,*(x,e24.16))") Egrid(iEn), nOpen, (zAmp(iEn,ich),ich=1,nOpen)
    enddo
    close(uid)

  end subroutine SavePWA


end program ComputeProjections


