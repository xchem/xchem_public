!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program CreateScattScript
  
  
  use, intrinsic :: ISO_FORTRAN_ENV
  

  Character(len=*), parameter :: GeneralInputFile        = "test_GeneralInputFile_Mixed_HePlus_GaussL3_Rmax200"
  character(len=*), parameter :: ProgramInputFile        = "XchemInputFile"
  Character(len=*), parameter :: GroupName               = "C1"
  Character(len=*), parameter :: SymmetryName            = "A"
  character(len=*), parameter :: CurrentMultiplicity     = "1"
  character(len=*), parameter :: CloseCouplingConfigFile = "C1_H2_Sym_L3"
  character(len=*), parameter :: NameDirQCResults        = "H2.Sl3.R1.4.thr-1e-7_1e-7.interface"
  character(len=*), parameter :: UseExternalMonomials    = "-usemon"
  character(len=*), parameter :: ForceComputation        = "-force"
  character(len=*), parameter :: CompMethod              = "hom"
  character(len=*), parameter :: ModeStrn                = "down"
  character(len=*), parameter :: GenSuffix               = "New"
  character(len=*), parameter :: SystDir                 = "../../DataStructure/Storage/R1.000/Structure/1A/"
  character(len=*), parameter :: Gauge                 = "v"
  !
  real(kind(1d0)) , parameter :: Emin                    = -0.569d0
  real(kind(1d0)) , parameter :: Emax                    = 0.1d0
  integer         , parameter :: NumEnergies             = 400
  integer         , parameter :: NumQuests               = 20
  !
  logical         , parameter :: CreateNullSpaceFile     = .false.

  
  real(kind(1d0)), allocatable :: Energies(:)
  real(kind(1d0)), allocatable :: EminEmax(:,:)
  integer,         allocatable :: NumEn(:)
  integer                      :: i
  character(len=:), allocatable :: ResultsDir



  call BuildGrid( NumEnergies, Emin, Emax, Energies )
  call BuildPartition( NumQuests, Energies, NumEn, EminEmax )
  !
!!$  do i = 1, size(NumEn)
!!$     write(*,*) EminEmax(i,1), EminEmax(i,2)
!!$  end do


  call PrepareScripts( NumEn, EminEmax )


  if ( CreateNullSpaceFile ) then
     allocate( ResultsDir, source = SystDir//"Results_"//NameDirQCResults//"/" )
     call BuildUniqueNullSpaceFile( ResultsDir, NumQuests, NumEnergies )
  end if


contains



  subroutine BuildGrid( N, Vmin, Vmax, Vector )
    !
    integer,                      intent(in)  :: N
    real(kind(1d0)),              intent(in)  :: Vmin
    real(kind(1d0)),              intent(in)  :: Vmax
    real(kind(1d0)), allocatable, intent(out) :: Vector(:)
    !
    integer :: i
    !
    allocate( Vector(N) )
    !
    if ( N == 1 ) then
       !
       Vector(1) = Vmin
       !
    else
       !
       do i = 1, N
          Vector(i) = Vmin + (Vmax-Vmin)*dble(i-1)/dble(N-1)
       end do
       !
    end if
    !
  end subroutine BuildGrid




  subroutine BuildPartition( NumQuests, Energies, NumEn, EminEmax )
    !
    integer,                      intent(in)  :: NumQuests
    real(kind(1d0)),              intent(in)  :: Energies(:)
    integer,         allocatable, intent(out) :: NumEn(:)
    real(kind(1d0)), allocatable, intent(out) :: EminEmax(:,:)
    !
    integer :: NumTot, AveQuest, i, Counter
    !
    NumTot = size(Energies)
    !
    allocate( NumEn(NumQuests) ) 
    NumEn = 0
    !
    allocate( EminEmax(NumQuests,2) )
    EminEmax = 0.d0
    !
    AveQuest = floor(dble(NumTot)/dble(NumQuests))
    !
    NumEn(1) = NumTot - (NumQuests-1)*AveQuest
    EminEmax(1,1) = Energies(1)
    EminEmax(1,2) = Energies(NumEn(1))
    Counter = NumEn(1)
    do i = 2, NumQuests
       NumEn(i) = AveQuest
       EminEmax(i,1) = Energies(Counter+1)
       Counter = Counter + NumEn(i)
       EminEmax(i,2) = Energies(Counter)
    end do
    !
  end subroutine BuildPartition




  subroutine PrepareScripts( NumEn, EminEmax )
    !
    integer,         intent(in) :: NumEn(:)
    real(kind(1d0)), intent(in) :: EminEmax(:,:)
    !
    integer :: i, uid, uidG, uidA, uid2, NumFiles
    character(len=:), allocatable :: File, GenFile, AnalFile, File2, Suffix, NSFile
    character(len=32) :: StrnI, StrnEmin, StrnEmax, StrnNE
    !
    NumFiles = size(NumEn)
    !
    allocate( GenFile, source = "ScatteringScripts_"//GenSuffix//".sh" )
    call OpenFile( GenFile, uidG, "write", "formatted" )
    allocate( AnalFile, source = "AnalysisScatteringScripts_"//GenSuffix//".sh" )
    call OpenFile( AnalFile, uidA, "write", "formatted" )
    !
    write(uidG,'(a)') "#!/bin/bash"
    write(uidG,'(a)') "#SBATCH --job-name="//GenFile
    write(uidA,'(a)') "#!/bin/bash"
    write(uidA,'(a)') "#SBATCH --job-name="//AnalFile
    !
    do i = 1, NumFiles
       !
       write(StrnI,* ) i
       allocate( File, source = "ScatteringScript_"//GenSuffix//"_"//trim(adjustl(StrnI))//".sh" )
       allocate( File2, source = "AnalysisScatteringScript_"//GenSuffix//"_"//trim(adjustl(StrnI))//".sh" )
       !
       call OpenFile( File, uid, "write", "formatted" )
       call OpenFile( File2, uid2, "write", "formatted" )
       !
       write(uid,'(a)') "#!/bin/bash"
       write(uid,'(a)') "#SBATCH --job-name="//File
       write(uid,'(a)') "#SBATCH --ntasks=32"
       write(uid,'(a)') "#SBATCH --nodes=1"
       write(uid,'(a)') "#SBATCH --partition=xchem"
       write(uid,'(a)') "#SBATCH --qos=normal"
       write(uid,'(a)') "#SBATCH --exclude=xchem69,xchem56,xchem61,xchem41,xchem49,xchem52"
       !
       write(uid2,'(a)') "#!/bin/bash"
       write(uid2,'(a)') "#SBATCH --job-name="//File2
       write(uid2,'(a)') "#SBATCH --ntasks=32"
       write(uid2,'(a)') "#SBATCH --nodes=1"
       write(uid2,'(a)') "#SBATCH --partition=xchem"
       write(uid2,'(a)') "#SBATCH --qos=normal"
       write(uid2,'(a)') "#SBATCH --exclude=xchem69,xchem56,xchem61,xchem41,xchem49,xchem52"
       !
       write(StrnNE,* ) NumEn(i)
       write(StrnEmin,* ) EminEmax(i,1)
       write(StrnEmax,* ) EminEmax(i,2)
       allocate( Suffix, source = "-suf "//trim(adjustl(StrnI)) )
       !
       write(uid,'(a)') "srun -n1 ./ComputeMultScattStates_2"//" "//&
            "-gif "//GeneralInputFile//" -pif "//ProgramInputFile//" -group "//GroupName//" -sym "//&
            SymmetryName//" -mult "//CurrentMultiplicity//" -ccfile "//CloseCouplingConfigFile//" -qcdir "//&
            NameDirQCResults//" "//UseExternalMonomials//" "//&
            ForceComputation//" -cm "//CompMethod//" -condm "//ModeStrn//" "//&
            "-NE"//" "//trim(adjustl(StrnNE))//" "//&
            "-Emin"//" "//trim(adjustl(StrnEmin))//" "//&
            "-Emax"//" "//trim(adjustl(StrnEmax))//" "//&
            Suffix
       !
       write(uid,'(a)') "echo 'terminada ejecución del programa'"
       !
       !
       allocate( NSFile, source = "NullSpace_"//CompMethod//"_"//ModeStrn//"_"//trim(adjustl(StrnI)) )
       !
       write(uid2,'(a)') "srun -n1 ./testMultichannelScattering_2"//" "//&
            "-gif "//GeneralInputFile//" -pif "//ProgramInputFile//" -group "//GroupName//" -sym "//&
            SymmetryName//" -mult "//CurrentMultiplicity//" -ccfile "//CloseCouplingConfigFile//" -qcdir "//&
            NameDirQCResults//" "//UseExternalMonomials//" "//&
            ForceComputation//" -gau "//Gauge//" -nsif "//NSFile
       !
       write(uid2,'(a)') "echo 'terminada ejecución del programa'"
       !
       !
       write(uidG,'(a)') "sbatch "//File
       write(uidG,'(a)') "sleep 10"
       write(uidA,'(a)') "sbatch "//File2
       write(uidA,'(a)') "sleep 10"
       !
       !
       close( uid )
       close( uid2)
       deallocate( File )
       deallocate( File2)
       deallocate( NSFile )
       deallocate( Suffix )
       StrnNE   = ' '
       StrnEmin = ' '
       StrnEmax = ' '
       StrnI    = ' '
       !
    end do
    !
    write(uidG,'(a)') "echo 'terminada ejecución del programa'"
    write(uidA,'(a)') "echo 'terminada ejecución del programa'"
    !
    close( uidG )
    deallocate( GenFile )
    close( uidA )
    deallocate( AnalFile )
    !
  end subroutine PrepareScripts




  subroutine OpenFile( FileName, uid, Purpose, Form )
    !> The name of the file to be opened.
    character(len=*),           intent(in)    :: FileName
    !> Unit number associated to the opened file.
    integer,                    intent(out)   :: uid
    !> Can be "write" or "read" depending on the action needed.
    character(len=*), optional, intent(in)    :: Purpose
    !> Can be "formatted" or "unformatted".
    character(len=*), optional, intent(in)    :: Form
    !
    integer :: iostat
    character(len=256) :: iomsg
    character(len=:), allocatable :: NewPurpose, NewForm
    !
    if ( .not.present(Purpose) ) then
       allocate( NewPurpose, source = "read" )
    else
       allocate( NewPurpose, source = Purpose )
    end if
    if ( .not.present(Form) ) then
       allocate( NewForm, source = "unformatted" )
    else
       allocate( NewForm, source = Form )
    end if
    !
    OPEN(NewUnit =  uid,          &
         File    =  FileName,     &
         Form    =  NewForm,      &
         Status  = "unknown",     &
         Action  = NewPurpose,    &
         iostat  = iostat ,       &
         iomsg   = iomsg    )
    if( iostat /= 0 ) write(*,*) iomsg
  end subroutine OpenFile




  subroutine BuildUniqueNullSpaceFile( Dir, NumQuests, NumEnergies )
    !
    character(len=*), intent(in) :: Dir
    integer,          intent(in) :: NumQuests
    integer,          intent(in) :: NumEnergies
    !
    integer :: i, j, k, n, uid, uidG, iostat, NumVec, NumElemVec
    character(len=:), allocatable :: File, FileG, FileTemp
    character(len=32) :: StrnI
    real(kind(1d0)) :: Energy
    complex(kind(1d0)) :: Val
    !
    write(*,*) "Preparing general Null Space ..."
    !
    allocate( FileG, source = Dir//"NullSpace_"//CompMethod//"_"//ModeStrn//"_"//GenSuffix )
    call OpenFile( FileG, uidG, "write", "formatted" )
    write(uidG,*) NumEnergies
    close( uidG )
    !
    allocate( FileTemp, source = FileG//"_"//"Temp" )
    !
    do i = 1, NumQuests
       !
       write(StrnI,* ) i
       allocate( File, source = FileG//"_"//trim(adjustl(StrnI)) )
       !
       write(*,*) "Copying "//File//" ..."
       call SYSTEM("sed '1d' "//File//" > "//FileTemp )
       call SYSTEM("cat "//FileTemp//" >> "//FileG )
       !
       deallocate( File )
       !
    end do
    !
    deallocate( FileG )
    deallocate( FileTemp )
    !
  end subroutine BuildUniqueNullSpaceFile



end program CreateScattScript
