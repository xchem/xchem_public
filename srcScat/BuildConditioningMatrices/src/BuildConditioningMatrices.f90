!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program computes the conditioning matrices, which after
!!  the proper contraction with the original basis, remove the
!!  linear dependencies.
!!
program BuildConditioningMatrices

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleString
  use ModuleMatrix
  use ModuleElectronicSpace
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleElementaryElectronicSpaceOperators

  implicit none


  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: InterfaceFileRootName = "interface"

  !.. Run time parameters
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  logical                       :: UsePerfectProjector

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod

  !
  type(ClassElectronicSpace)       :: Space
  type(ClassShortRangeSymOrbitals) :: SRSOrbSet
  type(ClassGroup), pointer        :: Group
  type(ClassIrrep), pointer        :: irrepv(:)
  integer                          :: iIrrep
  character(len=:), allocatable    :: InterfaceFileName
  logical :: exist



  !.. Read run-time parameters, among which there is the group and the symmetry
  call GetRunTimeParameters(      &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       UsePerfectProjector )
  !
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Program Input File  : "//ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(a)" ) "Close Coupling File : "//CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjector 



  call ParseProgramInputFile( &
       ProgramInputFile     , &
       StorageDir           , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  )       "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,i0)"  )     "NumBsDropBeginning...........", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)"  )     "NumBsDropEnding..............", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,e10.2)"  ) "ConditionBsThreshold.........", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  )       "ConditionBsMethod............"//ConditionBsMethod


  !..Initializes the electronic space.
  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  Group  => Space%GetGroup()
  irrepv => Group%GetIrrepList()
  !
  !..  Cycle over the irrep of the diffuse orbitals.
  do iIrrep = 1, size( irrepv )
     !
     if ( allocated(InterfaceFileName) ) deallocate( InterfaceFileName )
     allocate( InterfaceFileName, source = &
          AddSlash(StorageDir)//&
          AddSlash(NameDirQCResults)//&
          AddSlash(GetDiffuseOrbDir())//&
          InterfaceFileRootName//irrepv(iIrrep)%GetName() )
     !
     INQUIRE( file = InterfaceFileName, exist = exist )
     if( .not. exist )cycle
     !
     !.. Initialise short-range symmetric orbitals
     call SRSOrbSet%init( irrepv( iIrrep ), Space%GetStorageDir(), Space%GetLmax() )
     !
     if ( UsePerfectProjector ) then
        write(output_unit,*) 'Computing Gaussian monocentric overlap matrix for '//irrepv(iIrrep)%GetName()//' symmetry ...'
        call SRSOrbSet%ComputeAndSaveDiffMonOverlap( Space%GetStorageDir() )
     end if
     !
     call ComputeAndSaveConditioningMatrix(       &
          Space, irrepv(iIrrep)                 , &
          SRSOrbSet, UsePerfectProjector        , &
          NumBsDropBeginning, NumBsDropEnding   , &
          ConditionBsThreshold, ConditionBsMethod )
     !
  end do

  write(output_unit,'(a)') "Done"


contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       UsePerfectProjector       )
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    logical                      , intent(out)   :: UsePerfectProjector
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: pif, ccfile, qcdir
    character(len=:), allocatable :: AuxOp
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Computes the contitioning matrices to remove the "//&
         "linear dependencies from the original Close-Coupling basis."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-pp"     , "A perfect projector will be enforced in the conditioning" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-pif"    , pif          )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile       )
    call List%Get( "-esdir"  , qcdir        )
    UsePerfectProjector = List%Present( "-pp" )
    !
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    !
    call List%Free()
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "NumBsDropBeginning" , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile




  subroutine ComputeAndSaveConditioningMatrix( &
       Space, Irrep                          , &
       SRSOrbSet, UsePerfectProjector        , &
       NumBsDropBeginning, NumBsDropEnding   , &
       ConditionBsThreshold, ConditionBsMethod )
    Class(ClassElectronicSpace)      , intent(in) :: Space
    Class(ClassIrrep)                , intent(in) :: Irrep
    Class(ClassShortRangeSymOrbitals), intent(in) :: SRSOrbSet
    logical                          , intent(in) :: UsePerfectProjector
    integer                          , intent(in) :: NumBsDropBeginning
    integer                          , intent(in) :: NumBsDropEnding
    real(kind(1d0))                  , intent(in) :: ConditionBsThreshold
    character(len=*)                 , intent(in) :: ConditionBsMethod
    !
    type(ClassIrrep)              :: OpIrrep
    character(len=:), allocatable :: DummyStrn
    type(ClassXlmSymmetricSet)    :: XlmSymSet
    type(ClassXlm)                :: Xlm, OpXlm
    integer :: iXlm, l, m
    type(ClassDiffuseBsplineXlmBlock)    :: DBSBlock
    type(ClassBsplineXlmBsplineXlmBlock) :: BSBSBlock
    type(ClassMatrix) :: DBSMatrix, BSBSMatrix, ConditionerMat
    type(ClassConditionerBlock) :: Conditioner
    !
    OpXlm   = GetOperatorXlm( OverlapLabel, DummyStrn )
    OpIrrep = GetOperatorIrrep( Irrep%GetGroup(), OverlapLabel, DummyStrn )
    call XlmSymSet%init( SRSOrbSet%GetLmax(), Irrep * OpIrrep )
    !
    !
    do iXlm = 1, XlmSymSet%GetNXlm()
       !
       Xlm = XlmSymSet%GetXlm( iXlm )
       !
       call DBSBlock%init( Irrep, OpXlm, OverlapLabel, Xlm )
       call DBSBlock%ReadBlock( Space%GetStorageDir(), DBSMatrix )
       call DBSBlock%Free()
       !..Means than the block doesn't exist.
       if ( .not.DBSMatrix%IsInitialized() ) cycle
       !
       call BSBSBlock%init( Xlm, OpXlm, OverlapLabel, Xlm )
       call BSBSBlock%ReadBlock( Space%GetStorageDir(), BSBSMatrix )
       call BSBSBlock%Free()
       !..Means than the block doesn't exist.
       if ( .not.BSBSMatrix%IsInitialized() ) cycle
       !
       call Conditioner%Init(     &
            ConditionBsMethod   , &
            ConditionBsThreshold, &
            NumBsDropBeginning  , &
            NumBsDropEnding     , &
            Irrep, Xlm          , &
            UsePerfectProjector )
       call Conditioner%ComputeConditioner( Space%GetStorageDir(), SRSOrbSet, DBSMatrix, BSBSMatrix )
       !
       call DBSMatrix%Free()
       call BSBSMatrix%Free()
       !
       call Conditioner%Save( Space%GetStorageDir() )
       call Conditioner%Free()
       !
    end do
    !
  end subroutine ComputeAndSaveConditioningMatrix




end program BuildConditioningMatrices
