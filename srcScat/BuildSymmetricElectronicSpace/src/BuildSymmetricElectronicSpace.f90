!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program defines the close-coupling space for an assigned total symmetry.
!!  - define what a close-coupling space is
!!  - define partial wave channel
!!  - define loc channel
!!  - give crossreference to the GABS basis and the description of the BasicMatrixElement program that uses it
!!  - describe how general matrix elements are computed and provide explicit formulas for the overalp and the
!!    hamiltonian one-body and two-body matrix elements
!!  - also, must give a description of the data taken from QC interface
!!
program BuildSymmetricElectronicSpace

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleString
  use ModuleMatrix
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleGeneralInputFile
  use ModuleAbsorptionPotential


  implicit none


  logical        , parameter :: SET_FORMATTED_WRITE = .TRUE.


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  character(len=:), allocatable :: Gauge
  character(len=:), allocatable :: DipoleAxis
  logical                       :: ForceComputation
  character(len=:), allocatable :: BraLocDir
  character(len=:), allocatable :: KetLocDir
  logical                       :: IsBraLocChannel
  logical                       :: IsKetLocChannel
  logical                       :: PWCOnly
  logical                       :: LocSRCOnly


  !.. Config file parameters
  character(len=:), allocatable :: StorageDir

  !
  type(ClassSESSESBlock)     :: OperatorMat
  type(ClassAbsorptionPotential):: AbsPot
  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  character(len=:), allocatable :: NewOperatorLabel
  integer :: i, NComp
  !


  !.. Read run-time parameters, among which there is the group and the symmetry
  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &   
       OperatorLabel          ,   &
       BraSymLabel            ,   &
       KetSymLabel            ,   &
       Gauge                  ,   &
       DipoleAxis             ,   &
       ForceComputation       ,   &
       PWCOnly                ,   &
       LocSRCOnly)
  !
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "SAE General Input File  : "//GeneralInputFile
  write(OUTPUT_UNIT,"(a)" ) "Program Input File  : "//ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(a)" ) "Close Coupling File : "//CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  write(OUTPUT_UNIT,"(a)" ) "Operator Name       : "//OperatorLabel
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  if ( allocated(Gauge) ) then
     write(OUTPUT_UNIT,"(a)" ) "Gauge        : "//Gauge
  end if
  if ( allocated(DipoleAxis) ) then
     write(OUTPUT_UNIT,"(a)" ) "DipoleAxis        : "//DipoleAxis
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Force Computation :     ", ForceComputation 



  call ParseProgramInputFile( ProgramInputFile, &
       StorageDir           )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir


  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OperatorLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )

  BraLocDir = AddSlash(StorageDir)//AddSlash(NameDirQCResults)//AddSlash("CloseCoupling")//&
    AddSlash( trim(BraSymSpace%GetLabel())//"_"//trim(BraSymSpace%GetLabel() ))//AddSlash("Loc_Loc")//"S_0.0QC"
  KetLocDir = AddSlash(StorageDir)//AddSlash(NameDirQCResults)//AddSlash("CloseCoupling")//&
    AddSlash( trim(KetSymSpace%GetLabel())//"_"//trim(KetSymSpace%GetLabel() ))//AddSlash("Loc_Loc")//"S_0.0QC"
  inquire( file = BraLocDir, exist=IsBraLocChannel)
  inquire( file = KetLocDir, exist=IsKetLocChannel)
  if ( .not. IsBraLocChannel ) then
    call Assert("No local channel found for bra symmetry. If this is unexpected review ConvertQCI", assertion%LEVEL_WARNING)
  endif
  if ( .not. IsKetLocChannel ) then
    call Assert("No local channel found for ket symmetry. If this is unexpected review ConvertQCI", assertion%LEVEL_WARNING)
  endif

  if ( OperatorLabel .is. CAPLabel ) then
     !.. Load the absorption potential parameters
     call SAEGeneralInputData%ParseFile( GeneralInputFile )
     call AbsPot%Parse( AbsorptionPotentialFile )
     NComp = AbsPot%NumberOfTerms()
  else
     NComp = 1
  end if


  if ( PWCOnly .and. LocSRCOnly) then
    call Assert("It makes no sense to set PWCOnly and LocSRCOnly")
  endif
  do i = 1, NComp
     !
     if ( OperatorLabel .is. CAPLabel ) then
        allocate ( NewOperatorLabel, source = GetFullCAPLabel(i) )
     else
        allocate ( NewOperatorLabel, source = OperatorLabel )
     end if
     !
     if ( SET_FORMATTED_WRITE) call OperatorMat%SetFormattedWrite()
!!$     call OperatorMat%Init(          &
!!$          BraSymSpace, KetSymSpace , &
!!$          NewOperatorLabel, DipoleAxis, & 
!!$          ForceComputation )
!!$     deallocate( NewOperatorLabel )
!!$     call OperatorMat%Save( )
     !
     !.. This way it saves every block at running time instead
     ! of initializing all the blocks and save it at the end,
     ! which could provoke the program to run out of memory if
     ! the close-coupling space is not small.
     if (PWCOnly) then
        write(output_unit,'(a)') "Consider only blocks with at least one PWC"
        call OperatorMat%InitAndSavePWCOnly(      &
             BraSymSpace, KetSymSpace    , &
             NewOperatorLabel, DipoleAxis, & 
             ForceComputation, &
             IsBraLocChannel = IsBraLocChannel, &
             IsKetLocChannel = IsKetLocChannel)
     elseif (LocSRCOnly) then
        write(output_unit,'(a)') "Consider only blocks without PWCs"
        call OperatorMat%InitAndSaveLocSRCOnly(      &
             BraSymSpace, KetSymSpace    , &
             NewOperatorLabel, DipoleAxis, & 
             ForceComputation, &
             IsBraLocChannel = IsBraLocChannel, &
             IsKetLocChannel = IsKetLocChannel)
     else
        write(output_unit,'(a)') "Consider all blocks"
        call OperatorMat%InitAndSave(      &
             BraSymSpace, KetSymSpace    , &
             NewOperatorLabel, DipoleAxis, & 
             ForceComputation, &
             IsBraLocChannel = IsBraLocChannel, &
             IsKetLocChannel = IsKetLocChannel)
     endif
     deallocate( NewOperatorLabel )
     !
  end do
  
  write(output_unit,'(a)') "Done"


contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters(      &
       GeneralInputFile             ,   &
       ProgramInputFile             ,   &
       Multiplicity                 ,   &
       CloseCouplingConfigFile      ,   &
       NameDirQCResults             ,   &   
       OperatorLabel                ,   &
       BraSymLabel                  ,   &
       KetSymLabel                  ,   &
       Gauge                        ,   &
       DipoleAxis                   ,   &
       ForceComputation             ,   &
       PWCOnly                      ,   &
       LocSRCOnly)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(out)   :: OperatorLabel
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    character(len=:), allocatable, intent(inout) :: Gauge
    character(len=:), allocatable, intent(inout) :: DipoleAxis
    logical,                       intent(out)   :: ForceComputation
    logical,                       intent(out)   :: PWCOnly
    logical,                       intent(out)   :: LocSRCOnly
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: gif, pif, ccfile, qcdir, op, brasym, ketsym, gau, axis
    character(len=:), allocatable :: AuxOp
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Build the selected operator matrix, in the "//&
         "Close-Coupling basis."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-op"     , "Name of the Operator",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-gau"    , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-axis"   , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-force"  , "Force Computation" )
    call List%Add( "-PWCOnly"  , "Only include blocks with at least one PWC" )
    call List%Add( "-LocSRCOnly"  , "Only include blocks without PWCs" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif"    , gif          )
    call List%Get( "-pif"    , pif          )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile       )
    call List%Get( "-esdir"  , qcdir        )
    call List%Get( "-op"     , op           )
    !
    allocate( GeneralInputFile       , source = trim( gif )    )
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    allocate( OperatorLabel          , source = trim( op )     )
    !
    call CheckOperator( OperatorLabel )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    if(List%Present("-gau"))then
       call List%Get( "-gau", gau )
       allocate( Gauge, source = trim(gau) )
       call CheckGauge( Gauge )
    end if
    if(List%Present("-axis"))then
       call List%Get( "-axis", axis )
       allocate( DipoleAxis, source = trim(axis) )
       call CheckAxis( DipoleAxis )
    end if
    ForceComputation = List%Present( "-force" )
    PWCOnly = List%Present( "-PWCOnly" )
    LocSRCOnly = List%Present( "-LocSRCOnly" )
    !
    call List%Free()
    !
    !..Check dipole
    if ( OperatorLabel .is. DipoleLabel ) then
       if ( .not.allocated(Gauge) ) call Assert( 'For '//DipoleLabel//' operator the gauge must be present.' )
       if ( .not.allocated(DipoleAxis) ) call Assert( 'For '//DipoleLabel//' operator the orientation must be present.' )
       allocate( AuxOp, source = OperatorLabel//Gauge )
       deallocate( OperatorLabel )
       allocate( OperatorLabel, source = AuxOp )
       deallocate( AuxOp )
    end if
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile, &
       StorageDir )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*) , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/"     , "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"        , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile




end program BuildSymmetricElectronicSpace
