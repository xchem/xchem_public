
#.. Compile shared files
#..
gen_of77=$(foreach a,$(gen_ff77),$(SHARE)/$(OBJ)/$(a).o)
gen_of90=$(foreach a,$(gen_ff90),$(SHARE)/$(OBJ)/$(a).o)
$(gen_of77) : $(SHARE)/$(OBJ)/%.o : $(SHARE)/src/%.f
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(SHARE)/$(MOD) $< -o $@
$(gen_of90) : $(SHARE)/$(OBJ)/%.o : $(SHARE)/src/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(SHARE)/$(MOD) $< -o $@

#.. administrative
%/:
	mkdir -p $@

#.. Compile local files
#..
loc_of90=$(foreach a,$(loc_ff90),$(OBJ)/$(a).o)
$(loc_of90) : $(OBJ)/%.o : src/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(MOD) $(MOD_OPTION) $(SHARE)/$(MOD) $< -o $@

loc_oc  =$(foreach a,$(loc_fc),$(OBJ)/$(a).o)
$(loc_oc) : $(OBJ)/%.o : src/%.c
	$(CC) $(CC_OPTS) $< -o $@

#.. Link
#..
$(BIN)/$(PRG_NAME) : $(loc_of90) $(gen_of77) $(gen_of90) $(loc_oc)
	$(FC) $(LINK_OPTS) -o $(BIN)/$(PRG_NAME) $(loc_of90) $(gen_of77) $(gen_of90) $(loc_oc) 

#.. Clean
#..
clean :
	rm -f $(OBJ)/* $(MOD)/*


#.. Dependencies between shared files
#.. 
$(SHARE)/$(OBJ)/ModuleMatrix.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                                 $(SHARE)/$(OBJ)/ModuleHDF5.o \
	                         $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleRepresentation.o : $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                 $(SHARE)/$(OBJ)/ModuleMatrix.o 
$(SHARE)/$(OBJ)/ModuleBasis.o : $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                        $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                        $(SHARE)/$(OBJ)/ModuleString.o \
	                        $(SHARE)/$(OBJ)/ModuleGaussianBSpline.o \
	                        $(SHARE)/$(OBJ)/ModuleGaussian.o \
	                        $(SHARE)/$(OBJ)/ModuleParameterList.o \
	                        $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                        $(SHARE)/$(OBJ)/ModuleBSpline.o 
$(SHARE)/$(OBJ)/ModuleBSpline.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                          $(SHARE)/$(OBJ)/ModuleParameterList.o\
	                        $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                          $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleSimpleLapack.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                               $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleGaussian.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                           $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                           $(SHARE)/$(OBJ)/ModuleSimpleLapack.o \
	                           $(SHARE)/$(OBJ)/ModuleParameterList.o 
$(SHARE)/$(OBJ)/ModuleGaussianBSpline.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                                  $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                  $(SHARE)/$(OBJ)/ModuleSimpleLapack.o \
	                                  $(SHARE)/$(OBJ)/ModuleGaussian.o \
	                                  $(SHARE)/$(OBJ)/ModuleBSpline.o \
	                                  $(SHARE)/$(OBJ)/ModuleString.o \
	                                  $(SHARE)/$(OBJ)/ModuleParameterList.o 
$(SHARE)/$(OBJ)/ModuleGeneralInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModuleScatteringStatesInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModulePropagationInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o \
                                               $(SHARE)/$(OBJ)/ModuleConstants.o
$(SHARE)/$(OBJ)/ModuleAnalPropResInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModuleParameterList.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o
$(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o : $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                      $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                                      $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                      $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                                      $(SHARE)/$(OBJ)/ModuleRepresentation.o 

$(SHARE)/$(OBJ)/ModuleBase.o    : $(SHARE)/$(OBJ)/parser.o
$(SHARE)/$(OBJ)/ModuleAbsorptionPotential.o   : $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                        $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
						$(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                        $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                                        $(SHARE)/$(OBJ)/ModuleString.o \
						$(SHARE)/$(OBJ)/ModuleRepresentation.o
$(SHARE)/$(OBJ)/specfun.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o 

$(SHARE)/$(OBJ)/ModuleDiagonalize.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o 

$(SHARE)/$(OBJ)/ModuleScatteringStates.o: $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                                          $(SHARE)/$(OBJ)/ModuleString.o \
                                          $(SHARE)/$(OBJ)/ModuleConstants.o \
                                          $(SHARE)/$(OBJ)/ModuleDiagonalize.o \
                                          $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
                                          $(SHARE)/$(OBJ)/ModuleIO.o

$(SHARE)/$(OBJ)/ModuleElectronicSpace.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeOrbitals.o \
                            $(SHARE)/$(OBJ)/ModuleString.o 

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicChannel.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o

$(SHARE)/$(OBJ)/ModuleGroups.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o 



$(SHARE)/$(OBJ)/ModuleParentIons.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o

$(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicChannel.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o

$(SHARE)/$(OBJ)/ModuleIO.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o

$(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/symba.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o

$(SHARE)/$(OBJ)/ModuleShortRangeOrbitals.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/symba.o 

$(SHARE)/$(OBJ)/ModuleShortRangeChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeOrbitals.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/symba.o

$(SHARE)/$(OBJ)/ModulePartialWaveChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/symba.o \
	                    $(SHARE)/$(OBJ)/ModuleIO.o

$(SHARE)/$(OBJ)/ModuleElectronicSpaceOperators.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleElementaryElectronicSpaceOperators.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpaceOperators.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o \
                            $(SHARE)/$(OBJ)/ModuleElectronicSpace.o

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpaceOperators.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeOrbitals.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o \
                            $(SHARE)/$(OBJ)/ModuleElementaryElectronicSpaceOperators.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleElectronicSpace.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o

$(SHARE)/$(OBJ)/ModuleElementaryElectronicSpaceOperators.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/symba.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeOrbitals.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o \
                            $(SHARE)/$(OBJ)/ModuleElectronicSpace.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpaceSpectralMethods.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o 	\
                            $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleIO.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/specfun.o \
                            $(SHARE)/$(OBJ)/ModuleElementaryElectronicSpaceOperators.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o
