!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! {{{ Detailed description
!!  Author : Luca Argenti
!!           University of Central Florida, 2017
!!
!> \mainpage Program ComputePhotoelectronSpectrum.
!! 
!! Synopsis:
!! ---------
!!
!! ___
!! Description (applicable to atoms only):
!! ------------
!!
! }}}
program ProgramComputeScatteringStates

  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING

  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleString
  use ModuleIO

  use ModuleElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleParentIons

  use ModuleConstants
  use ModuleDiagonalize
  use ModuleScatteringStates

  implicit none

  enum, bind(c)
     enumerator :: MODE_PRINT_INFO
     enumerator :: MODE_UNIFORM
     enumerator :: MODE_RYDBERG
     enumerator :: MODE_RESOLVE
     enumerator :: MODE_REFINE 
  end enum

  !.. Run-time parameters
  !..
  character(len=:), allocatable :: ProgramInputFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: CloseCouplingConfigFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: SymLabel
  character(len=:), allocatable :: PsiFile
  character(len=:), allocatable :: OutDipFile
  real(kind(1d0))               :: Time
  character(len=:), allocatable :: OutDir

  character(len=:), allocatable :: StorageDir
  character(len=:), allocatable :: scatLabel
  type(ClassElectronicSpace)    :: Space
  character(len=:), allocatable :: SymStorageDir
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace

  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: nChannels

  integer :: i, ich, ipwc, lw, iBox
  integer :: uid, iBuf
  real(kind(1d0))               :: IonCharge

  real   (kind(1d0)), allocatable :: PWCThr(:)
  integer           , allocatable :: PWClan(:)

  !> Linked-list with the scattering states at several energies
  type(ClassScatteringStateList)          :: ScatStateList
  type(ClassScatteringState)    , pointer :: ScatState

  complex(kind(1d0)), allocatable :: zPsi(:)
  complex(kind(1d0)), allocatable :: zAmp(:,:)

  character(len=:), allocatable :: ccscat_dir
  integer                      :: iEn
  character(len=:),allocatable :: ChanName
  real(kind(1d0))              :: Energy, Rmax, RealTime
  real(kind(1d0))              :: IonEnergy, ElectronEnergy
  real(kind(1d0)), allocatable :: EnergyGrid(:)
  real(kind(1d0)), allocatable :: ThresholdList(:)
  integer        , allocatable :: nThresholdChannels(:), ThresholdChannelList(:,:)
  integer                      :: nThresholds,  iThr, nBoxStates
  integer                      :: iOpen, iOpenPwc, nEnergies, nOpen
  integer        , allocatable :: listOpen(:), vnOpen(:)
  integer        , allocatable :: listOpenPI(:)
  real(kind(1d0)), allocatable :: Egrid(:)
  integer, parameter           :: J_MAX = 100
  complex(kind(1d0))           :: zB(-J_MAX:J_MAX,0:J_MAX)

  character(len=1000) :: sBuffer, FileName
  integer                      :: iOpenPIKet, iOpenPIBra, lket, lbra, jmax, ichBra, ichKet, iOpenPI
  integer                      :: j, mu, mbra, mket, iOpenBraConjg, iOpenKetConjg, iOpenBra, iOpenKet
  logical                      :: BraConjgIsPresent, KetConjgIsPresent
  real(kind(1d0))              :: F0, F1, F2, sigmaKet, sigmaBra, PICharge
  complex(kind(1d0))           :: zF3, zF4, zF5, zF6, tmpzF4
  integer                      :: BoxStateIndex
  logical                      :: Dipole

  !.. External functions
  real(kind(1d0)), external :: CoulombPhase 

  call GetRunTimeParameters(    &
              ProgramInputFile, &
              NameDirQCResults, &
       CloseCouplingConfigFile, &
              Multiplicity    , &
              SymLabel        , &
              PsiFile         , &
              OutDipFile      , &
              Time            , &
              BoxStateIndex   , &
              Dipole          , &
              scatLabel       , &
              OutDir          )


  call ParseProgramInputFile( ProgramInputFile, StorageDir )
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  call GetSymmetricElectronicSpace( Space, SymLabel, SymSpace )
  call SymSpace.GetThresholdSequence( &
       nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList )
  allocate( SymStorageDir, source = GetBlockStorageDir( SymSpace, SymSpace ) )
  allocate(ccscat_dir, source = SymStorageDir//"/ScatteringStates/SPEC/")
  PICharge = SymSpace.GetPICharge()

  call ScatStateList%Init(ccscat_dir)

  !.. Reconstruct the list of thresholds and print it on screen
  call ScatStateList.Load( scatLabel )
  call ScatStateList%GetEnergyList( Egrid )
  nEnergies  = size(Egrid,1)
  call ScatStateList%GetnOpenList(  vnOpen )
!  call ScatStateList%GetPsiMinus( 1, 1, zPsi )
  call ScatStateList%GetNBoxStates(nBoxStates)
  allocate(zPsi( nBoxStates ))
  write(OUTPUT_UNIT,'(a,i0)') 'The Number of BoxStates is : ',nBoxStates
  zPsi = Z0

  RealTime = Time
  
  if (Dipole) then
    call LoadPsi( SymStorageDir//'BoxStates/'//PsiFile, RealTime, zPsi, iBox = BoxStateIndex  ) 
  else  
    call LoadPsi( PsiFile, RealTime, zPsi ) 
  endif

  !.. Compute the partial-wave amplitudes (PWA)
  zPsi=conjg(zPsi)
  call ScatStateList%ProjectOnBra(zPsi,zAmp)
  zAmp=conjg(zAmp)

  !.. Save PWA to disk
  if (Dipole) then
    call Execute_Command_Line(" mkdir -p "//OutDir)
    call SavePWA( zAmp, vnOpen, Egrid, OutDir, OutDipFile )
  else
    call SavePWA( zAmp, vnOpen, Egrid, OutDir )
  endif


  !.. Computes the Molecular-Frame Photoelectron Angular Distribution (MFPAD)
  !>
  !! \f[
  !!      \frac{dP_a}{dE d\Omega} = \sum_{j\mu} Y_{j\mu}(\Omega) B_{j\mu; a}(E)
  !! \f]
  !! \f[
  !!      B_{j\mu;a}(E) = & (2S_0+1) \sum_{\ell\ell'}\sqrt{\frac{(2j+1)(2\ell'+1)}{4\pi(2\ell+1)}} \times \\
  !!               \times & e^{i\sigma_\ell-i\ell\pi/2 - i\sigma_{\ell'}+i\ell'\pi/2} C_{j0, \ell'0}^{\ell 0} \times \\
  !!               \times & \sum_{mm'} C_{j\mu,\ell'm'}^{\ell m}
  !!                        [ A_{a\ell m E} +sgn(m) A_{a\ell-mE} ] [ A^*_{a\ell' m' E} +sgn(m') A^*_{a\ell'-m'E} ] 
  !! \f]
  !<
  do iThr = 1, nThresholds

     !*** UGLY
     sBuffer=SymSpace%GetPWCLabel(ThresholdChannelList(1,iThr))
     i=index(sBuffer,"X")
     sBuffer(i:)=" "
     if (Dipole) then
       FileName=trim(adjustl(OutDir))//"/"//trim(OutDipFile)//"mfpad_"//trim(adjustl(sBuffer))
     else
       FileName=trim(adjustl(OutDir))//"/mfpad_"//trim(adjustl(sBuffer))
     endif
     !***
     open(newunit=uid        , &
          file=trim(FileName), &
          form="formatted"   , &
          status="unknown"   )
     
     do iEn = 1, size( Egrid )

        zB = Z0

        Energy =  Egrid( iEn )
        if(Energy < ThresholdList( iThr )) cycle

        call GetOpenChannels(       &
             nThresholds          , &
             ThresholdList        , &
             nThresholdChannels   , &
             ThresholdChannelList , &
             Energy, nOpen, listOpen, iThr, listOpenPI )

        !.. Compute maximum j for the MFPAD
        jmax=0
        do iOpenPiBra = 1, nThresholdChannels( iThr )
           ichBra = listOpen( listOpenPI( iOpenPiBra ) ) 
           lbra = SymSpace%GetChannelL ( ichBra )
           jmax=max(jmax,2*lbra)
        enddo

        do j = 0, jmax

           do mu = 0, j
              do lBra = 0,int(jmax/2)
                 do mBra = -lBra,lBra
                    tmpzF4 = Z0
                    do iOpenPiBra = 1, nThresholdChannels( iThr )
                       iOpenBra = listOpenPI( iOpenPiBra )
                       ichBra   = listOpen( iOpenBra   )
                       if (lBra .eq. SymSpace%GetChannelL ( ichBra ) .and. mBra .eq. SymSpace%GetChannelM ( ichBra ) ) then
                          tmpzF4 = zAmp( iEn, iOpenBra )
                          exit
                       endif
                    enddo                
                    do i=1, nThresholdChannels( iThr )
                       BraConjgIsPresent = &
                            SymSpace%GetChannelL ( listOpen( listOpenPI(i) ) ) ==  lbra .and. &
                            SymSpace%GetChannelM ( listOpen( listOpenPI(i) ) ) == -mbra 
                       if(BraConjgIsPresent)then 
                          iOpenBraConjg = listOpenPI(i)
                          exit
                       endif
                    enddo
                    do lKet =  0,int(jmax/2)
                       do mKet = -lKet,lKet
                          zF5 = Z0
                          zF4 = tmpzF4
                          do iOpenPiKet = 1, nThresholdChannels( iThr )
                             iOpenKet = listOpenPI( iOpenPiKet )
                             !
                             ichKet = listOpen( iOpenKet ) 
                             if (lKet .eq. SymSpace%GetChannelL ( ichKet ) .and. mKet .eq. SymSpace%GetChannelM ( ichKet ) ) then
                                 zF5 = zAmp( iEn, iOpenKet )
                                 exit
                             endif
                          enddo
                          do i=1, nThresholdChannels( iThr )
                             KetConjgIsPresent = &
                                  SymSpace%GetChannelL ( listOpen( listOpenPI(i) ) ) ==  lket .and. &
                                  SymSpace%GetChannelM ( listOpen( listOpenPI(i) ) ) == -mket 
                             if(KetConjgIsPresent)then 
                                iOpenKetConjg = listOpenPI (i)
                                exit
                             endif
                          enddo

                          if( mBra   /=  mu + mKet   ) cycle
                          if( j >      lBra + lKet   ) cycle
                          if( j < abs( lBra - lKet ) ) cycle
                          F0 = dble(Multiplicity)
                          F1 = sqrt( dble( (2*j+1)*(2*lket+1) ) / (4*PI * (2*lbra+1) ) )
                          F2 = CGC( j, lket, lbra, 0, 0 ) * CGC( j, lket, lbra, mu, mKet )
                          zF3 = conjg( zsgn( mBra ) ) * zsgn( mKet ) / 2.d0 
                          if( mBra == 0 ) zF3 = zF3 * sqrt(2.d0)
                          if( mKet == 0 ) zF3 = zF3 * sqrt(2.d0)
                          if( BraConjgIsPresent .and. mBra /= 0 ) zF4 = zF4 - Zi * zAmp( iEn, iOpenBraConjg )
                          if( KetConjgIsPresent .and. mKet /= 0 ) zF5 = zF5 - Zi * zAmp( iEn, iOpenKetConjg )
                          zF5 = conjg(zF5)
                          !*** MUST ASSIGN PICHARGE
                          sigmaBra = CoulombPhase( lBra, PICharge, Energy - ThresholdList( iThr ) )
                          sigmaKet = CoulombPhase( lKet, PICharge, Energy - ThresholdList( iThr ) )
                          zF6 = exp( Zi * ( PI / 2.d0 * dble( lKet - lBra ) + sigmaBra - sigmaKet ) )
                          zB( mu, j ) = zB( mu, j ) + F0 * F1 * F2 * zF3 * zF4 * zF5 * zF6
                       enddo
                    enddo
                 enddo
              enddo

              zB(-mu,j) = dpar(mu) * conjg(zB(mu,j))
              
           enddo

        enddo

        write(uid,"(e24.16,x,i6,*(x,e24.16))") Energy, jmax, ( ( zB(mu,j), mu = -j, j ), j = 0, jmax )

     enddo

     close(uid)

  enddo

  stop

contains
  
  real(kind(1d0)) function dpar( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(mod(abs(i),2)==1)res=-1.d0
  end function dpar

  complex(kind(1d0)) function zsgn( i ) result( res )
    !.. Returns 1 if i>=0 and -i * (-1)^{i} if i<0
    integer, intent(in) :: i
    res=Z1
    if(i>=0)return
    res=-Zi
    if(mod(abs(i),2)/=0)res=-res
  end function zsgn

  real(kind(1d0)) function dsgn( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(i<0)res=-1.d0
  end function dsgn

  real(kind(1d0)) function dsgnpow( i ) result( res )
    integer, intent(in) :: i
    res=1.d0
    if(i<0.and.mod(abs(i),2)==1)res=-1.d0
  end function dsgnpow

  function UniformGrid(xmi,xma,n) result(grid)
    real(kind(1d0)), intent(in) :: xmi, xma
    integer        , intent(in) :: n
    real(kind(1d0)), allocatable :: grid(:)
    integer :: i
    allocate(grid,source=[(xmi+(xma-xmi)/dble(n-1)*dble(i-1),i=1,n)])
  end function UniformGrid


  !> Reads the run time parameters specified in the command line.
  !! If an optional inherently positive parameters is not specified
  !! on the command line, it is initialized to a negative number
  !! to signal that it should not be used to evaluate the energy grid.
  !<
  subroutine GetRunTimeParameters( &
       ProgramInputFile, &
       NameDirQCResults, &
       ConfigFile      , &
       Multiplicity    , &
       SymLabel        , &
       PsiFile         , &
       OutDipFile      , &
       Time            , &
       BoxStateIndex   , &
       Dipole          , &
       ScatLabel       , &
       OutDir          )
    !
    use ModuleErrorHandling
    use ModuleCommandLineParameterList
    use ModuleString    
    implicit none
    !
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    character(len=:), allocatable, intent(out) :: ConfigFile
    character(len=:), allocatable, intent(out) :: scatLabel
    logical                      , intent(out) :: Dipole
    integer                      , intent(out) :: Multiplicity
    integer                      , intent(out) :: BoxStateIndex
    character(len=:), allocatable, intent(out) :: SymLabel
    character(len=:), allocatable, intent(out) :: PsiFile
    character(len=:), allocatable, intent(out) :: OutDipFile
    real(kind(1d0)) ,              intent(out) :: Time
    character(len=:), allocatable, intent(out) :: OutDir
    character(len=:), allocatable              :: Gauge
    character(len=:), allocatable              :: DipoleAxis
    !
    type( ClassCommandLineParameterList ) :: List
    !
    character(len=*), parameter :: PROGRAM_DESCRIPTION=&
         "Compute multichannel scattering states"
    character(len=512) :: strnBuf, pif
    !
    call List.SetDescription(PROGRAM_DESCRIPTION)
    call List.Add( "--help" , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " "    , "required" )
    call List.Add( "-label" , "subdir label" , ""            , "optional" )
    call List.Add( "-ccfile"  , "Config File"  , "CCFile"      , "optional" )
    call List%Add( "-boxi"  , "Box state index", 1           , "optional" )
    call List%Add( "-gau"   , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-axis"  , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-mult"  , "Total Multiplicity", 1        , "required" )
    call List.Add( "-sym"   , "SymLabel"         , "A"       , "required" )
    call List.Add( "-psi"   , "WaveFunction File", "coef.out", "optional" )
    call List.Add( "-t"     , "Projection Time"  ,  0.d0     , "optional" )
    call List.Add( "-od"    , "OutDir"           , "out"     , "required" )
    call List.Add( "-scatLabel" , "label of the set of scattering states"  ,  "1", "optional" )
    !
    call List.Parse()
    !
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if

    if (List.Present("-axis") .and. List.Present("-gau") ) then
      Dipole = .TRUE.
    elseif (List.Present("-axis") .or. List.Present("-gau") ) then 
      call Assert('Both (-gau and -axis) have to be specified for calculating the Dipole Tranistion Amplitudes')
    else
      Dipole = .FALSE.
    endif

    call List%Get( "-gau"    , strnBuf )
    allocate( Gauge          , source = trim( strnBuf )    )
    call List%Get( "-axis"   , strnBuf )
    allocate( DipoleAxis             , source = trim( strnBuf )   )
    call List.Get( "-scatLabel",  strnBuf  )
    allocate(scatLabel,source=trim(adjustl(strnBuf)))
    call List%Get( "-boxi"   , BoxStateIndex )
    call List%Get( "-pif"    , pif  )
    allocate( ProgramInputFile       , source = trim( pif )    )
    call List.Get( "-label",  strnBuf  )
    allocate(NameDirQCResults,source=trim(adjustl(strnBuf)))
    call List.Get( "-ccfile",  strnBuf  )
    allocate(ConfigFile,source=trim(adjustl(strnBuf)))
    call List%Get( "-mult"   , Multiplicity )
    call List.Get( "-sym"  ,  strnBuf  )
    allocate( SymLabel, source=trim(adjustl(strnBuf)))
    call List.Get( "-psi"  ,  strnBuf  )
    allocate( PsiFile, source=trim(adjustl(strnBuf)))
    call List.Get( "-t",  Time  )
    call List.Get( "-od",  strnBuf  )
    allocate( OutDir, source=trim(adjustl(strnBuf)))
    !
    call List.Free()

    if (Dipole) then
      deallocate(PsiFile)
      if (trim(Gauge) .eq. 'l') then
        allocate( PsiFile, source=trim('Dipole'//'Len_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Len_'//trim(DipoleAxis)//'_FromBoxState_'),BoxStateIndex
        allocate( OutDipFile, source=trim(strnBuf))
      elseif (trim(Gauge) .eq. 'v' ) then    
        allocate( PsiFile, source=trim('Dipole'//'Vel_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Vel_'//trim(DipoleAxis)//'_FromBoxState_'),BoxStateIndex
        allocate( OutDipFile, source=trim(strnBuf))
      else
        call Assert('The Dipole gauge is not well specified')
      endif
    endif
    !.. Must add check on the input
    !
    write(OUTPUT_UNIT,"(a)" ) "Run time parameters :"
    write(OUTPUT_UNIT,"(a)" ) "Subdir label    : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)" ) "CC Config File  : "//ConfigFile
    write(OUTPUT_UNIT,"(a)" ) "Spectral SymLabel   : "//SymLabel
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile

  subroutine LoadPsi( PsiFile, RealTime, zPsi, iBox )
    character(len=*)  , intent(in)    :: PsiFile
    real(kind(1d0))   , intent(inout) :: RealTime
    complex(kind(1d0)), intent(inout) :: zPsi(:)
    integer, optional , intent(in)    :: iBox
    integer  :: uid, iostat, iTime, i, j
    real(kind(1d0)) :: t, norm, val

    open( newunit=uid, File=PsiFile, form="formatted", status="old" )
    if (present(iBox) ) then
      read(uid,*) t
      do i = 1,iBox-1
        do j = 1,t
          read(uid,*)
        enddo
      enddo
        do j = 1,t
          read(uid,*) val
          zPsi(j) = (1.d0,0)*val
        enddo
    else
      itime=0
      do
         read(uid,*,iostat=iostat) t
         if(iostat/=0)exit
         if(t>RealTime)exit
         itime=itime+1
      enddo
      rewind(uid)
      do i=1,itime-1
         read(uid,*)
      enddo
      read(uid,*,iostat=iostat) RealTime, norm, (zPsi(i),i=1,size(zPsi,1))
      if(iostat/=0) zPsi(i)=Z0
    endif
    close(uid)

  end subroutine LoadPsi


  subroutine SavePWA( zAmp, vnOpen, Egrid, OutDir, fname )
    complex(kind(1d0)), intent(in) :: zAmp(:,:)
    integer           , intent(in) :: vnOpen(:)
    real(kind(1d0))   , intent(in) :: Egrid(:)
    character(len=*)  , intent(in) :: OutDir
    character(len=*) , optional , intent(in) :: fname

    integer  :: uid, iostat, iEn, nOpen, ich
    if (present(fname) ) then
      open( newunit=uid, File=trim(OutDir)//'/'//trim(fname), form="formatted", status="replace" )
      print*, trim(OutDir)//trim(fname)
    else
      open( newunit=uid, File=trim(OutDir)//'zAmp', form="formatted", status="replace" )
    endif
    write(uid,*)"# ",size(Egrid)
    do iEn=1,size(Egrid)
       nOpen=vnOpen(iEn)
       write(uid,"(e24.16,x,i6,*(x,e24.16))") Egrid(iEn), nOpen, (zAmp(iEn,ich),ich=1,nOpen)
    enddo
    close(uid)

  end subroutine SavePWA

  subroutine getopenchannels( nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList , &
       E, nOpen, listOpen, nPI, listOpenPI )

    integer             , intent(in) :: nThresholds
    real(kind(1d0))     , intent(in) :: ThresholdList(:)
    integer             , intent(in) :: nThresholdChannels(:)
    integer             , intent(in) :: ThresholdChannelList(:,:)
    real(kind(1d0))     , intent(in) :: E
    integer             , intent(out):: nOpen
    integer, allocatable, intent(inout):: listOpen(:)
    integer             , intent(in)   :: nPI
    integer, allocatable, intent(inout):: listOpenPI(:)

    integer :: ithr, ich, iOpen

    nOpen=0
    do ithr = 1, nThresholds
       if( ThresholdList(ithr) <= E ) nOpen = nOpen + nThresholdChannels(ithr)
    enddo
    call realloc(listOpen,nOpen)
    call realloc(listOpenPI,nThresholdChannels(nPI))
    listOpenPI=0

    iOpen=0
    iOpenPI=0
    do ithr = 1, nThresholds
       if( ThresholdList(ithr) > E )exit
       do ich = 1, nThresholdChannels(ithr)
          iOpen=iOpen+1
          listOpen(iOpen) = ThresholdChannelList( ich, ithr )
          if(ithr==nPI)then
             iOpenPI=iOpenPI+1
             listOpenPI(iOpenPI) = iOpen
          endif
       enddo
    enddo

  end subroutine getopenchannels



end program ProgramComputeScatteringStates


