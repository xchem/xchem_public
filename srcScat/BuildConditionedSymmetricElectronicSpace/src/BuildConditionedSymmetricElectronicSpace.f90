!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program takes the previously computed close-coupling blocks and
!!  transform it using the conditioning matrices to remove linear dependencies.

program BuildConditionedSymmetricElectronicSpace

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleString
  use ModuleMatrix
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleShortRangeOrbitals
  use ModuleGeneralInputFile
  use ModuleAbsorptionPotential


  implicit none


  logical        , parameter :: SET_FORMATTED_WRITE = .TRUE.


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  character(len=:), allocatable :: Gauge
  character(len=:), allocatable :: DipoleAxis
  logical                       :: UsePerfectProjection


  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod


  !
  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassAbsorptionPotential):: AbsPot


  type(ClassSESSESBlock)        :: OperatorMat
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  character(len=:), allocatable :: ConditionLabel
  character(len=:), allocatable :: NewOperatorLabel
  integer :: i, NComp
  !


  !.. Read run-time parameters, among which there is the group and the symmetry
  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &   
       OperatorLabel          ,   &
       BraSymLabel            ,   &
       KetSymLabel            ,   &
       Gauge                  ,   &
       DipoleAxis             ,   &
       UsePerfectProjection    )
  !
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "SAE General Input File  : "//GeneralInputFile
  write(OUTPUT_UNIT,"(a)" ) "Program Input File  : "//ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(a)" ) "Close Coupling File : "//CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  write(OUTPUT_UNIT,"(a)" ) "Operator Name       : "//OperatorLabel
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  if ( allocated(Gauge) ) then
     write(OUTPUT_UNIT,"(a)" ) "Gauge        : "//Gauge
  end if
  if ( allocated(DipoleAxis) ) then
     write(OUTPUT_UNIT,"(a)" ) "DipoleAxis        : "//DipoleAxis
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 



  call ParseProgramInputFile( ProgramInputFile, &
       StorageDir                             , &
       NumBsDropBeginning                     , &
       NumBsDropEnding                        , &
       ConditionBsThreshold                   , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,e10.2)"  ) "ConditionBsThreshold.........", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  )       "ConditionBsMethod............"//ConditionBsMethod


  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OperatorLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )



  
  if ( OperatorLabel .is. CAPLabel ) then
     !.. Load the absorption potential parameters
     call SAEGeneralInputData%ParseFile( GeneralInputFile )
     call AbsPot%Parse( AbsorptionPotentialFile )
     NComp = AbsPot%NumberOfTerms()
  else
     NComp = 1
  end if

  do i = 1, NComp
     !
     if ( OperatorLabel .is. CAPLabel ) then
        allocate ( NewOperatorLabel, source = GetFullCAPLabel(i) )
     else
        allocate ( NewOperatorLabel, source = OperatorLabel )
     end if
     !
     if ( SET_FORMATTED_WRITE) call OperatorMat%SetFormattedWrite()
     call OperatorMat%Load(          &
          BraSymSpace, KetSymSpace , &
          NewOperatorLabel, DipoleAxis  )
     deallocate( NewOperatorLabel )
     !
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
     !.. Auxiliar conditioner. The real Xlm will be rewritten through the conditioning process as well as the real irrep.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          UsePerfectProjection )  
     !
     call OperatorMat%Condition( Conditioner )
     !
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
     call OperatorMat%Save( ConditionLabel )
     !
  end do



contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &   
       OperatorLabel             , &
       BraSymLabel               , &
       KetSymLabel               , &
       Gauge                     , &
       DipoleAxis                , &
       UsePerfectProjection      )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(out)   :: OperatorLabel
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    character(len=:), allocatable, intent(inout) :: Gauge
    character(len=:), allocatable, intent(inout) :: DipoleAxis
    logical,                       intent(out)   :: UsePerfectProjection
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: gif, pif, ccfile, qcdir, op, brasym, ketsym, gau, axis
    character(len=:), allocatable :: AuxOp
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Build the selected operator conditioned matrix, once the original "//&
         " matrix without conditioning has been built, and the propper "//&
         "conditioner is available."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-op"     , "Name of the Operator",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-gau"    , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-axis"   , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-pp"  , "A perfect projector conditioner will be loaded" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif"    , gif          )
    call List%Get( "-pif"    , pif          )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile       )
    call List%Get( "-esdir"  , qcdir        )
    call List%Get( "-op"     , op           )
    !
    allocate( GeneralInputFile       , source = trim( gif )    )
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    allocate( OperatorLabel          , source = trim( op )     )
    !
    call CheckOperator( OperatorLabel )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    if(List%Present("-gau"))then
       call List%Get( "-gau", gau )
       allocate( Gauge, source = trim(gau) )
       call CheckGauge( Gauge )
    end if
    if(List%Present("-axis"))then
       call List%Get( "-axis", axis )
       allocate( DipoleAxis, source = trim(axis) )
       call CheckAxis( DipoleAxis )
    end if
    UsePerfectProjection = List%Present( "-pp" )
    !
    call List%Free()
    !
    !..Check dipole
    if ( OperatorLabel .is. DipoleLabel ) then
       if ( .not.allocated(Gauge) ) call Assert( 'For '//DipoleLabel//' operator the gauge must be present.' )
       if ( .not.allocated(DipoleAxis) ) call Assert( 'For '//DipoleLabel//' operator the orientation must be present.' )
       allocate( AuxOp, source = OperatorLabel//Gauge )
       deallocate( OperatorLabel )
       allocate( OperatorLabel, source = AuxOp )
       deallocate( AuxOp )
    end if
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*) , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/"     , "optional" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"        , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile







end program BuildConditionedSymmetricElectronicSpace
