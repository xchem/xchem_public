!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
program ComputeDipoleTransitionAmplitudes


  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleIO


  implicit none


  logical         , parameter :: SET_FORMATTED_WRITE   = .TRUE.

  !.. Run time parameters.
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  character(len=:), allocatable :: Gauge
  character(len=:), allocatable :: DipoleAxis
  character(len=:), allocatable :: TransitionLabel
  real(kind(1d0))               :: BraEmin = -huge(1d0)
  real(kind(1d0))               :: BraEmax = huge(1d0)
  real(kind(1d0))               :: KetEmin = -huge(1d0)
  real(kind(1d0))               :: KetEmax = huge(1d0)
  character(len=:), allocatable :: CompMethod 
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  !> Only concatenates the transitions from a definite  box state
  !! to all the rest of states, box states or scattering states.
  !! For the box-box transition the box state is that of the bra.
  logical                       :: ConcatenateTransitions
  logical                       :: OnlyQC
  logical                       :: OnlyLoc
  logical                       :: OnlyPoly
  !> If .true. will use the states normalized as the sin function.
  !! If .false. will use the states normalized following the
  !! uncoming boundary conditions.
  logical                       :: UseSinStates
  !> If present at run time, transforms the scattering states into a different
  !! symmetry representation, given a set of coefficients in this
  !! file. I.e.: to pass from D2h channels to spherically
  !! symmetric channels in atoms.
  character(len=:), allocatable :: TransformScattStatesFile
  
  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ScattConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod
  integer                       :: BoxStateIndex

  
  character(len=:), allocatable :: DipOperatorLabel
  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: DipoleMat
  type(ClassMatrix)             :: DipMat
  type(ClassComplexMatrix)      :: BraMat, KetMat
  type(ClassSpectralResolution) :: SpecRes
  type(ClassComplexSpectralResolution) :: BraSpecRes, KetSpecRes
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  character(len=:), allocatable :: ConditionLabel
  character(len=:), allocatable :: DipoleStorageDir
  character(len=:), allocatable :: BraStatesFileName, KetStatesFileName
  character(len=:), allocatable :: BraStatesDir, KetStatesDir
  character(len=256), allocatable :: BraScatteringFilesVec(:), KetScatteringFilesVec(:)
  integer :: NumBra, NumKet, i, j
  real(kind(1d0)), allocatable :: BraEnergies(:), KetEnergies(:), EnergyVec(:,:)
  complex(kind(1d0)), allocatable :: CompEnergies(:)
  type(ClassMatrix) :: BraStates, KetStates
  type(ClassComplexMatrix) :: TransMat, CompBraStates, CompKetStates
  type(ClassComplexMatrix), allocatable :: TransMatArray(:,:)
  type(ClassMatrix), allocatable :: EnergyMatArray(:,:)
  real(kind(1d0)) :: EminConcatenate
  real(kind(1d0)) :: EmaxConcatenate
  real(kind(1d0)) :: BoxEnergy
  !> ClassComplexmatrix containing the transformation matrix
  !! from the old symmetry representation tp the new one.
  type(ClassComplexMatrix) :: TransfMat

  !.. Convenient normalization for comparison purposes.
  !.. For some reason it fails when defined in the variable declaration line.
  EminConcatenate = huge(1d0)
  EmaxConcatenate = -huge(1d0)


  call GetRunTimeParameters(         &
       ProgramInputFile            , &
       Multiplicity                , &
       CloseCouplingConfigFile     , &
       NameDirQCResults            , &    
       BraSymLabel                 , &
       KetSymLabel                 , &
       Gauge                       , &
       DipoleAxis                  , &
       TransitionLabel             , &
       BraEmin                     , &
       BraEmax                     , &
       KetEmin                     , &
       KetEmax                     , &
       CompMethod                  , &
       UseConditionedBlocks        , &
       UsePerfectProjection        , &
       ConcatenateTransitions      , &
       BoxStateIndex               , &
       OnlyQC                      , &
       OnlyLoc                     , &
       OnlyPoly                    , &
       UseSinStates                , &
       TransformScattStatesFile    )

  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" )  "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File :",               ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity : ",                    Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ",             CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(2a)" )  "Nuclear Configuration Dir Name : ", NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(2a)" ) "Bra Symmetry : ", BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(2a)" ) "Ket Symmetry : ", KetSymLabel
  end if
  write(OUTPUT_UNIT,"(2a)" )  "Gauge : ",                        Gauge
  write(OUTPUT_UNIT,"(2a)" )  "DipoleAxis : ",                   DipoleAxis
  write(OUTPUT_UNIT,"(2a)" )  "Transition requested : ",         TransitionLabel
  if ( BraEmin > -huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,e25.16)" )"Bra Minimum Energy :        ", BraEmin
  end if
  if ( BraEmax < huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,e25.16)" )"Bra Maximum Energy :        ", BraEmax
  end if
  if ( KetEmin > -huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,e25.16)" )"Ket Minimum Energy :        ", KetEmin
  end if
  if ( KetEmax < huge(1d0) ) then
     write(OUTPUT_UNIT,"(a,e25.16)" )"Ket Maximum Energy :        ", KetEmax
  end if
  write(OUTPUT_UNIT,"(2a)" ) "Scattering computational Method :  ", CompMethod
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks : ",        UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner : ", UsePerfectProjection 
  write(OUTPUT_UNIT,"(a,L)" )"Concatenate transitions :     ",   ConcatenateTransitions 
  if ( TransitionLabel .isnt. ConConTransitionLabel ) then
     write(OUTPUT_UNIT,"(a,i0)" )"Box state index : ",            BoxStateIndex
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Load QC bound state :        ",   OnlyQC 
  write(OUTPUT_UNIT,"(a,L)" )"Load Localized bound state : ",   OnlyLoc
  write(OUTPUT_UNIT,"(a,L)" )"Load polycentric bound state : ",   OnlyPoly
  write(OUTPUT_UNIT,"(a,L)" )"Use scattering states normalized as sin function : ",   UseSinStates
  if ( allocated(TransformScattStatesFile) ) then
     write(OUTPUT_UNIT,"(a,a)" )"Transform scattering states : ",   TransformScattStatesFile
  end if

  
  if ( ConcatenateTransitions .and. &
       (TransitionLabel .is. ConConTransitionLabel) ) call Assert( &
       'To concatenate the transitions either '//&
       'the ket, the bra or both must be box states. ' )

    
  call ReadTransformationMatrix( TransformScattStatesFile, TransfMat )

  
  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       ScattConditionNumber , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D25.16)" ) "Condition number of scattering states :      ", ScattConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D25.16)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod


  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
 
  
  allocate( DipOperatorLabel, source = DipoleLabel//Gauge )

  call GetSymmetricElectSpaces( Space            , &
       BraSymLabel, DipOperatorLabel, KetSymLabel, &
       DipoleAxis                                , &
       BraSymSpace, KetSymSpace )


  if ( SET_FORMATTED_WRITE) call DipoleMat%SetFormattedWrite()
  !
  if ( TransitionLabel .is. BoxBoxTransitionLabel ) then
     call DipoleMat%SetBoxOnly()     
  elseif ( TransitionLabel .is. BoxConTransitionLabel ) then
     call DipoleMat%SetBraBoxOnly()     
  elseif ( TransitionLabel .is. ConBoxTransitionLabel ) then
     call DipoleMat%SetKetBoxOnly()     
  end if
  
  write(output_unit,"(a)") 'Loading dipole ...'
  !
  if ( UseConditionedBlocks ) then
     Group  => Space%GetGroup()
     irrepv => Group%GetIrrepList()
     Xlm = GetOperatorXlm( DipOperatorLabel, DipoleAxis )
     !.. Auxiliar conditioner only with the purpose to get the label.
     call Conditioner%Init(     &
          ConditionBsMethod   , &
          ConditionBsThreshold, &
          NumBsDropBeginning  , &
          NumBsDropEnding     , &
          irrepv(1), Xlm      , &
          UsePerfectProjection )
     allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
     if ( .not. ConcatenateTransitions ) &
          call DipoleMat%Load( BraSymSpace, KetSymSpace, DipOperatorLabel, DipoleAxis, ConditionLabel )
  else
     if ( .not. ConcatenateTransitions ) &
          call DipoleMat%Load( BraSymSpace, KetSymSpace, DipOperatorLabel, DipoleAxis )
  end if
  !
  if ( .not. ConcatenateTransitions ) then
     call DipoleMat%Assemble( LoadLocStates, DipMat )
     call DipoleMat%Free()
  end if

  allocate( DipoleStorageDir, source = GetBlockStorageDir(BraSymSpace,KetSymSpace)  )


  allocate( BraStatesDir, source = GetBlockStorageDir(BraSymSpace,BraSymSpace) )
  allocate( KetStatesDir, source = GetBlockStorageDir(KetSymSpace,KetSymSpace) )



  if ( TransitionLabel .is. BoxBoxTransitionLabel ) then
     !
     !
     NumBra = 1
     NumKet = 1
     if( ConcatenateTransitions ) then
        allocate( TransMatArray(NumBra,NumKet) )
        allocate( EnergyMatArray(NumBra,NumKet) )
     end if
     !
     do j = 1, NumKet
        !
        if ( OnlyQC ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//QCLabel )
        elseif ( OnlyLoc ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//LocLabel )
        elseif ( OnlyPoly ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
        else
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel) )
        end if
        call SpecRes%Read( KetStatesFileName )
        call SpecRes%Fetch( KetEnergies )
        if ( ConcatenateTransitions ) then
           EminConcatenate = MIN(EminConcatenate,MINVAL(KetEnergies))
           EmaxConcatenate = MAX(EmaxConcatenate,MAXVAL(KetEnergies))
           if ( allocated(EnergyVec) ) deallocate(EnergyVec)
           allocate( EnergyVec(size(KetEnergies),1) )
           EnergyVec(:,1) = KetEnergies(:)
        else
           call SpecRes%Fetch( KetStates ) 
           CompKetStates = KetStates
           call KetStates%Free()
        end if
        call SpecRes%Free()
        !
        do i = 1, NumBra
           !
           if ( OnlyQC ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//QCLabel )
           elseif( OnlyLoc ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//LocLabel )
           elseif( OnlyPoly ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
           else
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel) )
           end if
           call SpecRes%Read( BraStatesFileName )
           call SpecRes%Fetch( BraEnergies )
           if ( ConcatenateTransitions ) then
              BoxEnergy = BraEnergies(BoxStateIndex)
           else
              call SpecRes%Fetch( BraStates, BoxStateIndex )
              CompBraStates = BraStates
              call BraStates%Free()
           end if
           call SpecRes%Free()
           !
           call PrintComputationProgres( (j-1)*NumBra+i, NumBra*NumKet )
           !
           if ( ConcatenateTransitions ) then
              !.. The energy for reading the dipole transition file has only 
              ! meaning when scattering states are involved.
              call ReadDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies(1)      , &
                   KetEnergies(1)      , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMatArray(i,j)  )
              EnergyMatArray(i,j) = EnergyVec
           else
              call ComputeMatrixTransition( CompBraStates, DipMat, CompKetStates, TransMat )
              call SaveDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies         , &
                   KetEnergies         , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMat             )
           end if
           !
           deallocate( BraStatesFileName )
           deallocate( BraEnergies )
           !
        end do
        !
        deallocate( KetStatesFileName )
        deallocate( KetEnergies )
        !
     end do
     !
     if ( ConcatenateTransitions ) &
          call SaveConcatenatedTransitions( &
          TransitionLabel     , &
          DipoleStorageDir    , &
          DipOperatorLabel    , &
          DipoleAxis          , &
          CompMethod          , &
          ConditionLabel      , &
          ScattConditionNumber, &
          EminConcatenate     , &
          EmaxConcatenate     , &
          EnergyMatArray      , &
          TransMatArray       , &
          BoxStateIndex       , &
          BoxEnergy           , &
          OnlyQC              , &
          OnlyLoc             , &
          OnlyPoly            , &
          UseSinStates        )
     !
     !
  elseif ( TransitionLabel .is. BoxConTransitionLabel ) then
     !
     !
     if ( UseSinStates ) then 
        call GetSinScatteringFilesVec( &
             KetStatesDir            , &
             CompMethod              , &
             KetEmin, KetEmax        , &
             ScattConditionNumber    , &  
             ConditionLabel          , &
             KetScatteringFilesVec   )
     else
        call GetIncomingScatteringFilesVec( &
             KetStatesDir            , &
             CompMethod              , &
             KetEmin, KetEmax        , &
             ScattConditionNumber    , &  
             ConditionLabel          , &
             KetScatteringFilesVec   )
     end if
     !
     NumBra = 1
     NumKet = size(KetScatteringFilesVec)
     if( ConcatenateTransitions ) then
        allocate( TransMatArray(NumKet,NumBra) )
        allocate( EnergyMatArray(NumKet,NumBra) )
     end if
     !
     do j = 1, NumKet
        !
        allocate( KetStatesFileName, source = trim(adjustl(KetScatteringFilesVec(j))) )
        call KetSpecRes%Read( KetStatesFileName )
        call TransformScatteringStates( KetSpecRes,TransfMat )
        call KetSpecRes%Fetch( CompEnergies )
        call KetSpecRes%Fetch( CompKetStates )
        call KetSpecRes%Free()
        allocate( KetEnergies, source = dble(CompEnergies(:)) )
        deallocate( CompEnergies )
        if ( ConcatenateTransitions ) then
           EminConcatenate = MIN(EminConcatenate,MINVAL(KetEnergies))
           EmaxConcatenate = MAX(EmaxConcatenate,MAXVAL(KetEnergies))
           if ( allocated(EnergyVec) ) deallocate(EnergyVec)
           allocate( EnergyVec(size(KetEnergies),1) )
           EnergyVec(:,1) = KetEnergies(:)
        end if
        !
        do i = 1, NumBra
           if ( OnlyQC ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//QCLabel )
           elseif( OnlyLoc ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//LocLabel )
           elseif( OnlyPoly ) then
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
           else
              allocate( BraStatesFileName, source = GetSpectrumFileName(BraStatesDir,HamiltonianLabel,ConditionLabel) )
           end if
           call SpecRes%Read( BraStatesFileName )
           call SpecRes%Fetch( BraEnergies )
           if ( ConcatenateTransitions ) then
              BoxEnergy = BraEnergies(BoxStateIndex)
           else
              call SpecRes%Fetch( BraStates, BoxStateIndex )
              CompBraStates = BraStates
              call BraStates%Free()
           end if
           call SpecRes%Free()
           !
           call PrintComputationProgres( (j-1)*NumBra+i, NumBra*NumKet )
           !
           if ( ConcatenateTransitions ) then
              !.. The energy for reading the dipole transition file has only 
              ! meaning when scattering states are involved.
              call ReadDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies(1)      , &
                   KetEnergies(1)      , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMatArray(j,i)  )
              EnergyMatArray(j,i) = EnergyVec
           else
              call ComputeMatrixTransition( CompBraStates, DipMat, CompKetStates, TransMat )
              call SaveDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies         , &
                   KetEnergies         , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMat             )
           end if
           !
           deallocate( BraStatesFileName )
           deallocate( BraEnergies )
        end do
        !
        deallocate( KetStatesFileName )
        deallocate( KetEnergies )
     end do
     !
     if ( ConcatenateTransitions ) &
          call SaveConcatenatedTransitions( &
          TransitionLabel     , &
          DipoleStorageDir    , &
          DipOperatorLabel    , &
          DipoleAxis          , &
          CompMethod          , &
          ConditionLabel      , &
          ScattConditionNumber, &
          EminConcatenate     , &
          EmaxConcatenate     , &
          EnergyMatArray      , &
          TransMatArray       , &
          BoxStateIndex       , &
          BoxEnergy           , &
          OnlyQC              , &
          OnlyLoc             , &
          OnlyPoly            , &
          UseSinStates        )
     !
     !
  elseif ( TransitionLabel .is. ConBoxTransitionLabel ) then
     !
     !
     if ( UseSinStates ) then
        call GetSinScatteringFilesVec( &
             BraStatesDir         , &
             CompMethod           , &
             BraEmin, BraEmax     , &
             ScattConditionNumber , &  
             ConditionLabel       , &
             BraScatteringFilesVec  )
     else
        call GetIncomingScatteringFilesVec( &
             BraStatesDir         , &
             CompMethod           , &
             BraEmin, BraEmax     , &
             ScattConditionNumber , &  
             ConditionLabel       , &
             BraScatteringFilesVec  )
     end if
     !
     NumBra = size(BraScatteringFilesVec)
     NumKet = 1
     if( ConcatenateTransitions ) then
        allocate( TransMatArray(NumBra,NumKet) )
        allocate( EnergyMatArray(NumBra,NumKet) )
     end if
     !
     do j = 1, NumKet
        if ( OnlyQC ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//QCLabel )
        elseif( OnlyLoc ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//LocLabel )
        elseif( OnlyPoly ) then
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
        else
           allocate( KetStatesFileName, source = GetSpectrumFileName(KetStatesDir,HamiltonianLabel,ConditionLabel) )
        end if
        call SpecRes%Read( KetStatesFileName )
        call SpecRes%Fetch( KetEnergies )
        if ( ConcatenateTransitions ) then
           BoxEnergy = KetEnergies(BoxStateIndex)
        else
           call SpecRes%Fetch( KetStates, BoxStateIndex ) 
           CompKetStates = KetStates
           call KetStates%Free()
        end if
        call SpecRes%Free()
        !
        do i = 1, NumBra
           !
           allocate( BraStatesFileName, source = trim(adjustl(BraScatteringFilesVec(i))) )
           call BraSpecRes%Read( BraStatesFileName )
           call TransformScatteringStates( BraSpecRes,TransfMat )
           call BraSpecRes%Fetch( CompEnergies )
           call BraSpecRes%Fetch( CompBraStates )
           call BraSpecRes%Free()
           allocate( BraEnergies, source = dble(CompEnergies(:)) )
           if ( ConcatenateTransitions ) then
              EminConcatenate = MIN(EminConcatenate,MINVAL(BraEnergies))
              EmaxConcatenate = MAX(EmaxConcatenate,MAXVAL(BraEnergies))
              if ( allocated(EnergyVec) ) deallocate(EnergyVec)
              allocate( EnergyVec(size(BraEnergies),1) )
              EnergyVec(:,1) = BraEnergies(:)
           end if
           !
           call PrintComputationProgres( (j-1)*NumBra+i, NumBra*NumKet )
           !
           if ( ConcatenateTransitions ) then
              !.. The energy for reading the dipole transition file has only 
              ! meaning when scattering states are involved.
              call ReadDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies(1)      , &
                   KetEnergies(1)      , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMatArray(i,j)  )
              EnergyMatArray(i,j) = EnergyVec
           else
              call ComputeMatrixTransition( CompBraStates, DipMat, CompKetStates, TransMat )
              call SaveDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies         , &
                   KetEnergies         , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMat             )
           end if
           !
           deallocate( BraStatesFileName )
           deallocate( BraEnergies )
           !
        end do
        !
        deallocate( KetStatesFileName )
        deallocate( KetEnergies )
        !
     end do
     !
     if ( ConcatenateTransitions ) &
          call SaveConcatenatedTransitions( &
          TransitionLabel     , &
          DipoleStorageDir    , &
          DipOperatorLabel    , &
          DipoleAxis          , &
          CompMethod          , &
          ConditionLabel      , &
          ScattConditionNumber, &
          EminConcatenate     , &
          EmaxConcatenate     , &
          EnergyMatArray      , &
          TransMatArray       , &
          BoxStateIndex       , &
          BoxEnergy           , &
          OnlyQC              , &
          OnlyLoc             , &
          OnlyPoly            , &
          UseSinStates        )
     !
     !
  elseif ( TransitionLabel .is. ConConTransitionLabel ) then
     !
     !
     if ( .not.ConcatenateTransitions) then
        !
        if ( UseSinStates ) then
           call GetSinScatteringFilesVec( &
                BraStatesDir         , &
                CompMethod           , &
                BraEmin, BraEmax     , &
                ScattConditionNumber , &  
                ConditionLabel       , &
                BraScatteringFilesVec  )
           !
           call GetSinScatteringFilesVec( &
                KetStatesDir         , &
                CompMethod           , &
                KetEmin, KetEmax     , &
                ScattConditionNumber , &  
                ConditionLabel       , &
                KetScatteringFilesVec  )
        else
           call GetIncomingScatteringFilesVec( &
                BraStatesDir         , &
                CompMethod           , &
                BraEmin, BraEmax     , &
                ScattConditionNumber , &  
                ConditionLabel       , &
                BraScatteringFilesVec  )
           !
           call GetIncomingScatteringFilesVec( &
                KetStatesDir         , &
                CompMethod           , &
                KetEmin, KetEmax     , &
                ScattConditionNumber , &  
                ConditionLabel       , &
                KetScatteringFilesVec  )
        end if
        !
        NumBra = size(BraScatteringFilesVec)
        NumKet = size(KetScatteringFilesVec)
        !
        !
        do j = 1, NumKet
           allocate( KetStatesFileName, source = trim(adjustl(KetScatteringFilesVec(j))) )
           call KetSpecRes%Read( KetStatesFileName )
           call TransformScatteringStates( KetSpecRes,TransfMat )
           call KetSpecRes%Fetch( CompEnergies )
           call KetSpecRes%Fetch( CompKetStates )
           call KetSpecRes%Free()
           allocate( KetEnergies, source = dble(CompEnergies(:)) )
           deallocate( CompEnergies )
           !
           do i = 1, NumBra
              allocate( BraStatesFileName, source = trim(adjustl(BraScatteringFilesVec(i))) )
              call BraSpecRes%Read( BraStatesFileName )
              call TransformScatteringStates( BraSpecRes,TransfMat )
              call BraSpecRes%Fetch( CompEnergies )
              call BraSpecRes%Fetch( CompBraStates )
              call BraSpecRes%Free()
              allocate( BraEnergies, source = dble(CompEnergies(:)) )
              deallocate( CompEnergies )
              !
              call PrintComputationProgres( (j-1)*NumBra+i, NumBra*NumKet )
              !
              call ComputeMatrixTransition( CompBraStates, DipMat, CompKetStates, TransMat )
              call SaveDipoleTransition( &
                   TransitionLabel     , &
                   DipoleStorageDir    , &
                   DipOperatorLabel    , &
                   DipoleAxis          , &
                   CompMethod          , &
                   ConditionLabel      , &
                   ScattConditionNumber, &
                   BraEnergies         , &
                   KetEnergies         , &
                   BoxStateIndex       , &
                   OnlyQC              , &
                   OnlyLoc             , &
                   OnlyPoly            , &
                   UseSinStates        , &
                   TransMat             )
              !
              deallocate( BraStatesFileName )
              deallocate( BraEnergies )
              !
           end do
           !
           deallocate( KetStatesFileName )
           deallocate( KetEnergies )
        end do
        !
     end if
     !
     !
  end if


  write(output_unit,'(a)') 'Done'


contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &    
       BraSymLabel               , &
       KetSymLabel               , &
       Gauge                     , &
       DipoleAxis                , &
       TransitionLabel           , &
       BraEmin                   , &
       BraEmax                   , &
       KetEmin                   , &
       KetEmax                   , &
       CompMethod                , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       ConcatenateTransitions    , &
       BoxStateIndex             , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       UseSinStates              , &
       TransformScattStatesFile  )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    !> [GeneralInputFile](@ref GeneralInputFile)  
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    character(len=:), allocatable, intent(out)   :: Gauge
    character(len=:), allocatable, intent(out)   :: DipoleAxis
    character(len=:), allocatable, intent(out)   :: TransitionLabel
    real(kind(1d0))              , intent(inout) :: BraEmin
    real(kind(1d0))              , intent(inout) :: BraEmax
    real(kind(1d0))              , intent(inout) :: KetEmin
    real(kind(1d0))              , intent(inout) :: KetEmax
    character(len=:), allocatable, intent(inout) :: CompMethod
    logical,                       intent(out)   :: UseConditionedBlocks
    logical,                       intent(out)   :: UsePerfectProjection
    logical,                       intent(out)   :: ConcatenateTransitions
    integer,                       intent(out)   :: BoxStateIndex
    logical,                       intent(out)   :: OnlyQC
    logical,                       intent(out)   :: OnlyLoc
    logical,                       intent(out)   :: OnlyPoly
    logical,                       intent(out)   :: UseSinStates
    !> If present at run time, transforms the scattering states into a different
    !! symmetry representation, given a set of coefficients in this
    !! file. I.e.: to pass from D2h channels to spherically
    !! symmetric channels in atoms.
    character(len=:), allocatable, intent(inout) :: TransformScattStatesFile
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: pif, ccfile, qcdir, brasym, ketsym, gau, axis, trans, cm, transfscattfile
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Computes the dipole transition amplitudes between box "//&
         "eigenstates, box and scattering states, and between "//&
         "scattering states. "
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"   , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir"  , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-gau"    , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "required" )
    call List%Add( "-axis"   , "Dipole orientation: x, y or z",  " ", "required" )
    call List%Add( "-trans"  , "Kind of transition : bb (box-box), bc (box-continuum), cb (continuum-box) or cc &
      (continnum-continuum)",  " ", "required" )
    call List%Add( "-braEmin"   , "Bra space minimum energy"    , 1.d0, "optional" )
    call List%Add( "-braEmax"   , "Bra space maximum energy"    , 1.d0, "optional" )
    call List%Add( "-ketEmin"   , "Ket space minimum energy"    , 1.d0, "optional" )
    call List%Add( "-ketEmax"   , "Ket space maximum energy"    , 1.d0, "optional" )
    call List%Add( "-cm"     , "Computational Method",  " ", "required" )
    call List%Add( "-cond"   , "Use the conditioned blocks" )
    call List%Add( "-pp"     , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-concatenate"     , "Concatenate the transitions in one file." )
    call List%Add( "-boxi"   , "Box state index", 1, "optional" )
    call List%Add( "-onlyqc" , "Loads the bound state from QC" )
    call List%Add( "-onlyloc", "Loads the bound state from the localized orbitals" )
    call List%Add( "-onlypoly", "Loads the bound state from the polycentric orbitals" )
    call List%Add( "-sinasymp", "Loads the scattering states the behave asymptotically as a sin function" )
    call List%Add( "-transfscattfile"  , "Name of the File with the coefficients to transform the scattering states",&
      " ", "optional" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-pif"    , pif           )
    call List%Get( "-mult"   , Multiplicity  )
    call List%Get( "-ccfile" , ccfile        )
    call List%Get( "-esdir"  , qcdir         )
    call List%Get( "-gau"    , gau           )
    call List%Get( "-axis"   , axis          )
    call List%Get( "-trans"  , trans         )
    call List%Get( "-boxi"   , BoxStateIndex )
    !
    if ( List%Present("-braEmin") ) then
       call List%Get( "-braEmin", BraEmin )
    end if
    if ( List%Present("-braEmax") ) then
       call List%Get( "-braEmax", BraEmax )
    end if
    if ( List%Present("-ketEmin") ) then
       call List%Get( "-ketEmin", KetEmin )
    end if
    if ( List%Present("-ketEmax") ) then
       call List%Get( "-ketEmax", KetEmax )
    end if
    if ( List%Present("-cm") ) then
       call List%Get( "-cm", cm )
       allocate( CompMethod, source = trim(cm) )
    end if
    if ( List%Present("-transfscattfile") ) then
       call List%Get( "-transfscattfile", transfscattfile )
       allocate( TransformScattStatesFile, source = trim( transfscattfile ) )
    end if
    !
    UseConditionedBlocks   = List%Present( "-cond" )
    UsePerfectProjection   = List%Present( "-pp" )
    ConcatenateTransitions = List%Present( "-concatenate" )
    OnlyQC                 = List%Present( "-onlyqc" )
    OnlyLoc                = List%Present( "-onlyloc" )
    OnlyPoly               = List%Present( "-onlypoly" )
    UseSinStates           = List%Present( "-sinasymp" )
    !
    allocate( ProgramInputFile       , source = trim( pif )    )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults       , source = trim( qcdir )  )
    allocate( Gauge                  , source = trim( gau )    )
    allocate( DipoleAxis             , source = trim( axis )   )
    allocate( TransitionLabel        , source = trim( trans )  )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    if ( TransitionLabel .isnt. ConConTransitionLabel ) then
       if ( .not.List%Present("-boxi") ) call Assert( &
            'For the transitions: box-box, box-continuum and '//&
            'continuum-box, the box state index: '//&
            'option "-boxi" must be set.')
    end if
    !
    call List%Free()
    !
    call SetStringToUppercase( CompMethod )
    call CheckScatteringMethod( CompMethod )
    call CheckGauge( Gauge )
    call SetStringToUppercase( TransitionLabel )
    call CheckTransitionLabel( TransitionLabel )
    call CheckScatteringInput( TransitionLabel, CompMethod )
    !
    if ( OnlyQC .and. OnlyLoc ) then
       call Assert( '"onlyqc" and "onlyloc" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyQC .and. OnlyPoly ) then
       call Assert( '"onlyqc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyPoly .and. OnlyLoc ) then
       call Assert( '"onlypoly" and "onlyloc" cannot be called '//&
            'at the same time at run time.' )
    end if
    !  
  end subroutine GetRunTimeParameters




  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ScattConditionNumber       , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ScattConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ScattConditionNumber", 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ScattConditionNumber", ScattConditionNumber  )
    call List%Get( "LoadLocStates"       , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ScattConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile





  
  subroutine ComputeMatrixTransition( CompBraStates, DipMat, CompKetStates, TransMat )
    class(ClassComplexMatrix), intent(in)  :: CompBraStates
    class(ClassMatrix)       , intent(in)  :: DipMat
    class(ClassComplexMatrix), intent(in)  :: CompKetStates
    class(ClassComplexMatrix), intent(out) :: TransMat
    type(ClassComplexMatrix) :: AuxMat
    !
    TransMat = CompBraStates
    AuxMat   = CompKetStates
    call RedimensionStates( TransMat, DipMat, AuxMat  )
    !
    call TransMat%TransposeConjugate()
    call TransMat%Multiply( DipMat, 'Right', 'N' )
    call TransMat%Multiply( AuxMat, 'Right', 'N' )
    !
  end subroutine ComputeMatrixTransition


  !> If OnlyQC or OnlyLoc or OnlyPoly is .true., then the dimension of the 
  !! bound states computed with this submatrices differs from 
  !! the dimensions of the dipole transition matrix. According to
  !! the conventions to assemble the full matrices, it is very
  !! easy to redimension the bound states, it is only needed to
  !! add the corresponding amount of zeros.
  subroutine RedimensionStates( NewBraStates, DipMat, NewKetStates )
    !
    class(ClassComplexMatrix), intent(inout) :: NewBraStates
    class(ClassMatrix)       , intent(in)    :: DipMat
    class(ClassComplexMatrix), intent(inout) :: NewKetStates
    !
    integer :: BraDipDim, KetDipDim, BraDim, KetDim, i, j, NCol
    type(ClassComplexMatrix) :: NewMat
    !
    BraDipDim = DipMat%NRows()
    KetDipDim = DipMat%NColumns()
    !
    BraDim = NewBraStates%NRows()
    KetDim = NewKetStates%NRows()
    !
    if ( BraDipDim > BraDim ) then
       NCol = NewBraStates%NColumns()
       call NewMat%InitFull( BraDipDim, NCol )
       do j = 1, NCol
          do i = 1, BraDim 
             call NewMat%SetElement( i, j, NewBraStates%Element(i,j) )
          end do
       end do
       call NewBraStates%Free()
       NewBraStates = NewMat
       call NewMat%Free()
    end if
    !
    if ( KetDipDim > KetDim ) then
       NCol = NewKetStates%NColumns()
       call NewMat%InitFull( KetDipDim, NCol )
       do j = 1, NCol
          do i = 1, KetDim 
             call NewMat%SetElement( i, j, NewKetStates%Element(i,j) )
          end do
       end do
       call NewKetStates%Free()
       NewKetStates = NewMat
       call NewMat%Free()
    end if
    !
  end subroutine RedimensionStates




  subroutine SaveConcatenatedTransitions( &
       TransitionLabel           , &
       DipoleStorageDir          , &
       DipOperatorLabel          , &
       DipoleAxis                , &
       CompMethod                , &
       ConditionLabel            , &
       ScattConditionNumber      , &
       EnergyMin                 , &
       EnergyMax                 , &
       EnergyMatArray            , &
       TransMatArray             , &
       BoxStateIndex             , &
       BoxEnergy                 , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       UseSinStates              )
    !
    character(len=*)             , intent(in)    :: TransitionLabel
    character(len=*)             , intent(in)    :: DipoleStorageDir
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: EnergyMin
    real(kind(1d0))              , intent(in)    :: EnergyMax
    class(ClassMatrix)           , intent(in)    :: EnergyMatArray(:,:)
    class(ClassComplexMatrix)    , intent(in)    :: TransMatArray(:,:)
    integer                      , intent(in)    :: BoxStateIndex
    real(kind(1d0))              , intent(in)    :: BoxEnergy
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    !
    character(len=:), allocatable :: FullDipoleDir, FileName, FullFileName, FileNameComplex, FullFileNameComplex
    integer :: uid, uidComplex, i, NumBlocksBra, NumBlocksKet, NChannels
    !
    write(output_unit,'(a)') 'Saving concatenated transition amplitudes ...'
    allocate( FullDipoleDir, source = GetFullDipoleDir(DipoleStorageDir,TransitionLabel) ) 
    !
    allocate( FileName, source = &
         GetConcatenatedFileName( &
         DipOperatorLabel    , &
         DipoleAxis          , &
         CompMethod          , &
         ConditionLabel      , &
         ScattConditionNumber, &
         EnergyMin           , &
         EnergyMax           , &
         BoxStateIndex       , &
         OnlyQC, OnlyLoc, OnlyPoly, UseSinStates )  )
    !
    allocate( FullFileName, source = FullDipoleDir//FileName )
    !
    call OpenFile( FullFileName, uid, 'write', 'formatted' )
    write(uid,'(4(a,3x))') '#', 'Energy Box State ', 'Energies', 'abs(Amplitud) ...'
    !
    !
    allocate( FileNameComplex, source = &
         GetComplexConcatenatedFileName( &
         DipOperatorLabel    , &
         DipoleAxis          , &
         CompMethod          , &
         ConditionLabel      , &
         ScattConditionNumber, &
         EnergyMin           , &
         EnergyMax           , &
         BoxStateIndex       , &
         OnlyQC, OnlyLoc, OnlyPoly, UseSinStates )  )
    !
    allocate( FullFileNameComplex, source = FullDipoleDir//FileNameComplex )
    !
    call OpenFile( FullFileNameComplex, uidComplex, 'write', 'formatted' )
    write(uidComplex,'(5(a,3x))') '#', 'Energy Box State ', 'Energies', 'Re(Amplitud)', 'Im(Amplitud) ...'
    !
    !
    NumBlocksBra = size(TransMatArray,1)
    NumBlocksKet = size(TransMatArray,2)
    !
    if ( NumBlocksBra == 1 ) then
       if ( NumBlocksKet == 1 ) then
          NChannels = EnergyMatArray(1,1)%NRows()
          do i = 1, NChannels
             write(uid,'(3(f32.16))') BoxEnergy, EnergyMatArray(1,1)%Element(i,1), abs(TransMatArray(1,1)%Element(1,i))
             write(uidComplex,'(4(f32.16))') BoxEnergy, EnergyMatArray(1,1)%Element(i,1), dble(TransMatArray(1,1)%Element(1,i)),&
               aimag(TransMatArray(1,1)%Element(1,i))
          end do
       else
          do i = 1, NumBlocksKet
             write(uid,'(2(f32.16))',ADVANCE="NO") BoxEnergy, EnergyMatArray(1,i)%Element(1,1)
             write(uidComplex,'(2(f32.16))',ADVANCE="NO") BoxEnergy, EnergyMatArray(1,i)%Element(1,1)
             NChannels = EnergyMatArray(1,i)%NRows()
             do j = 1, NChannels
                if ( j == NChannels ) then
                   write(uid,"(f32.16)",ADVANCE="YES") abs(TransMatArray(1,i)%Element(BoxStateIndex,j))
                   write(uidComplex,"(f32.16)",ADVANCE="NO") dble(TransMatArray(1,i)%Element(BoxStateIndex,j))
                   write(uidComplex,"(f32.16)",ADVANCE="YES") aimag(TransMatArray(1,i)%Element(BoxStateIndex,j))
                else
                   write(uid,"(f32.16)",ADVANCE="NO") abs(TransMatArray(1,i)%Element(BoxStateIndex,j)) 
                   write(uidComplex,"(2(f32.16))",ADVANCE="NO") dble(TransMatArray(1,i)%Element(BoxStateIndex,j)),&
                     aimag(TransMatArray(1,i)%Element(BoxStateIndex,j)) 
                end if
             end do
          end do
       end if
    else
       do i = 1, NumBlocksBra
          write(uid,'(2(f32.16))',ADVANCE="NO") BoxEnergy, EnergyMatArray(i,1)%Element(1,1)
          write(uidComplex,'(2(f32.16))',ADVANCE="NO") BoxEnergy, EnergyMatArray(i,1)%Element(1,1)
          NChannels = EnergyMatArray(i,1)%NRows()
          !
          if ( TransitionLabel .is. BoxConTransitionLabel ) then
             do j = 1, NChannels
                if ( j == NChannels ) then
                   write(uid,"(f32.16)",ADVANCE="YES") abs(TransMatArray(i,1)%Element(1,j))
                   write(uidComplex,"(f32.16)",ADVANCE="NO") dble(TransMatArray(i,1)%Element(1,j))
                   write(uidComplex,"(f32.16)",ADVANCE="YES") aimag(TransMatArray(i,1)%Element(1,j))
                else
                   write(uid,"(f32.16)",ADVANCE="NO") abs(TransMatArray(i,1)%Element(1,j)) 
                   write(uidComplex,"(2(f32.16))",ADVANCE="NO") dble(TransMatArray(i,1)%Element(1,j)),&
                     aimag(TransMatArray(i,1)%Element(1,j)) 
                end if
             end do
          elseif ( TransitionLabel .is. ConBoxTransitionLabel ) then
             do j = 1, NChannels
                if ( j == NChannels ) then
                   write(uid,"(f32.16)",ADVANCE="YES") abs(TransMatArray(i,1)%Element(j,1))
                   write(uidComplex,"(f32.16)",ADVANCE="NO") dble(TransMatArray(i,1)%Element(j,1))
                   write(uidComplex,"(f32.16)",ADVANCE="YES") aimag(TransMatArray(i,1)%Element(j,1))
                else
                   write(uid,"(f32.16)",ADVANCE="NO") abs(TransMatArray(i,1)%Element(j,1)) 
                   write(uidComplex,"(2(f32.16))",ADVANCE="NO") dble(TransMatArray(i,1)%Element(j,1)),&
                     aimag(TransMatArray(i,1)%Element(j,1)) 
                end if
             end do
          end if
       end do
    end if
    !
    close( uid )
    close( uidComplex )
    !
  end subroutine SaveConcatenatedTransitions





  function GetConcatenatedFileName(  &
       DipOperatorLabel, DipoleAxis, &
       CompMethod, ConditionLabel  , &
       ScattConditionNumber        , &
       EnergyMin, EnergyMax, BoxStateIndex, &
       OnlyQC, OnlyLoc, OnlyPoly, UseSinStates ) result(FileName)
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: EnergyMin
    real(kind(1d0))              , intent(in)    :: EnergyMax
    integer                      , intent(in)    :: BoxStateIndex
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    !
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: EnergyLabel, ConditioningLabel, IntFileName
    !
    allocate( EnergyLabel, source = GetEnergyRangeLabel(EnergyMin,EnergyMax) )
    !
    allocate( ConditioningLabel, source = GetScattConditioningLabel(CompMethod,ConditionLabel,ScattConditionNumber,UseSinStates))
    !
    allocate( IntFileName, source = &
         GetFullDipoleOperatorLabel(DipOperatorLabel,DipoleAxis)//&
         BoxIndexLabel//AlphabeticNumber(BoxStateIndex)//&
         EnergyLabel//ConditioningLabel//CompMethod )
    !
    if ( OnlyQC ) then
       allocate( FileName, source = IntFileName//QCLabel )
    elseif( OnlyLoc ) then
       allocate( FileName, source = IntFileName//LocLabel )
    elseif( OnlyPoly ) then
       allocate( FileName, source = IntFileName//PolyLabel )
    else
       allocate( FileName, source = IntFileName )
    end if
    !
  end function GetConcatenatedFileName



  function GetComplexConcatenatedFileName(  &
       DipOperatorLabel, DipoleAxis, &
       CompMethod, ConditionLabel  , &
       ScattConditionNumber        , &
       EnergyMin, EnergyMax, BoxStateIndex, &
       OnlyQC, OnlyLoc, OnlyPoly, UseSinStates ) result(FileName)
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: EnergyMin
    real(kind(1d0))              , intent(in)    :: EnergyMax
    integer                      , intent(in)    :: BoxStateIndex
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    !
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: EnergyLabel, ConditioningLabel, IntFileName
    !
    allocate( EnergyLabel, source = GetEnergyRangeLabel(EnergyMin,EnergyMax) )
    !
    allocate( ConditioningLabel, source = GetScattConditioningLabel(CompMethod,ConditionLabel,ScattConditionNumber,UseSinStates) )
    !
    allocate( IntFileName, source = &
         GetFullDipoleOperatorLabel(DipOperatorLabel,DipoleAxis)//&
         '_Complex_'//&
         BoxIndexLabel//AlphabeticNumber(BoxStateIndex)//&
         EnergyLabel//ConditioningLabel//CompMethod )
    !
    if ( OnlyQC ) then
       allocate( FileName, source = IntFileName//QCLabel )
    elseif( OnlyLoc ) then
       allocate( FileName, source = IntFileName//LocLabel )
    elseif( OnlyPoly ) then
       allocate( FileName, source = IntFileName//PolyLabel )
    else
       allocate( FileName, source = IntFileName )
    end if
    !
  end function GetComplexConcatenatedFileName





  subroutine PrintComputationProgres( Element, NumElements )
    integer, intent(in) :: Element
    integer, intent(in) :: NumElements
    write(output_unit,'(a)') 'Transition '//&
         AlphabeticNumber(Element)//' of '//&
         AlphabeticNumber(NumElements)//' ...' 
  end subroutine PrintComputationProgres





end program ComputeDipoleTransitionAmplitudes
