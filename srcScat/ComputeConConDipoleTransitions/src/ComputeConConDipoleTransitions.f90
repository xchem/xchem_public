!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************


! {{{ Detailed description
!!  Author : Luca Argenti
!!           University of Central Florida, 2017
!!
!> \mainpage Program ComputePhotoelectronSpectrum.
!! 
!! Synopsis:
!! ---------
!!
!! ___
!! Description (applicable to atoms only):
!! ------------
!!
! }}}
program ComputeConConDipoleTransitions

  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING

  use ModuleGeneralInputFile
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleString
  use ModuleIO
  use ModuleHDF5
  use ModuleBasis
  use ModuleBSpline
  use ModuleElectronicSpace
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleParentIons

  use ModuleConstants
  use ModuleDiagonalize
  use ModuleScatteringStates
  use ModuleMatrix

  implicit none

  enum, bind(c)
     enumerator :: MODE_PRINT_INFO
     enumerator :: MODE_UNIFORM
     enumerator :: MODE_RYDBERG
     enumerator :: MODE_RESOLVE
     enumerator :: MODE_REFINE 
  end enum

  !.. Run-time parameters
  !..
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  character(len=*), parameter :: FORM_IO = "FORMATTED"
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: CloseCouplingConfigFile
  integer                       :: Multiplicity
  character*200, allocatable    :: scatLabel(:)
  character(len=:), allocatable :: SymLabel
  character(len=:), allocatable :: PsiFile
  character(len=:), allocatable :: OutDipFile
  character(len=:), allocatable :: OperatorLabel
  character(len=:), allocatable :: OutDir

  character(len=:), allocatable :: StorageDir
  type(ClassElectronicSpace)    :: Space
  character(len=:), allocatable :: SymStorageDir
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace


  character(len=:), allocatable :: SAEBasisFile
  logical                       :: UseConditionedBlocks
  type(ClassSpectralResolution) :: HamSpecRes
  character(len=:), allocatable :: SAEstoredir
  type(ClassBasis)              :: GABSBasis
  type(ClassGroup), pointer     :: Group
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  type(ClassBSpline)            :: BSplineBasis
  character(len=:), allocatable :: BSplineBasisFile
  character(len=:), allocatable :: ConditionBsMethod
  character(len=:), allocatable :: DipoleAxis
  type(ClassSESSESBlock)        :: OpBlock
  type(ClassMatrix),allocatable :: OpMat(:,:)
  integer, allocatable          :: BsRemovedInf(:)
  character(len=:), allocatable :: ConditionLabel
  character(len=200) :: sBuffer
  complex(kind(1.d0)), allocatable   :: Amplitudes(:,:,:,:)

  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: nChannels

  integer         :: ibuf,i, j,k, BraIonIndex, KetIonIndex, NumPWC, uid,nPIs,nBas,RowStart,ColStart,iScat,jScat

  integer           , allocatable :: vnOpen1(:),vnOpen2(:)

  !> Linked-list with the scattering states at several energies
  type(ClassScatteringStateList)          :: ScatStateList1
  type(ClassScatteringStateList)          :: ScatStateList2
  type(ClassScatteringState)    , pointer :: ScatState


  character(len=:), allocatable :: ccscat_dir
  integer                      :: iEn,nBoxStates
  logical                      :: Dipole,exists 
  character(len=:),allocatable :: ChanName
  real(kind(1d0)), allocatable :: Egrid1(:),Egrid2(:)
  type(ClassMatrix)            :: DipMat
  call GetRunTimeParameters(    &
       GeneralInputFile       ,   &
              ProgramInputFile, &
              NameDirQCResults, &
       CloseCouplingConfigFile, &
              Multiplicity    , &
              SymLabel        , &
              PsiFile         , &
              OutDipFile      , &
              Dipole          , &
              scatLabel       , &
              OperatorLabel   , &
       UseConditionedBlocks   , &
       DipoleAxis             ,   &
       nPIs                   ,   &
              OutDir          )
  
  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       BSplineBasisFile     , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  call Execute_Command_Line(" mkdir -p "//OutDir)


  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir

  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )

  call GetSymmetricElectronicSpace( Space, SymLabel, SymSpace )
  allocate( SymStorageDir, source = GetBlockStorageDir( SymSpace, SymSpace ) )
  inquire(file=SymStorageDir//'BoxStates/'//PsiFile,exist=exists)
  if (exists) then
    open(1,file=trim(SymStorageDir)//'BoxStates/'//trim(PsiFile),status='old',form='formatted')
    read(1,*) i,j
    close(1)
    if (i.ne.j) exists=.FALSE.
  endif
   
  if (exists) then
    call LoadDipole(SymStorageDir//'BoxStates/'//PsiFile,DipMat)
  else
    call SAEGeneralInputData%ParseFile( GeneralInputFile )
    call SAEGeneralInputData%GetStore( SAEstoredir )
    call FormatDirectoryName( SAEstoredir ) 
    call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
    call GABSBasis%Init( SAEBasisFile )
    
    if ( UseConditionedBlocks ) then
       Group  => Space%GetGroup()
       irrepv => Group%GetIrrepList()
       Xlm = GetOperatorXlm( OverlapLabel, DipoleAxis )
       !.. Auxiliar conditioner only with the purpose to get the label.
       call Conditioner%Init(     &
            ConditionBsMethod   , &
            ConditionBsThreshold, &
            NumBsDropBeginning  , &
            NumBsDropEnding     , &
            irrepv(1), Xlm      , &
            .FALSE. )
       allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
    endif
  
    if ( SET_FORMATTED_WRITE ) call OpBlock%SetFormattedWrite()
    call OpBlock%SetBoxOnly()
  
    write(output_unit,"(a)") 'Loading Matrix ...'
    !
    allocate(OpMat(nPIs+1,nPIs+1))
    nBas=0
    do BraIonIndex=0,nPIs
      do KetIonIndex=0,nPIs
        write(*,"(a,i0)") "BraIonIndex: ",BraIonIndex
        write(*,"(a,i0)") "KetIonIndex: ",KetIonIndex
        if (UseConditionedBlocks) then
          call OpBlock%LoadMonoIonPolyCondition( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, Conditioner, BsRemovedInf = BsRemovedInf,Axis=DipoleAxis )
        else
          call OpBlock%LoadMonoIonPoly( SymSpace, SymSpace, OperatorLabel, BraIonIndex, KetIonIndex, DipoleAxis )
        endif
        call OpBlock%AssembleChCh( OpMat(BraIonIndex+1,KetIonIndex+1), BraIonIndex, KetIonIndex )
      enddo
      nBas=nBas+OpMat(BraIonIndex+1,1)%nRows()
    enddo
    call OpBlock%Free()
    call DipMat%InitFull(nBas,nBas)
    RowStart=1
    do BraIonIndex=0,nPIs
      ColStart=1
      do KetIonIndex=0,nPIs
        call DipMat%ComposeFromBlocks(RowStart,RowStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nRows()-1,ColStart,ColStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nColumns()-1,OpMat(BraIonIndex+1,KetIonIndex+1))
        ColStart = ColStart + OpMat(BraIonIndex+1,KetIonIndex+1)%nColumns()
        if (KetIonIndex.eq.nPIs) RowStart=RowStart+OpMat(BraIonIndex+1,KetIonIndex+1)%nRows()
        call OpMat(BraIonIndex+1,KetIonIndex+1)%Free()
      enddo
    enddo
    deallocate(OpMat)
    write(*,*) 'Reading the Hamiltonian already diagonalized'
    sBuffer = GetBlockStorageDir(SymSpace,SymSpace)
    sBuffer = GetSpectrumFileName(SymStorageDir,HamiltonianLabel) 
    call HamSpecRes%Read( sBuffer, FORM_IO )
    call DipMat%Multiply(HamSpecRes%EigenVectors, 'Right', 'N' )
    call DipMat%Multiply(HamSpecRes%EigenVectors, 'Left' , 'T' )
    call HamSpecRes%Free()
    call HamSpecRes%Free()
  endif
  allocate(ccscat_dir, source = SymStorageDir//"/ScatteringStates/SPEC/")
  call ScatStateList1%Init(ccscat_dir)
  call ScatStateList2%Init(ccscat_dir)
  do iScat=1,Size(scatLabel)
    call ScatStateList1%Load( trim(scatLabel(iScat))) 
    call ScatStateList1%GetEnergyList( Egrid1 )
    call ScatStateList1%GetnOpenList(  vnOpen1 )
    
    call ScatStateList1%ProjectTwoScatLists(ScatStateList1,DipMat,Amplitudes)

    call SaveAmplitudes( Amplitudes, vnOpen1, vnOpen1, Egrid1, Egrid1,  OutDir, OutDipFile//'_'//trim(scatLabel(iScat))//'_'//trim(scatLabel(iScat)))
    deallocate(Amplitudes)
    do jScat=iScat+1,size(scatLabel)
      call ScatStateList2%Load( trim(scatLabel(jScat)) )
      call ScatStateList2%GetEnergyList( Egrid2 )
      call ScatStateList2%GetnOpenList(  vnOpen2 )
      
      call ScatStateList1%ProjectTwoScatLists(ScatStateList2,DipMat,Amplitudes)

      call SaveAmplitudes( Amplitudes, vnOpen1, vnOpen2, Egrid1, Egrid2, OutDir, OutDipFile//'_'//trim(scatLabel(iScat))//'_'//trim(scatLabel(jScat)))
      deallocate(Amplitudes)
    enddo
  enddo

contains
  

  function UniformGrid(xmi,xma,n) result(grid)
    real(kind(1d0)), intent(in) :: xmi, xma
    integer        , intent(in) :: n
    real(kind(1d0)), allocatable :: grid(:)
    integer :: i
    allocate(grid,source=[(xmi+(xma-xmi)/dble(n-1)*dble(i-1),i=1,n)])
  end function UniformGrid


  !> Reads the run time parameters specified in the command line.
  !! If an optional inherently positive parameters is not specified
  !! on the command line, it is initialized to a negative number
  !! to signal that it should not be used to evaluate the energy grid.
  !<
  subroutine GetRunTimeParameters( &
       GeneralInputFile       ,   &
       ProgramInputFile, &
       NameDirQCResults, &
       ConfigFile      , &
       Multiplicity    , &
       SymLabel        , &
       PsiFile         , &
       OutDipFile      , &
       Dipole          , &
       ScatLabel       , &
       OperatorLabel   , &
       UseConditionedBlocks      , &
       DipoleAxis                , &
       nPIs                      , &
       OutDir)
    !
    use ModuleErrorHandling
    use ModuleCommandLineParameterList
    use ModuleString    
    implicit none
    !
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    character(len=:), allocatable, intent(out) :: ConfigFile
    logical                      , intent(out) :: Dipole
    integer                      , intent(out) :: Multiplicity
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out) :: SymLabel
    character(len=:), allocatable, intent(out) :: PsiFile
    character(len=:), allocatable              ::  scatLabelList
    logical,                       intent(out) :: UseConditionedBlocks
    character(len=:), allocatable, intent(out) :: OperatorLabel
    character*200, allocatable, intent(out)    :: scatLabel(:)
    integer,                       intent(out)   :: nPIs
    character(len=:), allocatable, intent(out) :: OutDipFile
    character(len=:), allocatable, intent(out) :: OutDir
    character(len=:), allocatable              :: Gauge
    character(len=:), allocatable              :: AuxOp
    character(len=:), allocatable, intent(out) :: DipoleAxis
    !
    type( ClassCommandLineParameterList ) :: List
    integer i,j,counter
    !
    character(len=*), parameter :: PROGRAM_DESCRIPTION=&
         "Compute multichannel scattering states"
    character(len=512) :: strnBuf, pif
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help" , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " "    , "required" )
    call List%Add( "-gif"   , "SAE General Input File",  " ", "required" )
    call List%Add( "-label" , "subdir label" , ""            , "optional" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-ccfile"  , "Config File"  , "CCFile"      , "optional" )
    call List%Add( "-gau"   , "Dipole Gauge: Length (l) or Velocity (v)",  " ", "optional" )
    call List%Add( "-scatlabellist" , "list of labels of the set of scattering statesi (one blank space between labels)"  ,  "1", "optional" )
    call List%Add( "-npi"   , "Number of Parent Ions",  1, "required" )
    call List%Add( "-axis"  , "Dipole orientation: x, y or z",  " ", "optional" )
    call List%Add( "-mult"  , "Total Multiplicity", 1        , "required" )
    call List%Add( "-op"     , "Name of the Operator",  " ", "required" )
    call List%Add( "-sym"   , "SymLabel"         , "A"       , "required" )
    call List%Add( "-psi"   , "WaveFunction File", "coef.out", "optional" )
    call List%Add( "-od"    , "OutDir"           , "out"     , "required" )
    !
    call List.Parse()
    !
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if

    UseConditionedBlocks = List%Present( "-cond" )
    if (List.Present("-axis") .and. List.Present("-gau") ) then
      Dipole = .TRUE.
    elseif (List.Present("-axis") .or. List.Present("-gau") ) then 
      call Assert('Both (-gau and -axis) have to be specified for calculating the Dipole Tranistion Amplitudes')
    else
      Dipole = .FALSE.
    endif

    !
    call List%Get( "-gif"   , strnBuf  )
    allocate( GeneralInputFile, source = trim( strnBuf ) )
    if(List%Present("-gau"))then
       call List%Get( "-gau", strnBuf )
       allocate( Gauge, source = trim(strnBuf) )
    end if
    call List%Get( "-axis"   , strnBuf )
    allocate( DipoleAxis             , source = trim( strnBuf )   )
    call List%Get( "-pif"    , pif  )
    allocate( ProgramInputFile       , source = trim( pif )    )
    call List.Get( "-label",  strnBuf  )
    allocate(NameDirQCResults,source=trim(adjustl(strnBuf)))
    call List.Get( "-ccfile",  strnBuf  )
    allocate(ConfigFile,source=trim(adjustl(strnBuf)))
    call List%Get( "-mult"   , Multiplicity )
    call List.Get( "-sym"  ,  strnBuf  )
    allocate( SymLabel, source=trim(adjustl(strnBuf)))
    call List.Get( "-psi"  ,  strnBuf  )
    allocate( PsiFile, source=trim(adjustl(strnBuf)))
    call List%Get( "-npi"  , nPIs )
    call List.Get( "-od",  strnBuf  )
    allocate( OutDir, source=trim(adjustl(strnBuf)))
    call List%Get( "-scatlabellist",  strnBuf  )
    allocate(scatLabelList,source=trim(adjustl(strnBuf)))
    call List%Get( "-op"     , strnBuf           )
    !
    allocate( OperatorLabel          , source = trim( strnBuf )     )
    !
    call CheckOperator( OperatorLabel )
    j=0
    do i=1,len(trim(scatLabelList))
      if (scatLabelList(i:i) .eq. ':') j=j+1
    enddo
    allocate(scatLabel(j+1))
    j=1
    counter=1
    do i=1,len(trim(scatLabelList))
      if (scatLabelList(i:i) .eq. ':') then
        scatLabel(counter) = scatLabelList(j:i-1)
        counter = counter + 1
        j = i + 1
      endif
    enddo
    scatLabel(counter) = scatLabelList(j:len(trim(scatLabelList)))

    !
    call List.Free()

    if (Dipole) then
      deallocate(PsiFile)
      if (trim(Gauge) .eq. 'l') then
        allocate( PsiFile, source=trim('Dipole'//'Len_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Len_'//trim(DipoleAxis)//'_ConCon_')
        allocate( OutDipFile, source=trim(strnBuf))
      elseif (trim(Gauge) .eq. 'v' ) then    
        allocate( PsiFile, source=trim('Dipole'//'Vel_'//trim(DipoleAxis)//'.box'))
        write(strnBuf,'(A,I0)') trim('DipoleTransAmp'//'Vel_'//trim(DipoleAxis)//'_ConCon_')
        allocate( OutDipFile, source=trim(strnBuf))
      else
        call Assert('The Dipole gauge is not well specified')
      endif
      call CheckGauge( Gauge )
    endif
    if ( OperatorLabel .is. DipoleLabel ) then
       if ( .not.allocated(Gauge) ) call Assert( 'For '//DipoleLabel//' operator the gauge must be present.' )
       if ( .not.allocated(DipoleAxis) ) call Assert( 'For '//DipoleLabel//' operator the orientation must be present.' )
       allocate( AuxOp, source = OperatorLabel//Gauge )
       deallocate( OperatorLabel )
       allocate( OperatorLabel, source = AuxOp )
       deallocate( AuxOp )
    end if
    !.. Must add check on the input
    !
    write(OUTPUT_UNIT,"(a)" ) "Run time parameters :"
    write(OUTPUT_UNIT,"(a)" ) "Subdir label    : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)" ) "CC Config File  : "//ConfigFile
    write(OUTPUT_UNIT,"(a)" ) "Spectral SymLabel   : "//SymLabel
    write(OUTPUT_UNIT,"(a)" ) "Labels of the Scattering States considered are:"
    do i=1,size(scatLabel)
      write(OUTPUT_UNIT,"(a)" ) trim(scatLabel(i))
    enddo
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       BSplineBasisFile           , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    character(len=:), allocatable, intent(out) :: BSplineBasisFile
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: sBuffer, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "."    , "optional" )
    call List%Add( "BSplineBasisFile"    , "BSplineBasis", "optional" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"                , sBuffer   )
    allocate( StorageDir      , source = trim(adjustl(sBuffer)) )
    !
    call List%Get( "BSplineBasisFile"          , sBuffer   )
    allocate( BSplineBasisFile, source = trim(adjustl(sBuffer)) )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , sBuffer   )
    allocate( ConditionBsMethod, source = trim(adjustl(sBuffer)) )
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
    !
    write(OUTPUT_UNIT,*)
    write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
    !
  end subroutine ParseProgramInputFile

  subroutine LoadDipole( PsiFile, DipMat )
    character(len=*)  , intent(in)    :: PsiFile
    integer  :: uid, nRows,nCols, i, j
    type(ClassMatrix), intent(inout) :: DipMat
    open( newunit=uid, File=PsiFile, form="formatted", status="old" )
    read(uid,*) nRows,nCols
    call DipMat%InitFull(nRows,nCols)
    do j = 1,nCols
      do i = 1,nRows
        read(uid,*) DipMat%A(i,j)
      enddo
    enddo
    close(uid)

  end subroutine LoadDipole


  subroutine SaveAmplitudes( Amplitudes, vnOpen1, vnOpen2, Egrid1, Egrid2, OutDir, fname )
    complex(kind(1d0)), intent(in) :: Amplitudes(:,:,:,:)
    integer           , intent(in) :: vnOpen1(:),vnOpen2(:)
    real(kind(1d0))   , intent(in) :: Egrid1(:),Egrid2(:)
    character(len=*)  , intent(in) :: OutDir
    character(len=*) ,  intent(in) :: fname

    integer  :: uid, iostat, iEn1, iEn2, nOpen1, nOpen2, iOpen1, iOpen2
    open( newunit=uid, File=trim(OutDir)//'/'//trim(fname), form="formatted", status="replace" )
    write(uid,*)"# ",size(Egrid1), size(Egrid2)
    do iEn1=1,size(Egrid1)
       nOpen1=vnOpen1(iEn1)
      do iEn2=1,size(Egrid2)
         nOpen2=vnOpen2(iEn2)
        write(uid,"(e24.16,x,e24.16,x,i0,x,i0)") Egrid1(iEn1),Egrid2(iEn2), nOpen1, nOpen2
        do iOpen1 = 1,nOpen1
          write(uid,"(*(x,e24.16))") (real(Amplitudes(iEn1,iEn2,iOpen1,iOpen2)),aimag(Amplitudes(iEn1,iEn2,iOpen1,iOpen2)),iOpen2=1,nOpen2)
        enddo
      enddo
    enddo
    close(uid)

  end subroutine SaveAmplitudes

end program ComputeConConDipoleTransitions


