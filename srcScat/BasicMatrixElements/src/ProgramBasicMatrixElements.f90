!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
! {{{ Detailed description

!> \mainpage Program BasicMatrixElements
!! BasicMatrixElements computes single-particle integrals on a numerical basis.
!! 
!! Synopsis:
!! ---------
!!
!!     BasicMatrixElements -gif File
!!
!! File is a general input file as described in [GeneralInputFile](@ref GeneralInputFile)
!! ___
!! Description:
!! ------------
!! Given a specified numerical radial basis \f$\chi_n(r)\f$, which can
!! be either a purely [B-spline](http://iopscience.iop.org/0034-4885/64/12/205) 
!! basis  or a mixed Gaussian/B-spline basis,
!! the program computes the following radial matrix elements:
!! \f{eqnarray*}
!! S_{nm}\equiv\left\langle \chi_{n}|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\chi_{m}(r),\\
!! R_{nm}\equiv\left\langle \chi_{n}\left|r\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)r\chi_{m}(r),\\
!! C_{nm}\equiv\left\langle \chi_{n}\left|\frac{1}{r}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{1}{r}\chi_{m}(r),\\
!! L_{nm}\equiv\left\langle \chi_{n}\left|\frac{1}{r^{2}}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{1}{r^{2}}\chi_{m}(r),\\
!! D_{nm}\equiv\left\langle \chi_{n}\left|\frac{d}{dr}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{d}{dr}\chi_{m}(r),\\
!! K_{nm}\equiv\left\langle \chi_{n}\left|\frac{d^{2}}{dr^{2}}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{d^{2}}{dr^{2}}\chi_{m}(r),\\
!! (RD)_{nm}\equiv\left\langle \chi_{n}\left|r\frac{d}{dr}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)r\frac{d}{dr}\chi_{m}(r)
!! \f}
!! and save them on disk in the file [matint](@ref matint), inside the directory specified in the configuration files. Also in he same directory but in the folder "plots", the basis functions are plotted if it was requested in the file [Basis](@ref Basis). 
!!
!! Configuration Files:                 {#Input_Files}
!! ====================
!! [GeneralInputFile](@ref GeneralInputFile) as specified in the command line
!! 
!> \file
!! BasicMatrixElements computes single-particle integrals on a numerical basis.
!!
!!
!! Given a specified numerical radial basis \f$\chi_n(r)\f$, which can
!! be either a purely [B-spline](http://iopscience.iop.org/0034-4885/64/12/205) 
!! basis  or a mixed Gaussian/B-spline basis,
!! the program computes the following radial matrix elements:
!! \f{eqnarray*}
!! S_{nm}\equiv\left\langle \chi_{n}|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\chi_{m}(r),\\
!! R_{nm}\equiv\left\langle \chi_{n}\left|r\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)r\chi_{m}(r),\\
!! C_{nm}\equiv\left\langle \chi_{n}\left|\frac{1}{r}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{1}{r}\chi_{m}(r),\\
!! L_{nm}\equiv\left\langle \chi_{n}\left|\frac{1}{r^{2}}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{1}{r^{2}}\chi_{m}(r),\\
!! D_{nm}\equiv\left\langle \chi_{n}\left|\frac{d}{dr}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{d}{dr}\chi_{m}(r),\\
!! K_{nm}\equiv\left\langle \chi_{n}\left|\frac{d^{2}}{dr^{2}}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)\frac{d^{2}}{dr^{2}}\chi_{m}(r),\\
!! (RD)_{nm}\equiv\left\langle \chi_{n}\left|r\frac{d}{dr}\right|\chi_{m}\right\rangle =\intop_{0}^{\infty}drr^{2}\chi_{n}(r)r\frac{d}{dr}\chi_{m}(r)
!! \f}
!! and save them on disk in the file [matint](@ref matint), inside "store/Basis/" directory. Also in he same directory but in the folder "plots", the basis functions are plotted if it was required in the file [Basis](@ref Basis). 
!! 
!! Synopsis:
!! ---------
!!
!!     BasicMatrixElements -gif File
!!
!! File is a general input file as described in [GeneralInputFile](@ref GeneralInputFile)
!!
!! Configuration Files:                 {#Input_Files}
!! ====================
!! [GeneralInputFile](@ref GeneralInputFile) as specified in the command line
!! 

! }}} 
program ProgramBasicMatrixElements


  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleSystemUtils
  use ModuleErrorHandling
  use ModuleString
  use ModuleGeneralInputFile
  use ModuleBasis
  use ModuleBasicMatrixElements
  use ModuleAbsorptionPotential


  implicit none


  character(len=*), parameter    :: NEW_LINE_CHARACTER = ACHAR(10)
  character(len=*), parameter    :: sPar = NEW_LINE_CHARACTER//" -- "
  character(len=*), parameter    :: sInd = "    "

  !.. Run time parameters 
  character(len=:), allocatable  :: GeneralInputFile
  logical                        :: ForceComputation
  logical                        :: FetchGaussian
  character(len=:), allocatable  :: EffectivePotentialFile

  !.. Local variables
  type(ClassBasis)               :: Basis
  type(ClassBasicMatrixElements) :: BasicMatrixElements
  type(ClassAbsorptionPotential) :: AbsorptionPotential
  character(len=:), allocatable  :: ConfigurationFilesCopyDir
  character(len=500)             :: BasisDir, FileName
  DoublePrecision                :: Time
  character(len=:), allocatable  :: FetchedGaussianFile
  character(len=:), allocatable  :: SAEstoredir
  character(len=:), allocatable  :: SAEBasisFile
  !

  !.. Read run-time parameters
  call GetRunTimeParameters(    &
       GeneralInputFile,        &
       ForceComputation,        &
       FetchGaussian,           &
       EffectivePotentialFile   )
  !
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "General Input File : "//GeneralInputFile
  write(OUTPUT_UNIT,"(a,L)" ) "Force Computation : ", ForceComputation 
  if(allocated(EffectivePotentialFile)) write(OUTPUT_UNIT,"(a)" ) "Effective Potential File : "//EffectivePotentialFile

  call SAEGeneralInputData%ParseFile( GeneralInputFile )
  call SAEGeneralInputData%GetStore( SAEstoredir )
  call FormatDirectoryName( SAEstoredir ) 
  
  call SAEGeneralInputData%GetBasisFile( SAEBasisFile )
  call Basis%Init( SAEBasisFile )
  call Basis%SetRoot( SAEstoredir )

  !.. Set the storage directory
  BasisDir=Basis%GetDir()
  call FormatDirectoryName( BasisDir )
  call SYSTEM("mkdir -p "//trim(BasisDir))
  

  !.. Save the current basis configuration
  allocate( ConfigurationFilesCopyDir, source = trim(BasisDir)//"etc/" )
  call SYSTEM("mkdir -p "//ConfigurationFilesCopyDir)


  if ( FetchGaussian ) then
     !
     call FetchPrimitiveGaussians( Basis, BasisDir, FetchedGaussianFile )
     write(OUTPUT_UNIT,"(a)") " Only the primitive Gaussian were fetched, the matrix elements were not computed." 
     stop
     !
  end if


  !.. Plot the basis, if required
  if( Basis%ToBePlotted() )then
     write(OUTPUT_UNIT,"(a)",advance="no") sInd//"Plot Basis.." 
     call SYSTEM("mkdir -p "//trim( BasisDir )//"plots/")
     Time=GetTime()
     call Basis%Plot( trim( BasisDir )//"plots/" )
     Time=GetTime()-Time
     write(OUTPUT_UNIT,"(a)") ".completed. Time : "//ConvertToStrn(Time)//" s"
  endif


  !.. Computes the basic integrals, if not already present
  call BasicMatrixElements%Init( Basis )
  write(OUTPUT_UNIT,"(a)") sPar//"Basic Matrix Elements"
  write(OUTPUT_UNIT,"(a)",advance="no") sInd//"Check file in '"//Basis%GetDir()//"' .."
  if( .not. BasicMatrixElements%Saved( Basis%GetDir() ) .or. ForceComputation )then
     write(OUTPUT_UNIT,"(a)") ".. Computation required "
     write(OUTPUT_UNIT,"(a)",advance="no") sInd//"Compute Basic Matrices.."
     Time=GetTime()
     !
     if ( allocated(EffectivePotentialFile) ) then
        call BasicMatrixElements%Compute( EffectivePotentialFile )
     else
        call BasicMatrixElements%Compute( )
     end if
     Time=GetTime()-Time
     write(OUTPUT_UNIT,"(a)") ".completed. Time : "//ConvertToStrn(Time)//" s"
     write(OUTPUT_UNIT,"(a)") sInd//"Save Basic Matrices"
     call BasicMatrixElements%Save( Basis%GetDir() )
  else
     write(OUTPUT_UNIT,"(a)") ".. stored basic matrices are valid."
  endif


  !.. Parse the absorption potential file
  write(OUTPUT_UNIT,"(a)") sPar//"Absorption Potential "
  write(OUTPUT_UNIT,"(a)") sInd//"Parse Input File = '"&
       //trim(adjustl(AbsorptionPotentialFile))//"'"
  call AbsorptionPotential%Parse( AbsorptionPotentialFile )
  call SYSTEM("cp "//trim( AbsorptionPotentialFile )//" "//trim(BasisDir)//"etc/")
  !.. Computes the absorption potentials, if necessary
  call AbsorptionPotential%Init( Basis )
  write(OUTPUT_UNIT,"(a)",advance="no") sInd//"Compute.."
  Time=GetTime()
  call AbsorptionPotential%ComputeAndSave( BasisDir )
  Time=GetTime()-Time
  write(OUTPUT_UNIT,"(a)") ".completed. Time : "//ConvertToStrn(Time)//" s"
  write(OUTPUT_UNIT,"(a)")
  write(OUTPUT_UNIT,'(a)') "Done"

contains

  !> Reads the run time parameters specified in the command line.
  subroutine GetRunTimeParameters(   &
       GeneralInputFile,             &
       ForceComputation,             &
       FetchGaussian,                &
       EffectivePotentialFile        )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    !> [GeneralInputFile](@ref GeneralInputFile)  
    character(len=:), allocatable, intent(out) :: GeneralInputFile
    logical,                       intent(out) :: ForceComputation
    logical                                    :: FetchGaussian
    character(len=:), allocatable              :: EffectivePotentialFile

    !
    character(len=100)                         :: gif, vif
    character(len=*), parameter                :: PROGRAM_DESCRIPTION =&
         "Computes from a given basis (B-Splines, Gaussians, Mixed Gaussians/B-Splines), "//&
         "the basic matrix elements that are required to build the scalar product "//&
         "within the basis as well as the representation of all the operators that are "//&
         "required to describe stationary states and time evolution of an hydrogenic "//&
         "ion, possible in the presence of external radiation and absorption boundaries."
    type( ClassCommandLineParameterList ) :: List

!
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "General Input File",  " ", "required" )
    call List%Add( "-force"  , "Force to recompute all integrals" )
    call List%Add( "-fetchgaussian", "Fetches the gaussian functions info" )
    call List%Add( "-vif", "Effective potential input file", " ", "optional" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    call List%Get( "-gif" , gif )
    ForceComputation = List%Present( "-force" )
    FetchGaussian = List%Present( "-fetchgaussian" )
    if ( List%Present( "-vif" ) ) then 
       call List%Get( "-vif", vif )
       allocate( EffectivePotentialFile, source = trim( adjustl( vif ) ) )
    end if
    !
    call List%Free()
    !
    allocate( GeneralInputFile, source = trim( adjustl( gif ) ) )
    !
  end subroutine GetRunTimeParameters

  subroutine FetchPrimitiveGaussians( Basis, BasisDir, FetchedGaussianFile)
    !
    type(ClassBasis),              intent(in)     :: Basis
    character(len=*),              intent(in)     :: BasisDir
    character(len=:), allocatable, intent(inout)  :: FetchedGaussianFile
    !
    !> Monomial exponents.
    integer, allocatable :: MonExp(:)
    !> Gaussian exponents.
    real(kind(1d0)), allocatable :: GaussExp(:)
    !> Normalization factors.
    real(kind(1d0)), allocatable :: NormFactor(:)
    integer :: uid, iostat, i, j, k
    character(len=IOMSG_LENGTH) :: iomsg
    real(kind(1d0)), allocatable :: Vector(:)
    !
    call Basis%GetLinIndGaussian( MonExp, GaussExp, NormFactor )
    !
    allocate ( FetchedGaussianFile, source = trim(BasisDir)//"FetchedGaussianPrimitives" )
    !
    open(&
         Newunit =  uid       , &
         File    =  FetchedGaussianFile  , &
         Status  = "unknown"  , &
         Action  = "write"    , &
         Form    = "FORMATTED", &
         iostat  =  iostat    , &
         iomsg   =  iomsg     )
    !
    write(unit=uid, fmt=*) 0.d0, Basis%GetGaussianLMax()
    !
    do k = 0, Basis%GetGaussianLMax()
       !
       write(unit=uid, fmt=*) Basis%GetGaussianNumExponents(), Basis%GetGaussianNumExponents()
       !
       do i = 1, Basis%GetGaussianNumExponents()
          !
          write(unit=uid, fmt=*) GaussExp(i)
          !
       end do
       !
       do i = 1, Basis%GetGaussianNumExponents()
          !
          allocate( Vector(Basis%GetGaussianNumExponents()) )
          Vector = 0.d0
          Vector(i) = 1.d0
          !
          write(unit=uid, fmt=*) (Vector(j), j=1,size(Vector))
          !
          deallocate( Vector)
          !
       end do
       !
    end do
    !
    close( uid )
    !
  end subroutine FetchPrimitiveGaussians


end program ProgramBasicMatrixElements
