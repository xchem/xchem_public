!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program TestModuleErrorHandling
  implicit none
  external FaultySubroutine
  call FaultySubroutine
  stop
end program TestModuleErrorHandling


subroutine FaultySubroutine()
  !
  use, intrinsic :: ISO_FORTRAN_ENV
  use ModuleErrorHandling
  !
  implicit none
  !
  character(len=*), parameter :: ROUTINE_NAME = "FaultySubroutine"
  character(len=*), parameter :: WHERE_I_AM = ROUTINE_NAME
  integer :: fpt 
  integer :: stat
  
  !.. Check assert on assigned default unit
  !..
  open(newunit=fpt,file="test.log",form="formatted",status="unknown")
  call Assertion%SetDefaultLevelToWarning
  call Assertion%SetDefaultUnit( fpt, stat )
  if(stat/=0)&
       call ASSERT( WHERE_I_AM//" Invalid assertion unit", assertion%LEVEL_FATAL )
  call Assert( WHERE_I_AM//" chk1" )
  call Assert( WHERE_I_AM//" chk2", assertion%LEVEL_SEVERE )
  call Assert( WHERE_I_AM//" test_Unit", Unit=ERROR_UNIT )

  !.. Check assert when default unit becomes invalid
  close(fpt)
  call Assert( WHERE_I_AM//" chk3" )
  
  !.. Check assert when required unit is invalid
  call assertion%ResetDefaultUnit
  call assertion%SetDefaultLevelToFatal
  call Assert( WHERE_I_AM//" chk4", Unit=fpt )
  
  return
end subroutine FaultySubroutine
