!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program testModuleParentIons
  
  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING
  
  use ModuleParentIons
  use ModuleGroups
  
  implicit none
  
  character(len=*), parameter :: GROUP_NAME  = "D2h"
  character(len=*), parameter :: PION_FILE   = "ParentIons"
  character(len=*), parameter :: STORAGE_DIR = "../../DataStructure/InterfaceData/R1.000"
  
  type(ClassGroup)        :: Group
  type(ClassParentIonSet) :: ParentIons
  
  call Group.Init( GROUP_NAME )

  call ParentIons.SetGroup( Group )
  call ParentIons.parseFile( PION_FILE )
  call ParentIons.LoadData( STORAGE_DIR, "1" ) ! "i" is the parent ion index.
  
  call ParentIons.show()
  
  
end program testModuleParentIons
