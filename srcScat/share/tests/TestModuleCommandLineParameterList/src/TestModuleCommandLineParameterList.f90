!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

!> Test functionality of the Module that handles
!! run time parameters 
program TestModuleCommandLineParameters

  use ISO_FORTRAN_ENV
  
  implicit none
  
  logical            :: Force, Verbous
  integer            :: NEner
  real(kind(1d0))    :: Emin, Emax
  character(len=100) :: FLabel

  call GetRunTimeParameters(& 
       Force              , &
       Verbous            , &
       Emin               , &
       Emax               , &
       Nener              , &
       FLabel             )

  write(OUTPUT_UNIT,*) Force
  write(OUTPUT_UNIT,*) Verbous
  write(OUTPUT_UNIT,*) Emin
  write(OUTPUT_UNIT,*) Emax
  write(OUTPUT_UNIT,*) Nener
  write(OUTPUT_UNIT,*) trim(FLabel)
  
  stop

contains
  
 
  subroutine GetRunTimeParameters(&
       ForceCalculation, &
       VerbousOutput   , &
       Emin            , &
       Emax            , &
       nener           , &
       Label             &
       )
    use ModuleCommandLineParameterList
    implicit none
    logical         , intent(out) :: ForceCalculation
    logical         , intent(out) :: VerbousOutput
    real(kind(1d0)) , intent(out) :: Emin
    real(kind(1d0)) , intent(out) :: Emax
    integer         , intent(out) :: nener
    character(len=*), intent(out) :: Label
    !
    type( ClassCommandLineParameterList ) :: List
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "This is a test program for the module "//&
         "ModuleCommandLineParameterList"
    !
    call List.SetDescription(PROGRAM_DESCRIPTION)
    call List.Add( "--help"   , "Print Command Usage" )
    call List.Add( "--force"  , "Force Calculation"   )
    call List.Add( "--verbous", "Set verbous Output"  )
    call List.Add( "-Emin"    , "Minimum Energy"    , 0.d0, "required" )
    call List.Add( "-Emax"    , "Maximum Energy"    , 0.d0, "required" )
    call List.Add( "-ne"      , "Number of energies",   10, "optional" )
    call List.Add( "-Label"   , "File Suffix"       ,  " ", "optional" )
    call List.Parse()
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if
    ForceCalculation = List.Present("--force")
    VerbousOutput    = List.Present("--verbous")
    call List.Get( "-Emin" , Emin  )
    call List.Get( "-Emax" , Emax  )
    call List.Get( "-ne"   , nener )
    call List.Get( "-Label", Label )
    !
  end subroutine GetRunTimeParameters


  !> Write a message to standard error
  subroutine ErrorMessage(Message)
    use ISO_FORTRAN_ENV
    implicit none
    character(len=*), intent(in) :: Message
    write(ERROR_UNIT,"(a)") Message
    return
  end subroutine ErrorMessage


end program TestModuleCommandLineParameters
