!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************


!.. Test program for the calculation of a particle in a central
!   potential on a mixed basis of gaussian and B-spline functions
!..
program main
  !
  !
  use omp_lib
  use ModuleGaussian
  use spline
  use gaspmodule
  use MyLapackModule
  !
  !
  implicit none
  
  real(kind(1d0)), parameter :: PI=3.141592653589793238d0
  
  !.. NALPHA : Number of gaussian exponents
  !..
  !.. NPOLY  : Number of monomial factors  
  !            1, r, r^2, ..., r^(NPOLY-1)
  !..
  integer        , parameter   :: NALPHA    = 10
  integer        , parameter   :: NPOLY     = 12
  real(kind(1d0)), parameter   :: Min_Alpha = 1.d-2
  real(kind(1d0)), parameter   :: Max_Alpha = 30.d0
  real(kind(1d0)), parameter   :: Delta = 0.1d0
  real(kind(1d0))              :: Alpha_Scale_Factor
  real(kind(1d0))              :: Alpha
  type(ClassGaussian), pointer :: Gauss(:)
  !
  !.. Polynomial Gaussian Family
  !..
  real(kind(1d0))       :: alpha_vec(NALPHA)
  
  
  !.. GASP_Ga_NTOT : Number of total functions = NALPHA * NPOLY
  !..
  !.. GASP_Ga_NLIF : Number of Linearly Independent Functions
  !..
  integer                      :: GASP_Ga_NTOT, GASP_Ga_NLIF 
  integer                      :: GASP_NTOT, GASP_NLIF
  real(kind(1d0)), allocatable :: GASP_S(:,:), GASP_ES(:),GASP_T(:,:)
  real(kind(1d0)), allocatable :: GASP_H(:,:), GASP_EH(:),GASP_C(:,:)
  real(kind(1d0)), allocatable :: Sgg(:,:),Tg(:,:)
  real(kind(1d0)), allocatable :: ESg(:)
  
  integer         :: i, j, j_, k
  real(kind(1d0)) :: Momentum
  real(kind(1d0)), allocatable :: mat1(:,:)
  
  character(len=32 ) :: nstrn
  character(len=128) :: FileName
  character(len=128) :: FileRoot = "gauss"
  
  
  !.. Variables for matrix diagonalization
  !..
  integer                      :: INFO
  integer                      :: LWORK
  real(kind(1d0)), allocatable :: WORK(:)
  
  
  !.. Plot Grid parameters
  !..
  integer                      :: igrid
  integer        , parameter   :: NGRID=500
  real(kind(1d0)), parameter   :: Rmin= 0.d0
  real(kind(1d0)), parameter   :: Rmax=25.d0
  real(kind(1d0)), allocatable :: RGrid(:)
  real(kind(1d0)), allocatable :: FunGrid(:)
  real(kind(1d0)), allocatable :: Fun1Grid(:)
  real(kind(1d0)), allocatable :: Fun2Grid(:)
  real(kind(1d0)), allocatable :: RefGrid(:)
  real(kind(1d0))              :: w1,w2
  
  
  !.. Bspline parameters
  !
  !   GASP_Bs_NTOT : total number of B-splines considered
  !           (we exclude the first and the last 
  !            Bs_Set%k-1 functions to ensure maximal
  !            regularity)
  !
  !   NLIB  : Number of B-spline which are linearly
  !           independent *FROM THE GAUSSIAN FUNCTIONS*
  !          (B-splines by themselves are pretty much
  !           a linearly independent set)
  !..
  type(spline_type)            :: Bs_Set
  integer                      :: Bs_i, Bs_j, Bs_info
  integer                      :: GASP_Bs_NTOT, NLIB
  real(kind(1d0)), parameter   :: Bs_Rmin = 15.d0
  real(kind(1d0)), parameter   :: Bs_Rmax = 100.d0
  integer        , parameter   :: Bs_nnod = 601
  integer        , parameter   :: Bs_Order=   6
  real(kind(1d0))              :: Bs_vec(Bs_nnod+Bs_Order-2)
  
  !.. Bs_Beg(End)_Skip : number of B-splines skipped at 
  !      the beginning (end)
  !..
  integer        , parameter   :: Bs_Beg_Skip=   3
  integer        , parameter   :: Bs_End_Skip=   1
  real(kind(1d0)), allocatable :: Bs_grid(:)
  !
  !.. Sbb : Overlap matrix between B-splines 
  !         $S_{BB} = U \Lambda U\dagger,\qquad
  !         \Lambda_{ij}=\lambda_i\delta_{ij}$ 
  !
  !   Tb  : Transformation matrix for B-splines
  !         $T_B = U \Lambda^{-1/2}$
  !
  !   ESb : Eigenvalues of the overlap matrix
  !         $\lambda_i$
  !..
  real(kind(1d0)), allocatable :: Sbb(:,:), Tb(:,:), ESb(:)
  !
  !
  !.. Gaussian - B-spline variables
  !..
  !.. Overlap matrix between gaussian functions and
  !   B-splines
  !..
  real(kind(1d0)), allocatable :: Sgb(:,:)
  !
  real(kind(1d0)), allocatable :: ESp(:)
  !
  !.. GASP_Bs_NLIF
  !   Number of B-spline based functions that are deemed 
  !   to be sufficiently linearly independent of the 
  !   orthogonalized set of gaussian functions as to be
  !   be safely used in the building of the GASP basis
  !..
  integer                    :: GASP_Bs_NLIF
  real(kind(1d0)), allocatable :: GASP_Ga_F(:,:)
  !
  integer :: i_alpha
  !
  !
  !.. Comparison B-splines
  !.. 
  type(spline_type)            :: Bs_Comp_Set
  integer                      :: Bs_Comp_NTOT
  integer        , parameter   :: Bs_Comp_nnod = 401
  integer        , parameter   :: Bs_Comp_Order=   6
  real(kind(1d0))              :: Bs_Comp_vec(Bs_Comp_nnod+Bs_Comp_Order-2) 
  integer        , parameter   :: Bs_Comp_Beg_Skip=1
  integer        , parameter   :: Bs_Comp_End_Skip=1
  real(kind(1d0)), allocatable :: Bs_Comp_grid(:),Bs_Comp_ES(:)
  real(kind(1d0)), allocatable :: Hbb(:,:),Cbb(:,:)
  !
  !.. Fitting parameters
  !..
  integer        , parameter :: NFIT = 10
  real(kind(1d0))            :: FunFit( NFIT )
  real(kind(1d0))            :: Fun1Fit( NFIT )
  real(kind(1d0))            :: Fun2Fit( NFIT )
  real(kind(1d0))            :: RefFit( NFIT )
  real(kind(1d0))            :: RFit( NFIT )
  real(kind(1d0))            :: RFit_Min,RFit_Max
  !
  real(kind(1d0)), parameter :: Z_Asymptotic = 8.d0
  !
  real(kind(1d0)) :: Bs_Comp_Momentum
  real(kind(1d0)) :: Bs_Comp_NormFact
  real(kind(1d0)) :: Bs_Comp_Phase_Shift
  real(kind(1d0)) :: Bs_Comp_freg
  real(kind(1d0)) :: Bs_Comp_firr
  real(kind(1d0)) :: Bs_Comp_Delta
  real(kind(1d0)) :: Bs_Comp_fcoe
  real(kind(1d0)) :: Bs_Comp_gcoe
  !
  real(kind(1d0)) :: GASP_Momentum
  real(kind(1d0)) :: GASP_NormFact
  real(kind(1d0)) :: GASP_Phase_Shift
  real(kind(1d0)) :: GASP_freg
  real(kind(1d0)) :: GASP_firr
  real(kind(1d0)) :: GASP_Delta
  real(kind(1d0)) :: GASP_fcoe
  real(kind(1d0)) :: GASP_gcoe


  !.. Set the exponent vector
  !..
  Alpha_Scale_Factor = exp(log(Max_Alpha/Min_Alpha)/dble(NALPHA-1))
  Alpha_vec(1) = Min_Alpha
  do i=2,NALPHA
     Alpha_vec(i) = Alpha_vec(i-1) * Alpha_Scale_Factor
  enddo



  !.. Initializes the vector of even tempered exponents
  !..
  Alpha_Scale_Factor = exp(log(Max_Alpha/Min_Alpha)/dble(NALPHA-1))
  !
  GASP_Ga_NTOT=NALPHA*NPOLY
  allocate(Gauss(GASP_Ga_NTOT))
  allocate(GASP_Ga_F(NPOLY,NALPHA))
  k=0
  do i=1,NALPHA
     !
     do j=0,NPOLY-1
        !
        !.. The family of Polynomial Gaussian functions
        !   includes only those with even powers of r,
        !   in agreement with the even parity of the state
        !   considered in the present program prototype.
        !   As a consequence, the reduced radial functions
        !   u(r) = r R(r) will contain only *odd* powers
        !   of r. Hence, the expansions in terms of 
        !   Hermite polynomials times gaussian functions
        !   must run only over odd polynomials.
        !   Conversely, for odd angular momentum states,
        !   which must be considered in the general case,
        !   even hermite polynomials must be used instead.
        !..
        j_= 2*j
        k = k + 1
        call CreateClassGaussian(Gauss(k),Alpha_vec(i),j_)
        GASP_Ga_F(j+1,i)=Gauss(k)%F
        !
     enddo
     !
  enddo


  !.. Initializes the B-spline basis set
  !..
  allocate(Bs_grid(Bs_nnod))
  do i=1,Bs_nnod
     !
     Bs_grid(i)=Bs_Rmin+(Bs_Rmax-Bs_Rmin)*dble(i-1)/dble(Bs_nnod-1)
     !
  enddo
  !
  Bs_info=init_spline(Bs_nnod,Bs_Order,Bs_grid,Bs_Set)
  !
  if(Bs_info/=0)then
     !
     write(*,"(a,i4)") "(EEE) - B-spline intialization error ",Bs_info
     write(*,"(a)") "STOP FORCED"
     stop
     !
  endif


  !.. Initializes the B-spline overlap matrix.
  !   We leave out a predefined number of initial
  !   B-spline functions, to grant a minimal 
  !   level of regularity, and the last B-spline
  !   so that the kinetic energy is hermitian.
  !..
  GASP_Bs_NTOT = Bs_Set%t - ( Bs_Beg_Skip + Bs_End_Skip )
  allocate(Sbb(GASP_Bs_NTOT,GASP_Bs_NTOT))
  Sbb=0.d0
  do i=1,GASP_Bs_NTOT
     !
     Bs_i = i + Bs_Beg_Skip
     !
     do j=i,min(GASP_Bs_NTOT,i+Bs_Set%k-1)
        !
        Bs_j = j + Bs_Beg_Skip
        !
        Sbb(i,j)=4.d0*PI*Spline_Integral(Bs_i,Bs_j,0,0,0,Bs_Set)
        Sbb(j,i)=Sbb(i,j)
        !
     enddo
     !
  enddo


  !.. Initializes the Gaussian overlap matrix
  !.. 
  allocate(Sgg(GASP_Ga_NTOT,GASP_Ga_NTOT))
  !
  Sgg=0.d0
  !
  do j=1,GASP_Ga_NTOT
     !
     do i=1,GASP_Ga_NTOT
        !
        Sgg(i,j)=GaussOverlap(Gauss(i),Gauss(j))
        !
     enddo
     !
  enddo


  !.. Build the Gaussian-Bspline overlap matrix
  !..
  allocate(Sgb(GASP_Ga_NTOT,GASP_Bs_NTOT))
  Sgb=0.d0
  !
  do i=1,GASP_Ga_NTOT
     !
     do j=1,GASP_Bs_NTOT
        !
        Bs_j = j + Bs_Beg_Skip
        !
        Sgb(i,j) = 4.d0 * PI * GaspIntegral( Gauss(i), Bs_j, 0, 0, Bs_Set )
        !
     enddo
     !
  enddo


  !.. Build the regularized matrix
  !.. 
  !.. Initialize the vectors with the
  !   eigenvalues of the two gauss-gauss
  !   and spline-spline overlap matrices
  !   and of the projector.
  !..
  allocate( ESg( GASP_Ga_NTOT ) )
  allocate( ESb( GASP_Bs_NTOT ) )
  allocate( ESp( GASP_Bs_NTOT ) )
  !
  ESp = 0.d0
  ESb = 0.d0
  ESg = 0.d0
  !
  !.. Eigenvectors of the orthogonalized gaussian
  !   and B-spline functions
  !..
  allocate( Tg ( GASP_Ga_NTOT, GASP_Ga_NTOT ) )
  allocate( Tb ( GASP_Bs_NTOT, GASP_Bs_NTOT ) )
  !
  Tg = Sgg
  Tb = Sbb
  !
  call GaspOrthogon(                          &
       Tg , GASP_Ga_NTOT , ESg, GASP_Ga_NLIF, &
       Tb , GASP_Bs_NTOT , ESb,               &
       Sgb, GASP_Ga_NTOT , ESp, GASP_Bs_NLIF  )



  !.. Build the total superposition matrix
  !..
  GASP_NLIF = GASP_Ga_NLIF + GASP_Bs_NLIF 
  allocate( GASP_S( GASP_NLIF, GASP_NLIF ) )
  GASP_S = 0.d0
  !
  !.. Gauss-gauss block
  !   The gauss-gauss block is just the
  !   identity in the GASP_LIF basis
  !..
  do i = 1, GASP_Ga_NLIF
     !
     GASP_S(i,i)=1.d0
     !
  enddo
  !
  !.. gauss-bspline block
  !..
  allocate(mat1(GASP_Ga_NLIF,GASP_Bs_NTOT))
  mat1=0.d0
  call DGEMM("T","N",GASP_Ga_NLIF,GASP_Bs_NTOT,GASP_Ga_NTOT,&
       1.d0, Tg, GASP_Ga_NTOT, Sgb, GASP_Ga_NTOT, &
       0.d0, mat1, GASP_Ga_NLIF)
  call DGEMM("N","N",GASP_Ga_NLIF,GASP_Bs_NLIF,GASP_Bs_NTOT,&
       1.d0, mat1, GASP_Ga_NLIF, Tb, GASP_Bs_NTOT,&
       0.d0, GASP_S(1,GASP_Ga_NLIF+1),GASP_NLIF)
  GASP_S(GASP_Ga_NLIF+1:,1:GASP_Ga_NLIF)=&
       transpose(GASP_S(1:GASP_Ga_NLIF,GASP_Ga_NLIF+1:))
  deallocate(mat1)
  !
  !.. spline-spline block
  !..
  allocate(mat1(GASP_Bs_NLIF,GASP_Bs_NTOT))
  mat1=0.d0
  call DGEMM("T","N",GASP_Bs_NLIF,GASP_Bs_NTOT,GASP_Bs_NTOT,&
       1.d0, Tb, GASP_Bs_NTOT, Sbb, GASP_Bs_NTOT, &
       0.d0, mat1, GASP_Bs_NLIF)
  call DGEMM("N","N",GASP_Bs_NLIF,GASP_Bs_NLIF,GASP_Bs_NTOT,&
       1.d0, mat1, GASP_Bs_NLIF, Tb, GASP_Bs_NTOT,&
       0.d0, GASP_S(GASP_Ga_NLIF+1,GASP_Ga_NLIF+1),GASP_NLIF)
  deallocate(mat1)



  !.. Diagonalize the total superposition matrix
  !   and build the transformation matrix
  !..
  allocate(GASP_ES(GASP_NLIF))
  GASP_ES=0.d0
  call mydsyev(GASP_NLIF,GASP_S,GASP_NLIF,GASP_ES)
  !
  do i=1,GASP_NLIF
     !
     write(*,"(a,i4,a,d24.16)") "GASP_ES(",i,")=",GASP_ES(i)
     !
     GASP_S(:,i) = GASP_S(:,i) / sqrt( GASP_ES(i) )
     !
  enddo
  !
  !.. Transform the transformation matrix on the original
  !   basis
  !..
  GASP_NTOT = GASP_Ga_NTOT + GASP_Bs_NTOT
  allocate(GASP_T(GASP_NTOT,GASP_NLIF))
  GASP_T=0.d0
  !
  !.. Eigenvector components on the pristine Gaussian states
  !..
  call DGEMM("N","N",GASP_Ga_NTOT,GASP_NLIF,GASP_Ga_NLIF,&
       1.d0, Tg, GASP_Ga_NTOT, GASP_S, GASP_NLIF, &
       0.d0, GASP_T(1,1),GASP_NTOT)
  !
  !.. Eigenvector components on the pristine B-spline states
  !..
  call DGEMM("N","N",GASP_Bs_NTOT,GASP_NLIF,GASP_Bs_NLIF,&
       1.d0, Tb, GASP_Bs_NTOT, GASP_S(GASP_Ga_NLIF+1,1), GASP_NLIF, &
       0.d0, GASP_T(GASP_Ga_NTOT+1,1),GASP_NTOT)
  !
  deallocate(GASP_S)


  !.. Build the hamiltonian matrix on the
  !   pristine basis
  !..
  allocate(GASP_H(GASP_NTOT,GASP_NTOT))
  GASP_H = 0.d0
  !
  !.. Gauss-gauss block
  !..
  do j = 1, GASP_Ga_NTOT
     !
     do i = 1, j
        !
        GASP_H(i,j) = GaussKinetic(Gauss(i),Gauss(j))
        !
        GASP_H(i,j) = GASP_H(i,j) - Z_Asymptotic * GaussPotential(Gauss(i),Gauss(j))
        !
        GASP_H(j,i) = GASP_H(i,j)
        !
     enddo
     !
  enddo
  !
  !.. spline-spline block
  !..
  do i = 1, GASP_Bs_NTOT
     !
     Bs_i = i + Bs_Beg_Skip
     !
     do j=i,min(GASP_Bs_NTOT,i+Bs_Set%k-1)
        !
        Bs_j = j + Bs_Beg_Skip
        !
        GASP_H( i + GASP_Ga_NTOT, j + GASP_Ga_NTOT ) = &
             -2.d0 * PI * Spline_Integral(Bs_i,Bs_j,0,0,2,Bs_Set)
        !
        GASP_H( i + GASP_Ga_NTOT, j + GASP_Ga_NTOT ) = &
             GASP_H( i + GASP_Ga_NTOT, j + GASP_Ga_NTOT ) - &
             4.d0 * PI * Z_Asymptotic * Spline_Integral(Bs_i,Bs_j,0,-1,0,Bs_Set)
        !
        GASP_H( j + GASP_Ga_NTOT, i + GASP_Ga_NTOT ) = &
             GASP_H( i + GASP_Ga_NTOT, j + GASP_Ga_NTOT )
        !
     enddo
     !
  enddo
  !
  !.. Gauss-spline blocks
  !..
  do i = 1, GASP_Ga_NTOT
     !
     do j = 1, GASP_Bs_NTOT
        !
        Bs_j = j + Bs_Beg_Skip
        !
        GASP_H( i, j + GASP_Ga_NTOT ) = -2.d0 * PI * GaspIntegral( Gauss(i), Bs_j, 0, 2, Bs_Set )
        !
        GASP_H( i, j + GASP_Ga_NTOT ) = GASP_H( i, j + GASP_Ga_NTOT ) - &
             4.d0 * PI * Z_Asymptotic * GaspIntegral( Gauss(i), Bs_j, -1, 0, Bs_Set )
        !
        GASP_H( j + GASP_Ga_NTOT, i ) = GASP_H( i, j + GASP_Ga_NTOT )
        !
     enddo
     !
  enddo


  !.. Transform the hamiltonian on the GASP linearly 
  !   independent basis
  !.. 
  allocate(mat1(GASP_NTOT,GASP_NLIF))
  mat1=0.d0
  call DGEMM("N","N",GASP_NTOT,GASP_NLIF,GASP_NTOT,&
       1.d0, GASP_H, GASP_NTOT, GASP_T, GASP_NTOT, &
       0.d0, mat1, GASP_NTOT)
  deallocate(GASP_H)
  allocate(GASP_H(GASP_NLIF,GASP_NLIF))
  GASP_H=0.d0
  call DGEMM("T","N",GASP_NLIF,GASP_NLIF,GASP_NTOT,&
       1.d0, GASP_T, GASP_NTOT, mat1, GASP_NTOT, &
       0.d0, GASP_H, GASP_NLIF)


  !.. Diagonalizes the hamiltonian matrix
  !..
  allocate(GASP_EH(GASP_NLIF))
  GASP_EH=0.d0
  call mydsyev(GASP_NLIF,GASP_H,GASP_NLIF,GASP_EH)
  !
  !.. Print on screen the eigenvalues
  !..
  open(10,file="Energies",form="formatted",status="unknown")
  open(20,file="Energies_in_line",form="formatted",status="unknown")
  write(20,"(a)",advance="no") "energies='"
  do i = 1, GASP_NLIF
     !
     write(*,"(a,i4,a,d24.16)") "GASP_EH(",i,")=",GASP_EH(i)
     write(10,"(i,f24.16)")i, GASP_EH(i)
     write(20,"(x,f14.6)",advance="no") GASP_EH(i)
     !
  enddo
  write(20,"(a)")"'"
  close(10)
  !
  !.. Transforms the eigevectors to the pristine basis
  !..
  allocate(GASP_C(GASP_NTOT,GASP_NLIF))
  GASP_C=0.d0
  call DGEMM("N","N",GASP_NTOT,GASP_NLIF,GASP_NLIF,&
       1.d0, GASP_T,GASP_NTOT,GASP_H,GASP_NLIF,&
       0.d0, GASP_C,GASP_NTOT)



  !.. Initializes the new B-spline basis set
  !..
  allocate(Bs_Comp_grid(Bs_Comp_nnod))
  do i=1,Bs_Comp_nnod
     !
     Bs_Comp_grid(i)=Bs_Rmax*dble(i-1)/dble(Bs_Comp_nnod-1)
     !
  enddo
  !
  Bs_info=init_spline(Bs_Comp_nnod,Bs_Comp_Order,Bs_Comp_grid,Bs_Comp_Set)
  !
  if(Bs_info/=0)then
     !
     write(*,"(a,i4)") "(EEE) - B-spline intialization error ",Bs_info
     write(*,"(a)") "STOP FORCED"
     stop
     !
  endif


  !.. Initializes the B-spline overlap matrix and hamiltonian matrix.
  !..
  Bs_Comp_NTOT = Bs_Comp_Set%t - ( Bs_Comp_Beg_Skip + Bs_Comp_End_Skip )
  if(allocated(Sbb))deallocate(Sbb)
  allocate(Sbb(Bs_Comp_NTOT,Bs_Comp_NTOT))
  Sbb=0.d0
  do i=1,Bs_Comp_NTOT
     !
     Bs_i = i + Bs_Comp_Beg_Skip
     !
     do j=i,min(Bs_Comp_NTOT,i+Bs_Comp_Set%k-1)
        !
        Bs_j = j + Bs_Comp_Beg_Skip
        !
        Sbb(i,j)=4.d0*PI*Spline_Integral(Bs_i,Bs_j,0,0,0,Bs_Comp_Set)
        Sbb(j,i)=Sbb(i,j)
        !
     enddo
     !
  enddo


  !.. Diagonalize the total superposition matrix
  !   and build the transformation matrix
  !..
  allocate(Bs_Comp_ES(Bs_Comp_Ntot))
  Bs_Comp_ES=0.d0
  call mydsyev(Bs_Comp_NTOT,Sbb,Bs_Comp_NTOT,Bs_Comp_Es)
  !
  do i=1,Bs_Comp_NTOT
     !
     write(*,"(a,i4,a,d24.16)") "Bs_Comp_ES(",i,")=",Bs_Comp_ES(i)
     !
     Sbb(:,i) = Sbb(:,i) / sqrt( Bs_Comp_ES(i) )
     !
  enddo


  !.. Initializes the B-spline overlap matrix and hamiltonian matrix.
  !..
  if(allocated(Hbb))deallocate(Hbb)
  allocate(Hbb(Bs_Comp_NTOT,Bs_Comp_NTOT))
  Hbb=0.d0
  do i=1,Bs_Comp_NTOT
     !
     Bs_i = i + Bs_Comp_Beg_Skip
     !
     do j=i,min(Bs_Comp_NTOT,i+Bs_Comp_Set%k-1)
        !
        Bs_j = j + Bs_Comp_Beg_Skip
        !
        Hbb(i,j)=4.d0*PI*(-0.5d0*Spline_Integral(Bs_i,Bs_j,0,0,2,Bs_Comp_Set))
        !
        Hbb(i,j) = Hbb(i,j) - &
             4.d0*PI*(Z_Asymptotic * Spline_Integral(Bs_i,Bs_j,0,-1,0,Bs_Comp_Set))
        !
        Hbb(j,i)=Hbb(i,j)
        !
     enddo
     !
  enddo


  !.. Transform the hamiltonian
  !..
  if(allocated(Cbb))deallocate(Cbb)
  allocate(Cbb(Bs_Comp_NTOT,Bs_Comp_NTOT))
  call DGEMM("N","N",Bs_Comp_NTOT,Bs_Comp_NTOT,Bs_Comp_NTOT,&
       1.d0, Hbb,Bs_Comp_NTOT,Sbb,Bs_Comp_NTOT,&
       0.d0, Cbb,Bs_Comp_NTOT)
  call DGEMM("T","N",Bs_Comp_NTOT,Bs_Comp_NTOT,Bs_Comp_NTOT,&
       1.d0, Sbb,Bs_Comp_NTOT,Cbb,Bs_Comp_NTOT,&
       0.d0, Hbb,Bs_Comp_NTOT)
  !
  !.. Diagonalize the hamiltonian
  !..
  Bs_Comp_ES=0.d0
  call mydsyev(Bs_Comp_NTOT,Hbb,Bs_Comp_NTOT,Bs_Comp_Es)
  !
  !.. Cast the eigenvectors in the original form
  !..
  call DGEMM("N","N",Bs_Comp_NTOT,Bs_Comp_NTOT,Bs_Comp_NTOT,&
       1.d0, Sbb,Bs_Comp_NTOT,Hbb,Bs_Comp_NTOT,&
       0.d0, Cbb,Bs_Comp_NTOT)
  !
  deallocate(Sbb,Hbb)
  !
  do i=1,Bs_Comp_NTOT
     !
     write(*,"(a,i4,a,d24.16)") "Bs_Comp_ES(",i,")=",Bs_Comp_ES(i)
     !
  enddo


  !.. Plot the eigenfunctions
  !..
  !
  !.. Initializes the radial grid
  !..
  allocate(RGrid(NGRID))
  do i=1,NGRID
     !
     RGrid(i)=Rmin+(Rmax-Rmin)*dble(i-1)/dble(NGRID-1)
     !
  enddo

  !.. Initializes the vectors which will contain the
  !   tabulated vaules of the wavefunction and of the
  !   exact eigenfunction at the same energy
  !..
  allocate(FunGrid(NGRID))
  allocate(Fun1Grid(NGRID))
  allocate(Fun2Grid(NGRID))
  allocate(RefGrid(NGRID))
  FunGrid=0.d0
  Fun1Grid=0.d0
  Fun2Grid=0.d0
  RefGrid=0.d0



  !.. Cycle over the wavefunctions found by 
  !   diagonalizing the hamiltonian on a mixed 
  !   basis.
  !   The functions are compared with those
  !   from a diagonalization of the hamiltonian
  !   in a purely B-spline basis.
  !..
  do i=1,min(GASP_NLIF,Bs_Comp_NTOT)
     !
     !
     !.. Tabulate the reference wave function
     !..
     Bs_Comp_vec=0.d0
     Bs_Comp_vec(1+Bs_Comp_Beg_Skip:1+Bs_Comp_Beg_Skip+Bs_Comp_NTOT)=Cbb(1:Bs_Comp_NTOT,i)
     call Tabulate_Function(NGRID,Rgrid,RefGrid,Bs_Comp_vec,Bs_Comp_Set)
     !
     !
     !.. Tabulate the wave function
     !..
     do igrid=1,NGRID
        !
        Fun1Grid(igrid)=GaussVecTabulation(Rgrid(igrid),&
             GASP_Ga_NTOT,GASP_C(:,i),Gauss)*Rgrid(igrid)
        !
     enddo
     !
     Bs_vec=0.d0
     Bs_vec(1+Bs_Beg_Skip:1+Bs_Beg_Skip+GASP_Bs_NTOT)=GASP_C(GASP_Ga_NTOT+1:GASP_NTOT,i)
     call Tabulate_Function(NGRID,Rgrid,Fun2Grid,Bs_vec,Bs_Set)
     !
     FunGrid=Fun1Grid+Fun2Grid
     !
     !.. Normalize the wave function
     !   (in the case of bound functions,
     !    set its sign so that the function
     !    is positive at the beginning)
     !..
     !
     if(FunGrid(2)<FunGrid(1))then
        !
        FunGrid =-FunGrid
        Fun1Grid=-Fun1Grid
        Fun2Grid=-Fun2Grid
        !
     endif
     !
     if(RefGrid(2)<RefGrid(1))RefGrid=-RefGrid
     !
     !.. Compare the energies
     !..   
     !
     write(*,"(a,i4,a,3(x,f26.16))") "E(",i,")=",GASP_EH(i),Bs_Comp_ES(i),GASP_EH(i)-Bs_Comp_ES(i)
     !
!!$     if(GASP_EH(i)<=0.d0)then
!!$        !
!!$        !.. Negative energies
!!$        !..
!!$        if(FunGrid(2)<FunGrid(1))then
!!$           !
!!$           FunGrid =-FunGrid
!!$           Fun1Grid=-Fun1Grid
!!$           Fun2Grid=-Fun2Grid
!!$           !
!!$        endif
!!$        !
!!$        if(RefGrid(2)<RefGrid(1))RefGrid=-RefGrid
!!$        !
!!$     else
!!$        !
!!$        !.. Positive energies
!!$        !..
!!$        !
!!$        Bs_Comp_Momentum=sqrt(2.d0*BS_Comp_Es(i))
!!$        Momentum        =sqrt(2.d0*GASP_EH(i))
!!$        !
!!$        !.. Define the appropriate fitting interval
!!$        !..
!!$        Rfit_min=max(Bs_Rmin,Bs_Rmax-2*PI/Bs_Comp_Momentum)
!!$        Rfit_max=Bs_Rmax
!!$        do j=1,NFIT
!!$           !
!!$           RFit(j)=Rfit_min+(Rfit_max-Rfit_min)*dble(j-1)/dble(NFIT-1)
!!$           !
!!$        enddo
!!$        !
!!$        !.. Plot the functions on the fitting interval
!!$        !..
!!$        !
!!$        !.. Tabulate the reference wave function
!!$        !..
!!$        call Tabulate_Function(NFIT,Rfit,RefFit,Bs_Comp_vec,Bs_Comp_Set)
!!$        !
!!$        !
!!$        !.. Tabulate the wave function
!!$        !..
!!$        do igrid=1,NFIT
!!$           !
!!$           Fun1Fit(igrid)=GaussVecTabulation(RFit(igrid),&
!!$                GASP_Ga_NTOT,GASP_C(:,i),Gauss)*Rfit(igrid)
!!$           !
!!$        enddo
!!$        !
!!$        call Tabulate_Function(NFIT,Rfit,Fun2Fit,Bs_vec,Bs_Set)
!!$        !
!!$        FunFit=Fun1Fit+Fun2Fit
!!$        !
!!$        !.. Fit the reference wavefunction in the asymptotic region
!!$        !..
!!$        call cnormenak(&
!!$             Bs_Comp_NormFact,&
!!$             Bs_Comp_Phase_Shift,&
!!$             0,&
!!$             Bs_Comp_Momentum,&
!!$             Z_asymptotic,&
!!$             0.d0,&
!!$             RFit,&
!!$             RefFit,&
!!$             Bs_Comp_freg,&
!!$             Bs_Comp_firr,&
!!$             NFIT,&
!!$             info,&
!!$             6,&
!!$             Bs_Comp_Delta,&
!!$             Bs_Comp_fcoe,&
!!$             Bs_Comp_gcoe)
!!$        !
!!$        Bs_Comp_Delta=Bs_Comp_Delta/dble(NFIT)/&
!!$             (Bs_Comp_Fcoe**2+Bs_Comp_Gcoe**2)
!!$        !
!!$        if(info==-1)write(6,"(a)")"(EEE) - cnormena failed"
!!$        !
!!$        !.. Scale the function
!!$        !..
!!$        if(Bs_Comp_Delta<0.01)then
!!$           !
!!$           RefGrid = Bs_Comp_NormFact * RefGrid
!!$           !
!!$        elseif(RefGrid(2)<RefGrid(1))then
!!$           !
!!$           RefGrid = - RefGrid
!!$           !
!!$        endif
!!$        !
!!$        !
!!$        !.. Fit the reference wavefunction in the asymptotic region
!!$        !..
!!$        call cnormenak(&
!!$             GASP_NormFact,&
!!$             GASP_Phase_Shift,&
!!$             0,&
!!$             Momentum,&
!!$             Z_asymptotic,&
!!$             0.d0,&
!!$             RFit,&
!!$             RefFit,&
!!$             GASP_freg,&
!!$             GASP_firr,&
!!$             NFIT,&
!!$             info,&
!!$             6,&
!!$             GASP_Delta,&
!!$             GASP_fcoe,&
!!$             GASP_gcoe)
!!$        !
!!$        GASP_Delta=GASP_Delta/dble(NFIT)/(GASP_Fcoe**2+GASP_Gcoe**2)
!!$        !
!!$        if(info==-1)write(6,"(a)")"(EEE) - cnormena failed"
!!$        !
!!$        !.. Scale the function
!!$        !..
!!$        if(.FALSE.)then!GASP_Delta<0.01)then
!!$           !
!!$           FunGrid  = GASP_NormFact * FunGrid
!!$           Fun1Grid = GASP_NormFact * Fun1Grid
!!$           Fun2Grid = GASP_NormFact * Fun2Grid
!!$           !
!!$        elseif(FunGrid(2)<FunGrid(1))then
!!$           !
!!$           FunGrid  = - FunGrid
!!$           Fun1Grid = - Fun1Grid
!!$           Fun2Grid = - Fun2Grid
!!$           !
!!$        endif
!!$        !
!!$     endif

     !.. Write the comparison between wavefunctions on file
     !..
     write(nstrn,*) i
     FileName=trim(FileRoot)//"."//trim(adjustl(nstrn))
     !write(*,"(a)")trim(FileName)
     open(10,File=FileName,Form="formatted",Status="unknown")
     !
     do igrid=1,NGRID
        !
        write(10,"(i6,4(x,f26.16))")igrid,RGrid(igrid),&
             RefGrid(igrid),Fun1Grid(igrid),Fun2Grid(igrid)
        !
     enddo
     !
     close(10)


  enddo


  stop


contains


  subroutine crash()
    !
    !.. Crash deliberately causes
    !   a segmentation fault. 
    !   Rudimentary, Quick & Dirty 
    !   trick to trace errors
    !..
    integer, allocatable :: a(:)
    !
    deallocate(a)
    !
    return
    !
  end subroutine crash


end program main







