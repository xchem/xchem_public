!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module ModuleBasis

  use, intrinsic :: ISO_FORTRAN_ENV
  use ModuleParameterList

  implicit none
  public

  !.. These must all become attributes of the basis class
  character(132), public :: BasisDir    
  character(132), public :: BasisKind   
  character(132), public :: BasisParFile
  logical       , public :: PlotBasis   
  integer       , public :: nplot       

contains

  subroutine ParseBasisConfFile(file_name)
    character(len=*), intent(in) :: file_name
    type(ClassParameterList) :: List

    !.. Initializes the configuration file parameters
    !..
    call List%Add( "BasisDirectory"    , "basedir", "required" )
    call List%Add( "BasisKind"         , "B"      , "required" )
    call List%Add( "BasisParameterFile", "BSpline", "required" )
    call List%Add( "PlotBasis"         , .TRUE.   , "optional" )
    call List%Add( "Nplot"             ,  500     , "optional" )

    !.. Read the parameters from the configuration file
    !..
    call List%Parse( file_name )
    call List%Get( "BasisDirectory"    , BasisDir     )
    call List%Get( "BasisParameterFile", BasisParFile )
    call List%Get( "BasisKind"         , BasisKind    )
    call List%Get( "PlotBasis"         , PlotBasis    )
    call List%Get( "Nplot"             , Nplot        )

  end subroutine ParseBasisConfFile
end module ModuleBasis


program TestModuleParameterList
  use ModuleBasis
  implicit none
  
  call ParseBasisConfFile("baseTest")
  
  write(*,*) "'"//trim(BasisDir)//"'"
  write(*,*) "'"//trim(BasisKind)//"'"
  write(*,*) "'"//trim(BasisParFile)//"'"
  write(*,*) "'",PlotBasis,"'"
  write(*,*) "'",nplot,"'"
  
end program TestModuleParameterList
