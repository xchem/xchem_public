!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program testModuleGroups
  
  use ModuleGroups
  
  implicit none
  
  type(ClassIrrep), pointer :: sym1 => NULL()
  type(ClassIrrep), pointer :: sym2 => NULL()
  type(ClassIrrep), pointer :: sym3 => NULL()

  type(ClassGroup) :: LocalGroup

  type(ClassIrrep), pointer, dimension(:) :: symv

  integer :: iSym, jSym

  call GlobalGroup.init("D2h")
  call GlobalGroup.show

  write(*,*) GlobalGroup.DefinesIrrep("B2u")

  sym1 => GlobalGroup.GetIrrep("B2u")
  sym2 => GlobalGroup.GetIrrep("B3u")
  sym3 => sym1 * sym2
  write(*,*) sym1.GetName()," x ",sym2.GetName()," = ",sym3.GetName()
  write(*,*) sym1.GetIdStrn()

  call sym3.show

  !.. Print the multiplication table of the irreducible representations
  call LocalGroup.init("C2v")
  symv => LocalGroup.GetIrrepList()
  write(*,"(5x,*(1x,a3))") (symv(iSym).GetName(),iSym=1,size(symv))
  write(*,*)
  do iSym = 1, size(symv)
     write(*,"(a3,2x)",advance="no") symv(iSym).GetName()
     do jSym = 1, size(symv)
        sym1 => symv(iSym) * symv(jSym)
        write(*,"(1x,a3)",advance="no") sym1.GetName()
     enddo
     write(*,*)
  enddo
     
end program testModuleGroups
