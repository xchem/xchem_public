!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program ProgramTestModuleMatrix

  use ISO_FORTRAN_ENV
  use ModuleMatrix
  implicit none

  integer, parameter :: NR=10
  integer, parameter :: NC=10

  type(ClassMatrix) :: Matrix1, Matrix2

  integer :: fp

  call Matrix1%InitFull(NR,NC)
  Matrix1=sqrt(2.d0)
  call Matrix1%SetElement(2,3,0.d0)
  call Matrix1%Write

  open(NEWUNIT=fp,File="foo",FORM="formatted",STATUS="Unknown")
  call Matrix1%Write(fp)
  rewind(fp)
  call Matrix2%Read(fp)
  call Matrix2%Write
  

end program ProgramTestModuleMatrix
