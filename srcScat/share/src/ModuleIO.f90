!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module ModuleIO

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleString

  implicit none

  private

  public :: OpenFile
  public :: GetFullText
  public :: FetchGlobalVariable
  public :: FetchLine
  public :: ReadMatrix
  public :: WriteMatrix
  public :: CheckFileMustBePresent
  public :: GetListOfFilesWithPattern
  public :: RemoveFilesInList
  !> Given a vector of file names, opens each for reading
  !! and returns a vector with the connected unit numbers.
  public :: GetUnitNumberVectorForReading
  !> Given a vector of file names, opens each for writting
  !! and returns a vector with the connected unit numbers.
  public :: GetUnitNumberVectorForWritting
  !> Given a vector of unit numbers, closes all the connected
  !! files.
  public :: CloseUnitNumberVector
  !> Writes the same string in all files connected to the
  !! unit numbers in a vector.
  public :: WriteStringUnitNumberVector

contains


  subroutine GetFullText( FileName, FullText_ )
    character(len=*),              intent(in)    :: FileName
    character(len=:), allocatable, intent(inout) :: FullText_

    character(len=1024*1024) :: FullText
    character(len=1024)      :: LineStrn
    integer :: uid, iostat, ichar

    call OpenFile( FileName, uid, "read", "formatted" )

    FullText=" "
    lineCycle : do

       !.. Read one line from file. Exit if the file is over
       read(uid,"(a)",IOSTAT=iostat) LineStrn
       if( iostat /= 0 ) exit lineCycle

       !.. Skips comented (#) and blanck lines.
       ichar = index(LineStrn,"#")
       if ( ichar > 0 ) LineStrn(ichar:) = " "
       LineStrn = adjustl(LineStrn)
       if ( len_trim(LineStrn) == 0 ) cycle lineCycle

       !.. Accumulates the new line on the full text variable
       FullText=trim(FullText)//" "//trim(LineStrn)

    end do lineCycle
    close(uid)

    if(allocated(FullText_)) deallocate(FullText_)
    allocate(FullText_,source=trim(adjustl(FullText)))

  end subroutine GetFullText


  subroutine FetchGlobalVariable( Text, VariableName, VariableLabel )
    character(len=*),              intent(in)  :: Text
    character(len=*),              intent(in)  :: VariableName
    character(len=:), allocatable, intent(out) :: VariableLabel
    !
    character(len=512) :: strn
    integer :: ichar
    !
    ichar = index( Text, VariableName )
    if(ichar<1) return
    !
    ichar = ichar + index( Text(ichar+1:),"=" )
    read(Text(ichar+1:),*)strn
    allocate( VariableLabel, source = trim(adjustl(strn)) )
    !
  end subroutine FetchGlobalVariable


  subroutine FetchLine( uid, Line)
    integer                      , intent(in) :: uid
    character(len=:), allocatable, intent(out) :: Line
    !
    character(len=1024) :: LineStrn
    integer             :: ichar, iostat
    !
    lineCycle : do

       read(uid,"(a)",IOSTAT=iostat) LineStrn
       if( iostat /= 0 )then
          LineStrn=" "
          exit lineCycle
       endif

       !.. Skips comented (#) and blanck lines.
       ichar = index(LineStrn,"#")
       if ( ichar > 0 ) LineStrn(ichar:) = " "
       LineStrn = adjustl(LineStrn)
       if ( len_trim(LineStrn) == 0 )then
          cycle lineCycle
       else
          exit lineCycle
       endif
    end do lineCycle
    
    if( len_trim(LineStrn) == 0 )then
       allocate(Line,source=" ")
    else
       allocate(Line,source=trim(adjustl(LineStrn)))
    endif
  end subroutine FetchLine


  subroutine OpenFile( FileName, uid, Purpose, Form )
    !> The name of the file to be opened.
    character(len=*),           intent(in)    :: FileName
    !> Unit number associated to the opened file.
    integer,                    intent(out)   :: uid
    !> Can be "write" or "read" depending on the action needed.
    character(len=*), optional, intent(in)    :: Purpose
    !> Can be "formatted" or "unformatted".
    character(len=*), optional, intent(in)    :: Form
    !
    logical :: opened
    integer :: iostat
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=:), allocatable :: NewPurpose, NewForm
    character(len=*), parameter :: FormattedLabel   = 'formatted'
    character(len=*), parameter :: UnformattedLabel = 'unformatted'
    character(len=*), parameter :: ReadLabel        = 'read'
    character(len=*), parameter :: WriteLabel       = 'write'
    !
    if ( .not.present(Purpose) ) then
       allocate( NewPurpose, source = ReadLabel )
    else
       if ( (Purpose .isnt. ReadLabel) .and. &
            (Purpose .isnt. WriteLabel) ) &
            call Assert( 'The action label must '//&
            'be either '//&
            ReadLabel//' or '//&
            WriteLabel//'.' )
       allocate( NewPurpose, source = Purpose )
    end if
    if ( .not.present(Form) ) then
       allocate( NewForm, source = UnformattedLabel )
    else
       if ( (Form .isnt. FormattedLabel) .and. &
            (Form .isnt. UnformattedLabel) ) &
            call Assert( 'The format label must '//&
            'be either '//&
            FormattedLabel//' or '//&
            UnformattedLabel//'.' )
       allocate( NewForm, source = Form )
    end if
    !
    OPEN(NewUnit =  uid,          &
         File    =  FileName,     &
         Form    =  NewForm,      &
         Status  = "unknown",     &
         Action  = NewPurpose,    &
         iostat  = iostat ,       &
         iomsg   = iomsg    )
    if( iostat /= 0 ) call ErrorMessage(iomsg)

    INQUIRE( unit = uid, &
         iomsg = iomsg, &
         iostat = iostat, &
         opened = opened )
    if ( iostat /= 0 ) call Assert(iomsg)

  end subroutine OpenFile



  subroutine ReadMatrix( FileName, Array, OnlyDimension )
    !
    character(len=*)            , intent(in)  :: FileName
    real(kind(1d0)), allocatable, intent(out) :: Array(:,:)
    logical        , optional   , intent(in)  :: OnlyDimension
    !
    integer :: uid, iostat, NumRows, NumColumns, i, j
    character(len=IOMSG_LENGTH) :: iomsg
    logical :: opened
    !
    NumRows = 0
    NumColumns = 0
    !
    call OpenFile( FileName, uid, "read", "formatted" )
    INQUIRE( unit = uid, &
         iomsg = iomsg, &
         iostat = iostat, &
         opened = opened )
    if ( iostat /= 0 ) call Assert(iomsg)
    !
    if ( opened ) then
       !
       read(uid,*,iostat=iostat) NumRows, NumColumns
       if(iostat/=0)then
          call Assert( "Invalid dimensions on the first line in "//FileName )
       endif
       !
       allocate( Array(NumRows,NumColumns) )
       Array = 0.d0
       !
       if ( present(OnlyDimension) .and. OnlyDimension ) return
       !
       !.. Read by rows.
       do i = 1, NumRows
          read(uid,*,iostat=iostat) ( Array(i,j), j=1,NumColumns )
          if(iostat/=0)then
!!$             call Assert( "Read error on line "//AlphabeticNumber(i+2)//" in file "//FileName )
             write(output_unit,*) 'If you are doing a Feshbach partition '//&
                  'then ignore the following warning:'
             call ErrorMessage( "Read error on line "//AlphabeticNumber(i+2)//" in file "//FileName )
          endif
       end do
       !
       close(uid)
       !
    else
       !
       call Assert( "The file: "//FileName//" does not exist." )
       !
    end if
    !
  end subroutine ReadMatrix




  subroutine WriteMatrix( FileName, Array )
    !
    character(len=*), intent(in) :: FileName
    real(kind(1d0)) , intent(in) :: Array(:,:)
    !
    integer :: uid, iostat, NumRows, NumColumns, i, j
    character(len=IOMSG_LENGTH) :: iomsg
    !
    NumRows    = size(Array,1)
    NumColumns = size(Array,2)
    !
    call OpenFile( FileName, uid, "write", "formatted" )
    !
    write(uid,IOSTAT=iostat,fmt=*) NumRows, NumColumns
    if ( iostat /= 0 )call Assert( "Error writing  block dimensions in "//FileName//"." )
    !
    !.. Write by rows.
    do i = 1, NumRows
       write(uid,IOSTAT=iostat,fmt=*) ( Array(i,j), j=1,NumColumns )
       if(iostat/=0) call Assert( "Write error on line "//AlphabeticNumber(j+2)//" in file "//FileName )
    end do
    !
    close(uid)
    !
  end subroutine WriteMatrix


  subroutine CheckFileMustBePresent( FileName )
    character(len=*), intent(in) :: FileName
    logical :: exist
    INQUIRE( file = FileName, exist = exist )
    if (.not.exist ) then 
       INQUIRE( file = FileName//'.h5', exist = exist )
    endif
    if (.not.exist ) call Assert( &
      'File: '//FileName//'or'//FileName//'.h5 is not present and it must be.' )
  end subroutine CheckFileMustBePresent


  !> Given a directory and a pattern, the routine builds a list of files
  !! contained in that directory that fullfills the pattern, in the following way:
!! If we are interested in listing the files with the structure:
!! <HeadCharacter><Pattern>, where pattern is
!! <Pattern> = <RealNumber><SeparationCharacter><LastLabel>,
!! for all real numbers between RealMin and RealMax.
  subroutine GetListOfFilesWithPattern( &
       Dir              , &
       Pattern          , &
       SeparationCharacter, &
       HeadCharacter, &
       RealMin, RealMax                 , &
       FilesVec       )
    character(len=*)               , intent(in)    :: Dir
    character(len=*)               , intent(in)    :: Pattern
    character(len=*)               , intent(in)    :: SeparationCharacter
    character(len=*)               , intent(in)    :: HeadCharacter
    real(kind(1d0))                , intent(in)    :: RealMin
    real(kind(1d0))                , intent(in)    :: RealMax
    character(len=256), allocatable, intent(out)   :: FilesVec(:)
    !
    character(len=:), allocatable :: Label, ListFileName, Line, RealNumStrn, ResidueLabel
    integer :: ichar, ichar2, ichar3, uid, NFiles
    logical :: exist, GoodLine
    real(kind(1d0)) :: RealNum
    character(len=256), allocatable :: AuxFilesVec(:)
    !
    real(kind(1d0)) , parameter :: Threshold    = 1.d-8
    integer         , parameter :: MaxNEnergies = 100000
    !
    !.. Gets the string following the energy value in the name.
    ichar = index(Pattern,SeparationCharacter)
    if ( ichar > 0 ) allocate( Label, source = Pattern(ichar:) )
    !
    !
    !.. Build a list with all the files in the directory.
    call execute_command_line("ls -1 "//AddSlash(Dir)//" > "//AddSlash(Dir)//".list ")
    allocate( ListFileName, source = AddSlash(Dir)//'.list' )
    INQUIRE( file = ListFileName, exist = exist )
    if ( .not.exist ) call Assert( &
         'There are not computed scattering states for the '//&
         'requested parameters.' )
    !
    !
    call OpenFile( ListFileName, uid, 'read', 'formatted' )
    !
    allocate( AuxFilesVec(MaxNEnergies) )
    AuxFilesVec = ' '
    NFiles = 0
    do
       !
       if ( allocated(Line) ) deallocate(Line)
       call FetchLine( uid, Line )
       if ( len_trim(Line) == 0 ) exit
       !
       ichar2 = index(Line,HeadCharacter)
       if ( ichar2 <= 0 ) cycle
       ichar3 = index(Line,SeparationCharacter)
       if ( allocated(RealNumStrn) ) deallocate( RealNumStrn )
       if ( ichar > 0 ) then
          if (ichar3 > 0 ) then
             allocate( RealNumStrn, source = Line(ichar2+1:ichar3-1) )
             if ( allocated(ResidueLabel) ) deallocate(ResidueLabel)
             allocate( ResidueLabel, source = trim(Line(ichar3:)) )
          end if
       else
          if (ichar3 > 0 ) then
             allocate( RealNumStrn, source = trim(Line(ichar2+1:ichar3-1)) )
             if ( allocated(ResidueLabel) ) deallocate(ResidueLabel)
             allocate( ResidueLabel, source = trim(Line(ichar3:)) )
          else
             allocate( RealNumStrn, source = trim(Line(ichar2+1:)) )
          end if
       end if
       !
       if ( .not.allocated(RealNumStrn)  )       cycle
       if ( (len_trim(RealNumStrn) == 0) )       cycle
       if ( .not.StringHasNumbers(RealNumStrn) ) cycle
       !
       read(RealNumStrn,*) RealNum 
       !
       GoodLine = .false.
       if ( (RealNum > RealMin-Threshold) .and. (RealNum < RealMax+Threshold) ) then
          if ( allocated(Label) ) then
             if ( ResidueLabel .is. Label ) GoodLine = .true.
          else
             if ( ichar3 <= 0 ) GoodLine = .true.
          end if
       end if
       !
       if ( GoodLine ) then
          NFiles = NFiles + 1
          AuxFilesVec(NFiles) = AddSlash(Dir)//trim(Line)
       end if
       !
    end do
    !
    close( uid )
    !
    if ( NFiles == 0 ) then
       call ErrorMessage( 'No files were found for the requested parameters.' )
       return
    end if
    !
    allocate( FilesVec, source = AuxFilesVec(1:NFiles) )
    !
    !
  end subroutine GetListOfFilesWithPattern


  !> Removes all the files in the list.
  subroutine RemoveFilesInList( FilesVec )
    character(len=*), intent(in) :: FilesVec(:)
    integer :: i
    do i = 1, size(FilesVec) 
       call execute_command_line( "rm "//trim(adjustl(FilesVec(i))) )
    end do
  end subroutine RemoveFilesInList
  

  !> Given a vector of file names, opens each for reading
  !! and returns a vector with the connected unit numbers.
  subroutine GetUnitNumberVectorForReading( FilesVec, UidVec )
    !> Vector of file names.
    character(len=*)    , intent(in) :: FilesVec(:)
    !> Vector of the unit numbers.
    integer, allocatable, intent(out) :: UidVec(:)
    !
    integer :: i, NumFiles
    !
    NumFiles = size(FilesVec)
    allocate( UidVec(NumFiles) )
    !
    do i = 1, NumFiles
       call OpenFile( &
            trim(adjustl(FilesVec(i))), &
            UidVec(i), &
            'read', &
            'formatted' )
    end do
    !
  end subroutine GetUnitNumberVectorForReading



  !> Given a vector of file names, opens each for writting
  !! and returns a vector with the connected unit numbers.
  subroutine GetUnitNumberVectorForWritting( FilesVec, UidVec )
    !> Vector of file names.
    character(len=*)    , intent(in) :: FilesVec(:)
    !> Vector of the unit numbers.
    integer, allocatable, intent(out) :: UidVec(:)
    !
    integer :: i, NumFiles
    !
    NumFiles = size(FilesVec)
    allocate( UidVec(NumFiles) )
    !
    do i = 1, NumFiles
       call OpenFile( &
            trim(adjustl(FilesVec(i))), &
            UidVec(i), &
            'write', &
            'formatted' )
    end do
    !
  end subroutine GetUnitNumberVectorForWritting
    


  !> Given a vector of unit numbers, closes all the connected
  !! files.
  subroutine CloseUnitNumberVector( UidVec )
    !> Vector of the unit numbers.
    integer, intent(in) :: UidVec(:)
    integer :: i
    do i = 1, size(UidVec)
       close( UidVec(i) )
    end do
  end subroutine CloseUnitNumberVector


  !> Writes the same string in all files connected to the
  !! unit numbers in a vector.
  subroutine WriteStringUnitNumberVector( UidVec, String )
    !> Vector of the unit numbers.
    integer         , intent(in) :: UidVec(:)
    character(len=*), intent(in) :: String
    integer :: i, iostat
    do i = 1, size(UidVec)
       write(UidVec(i),'(a)',iostat=iostat) String
       if ( iostat /= 0 ) call Assert( &
            'Error writing the string for '//&
            'the unit number vector.')
    end do
    !
  end subroutine WriteStringUnitNumberVector




end module ModuleIO
