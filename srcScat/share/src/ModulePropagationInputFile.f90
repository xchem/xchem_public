!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Parses the configuration file that contains the parameters to perform the wave packet time propagation ([PropagationInputFile](@ref PropagationInputFile)).


module ModulePropagationInputFile
    

  use ModuleParameterList
  use ModuleConstants

  implicit none

  private

  !> Maximum length of a name to be used.
  integer, parameter :: NAME_LENGTH=500
        
  !> File in which the external field parameters are stored.
  character(len=NAME_LENGTH), public :: ExtFieldFile
  !> Maximum propagation time.
  real(kind(1d0))           , public :: TimeMax
  !> The time step.
  real(kind(1d0))           , public :: TimeStep
  !> Number of iterations before the next function is saved.
  integer                   , public :: NumIterBefSave
  !> Norm limit.
  real(kind(1d0))           , public :: NormLimit
  !> Result accuracy.
  real(kind(1d0))           , public :: ResAccur
  !> Minimum number of iterations.
  integer                   , public :: MinNumIter
  !> Maximum number of iterations.
  integer                   , public :: MaxNumIter
  !> Indicates whether an initial state file will be used or not.
  logical                   , public :: UseInitStateFile
  !> Initial state file. 
  character(len=NAME_LENGTH), public :: InitStateFile
  !> Last angular momentum threshold.
  real(kind(1d0))           , public :: LastAngMomThres
  !> Magnus expansion number of terms.
  integer                   , public :: NumMagnusTerms
  !> Indicates whether a complex extraction/absorption potential will be used or not.
  logical                   , public :: ComplexPot
  

  public ParsePropagationInputFile


contains

  !> Parses the [PropagationInputFile](@ref PropagationInputFile).
  subroutine ParsePropagationInputFile( ConfigurationFile )
    !
    use, intrinsic :: ISO_FORTRAN_ENV
    !
    character(len=*) , intent(in) :: ConfigurationFile
    !
    type(ClassParameterList) :: List
    !
    call List%Add( "ExtFieldFile"      ,"A"     , "required" )
    call List%Add( "TimeMax"           ,1d0     , "required" )
    call List%Add( "TimeStep"          ,1d0     , "required" )
    call List%Add( "NumIterBefSave"    ,1       , "required" )
    call List%Add( "NormLimit"         ,1d0     , "required" )
    call List%Add( "ResAccur"          ,1d0     , "required" )
    call List%Add( "MinNumIter"        ,1       , "required" )
    call List%Add( "MaxNumIter"        ,1       , "required" )
    call List%Add( "UseInitStateFile"  ,.false. , "required" )
    call List%Add( "InitStateFile"     ,"A"     , "required" )
    call List%Add( "LastAngMomThres"   ,1d0     , "required" )
    call List%Add( "NumMagnusTerms"    ,1       , "required" )
    call List%Add( "ComplexPot"        ,.true.  , "required" )
    !
    call List%Parse( ConfigurationFile )
    !
    call List%Get( "ExtFieldFile"      , ExtFieldFile  )
    call List%Get( "TimeMax"           , TimeMax  )
    ! Converts from femtosecods to a.u.
    TimeMax = TimeMax * TimeConvFStoAU
    call List%Get( "TimeStep"          , TimeStep  )
    call List%Get( "NumIterBefSave"    , NumIterBefSave  )
    call List%Get( "NormLimit"         , NormLimit  )
    call List%Get( "ResAccur"          , ResAccur  )
    call List%Get( "MinNumIter"        , MinNumIter  )
    call List%Get( "MaxNumIter"        , MaxNumIter  )
    call List%Get( "UseInitStateFile"  , UseInitStateFile  )
    call List%Get( "InitStateFile"     , InitStateFile  )
    call List%Get( "LastAngMomThres"   , LastAngMomThres  )
    call List%Get( "NumMagnusTerms"    , NumMagnusTerms )
    call List%Get( "ComplexPot"        , ComplexPot  )
    !
  end subroutine ParsePropagationInputFile
end module ModulePropagationInputFile
