!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines the subroutines that serve as an interface with those contained in BLAS package.

module ModuleBLASWrapper

  implicit none
  private

  public :: MatrixMult

contains

  !> Serves as an interface with the intrinsic BLAS matrix multiplication routine that performs: \f$C=A*B\f$.
  subroutine MatrixMult( TransA, TRansB, A, B, C )
    !> Form of the matrix A: "N" (remains the same), "T" (transpose), or "C" (transpose conjugate).
    character(len=1), intent(in)  :: TransA
    !> Form of the matrix B: "N" (remains the same), "T" (transpose), or "C" (transpose conjugate).
    character(len=1), intent(in)  :: TRansB
    !> \f$A\f$ input matrix.
    real(kind(1d0)),  intent(in)  :: A(:,:)
    !> \f$B\f$ input matrix.
    real(kind(1d0)),  intent(in)  :: B(:,:)
    !> \f$C\f$ output matrix.
    real(kind(1d0)),  intent(out) :: C(:,:)
    !
    integer :: m, n, k, lda, ldb, ldc
    DoublePrecision, parameter :: alpha = 1.d0
    DoublePrecision, parameter :: beta  = 0.d0
    !
    lda = size( A, 1 )
    ldb = size( B, 1 )
    ldc = size( C, 1 )
    !
    m = size( A, 1 )
    k = size( A, 2 )
    n = size( B, 2 )
    !
    call DGEMM(   &
         TransA , &
         TransB , &
         m, n, k, &
         alpha  , &
         A, lda , &
         B, ldb , &
         beta   , &
         C, ldc )
    !
  end subroutine MatrixMult


end module ModuleBLASWrapper
