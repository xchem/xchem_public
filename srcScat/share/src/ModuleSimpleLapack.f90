!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines some subroutines that serve as an interface with those contained in LAPACK package.


module ModuleSimpleLapack

  use ModuleErrorHandling
  use ModuleString

  implicit none

  private
  public :: simpleDSYEV
  public :: simpleDSYGV

contains


  !> Simplified interface to the DSYEV diagonalization routine.
  subroutine SimpleDSYEV(N,A,LDA,W)
    !
    !..
    !> Dimension of the matrix to diagonalize.
    integer        , intent(in)    :: N
    !
    !> A(i,j) : input  : Matrix to diagonalize,
    !!           output : A(:,i) is the i-th eigenvector of A
    real(kind(1d0)), intent(inout) :: A(:,:)
    !
    !> Leading dimension of A.
    integer        , intent(in)    :: LDA
    !
    !> W(i): i-th eigenvalue of A.
    real(kind(1d0)), intent(out)   :: W(:)
    !
    !.. Variables for matrix diagonalization
    !..
    integer                      :: INFO
    integer                      :: LWORK
    real(kind(1d0)), allocatable :: WORK(:)
    !
    character(len=*), parameter :: HERE = "ModuleSimpleLapack::SimpleDSYEV : "
    !
    !.. Initializes the service vector 
    !   for the diagonalization
    !..
    LWORK = 16 * N
    allocate(WORK(LWORK))
    WORK=0.d0
    !
    W=0.d0
    call DSYEV("V","U",N,A,LDA,W,WORK,LWORK,INFO)
    deallocate(WORK)
    !
    if(INFO/=0) call ErrorMessage(HERE//" DSYEV failed")
    !
  end subroutine SimpleDSYEV



  !> Simplified interface to the DSYGV generalized
  !! diagonalization routine. Find the solutions
  !! C to the problem:
  !!   
  !!   A C = S C E
  subroutine SimpleDSYGV(N,A,LDA,S,LDS,W)
    !
    !..
    !> Dimension of the matrix to diagonalize.
    integer        , intent(in)    :: N
    !
    !> A(i,j) : input  : Matrix to diagonalize,
    !!           output : A(:,i) is the i-th eigenvector of A.
    real(kind(1d0)), intent(inout) :: A(:,:)
    !
    !> Leading dimension of A.
    integer        , intent(in)    :: LDA
    !
    !> S(i,j) : input  : Overlap matrix,
    !!          output : contains the upper triangular matrix of the Cholesky factorization of S.
    real(kind(1d0)), intent(inout) :: S(:,:)
    !
    !>  Leading dimension of S.
    integer        , intent(in)    :: LDS
    !
    !> W(i): i-th eigenvalue of A.
    real(kind(1d0)), intent(out)   :: W(:)
    !
    !.. Variables for matrix diagonalization
    !..
    integer                      :: INFO
    integer                      :: LWORK
    real(kind(1d0)), allocatable :: WORK(:)
    !
    !.. Initializes the service vector 
    !   for the diagonalization
    !..

    !.. Optimal work-size query
    LWORK=-1
    allocate( WORK( 1 ) )
    call DSYGV( 1, 'V', 'U', n, A, lda, S, lds, W, WORK, LWORK, INFO )
    LWORK = max(3*n,int(WORK(1)))
    deallocate( WORK )
    allocate( WORK( LWORK ) )
    !
    call DSYGV( 1, 'V', 'U', n, A, lda, S, lds, W, WORK, LWORK, INFO )
    deallocate( WORK )
    !
    if(info<0)call ErrorMessage(&
         "DSYGV: The "//AlphabeticNumber(-info)//"-th parameter has an illegal value")
    if(info>0.and.info<=n)call ErrorMessage(&
         "DSYGV: DSYEV failed to converge, and "//AlphabeticNumber(info)//" off-diagonal"//&
         "elements of an intermediate tridiagonal did not converge to zero.")
    if(info>n.and.info<=2*n)call ErrorMessage(&
         "DSYGV: the leading minor of order "//AlphabeticNumber(info-n)//" of the metric is not positive-definite."//&
         "The factorization of the metric could not be completed and no eigenvalues or eigenvectors were computed.")
    !
  end subroutine SimpleDSYGV



end module ModuleSimpleLapack
