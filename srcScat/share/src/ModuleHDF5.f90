!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module ModuleHDF5
  
  use, intrinsic :: ISO_FORTRAN_ENV

  use HDF5


  implicit none

  contains

  subroutine writeHDF5(path,Matrix,MetaData)
  
    real*8, allocatable       , intent(in) :: Matrix(:,:) 
    character(len=*), intent(in) :: path
    integer                   , intent(in) :: MetaData(9)
    integer(hsize_t),dimension(1),parameter:: dimMetadata=9
    integer                                :: error, lastslash,i,uid
    integer(hid_t)                         :: file_id,group_id, dataset_id, dataspace_id
    integer(hid_t)                         :: plist_id
    integer(hsize_t), dimension(1:2)       :: dimsMat,cdimsMat
    character*300                          :: dir, filename
    logical                                   fex 

    inquire(file=trim(path)//'.lock',exist=fex)
    if (fex) then
      do while (fex)
        call sleep(2)
        inquire(file=trim(path)//'.lock',exist=fex)
      enddo
    endif
!    open(newunit=uid,file=trim(path)//'.lock',status='scratch',form='formatted')
!    write(uid,*) 'writting'
    call system('touch '//trim(path)//'.lock')
!!! Splitting the path and the filename    
    do i=1,len(trim(path)) 
      if (path(i:i) .eq. "/") lastslash = i
    enddo

    dir=path(1:lastslash)
    filename=path(lastslash+1:len(trim(path)))

    CALL h5open_f(error)
    if (error .ne. 0) write(6,'(A,x,A)') 'XCHEM_WARNING: error opening file:',trim(path)//'.h5'

!!! Creating the new file
    CALL h5fcreate_f(trim(path)//'.h5', H5F_ACC_TRUNC_F, file_id, error)
    if (error .ne. 0) write(6,'(A,x,A)') 'XCHEM_WARNING: error creating file:',trim(path)//'.h5'

!!! Creating a group where the matrix and the metadata is stored    
    CALL h5gcreate_f(file_id, trim(filename), group_id, error)
    
    dimsMat(1) = MetaData(2)
    dimsMat(2) = MetaData(3)

!!! Creating a dataspace that will contain the matrix
    CALL h5screate_simple_f(2, dimsMat, dataspace_id, error)

!!! Creating a dataset that will contain the matrix
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, plist_id, error)

!!! Chunk size 100x100 default
    cdimsMat(1:2) = 100
    if (dimsMat(1).le.cdimsMat(1)) cdimsMat(1)=dimsMat(1)
    if (dimsMat(2).le.cdimsMat(2)) cdimsMat(2)=dimsMat(2)

!!! Defining the Chunk size
    CALL h5pset_chunk_f(plist_id, 2, cdimsMat, error)

!!! Defining the compression to ZLIB/DEFLATE level 6
    CALL h5pset_deflate_f(plist_id, 6, error)

!!! Creating the dataset
    CALL h5dcreate_f(group_id, trim(Filename)//"Mat", H5T_NATIVE_DOUBLE, dataspace_id, dataset_id,error, dcpl_id=plist_id)

!!! Writting the matrix in the dataset
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_DOUBLE, Matrix , dimsMat, error)

!!! Closing dataspace/dataset
    CALL h5sclose_f(dataspace_id, error)
    CALL h5pclose_f(plist_id, error)
    CALL h5dclose_f(dataset_id, error)

!!! Creating a dataspace that will contain the metadata
    CALL h5screate_simple_f(1, dimMetadata , dataspace_id, error)

!!! Creating a dataset that will contain the metadata
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, plist_id, error)


!!! Defining the Chunk size
    CALL h5pset_chunk_f(plist_id, 1, dimMetadata, error)

!!! Defining the compression to ZLIB/DEFLATE level 6
    CALL h5pset_deflate_f(plist_id, 6, error)

!!! Creating the dataset
    CALL h5dcreate_f(group_id, trim(Filename)//"Meta", H5T_NATIVE_INTEGER, dataspace_id, dataset_id,error, dcpl_id=plist_id)

!!! Writting the metadata in the dataset
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_INTEGER, Metadata , dimMetadata, error)

!!! Closing dataspace/dataset
    CALL h5sclose_f(dataspace_id, error)
    CALL h5pclose_f(plist_id, error)
    CALL h5dclose_f(dataset_id, error)
    CALL h5gclose_f(group_id, error)

    CALL h5fclose_f(file_id, error)
    
    CALL system('rm -f '//trim(path)//'.lock')

  end subroutine writeHDF5 
  
  subroutine readHDF5(path,Matrix,MetaData)
  
    real*8, allocatable       , intent(out):: Matrix(:,:)
    character(len=*)          , intent(in) :: path
    integer                   , intent(out):: MetaData(9)
    integer                                :: error, lastslash,i,numfilt,uid
!    integer, parameter, dimension(1)       :: dim_metadata(1) = 9
    integer(hid_t)                         :: file_id, dataset_id, dataspace_id, group_id
    integer(hsize_t), dimension(1),parameter         :: dim_metadata = 9
    integer(hsize_t), dimension(1:2)       :: dims
    character*300                          :: dir, filename
    logical                                   fex 
    inquire(file=trim(path)//'.lock',exist=fex)
    if (fex) then
      do while (fex)
        call sleep(2)
        inquire(file=trim(path)//'.lock',exist=fex)
      enddo
    endif
    !open(newunit=uid,file=trim(path)//'.lock',status='scratch',form='formatted')
    !write(uid,*) 'reading'
    call system('touch '//trim(path)//'.lock')

!!! Splitting the path and the filename    
    do i=1,len(trim(path)) 
      if (path(i:i) .eq. "/") lastslash = i
    enddo

    dir=path(1:lastslash)
    filename=path(lastslash+1:len(trim(path)))

    CALL h5open_f(error)
    if (error .ne. 0) write(6,'(A,x,A)') 'XCHEM_WARNING: error opening file:',trim(path)//'.h5'
    CALL h5fopen_f(trim(path)//'.h5', H5F_ACC_RDONLY_F, file_id, error)
    if (error .ne. 0) write(6,'(A,x,A)') 'XCHEM_WARNING: error opening file:',trim(path)//'.h5'
    CALL h5gopen_f(file_id, trim(Filename), group_id, error)
    CALL h5dopen_f(group_id, trim(Filename)//'Meta', dataset_id, error)

    !CALL h5dget_create_plist_f(dataset_id, plist_id, error)
      
    !CALL h5pget_nfilters_f(plist_id, numfilt, error)
    CALL h5dread_f(dataset_id, H5T_NATIVE_INTEGER, MetaData, dim_metadata, error)
    CALL h5dclose_f(dataset_id, error)

    allocate(Matrix(MetaData(2),MetaData(3)))
    dims(1)= MetaData(2)   
    dims(2)= MetaData(3)   
    CALL h5dopen_f(group_id, trim(Filename)//'Mat', dataset_id, error)
    CALL h5dread_f(dataset_id, H5T_NATIVE_DOUBLE, Matrix, dims, error)
      
    CALL h5dclose_f(dataset_id, error)
    !CALL h5pclose_f(plist_id, error)
    CALL h5fclose_f(file_id, error)
    CALL h5gclose_f(group_id, error)
    
    CALL system('rm -f '//trim(path)//'.lock')


  end subroutine readHDF5 

end module ModuleHDF5
