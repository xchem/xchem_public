!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fedc-struc fixed
module ModuleSymmetricElectronicSpaceSpectralMethods


  use, intrinsic :: ISO_FORTRAN_ENV


  use ModuleErrorHandling
  use ModuleMatrix
  use ModuleSymmetricElectronicSpace
  use ModuleIO
  use ModuleString
  use ModuleElementaryElectronicSpaceOperators
  use ModuleBasis
  use ModuleConstants
  use specfun

  implicit none


  private
  
  !> If .true. conditions the QC part, which is better for the 
  !! application of the HSLE and INVM scattering methods.
  logical, public , parameter :: CONDITION_QC      = .TRUE. 
  character(len=*), parameter :: RootCondNumber    = '_CN'
  character(len=*), public, parameter :: RootEnergy= 'E'
  character(len=*), public, parameter :: SeparationCharacterLabel= '_'
  character(len=*), parameter :: RootEigenValLabel = '_Eigenvalues'
  character(len=*), parameter :: RootEigenVecAnalLabel = '_EVctAnal'
  character(len=*), parameter :: RootRowLabelsLabel = '_RowLabels'
  character(len=*), parameter :: RootColumnLabelsLabel = '_ColumnLabels'
  character(len=*), parameter :: RootSpectrumLabel = '_Spectrum'
  character(len=*), public, parameter :: RootEigenphasesLabel = 'eigenphases'
  character(len=*), parameter :: RootTotalPhaseLabel  = 'PhaseShift'
  character(len=*), public, parameter :: QCLabel   = 'OnlyQC'
  character(len=*), public, parameter :: LocLabel   = 'OnlyLocalized'
  character(len=*), public, parameter :: PolyLabel  = 'OnlyPolycentric'
  character(len=*), public, parameter :: VirialLabel  = 'VirialCoeff'
  character(len=*), parameter :: SpectrumDir       = 'BoxStates'
  !> Scattering solution directory.
  character(len=*), parameter :: ScatteringDir     = 'ScatteringStates'
  !> Eigenphases directory.
  character(len=*), parameter :: EigenPhasesDir    = 'EigenPhases'
  !> Plots directory.
  character(len=*), parameter :: PlotDir    = 'Plots'
  !> Spectral method label.
  character(len=*), public, parameter :: SpectralMethodLabel = 'SPEC'
  
  !> Homogeneous system of linear equations label.
  character(len=*), public, parameter :: HSLEMethodLabel     = 'HSLE'
  !> Inverse matrix method label.
  character(len=*), public, parameter :: InvMatMethodLabel   = 'INVM'
  !> Incoming boundary condition label.
  character(len=*), public, parameter :: IncomingBoundCondLabel = '_Incoming'
  !> Incoming boundary condition flag.
  character(len=*), public, parameter :: IncomingBoundCondFlag = 'in'
  !> Incoming boundary condition message.
  character(len=*), public, parameter :: IncomingBoundCondMessage = 'incoming boundary condition'
  !> Sin function boundary condition label.
  character(len=*), public, parameter :: SinBoundCondLabel = '_SinFunction'
  !> Feshbach label.
  character(len=*), public, parameter :: FeshbachLabel = '_Feshbach'
  !> Sin function boundary condition flag.
  character(len=*), public, parameter :: SinBoundCondFlag = 'sin'
  !> Sin function boundary condition message.
  character(len=*), public, parameter :: SinBoundCondMessage = 'sin function boundary condition'
  !> Scattering channels label.
  character(len=*), public, parameter :: ScattChannelLabel = '_ScattChannels'
  !
  character(len=*), public, parameter :: BoxBoxTransitionLabel = 'BB'
  character(len=*), public, parameter :: BoxConTransitionLabel = 'BC'
  character(len=*), public, parameter :: ConBoxTransitionLabel = 'CB'
  character(len=*), public, parameter :: ConConTransitionLabel = 'CC'
  character(len=*), public, parameter :: DipoleTransitionDir   = 'DipoleTransitions'
  character(len=*), public, parameter :: BoxIndexLabel = 'BoxI'
  !
  !
  real(kind(1d0)), parameter :: DEFINED_TOLERANCE = 1.d-6
  !> If .true. the unnormalized scattering solutions and its
  !! first derivatives will be evaluated at the border to
  !! compute the scattering S and K matrices. If .false. this
  !! matrices are obtained from the asymptotic Coulomb functions fitting.
  logical         , parameter :: EVALUATE_FUN_BORDER = .FALSE.
  !> Number of points to be used in the Coulomb function fitting
  !! in the asymptotic region.
  integer         , parameter :: N_FITTING_POINTS    = 20

  

  type, public :: ClassScatteringStates
     private
     !
     character(len=:), allocatable                :: SymStorageDir
     character(len=:), allocatable                :: CompMethod
     character(len=:), allocatable                :: ConditionLabel
     real(kind(1d0))                              :: Energy
     real(kind(1d0))                              :: ScattConditionNumber
     integer                                      :: NumBsNotOverlap
     type(ClassBasis)                   , pointer :: Basis     
     type(ClassSymmetricElectronicSpace), pointer :: SymSpace
     !> Class of the partial wave channel vector associated with the 
     !! scattering solutions.
     type(ClassPartialWaveChannel), allocatable   :: PWCVec(:) 
     !> The computed scattering state without any boundary condition.
     type(ClassComplexSpectralResolution)         :: ScattState
     !> The scattering states with incoming boundary condition.
     type(ClassComplexSpectralResolution)         :: IncomingScattState
     !> The scattering states with sin function boundary condition.
     type(ClassComplexSpectralResolution)         :: SinScattState
     type(ClassMatrix)                            :: EigenPhase
     !
   contains
     !
     generic, public :: Init                      => ClassScatteringStatesInit
     generic, public :: Free                      => ClassScatteringStatesFree
     generic, public :: ComputeSPEC               => ClassScatteringStatesComputeSPEC
     generic, public :: ComputeHSLE               => ClassScatteringStatesComputeHSLE
     generic, public :: ComputeINVM               => ClassScatteringStatesComputeINVM
     generic, public :: NormalizeScattStates      => ClassScatteringStatesNormalizeScattStates
     generic, public :: SaveScatteringStates      => ClassScatteringStatesSaveScatteringStates
     generic, public :: SaveEigenPhases           => ClassScatteringStatesSaveEigenPhases
     !
     procedure, private :: ClassScatteringStatesInit
     procedure, private :: ClassScatteringStatesFree
     procedure, private :: ClassScatteringStatesComputeSPEC
     procedure, private :: ClassScatteringStatesComputeHSLE
     procedure, private :: ClassScatteringStatesComputeINVM
     procedure, private :: ClassScatteringStatesNormalizeScattStates
     procedure, private :: ClassScatteringStatesSaveScatteringStates
     procedure, private :: ClassScatteringStatesSaveEigenPhases
     !
  end type ClassScatteringStates



  type, public :: ClassScatteringStatesVec
     private
     !
     character(len=:), allocatable                :: SymStorageDir
     character(len=:), allocatable                :: CompMethod
     character(len=:), allocatable                :: ConditionLabel
     real(kind(1d0))                              :: ScattConditionNumber
     integer                                      :: NumEnergies
     integer                                      :: NumBsNotOverlap
     type(ClassBasis)                   , pointer :: Basis     
     type(ClassSymmetricElectronicSpace), pointer :: SymSpace
     type(ClassScatteringStates), allocatable     :: ScattStates(:)
     logical                                      :: ConditionQC = .FALSE.
     type(ClassMatrix)                            :: ConditionerQC
     !
   contains
     !
     generic, public :: Init                      => ClassScatteringStatesVecInit
     generic, public :: SetConditionerQC          => ClassScatteringStatesVecSetConditionerQC
     generic, public :: ComputeScatteringStates   => ClassScatteringStatesVecComputeScatteringStates
     generic, public :: SaveScatteringStates      => ClassScatteringStatesVecSaveScatteringStates
     generic, public :: SaveEigenPhases           => ClassScatteringStatesVecSaveEigenPhases
     !
     procedure, private :: ClassScatteringStatesVecInit
     procedure, private :: ClassScatteringStatesVecSetConditionerQC
     procedure, private :: ClassScatteringStatesVecComputeScatteringStates
     procedure, private :: ClassScatteringStatesVecSaveScatteringStates
     procedure, private :: ClassScatteringStatesVecSaveEigenPhases
     !
  end type ClassScatteringStatesVec





  public :: BuildGrid
  public :: CheckScatteringMethod
  public :: CheckBoundaryConditionFlag
  public :: AssignBoundaryConditionLabel
  public :: GetEigenvaluesFileName
  public :: GetOperatorFileName
  public :: GetSpectrumFileName
  public :: GetEigenVecAnalFileName
  public :: GetColumnLabelsFileName
  public :: GetRowLabelsFileName
  public :: GetVirialFileName
  public :: GetIncomingScattStateFileName
  public :: GetIncomingScattStateFeshbachFileName
  public :: GetSinScattStateFileName
  public :: GetScatteringChannelsFileName
  public :: GetScattStateDirName
  public :: GetPlotScattStateDirName
  public :: GetEigenPhaseFileName
  public :: GetEigenPhaseFeshbachFileName
  public :: GetEigenPhaseDirName
  public :: GetIncomingScatteringLabel
  public :: GetIncomingScatteringFeshbachLabel
  public :: GetSinScatteringLabel
  public :: GetScatteringChannelsLabel
  public :: GetIncomingFinalScatteringLabel
  public :: GetIncomingFinalScatteringFeshbachLabel
  public :: GetSinFinalScatteringLabel
  public :: GetScatteringChannelsFinalLabel
  public :: GetScattConditioningLabel
  public :: GetIncomingConditioningLabel
  public :: GetSinConditioningLabel
  public :: GetEnergyRangeLabel
  public :: GetIncomingScatteringFilesVec
  public :: GetIncomingScatteringFeshbachFilesVec
  public :: GetSinScatteringFilesVec
  public :: GetScatteringChannelsFilesVec
  public :: GetEigenphasesFilesVec
  public :: GetEigenphasesFeshbachFilesVec
  public :: GetConcatenatedEigenphasesFileName
  public :: GetConcatenatedEigenphasesFeshbachFileName
  public :: GetConcatenatedTotalPhaseFileName
  public :: GetConcatenatedTotalPhaseFeshbachFileName
  public :: GetDipoleEnergyLabel
  public :: GetFullDipoleDir
  public :: GetDipoleFileName
  public :: ReadDipoleTransition
  public :: SaveDipoleTransition
  public :: CheckTransitionLabel
  public :: CheckScatteringInput
  public :: ApplyQCConditioner
  public :: ComputeQCConditioner
  public :: GetFullConditionerMat
  public :: ReadTransformationMatrix
  public :: TransformScatteringStates
  public :: ComputeEigenPhases



contains


  !----------------------------------------
  ! General Methods
  !----------------------------------------


  function GetEnergyRangeLabel( Emin, Emax ) result(Label)
    real(kind(1d0)), intent(in) :: Emin
    real(kind(1d0)), intent(in) :: Emax
    character(len=:), allocatable :: Label
    character(len=64) :: EminStrn, EmaxStrn
    write(EminStrn,*) Emin
    write(EmaxStrn,*) Emax
    allocate( Label, source = &
         '_min_'//trim(adjustl(EminStrn))//&
         '_max_'//trim(adjustl(EmaxStrn)) )
  end function GetEnergyRangeLabel
  


  subroutine BuildGrid( N, Vmin, Vmax, Vector )
    integer,                      intent(in)  :: N
    real(kind(1d0)),              intent(in)  :: Vmin
    real(kind(1d0)),              intent(in)  :: Vmax
    real(kind(1d0)), allocatable, intent(out) :: Vector(:)
    integer :: i
    allocate( Vector(N) )
    if ( N == 1 ) then
       Vector(1) = Vmin
    else
       do i = 1, N
          Vector(i) = Vmin + (Vmax-Vmin)*dble(i-1)/dble(N-1)
       end do
    end if
  end subroutine BuildGrid



  subroutine CheckScatteringMethod( CompMethod )
    character(len=*), intent(in) :: CompMethod
    !
    if ( (CompMethod .isnt. SpectralMethodLabel) .and. &
         (CompMethod .isnt. HSLEMethodLabel) .and. &
         (CompMethod .isnt. InvMatMethodLabel) ) &
         call Assert( 'Invalid computational method option "-cm". It must be '//&
         SpectralMethodLabel//' for spectral, '//&
         HSLEMethodLabel//' for homogeneous system with LU decomposition, or '//&
         InvMatMethodLabel//' for inversion matrix.' )
    !  
  end subroutine CheckScatteringMethod


  subroutine CheckBoundaryConditionFlag( BoundCond )
    character(len=*), intent(in) :: BoundCond
    !
    if ( (BoundCond .isnt. IncomingBoundCondFlag) .and. &
         (BoundCond .isnt. SinBoundCondFlag) ) &
         call Assert( 'Invalid boundary condition option "-boundary". It must be '//&
         IncomingBoundCondFlag//' for incoming wave, or '//&
         SinBoundCondFlag//' for sin function wave.' )
    !  
  end subroutine CheckBoundaryConditionFlag


  subroutine AssignBoundaryConditionLabel( BoundCond, BoundCondLabel )
    character(len=*)             , intent(in)  :: BoundCond
    character(len=:), allocatable, intent(out) :: BoundCondLabel
    !
    if ( BoundCond .is. IncomingBoundCondFlag ) then
       allocate( BoundCondLabel, source = IncomingBoundCondLabel )
    elseif ( BoundCond .is. SinBoundCondFlag ) then
       allocate( BoundCondLabel, source = SinBoundCondLabel )
    else
       call Assert( 'Boundary condition label cannot be assign. The flag must be '//&
         IncomingBoundCondFlag//' for incoming wave, or '//&
         SinBoundCondFlag//' for sin function wave.' )
    end if
    !  
  end subroutine AssignBoundaryConditionLabel


  function BoundaryConditionStringForPrinting( BoundCondLabel ) result( Label )
    character(len=*), intent(in) :: BoundCondLabel
    character(len=:), allocatable :: Label
    !
    if ( BoundCondLabel .is. IncomingBoundCondLabel ) then
       allocate( Label, source = IncomingBoundCondMessage )
    elseif ( BoundCondLabel .is. SinBoundCondLabel ) then
       allocate( Label, source = SinBoundCondMessage )
    else
       call Assert( 'Boundary condition message cannot be determined. The label must be '//&
         IncomingBoundCondLabel//' for incoming wave, or '//&
         SinBoundCondLabel//' for sin function wave.' )
    end if
    !  
  end function BoundaryConditionStringForPrinting



  function GetEigenvaluesFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: OpLabel
    character(len=:), allocatable, optional, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if ( present( ConditionLabel ) )then
       if ( allocated(ConditionLabel) ) &
            allocate( FileName, source = BoxStatesDir//OpLabel//RootEigenValLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//OpLabel//RootEigenValLabel )
    end if
    !
  end function GetEigenvaluesFileName

  function GetEigenVecAnalFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: OpLabel
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if ( allocated(ConditionLabel) ) then
       allocate( FileName, source = BoxStatesDir//OpLabel//RootEigenVecAnalLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//OpLabel//RootEigenVecAnalLabel )
    end if
    !
  end function GetEigenVecAnalFileName

  function GetRowLabelsFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: OpLabel
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if ( allocated(ConditionLabel) ) then
       allocate( FileName, source = BoxStatesDir//OpLabel//RootRowLabelsLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//OpLabel//RootRowLabelsLabel )
    end if
    !
  end function GetRowLabelsFileName

  function GetColumnLabelsFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: OpLabel
    character(len=:), allocatable, optional, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if( present(ConditionLabel) )then
       if ( allocated(ConditionLabel) ) &
          allocate( FileName, source = BoxStatesDir//OpLabel//RootColumnLabelsLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//OpLabel//RootColumnLabelsLabel )
    end if
    !
  end function GetColumnLabelsFileName

  function GetSpectrumFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: OpLabel
    character(len=:), allocatable, optional, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    
    if ( present( ConditionLabel ) ) then
       if ( allocated(ConditionLabel) ) then
            allocate( FileName, source = BoxStatesDir//OpLabel//RootSpectrumLabel//ConditionLabel )
       else
          allocate( FileName, source = BoxStatesDir//OpLabel//RootSpectrumLabel )
       endif
    else
       allocate( FileName, source = BoxStatesDir//OpLabel//RootSpectrumLabel )
    end if
  end function GetSpectrumFileName


  function GetOperatorFileName( SymStorageDir, OpLabel, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in) :: SymStorageDir
    character(len=*)             , intent(in) :: OpLabel
    character(len=*),    optional, intent(in) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if ( present( ConditionLabel ) ) then
       allocate( FileName, source = BoxStatesDir//OpLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//OpLabel )
    end if
    !
  end function GetOperatorFileName


  function GetVirialFileName( SymStorageDir, ConditionLabel ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: BoxStatesDir
    !
    allocate( BoxStatesDir, source = AddSlash(SymStorageDir)//AddSlash(SpectrumDir) )
    call execute_command_line("mkdir -p "//BoxStatesDir )
    !
    if ( allocated(ConditionLabel) ) then
       allocate( FileName, source = BoxStatesDir//VirialLabel//ConditionLabel )
    else
       allocate( FileName, source = BoxStatesDir//VirialLabel )
    end if
    !
  end function GetVirialFileName


  
  !> Gets the full path name of the scattering states normalized according to incoming boundary conditions.
  function GetIncomingScattStateFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullScattStatesDir//GetIncomingScatteringLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetIncomingScattStateFileName



  !> Gets the full path name of the non-resonant scattering states
  !! normalized according to incoming boundary conditions.
  function GetIncomingScattStateFeshbachFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber )&
    result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullScattStatesDir//&
      GetIncomingScatteringFeshbachLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetIncomingScattStateFeshbachFileName



  !> Gets the full path name of the scattering states normalized according to sin function.
  function GetSinScattStateFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullScattStatesDir//GetSinScatteringLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetSinScattStateFileName



  !> Gets the full path name of the scattering channels.
  function GetScatteringChannelsFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullScattStatesDir//GetScatteringChannelsLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetScatteringChannelsFileName



  function GetScattStateDirName( SymStorageDir, CompMethod ) result(DirName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable :: DirName
    allocate( DirName, source = AddSlash(SymStorageDir)//AddSlash(ScatteringDir)//AddSlash(CompMethod) )
  end function GetScattStateDirName


  function GetPlotScattStateDirName( SymStorageDir, CompMethod ) result(DirName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable :: DirName
    allocate( DirName, source = GetScattStateDirName(SymStorageDir,CompMethod)//AddSlash(PlotDir) )
  end function GetPlotScattStateDirName



  function GetEigenPhaseFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullEigPhaseDir
    !
    allocate( FullEigPhaseDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullEigPhaseDir//GetIncomingScatteringLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetEigenPhaseFileName


  function GetEigenPhaseFeshbachFileName( SymStorageDir, Energy, CompMethod, ConditionLabel, ScattConditionNumber ) result(FileName)
    character(len=*)             , intent(in)    :: SymStorageDir
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullEigPhaseDir
    !
    allocate( FullEigPhaseDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    allocate( FileName, source = FullEigPhaseDir//GetIncomingScatteringFeshbachLabel(Energy,ConditionLabel,ScattConditionNumber) )
    !
  end function GetEigenPhaseFeshbachFileName



  function GetEigenPhaseDirName( SymStorageDir, CompMethod ) result(DirName)
    character(len=*)             , intent(in)    :: SymStorageDir
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable :: DirName
    allocate( DirName, source = AddSlash(SymStorageDir)//AddSlash(ScatteringDir)//AddSlash(CompMethod)//AddSlash(EigenPhasesDir) )
  end function GetEigenPhaseDirName



  !> Gets the file name of the scattering states normalized with
  !! incoming boundary conditions.

  function GetIncomingScatteringLabel( Energy, ConditionLabel, ScattConditionNumber) result(Label)
    implicit none 
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: EnergyStrn, CondNumStrn
    !
    write(EnergyStrn,*) Energy
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            IncomingBoundCondLabel//'_'//ConditionLabel )
    else
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            IncomingBoundCondLabel)
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetIncomingScatteringLabel

  !> Gets the file name of the non-resonant scattering states 
  !! normalized with incoming boundary conditions.
  function GetIncomingScatteringFeshbachLabel( Energy, ConditionLabel, ScattConditionNumber) result(Label)
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: EnergyStrn, CondNumStrn
    !
    write(EnergyStrn,*) Energy
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            IncomingBoundCondLabel//FeshbachLabel//'_'//ConditionLabel )
    else
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            IncomingBoundCondLabel//FeshbachLabel)
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetIncomingScatteringFeshbachLabel


  !> Gets the file name of the scattering states normalized as
  !! sin function.
  function GetSinScatteringLabel( Energy, ConditionLabel, ScattConditionNumber) result(Label)
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: EnergyStrn, CondNumStrn
    !
    write(EnergyStrn,*) Energy
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            SinBoundCondLabel//'_'//ConditionLabel )
    else
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            SinBoundCondLabel)
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetSinScatteringLabel



  !> Gets the file name of the scattering channels.
  function GetScatteringChannelsLabel( Energy, ConditionLabel, ScattConditionNumber) result(Label)
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: EnergyStrn, CondNumStrn
    !
    write(EnergyStrn,*) Energy
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            ScattChannelLabel//'_'//ConditionLabel )
    else
       allocate( RootFileName, source = RootEnergy//trim(adjustl(EnergyStrn))//&
            ScattChannelLabel)
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetScatteringChannelsLabel



  function GetIncomingConditioningLabel( ConditionLabel, ScattConditionNumber) result(Label)
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: CondNumStrn
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = ConditionLabel//IncomingBoundCondLabel )
    else
       allocate( RootFileName, source = IncomingBoundCondLabel )
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetIncomingConditioningLabel


  function GetSinConditioningLabel( ConditionLabel, ScattConditionNumber) result(Label)
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0)) , optional   , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    character(len=:), allocatable :: RootFileName
    character(len=64) :: CondNumStrn
    !
    if ( allocated(ConditionLabel) ) then
       allocate( RootFileName, source = ConditionLabel//SinBoundCondLabel )
    else
       allocate( RootFileName, source = SinBoundCondLabel )
    end if
    !
    if ( present(ScattConditionNumber) ) then
       write(CondNumStrn,*) ScattConditionNumber
       allocate( Label, source = RootFileName//RootCondNumber//trim(adjustl(CondNumStrn)) )
    else
       allocate( Label, source = RootFileName )
    end if
    !
  end function GetSinConditioningLabel



  function GetScattConditioningLabel( CompMethod, ConditionLabel, ScattConditionNumber, UseSinStates ) result(FinalLabel)
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    logical                      , intent(in)    :: UseSinStates
    character(len=:), allocatable :: Label, FinalLabel
    !
    if ( CONDITION_QC ) then
       if ( (CompMethod .is. HSLEMethodLabel) .or. &
            (CompMethod .is. InvMatMethodLabel) ) then
          if ( UseSinStates ) then    
             allocate( Label, source = &
                  GetSinConditioningLabel(&
                  ConditionLabel,ScattConditionNumber ) )
          else
             allocate( Label, source = &
                  GetIncomingConditioningLabel(&
                  ConditionLabel,ScattConditionNumber ) )
          end if
       else
          if ( UseSinStates ) then    
             allocate( Label, source = &
                  GetSinConditioningLabel(  &
                  ConditionLabel       ) )
          else
             allocate( Label, source = &
                  GetIncomingConditioningLabel(  &
                  ConditionLabel       ) )
          end if
       end if
    else
       if ( UseSinStates ) then    
          allocate( Label, source = &
               GetSinConditioningLabel(  &
               ConditionLabel       ) )
       else
          allocate( Label, source = &
               GetIncomingConditioningLabel(  &
               ConditionLabel       ) )
       end if
    end if
    !
    allocate( FinalLabel, source = Label )
    !
  end function GetScattConditioningLabel



  !> Gets the final label of the scattering states normalized following incoming boundary conditions.
  function GetIncomingFinalScatteringLabel( CompMethod, Energy, ConditionLabel, ScattConditionNumber) result(Label)
    character(len=*)             , intent(in)    :: CompMethod
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    !
    if ( CONDITION_QC ) then
       if ( (CompMethod .is. HSLEMethodLabel) .or. &
            (CompMethod .is. InvMatMethodLabel) ) then
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetIncomingScatteringLabel(  &
               Energy             , &
               ConditionLabel        , &
               ScattConditionNumber ) )
       else
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetIncomingScatteringLabel(  &
               Energy                  , &
               ConditionLabel       ) )
       end if
    else
       if (allocated(Label)) deallocate(Label)
       allocate( Label, source = &
            GetIncomingScatteringLabel(  &
            Energy                  , &
            ConditionLabel       ) )
    end if
    !
  end function GetIncomingFinalScatteringLabel



  !> Gets the final label of the non-resonant scattering states
  !! normalized following incoming boundary conditions.
  function GetIncomingFinalScatteringFeshbachLabel( CompMethod, Energy, ConditionLabel, ScattConditionNumber) result(Label)
    character(len=*)             , intent(in)    :: CompMethod
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    !
    if ( CONDITION_QC ) then
       if ( (CompMethod .is. HSLEMethodLabel) .or. &
            (CompMethod .is. InvMatMethodLabel) ) then
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetIncomingScatteringFeshbachLabel(  &
               Energy             , &
               ConditionLabel        , &
               ScattConditionNumber ) )
       else
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetIncomingScatteringFeshbachLabel(  &
               Energy                  , &
               ConditionLabel       ) )
       end if
    else
       if (allocated(Label)) deallocate(Label)
       allocate( Label, source = &
            GetIncomingScatteringFeshbachLabel(  &
            Energy                  , &
            ConditionLabel       ) )
    end if
    !
  end function GetIncomingFinalScatteringFeshbachLabel



  !> Gets the final label of the scattering states normalized as sin function.
  function GetSinFinalScatteringLabel( CompMethod, Energy, ConditionLabel, ScattConditionNumber) result(Label)
    character(len=*)             , intent(in)    :: CompMethod
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    !
    if ( CONDITION_QC ) then
       if ( (CompMethod .is. HSLEMethodLabel) .or. &
            (CompMethod .is. InvMatMethodLabel) ) then
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetSinScatteringLabel(  &
               Energy             , &
               ConditionLabel        , &
               ScattConditionNumber ) )
       else
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetSinScatteringLabel(  &
               Energy                  , &
               ConditionLabel       ) )
       end if
    else
       if (allocated(Label)) deallocate(Label)
       allocate( Label, source = &
            GetSinScatteringLabel(  &
            Energy                  , &
            ConditionLabel       ) )
    end if
    !
  end function GetSinFinalScatteringLabel


  !> Gets the final label of the scattering channels.
  function GetScatteringChannelsFinalLabel( CompMethod, Energy, ConditionLabel, ScattConditionNumber) result(Label)
    character(len=*)             , intent(in)    :: CompMethod
    real(kind(1d0))              , intent(in)    :: Energy
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable :: Label
    !
    if ( CONDITION_QC ) then
       if ( (CompMethod .is. HSLEMethodLabel) .or. &
            (CompMethod .is. InvMatMethodLabel) ) then
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetScatteringChannelsLabel(  &
               Energy             , &
               ConditionLabel        , &
               ScattConditionNumber ) )
       else
          if (allocated(Label)) deallocate(Label)
          allocate( Label, source = &
               GetScatteringChannelsLabel(  &
               Energy                  , &
               ConditionLabel       ) )
       end if
    else
       if (allocated(Label)) deallocate(Label)
       allocate( Label, source = &
            GetScatteringChannelsLabel(  &
            Energy                  , &
            ConditionLabel       ) )
    end if
    !
  end function GetScatteringChannelsFinalLabel



  subroutine GetIncomingScatteringFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       ScatteringFilesVec         )
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: ScatteringFilesVec(:)
    !
    character(len=:), allocatable :: ScattStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored scattering states file names ...'
    !
    allocate( ScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         ScattStatesDir           , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         ScatteringFilesVec       )
    !
  end subroutine GetIncomingScatteringFilesVec



  subroutine GetIncomingScatteringFeshbachFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       ScatteringFilesVec         )
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: ScatteringFilesVec(:)
    !
    character(len=:), allocatable :: ScattStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored scattering states file names ...'
    !
    allocate( ScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringFeshbachLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         ScattStatesDir           , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         ScatteringFilesVec       )
    !
  end subroutine GetIncomingScatteringFeshbachFilesVec



  subroutine GetSinScatteringFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       ScatteringFilesVec         )
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: ScatteringFilesVec(:)
    !
    character(len=:), allocatable :: ScattStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored scattering states file names ...'
    !
    allocate( ScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetSinFinalScatteringLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         ScattStatesDir           , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         ScatteringFilesVec       )
    !
  end subroutine GetSinScatteringFilesVec



  subroutine GetScatteringChannelsFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       ScatteringFilesVec         )
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: ScatteringFilesVec(:)
    !
    character(len=:), allocatable :: ScattStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored scattering states file names ...'
    !
    allocate( ScattStatesDir, source = GetScattStateDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetScatteringChannelsFinalLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         ScattStatesDir           , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         ScatteringFilesVec       )
    !
  end subroutine GetScatteringChannelsFilesVec



  subroutine GetEigenphasesFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       EigenPhasesFilesVec        )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: EigenPhasesFilesVec(:)
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored eigenphases file names ...'
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         EigenPhasesStatesDir     , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         EigenPhasesFilesVec      )
    !
  end subroutine GetEigenphasesFilesVec



  subroutine GetEigenphasesFeshbachFilesVec( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       EigenPhasesFilesVec        )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=256), allocatable, intent(out)   :: EigenPhasesFilesVec(:)
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel
    !
    write(output_unit,'(a)') 'Getting valid stored eigenphases file names ...'
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringFeshbachLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    call GetListOfFilesWithPattern( &
         EigenPhasesStatesDir     , &
         ScattStatesLabel         , &
         SeparationCharacterLabel , &
         RootEnergy               , &
         EMin, EMax               , &
         EigenPhasesFilesVec      )
    !
  end subroutine GetEigenphasesFeshbachFilesVec



  subroutine GetConcatenatedEigenphasesFileName( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       FileName         )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable  , intent(out)   :: FileName
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel, Label
    integer :: ichar
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    !
    ichar = index(ScattStatesLabel,SeparationCharacterLabel)
    !
    if ( ichar > 0 ) then
       !
       allocate( Label, source = ScattStatesLabel(ichar:) )
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootEigenphasesLabel//&
            GetEnergyRangeLabel(Emin,Emax)//&         
            Label )
       !
    else
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootEigenphasesLabel//&
            GetEnergyRangeLabel(Emin,Emax) )
       !
    end if
    !
  end subroutine GetConcatenatedEigenphasesFileName



  subroutine GetConcatenatedEigenphasesFeshbachFileName( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       FileName         )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable  , intent(out)   :: FileName
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel, Label
    integer :: ichar
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringFeshbachLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    !
    ichar = index(ScattStatesLabel,SeparationCharacterLabel)
    !
    if ( ichar > 0 ) then
       !
       allocate( Label, source = ScattStatesLabel(ichar:) )
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootEigenphasesLabel//&
            GetEnergyRangeLabel(Emin,Emax)//&         
            Label )
       !
    else
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootEigenphasesLabel//&
            GetEnergyRangeLabel(Emin,Emax) )
       !
    end if
    !
  end subroutine GetConcatenatedEigenphasesFeshbachFileName



  subroutine GetConcatenatedTotalPhaseFileName( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       FileName         )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable  , intent(out)   :: FileName
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel, Label
    integer :: ichar
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    !
    ichar = index(ScattStatesLabel,SeparationCharacterLabel)
    !
    if ( ichar > 0 ) then
       !
       allocate( Label, source = ScattStatesLabel(ichar:) )
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootTotalPhaseLabel//&
            GetEnergyRangeLabel(Emin,Emax)//&         
            Label )
       !
    else
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootTotalPhaseLabel//&
            GetEnergyRangeLabel(Emin,Emax) )
       !
    end if
    !
  end subroutine GetConcatenatedTotalPhaseFileName



  subroutine GetConcatenatedTotalPhaseFeshbachFileName( &
       SymStorageDir              , &
       CompMethod                 , &
       Emin, Emax                 , &
       ScattConditionNumber       , &  
       ConditionLabel             , &
       FileName         )
    !
    character(len=*)               , intent(in)    :: SymStorageDir
    character(len=*)               , intent(in)    :: CompMethod
    real(kind(1d0))                , intent(in)    :: Emin
    real(kind(1d0))                , intent(in)    :: Emax
    real(kind(1d0))                , intent(in)    :: ScattConditionNumber
    character(len=:)  , allocatable, intent(inout) :: ConditionLabel
    character(len=:), allocatable  , intent(out)   :: FileName
    !
    character(len=:), allocatable :: EigenPhasesStatesDir, ScattStatesLabel, Label
    integer :: ichar
    !
    allocate( EigenPhasesStatesDir, source = GetEigenPhaseDirName(SymStorageDir,CompMethod) )
    !
    !.. Takes a sample of the scattering states file names (not the full path)
    allocate( ScattStatesLabel, source = &
         GetIncomingFinalScatteringFeshbachLabel(&
         CompMethod,Emin,ConditionLabel,ScattConditionNumber) )
    !
    !
    ichar = index(ScattStatesLabel,SeparationCharacterLabel)
    !
    if ( ichar > 0 ) then
       !
       allocate( Label, source = ScattStatesLabel(ichar:) )
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootTotalPhaseLabel//&
            GetEnergyRangeLabel(Emin,Emax)//&         
            Label )
       !
    else
       !
       allocate( FileName, source = &
            AddSlash(EigenPhasesStatesDir)//&
            RootTotalPhaseLabel//&
            GetEnergyRangeLabel(Emin,Emax) )
       !
    end if
    !
  end subroutine GetConcatenatedTotalPhaseFeshbachFileName




  subroutine BuildInvEnergMat( EigenEnergies, Energy, EnergMatInv )
    real(kind(1d0))   , intent(in)  :: EigenEnergies(:)
    real(kind(1d0))   , intent(in)  :: Energy
    class(ClassMatrix), intent(out) :: EnergMatInv
    integer :: i, j, n
    real(kind(1d0)), allocatable :: Array(:,:)
    !
    n = size(EigenEnergies)
    allocate( Array(n,n) )
    Array = 0.d0
    !
    do i = 1, n
       Array(i,i) = 1.d0/(EigenEnergies(i) - Energy)
    end do
    !
    EnergMatInv = Array
    deallocate( Array )
    !
  end subroutine BuildInvEnergMat



  subroutine CheckSolution( SystemMat, NullSpaceMat )
    class(ClassMatrix), intent(in) :: SystemMat
    class(ClassMatrix), intent(in) :: NullSpaceMat
    integer :: j
    type(ClassMatrix) :: CopySysMat, NullSpaceColumn
    real(kind(1d0)), allocatable :: Residue(:,:), NullSpaceArray(:,:)
    real(kind(1d0)) :: VecNorm
    !
!!$    call NullSpaceMat%FetchMatrix(NullSpaceArray)
!!$    do j = 1, NullSpaceMat%NColumns()
!!$       NullSpaceColumn = NullSpaceArray(:,j)
!!$       if ( NullSpaceColumn%IsZero(DEFINED_TOLERANCE) ) call Assert( &
!!$            'The column '//AlphabeticNumber(j)//' of the null space is zero.' )
!!$    end do
    !
    CopySysMat = SystemMat
    call CopySysMat%Multiply( NullSpaceMat, 'Right', 'N' )
    call CopySysMat%FetchMatrix( Residue )
    call CopySysMat%Free()
    !
    do j = 1, size(Residue,2)
       VecNorm = sqrt(DOT_PRODUCT(Residue(:,j),Residue(:,j)))
       write(output_unit,*) "Norm2 residue",j,"=",VecNorm
    end do
    !
  end subroutine CheckSolution




  subroutine SetSolution( Energy, PartialSol, Solution, PartialIsSolution )
    real(kind(1d0))                      , intent(in)  :: Energy
    class(ClassMatrix)                   , intent(in)  :: PartialSol
    class(ClassComplexSpectralResolution), intent(out) :: Solution
    logical, optional                    , intent(in)  :: PartialIsSolution
    !
    integer :: j, NRows, NCols
    complex(kind(1d0)), allocatable :: EnVec(:), PartialSolArray(:,:), EigVec(:,:)
    type(ClassComplexMatrix) :: CompPartialSol
    !
    NCols = PartialSol%NColumns()
    if ( present(PartialIsSolution) .and. PartialIsSolution ) then
       NRows = PartialSol%NRows()
    else
       NRows = PartialSol%NRows() + NCols
    end if
    !
    call Solution%Init( NRows, NCols ) 
    !
    allocate( EnVec(NCols) )       
    EnVec = Z1*Energy
    call Solution%SetEigenValues( EnVec )
    !
    CompPartialSol = PartialSol
    call CompPartialSol%FetchMatrix( PartialSolArray )
    if ( present(PartialIsSolution) .and. PartialIsSolution ) then
       call Solution%SetEigenVectors( PartialSolArray )
       return
    end if
    !
    call Solution%Fetch( EigVec )
    EigVec(1:PartialSol%NRows(),:) = PartialSolArray(:,:)
    do j = 1, NCols
       EigVec(PartialSol%NRows()+j,j) = -Z1
    end do
    call Solution%SetEigenVectors( EigVec )
    !
  end subroutine SetSolution



  subroutine ComputeBackSustSol( NumSol, ArrayLU, Sol )
    !
    integer                     , intent(in)  :: NumSol
    real(kind(1d0))             , intent(in)  :: ArrayLU(:,:)
    real(kind(1d0)), allocatable, intent(out) :: Sol(:,:)
    !
    integer :: i, j, n, NumElem
    complex(kind(1d0)) :: Sum
    !
    NumElem = size(ArrayLU,1)
    !
    allocate( Sol(NumElem,NumSol) )
    Sol = 0.d0
    !
    do j = 1, NumSol
       Sol(NumElem-NumSol+j,j) = -1.d0
    end do
    !
    do j = 1, NumSol
       do i = NumElem-NumSol, 1, -1
          Sum = 0.d0
          do n = i+1, NumElem
             Sum = Sum + ArrayLU(i,n)*Sol(n,j)
          end do
          Sol(i,j) = -Sum/ArrayLU(i,i)
       end do
    end do
!!$    !.. Print last N diagonal entries of the U matrix in the
!!$    ! LU decomposition. Where N is NPWC + 1, being NLastBs
!!$    ! the number of channel functions. The last NPWC entries should 
!!$    ! be zero, and the remaining entry must be dfferent from zero
!!$    ! the method to be applicable.
!!$    write(output_unit,'(a)') "ArrayLU diag :"
!!$    do i = NumElem, NumElem-NumSol, -1
!!$       write(output_unit,'(i,f)') i, ArrayLU(i,i)
!!$    end do
!!$    write(output_unit,'(L)') HaveInfElement(ArrayLU)
    !
  end subroutine ComputeBackSustSol



  subroutine CheckInverseMat( OriginalMat, InverseMat, Tolerance )
    class(ClassMatrix), intent(in) :: OriginalMat
    class(ClassMatrix), intent(in) :: InverseMat
    real(kind(1d0))   , intent(in) :: Tolerance
    type(Classmatrix) :: CopyOrigMat
    CopyOrigMat = OriginalMat
    call CopyOrigMat%Multiply( InverseMat, 'Right', 'N' )
    if ( .not. CopyOrigMat%IsIdentity( Tolerance ) ) call Assert( &
         "The computed inverse matrix is not good enough." )
    !
  end subroutine CheckInverseMat



  integer function GetNumOpenChannels( SymSpace, Energy, NChannels ) result(NOC)
    class(ClassSymmetricElectronicSpace), intent(in) :: SymSpace
    real(kind(1d0))                     , intent(in) :: Energy
    integer                             , intent(in) :: NChannels
    integer :: i
    real(kind(1d0)) :: PIEnergy
    NOC = 0
    do i = 1, NChannels
       PIEnergy = SymSpace%GetChannelPIEnergy(i)
       if ( (Energy-PIEnergy) > 0.d0 ) NOC = NOC + 1
    end do
  end function GetNumOpenChannels



  subroutine GetOpenChannelsIndex( SymSpace, Energy, NChannels, IndexVec )
    class(ClassSymmetricElectronicSpace), intent(in)  :: SymSpace
    real(kind(1d0))                     , intent(in)  :: Energy
    integer                             , intent(in)  :: NChannels
    integer, allocatable                , intent(out) :: IndexVec(:)
    integer :: i, NOC
    real(kind(1d0)) :: PIEnergy
    integer, allocatable :: AuxVec(:)
    !
    allocate( AuxVec(NChannels) )
    AuxVec = 0
    NOC = 0
    do i = 1, NChannels
       PIEnergy = SymSpace%GetChannelPIEnergy(i)
       if ( (Energy-PIEnergy) > 0.d0 ) then
          NOC = NOC + 1
          AuxVec(NOC) = i
       end if
    end do
    if ( NOC == 0 ) return
    allocate( IndexVec, source = AuxVec(1:NOC) )
    !
  end subroutine GetOpenChannelsIndex


  subroutine ComputeValuesBorder( Basis, SymSpace, CoeffMat, Energy, FunValBorder, FunDerValBorder )
    class(ClassBasis)                   , intent(in)    :: Basis
    class(ClassSymmetricElectronicSpace), intent(in)    :: SymSpace
    class(ClassComplexMatrix)           , intent(in)    :: CoeffMat(:,:)
    real(kind(1d0))                     , intent(in)    :: Energy
    class(ClassMatrix)                  , intent(out)   :: FunValBorder
    class(ClassMatrix)                  , intent(out)   :: FunDerValBorder
    !
    integer :: NChannels, i, j, NumOpenChannels, RowIndex, ColIndex
    integer, allocatable :: IndexOpenChannels(:)
    real(kind(1d0)) :: Rbox, PIEnergy, RelEnergy, MomentumMod, ValBorder, ValDerBorder
    complex(kind(1d0)), allocatable :: AuxArray(:,:)
    real(kind(1d0)), allocatable :: AuxVec(:)
    !
    NChannels = size(CoeffMat,1)
    NumOpenChannels = GetNumOpenChannels( SymSpace, Energy, NChannels )
    if ( NumOpenChannels == 0 ) return
    !
    call GetOpenChannelsIndex( SymSpace, Energy, NChannels, IndexOpenChannels )
    !
    call FunValBorder%InitFull( NumOpenChannels, NumOpenChannels )
    call FunDerValBorder%InitFull( NumOpenChannels, NumOpenChannels )
    !
    Rbox = Basis%GetRMax()
    !
    !
    do j = 1, NumOpenChannels
       !
       ColIndex = IndexOpenChannels(j)
       !
       do i = 1, NumOpenChannels
          !
          RowIndex = IndexOpenChannels(i)
          PIEnergy = SymSpace%GetChannelPIEnergy(RowIndex)
          RelEnergy = Energy - PIEnergy
          if ( RelEnergy < 0.d0 ) call Assert( &
               'It is supposed to be an open channel but it is not.' )
          MomentumMod = sqrt(2.d0*RelEnergy)
          call CoeffMat(RowIndex,ColIndex)%FetchMatrix( AuxArray )
          allocate( AuxVec, source = dble(AuxArray(:,1)) )
          deallocate( AuxArray )
          !
          ValBorder = Basis%Eval( RBox, AuxVec )
          ValDerBorder = Basis%Eval( RBox, AuxVec, 1 )
          deallocate( AuxVec )
          !
          call FunValBorder%SetElement( i, j, ValBorder )
          call FunDerValBorder%SetElement( i, j, ValDerBorder )
          !
       end do
    end do
    !
  end subroutine ComputeValuesBorder



  subroutine ComputeCoeffForMatS( Basis, SymSpace, CoeffMat, Energy, MatA, MatB )
    class(ClassBasis)                   , intent(in)    :: Basis
    class(ClassSymmetricElectronicSpace), intent(in)    :: SymSpace
    class(ClassComplexMatrix)           , intent(in)    :: CoeffMat(:,:)
    real(kind(1d0))                     , intent(in)    :: Energy
    class(ClassMatrix)                  , intent(out)   :: MatA
    class(ClassMatrix)                  , intent(out)   :: MatB
    !
    logical, parameter :: FORCE_RCWF = .TRUE.
    !
    integer :: NChannels, i, j, k, NumOpenChannels, RowIndex, ColIndex, AngMom, info
    integer, allocatable :: IndexOpenChannels(:)
    real(kind(1d0)) :: MomentumMod, PIEnergy, RelEnergy, NormConst, PICharge
    real(kind(1d0)) :: XGridMax, XGridMin, R0, FFVal, FDVal, GFVal, GDVal
    real(kind(1d0)) :: DerCoulRegFunR0, DerCoulIrregFunR0, CoulRegModSqr, CoulIrregModSqr, RegIrregDotProd, CompRegDotProd
    real(kind(1d0)) :: CompIrregDotProd, Denom, Det, ValA, ValB
    real(kind(1d0)), allocatable :: XGrid(:), FF(:), FD(:), GF(:), GD(:), CompFun(:), CoulRegFun(:), CoulIrregFun(:)
    complex(kind(1d0)), allocatable :: AuxArray(:,:)
    type(ClassMatrix) :: FunValBorder, FunDerValBorder 
    
    !
    NChannels = size(CoeffMat,1)
    NumOpenChannels = GetNumOpenChannels( SymSpace, Energy, NChannels )
    if ( NumOpenChannels == 0 ) return
    !
    call GetOpenChannelsIndex( SymSpace, Energy, NChannels, IndexOpenChannels )
    !
    call MatA%InitFull(NumOpenChannels, NumOpenChannels)
    call MatB%InitFull(NumOpenChannels, NumOpenChannels)
    !.. The parent-ion charge is the same for all channels.
    PICharge = SymSpace%GetPICharge(1)
    !
    allocate( CompFun(N_FITTING_POINTS) )
    CompFun = 0.d0
    allocate( CoulRegFun(N_FITTING_POINTS) )
    CoulRegFun = 0.d0
    allocate( CoulIrregFun(N_FITTING_POINTS) )
    CoulIrregFun = 0.d0
    !
    !
    if ( EVALUATE_FUN_BORDER ) then
       !
       call ComputeValuesBorder( Basis, SymSpace, CoeffMat, Energy, FunValBorder, FunDerValBorder )
       !
       
       allocate( FF(NumOpenChannels) )
       FF = 0.d0
       allocate( FD(NumOpenChannels) )
       FD = 0.d0
       allocate( GF(NumOpenChannels) )
       GF = 0.d0
       allocate( GD(NumOpenChannels) )
       GD = 0.d0
       !
       do i = 1, NumOpenChannels
          RowIndex = IndexOpenChannels(i)
          AngMom   = SymSpace%GetChannelL(RowIndex)
          PIEnergy = SymSpace%GetChannelPIEnergy(RowIndex)
          RelEnergy = Energy - PIEnergy
          if ( RelEnergy < 0.d0 ) call Assert( &
               'It is supposed to be an open channel for A and B matrices but it is not.' )
          MomentumMod = sqrt(2.d0*RelEnergy)
          NormConst = sqrt(2.d0/PI/MomentumMod)
          R0 = MomentumMod * Basis%GetRMax()
          call SF_Coulomb( R0, -PICharge/MomentumMod, dble(AngMom), &
               FFVal, GFVal, FDVal, GDVal, INFO, FORCE_RCWF )
          FF(i) = FFVal * NormConst
          FD(i) = FDVal * NormConst * MomentumMod
          GF(i) = GFVal * NormConst
          GD(i) = GDVal * NormConst * MomentumMod
       end do
       !
    end if
    !
    !
    do j = 1, NumOpenChannels
       ColIndex = IndexOpenChannels(j)
       do i = 1, NumOpenChannels
          !
          RowIndex = IndexOpenChannels(i)
          !
          if ( EVALUATE_FUN_BORDER ) then
             !
             Det = FF(i)*GD(i)-FD(i)*GF(i)
             ValA = 1.d0/Det * ( GD(i)*FunValBorder%Element(i,j)-GF(i)*FunDerValBorder%Element(i,j) )
             ValB = 1.d0/Det * ( -FD(i)*FunValBorder%Element(i,j)+FF(i)*FunDerValBorder%Element(i,j) )
             !
          else
             !
             AngMom   = SymSpace%GetChannelL(RowIndex)
             PIEnergy = SymSpace%GetChannelPIEnergy(RowIndex)
             RelEnergy = Energy - PIEnergy
             if ( RelEnergy < 0.d0 ) call Assert( &
                  'It is supposed to be an open channel for A and B matrices but it is not.' )
             !
             MomentumMod = sqrt(2.d0*RelEnergy)
             NormConst = sqrt(2.d0/PI/MomentumMod)
             !
             XGridMax = Basis%GetRMax()
             XGridMin = max(XGridMax/2, XGridMax-2.d0*PI/MomentumMod)
             call BuildGrid( N_FITTING_POINTS, XGridMin, XGridMax, XGrid ) 
!!$             call CoeffMat(i,j)%FetchMatrix( AuxArray )
             call CoeffMat(RowIndex,ColIndex)%FetchMatrix( AuxArray )
             !
             do k = 1, N_FITTING_POINTS
                !..Tabulate the computed function.
                CompFun(k) = Basis%Eval( XGrid(k), dble(AuxArray(:,1)) )
                !.. Tabulate the regular and irregular Coulomb functions 
                R0 = Xgrid(k)*MomentumMod
                call SF_Coulomb( R0, -PICharge/MomentumMod, dble(AngMom), &
                     CoulRegFun(k), CoulIrregFun(k)                     , &
                     DerCoulRegFunR0, DerCoulIrregFunR0                 , &
                  INFO, FORCE_RCWF )   
             end do
             !
             deallocate( AuxArray )
             CoulRegFun   = NormConst * CoulRegFun
             CoulIrregFun = NormConst * CoulIrregFun
             CoulRegModSqr    = sum( CoulRegFun*CoulRegFun )
             CoulIrregModSqr  = sum( CoulIrregFun*CoulIrregFun )
             RegIrregDotProd  = sum( CoulRegFun*CoulIrregFun )
             CompRegDotProd   = sum( CompFun*CoulRegFun )
             CompIrregDotProd = sum( CompFun*CoulIrregFun )
             !
             Denom = CoulRegModSqr*CoulIrregModSqr - RegIrregDotProd*RegIrregDotProd
             ValA = ( CoulIrregModSqr*CompRegDotProd - RegIrregDotProd*CompIrregDotProd )/Denom
             ValB = ( CoulRegModSqr*CompIrregDotProd - RegIrregDotProd*CompRegDotProd )/Denom
             !
          end if
          !
          call MatA%SetElement( i, j, ValA ) 
          call MatB%SetElement( i, j, ValB ) 
          !
       end do
    end do
    !
  end subroutine ComputeCoeffForMatS
  


  
  
  !> Normalizes the scattering states according to incoming boundary conditions.
  subroutine ComputeIncomingNormalizedStates( SymSpace, Energy, MatA, MatB, ScattStatesMat )
    class(ClassSymmetricElectronicSpace), intent(in)    :: SymSpace
    real(kind(1d0))                     , intent(in)    :: Energy
    class(ClassMatrix)                  , intent(in)    :: MatA
    class(ClassMatrix)                  , intent(in)    :: MatB
    class(ClassComplexMatrix)           , intent(inout) :: ScattStatesMat
    !
    type(ClassComplexMatrix) :: MatCompA, MatCompB, MatUT
    integer :: i, j, NChannels, NumOpenChannels, RowIndex, NRows
    integer, allocatable :: IndexOpenChannels(:)
    complex(kind(1d0)), allocatable :: NewEigVecArray(:,:), OldEigVecArray(:,:)
    !
    NChannels = ScattStatesMat%NColumns()
    NRows = ScattStatesMat%NRows()
    NumOpenChannels = GetNumOpenChannels( SymSpace, Energy, NChannels )
    call GetOpenChannelsIndex( SymSpace, Energy, NChannels, IndexOpenChannels )
    !
    allocate( NewEigVecArray(NRows,NumOpenChannels) )
    NewEigVecArray = Z0
    call ScattStatesMat%FetchMatrix( OldEigVecArray )
    !
    MatCompA = MatA
    MatCompB = MatB
    call MatCompB%Multiply( Zi )
    call MatCompA%Multiply( Z1 )
    call MatCompB%Add( MatCompA )
    call MatCompA%Free()
    call MatCompB%Inverse( MatUT )
    call MatCompB%Free()
    !
    do j = 1, NumOpenChannels
       do i = 1, NumOpenChannels
          RowIndex = IndexOpenChannels(i)
          NewEigVecArray(:,j) = NewEigVecArray(:,j) + &
               OldEigVecArray(:,RowIndex)*MatUT%Element(i,j)
       end do
    end do
    !
    deallocate( OldEigVecArray )
    call ScattStatesMat%Free()
    ScattStatesMat = NewEigVecArray
    !
  end subroutine ComputeIncomingNormalizedStates



  !> Normalizes the scattering states with the asymptotic condition
  !!\f$\sqrt{\frac{2}{\pi k}}\frac{\sin(kr+\frac{Z}{k}\ln(2kr)-\frac{\ell\pi}{2}+\sigma_{k,\ell})}{r}\f$ 
  !!in one channel and a sum over all the channels of
  !! \f$\sqrt{\frac{2}{\pi k}}\frac{\cos(kr+\frac{Z}{k}\ln(2kr)-\frac{\ell\pi}{2}+\sigma_{k,\ell})}{r}\f$ .
  subroutine ComputeSinNormalizedStates( SymSpace, Energy, MatA, MatB, ScattStatesMat )
    class(ClassSymmetricElectronicSpace), intent(in)    :: SymSpace
    real(kind(1d0))                     , intent(in)    :: Energy
    class(ClassMatrix)                  , intent(in)    :: MatA
    class(ClassMatrix)                  , intent(in)    :: MatB
    class(ClassComplexMatrix)           , intent(inout) :: ScattStatesMat
    !
    type(ClassComplexMatrix) :: MatUT, MatCompA
    integer :: i, j, NChannels, NumOpenChannels, RowIndex, NRows
    integer, allocatable :: IndexOpenChannels(:)
    complex(kind(1d0)), allocatable :: NewEigVecArray(:,:), OldEigVecArray(:,:)
    real(kind(1d0)) :: Phase
    !
    NChannels = ScattStatesMat%NColumns()
    NRows = ScattStatesMat%NRows()
    NumOpenChannels = GetNumOpenChannels( SymSpace, Energy, NChannels )
    call GetOpenChannelsIndex( SymSpace, Energy, NChannels, IndexOpenChannels )
    !
    allocate( NewEigVecArray(NRows,NumOpenChannels) )
    NewEigVecArray = Z0
    call ScattStatesMat%FetchMatrix( OldEigVecArray )
    !
    MatCompA = MatA
    call MatCompA%Inverse(MatUT)
    !
    do j = 1, NumOpenChannels
       do i = 1, NumOpenChannels
          RowIndex = IndexOpenChannels(i)
          NewEigVecArray(:,j) = NewEigVecArray(:,j) + &
               OldEigVecArray(:,RowIndex)*MatUT%Element(i,j)
       end do
    end do
    !
    deallocate( OldEigVecArray )
    call ScattStatesMat%Free()
    ScattStatesMat = NewEigVecArray
    !
  end subroutine ComputeSinNormalizedStates


  
 
  subroutine ComputeS( MatA, MatB, MatS )
    class(ClassMatrix)       , intent(in)  :: MatA
    class(ClassMatrix)       , intent(in)  :: MatB
    class(ClassComplexMatrix), intent(out) :: MatS
    !
    type(ClassComplexMatrix) :: MatDenom, AuxMat
    !
    AuxMat = MatA
    !
    MatS = MatB
    call MatS%Multiply( Zi )
    call MatS%Add( AuxMat )
    !
    MatDenom = MatB
    call MatDenom%Multiply( -Zi )
    call MatDenom%Add( AuxMat )
    call AuxMat%Free()
    call MatDenom%Inverse( AuxMat )
    call MatDenom%Free()
    !
    call MatS%Multiply( AuxMat, 'Right', 'N' )
    if ( .not.MatS%IsUnitary( DEFINED_TOLERANCE ) ) call ErrorMessage( &
         'The S scattering matrix is not unitary.' )
    !
  end subroutine ComputeS
  



  subroutine ComputeEigenPhases( MatS, EigenPhases, EigenVectors, FirstEnergy )
    !> S scattering matrix.
    class(ClassComplexMatrix)     , intent(in)  :: MatS
    !> S scattering matrix eigenphases.
    class(ClassMatrix)            , intent(out) :: EigenPhases
    !> S scattering matrix eigenvectors.
    class(ClassComplexMatrix)     , intent(out) :: EigenVectors
    !> If .true. this is the first energy to be computed in the array.
    logical                       , intent(in)  :: FirstEnergy
    !
    integer :: NOpenChannels, i, OldIndex, j
    type(ClassComplexSpectralResolution) :: SpecRes
    complex(kind(1d0)), allocatable :: EigenValues(:), EigenVec(:,:)
    integer, allocatable :: TransfIndex(:), NewIndex(:)
    real(kind(1d0)) :: RealVal, ImagVal, phi, w1
    !.. Variables that will be kept in the memory after the subroutine returns.
    complex(kind(1d0)), allocatable, save :: OldEigenVec(:,:)
    type(ClassMatrix)              , save :: OldPhases
    !
    NOpenChannels = MatS%NColumns()
    call EigenPhases%InitFull( NOpenChannels, 1 )
    allocate( NewIndex(NOpenChannels) )
    !
    call MatS%Diagonalize( SpecRes )
    call SpecRes%Fetch( EigenValues )
    call CheckMatSEigenvalues( EigenValues )
    call SpecRes%Fetch( EigenVec )
    !
    if ( FirstEnergy ) then
       call OldPhases%InitFull( NOpenChannels, 1 )
       allocate( OldEigenVec, source = EigenVec )
    end if
    !
    call EigenVectorsOrdering( EigenVec, OldEigenVec, TransfIndex, FirstEnergy )
    !
    EigenVectors = EigenVec   
    deallocate( EigenVec )
    !
    do i = 1, NOpenChannels
       phi = 0.d0
       RealVal = dble(sqrt(EigenValues(i)))
       ImagVal = aimag(sqrt(EigenValues(i)))
       if ( abs(RealVal) >= abs(ImagVal) ) then
          phi = atan( ImagVal/RealVal )
          if ( RealVal < 0.d0 ) phi = phi + PI
       else
          phi = 0.5d0*PI - atan( RealVal/ImagVal )
          if ( ImagVal < 0.d0 ) phi = phi + PI
       end if
       !
       if ( phi < 0.d0 ) phi = phi + 2.d0*PI
       !
       if ( .not. FirstEnergy ) then
          OldIndex = FindIntegerInVec( i, TransfIndex )
          w1 = OldPhases%Element(OldIndex,1) - PI*int(OldPhases%Element(OldIndex,1)/PI)
          if ( abs(w1-phi) > PI/2.d0 ) then
             if ( phi > w1 ) then
                phi = phi - PI
             else
                phi = phi + PI
             end if
          end if
          phi = phi + PI*int(OldPhases%Element(OldIndex,1)/PI)
       end if
       !
       NewIndex(i) = FindIntegerInVec( i, TransfIndex )
       call EigenPhases%SetElement( NewIndex(i), 1, phi )
       !
    end do
    !
    OldPhases = EigenPhases
    !
  end subroutine ComputeEigenPhases




  !Check eigenvalues have module 1.
  subroutine CheckMatSEigenvalues( EigenValues )
    complex(kind(1d0)), intent(in) :: EigenValues(:)
    integer :: i
    real(kind(1d0)) :: Val
    !
    do i = 1, size(EigenValues)
       Val = EigenValues(i)*conjg(EigenValues(i))
       if ( abs(Val - 1.d0 ) > DEFINED_TOLERANCE ) then
          write(output_unit,*) "S matrix eigenvalue: ", i, EigenValues(i)
          call Assert( "The SMat eigenvalue number "//AlphabeticNumber(i)//&
               " is not unitary." )
       end if
    end do
    !
  end subroutine CheckMatSEigenvalues
  


  !> Permutes the S scattering matrix eigenvectors to enforce continuity with energy.
  subroutine EigenVectorsOrdering( InEigVec, OutEigVec, TransfIndex, FirstEnergy )
    !
    complex(kind(1d0)),               intent(in)    :: InEigVec(:,:)
    complex(kind(1d0)),               intent(inout) :: OutEigVec(:,:)
    !> Vector of integers containing the new order of eigenvectors
    !! according to the continuity condition. In the slot n, now
    !! is the number TransfIndex(n), what means that the eigenvector
    !! n for energy E corresponds to the eigenvector TransfIndex(n) for
    !! energy E+dE.
    integer, allocatable,             intent(inout) :: TransfIndex(:)
    logical             ,             intent(in)    :: FirstEnergy
    !
    integer :: i, j, NumEigVec, Index, n, IndexJ, IndexI, Counter
    real(kind(1d0)) :: Val1, Val2, ValFinal
    complex(kind(1d0)), allocatable :: AuxMat(:,:), OldAuxMat(:,:), AuxOutMatTC2(:,:), AuxOutMatTC(:,:)
    integer, allocatable :: OldTransfIndex(:), ArrayJ(:), ArrayI(:)
    type(ClassComplexMatrix) :: OutMat, OutMatTC
    !
    NumEigVec = size(InEigVec,2)
    !
    if ( allocated(TransfIndex) ) deallocate(TransfIndex)
    allocate( TransfIndex(NumEigVec) )
    ! What the hell?
    !TransfIndex = [1:NumEigVec]
    do n=1,NumEigVec
        TransfIndex(n)=n
    enddo
    !
    if ( FirstEnergy ) then
       OutEigVec(:,:) = InEigVec(:,:)
       return
    end if
    !
    allocate( OldTransfIndex(NumEigVec) )
    OldTransfIndex = TransfIndex
    !
    allocate( AuxMat(NumEigVec,NumEigVec) )
    AuxMat = InEigVec
    allocate( OldAuxMat(NumEigVec,NumEigVec) )
    OldAuxMat = AuxMat
    !
    OutMat = OutEigVec
    call OutMat%TransposeConjugate( OutMatTC )
    call OutMatTC%FetchMatrix( AuxOutMatTC2 )
    allocate( AuxOutMatTC(NumEigVec,NumEigVec) )
    AuxOutMatTC = Z0
    AuxOutMatTC(1:size(AuxOutMatTC2,1),1:size(AuxOutMatTC2,2)) = AuxOutMatTC2(:,:)
    call OutMat%Free()
    call OutMatTC%Free()
    deallocate( AuxOutMatTC2 )
    !
    !
    allocate( ArrayJ(NumEigVec) )
    ArrayJ = 0
    allocate( ArrayI(NumEigVec) )
    ArrayI = 0
    !
    TransfIndex = 0
    AuxMat = Z0
    !
    Counter = 0
    !
    do n = 1, NumEigVec
       !
       ValFinal = 0.d0
       !
       Index = 0
       do j = 1, NumEigVec
          !
          Val1 = 0.d0
          do i = 1, NumEigVec
             !
             if ( EigenPhaseStillAvailable(ArrayJ,ArrayI,j,i) ) then
                !
                Val2 = abs(dot_product(OldAuxMat(:,i),AuxOutMatTC(j,:)))
!!$                   write(*,*) "n,i,j,Val2", n, i, j, Val2
                if ( ((Val2-Val1) > 0.d0)  ) then
                   Val1 = Val2
                   Index = i
                end if
                !
             end if
             !
          end do
!!$             write(*,*) "n, Index, j,Val1", n, Index, j, Val1
          !
          if ( Val1 > ValFinal ) then
             ValFinal = Val1
             IndexJ = j
             IndexI = Index
          end if
          !
       end do
       !
!!$          write(*,*) "n,IndexI,IndexJ,ValFinal", n, IndexI, IndexJ, ValFinal
       !
       if ( Index == 0 ) then
          write(*,*) IndexJ, IndexI
          write(*,*) "ArrayJ", ArrayJ
          write(*,*) "ArrayI", ArrayI
          call Assert( "Index cannot be zero" )!Index = j
       end if
       if ( ValFinal < epsilon(1d0) ) then
          write(*,*) Val1, IndexJ, IndexI
          call Assert( "ValFinal cannot be zero" )
       else
!!$             write(*,*) ValFinal, IndexJ, IndexI
       end if
       !
       AuxMat(:,IndexJ) = OldAuxMat(:,IndexI)
!!$          AuxMat(:,IndexI) = OldAuxMat(:,IndexJ)
!!$          OldAuxMat = AuxMat
       !
!!$          write(*,*) "position, to: ", IndexJ, IndexI
       TransfIndex(IndexJ) = OldTransfIndex(IndexI)
!!$          OldTransfIndex = TransfIndex
       !
       Counter = Counter + 1
       ArrayJ(Counter) = IndexJ
       ArrayI(Counter) = IndexI
       !
    end do
    !
    !
    OutEigVec = AuxMat
    deallocate( AuxMat,  OldAuxMat, AuxOutMatTC )
    !
  end subroutine EigenVectorsOrdering



  logical function EigenPhaseStillAvailable(ArrayJ,ArrayI,j,i) result( Avail )
    !
    integer, intent(in) :: ArrayJ(:)
    integer, intent(in) :: ArrayI(:)
    integer, intent(in) :: j
    integer, intent(in) :: i
    !
    integer :: n
    !
    Avail = .true.
    !
    do n = 1, size(ArrayJ)
       if( ArrayJ(n) == j ) then
          Avail = .false.
          return
       end if
       if( ArrayI(n) == i ) then
          Avail = .false.
          return
       end if
    end do
    !
  end function EigenPhaseStillAvailable
  
  
  
  
  integer function FindIntegerInVec( Number, Vector ) result( Pos )
    !
    integer, intent(in) :: Number
    integer, intent(in) :: Vector(:)
    !
    integer :: i
    !
    Pos = 0
    !
    do i = 1, size(Vector)
       if ( Number == Vector(i) ) then
          Pos = i
          return
       end if
    end do
    !
    if ( Pos == 0 ) then
       write(*,*) "Number", Number
       write(*,*) "Vector", Vector
       call Assert( "The number does not exist in the vector." )
    end if
    !
  end function FindIntegerInVec



  logical function HaveInfElement( Array )
    !
    real(kind(1d0)), intent(in) :: Array(:,:)
    integer :: i, j
    real(kind(1d0)), parameter :: Threshold = 1.d100
    !
    HaveInfElement = .false.
    do j = 1, size(Array,2)
       do i = 1, size(Array,1)
          if ( abs(Array(i,j)) > Threshold ) then
             HaveInfElement = .true.
             write(output_unit,'(a,2i0,f25.16)') "Element", i, j, Array(i,j)
             return
          end if
       end do
    end do
    !
  end function HaveInfElement



  function GetDipoleEnergyLabel( TransitionLabel, BraEnergy, KetEnergy ) result(Label)
    character(len=*), intent(in) :: TransitionLabel
    real(kind(1d0)) , intent(in) :: BraEnergy
    real(kind(1d0)) , intent(in) :: KetEnergy
    character(len=:), allocatable :: Label
    character(len=64) :: BraEnergyStrn, KetEnergyStrn
    character(len=*), parameter :: BraName = 'BE'
    character(len=*), parameter :: KetName = 'KE'
    !
    if ( TransitionLabel .is. BoxBoxTransitionLabel ) then
       allocate( Label, source = '' )
    elseif ( TransitionLabel .is. BoxConTransitionLabel ) then
       write(KetEnergyStrn,*) KetEnergy
       allocate( Label, source = KetName//trim(adjustl(KetEnergyStrn)) )
    elseif ( TransitionLabel .is. ConBoxTransitionLabel ) then
       write(BraEnergyStrn,*) BraEnergy
       allocate( Label, source = BraName//trim(adjustl(BraEnergyStrn)) )
    elseif ( TransitionLabel .is. ConConTransitionLabel ) then
       write(BraEnergyStrn,*) BraEnergy
       write(KetEnergyStrn,*) KetEnergy
       allocate( Label, source = &
            BraName//trim(adjustl(BraEnergyStrn))//&
            KetName//trim(adjustl(KetEnergyStrn)) )
    end if
    !
  end function GetDipoleEnergyLabel


  function GetFullDipoleDir( DipoleStorageDir, TransitionLabel ) result(Dir)
    character(len=*), intent(in) :: DipoleStorageDir
    character(len=*), intent(in) :: TransitionLabel
    character(len=:), allocatable :: Dir
    allocate( Dir, source = &
         AddSlash(DipoleStorageDir)//&
         AddSlash(DipoleTransitionDir)//&
         AddSlash(TransitionLabel) )
  end function GetFullDipoleDir


  function GetDipoleFileName( &
       TransitionLabel, DipOperatorLabel, &
       DipoleAxis, CompMethod, ConditionLabel, &
       ScattConditionNumber, BraEnergy, KetEnergy, &
       BoxStateIndex, OnlyQC, OnlyLoc, OnlyPoly, UseSinStates ) result(FileName)
    character(len=*)             , intent(in)    :: TransitionLabel
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: BraEnergy
    real(kind(1d0))              , intent(in)    :: KetEnergy
    integer                      , intent(in)    :: BoxStateIndex
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    !
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: EnergyLabel, ConditioningLabel, IntFileName
    !
    allocate( EnergyLabel, source = GetDipoleEnergyLabel(TransitionLabel,BraEnergy,KetEnergy) )
    !
    allocate( ConditioningLabel, source = GetScattConditioningLabel(CompMethod,ConditionLabel,ScattConditionNumber,UseSinStates) )
    !
    !
    if ( TransitionLabel .isnt. ConConTransitionLabel ) then
       allocate( IntFileName, source = &
            GetFullDipoleOperatorLabel(DipOperatorLabel,DipoleAxis)//&
            BoxIndexLabel//AlphabeticNumber(BoxStateIndex)//&
            EnergyLabel//ConditioningLabel//CompMethod )
       if ( OnlyQC ) then
          allocate( Filename, source = IntFileName//QCLabel )
       elseif( OnlyLoc ) then
          allocate( Filename, source = IntFileName//LocLabel )
       elseif( OnlyPoly ) then
          allocate( Filename, source = IntFileName//PolyLabel )
       else
          allocate( Filename, source = IntFileName )
       end if
    else
       allocate( FileName, source = &
            GetFullDipoleOperatorLabel(DipOperatorLabel,DipoleAxis)//&
            EnergyLabel//ConditioningLabel//CompMethod )
    end if
    !
  end function GetDipoleFileName


  subroutine ReadDipoleTransition( &
       TransitionLabel           , &
       DipoleStorageDir          , &
       DipOperatorLabel          , &
       DipoleAxis                , &
       CompMethod                , &
       ConditionLabel            , &
       ScattConditionNumber      , &
       BraEnergy                 , &
       KetEnergy                 , &
       BoxStateIndex             , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       UseSinStates              , &
       TransMat                  )
    !
    character(len=*)             , intent(in)    :: TransitionLabel
    character(len=*)             , intent(in)    :: DipoleStorageDir
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: BraEnergy
    real(kind(1d0))              , intent(in)    :: KetEnergy
    integer                      , intent(in)    :: BoxStateIndex
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    class(ClassComplexMatrix)    , intent(out)   :: TransMat
    !
    character(len=:), allocatable :: FullDipoleDir, FileName, FullFileName
    integer :: uid
    type(ClassMatrix) :: BraEnMat, KetEnMat
    !
    allocate( FullDipoleDir, source = GetFullDipoleDir(DipoleStorageDir,TransitionLabel) ) 
    !
    allocate( FileName, source = &
         GetDipoleFileName(    &
         TransitionLabel     , &
         DipOperatorLabel    , &
         DipoleAxis          , &
         CompMethod          , &
         ConditionLabel      , &
         ScattConditionNumber, &
         BraEnergy           , &
         KetEnergy           , &
         BoxStateIndex       , &
         OnlyQC, OnlyLoc, OnlyPoly, UseSinStates )  )
    !
    allocate( FullFileName, source = FullDipoleDir//FileName )
    !
    call OpenFile( FullFileName, uid, 'read', 'formatted' )
    call BraEnMat%Read( uid )
    call KetEnMat%Read( uid )
    call TransMat%Read( uid )
    close( uid )
    !
  end subroutine ReadDipoleTransition



  subroutine SaveDipoleTransition( &
       TransitionLabel           , &
       DipoleStorageDir          , &
       DipOperatorLabel          , &
       DipoleAxis                , &
       CompMethod                , &
       ConditionLabel            , &
       ScattConditionNumber      , &
       BraEnergies               , &
       KetEnergies               , &
       BoxStateIndex             , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       UseSinStates              , &
       TransMat                  )
    !
    character(len=*)             , intent(in)    :: TransitionLabel
    character(len=*)             , intent(in)    :: DipoleStorageDir
    character(len=*)             , intent(in)    :: DipOperatorLabel
    character(len=*)             , intent(in)    :: DipoleAxis
    character(len=*)             , intent(in)    :: CompMethod
    character(len=:), allocatable, intent(inout) :: ConditionLabel
    real(kind(1d0))              , intent(in)    :: ScattConditionNumber
    real(kind(1d0))              , intent(in)    :: BraEnergies(:)
    real(kind(1d0))              , intent(in)    :: KetEnergies(:)
    integer                      , intent(in)    :: BoxStateIndex
    logical                      , intent(in)    :: OnlyQC
    logical                      , intent(in)    :: OnlyLoc
    logical                      , intent(in)    :: OnlyPoly
    logical                      , intent(in)    :: UseSinStates
    class(ClassComplexMatrix)    , intent(in)    :: TransMat
    !
    character(len=:), allocatable :: FullDipoleDir, FileName, FullFileName
    integer :: uid, NBra, NKet
    type(ClassMatrix) :: BraEnMat, KetEnMat
    real(kind(1d0)), allocatable :: BraEnArray(:,:), KetEnArray(:,:)
    !
    allocate( FullDipoleDir, source = GetFullDipoleDir(DipoleStorageDir,TransitionLabel) ) 
    call execute_command_line("mkdir -p "//FullDipoleDir )
    !
    !..The energies in the file name are only present for
    ! the scattering states, and all the energies are the same
    ! in the array.
    allocate( FileName, source = &
         GetDipoleFileName(    &
         TransitionLabel     , &
         DipOperatorLabel    , &
         DipoleAxis          , &
         CompMethod          , &
         ConditionLabel      , &
         ScattConditionNumber, &
         BraEnergies(1)      , &
         KetEnergies(1)      , &
         BoxStateIndex       , &
         OnlyQC, OnlyLoc, OnlyPoly, UseSinStates ) )
    !
    allocate( FullFileName, source = FullDipoleDir//FileName )
    !
    NBra = size(BraEnergies)
    allocate( BraEnArray(NBra,1) )
    BraEnArray(:,1) = BraEnergies(:)
    BraEnMat = BraEnArray
    deallocate( BraEnArray )
    !
    NKet = size(KetEnergies)
    allocate( KetEnArray(NKet,1) )
    KetEnArray(:,1) = KetEnergies(:)
    KetEnMat = KetEnArray
    deallocate( KetEnArray )
    !
    call OpenFile( FullFileName, uid, 'write', 'formatted' )
    call BraEnMat%Write( uid )
    call KetEnMat%Write( uid )
    call TransMat%Write( uid )
    close( uid )
    !
  end subroutine SaveDipoleTransition


  subroutine CheckTransitionLabel( TransitionLabel )
    character(len=*), intent(in) :: TransitionLabel
    if ( (TransitionLabel .isnt. BoxBoxTransitionLabel) .and. &
         (TransitionLabel .isnt. BoxConTransitionLabel) .and. &
         (TransitionLabel .isnt. ConBoxTransitionLabel) .and. &
         (TransitionLabel .isnt. ConConTransitionLabel) ) &
         call Assert( 'Invalid transition kind, it must be either: '//&
         BoxBoxTransitionLabel//', '//&
         BoxConTransitionLabel//', '//&
         ConBoxTransitionLabel//' or '//&
         ConConTransitionLabel )
  end subroutine CheckTransitionLabel


  subroutine CheckScatteringInput( TransitionLabel, CompMethod )
    character(len=*)             , intent(in)    :: TransitionLabel
    character(len=:), allocatable, intent(inout) :: CompMethod
    character(len=*), parameter :: IntroLine = &
         'To compute the transitions involving continuum states, '
    character(len=*), parameter :: FinalLine = 'must be set.'
    if ( (TransitionLabel .is. BoxConTransitionLabel) .or. &
         (TransitionLabel .is. ConBoxTransitionLabel) .or. &
         (TransitionLabel .is. ConConTransitionLabel) ) then
       if ( .not.allocated(CompMethod) ) call Assert( &
            IntroLine//'the scattering computation method: "-cm", '//FinalLine )
    end if
  end subroutine CheckScatteringInput


  !> Removes the rows and columns linear dependencies of a matrix
  !! given a conditioning matrix: 
  !!\f$\tilde{\mathbf{O}}=\mathbf{P_{con}^{T}}\mathbf{O}\mathbf{P_{con}}\f$
  subroutine ApplyQCConditioner( InOutMat, QCFullConditioner )
    !> Matrix \f$\mathbf{O}\f$ to be conditioned in the input,
    !! giving \f$\tilde{\mathbf{O}}\f$ as an output.
    class(ClassMatrix), intent(inout) :: InOutMat
    !> Conditioning matrix \f$\mathbf{P_{con}}\f$
    class(ClassMatrix), intent(in)    :: QCFullConditioner
    call InOutMat%Multiply( QCFullConditioner, 'Right', 'N' )
    call InOutMat%Multiply( QCFullConditioner, 'Left' , 'T' )
  end subroutine ApplyQCConditioner



  !> Computes the conditioning matrix \f$\mathbf{P_{con}}\f$ to
  !! be use to remove the linear dependencies in an operator matrix.
  subroutine ComputeQCConditioner( OvMat, NumFunQCBra, ConditionNumber, ConditionerFullMat )
    !> Overlpa matrix \f$\mathbf{S}\f$.
    class(ClassMatrix), intent(in)  :: OvMat
    !> Number of rows and columns that defines the submatrix to be
    !! conditioned (1:NumFunQCBra)x(1:NumFunQCBra).
    integer           , intent(in)  :: NumFunQCBra
    !> Threshold for removing linear dependencies.
    real(kind(1d0))   , intent(in)  :: ConditionNumber
    !> Conditioning matrix \f$\mathbf{P_{con}}\f$.
    class(ClassMatrix), intent(out) :: ConditionerFullMat
    !
    type(ClassMatrix) :: OvMatQC, QCConditionerMat
    !
    call OvMat%GetSubMatrix( &
         1, NumFunQCBra    , &
         1, NumFunQCBra    , &
         OvMatQC           )
    call OvMatQC%ConditionMatrix( ConditionNumber, QCConditionerMat )
    call OvMatQC%Free()
    call GetFullConditionerMat( OvMat%NRows(), QCConditionerMat, ConditionerFullMat )
    call QCConditionerMat%Free()
    !
  end subroutine ComputeQCConditioner



  !>Gets the full conditionin matrix \f$\mathbf{P_{con}}\f$, not
  !! only the part that removes the operator matrices linear 
  !! dependencies but also the part that keep unchanged the 
  !! submatrices that don't need to be transformed.
  subroutine GetFullConditionerMat( TotNumRows, ConditionerTMat, ConditionerFullMat )
    !> Total matrix dimension.
    integer,                    intent(in)    :: TotNumRows
    !> \f$\mathbf{P_{con}}\f$ submatrix that removes de linear
    !! dependencies.
    class(ClassMatrix),         intent(inout) :: ConditionerTMat
    !> Full \f$\mathbf{P_{con}}\f$ matrix.
    class(ClassMatrix),         intent(out)   :: ConditionerFullMat
    !
    real(kind(1d0)), allocatable :: Array(:,:), PrecArray(:,:)
    integer :: OldNumRows, OldNumCols, NewNumRows, NewNumCols, i, uidQC
    !
    OldNumRows = TotNumRows
    OldNumCols = OldNumRows
    !
    NewNumCols = OldNumCols - (ConditionerTMat%NRows()-ConditionerTMat%NColumns())
    allocate( Array(OldNumRows,NewNumCols) )
    Array = 0.d0
    !
    call ConditionerTMat%FetchMatrix( PrecArray )
    !
    Array(1:ConditionerTMat%NRows(),1:ConditionerTMat%NColumns()) = PrecArray(:,:)
    !
    do i = ConditionerTMat%NRows()+1, OldNumRows
       Array(i,i-(ConditionerTMat%NRows()-ConditionerTMat%NColumns())) = 1.d0
    end do
    !
    ConditionerFullMat = Array
    !
  end subroutine GetFullConditionerMat



  !> Reads the transformation matrix to be applied to the
  !!computes scattering states in order to pass from the channels
  !!in one symetry representation, like D2h for instance, to a new
  !! one like spherical symmetry in atoms.
  subroutine ReadTransformationMatrix( FileName, TransfMat )
    !> File in which the transformation matrix is stored.
    character(len=:), allocatable, intent(in) :: FileName
    !> Transformation matrix.
    class(ClassComplexMatrix), intent(inout)  :: TransfMat
    !
    integer :: uid, iostat, NumRow, NumCol, i, j
    character(len=:), allocatable :: Line
    real(kind(1d0)), allocatable :: Rpart(:,:), Ipart(:,:)
    !
    if ( .not.allocated( FileName ) ) return
    !
    call OpenFile( FileName, uid, 'read', 'formatted' )
    !
    call FetchLine( uid, Line )
    !.. The first uncommented line has the matrix dimension
    read(Line,*) NumRow, NumCol
    call TransfMat%InitFull( NumRow, NumCol )
    allocate( Rpart(NumRow,NumCol) )
    Rpart = 0.d0
    allocate( Ipart(NumRow,NumCol) )
    Ipart = 0.d0
    !
    do j = 1, NumCol
       read(uid,*) ( Rpart(i,j), Ipart(i,j), i=1, NumRow) 
    end do
    !
    close( uid )
    !
    do j = 1, NumCol
       do i = 1, NumRow
          call TransfMat%SetElement( i, j , Z1*Rpart(i,j)+Zi*Ipart(i,j) )
       end do
    end do
    !
  end subroutine ReadTransformationMatrix



  !> Transform the scattering states in some symmetry representation
  !! to other, given by the transformation matrix TransfMat.
  subroutine TransformScatteringStates( ScattSpecRes,TransfMat )
    !> Scattering states in the old representation to be overwrite
    !! by the new one.
    class(ClassComplexSpectralResolution), intent(inout) :: ScattSpecRes
    !> Transformation matrix.
    class(ClassComplexMatrix)            , intent(in)    :: TransfMat
    !
    complex(kind(1d0)), allocatable :: EnergyVec(:)
    type(ClassComplexMatrix) :: ScattState
    !
    if ( .not.TransfMat%IsInitialized() ) return
    !

    call ScattSpecRes%Fetch( EnergyVec )
    call ScattSpecRes%Fetch( ScattState )

    call ScattSpecRes%Free()
    !
    call ScattState%Multiply( TransfMat, 'Right', 'N' )
    call ScattSpecRes%Init( ScattState%NRows(), ScattState%NColumns() )
    call ScattSpecRes%SetEigenvalues( EnergyVec(:ScattState%NColumns()) )
    call ScattSpecRes%SetEigenVectors( ScattState )
    !
  end subroutine TransformScatteringStates


  
  !----------------------------------------
  ! ClassScatteringStates Methods
  !----------------------------------------


  subroutine ClassScatteringStatesInit( Self, &
       Basis, SymSpace, SymStorageDir, NumBsNotOverlap, &
       CompMethod, Energy, ScattConditionNumber, ConditionLabel )
    class(ClassScatteringStates)                , intent(inout) :: Self
    class(ClassBasis)                   , target, intent(in)    :: Basis
    class(ClassSymmetricElectronicSpace), target, intent(in)    :: SymSpace
    character(len=*)                            , intent(in)    :: SymStorageDir
    integer                                     , intent(in)    :: NumBsNotOverlap
    character(len=*)                            , intent(in)    :: CompMethod
    real(kind(1d0))                             , intent(in)    :: Energy
    real(kind(1d0))                             , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable               , intent(inout) :: ConditionLabel
    !
    if ( associated(Self%Basis) ) Self%Basis => NULL()
    allocate( Self%Basis, source = Basis )
    !
    if ( associated(Self%SymSpace) ) Self%SymSpace => NULL()
    allocate( Self%SymSpace, source = SymSpace )
    !
    if ( allocated(Self%SymStorageDir) ) deallocate( Self%SymStorageDir )
    allocate( Self%SymStorageDir, source = SymStorageDir )
    !
    if ( allocated(Self%CompMethod) ) deallocate( Self%CompMethod )
    allocate( Self%CompMethod, source = CompMethod )
    !
    if ( allocated(ConditionLabel) ) then
       if ( allocated(Self%ConditionLabel) ) deallocate( Self%ConditionLabel )
       allocate( Self%ConditionLabel, source = ConditionLabel )
    end if
    !
    Self%Energy = Energy
    Self%ScattConditionNumber = ScattConditionNumber
    Self%NumBsNotOverlap = NumBsNotOverlap
    !
  end subroutine ClassScatteringStatesInit



  subroutine ClassScatteringStatesFree( Self )
    class(ClassScatteringStates), intent(inout) :: Self
    if ( associated(Self%Basis) ) Self%Basis => NULL()
    if ( associated(Self%SymSpace) ) Self%SymSpace => NULL()
    if ( allocated(Self%SymStorageDir) ) deallocate( Self%SymStorageDir )
    if ( allocated(Self%CompMethod) ) deallocate( Self%CompMethod )
    if ( allocated(Self%ConditionLabel) ) deallocate( Self%ConditionLabel )
    if ( allocated(Self%PWCVec) ) deallocate( Self%PWCVec )
    !
    Self%Energy = -huge(1d0)
    Self%ScattConditionNumber = -huge(1d0)
    Self%NumBsNotOverlap = 0
    call Self%ScattState%Free()
    call Self%IncomingScattState%Free()
    call Self%SinScattState%Free()
    call Self%EigenPhase%Free()
    !
  end subroutine ClassScatteringStatesFree




  subroutine ClassScatteringStatesComputeSPEC( Self, ScattMat, HamSpecRes )
    class(ClassScatteringStates)  , intent(inout) :: Self
    class(ClassMatrix)            , intent(in)    :: ScattMat
    class(ClassSpectralResolution), intent(in)    :: HamSpecRes
    !
    real(kind(1d0)), allocatable :: EigenEnergies(:)
    type(ClassMatrix) :: EigenStates, MatLastBs, EnergMatInv, SolMat
    complex(kind(1d0)), allocatable :: SolArray(:,:)
    !
    call HamSpecRes%Fetch( EigenEnergies )
    call HamSpecRes%Fetch( EigenStates )
    !
    call ScattMat%GetSubMatrix(                     &
         1, ScattMat%NRows()                      , &
         ScattMat%NRows() + 1, ScattMat%NColumns(), &
         MatLastBs               )
    !
    call BuildInvEnergMat( EigenEnergies, Self%Energy, EnergMatInv )
    call EnergMatInv%Multiply( EigenStates, 'Left' , 'N' )
    call EnergMatInv%Multiply( EigenStates, 'Right', 'T' )
    call EnergMatInv%Multiply( MatLastBs  , 'Right', 'N' )
    !
    call SetSolution( Self%Energy, EnergMatInv, Self%ScattState )
    call EnergMatInv%Free()
    !
    call Self%ScattState%Fetch( SolArray )
    SolMat = dble(SolArray(:,:))
    deallocate( SolArray )
    call CheckSolution( ScattMat, SolMat )
    !
  end subroutine ClassScatteringStatesComputeSPEC




  subroutine ClassScatteringStatesComputeHSLE( Self, ScattMat, ConditionerQC )
    class(ClassScatteringStates)  , intent(inout) :: Self
    class(ClassMatrix)            , intent(in)    :: ScattMat
    class(ClassMatrix), optional  , intent(in)    :: ConditionerQC
    !
    real(kind(1d0)), allocatable :: Solution(:,:), HArray(:,:)
    type(ClassMatrix) :: CopyScattMat, MatLastBsSolMat, SolMat
    logical :: SolMatIsSolution = .true.
    integer :: NumLastBs, NRows, info
    integer, allocatable :: ipiv(:)
    !
    NumLastBs = ScattMat%NColumns() - ScattMat%NRows()
    CopyScattMat = ScattMat
    call CopyScattMat%AddRows( NumLastBs, "END" )
    NRows = CopyScattMat%NRows()
    allocate( ipiv(NRows) )
    !
    call CopyScattMat%FetchMatrix( HArray )
    call dgetrf( NRows, NRows, HArray, NRows, ipiv, info )
!!$       write(output_unit,'(a,i)') "info", info
    call ComputeBackSustSol( NumLastBs, HArray, Solution )
    deallocate( HArray )
    SolMat = Solution
    deallocate(Solution)
    !
    call CheckSolution( CopyScattMat, SolMat )
    !
    if ( present(ConditionerQC) ) call SolMat%Multiply( ConditionerQC, 'Left', 'N' )
    !
    call SetSolution( Self%Energy, SolMat, Self%ScattState, SolMatIsSolution )
    !
  end subroutine ClassScatteringStatesComputeHSLE





  subroutine ClassScatteringStatesComputeINVM( Self, ScattMat, ConditionerQC )
    class(ClassScatteringStates)  , intent(inout) :: Self
    class(ClassMatrix)            , intent(in)    :: ScattMat
    class(ClassMatrix), optional  , intent(in)    :: ConditionerQC
    !
    real(kind(1d0)), allocatable :: Solution(:,:), HArray(:,:)
    type(ClassMatrix) :: MatLastBs, SolMat, HermSubMat, InvHermSubMat
    complex(kind(1d0)), allocatable :: SolArray(:,:) 
    integer :: NumLastBs, NRows, NCols
    logical :: SolMatIsSolution = .true.
    !
    NumLastBs = ScattMat%NColumns() - ScattMat%NRows()
    NRows = ScattMat%NRows()
    NCols = ScattMat%NColumns()
    !
    call ScattMat%GetSubMatrix( 1, NRows, 1, NRows, HermSubMat )
    call HermSubMat%Inverse( InvHermSubMat )
    call CheckInverseMat( HermSubMat, InvHermSubMat, DEFINED_TOLERANCE )
    call HermSubMat%Free()
    !
    call ScattMat%GetSubMatrix( 1, NRows, NRows + 1, NCols, MatLastBs )
    call InvHermSubMat%Multiply( MatLastBs, 'Right', 'N' )
    !
    call SetSolution( Self%Energy, InvHermSubMat, Self%ScattState )
    !
    call Self%ScattState%Fetch( SolArray )
    SolMat = dble(SolArray(:,:))
    deallocate( SolArray )
    call CheckSolution( ScattMat, SolMat )
    !
    if ( present(ConditionerQC) ) then
       call SolMat%Multiply( ConditionerQC, 'Left', 'N' )
       call SetSolution( Self%Energy, SolMat, Self%ScattState, SolMatIsSolution )
    end if
    !
  end subroutine ClassScatteringStatesComputeINVM




  subroutine ClassScatteringStatesNormalizeScattStates( Self, FirstEnergy, ConditionQC )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: FirstEnergy
    logical                       , intent(in)    :: ConditionQC
    !
    type(ClassComplexMatrix) :: ScattStatesMat, MatS, NormalizedScattStatesMat
    real(kind(1d0)) :: Energy, PIEnergy
    integer :: i, j, n, ColIndex
    integer :: NChannels, NumRadFunc, Index, NumCCFun, NumOpenChannels
    !.. 2-D array where the rows represent the channel function and
    !! the columns the vector arising from the scattering states
    !! calculation. Each element represent a vector
    ! of coefficients corresponding to the monoelectronic radial
    !! basis, being non-zero only for the well defined B-splines 
    !! (those not overlapping with the diffuse Gaussian).
    type(ClassComplexMatrix), allocatable :: CoeffMat(:,:)
    complex(kind(1d0)) :: Val
    type(ClassMatrix) :: FunValBorder, FunDerValBorder, MatA, MatB
    complex(kind(1d0)), allocatable :: EnergyVec(:)
    integer, allocatable :: IndexOpenChannels(:)
    type(ClassComplexMatrix) :: MatSEigVec
    !
    call Self%ScattState%Fetch( ScattStatesMat )
    call Self%ScattState%Fetch( EnergyVec )
    !
    NChannels = Self%ScattState%NEval()
    NumOpenChannels = GetNumOpenChannels( Self%SymSpace, Self%Energy, NChannels )
    write(*,*) 'The Number of open channels is ',NumOpenChannels
    !
    if ( NumOpenChannels == 0 ) then
       ScattStatesMat = Z0
       call Self%ScattState%SetEigenVectors( ScattStatesMat )
       call SetScatteringStates( Self%SinScattState, EnergyVec, ScattStatesMat )
       call SetScatteringStates( Self%IncomingScattState, EnergyVec, ScattStatesMat )
       return
    end if
    !
    call GetOpenChannelsIndex( Self%SymSpace, Self%Energy, NChannels, IndexOpenChannels )
    allocate( Self%PWCVec(NumOpenChannels) )
    do j = 1, NumOpenChannels
       ColIndex = IndexOpenChannels(j)
       Self%PWCVec(j) = Self%SymSpace%GetChannelPWC(ColIndex) 
    end do
    !
    NumCCFun  = Self%ScattState%Size()
    allocate( CoeffMat(NChannels,NChannels) )
    NumRadFunc  = Self%Basis%GetNFun()
    !
    do j = 1, NChannels
       do i = 1, NChannels
          call CoeffMat(i,j)%InitFull(NumRadFunc,1)
          Index = NumCCFun - NChannels*Self%NumBsNotOverlap + i
          do n = NumRadFunc - Self%NumBsNotOverlap + 1, NumRadFunc
             Val = ScattStatesMat%Element(Index,j)
             call CoeffMat(i,j)%SetElement( n, 1, Val )
             Index = Index + NChannels
          end do
          PIEnergy = Self%SymSpace%GetChannelPIEnergy(i)
          if ( (Self%Energy-PIEnergy) < 0.d0 ) call CoeffMat(i,j)%SetElement( NumRadFunc, 1, Z0 )
       end do
    end do
    !
    !
    call ComputeCoeffForMatS( Self%Basis, Self%SymSpace, CoeffMat, Self%Energy, MatA, MatB )
    deallocate( CoeffMat )
    !
    call ComputeS( MatA, MatB, MatS )
    call ComputeEigenPhases( MatS, Self%EigenPhase, MatSEigVec, FirstEnergy )
    !
    !..Normalizes the scattering states with the asymptotic condition ~\f$\sqrt{\frac{2}{\pi k}}\frac{\sin(kr+\frac{Z}{k}\ln(2kr)-\frac{\ell\pi}{2}+\sigma_{k,\ell}+\delta_{k,\ell})}{r} \f$.
    NormalizedScattStatesMat = ScattStatesMat
    call ComputeSinNormalizedStates( Self%SymSpace, Self%Energy, MatA, MatB, NormalizedScattStatesMat )
    call SetScatteringStates( Self%SinScattState, EnergyVec, NormalizedScattStatesMat )
    call NormalizedScattStatesMat%Free()
    !
    !..Normalizes the scattering states according to incoming boundary conditions.
    NormalizedScattStatesMat = ScattStatesMat
    call ComputeIncomingNormalizedStates( Self%SymSpace, Self%Energy, MatA, MatB, NormalizedScattStatesMat )
    call SetScatteringStates( Self%IncomingScattState, EnergyVec, NormalizedScattStatesMat )
    call NormalizedScattStatesMat%Free()
    !
    call ScattStatesMat%Free()
    !
  end subroutine ClassScatteringStatesNormalizeScattStates



  subroutine SetScatteringStates( NewScattState, EnergyVec, ScattStatesMat )
    !
    class(ClassComplexSpectralResolution), intent(inout) :: NewScattState
    complex(kind(1d0))                   , intent(in)    :: EnergyVec(:)
    class(ClassComplexMatrix)            , intent(in)    :: ScattStatesMat
    !
    call NewScattState%Free()
    call NewScattState%Init( ScattStatesMat%NRows(), ScattStatesMat%NColumns() )
    call NewScattState%SetEigenValues( EnergyVec(1:NewScattState%NEval()) )
    call NewScattState%SetEigenVectors( ScattStatesMat )
    !
  end subroutine SetScatteringStates



  subroutine ClassScatteringStatesSaveScatteringStates( Self, ConditionQC, FeshbachMethod )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: ConditionQC
    logical                       , intent(in)    :: FeshbachMethod                      
    !
    call SaveSinScatteringStates( Self, ConditionQC )
    call SaveIncomingScatteringStates( Self, ConditionQC, FeshbachMethod )
    call SaveScatteringChannels( Self, ConditionQC )
    !
  end subroutine ClassScatteringStatesSaveScatteringStates



  subroutine SaveIncomingScatteringStates( Self, ConditionQC, FeshbachMethod )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: ConditionQC
    logical                       , intent(in)    :: FeshbachMethod
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    logical :: UseFeshbach
    !
    UseFeshbach = .false.
    if ( FeshbachMethod ) UseFeshbach =.true.
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(Self%SymStorageDir,Self%CompMethod) )
    call execute_command_line("mkdir -p "//FullScattStatesDir )
    !
    if ( ConditionQC ) then
       if ( UseFeshbach ) then
          allocate( FileName, source = &
               GetIncomingScattStateFeshbachFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel  ,  &
               Self%ScattConditionNumber) )
       else
          allocate( FileName, source = &
               GetIncomingScattStateFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel  ,  &
               Self%ScattConditionNumber) )
       end if
    else
       if ( UseFeshbach ) then
          allocate( FileName, source = &
               GetIncomingScattStateFeshbachFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel) )
       else
          allocate( FileName, source = &
               GetIncomingScattStateFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel) )
       end if
    end if
    !
    call Self%IncomingScattState%Write( FileName )
    !
  end subroutine SaveIncomingScatteringStates


  subroutine SaveSinScatteringStates( Self, ConditionQC )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: ConditionQC
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    character(len=:), allocatable :: temp
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(Self%SymStorageDir,Self%CompMethod) )
    allocate(temp, source="mkdir -p "//TRIM(FullScattStatesDir))
    !call execute_command_line("mkdir -p "//FullScattStatesDir )
    call execute_command_line(temp)
    !
    if ( ConditionQC ) then
       allocate( FileName, source = &
            GetSinScattStateFileName(  &
            Self%SymStorageDir   ,  &
            Self%Energy          ,  &
            Self%CompMethod      ,  &
            Self%ConditionLabel  ,  &
            Self%ScattConditionNumber) )
    else
       allocate( FileName, source = &
            GetSinScattStateFileName(  &
            Self%SymStorageDir   ,  &
            Self%Energy          ,  &
            Self%CompMethod      ,  &
            Self%ConditionLabel) )
    end if
    !
    call Self%SinScattState%Write( FileName )
    !
  end subroutine SaveSinScatteringStates

  !subroutine test_system(cmd)
  !  character(len=*) :: cmd
  !  call system(cmd)
  !end subroutine

  subroutine SaveScatteringChannels( Self, ConditionQC )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: ConditionQC
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullScattStatesDir
    integer :: i, uid
    !
    allocate( FullScattStatesDir, source = GetScattStateDirName(Self%SymStorageDir,Self%CompMethod) )
    call execute_command_line("mkdir -p "//FullScattStatesDir )
    !
    if ( ConditionQC ) then
       allocate( FileName, source = &
            GetScatteringChannelsFileName(  &
            Self%SymStorageDir   ,  &
            Self%Energy          ,  &
            Self%CompMethod      ,  &
            Self%ConditionLabel  ,  &
            Self%ScattConditionNumber) )
    else
       allocate( FileName, source = &
            GetScatteringChannelsFileName(  &
            Self%SymStorageDir   ,  &
            Self%Energy          ,  &
            Self%CompMethod      ,  &
            Self%ConditionLabel) )
    end if
    !
    if ( .not.allocated(Self%PWCVec) ) call Assert( &
         'Impossible to save the scattering channels '//&
         'because are not allocated.' )
    !
    call OpenFile( FileName, uid, 'write', 'formatted' )
    write(uid,*) size(Self%PWCVec)
    !
    do i = 1, size(Self%PWCVec)
       call Self%PWCVec(i)%Save( uid )
    end do
    !
    close( uid )
    !
  end subroutine SaveScatteringChannels



  subroutine LoadScatteringChannels( FileName, ConditionQC, PWCVec )
    !
    character(len=*)                           , intent(in)  :: FileName
    logical                                    , intent(in)  :: ConditionQC
    class(ClassPartialWaveChannel), allocatable, intent(out) :: PWCVec(:)
    !
    integer :: i, uid, NumChannels
    !
    call OpenFile( FileName, uid, 'read', 'formatted' )
    read(uid,*) NumChannels
    !
    allocate( PWCVec(NumChannels) )
    !
    do i = 1, NumChannels
       call PWCVec(i)%Load( uid )
    end do
    !
    close( uid )
    !
  end subroutine LoadScatteringChannels



  subroutine ClassScatteringStatesSaveEigenPhases( Self, ConditionQC, FeshbachMethod )
    class(ClassScatteringStates)  , intent(inout) :: Self
    logical                       , intent(in)    :: ConditionQC
    logical                       , intent(in)    :: FeshbachMethod
    character(len=:), allocatable :: FileName
    character(len=:), allocatable :: FullEigPhaseDir
    integer :: j, uid, NOpenChannels
    logical :: UseFeshbach
    !
    UseFeshbach = .false.
    if( FeshbachMethod ) UseFeshbach = .true.
    !
    allocate( FullEigPhaseDir, source = GetEigenPhaseDirName(Self%SymStorageDir,Self%CompMethod) )
    call execute_command_line("mkdir -p "//FullEigPhaseDir )
    !
    if ( ConditionQC ) then
       if ( UseFeshbach ) then
          allocate( FileName, source = &
               GetEigenPhaseFeshbachFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel  ,  &
               Self%ScattConditionNumber) )
       else
          allocate( FileName, source = &
               GetEigenPhaseFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel  ,  &
               Self%ScattConditionNumber) )
       end if
    else
       if ( UseFeshbach ) then
          allocate( FileName, source = &
               GetEigenPhaseFeshbachFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel) )
       else
          allocate( FileName, source = &
               GetEigenPhaseFileName(  &
               Self%SymStorageDir   ,  &
               Self%Energy          ,  &
               Self%CompMethod      ,  &
               Self%ConditionLabel) )
       end if
    end if
    !
    call OpenFile( FileName, uid, 'write', 'formatted' )
    deallocate( FileName )
    !
    write(uid,"(f32.16)",ADVANCE="NO") Self%Energy
    !
    NOpenChannels =  Self%EigenPhase%NRows()
    write(uid,"(x,i0,x)",ADVANCE="NO") NOpenChannels
    !
    do j = 1, NOpenChannels
       if ( j == NOpenChannels ) then
          write(uid,"(f32.16)",ADVANCE="YES") Self%EigenPhase%Element(j,1) 
       else
          write(uid,"(f32.16)",ADVANCE="NO") Self%EigenPhase%Element(j,1)  
       end if
    end do
    !
    close( uid )
    !
  end subroutine ClassScatteringStatesSaveEigenPhases



  !----------------------------------------
  ! ClassScatteringStatesVec Methods
  !----------------------------------------


  subroutine ClassScatteringStatesVecInit( Self, &
       Basis, SymSpace, SymStorageDir, NumBsNotOverlap, &
       CompMethod, EnergyVec, ScattConditionNumber, ConditionLabel )
    class(ClassScatteringStatesVec)             , intent(inout) :: Self
    class(ClassBasis)                   , target, intent(in)    :: Basis
    class(ClassSymmetricElectronicSpace), target, intent(in)    :: SymSpace
    character(len=*)                            , intent(in)    :: SymStorageDir
    integer                                     , intent(in)    :: NumBsNotOverlap
    character(len=*)                            , intent(in)    :: CompMethod
    real(kind(1d0))                             , intent(in)    :: EnergyVec(:)
    real(kind(1d0))                             , intent(in)    :: ScattConditionNumber
    character(len=:), allocatable               , intent(inout) :: ConditionLabel
    integer :: i
    !
    if ( associated(Self%Basis) ) deallocate( Self%Basis )
    allocate( Self%Basis, source = Basis )
    !
    if ( associated(Self%SymSpace) ) deallocate( Self%SymSpace )
    allocate( Self%SymSpace, source = SymSpace )
    !
    if ( allocated(Self%SymStorageDir) ) deallocate( Self%SymStorageDir )
    allocate( Self%SymStorageDir, source = SymStorageDir )
    !
    if ( allocated(Self%CompMethod) ) deallocate( Self%CompMethod )
    allocate( Self%CompMethod, source = CompMethod )
    !
    if ( allocated(ConditionLabel) ) then
       if ( allocated(Self%ConditionLabel) ) deallocate( Self%ConditionLabel )
       allocate( Self%ConditionLabel, source = ConditionLabel )
    end if
    !
    Self%ScattConditionNumber = ScattConditionNumber
    Self%NumBsNotOverlap = NumBsNotOverlap
    Self%NumEnergies = size(EnergyVec)
    Self%ConditionQC = .false.
    !
    allocate( Self%ScattStates(Self%NumEnergies) )
    !
    do i = 1, Self%NumEnergies
       call Self%ScattStates(i)%Init( &
            Basis, SymSpace, SymStorageDir, NumBsNotOverlap, &
            CompMethod, EnergyVec(i), ScattConditionNumber, &
            ConditionLabel )
    end do
    !
  end subroutine ClassScatteringStatesVecInit



  subroutine ClassScatteringStatesVecSetConditionerQC( Self, ConditionerQC )
    class(ClassScatteringStatesVec), intent(inout) :: Self
    class(ClassMatrix)             , intent(in)    :: ConditionerQC
    if ( (Self%CompMethod .is. HSLEMethodLabel) .or. &
         (Self%CompMethod .is. InvMatMethodLabel) ) then
       Self%ConditionQC = .true. 
       Self%ConditionerQC = ConditionerQC
    end if
  end subroutine ClassScatteringStatesVecSetConditionerQC



  subroutine ClassScatteringStatesVecComputeScatteringStates( Self, Overlap, Hamiltonian, FeshbachMethod )
    class(ClassScatteringStatesVec), intent(inout) :: Self
    !> Overlap matrix.
    class(ClassMatrix)             , intent(in)    :: Overlap
    !> For the input is the Hamiltonian matrix, but will be overwritten
    !! with the H-ES matrix.
    class(ClassMatrix)             , intent(inout) :: Hamiltonian
    logical                        , intent(in)    :: FeshbachMethod
    !
    type(ClassMatrix) :: Ov
    integer :: i, NSolutions
    logical :: FirstEnergy
    real(kind(1d0)) :: E1, E2
    character(len=:), allocatable :: HSpectrumFileName
    type(ClassSpectralResolution) :: HamSpecRes
    !
    if ( Self%CompMethod .is. SpectralMethodLabel ) then
       allocate( HSpectrumFileName, source = GetSpectrumFileName(Self%SymStorageDir,HamiltonianLabel,Self%ConditionLabel) )
       call HamSpecRes%Read( HSpectrumFileName )
    end if
    !
    E1 = 0.d0
    FirstEnergy = .true.
    !
    do i = 1, Self%NumEnergies
       !
       write(output_unit,'(i0,x,a,x,i0,x,a)') i, "from", Self%NumEnergies, "energies ..."
       E2 = Self%ScattStates(i)%Energy
       Ov = Overlap
       call Ov%Multiply(E1-E2)
       call Hamiltonian%Add( Ov )
       call Ov%Free()
       E1 = E2
       !
       if ( Self%CompMethod .is. SpectralMethodLabel ) then
!          call Self%ScattStates(i)%ComputeSPEC( Overlap, HamSpecRes )
          call Self%ScattStates(i)%ComputeSPEC( Hamiltonian, HamSpecRes )
       elseif ( Self%CompMethod .is. HSLEMethodLabel ) then
          if ( Self%ConditionQC ) then
             call Self%ScattStates(i)%ComputeHSLE( Hamiltonian, Self%ConditionerQC )
          else
             call Self%ScattStates(i)%ComputeHSLE( Hamiltonian )
          end if
       elseif ( Self%CompMethod .is. InvMatMethodLabel ) then
          if ( Self%ConditionQC ) then
             call Self%ScattStates(i)%ComputeINVM( Hamiltonian, Self%ConditionerQC )
          else
             call Self%ScattStates(i)%ComputeINVM( Hamiltonian )
          end if
       end if
       !
       call Self%ScattStates(i)%NormalizeScattStates( FirstEnergy, Self%ConditionQC )
       FirstEnergy = .false.
       !
       call Self%ScattStates(i)%SaveScatteringStates( Self%ConditionQC, FeshbachMethod )
       call Self%ScattStates(i)%SaveEigenPhases( Self%ConditionQC, FeshbachMethod )
       call Self%ScattStates(i)%Free()
       !
    end do
    !
  end subroutine ClassScatteringStatesVecComputeScatteringStates




  subroutine ClassScatteringStatesVecSaveScatteringStates( Self, FeshbachMethod )
    class(ClassScatteringStatesVec), intent(inout) :: Self
    logical                        , intent(in)    :: FeshbachMethod
    integer :: i
    do i = 1, Self%NumEnergies
       call Self%ScattStates(i)%SaveScatteringStates( Self%ConditionQC, FeshbachMethod )
    end do
  end subroutine ClassScatteringStatesVecSaveScatteringStates



  subroutine ClassScatteringStatesVecSaveEigenPhases( Self, FeshbachMethod )
    class(ClassScatteringStatesVec), intent(inout) :: Self
    logical                        , intent(in)    :: FeshbachMethod
    integer :: i
    do i = 1, Self%NumEnergies
       call Self%ScattStates(i)%SaveEigenPhases( Self%ConditionQC, FeshbachMethod )
    end do
  end subroutine ClassScatteringStatesVecSaveEigenPhases





end module ModuleSymmetricElectronicSpaceSpectralMethods
