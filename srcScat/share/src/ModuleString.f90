!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines some subroutines appropiated to work with strings.


module ModuleString

  private

  character(len=*), parameter :: UPPERCASE_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  character(len=*), parameter :: LOWERCASE_ALPHABET = "abcdefghijklmnopqrstuvwxyz" 
  character(len=*), parameter :: DIGITS_STRING = "0123456789"
 
  interface operator (.is.)
     module procedure CaseInsensitiveStrnCmp
  end interface

  interface operator (.isnt.)
     module procedure CaseInsensitiveStrnCmpNot
  end interface

  public :: operator(.is.)
  public :: operator(.isnt.)
  public :: SetStringToUppercase
  public :: FormatDirectoryName
  public :: AlphabeticNumber
  public :: ConvertToStrn
  public :: AddSlash
  public :: FindRepeatingNumberOfPatternInString
  public :: StringHasNumbers
  public :: FormatAsDir
 
contains


  !> Converts a double precision number to a string.
  function ConvertToStrn(InputObject) result(strn)
    implicit none
    !> Input number.
    class(*), intent(in) :: InputObject
    character(len=:), allocatable :: tmpStrn
    character(len=:), allocatable :: Strn
    integer         :: iBuffer
    DoublePrecision :: dBuffer
    allocate(tmpStrn,source="000000000000000000000000")
    select type( Ptr => InputObject)
    type is ( Integer )
       iBuffer=Ptr
       write(tmpStrn,"(i0)") iBuffer
    type is( Real(kind(1d0)) )
       dBuffer=Ptr
       write(tmpStrn,"(d24.16)") dBuffer
    class default
       tmpStrn=" "
    end select
    allocate(Strn,source=trim(adjustl(tmpStrn)))
  end function ConvertToStrn

  !> Given a positive integer number n and a maximum number Nmax, 
  !> returns a string with as many non-blank characters as digits 
  !> in the maximum number: the last are the digits of n, the first
  !> are a repetition of a filling character. E.g.,
  !>  n=42 Nmax=10000 FillingCharacter="*" => Returns "***42".
  function AlphabeticNumber(n,Nmax,FillingCharacter) result(strn)
    implicit none
    !> Original positive integer number.
    integer  , intent(in) :: n
    !> Number of non-blanck characters in the new string containing the original number and the filling characters.
    integer  , optional, intent(in) :: Nmax
    !> The filling character.
    character, optional, intent(in) :: FillingCharacter
    character(len=:), allocatable :: Strn
    character, parameter :: DEFAULT_FILLING_CHARACTER="0"
    character :: FillingCharacter_
    !
    integer :: i,NDigitsN, NDigitsNmax
    allocate(Strn,source="000000000000000000000000")
    write(Strn,*)n
    Strn=trim(adjustl(Strn))
    NDigitsN=NDigitsInteger(n)
    if(present(Nmax))then
       NDigitsNmax=NDigitsInteger(Nmax)
    else
       NDigitsNmax=NDigitsN
    endif
    FillingCharacter_=DEFAULT_FILLING_CHARACTER
    if(present(FillingCharacter))FillingCharacter_=FillingCharacter
    do i=NDigitsN+1,NDigitsNmax
       Strn=FillingCharacter_//trim(Strn)
    enddo
  end function AlphabeticNumber


  !> Returns the number of digits of a positive integer number.
  !>
  integer function NDigitsInteger( n ) result( NDigit)
    implicit none
    integer, intent(in) :: n
    NDigit=int(log(dble(abs(n))+0.5d0)/log(10.d0))
    return
  end function NDigitsInteger


  !> Complete Dir with a "/" character at the end, if absent
  function AddSlash( Dir ) result( DirSlash )
    !> Directory name.
    character(len=*), intent(in) :: Dir
    character(len=:), allocatable :: DirSlash
    if (len_trim(Dir) == 0) then
        allocate(DirSlash, source = "/")
    else
        if(Dir(len_trim(Dir):len_trim(Dir))/="/")then
           allocate(DirSlash,source=trim(adjustl(Dir))//"/")
        else
           allocate(DirSlash,source=trim(adjustl(Dir)))
        endif
    endif
  end function AddSlash


  !> Complete Dir with a "/" character at the end, if absent
  subroutine  FormatDirectoryName( DirectoryPath )
    implicit none
    !> Directory path.
    character(len=*), intent(inout) :: DirectoryPath
    integer :: i
    DirectoryPath=adjustl(DirectoryPath)
    i=len_trim(DirectoryPath)
    if(DirectoryPath(i:i)/="/")DirectoryPath(i+1:i+1)="/"
  end subroutine FormatDirectoryName

  logical function CaseInsensitiveStrnCmp( StringA, StringB ) result( Equivalent )
    !
    character(len=*), intent(in) :: StringA, StringB
    !
    integer   :: CharacterPosition
    integer   :: LengthOfStringA
    integer   :: LengthOfStringB
    character :: charA
    character :: charB
    !
    Equivalent = .TRUE.
    !
    LengthOfStringA = len_trim( StringA )
    LengthOfStringB = len_trim( StringB )
    Equivalent = Equivalent .and. ( LengthOfStringA == LengthOfStringB )
    !
    if(.not.Equivalent) return
    !
    if( LengthOfStringA == 0 )return
    !
    do CharacterPosition = 1, LengthOfStringA
       !
       charA = UppercaseCharacter( StringA( CharacterPosition : CharacterPosition ) )
       charB = UppercaseCharacter( StringB( CharacterPosition : CharacterPosition ) )
       !
       Equivalent = Equivalent .and. ( charA == charB )
       if( .not.Equivalent ) exit
       !
    enddo
    !
  end function CaseInsensitiveStrnCmp


  logical function CaseInsensitiveStrnCmpNot( StringA, StringB ) result( NotEquivalent )
    character(len=*), intent(in) :: StringA, StringB
    NotEquivalent = .not. ( StringA .is. StringB )
    !NotEquivalent = CaseInsensitiveStrnCmp( StringA, StringB) 
  end function CaseInsensitiveStrnCmpNot


  character function UppercaseCharacter( Char ) result( UCChar )
    implicit none
    character(len=*), intent(in) :: Char
    integer :: LetterPosition
    LetterPosition = index( LOWERCASE_ALPHABET, Char )
    if( LetterPosition > 0 )then
       UCChar = UPPERCASE_ALPHABET( LetterPosition : LetterPosition )
    else
       UCChar = Char
    endif
  end function UppercaseCharacter
  
  !> Converts all the letters in a string to upper case.
  subroutine SetStringToUppercase( String ) 
    implicit none
    !> In the input is the string to be transformed, in the ourput the upper case string.
    character(len=*), intent(inout) :: String
    integer :: ichar
    character :: char
    do ichar=1,len_trim( String )
       char=String(ichar:ichar)
       String(ichar:ichar)=UppercaseCharacter(char)
    enddo
  end subroutine SetStringToUppercase
  

  !> Find how many times a substring is repeated in a string
  subroutine FindRepeatingNumberOfPatternInString( String, Pattern, N )
    implicit none
    !> Full string.
    character(len=*), intent(in)  :: String
    !> Sub-string to be found.
    character(len=*), intent(in)  :: Pattern
    !> Times the sub.string is found in the string
    integer,          intent(out) :: N
    !
    integer :: ichar, ichar2
    character(len=:), allocatable :: CopyString, CopyPattern
    !
    N = 0
    !
    allocate( CopyString, source = String )
    allocate( CopyPattern, source = Pattern )
    call SetStringToUppercase(CopyString)
    call SetStringToUppercase(CopyPattern)
    !
    ichar2 = 0
    do
       ichar = index( CopyString(ichar2+1:), CopyPattern )
       if ( ichar < 1 ) exit
       N = N + 1
       ichar2 = ichar2 + ichar
    end do
  end subroutine FindRepeatingNumberOfPatternInString
    


  logical function StringHasNumbers( Strn ) result(HasNum)
    character(len=*), intent(in) :: Strn
    character(len=1) :: DigitChar
    integer :: i, ichar
    HasNum = .false.
    do i = 1, LEN(DIGITS_STRING)
       ichar = INDEX(Strn,DIGITS_STRING(i:i))
       if ( ichar > 0 ) then
          HasNum = .true.
          return
       end if
    end do
  end function StringHasNumbers


  !> Complete Dir with a "/" character at the end, if absent
  function  FormatAsDir( inpDir ) result( outDir )
    implicit none
    character(len=*), intent(in)  :: inpDir
    character(len=:), allocatable :: outDir
    integer :: i
    i = len_trim( inpDir )
    if( i == 0 )then
       allocate( outDir, source = trim(adjustl( inpDir )) )
       return
    endif
    if( inpDir( i : i ) == "/" )then
       allocate( outDir, source = trim(adjustl( inpDir )) )
    else
       allocate( outDir, source = trim(adjustl( inpDir ))//"/" )
    endif
  end function FormatAsDir



end module ModuleString
