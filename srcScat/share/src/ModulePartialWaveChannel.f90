!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module ModulePartialWaveChannel

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleString
  use ModuleGroups
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleParentIons
  use ModuleMatrix
  use symba
  use ModuleConstants
  use ModuleIO

  implicit none

  private

  character(len=*), parameter :: BsplineOrbDir = "BsplineOrbitals"


  type, public :: ClassPartialWaveChannel
     ! {{{ private attributes

     private
     type(ClassParentIon), public, pointer :: ParentIon
     integer                       :: Lmax
     integer                       :: TotalMultiplicity
     type(ClassIrrep), public, pointer :: TotalIrrep
     type(ClassIrrep),     pointer :: OrbitalIrrep
     type(ClassXlm)                :: Xlm
     character(len=:), allocatable :: StorageDir

     ! }}}
   contains
     generic, public :: init                => ClassPartialWaveChannelInit
     generic, public :: show                => ClassPartialWaveChannelShow
     generic, public :: Save                => ClassPartialWaveChannelSave
     generic, public :: Load                => ClassPartialWaveChannelLoad
     generic, public :: free                => ClassPartialWaveChannelFree
     generic, public :: GetLmax             => ClassPartialWaveChannelGetLmax
     generic, public :: GetL                => ClassPartialWaveChannelGetL
     generic, public :: GetM                => ClassPartialWaveChannelGetM
     generic, public :: GetStorageDir       => ClassPartialWaveChannelGetStorageDir
     generic, public :: GetTotIrrepName     => ClassPartialWaveChannelGetTotIrrepName
     generic, public :: GetTotIrrep         => ClassPartialWaveChannelGetTotIrrep
     generic, public :: GetOrbIrrep         => ClassPartialWaveChannelGetOrbIrrep
     generic, public :: GetTotMultiplicity  => ClassPartialWaveChannelGetTotMultiplicity
     generic, public :: GetXlmLabel         => ClassPartialWaveChannelGetXlmLabel
     generic, public :: GetSymLabel         => ClassPartialWaveChannelGetSymLabel
     procedure, public :: GetPILabel        => ClassPartialWaveChannelGetPILabel
     procedure, public :: GetPI             => ClassPartialWaveChannelGetPI
     procedure, public :: GetPINelect       => ClassPartialWaveChannelGetPINelect
     procedure, public :: GetPICharge       => ClassPartialWaveChannelGetPICharge
     procedure, public :: GetPIMultiplicity => ClassPartialWaveChannelGetPIMultiplicity
     generic, public :: GetPIEnergy         => ClassPartialWaveChannelGetPIEnergy, ClassPartialWaveChannelReadPIEnergy
     generic, public :: assignment(=)       => ClassPartialWaveChannelCopyPWC
     ! {{{ private procedures

     procedure, private :: ClassPartialWaveChannelInit
     procedure, private :: ClassPartialWaveChannelShow
     procedure, private :: ClassPartialWaveChannelSave
     procedure, private :: ClassPartialWaveChannelLoad
     procedure, private :: ClassPartialWaveChannelFree
     procedure, private :: ClassPartialWaveChannelGetLMax
     procedure, private :: ClassPartialWaveChannelGetL
     procedure, private :: ClassPartialWaveChannelGetM
     procedure, private :: ClassPartialWaveChannelGetStorageDir
     procedure, private :: ClassPartialWaveChannelGetTotIrrepName
     procedure, private :: ClassPartialWaveChannelGetTotIrrep
     procedure, private :: ClassPartialWaveChannelGetOrbIrrep
     procedure, private :: ClassPartialWaveChannelGetTotMultiplicity
     procedure, private :: ClassPartialWaveChannelGetXlmLabel
     procedure, private :: ClassPartialWaveChannelGetSymLabel
     procedure, private :: ClassPartialWaveChannelGetPILabel
     procedure, private :: ClassPartialWaveChannelGetPI
     procedure, private :: ClassPartialWaveChannelGetPINelect
     procedure, private :: ClassPartialWaveChannelGetPICharge
     procedure, private :: ClassPartialWaveChannelGetPIMultiplicity
     procedure, private :: ClassPartialWaveChannelGetPIEnergy
     procedure, private :: ClassPartialWaveChannelReadPIEnergy
     procedure, private :: ClassPartialWaveChannelCopyPWC

     ! }}}
  end type ClassPartialWaveChannel





  type, public :: ClassBsplineXlmBsplineXlmBlock

     type(ClassXlm)  , pointer     :: BraXlm
     type(ClassXlm)  , pointer     :: OperatorXlm
     character(len=:), allocatable :: OperatorLabel
     type(ClassXlm)  , pointer     :: KetXlm
     type(ClassMatrix)             :: Block

   contains

     generic, public :: init      => ClassBsplineXlmBsplineXlmBlockInit
     generic, public :: save      => ClassBsplineXlmBsplineXlmBlockSave
     generic, public :: ReadBlock => ClassBsplineXlmBsplineXlmBlockReadBlock
     generic, public :: GetFile   => ClassBsplineXlmBsplineXlmBlockGetFile
     generic, public :: Free      => ClassBsplineXlmBsplineXlmBlockFree

     procedure, private :: ClassBsplineXlmBsplineXlmBlockInit
     procedure, private :: ClassBsplineXlmBsplineXlmBlockSave
     procedure, private :: ClassBsplineXlmBsplineXlmBlockReadBlock
     procedure, private :: ClassBsplineXlmBsplineXlmBlockGetFile
     procedure, private :: ClassBsplineXlmBsplineXlmBlockFree
     
  end type ClassBsplineXlmBsplineXlmBlock



  public :: GetBsplineOrbDir



contains



  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !      ClassPartialWaveChannel
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  subroutine ClassPartialWaveChannelInit( &
       Channel          , &
       ParentIon        , &
       Lmax             , &
       TotalMultiplicity, &
       TotalIrrep       , &
       Xlm              , &
       StorageDir         )
    class(ClassPartialWaveChannel) , intent(inout) :: Channel
    class(ClassParentIon), target  , intent(in)    :: ParentIon
    integer                        , intent(in)    :: Lmax
    integer                        , intent(in)    :: TotalMultiplicity
    class(ClassIrrep),     target  , intent(in)    :: TotalIrrep
    class(ClassXlm)                , intent(in)    :: Xlm
    character(len=*)               , intent(in)    :: StorageDir
    Channel%ParentIon          =>  ParentIon
    Channel%Lmax               =   Lmax
    Channel%TotalMultiplicity  =   TotalMultiplicity
    Channel%TotalIrrep         =>  TotalIrrep
    Channel%OrbitalIrrep       =>  TotalIrrep * ParentIon%GetIrrep()
    Channel%Xlm                =   Xlm
    if ( allocated(Channel%StorageDir) ) deallocate( Channel%StorageDir )
    allocate( Channel%StorageDir, source = StorageDir )
  end subroutine ClassPartialWaveChannelInit




  subroutine ClassPartialWaveChannelShow( Channel, unit )
    class(ClassPartialWaveChannel), intent(in) :: Channel
    integer, optional             , intent(in) :: unit
    integer :: outunit
    outunit = OUTPUT_UNIT
    if(present(unit)) outunit = unit
    !
    write(outunit,"(a)") "Partial Wave Channels Info : "
    !
    call Channel%ParentIon%Show(unit)
    write(outunit,"(a,i4)"          ) "  Maximum Angular Momentum :",Channel%Lmax
    write(outunit,"(a,i4)"          ) "  Total Multiplicity :",Channel%TotalMultiplicity
    write(outunit,"(a)",advance="no") "  Total Symmetry     :"
    call Channel%TotalIrrep%show(unit)
    write(outunit,"(a)",advance="no") "  Orbital Symmetry     :"
    call Channel%OrbitalIrrep%show(unit)
    call Channel%Xlm%show(unit)
  end subroutine ClassPartialWaveChannelShow



  subroutine ClassPartialWaveChannelSave( Channel, unit )
    class(ClassPartialWaveChannel), intent(in) :: Channel
    integer                       , intent(in) :: unit
    !
    call Channel%ParentIon%SaveData(unit)
    write(unit,*) Channel%TotalMultiplicity
    call Channel%Xlm%Save(unit)
  end subroutine ClassPartialWaveChannelSave



  subroutine ClassPartialWaveChannelLoad( Channel, unit )
    class(ClassPartialWaveChannel), intent(inout) :: Channel
    integer                       , intent(in)    :: unit
    !
    call Channel%ParentIon%LoadData(unit)
    read(unit,*) Channel%TotalMultiplicity
    call Channel%Xlm%Load(unit)
  end subroutine ClassPartialWaveChannelLoad



  subroutine ClassPartialWaveChannelFree( Channel )
    class(ClassPartialWaveChannel), intent(inout) :: Channel
    Channel%ParentIon    => NULL()
    Channel%TotalIrrep   => NULL()
    Channel%OrbitalIrrep => NULL()
    Channel%Lmax = -1
    Channel%TotalMultiplicity = 0
    call Channel%Xlm%init(0,0)
  end subroutine ClassPartialWaveChannelFree


  integer function ClassPartialWaveChannelGetLMax( Channel ) result( LMax )
    class(ClassPartialWaveChannel), intent(in) :: Channel
    LMax = Channel%Lmax
  end function ClassPartialWaveChannelGetLMax



  integer function ClassPartialWaveChannelGetL( Channel ) result( L )
    class(ClassPartialWaveChannel), intent(in) :: Channel
    L = Channel%Xlm%GetL()
  end function ClassPartialWaveChannelGetL



  integer function ClassPartialWaveChannelGetM( Channel ) result( M )
    class(ClassPartialWaveChannel), intent(in) :: Channel
    M = Channel%Xlm%GetM()
  end function ClassPartialWaveChannelGetM


  function ClassPartialWaveChannelGetStorageDir( Self ) result( Dir )
    class(ClassPartialWaveChannel), intent(inout) :: Self
    character(len=:), allocatable :: Dir
    if ( .not.allocated(Self%StorageDir) ) call Assert( &
         'The storage directory is not allocated in PWC, impossible to fetch.' )
    allocate( Dir, source = Self%StorageDir )
  end function ClassPartialWaveChannelGetStorageDir



  function ClassPartialWaveChannelGetTotIrrepName( Self ) result(IrrepName)
    class(ClassPartialWaveChannel), intent(in) :: Self
    character(len=:), allocatable :: IrrepName
    allocate( IrrepName, source = Self%TotalIrrep%GetName() )
  end function ClassPartialWaveChannelGetTotIrrepName


  function ClassPartialWaveChannelGetTotIrrep( Self ) result(Irrep)
    class(ClassPartialWaveChannel), target, intent(in) :: Self
    type(ClassIrrep), pointer :: Irrep
    Irrep => Self%TotalIrrep
  end function ClassPartialWaveChannelGetTotIrrep


  function ClassPartialWaveChannelGetOrbIrrep( Self ) result(Irrep)
    class(ClassPartialWaveChannel), target, intent(in) :: Self
    type(ClassIrrep), pointer :: Irrep
    Irrep => Self%OrbitalIrrep
  end function ClassPartialWaveChannelGetOrbIrrep



  function ClassPartialWaveChannelGetTotMultiplicity( Self ) result(Mult)
    class(ClassPartialWaveChannel), intent(in) :: Self
    integer :: Mult
    Mult = Self%TotalMultiplicity
  end function ClassPartialWaveChannelGetTotMultiplicity




  function ClassPartialWaveChannelGetSymLabel( Self ) result(Label)
    class(ClassPartialWaveChannel), intent(in) :: Self
    character(len=:), allocatable :: Label
    allocate( Label, source = AlphabeticNumber(Self%GetTotMultiplicity())//Self%GetTotIrrepName() )
  end function ClassPartialWaveChannelGetSymLabel



  function ClassPartialWaveChannelGetXlmLabel( Channel ) result( Label )
    class(ClassPartialWaveChannel), intent(inout) :: Channel
    character(len=:), allocatable :: Label
    allocate( Label, source = Channel%Xlm%GetLabel() )
  end function ClassPartialWaveChannelGetXlmLabel




  function ClassPartialWaveChannelGetPILabel( PWC ) result( Label )
    class(ClassPartialWaveChannel), intent(inout) :: PWC
    character(len=:), allocatable :: Label
    allocate( Label, source = PWC%Parention%GetLabel() )
  end function ClassPartialWaveChannelGetPILabel



  function ClassPartialWaveChannelGetPI( PWC ) result( PI )
    class(ClassPartialWaveChannel), target, intent(in) :: PWC
    class(ClassparentIon), pointer :: PI
    allocate( PI, source = PWC%Parention )
  end function ClassPartialWaveChannelGetPI


  integer function ClassPartialWaveChannelGetPINelect( PWC ) result( PINE )
    class(ClassPartialWaveChannel), intent(in) :: PWC
    PINE = PWC%Parention%GetNelect()
  end function ClassPartialWaveChannelGetPINelect



  integer function ClassPartialWaveChannelGetPICharge( PWC ) result( PICharge )
    class(ClassPartialWaveChannel), intent(in) :: PWC
    PICharge = PWC%ParentIon%GetCharge()
  end function ClassPartialWaveChannelGetPICharge



  integer function ClassPartialWaveChannelGetPIMultiplicity( PWC ) result( PIMult )
    class(ClassPartialWaveChannel), intent(in) :: PWC
    PIMult = PWC%ParentIon%GetMultiplicity()
  end function ClassPartialWaveChannelGetPIMultiplicity



  real(kind(1d0)) function ClassPartialWaveChannelGetPIEnergy( PWC ) result( PIE )
    class(ClassPartialWaveChannel), intent(inout) :: PWC
    PIE = PWC%ParentIon%GetEnergy()
  end function ClassPartialWaveChannelGetPIEnergy



  real(kind(1d0)) function ClassPartialWaveChannelReadPIEnergy( PWC, StorageDir ) result( PIE )
    class(ClassPartialWaveChannel), intent(inout) :: PWC
    character(len=*)              , intent(in)    :: StorageDir
    PIE = PWC%ParentIon%ReadEnergy( StorageDir )
  end function ClassPartialWaveChannelReadPIEnergy



  !> Copies the information of PWC class to another.
  subroutine ClassPartialWaveChannelCopyPWC( PWCout, PWCin )
    !
    class(ClassPartialWaveChannel)        , intent(inout) :: PWCout
    class(ClassPartialWaveChannel), target, intent(in)    :: PWCin
    !
    call PWCout%Free()
    !
    PWCout%ParentIon         => PWCin%ParentIon
    PWCout%Lmax              =  PWCin%Lmax
    PWCout%TotalMultiplicity =  PWCin%TotalMultiplicity
    PWCout%TotalIrrep        => PWCin%TotalIrrep
    PWCout%OrbitalIrrep      => PWCin%OrbitalIrrep
    PWCout%Xlm               =  PWCin%Xlm
    allocate( PWCout%StorageDir, source = PWCin%StorageDir )
    !
  end subroutine ClassPartialWaveChannelCopyPWC

  
  subroutine ClassPartialWaveChannelFinal( Channel )
    type(ClassPartialWaveChannel) :: Channel
    call Channel%free()
  end subroutine ClassPartialWaveChannelFinal





  !------------------------------------------------------
  ! Methods for ClassBsplineXlmBsplineXlmBlock
  !------------------------------------------------------



  subroutine ClassBsplineXlmBsplineXlmBlockInit( this, BraXlm, OpXlm, OpLabel, KetXlm, Block )
    class(ClassBsplineXlmBsplineXlmBlock), intent(inout) :: this
    class(ClassXlm)  , target, intent(in) :: BraXlm
    class(ClassXlm)  , target, intent(in) :: OpXlm
    character(len=*)         , intent(in) :: OpLabel
    class(ClassXlm)  , target, intent(in) :: KetXlm
    class(ClassMatrix), optional, intent(in) :: Block
    
    this%BraXlm       => BraXlm
    this%OperatorXlm  => OpXlm
    this%KetXlm       => KetXlm
    if(allocated(this%OperatorLabel))deallocate(this%OperatorLabel)
    allocate(this%OperatorLabel,source=OpLabel)
    if(present(Block)) this%Block = Block
    
  end subroutine ClassBsplineXlmBsplineXlmBlockInit



  subroutine ClassBsplineXlmBsplineXlmBlockFree( this )
    class(ClassBsplineXlmBsplineXlmBlock), intent(inout) :: this
    this%BraXlm       => NULL()
    this%OperatorXlm  => NULL()
    this%KetXlm       => NULL()
    if(allocated(this%OperatorLabel))deallocate(this%OperatorLabel)
    call this%Block%Free()
  end subroutine ClassBsplineXlmBsplineXlmBlockFree



  function ClassBsplineXlmBsplineXlmBlockGetFile( this ) result( FileName )
    class(ClassBsplineXlmBsplineXlmBlock), intent(in) :: this
    character(len=:), allocatable :: FileName
    allocate(FileName, source=&
         this%OperatorLabel//&
         AlphabeticNumber(this%OperatorXlm%Getl())//"."//AlphabeticNumber(this%OperatorXlm%Getm())//"_"//&
         AlphabeticNumber(this%BraXlm%Getl())//"."//AlphabeticNumber(this%BraXlm%Getm())//"_"//&
         AlphabeticNumber(this%KetXlm%Getl())//"."//AlphabeticNumber(this%KetXlm%Getm()) )
  end function ClassBsplineXlmBsplineXlmBlockGetFile



  subroutine ClassBsplineXlmBsplineXlmBlockSave( this, Storage, WithFormat )
    class(ClassBsplineXlmBsplineXlmBlock), intent(inout) :: this
    character(len=*)                     , intent(in)    :: Storage
    logical, optional                    , intent(in)  :: WithFormat
    
    character(len=:), allocatable :: FileName
    integer                       :: uid
    logical :: Formatted
    
    if ( present(WithFormat) .and. WithFormat ) then
       Formatted = .true.
    else
       Formatted = .false.
    end if

    call execute_command_line("mkdir -p "//AddSlash(Storage)//AddSlash(BsplineOrbDir) )

    allocate(FileName, source= AddSlash(Storage)//AddSlash(BsplineOrbDir)//this%GetFile())
    if ( Formatted ) then
       call OpenFile( FileName, uid, "write", "formatted" )
    else
       call OpenFile( FileName, uid, "write", "unformatted" )
    end if
    call this%Block%write(uid)
    close(uid)
    
  end subroutine ClassBsplineXlmBsplineXlmBlockSave



  subroutine ClassBsplineXlmBsplineXlmBlockReadBlock( this, Storage, Matrix )
    class(ClassBsplineXlmBsplineXlmBlock), intent(in)  :: this
    character(len=*)                     , intent(in)  :: Storage
    class(ClassMatrix)                   , intent(out) :: Matrix
    !
    character(len=:), allocatable :: FileName
    integer                       :: uid, iostat
    character(len=32) :: form
    character(len=IOMSG_LENGTH) :: iomsg
    logical :: exist
    !
    allocate(FileName, source= AddSlash(Storage)//AddSlash(BsplineOrbDir)//this%GetFile())
    INQUIRE( file = FileName, exist = exist )
    if ( .not.exist ) then
       call ErrorMessage( 'The file '//FileName//' does not exist.' )
       return
    end if
    !
    call OpenFile( FileName, uid, "read", "formatted" )
    call Matrix%Read(uid)
    close(uid)
    !
  end subroutine ClassBsplineXlmBsplineXlmBlockReadBlock



  function GetBsplineOrbDir() result(Dir)
    character(len=:), allocatable :: Dir
    allocate( Dir, source = BsplineOrbDir )
  end function GetBsplineOrbDir



end module ModulePartialWaveChannel
