!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module chebmod

  use ModuleErrorHandling

  implicit none

  integer, parameter :: MAXCHEBORDER=511

  type cheb_series
     integer :: order
     real(kind(1d0)) :: a,b
     real(kind(1d0)) :: c(0:MAXCHEBORDER)
  end type cheb_series

contains

  subroutine set_cheb_zero(cs)
    type(cheb_series) :: cs
    cs%order=0
    cs%a=0.d0
    cs%b=0.d0
    return
  end subroutine set_cheb_zero

  subroutine free_cheb_series(cs)
    type(cheb_series) :: cs
    cs%order=0
    cs%a=0.d0
    cs%b=0.d0
    return
  end subroutine free_cheb_series

  integer function save_cheb_series(c,STREAM)
    type(cheb_series), intent(in) :: c
    integer          , intent(in) :: STREAM
    integer :: stat,i
    save_cheb_series=0
    write(STREAM,IOSTAT=stat)c%order,c%a,c%b
    if(stat/=0)then
       save_cheb_series=1
       return
    endif
       write(STREAM,IOSTAT=stat)(c%c(i),i=0,c%order)
       if(stat/=0)save_cheb_series=2
    return
  end function save_cheb_series

  integer function load_cheb_series(c,STREAM)
    type(cheb_series), intent(out):: c
    integer          , intent(in) :: STREAM
    integer :: stat,i
    load_cheb_series=0
    read(STREAM,IOSTAT=stat)c%order,c%a,c%b
    if(stat/=0)then
       load_cheb_series=1
       return
    endif
       read(STREAM,IOSTAT=stat)(c%c(i),i=0,c%order)
       if(stat/=0)load_cheb_series=2
    return
  end function load_cheb_series

  integer function cheb_copy(c1,c2)
    type(cheb_series), intent(in) :: c1
    type(cheb_series), intent(out):: c2
    cheb_copy=0
    c2%a=c1%a
    c2%b=c1%b
    c2%order=c1%order
    c2%c(0:c2%order)=c1%c(0:c1%order)
    return
  end function cheb_copy

  real(kind(1d0)) function cheb_eval_e(x,cs)
    !Input variables
    real(kind(1d0))  , intent(in) :: x
    type(cheb_series), intent(in) :: cs
    !Local variables
    integer j
    real(kind(1d0)) :: d,dd,y,y2,temp!,e
    d =0.d0
    dd=0.d0
    y =(2.d0*x-cs%a-cs%b)/(cs%b-cs%a)
    y2= 2.d0*y
    do j=cs%order,1,-1
       temp=d
       d=y2*d-dd+cs%c(j)
       dd=temp
    enddo
    d=y*d-dd+0.5d0*cs%c(0)
    cheb_eval_e=d
    return
  end function cheb_eval_e

  real(kind(1d0)) function cheb_eval(x,cs)
    real(kind(1d0))  , intent(in) :: x
    type(cheb_series), intent(in) :: cs
    cheb_eval=cheb_eval_e(x,cs)
    return
  end function cheb_eval
  
  subroutine cheb_init_grid(a,b,order,xgrid)
    real(kind(1d0)), parameter  :: PI=3.1415926535897932d0
    real(kind(1d0)), intent(in) :: a,b
    integer        , intent(in) :: order
    real(kind(1d0)), intent(out):: xgrid(0:order)
    integer :: i
    real(kind(1d0)) :: wp,wm,ropu
    wm=0.5d0*(b-a)
    wp=0.5d0*(b+a)
    ropu=1.d0/dble(order+1)
    do i=0,order
       xgrid(i)=wp+wm*cos(PI*(dble(i)+0.5d0)*ropu)
    enddo
    return
  end subroutine cheb_init_grid
  
  subroutine cheb_init(a,b,order,yg,cs)
    real(kind(1d0))  , parameter  :: PI=3.1415926535897932d0
    real(kind(1d0))  , intent(in) :: a,b
    integer          , intent(in) :: order
    real(kind(1d0))  , intent(in) :: yg(0:order)
    type(cheb_series), intent(out):: cs
    integer :: j, k
    real(kind(1d0)) :: sum,fac,ropu,jr

    if(order>MAXCHEBORDER) stop !call Assert("Error working with special functions.")
    cs%a=a
    cs%b=b
    cs%order=order
    cs%c=0.d0
    ropu=1.d0/dble(order+1)
    fac =2.d0*ropu
    ropu=PI*ropu
    do j=0,order
       jr=dble(j)
       sum=0.d0
       do k=0,order
          sum=sum+yg(k)*cos(jr*(dble(k)+0.5d0)*ropu)
       enddo
       cs%c(j)=fac*sum
    enddo
    return
  end subroutine cheb_init

end module chebmod


module specfun

  use ModuleErrorHandling

  implicit none

  private

  integer, parameter, public :: SF_SUCCESS   = 0
  integer, parameter, public :: SF_UNDERFLOW = 1
  integer, parameter, public :: SF_OVERFLOW  = 2

  real(kind(1d0)), parameter, public :: SF_PI           =  3.1415926535897932d0
  real(kind(1d0)), parameter, public :: SF_E            =  2.7182818284590452d0
  real(kind(1d0)), parameter, public :: SF_EULER        =  0.5772156649015329d0
  real(kind(1d0)), parameter, public :: SF_LNPI         =  1.1447298858494002d0
  real(kind(1d0)), parameter, public :: SF_LN2          =  6.9314718055994531d-1 
  real(kind(1d0)), parameter, public :: SF_LR2P         =  0.9189385332046727d0
  real(kind(1d0)), parameter, public :: SF_SQRT2        =  1.4142135623730950d0
  real(kind(1d0)), parameter, public :: SF_SQRTPI       =  1.7724538509055160d0
  real(kind(1d0)), parameter, public :: SF_DBL_MIN      =  2.2250738585072014d-308
  real(kind(1d0)), parameter, public :: SF_DBL_MAX      =  1.7976931348623157d+308
  real(kind(1d0)), parameter, public :: SF_LOG_DBL_MAX  =  7.0978271289338397d2
  real(kind(1d0)), parameter, public :: SF_LOG_DBL_MIN  = -7.0839641853226408d2
  real(kind(1d0)), parameter, public :: SF_SQRT_DBL_MIN =  1.4916681462400413d-154
  real(kind(1d0)), parameter, public :: SF_SQRT_DBL_MAX =  1.3407807929942596d15
  real(kind(1d0)), parameter, public :: SF_DBL_EPSILON  =  2.2204460492503131d-16
  integer        , parameter, public :: SF_INT_MIN      = -2147483647
  integer        , parameter, public :: SF_INT_MAX      =  2147483647

  public :: SF_Gamma, SF_Laguerre, SF_Hydrogenic, SF_Coulomb,SF_Hyperg_2F1!,crash

  integer        , parameter :: NFACT=170
  real(kind(1d0)), parameter :: FACT(0:NFACT)=(/&
       1.000000000000000d0,&
       1.000000000000000d0,&
       2.000000000000000d0,&
       6.000000000000000d0,&
       24.00000000000000d0,&
       120.0000000000000d0,&
       720.0000000000000d0,&
       5040.000000000000d0,&
       40320.00000000000d0,&
       362880.0000000000d0,&
       3628800.000000000d0,&
       39916800.00000000d0,&
       479001600.0000000d0,&
       6227020800.000000d0,&
       87178291200.00000d0,&
       1307674368000.000d0,&
       20922789888000.00d0,&
       355687428096000.0d0,&
       6402373705728000.d0,&
       1.216451004088320d17,&
       2.432902008176640d18,&
       5.109094217170944d19,&
       1.124000727777608d21,&
       2.585201673888498d22,&
       6.204484017332394d23,&
       1.551121004333099d25,&
       4.032914611266057d26,&
       1.088886945041835d28,&
       3.048883446117138d29,&
       8.841761993739701d30,&
       2.652528598121910d32,&
       8.222838654177922d33,&
       2.631308369336935d35,&
       8.683317618811886d36,&
       2.952327990396041d38,&
       1.033314796638614d40,&
       3.719933267899012d41,&
       1.376375309122634d43,&
       5.230226174666010d44,&
       2.039788208119744d46,&
       8.159152832478977d47,&
       3.345252661316380d49,&
       1.405006117752880d51,&
       6.041526306337383d52,&
       2.658271574788449d54,&
       1.196222208654802d56,&
       5.502622159812088d57,&
       2.586232415111682d59,&
       1.241391559253607d61,&
       6.082818640342675d62,&
       3.041409320171338d64,&
       1.551118753287382d66,&
       8.065817517094388d67,&
       4.274883284060025d69,&
       2.308436973392414d71,&
       1.269640335365828d73,&
       7.109985878048635d74,&
       4.052691950487722d76,&
       2.350561331282879d78,&
       1.386831185456899d80,&
       8.320987112741392d81,&
       5.075802138772248d83,&
       3.146997326038794d85,&
       1.982608315404440d87,&
       1.268869321858842d89,&
       8.247650592082472d90,&
       5.443449390774431d92,&
       3.647111091818868d94,&
       2.480035542436831d96,&
       1.711224524281413d98,&
       1.197857166996989d100,&
       8.504785885678622d101,&
       6.123445837688608d103,&
       4.470115461512683d105,&
       3.307885441519386d107,&
       2.480914081139539d109,&
       1.885494701666050d111,&
       1.451830920282858d113,&
       1.132428117820629d115,&
       8.946182130782973d116,&
       7.156945704626378d118,&
       5.797126020747366d120,&
       4.753643337012840d122,&
       3.945523969720657d124,&
       3.314240134565352d126,&
       2.817104114380549d128,&
       2.422709538367272d130,&
       2.107757298379527d132,&
       1.854826422573984d134,&
       1.650795516090845d136,&
       1.485715964481761d138,&
       1.352001527678402d140,&
       1.243841405464130d142,&
       1.156772507081641d144,&
       1.087366156656742d146,&
       1.032997848823905d148,&
       9.916779348709491d149,&
       9.619275968248206d151,&
       9.426890448883242d153,&
       9.332621544394410d155,&
       9.332621544394410d157,&
       9.425947759838354d159,&
       9.614466715035121d161,&
       9.902900716486175d163,&
       1.029901674514562d166,&
       1.081396758240290d168,&
       1.146280563734708d170,&
       1.226520203196137d172,&
       1.324641819451828d174,&
       1.443859583202493d176,&
       1.588245541522742d178,&
       1.762952551090244d180,&
       1.974506857221073d182,&
       2.231192748659812d184,&
       2.543559733472186d186,&
       2.925093693493014d188,&
       3.393108684451897d190,&
       3.969937160808719d192,&
       4.684525849754288d194,&
       5.574585761207603d196,&
       6.689502913449124d198,&
       8.094298525273440d200,&
       9.875044200833598d202,&
       1.214630436702532d205,&
       1.506141741511140d207,&
       1.882677176888925d209,&
       2.372173242880046d211,&
       3.012660018457658d213,&
       3.856204823625803d215,&
       4.974504222477285d217,&
       6.466855489220472d219,&
       8.471580690878817d221,&
       1.118248651196004d224,&
       1.487270706090685d226,&
       1.992942746161518d228,&
       2.690472707318050d230,&
       3.659042881952547d232,&
       5.012888748274990d234,&
       6.917786472619486d236,&
       9.615723196941086d238,&
       1.346201247571752d241,&
       1.898143759076170d243,&
       2.695364137888161d245,&
       3.854370717180071d247,&
       5.550293832739301d249,&
       8.047926057471987d251,&
       1.174997204390910d254,&
       1.727245890454638d256,&
       2.556323917872864d258,&
       3.808922637630567d260,&
       5.713383956445850d262,&
       8.627209774233235d264,&
       1.311335885683452d267,&
       2.006343905095681d269,&
       3.089769613847349d271,&
       4.789142901463391d273,&
       7.471062926282891d275,&
       1.172956879426414d278,&
       1.853271869493734d280,&
       2.946702272495037d282,&
       4.714723635992059d284,&
       7.590705053947215d286,&
       1.229694218739449d289,&
       2.004401576545302d291,&
       3.287218585534294d293,&
       5.423910666131586d295,&
       9.003691705778433d297,&
       1.503616514864998d300,&
       2.526075744973197d302,&
       4.269068009004703d304,&
       7.257415615307994d306/)

  real(kind(1d0)), parameter :: SF_GAMMA_XMAX=170.d0

  integer :: SF_GAMMA_IERRNO
  integer :: SF_GAMMA_XGTHALF_IERRNO

  !Chebyshev expansion for log(gamma(x)/gamma(8))
  ! 5 < x < 10
  ! -1 < t < 1
  real(kind(1d0)), parameter :: gamma_5_10_data(0:23) =(/&
       -1.5285594096661578881275075214d0,&
       4.8259152300595906319768555035d0,&
       0.2277712320977614992970601978d0,&
       -0.0138867665685617873604917300d0,&
       0.0012704876495201082588139723d0,&
       -0.0001393841240254993658962470d0,&
       0.0000169709242992322702260663d0,&
       -2.2108528820210580075775889168d-6,&
       3.0196602854202309805163918716d-7,&
       -4.2705675000079118380587357358d-8,&
       6.2026423818051402794663551945d-9,&
       -9.1993973208880910416311405656d-10,&
       1.3875551258028145778301211638d-10,&
       -2.1218861491906788718519522978d-11,&
       3.2821736040381439555133562600d-12,&
       -5.1260001009953791220611135264d-13,&
       8.0713532554874636696982146610d-14,&
       -1.2798522376569209083811628061d-14,&
       2.0417711600852502310258808643d-15,&
       -3.2745239502992355776882614137d-16,&
       5.2759418422036579482120897453d-17,&
       -8.5354147151695233960425725513d-18,&
       1.3858639703888078291599886143d-18,&
       -2.2574398807738626571560124396d-19/)


contains

  ! Gamma is the f90 translation by Luca Argenti of subroutines in
  ! file 
  ! specfunc/gamma.c
  ! implemented in GSL (Gnu Scientific Library) by Gerard Jungman
  ! under GPL General Public license:
  ! 
  !! Copyright (C) 1996, 1997, 1998, 1999, 2000 Gerard Jungman
  !! 
  !! This program is free software; you can redistribute it and/or modify
  !! it under the terms of the GNU General Public License as published by
  !! the Free Software Foundation; either version 2 of the License, or (at
  !! your option) any later version.
  !! 
  !! This program is distributed in the hope that it will be useful, but
  !! WITHOUT ANY WARRANTY; without even the implied warranty of
  !! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  !! General Public License for more details.
  !! 
  !! You should have received a copy of the GNU General Public License
  !! along with this program; if not, write to the Free Software
  !! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
  !! MA 02110-1301, USA.
  !!

  real(kind(1d0)) function SF_Gamma(x)
    !
    !..
    !.. Description 
    !
    !   Real Gamma function based on Lanczos Algorithm
    !   Lanczos C, SIAM Journal on Numerical Analysis B, 
    !              vol. 1, pp.86(1964)
    !
    !..
    !.. Input variables
    real(kind(1d0)), intent(in) :: x
    !
    !..
    !.. Local variables
    integer         :: rint_x
    real(kind(1d0)) :: f_x, sgn_gamma, sin_term,g


    SF_Gamma=0.d0
    SF_GAMMA_IERRNO=0

    if(x<0.d0)then
       SF_GAMMA_IERRNO=-1
       return
    elseif(x<0.5d0)then
       rint_x=floor(x+0.5d0);
       f_x=x-rint_x;
       sgn_gamma=1.d0-2.d0*mod(rint_x,2)
       sin_term=sgn_gamma*sin(SF_PI*f_x)/SF_PI;
       if(sin_term==0.d0)then
          SF_GAMMA_IERRNO=1
          return
       else
          g=gamma_xgthalf(1.d0-x);
          if(abs(sin_term)*g*SF_DBL_MIN<1.d0)then
             SF_Gamma= 1.d0/(sin_term*g);
             return
          else
             SF_GAMMA_IERRNO=2
             return
          endif
       end if
    else
       SF_Gamma=gamma_xgthalf(x);
    end if
    return
  end function SF_Gamma

  real(kind(1d0)) function gamma_xgthalf(x)
    use chebmod
    !Input variables
    real(kind(1d0)), intent(in) :: x
    !Parameters
    real(kind(1d0)), parameter :: c1 =  0.4227843350984671394d0
    real(kind(1d0)), parameter :: c2 = -0.01094400467202744461d0
    real(kind(1d0)), parameter :: c3 =  0.09252092391911371098d0
    real(kind(1d0)), parameter :: c4 = -0.018271913165599812664d0
    real(kind(1d0)), parameter :: c5 =  0.018004931096854797895d0
    real(kind(1d0)), parameter :: c6 = -0.006850885378723806846d0
    real(kind(1d0)), parameter :: c7 =  0.003998239557568466030d0
    !
    real(kind(1d0)), parameter :: d1 =  0.4227843350984671394d0
    real(kind(1d0)), parameter :: d2 =  0.4118403304264396948d0
    real(kind(1d0)), parameter :: d3 =  0.08157691924708626638d0
    real(kind(1d0)), parameter :: d4 =  0.07424901075351389832d0
    real(kind(1d0)), parameter :: d5 = -0.00026698206874501476832d0
    real(kind(1d0)), parameter :: d6 =  0.011154045718130991049d0
    real(kind(1d0)), parameter :: d7 = -0.002852645821155340816d0
    real(kind(1d0)), parameter :: d8 =  0.0021039333406973880085d0
    !Local variables
    real(kind(1d0))         :: eps,lg,t,p,e,q,pre
    logical          , save :: INITIALIZED=.FALSE.
    type(cheb_series), save :: gamma_5_10_cs

    if(.not.INITIALIZED)then
       gamma_5_10_cs%order=23
       !allocate(gamma_5_10_cs%c(0:23))
       gamma_5_10_cs%c(0:23)=gamma_5_10_data(0:23)
       gamma_5_10_cs%a=-1.d0
       gamma_5_10_cs%b= 1.d0
       INITIALIZED=.TRUE.
    endif

    SF_GAMMA_XGTHALF_IERRNO=0
    if(x==0.5d0)then
       gamma_xgthalf=1.77245385090551602729817d0;
    elseif(x<=(dble(NFACT)+1.d0).and.x==dble(floor(x)))then
       gamma_xgthalf=FACT(floor(x)-1)
    elseif(abs(x-1.d0)<0.01d0)then
       !Use series for Gamma[1+eps] - 1/(1+eps).
       eps = x - 1.d0;
       gamma_xgthalf = 1.d0/x + &
            eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*c7))))));
    elseif(abs(x - 2.d0) < 0.01d0)then
       !Use series for Gamma[1 + eps].
       eps = x - 2.d0;
       gamma_xgthalf = 1.d0 +&
            eps*(d1+eps*(d2+eps*(d3+eps*(d4+eps*(d5+eps*(d6+eps*(d7+eps*d8)))))));
    elseif(x<5.d0)then
       ! Exponentiating the logarithm is fine, as
       ! long as the exponential is not so large
       ! that it greatly amplifies the error.
       lg=lngamma_lanczos(x)
       gamma_xgthalf = exp(lg)
    elseif(x<10.d0)then
       ! This is a sticky area. The logarithm is too large and 
       ! the gammastar series is not good.
       t=(2.d0*x-15.d0)/5.d0;
       gamma_xgthalf=5040.d0*exp(cheb_eval_e(t,gamma_5_10_cs));
    elseif(x<SF_GAMMA_XMAX)then
       ! We do not want to exponentiate the logarithm
       ! if x is large because of the inevitable
       ! inflation of the error. So we carefully
       ! use pow() and exp() with exact quantities.
       p=pow(x,0.5d0*x);
       e=exp(-x)
       q=p*p*e;
       pre=SF_SQRT2*SF_SQRTPI*q/sqrt(x);
       gamma_xgthalf=pre*gammastar_ser(x)
    else
       SF_GAMMA_XGTHALF_IERRNO=1
    end if
    return
  end function gamma_xgthalf

  real(kind(1d0)) function pow(x,y)
    real(kind(1d0)), intent(in) :: x,y
    pow=0.d0
    if(x<=0)return
    pow=exp(y*log(x))
    return
  end function pow

  ! series for gammastar(x)
  ! double-precision for x > 10.0
  real(kind(1d0)) function gammastar_ser(x)
    ! Use the Stirling series for the correction to 
    ! Log(Gamma(x)), which is better behaved and easier to 
    ! compute than the regular Stirling series for Gamma(x). 
    !
    !Input variables
    real(kind(1d0)), intent(in) :: x
    !Parameters
    real(kind(1d0)), parameter :: c0 =  1.d0/12.d0
    real(kind(1d0)), parameter :: c1 = -1.d0/360.d0
    real(kind(1d0)), parameter :: c2 =  1.d0/1260.d0
    real(kind(1d0)), parameter :: c3 = -1.d0/1680.d0
    real(kind(1d0)), parameter :: c4 =  1.d0/1188.d0
    real(kind(1d0)), parameter :: c5 = -691.d0/360360.d0
    real(kind(1d0)), parameter :: c6 =  1.d0/156.d0
    real(kind(1d0)), parameter :: c7 = -3617.d0/122400.d0
    !Local variables
    real(kind(1d0)) :: y,a,b,ser
    y=1.0/(x*x)
    a=c6+y*c7
    b=c5+y*a
    a=c4+y*b
    b=c3+y*a
    a=c2+y*b
    b=c1+y*a
    ser=c0+y*b
    gammastar_ser=exp(ser/x)
    return
  end function gammastar_ser

  real(kind(1d0)) function lngamma_lanczos(x_)
    !Description:
    ! Lanczos method for real x > 0;
    ! gamma=7, truncated at 1/(z+8) 
    ! [J. SIAM Numer. Anal, Ser. B, 1 (1964) 86]
    !
    !Input variables
    real(kind(1d0)), intent(in) :: x_
    !
    !Parameters
    real(kind(1d0)), parameter :: lanczos_7_c(0:8)=(/&
         0.99999999999980993227684700473478d0,&
         676.520368121885098567009190444019d0,&
         -1259.13921672240287047156078755283d0,&
         771.3234287776530788486528258894d0,&
         -176.61502916214059906584551354d0,&
         12.507343278686904814458936853d0,&
         -0.13857109526572011689554707d0,&
         9.984369578019570859563d-6,&
         1.50563273514931155834d-7/)
    !
    !Local variables
    integer         :: k
    real(kind(1d0)) :: Ag,term1,term2,x
    !
    x = x_-1.d0 !Lanczos writes z! instead of Gamma(z)
    Ag = lanczos_7_c(0);
    do k=1, 8
       Ag=Ag+lanczos_7_c(k)/(x+dble(k))
    enddo
    term1 = (x+0.5d0)*log((x+7.5d0)/SF_E)
    term2 = SF_LR2P+log(Ag)
    lngamma_lanczos=term1+(term2-7.d0)
    return
  end function lngamma_lanczos

  real(kind(1d0)) function lnfact(n)
    !Input variables
    integer, intent(in) :: n
    !Local variables and parameters
    integer        , parameter :: NVEC=4096
    logical        , save      :: INITIALIZED=.FALSE.
    real(kind(1d0)), save      :: VEC(0:NVEC)
    integer :: i
    if(n<2)then
       lnfact=0.d0
       return
    endif
    if(.not.INITIALIZED)then
       VEC(0)=0.d0
       VEC(1)=0.d0
       do i=2,NVEC
          VEC(i)=VEC(i-1)+log(dble(i))
       enddo
       INITIALIZED=.TRUE.
    end if
    if(n<=NVEC)then
       lnfact=VEC(n)
    else
       lnfact=VEC(NVEC)
       do i=NVEC+1,n
          lnfact=lnfact+log(dble(i))
       enddo
    endif
    return
  end function lnfact

  integer function laguerre_n_cp(x,a_,n,res)
    ! Evaluate polynomial based on confluent hypergeometric representation.
    ! L^a_n(x) = (a+1)_n / n! 1F1(-n,a+1,x)
    ! assumes n > 0 and a != negative integer greater than -n
    implicit none
    !Input variables
    real(kind(1d0)), intent(in) :: x,a_
    integer        , intent(in) :: n
    real(kind(1d0)), intent(out):: res
    !Local variables
    integer :: k,a
    a=int(a_+0.1)
    laguerre_n_cp=SF_OVERFLOW
    res=1.d0
    do k=n-1,0,-1
       res=1+res*dble(-n+k)/dble(a+1+k)*(x/dble(k+1))
       if(res>0.9*SF_DBL_MAX)return
    end do
    laguerre_n_cp=SF_SUCCESS
    res=res*exp(lnfact(a+n)-lnfact(a)-lnfact(n))
    return
  end function laguerre_n_cp


  integer function laguerre_n_poly_safe(x,a,n,res)
    ! Evaluate the polynomial based on the confluent hypergeometric
    ! function in a safe way, with no restriction on the arguments.
    ! assumes x != 0
    real(kind(1d0)), intent(in) :: x,a
    integer        , intent(in) :: n
    real(kind(1d0)), intent(out):: res
    !Local variables
    integer         :: k
    real(kind(1d0)) :: b
    real(kind(1d0)) :: mx
    real(kind(1d0)) :: tc_sgn,tc,term,sum_val,rk,rn
    mx=-x
    b=a+1.d0
    res=0.d0
    if(x<=0.d0)return
    tc_sgn=1.d0-2.d0*dble(mod(n,2))
    laguerre_n_poly_safe=sf_taylorcoeff_e(n,x,tc)
    if(laguerre_n_poly_safe==SF_SUCCESS)then
       rn=dble(n)
       term=tc*tc_sgn
       sum_val=term
       do k=n-1, 0, -1
          rk=dble(k)
          term= term*((b+rk)/(rn-rk))*(rk+1.d0)/mx
          sum_val=sum_val+term
       enddo
       res=sum_val
    endif
    return
  end function laguerre_n_poly_safe


  real(kind(1d0)) function SF_laguerre(x,n,a,info)
    !Calcola L_n^k=(-)^kd^k/dx^k{e^x/n!d^n/dx^n(e^-x x^n)}
    implicit none
    !Input variables 
    real(kind(1d0)), intent(in) :: x,a
    integer        , intent(in) :: n
    integer        , intent(out):: info
    !Local variable and parameters
    real(kind(1d0))::lg2,lkm1,lk,lkp1,ri
    integer :: i
    SF_laguerre=0.d0;if(n<0.or.a<0.d0.or.x<0.d0)return
    if(n==0)then
       SF_laguerre=1.d0
       !elseif(n==1)then
       !   SF_laguerre=1.d0+a-x
    elseif(x==0.d0)then
       SF_laguerre=1.d0
       do i=1, n
          SF_laguerre=SF_laguerre*(a+dble(i))/dble(i)
       enddo
    else if(n<5.or.(x>0.d0.and.a<-n-1))then
       if(laguerre_n_cp(x,a,n,SF_laguerre)==SF_SUCCESS)return
       info=laguerre_n_poly_safe(x,a,n,SF_laguerre)
    else if(a>0.d0.or.(x>0.d0.and.a<-n-1))then
       lg2=sf_laguerre_2_e(a,x)
       lkm1=1.d0+a-x
       lk=lg2
       do i=2,n-1
          ri=dble(i)
          Lkp1=(-(ri+a)*Lkm1+(2.d0*ri+a+1.d0-x)*Lk)/(ri+1.d0)
          Lkm1=Lk
          Lk  =Lkp1
       enddo
       SF_laguerre=lk
       info=SF_SUCCESS
    else
       info=laguerre_n_poly_safe(x,a,n,SF_laguerre)
    endif
    return
  end function SF_laguerre


  integer function sf_taylorcoeff_e(n,x,res)
    implicit none
    !Input variables
    integer, intent(in) :: n
    real(kind(1d0)), intent(in) :: x
    real(kind(1d0)), intent(out):: res
    !Parameters
    real(kind(1d0)), parameter :: log2pi = SF_LNPI + SF_LN2
    !Local variables
    real(kind(1d0)) :: ln_test,rn
    integer         :: k
    sf_taylorcoeff_e=-1;if(n<0)return
    sf_taylorcoeff_e=-2;if(x<0.d0)return
    sf_taylorcoeff_e=SF_SUCCESS
    res=1.d0;if(n==0)return
    res=x;if(n==1)return
    res=0.d0;if(x==0.d0)return
    rn=dble(n)
    ln_test=rn*(log(x)+1.d0)+1.d0-(rn+0.5d0)*log(rn+1.d0)+0.5d0*log2pi
    if(ln_test<SF_LOG_DBL_MIN+1.d0)then
       sf_taylorcoeff_e=SF_UNDERFLOW
    else if(ln_test>SF_LOG_DBL_MAX-1.d0)then
       sf_taylorcoeff_e=SF_OVERFLOW
    else
       res=1.d0
       do k=1,n
          res=res*x/dble(k)
       enddo
       sf_taylorcoeff_e=SF_SUCCESS
    end if
    return
  end function sf_taylorcoeff_e


  real(kind(1d0)) function sf_laguerre_2_e(a,x)
    implicit none
    !Input variables
    real(kind(1d0)), intent(in) ::a,x
    !Local variables and parameters
    real(kind(1d0)) :: c0,c1,c2
    if(a==-2.d0)then
       sf_laguerre_2_e=0.5d0*x*x
    else
       c0 = 0.5d0*(2.d0+a)*(1.d0+a)
       c1 = -(2.d0+a)
       c2 = -0.5d0/(2.d0+a)
       sf_laguerre_2_e=c0+c1*x*(1.d0+c2*x)
    endif
    return
  end function sf_laguerre_2_e

  real(kind(1d0)) function sf_hydrogenic(r,n,l,Z)
    !Calcola le funzioni legate idrogenoidi.
    implicit none
    !Input variables
    integer        , intent(in) :: n,l
    real(kind(1d0)), intent(in) :: Z,r
    !Local variables
    real(kind(1d0)) :: A,norm,rho,rad,lag
    integer :: info
    sf_hydrogenic=0.d0
    if(n < 1 .or. l > n-1 .or. Z <= 0.d0 .or. r < 0.0)return
    A=2.d0*Z/dble(n)
    norm=R_norm(n,l,Z)
    rho=A*r
    rad=1.d0
    if(rho/=0.d0)then
       rad=exp(-0.5d0*rho)*exp(dble(l)*log(rho))
    elseif(l/=0)then
       rad=0.d0
    endif
    lag=SF_laguerre(rho,n-l-1,dble(2*l+1),info)
    sf_hydrogenic=norm*rad*lag
    return
  end function sf_hydrogenic

  real(kind(1d0)) function R_norm(n,l,Z)
    integer        , intent(in) :: n,l
    real(kind(1d0)), intent(in) :: Z
    R_norm=2.d0/dble(n*n)*exp(1.5d0*log(Z))*&
         exp(0.5d0*(lnfact(n-l-1)-lnfact(n+l)))
    return
  end function R_norm

  subroutine SF_Coulomb(xx,eta1,xlamda,fc,gc,fcp,gcp,ifail,ForceRCWF)  
    implicit none
    !
    !.. Input Variables
    !..
    real(kind(1d0)), intent(in) :: xx,eta1,xlamda
    real(kind(1d0)), intent(out):: fc,gc,fcp,gcp
    integer        , intent(out):: ifail
    logical        , intent(in) :: ForceRCWF  
    integer :: l
    external :: fcoul
    l=int(xlamda+0.1d0)
!!$    if(((eta1<3.d0.and.eta1>-80.d0).or.(xx<=1.d-2)).and.(.not.ForceRCWF))then
!!$       write(*,"(a)",advance="no") "." 
!!$       call flush(6)
!!$       call fcoul(l,eta1,xx,fc,gc,fcp,gcp,ifail)
!!$    else
       call RCWF(xx,eta1,l,fc,gc,fcp,gcp)
!!$    endif
    return
  end subroutine SF_Coulomb

  subroutine RCWF(rho,eta,l,fc,gc,fcp,gcp)
    !
    !.. Description
    !..
    ! Coulomb wavefunctions calculated at r = rho by the
    ! continued-fraction method of Steed. See:
    ! A. R. Barnett et al, Comput. Phys. Commun.  8, 377(1974) 
    ! A. R. Barnett      , Comput. Phys. Commun. 11, 141(1976) 
    !
    implicit none
    !
    !.. Input variables
    !..
    real(kind(1d0)), intent(in) :: rho,eta
    real(kind(1d0)), intent(out):: fc,gc,fcp,gcp
    integer        , intent(in) :: l
    !
    !.. Parameters
    !..
    real(kind(1d0)), parameter :: ACCURACY =   1.d-15
    real(kind(1d0)), parameter :: STEP     = 999.d0
    !
    !.. Local variables
    !..
    real(kind(1d0)) :: xll1,eta2,turn,ktrp,pmx,fp,rh
    real(kind(1d0)) :: rho2,pl,tf,dk,del,d,h,w,g,gp,f
    real(kind(1d0)) :: r3,p,q,ar,ai,br,bi,wi,dr,di
    real(kind(1d0)) :: dp,dq,t,s,h2,i2,etah,h2ll,rh2
    real(kind(1d0)) :: r,ktr,etar,tfp
    real(kind(1d0)) :: k,k1,k2,k3,k4,m1,m2,m3,m4
    integer         :: iflag

    r    = rho                                                        
    ktr  = 1                                                          
    xll1 = dble(l*(l+1))                                          
    eta2 = eta*eta                                                    
    turn = eta + sqrt(eta2 + xll1)                                    
    if(r.lt.turn.and.abs(eta).ge.1.0e-6) ktr = -1                     
    ktrp = ktr                                                        

    iflag=0
    outer : do
       do
          if(iflag==1)then
             r    = turn                                                       
             tf   = f                                                          
             tfp  = fp                                                         
             ktrp = 1     
          endif
          iflag=1
          do
             etar = eta*r                                                      
             rho2 =   r*r                                                      
             pl   = dble(l+1)                                            
             pmx  = pl + 0.5                                                   
             !continued fraction for fp(l)/f(l)
             !xl is f  xlprime is fp **
             fp  = eta/pl + pl/r                                               
             dk  = etar*2.0                                                    
             del = 0.0                                                         
             d   = 0.0                                                         
             f   = 1.0                                                         
             k   = (pl*pl - pl + etar)*(2.0*pl - 1.0)                          
             if(abs(pl*pl+pl+etar)>ACCURACY)exit
             r   = r + 1.0e-6                                                  
          enddo
          do
             h   = (pl*pl + eta2)*(1.0 - pl*pl)*rho2                           
             k   = k + dk + pl*pl*6.0                                          
             d   =  1.0/(d*h + k)                                              
             del =  del*(d*k - 1.0)                                            
             if(pl.lt.pmx) del = -r*(pl*pl + eta2)*(pl + 1.0)*d/pl             
             pl  = pl + 1.0                                                    
             fp  = fp + del                                                    
             if(d.lt.0.0) f = -f                                               
             if(pl.gt.20000.)then
                w  = 0.0
                g  = 0.0
                gp = 0.0
                exit outer
             endif

             if(abs(del)<abs(fp)*ACCURACY)exit
          enddo
          fp  = f*fp                                                        
          if(abs(ktrp+1.d0)>ACCURACY)exit
       enddo

       !repeat for r = turn if rho lt turn                                
       !now obtain p + i.q for l from continued fraction (32)          
       !real arithmetic to facilitate conversion to ibm using real*8      
       p  = 0.0                                                          
       q  = r - eta                                                      
       pl = 0.0                                                          
       ar = -(eta2 + xll1)                                               
       ai =   eta                                                        
       br = 2.0*q                                                        
       bi = 2.0                                                          
       wi = 2.0*eta                                                      
       dr =   br/(br*br + bi*bi)                                         
       di =  -bi/(br*br + bi*bi)                                         
       dp = -(ar*di + ai*dr)                                             
       dq =  (ar*dr - ai*di)                                             
       do
          p  =  p + dp                                                      
          q  =  q + dq                                                      
          pl = pl + 2.0                                                     
          ar = ar + pl                                                      
          ai = ai + wi                                                      
          bi = bi + 2.0                                                     
          d  = ar*dr - ai*di + br                                           
          di = ai*dr + ar*di + bi                                           
          t  = 1.0/(d*d + di*di)                                            
          dr =  t*d                                                         
          di = -t*di                                                        
          h  = br*dr - bi*di - 1.0                                          
          k  = bi*dr + br*di                                                
          t  = dp*h  - dq*k                                                 
          dq = dp*k  + dq*h                                                 
          dp = t                                                            
          if(pl>46000.)then
             w  = 0.0                                                          
             g  = 0.0                                                          
             gp = 0.0                                                          
             exit outer
          endif
          if(abs(dp)+abs(dq)<(abs(p)+abs(q))*ACCURACY)exit
       enddo
       p  = p/r                                                          
       q  = q/r                                                          
       !solve for fp,g,gp and normalise f  at l=l                      
       g  = (fp - p*f)/q                                                 
       gp = p*g - q*f                                                    
       w  = 1.0/sqrt(fp*g - f*gp)                                        
       g  = w*g                                                          
       gp = w*gp                                                         
       if(abs(ktr-1.d0)<ACCURACY)exit outer
       f  = tf                                                           
       fp = tfp                                                          
       !runge-kutta integration of g(l) and gp(l) inwards from turn 
       !            see fox and mayers 1968 pg 202                        
       r3 = 1.0/3.0d0                                                    
       h  = (rho - turn)/(STEP + 1.0)                                    
       h2 = 0.5d0*h                                                      
       i2 = int(STEP + 0.001)                                           
       etah = eta*h                                                      
       h2ll = h2*xll1                                                    
       s  = (etah + h2ll/r  )/r   - h2                                   
       do
          rh2= r + h2                                                       
          t  = (etah + h2ll/rh2)/rh2 - h2                                   
          k1 = h2*gp                                                        
          m1 =  s*g                                                         
          k2 = h2*(gp + m1)                                                 
          m2 =  t*(g  + k1)                                                 
          k3 =  h*(gp + m2)                                                 
          m3 =  t*(g  + k2)                                                 
          m3 =     m3 + m3                                                  
          k4 = h2*(gp + m3)                                                 
          rh = r + h                                                        
          s  = (etah + h2ll/rh )/rh  - h2                                   
          m4 =  s*(g + k3)                                                  
          g  = g  + (k1 + k2 + k2 + k3 + k4)*r3                             
          gp = gp + (m1 + m2 + m2 + m3 + m4)*r3                             
          r  = rh                                                           
          i2 = i2 - 1                                                       
          if(abs(gp).gt.1.d300)then
             w  = 0.0                                                          
             g  = 0.0                                                          
             gp = 0.0                                                          
             exit outer
          endif
          if(i2<0)exit
       enddo
       w  = 1.0/(fp*g - f*gp)                                            
       exit outer
    enddo outer

    !gc and gcp, stored values are r,s
    !renormalise fc,fcp                            
    gc =g                                       
    gcp=gp                                      
    fcp=fp*w                                    
    fc= f*w

    return                                                            

  end subroutine RCWF

  subroutine klein(xx,eta1,xlamda,fc,gc,fcp,gcp,ifail)  
    !
    ! Title                : KLEIN
    !
    ! Authors              : A.R. Barnett
    !
    ! Catalogue identifier : ABNJ_v1_0
    !
    ! Nature of problem:
    !   KLEIN computes relativistic Schrodinger (Klein-Gordon) 
    !   equation solutions, i.e. Coulomb functions for real 
    !   lambda > - 1, F lambda (eta,x), G lambda (eta,x), 
    !   F'lambda (eta,x) and G'lambda (eta,x) for real x > 0 
    !   and real eta, -10**4 < eta < 10**4. 
    !   Hence it is also suitable for Bessel and spherical 
    !   Bessel functions. Accuracies are in the range 
    !   1.d-14-1.d-16 in oscillating region, and approximately 
    !   1.d-30 on an extended precision compiler. The program 
    !   is suitable for generating Klein-Gordon wavefunctions 
    !   for matching in pion and kaon physics.
    !
    ! Solution method:
    !   An extended version of Steed's method used previously 
    !   for integer lambda in subroutine RCWFN, is adopted.
    !
    ! Restrictions:
    !   The standard version loses accuracy as x>xlamda (the 
    !   turning point) and eventually when G>~10**6 the 
    !   subroutine is ineffective; a JWKB approximation and 
    !   full information is output.
    !
    ! Suitable ACCURACY = 1.d-16 if 56-bit mantissa
    !                     1.d-14 if 48-bit mantissa  
    !
    ! References:
    ! A. R. Barnett, J. Comput. Appl. Math. 8,  29(1982)  
    ! A. R. Barnett, Comput. Phys. Commun. 24, 141(1981)
    ! A. R. Barnett, Comput. Phys. Commun. 21, 297(1981)
    ! A. R. Barnett, Comput. Phys. Commun. 11, 141(1976)
    ! Barnett et al, Comput. Phys. Commun.  8, 377(1974)
    !
    !
    ! Note:
    !      - l'identita` del Wronskiano e` garantita in precisione
    !        macchina per costruzione, dal momento che uno dei
    !        quattro valori e` determinato dagli altri proprio grazie
    !        a questa relazione. Non si puo` percio` sfruttare per
    !        testare la qualita` del calcolo.
    !
    ! Nota: e` parecchio piu` accurata di FCOUL, ma e` anche eccezionalmente
    !       piu` lenta, lusso che non ci possiamo permettere. Percio` fissiamo
    !       la precisione a 10^-11. Rispetto ad FCOUL non perdiamo praticamente
    !       di precisione e limiamo i difetti principali. 
    !
    implicit none
    !
    !.. Input Variables
    !..
    real(kind(1d0)), intent(in) :: xx,eta1,xlamda
    real(kind(1d0)), intent(out):: fc,gc,fcp,gcp
    integer        , intent(out):: ifail
    !
    !.. Parameters
    !..
    real(kind(1d0)), parameter :: ACCURACY=1.d-11
    !
    !.. Local Variables
    !..
    real(kind(1d0)) :: eta,acc,acc4,acch,gjwkb,x,xl,pk,e2ll1
    real(kind(1d0)) :: px,ek,f,pk1,d,df,p,tk,fjwkb,ta,q,br,bi
    real(kind(1d0)) :: wi,dr,di,dp,dq,c,a,b,gam,w,ar,ai,xi,fcl
    integer :: iexp
    logical :: xlturn 

    fc =0.d0
    fcp=0.d0
    gc =0.d0
    gcp=0.d0

    iexp=1
    eta=eta1
    acc=ACCURACY
    acc4=acc*1.d4
    acch=sqrt(acc) 
    gjwkb=0.d0 
    ifail=-1;if(abs(xx)<=acch)return
    x=xx
    if(xx<=0.d0)then
       ifail=-1;if(eta==0.d0)return
       x=-xx
       eta=-eta     
    endif
    xl=xlamda
    if(xl<-1.d0)xl=-xl-1.d0
    pk=xl+1.d0   
    e2ll1=eta*eta+xl*pk  
    xlturn=x*(x-2.d0*eta)<xl*pk  

    !Evaluate cf1=f=fprime(xl,x,eta)/f(xl,x,eta)     
    xi=1.d0/x  
    fcl=1.d0    
    px=pk+2.d4   
    do
       ek=eta/pk 
       f=(ek+pk*xi)*fcl+(fcl-1.d0)*xi   
       pk1=pk+1.d0    
       !Test ensures b1/=0.d0 
       !for negative eta: fixup is exact.     
       if(abs(eta*x+pk*pk1)>acc)exit
       fcl=(1.d0+ek*ek)/(1.d0+(eta/pk1)**2) 
       pk=2.d0+pk   
    enddo
    d=1.d0/((pk+pk1)*(xi+ek/pk1))     
    df=-fcl*(1.d0+ek*ek)*d     
    f=f+df
    if(fcl/=1.d0)fcl=-1.d0  
    if(d<0.d0)fcl=-fcl  

    !Begin cf1 loop on pk = k = lamda + 1    
    p=1.d0  
    do
       pk=pk1  
       pk1=pk1+1.d0  
       ek=eta/pk   
       tk=(pk+pk1)*(xi+ek/pk1)    
       d=tk-d*(1.d0+ek*ek)   
       if(abs(d)<=acch)then
          p=p+1.d0   
          ifail=1;if(p>2.d0)return
       endif
       d=1.d0/d
       if(d<0.d0)fcl=-fcl 
       df=df*(d*tk-1.d0)  
       f=f+df    
       ifail=1;if(pk>px)return
       if(abs(df)<abs(f)*acc)exit
    enddo
    if(xlturn) call jwkb(x,eta,xl,fjwkb,gjwkb,iexp) 
    if(iexp<=1.and.gjwkb<=1.d0/(acch*1.d2))then

       !evaluate cf2 = p + i.q again using steed's algorithm    
       ta=2.d0*2.d4     
       p =0.d0    
       q =1.d0-eta*xi  
       pk=0.d0    
       ar=-e2ll1  
       ai=eta     
       br=2.d0*(x-eta) 
       bi=2.d0     
       wi=2.d0*eta 
       dr= br/(br*br+bi*bi) 
       di=-bi/(br*br+bi*bi) 
       dp=-xi*(ar*di+ai*dr) 
       dq= xi*(ar*dr-ai*di) 
       do
          p=p+dp    
          q=q+dq    
          pk=pk+2.d0   
          ar=ar+pk    
          ai=ai+wi    
          bi=bi+2.d0   
          d=ar*dr-ai*di+br     
          di=ai*dr+ar*di+bi     
          c=1.d0/(d*d + di*di)
          dr=c*d 
          di=-c*di
          a=br*dr-bi*di-1.d0    
          b=bi*dr+br*di    
          c=dp*a-dq*b     
          dq=dp*b+dq*a     
          dp=c    
          ifail=2;if(pk>ta)return
          if(abs(dp)+abs(dq)<(abs(p)+abs(q))*acc)exit
       enddo

       !solve for fp,g,gp and normalise f at lamda = xl
       ifail=3;if(q<=acc4*abs(p))return
       gam=(f-p)/q    
       w=fcl/sqrt((f-p)*gam+q)    
       gc=w*gam  
       gcp=w*(p*gam-q)
    else
       w=fjwkb  
       gc=gjwkb  
       gcp=f*gc-1.d0/w 
    endif
    fc=w
    fcp=w*f    
    ifail=0
    return 
  end subroutine klein


  subroutine jwkb(xx,eta1,xl,fjwkb,gjwkb,iexp)    
    !
    !.. Description
    !..
    ! Computes JWKB approximations to Coulomb 
    ! functions for x >= 0 as modified by 
    ! Biedenharn et al., Phys. Rev. A 97, 542(1955). 
    implicit none
    !
    !.. Input variables
    !..
    real(kind(1d0)), intent(in ) :: xx, eta1, xl
    real(kind(1d0)), intent(out) :: fjwkb, gjwkb
    integer  , intent(out) :: iexp
    !
    !.. Parameters
    !..
    real(kind(1d0)), parameter :: ALOGE = 0.4342945d0
    real(kind(1d0)), parameter :: SOTCQ = .17142857142857142857d0
    !
    !.. Local variables
    !..
    real(kind(1d0)) :: x,eta,gh2,xll1,hll,hl,sl,rl2,gh,phi,phi10
    x=xx    
    eta=eta1  
    gh2=x*(eta+eta-x)
    xll1=max(xl*(xl+1.d0),0.d0)   
    if(gh2+xll1<=0.d0)return !Senza aver inizializzato?!
    hll=xll1+SOTCQ
    hl=sqrt(hll)    
    sl=eta/hl+hl/x     
    rl2=1.d0+eta*eta/hll
    gh=sqrt(gh2+hll)/x
    phi=x*gh-0.5d0*(hl*log((gh+sl)**2/rl2)-log(gh))
    if(eta/=0.d0)phi=phi-eta*atan2(x*gh,x-eta)    
    phi10=phi*ALOGE
    iexp=int(-phi10)    
    if(iexp>70)gjwkb=10.d0**(-phi10-dble(iexp)) 
    if(iexp<=70)then
       gjwkb=exp(-phi)
       iexp=0
    endif
    fjwkb=0.5d0/(gh*gjwkb)  
    return 
  end subroutine jwkb

  real(kind(1d0)) function SF_Hyperg_2F1(a_,b_,c_,x_,info)
    !Description:
    !Calcola la funzione ipergeometrica gaussiana
    !2F1, definita come
    !
    !$$
    !{_2F_1}(a,b;c;x)=\sum_n=0^\infty\frac{(a)_n(b)_n}{(c)_n}\frac{z^n}{n!}
    !$$
    !
    ![ Milton Abramowitz and Irene A. Stegun, 
    !  "Handbook of Mathematical Functions", ninth ed., par. 15.1.1 pag. 556
    !  (Dover Publications, Inc, Mineola, N.Y.]
    !
    !
    !Il presente codice e` un libero adattamento del modulo MHYGFX:
    !
    !>> from the book "computation of special functions"
    !>> by shanjie zhang and jianming jin
    !>> copyright 1996 by john wiley & sons, inc.
    !>> the authors state:
    !>> "however, we give permission to the reader who purchases this book
    !>> to incorporate any of these programs into his or her programs
    !>> provided that the copyright is acknowledged."
    !>> latest revision - 16 january 2002
    !>> corrections by alan miller (amiller @ bigpond.net.au)
    !>> in 4 places hw was used without being initialized.
    !
    !

    real(kind(1d0)), intent(in) :: a_
    real(kind(1d0)), intent(in) :: b_
    real(kind(1d0)), intent(in) :: c_
    real(kind(1d0)), intent(in) :: x_
    integer        , intent(out):: info

    real(kind(1d0)) :: a,b,c,x

    logical    :: l0, l1, l2, l3, l4, l5
    real(kind(1d0))  :: a0, aa, bb, c0, c1, eps, f0, f1, g0, g1, g2, g3,   &
         ga, gabc, gam, gb, gbm, gc, gca, gcb, gcab, gm, hw,  &
         pa, pb, r, r0, r1, rm, rp, sm, sp, sp0, x1
    integer    :: j, k, m, nm
    a=a_
    b=b_
    c=c_
    x=x_

    info=0
    SF_Hyperg_2F1=0.d0
    l0 = c == int(c) .and. c < 0.0
    l1 = 1.0d0 - x < 1.0d-15 .and. c - a - b <= 0.0
    l2 = a == int(a) .and. a < 0.0
    l3 = b == int(b) .and. b < 0.0
    l4 = c - a == int(c-a) .and. c - a <= 0.0
    l5 = c - b == int(c-b) .and. c - b <= 0.0
    if (l0 .or. l1) then
       !the hypergeometric series is divergent
       info=-1
       return
    end if
    eps = 1.0d-15
    if (x > 0.95) eps = 1.0d-8
    if (x == 0.0 .or. a == 0.0.or.b == 0.0) then
       SF_Hyperg_2F1 = 1.0d0
       return
    else if (1.0d0-x == eps .and. c-a-b > 0.0) then
       call gamma_ZhangJin(c,gc)
       call gamma_ZhangJin(c-a-b,gcab)
       call gamma_ZhangJin(c-a,gca)
       call gamma_ZhangJin(c-b,gcb)
       SF_Hyperg_2F1 = gc * gcab / (gca*gcb)
       return
    else if (1.0d0+x <= eps .and. abs(c-a+b-1.0) <= eps) then
       g0 = sqrt(SF_PI) * 2.0d0 ** (-a)
       call gamma_ZhangJin(c,g1)
       call gamma_ZhangJin(1.0d0+a/2.0-b,g2)
       call gamma_ZhangJin(0.5d0+0.5*a,g3)
       SF_Hyperg_2F1 = g0 * g1 / (g2*g3)
       return
    else if (l2 .or. l3) then
       if (l2) nm = int(abs(a))
       if (l3) nm = int(abs(b))
       SF_Hyperg_2F1 = 1.0d0
       r = 1.0d0
       do  k = 1, nm
          r = r * (a+k-1.0d0) * (b+k-1.0d0) / (k*(c+k-1.0d0)) * x
          SF_Hyperg_2F1 = SF_Hyperg_2F1 + r
       end do
       return
    else if (l4 .or. l5) then
       if (l4) nm = int(abs(c-a))
       if (l5) nm = int(abs(c-b))
       SF_Hyperg_2F1 = 1.0d0
       r = 1.0d0
       do  k = 1, nm
          r = r * (c-a+k-1.0d0) * (c-b+k-1.0d0) / (k*(c+k-1.0d0)) * x
          SF_Hyperg_2F1 = SF_Hyperg_2F1 + r
       end do
       SF_Hyperg_2F1 = (1.0d0-x) ** (c-a-b) * SF_Hyperg_2F1
       return
    end if
    aa = a
    bb = b
    x1 = x
    if (x < 0.0d0) then
       x = x / (x-1.0d0)
       if (c > a .and. b < a.and.b > 0.0) then
          a = bb
          b = aa
       end if
       b = c - b
    end if
    if (x >= 0.75d0) then
       gm = 0.0d0
       if (abs(c-a-b-int(c-a-b)) < 1.0d-15) then
          m = int(c-a-b)
          call gamma_ZhangJin(a,ga)
          call gamma_ZhangJin(b,gb)
          call gamma_ZhangJin(c,gc)
          call gamma_ZhangJin(a+m,gam)
          call gamma_ZhangJin(b+m,gbm)
          call psi_ZhangJin(a,pa)
          call psi_ZhangJin(b,pb)
          if (m /= 0) gm = 1.0d0
          do  j = 1, abs(m) - 1
             gm = gm * j
          end do
          rm = 1.0d0
          do  j = 1, abs(m)
             rm = rm * j
          end do
          f0 = 1.0d0
          r0 = 1.0d0
          r1 = 1.0d0
          sp0 = 0.d0
          sp = 0.0d0
          if (m >= 0) then
             c0 = gm * gc / (gam*gbm)
             c1 = -gc * (x-1.0d0) ** m / (ga*gb*rm)
             do  k = 1, m - 1
                r0 = r0 * (a+k-1.0d0) * (b+k-1.0) / (k*(k-m)) * (1.0-x)
                f0 = f0 + r0
             end do
             do  k = 1, m
                sp0 = sp0 + 1.0d0 / (a+k-1.0) + 1.0 / (b+k-1.0) - 1.0 / k
             end do
             f1 = pa + pb + sp0 + 2.0d0 * SF_EULER + log(1.0d0-x)
             hw = f1
             do  k = 1, 250
                sp = sp + (1.0d0-a) / (k*(a+k-1.0)) + (1.0-b) / (k*(b+k- 1.0))
                sm = 0.0d0
                do  j = 1, m
                   sm = sm + (1.0d0-a) / ((j+k)*(a+j+k-1.0)) + 1.0 / (b+j+k -1.0)
                end do
                rp = pa + pb + 2.0d0 * SF_EULER + sp + sm + log(1.0d0-x)
                r1 = r1 * (a+m+k-1.0d0) * (b+m+k-1.0) / (k*(m+k)) * (1.0-x )
                f1 = f1 + r1 * rp
                if (abs(f1-hw) < abs(f1)*eps) exit
                hw = f1
             end do
             SF_Hyperg_2F1 = f0 * c0 + f1 * c1
          else if (m < 0) then
             m = -m
             c0 = gm * gc / (ga*gb*(1.0d0-x)**m)
             c1 = -(-1) ** m * gc / (gam*gbm*rm)
             do  k = 1, m - 1
                r0 = r0 * (a-m+k-1.0d0) * (b-m+k-1.0) / (k*(k-m)) * (1.0-x )
                f0 = f0 + r0
             end do
             do  k = 1, m
                sp0 = sp0 + 1.0d0 / k
             end do
             f1 = pa + pb - sp0 + 2.0d0 * SF_EULER + log(1.0d0-x)
             hw = f1
             do  k = 1, 250
                sp = sp + (1.0d0-a) / (k*(a+k-1.0)) + (1.0-b) / (k*(b+k- 1.0))
                sm = 0.0d0
                do  j = 1, m
                   sm = sm + 1.0d0 / (j+k)
                end do
                rp = pa + pb + 2.0d0 * SF_EULER + sp - sm + log(1.0d0-x)
                r1 = r1 * (a+k-1.0d0) * (b+k-1.0) / (k*(m+k)) * (1.0-x)
                f1 = f1 + r1 * rp
                if (abs(f1-hw) < abs(f1)*eps) exit
                hw = f1
             end do
             SF_Hyperg_2F1 = f0 * c0 + f1 * c1
          end if
       else
          call gamma_ZhangJin(a,ga)
          call gamma_ZhangJin(b,gb)
          call gamma_ZhangJin(c,gc)
          call gamma_ZhangJin(c-a,gca)
          call gamma_ZhangJin(c-b,gcb)
          call gamma_ZhangJin(c-a-b,gcab)
          call gamma_ZhangJin(a+b-c,gabc)
          c0 = gc * gcab / (gca*gcb)
          c1 = gc * gabc / (ga*gb) * (1.0d0-x) ** (c-a-b)
          SF_Hyperg_2F1 = 0.0d0
          hw = SF_Hyperg_2F1
          r0 = c0
          r1 = c1
          do  k = 1, 250
             r0 = r0 * (a+k-1.0d0) * (b+k-1.0) / (k*(a+b-c+k)) * (1.0-x)
             r1 = r1 * (c-a+k-1.0d0) * (c-b+k-1.0) / (k*(c-a-b+k)) * (1.0 -x)
             SF_Hyperg_2F1 = SF_Hyperg_2F1 + r0 + r1
             if (abs(SF_Hyperg_2F1-hw) < abs(SF_Hyperg_2F1)*eps) exit
             hw = SF_Hyperg_2F1
          end do
          SF_Hyperg_2F1 = SF_Hyperg_2F1 + c0 + c1
       end if
    else
       a0 = 1.0d0
       if (c > a .and. c < 2.0d0*a .and. c > b .and. c < 2.0d0*b) then
          a0 = (1.0d0-x) ** (c-a-b)
          a = c - a
          b = c - b
       end if
       SF_Hyperg_2F1 = 1.0d0
       hw = SF_Hyperg_2F1
       r = 1.0d0
       do  k = 1, 250
          r = r * (a+k-1.0d0) * (b+k-1.0d0) / (k*(c+k-1.0d0)) * x
          SF_Hyperg_2F1 = SF_Hyperg_2F1 + r
          if (abs(SF_Hyperg_2F1-hw) <= abs(SF_Hyperg_2F1)*eps) exit
          hw = SF_Hyperg_2F1
       end do
       SF_Hyperg_2F1 = a0 * SF_Hyperg_2F1
    end if
    if (x1 < 0.0d0) then
       x = x1
       c0 = 1.0d0 / (1.0d0-x) ** aa
       SF_Hyperg_2F1 = c0 * SF_Hyperg_2F1
    end if
    a = aa
    b = bb
    if (k > 120)info=2!warning! you should check the accuracy
    return
  end function SF_Hyperg_2F1

  subroutine gamma_ZhangJin(x, ga)
    real(kind(1d0)), intent(in)      :: x
    real(kind(1d0)), intent(out)     :: ga
    real(kind(1d0)), parameter  :: g(26) = (/  &
         1.0d0, 0.5772156649015329d0, -0.6558780715202538d0,  &
         -0.420026350340952d-1, 0.1665386113822915d0,   &
         -0.421977345555443d-1, -.96219715278770d-2,  &
         .72189432466630d-2, -.11651675918591d-2, -.2152416741149d-3,  &
         .1280502823882d-3, -.201348547807d-4, -.12504934821d-5,  &
         .11330272320d-5, -.2056338417d-6, .61160950d-8,  &
         .50020075d-8, -.11812746d-8, .1043427d-9, .77823d-11,  &
         -.36968d-11, .51d-12, -.206d-13, -.54d-14, .14d-14, .1d-15 /)
    real(kind(1d0))  :: gr, r, z
    integer    :: k, m, m1

    if (x == int(x)) then
       if (x > 0.0d0) then
          ga = 1.0d0
          m1 = x - 1
          do  k = 2, m1
             ga = ga * k
          end do
       else
          ga = 1.0d+300
       end if
    else
       if (abs(x) > 1.0d0) then
          z = abs(x)
          m = int(z)
          r = 1.0d0
          do  k = 1, m
             r = r * (z-k)
          end do
          z = z - m
       else
          z = x
       end if
       gr = g(26)
       do  k = 25, 1, -1
          gr = gr * z + g(k)
       end do
       ga = 1.0d0 / (gr*z)
       if (abs(x) > 1.0d0) then
          ga = ga * r
          if (x < 0.0d0) ga = -SF_PI / (x*ga*sin(SF_PI*x))
       end if
    end if
    return
  end subroutine gamma_ZhangJin



  subroutine psi_ZhangJin(x, ps)

    !       ======================================
    !       purpose: compute psi function
    !       input :  x  --- argument of psi(x)
    !       output:  ps --- psi(x)
    !       ======================================

    real(kind(1d0)), intent(in)      :: x
    real(kind(1d0)), intent(out)     :: ps

    real(kind(1d0))  :: a1, a2, a3, a4, a5, a6, a7, a8, s, x2, xa
    integer    :: k, n

    xa = abs(x)
    s = 0.0d0
    if (x == int(x) .and. x <= 0.0) then
       ps = 1.0d+300
       return
    else if (xa == int(xa)) then
       n = xa
       do  k = 1, n - 1
          s = s + 1.0d0 / k
       end do
       ps = -SF_EULER + s
    else if (xa+.5 == int(xa+.5)) then
       n = xa - .5
       do  k = 1, n
          s = s + 1.d0/dble(2*k-1)
       end do
       ps = -SF_EULER + 2.0d0 * s - 1.386294361119891d0
    else
       if (xa < 10.0) then
          n = 10 - int(xa)
          do  k = 0, n - 1
             s = s + 1.0d0 / (xa + k)
          end do
          xa = xa + n
       end if
       x2 = 1.0d0 / (xa*xa)
       a1 = -.8333333333333d-01
       a2 = .83333333333333333d-02
       a3 = -.39682539682539683d-02
       a4 = .41666666666666667d-02
       a5 = -.75757575757575758d-02
       a6 = .21092796092796093d-01
       a7 = -.83333333333333333d-01
       a8 = .4432598039215686d0
       ps = log(xa) - .5d0 / xa + x2 * (((((((a8*x2 + a7)*x2 + a6)*x2 + a5)*  &
            x2 + a4)*x2 + a3)*x2 + a2)*x2 + a1)
       ps = ps - s
    end if
    if (x < 0.0) ps = ps - SF_PI * cos(SF_PI*x) / sin(SF_PI*x) - 1.0d0 / x
    return
  end subroutine psi_ZhangJin

  subroutine crash()
    integer, allocatable :: a(:)
    deallocate(a)
  end subroutine crash

end module specfun

