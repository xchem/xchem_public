!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module ModuleSymmetricElectronicSpaceOperators

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleMatrix
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleSymmetricLocalizedStates
  use ModuleShortRangeChannel
  use ModulePartialWaveChannel
  use ModuleSymmetricElectronicSpace
  use ModuleElectronicSpace
  use ModuleShortRangeOrbitals
  use ModuleGroups
  use ModuleElementaryElectronicSpaceOperators

  implicit none

  character(len=*), parameter :: RowsLabel = 'ROWS'
  character(len=*), parameter :: ColsLabel = 'COLUMNS'
  logical         , parameter :: DoBsPermutation = .true.
  
  
  !> Class of the symmetric electronic space blocks, devoted to manage the matrices: S, H, D, ... defined in this space.
  type, public :: ClassSESSESBlock
     ! {{{ private attributes
     private
     !***
     !> Space of the Bra functions.
     type(ClassSymmetricElectronicSpace), pointer :: SpaceBra
     !> Space of the Ket functions.
     type(ClassSymmetricElectronicSpace), pointer :: SpaceKet
     type(LocLocBlock),allocatable                :: LocLocBlock(:,:)
     type(LocSRCBlock),allocatable                :: LocSRCBlock(:,:)
     type(LocPWCBlock),allocatable                :: LocPWCBlock(:,:)
     type(SRCLocBlock),allocatable                :: SRCLocBlock(:,:)
     type(SRCSRCBlock),allocatable                :: SRCSRCBlock(:,:)
     type(SRCPWCBlock),allocatable                :: SRCPWCBlock(:,:)
     type(PWCLocBlock),allocatable                :: PWCLocBlock(:,:)
     type(PWCSRCBlock),allocatable                :: PWCSRCBlock(:,:)
     type(PWCPWCBlock),allocatable                :: PWCPWCBlock(:,:)
     logical                                      :: BoxStatesOnly      = .FALSE.
     logical                                      :: BraBoxStatesOnly   = .FALSE.
     logical                                      :: KetBoxStatesOnly   = .FALSE.
     logical                                      :: AsympBsPermutation = .FALSE.
     logical                                      :: FormattedWrite     = .FALSE.
     logical                                      :: AvailableBlocks    = .FALSE.
     logical                                      :: DecoupleChannels   = .FALSE.
     ! }}}
     !
   contains
     !
     generic, public :: Init                    => ClassSESSESBlockInit
     generic, public :: InitAndSave             => ClassSESSESBlockInitAndSave
     generic, public :: InitAndSavePWCOnly      => ClassSESSESBlockInitAndSavePWCOnly
     generic, public :: InitAndSaveLocSRCOnly   => ClassSESSESBlockInitAndSaveLocSRCOnly
     generic, public :: Load                    => ClassSESSESBlockLoad
     generic, public :: LoadQC                  => ClassSESSESBlockLoadQC
     generic, public :: LoadLoc                 => ClassSESSESBlockLoadLoc
     generic, public :: LoadPoly                => ClassSESSESBlockLoadPoly
     generic, public :: LoadMonoIon             => ClassSESSESBlockLoadMonoIon
     generic, public :: LoadMonoIonPoly         => ClassSESSESBlockLoadMonoIonPoly
     generic, public :: LoadMonoIonPolyCondition=> ClassSESSESBlockLoadMonoIonPolyCondition
     generic, public :: LoadMonoIonPolyLastBs   => ClassSESSESBlockLoadMonoIonPolyLastBs
     generic, public :: LoadMonoIonPolyLastBsCondition   => ClassSESSESBlockLoadMonoIonPolyLastBsCondition
     generic, public :: GetMonoIonPolyLastBs    => ClassSESSESBlockGetMonoIonPolyLastBs
     generic, public :: Save                    => ClassSESSESBlockSave
     generic, public :: Assemble                => ClassSESSESBlockAssemble
     generic, public :: AssembleQC              => ClassSESSESBlockAssembleQC
     generic, public :: AssembleLoc             => ClassSESSESBlockAssembleLoc
     generic, public :: AssemblePoly            => ClassSESSESBlockAssemblePoly
     generic, public :: AssembleChCh            => ClassSESSESBlockAssembleChCh
     generic, public :: AssembleChChLastBs      => ClassSESSESBlockAssembleChChLastBs
     generic, public :: Condition               => ClassSESSESBlockCondition
     generic, public :: SetDecoupleChannels     => ClassSESSESBlockSetDecoupleChannels
     generic, public :: UnsetDecoupleChannels   => ClassSESSESBlockUnsetDecoupleChannels
     generic, public :: SetBoxOnly              => ClassSESSESBlockSetBoxOnly
     generic, public :: SetBraBoxOnly           => ClassSESSESBlockSetBraBoxOnly
     generic, public :: SetKetBoxOnly           => ClassSESSESBlockSetKetBoxOnly
     generic, public :: UnsetBoxOnly            => ClassSESSESBlockUnsetBoxOnly
     generic, public :: UnsetBraBoxOnly         => ClassSESSESBlockUnsetBraBoxOnly
     generic, public :: UnsetKetBoxOnly         => ClassSESSESBlockUnsetKetBoxOnly
     generic, public :: SetAsympBsPermutation   => ClassSESSESBlockSetAsympBsPermutation
     generic, public :: UnsetAsympBsPermutation => ClassSESSESBlockUnsetAsympBsPermutation
     generic, public :: SetFormattedWrite       => ClassSESSESBlockSetFormattedWrite
     generic, public :: UnsetFormattedWrite     => ClassSESSESBlockUnsetFormattedWrite
     generic, public :: IsDecoupleChannels      => ClassSESSESBlockIsDecoupleChannels
     generic, public :: IsBoxOnly               => ClassSESSESBlockIsBoxOnly
     generic, public :: IsBraBoxOnly            => ClassSESSESBlockIsBraBoxOnly
     generic, public :: IsKetBoxOnly            => ClassSESSESBlockIsKetBoxOnly
     generic, public :: BlockIsAvailable        => ClassSESSESBlockIsAvailable
     generic, public :: GetStorageDir           => ClassSESSESBlockGetStorageDir
     generic, public :: GetNumFunQC             => ClassSESSESBlockGetNumFunQC
     generic, public :: GetMinNumNotOvBs        => ClassSESSESBlockGetMinNumNotOvBs
     generic, public :: Free                    => ClassSESSESBlockFree
     ! {{{ private procedures

     procedure, private :: ClassSESSESBlockIsDecoupleChannels
     procedure, private :: ClassSESSESBlockSetDecoupleChannels
     procedure, private :: ClassSESSESBlockUnsetDecoupleChannels
     procedure, private :: ClassSESSESBlockInit
     procedure, private :: ClassSESSESBlockInitAndSave
     procedure, private :: ClassSESSESBlockInitAndSavePWCOnly
     procedure, private :: ClassSESSESBlockInitAndSaveLocSRCOnly
     procedure, private :: ClassSESSESBlockAssemble
     procedure, private :: ClassSESSESBlockAssembleQC
     procedure, private :: ClassSESSESBlockAssembleLoc
     procedure, private :: ClassSESSESBlockAssemblePoly
     procedure, private :: ClassSESSESBlockAssembleChCh
     procedure, private :: ClassSESSESBlockAssembleChChLastBs
     procedure, private :: ClassSESSESBlockCondition
     procedure, private :: ClassSESSESBlockLoad
     procedure, private :: ClassSESSESBlockLoadQC
     procedure, private :: ClassSESSESBlockLoadLoc
     procedure, private :: ClassSESSESBlockLoadPoly
     procedure, private :: ClassSESSESBlockLoadMonoIon
     procedure, private :: ClassSESSESBlockLoadMonoIonPoly
     procedure, private :: ClassSESSESBlockLoadMonoIonPolyCondition
     procedure, private :: ClassSESSESBlockLoadMonoIonPolyLastBs
     procedure, private :: ClassSESSESBlockLoadMonoIonPolyLastBsCondition
     procedure, private :: ClassSESSESBlockGetMonoIonPolyLastBs
     procedure, private :: ClassSESSESBlockSave
     procedure, private :: ClassSESSESBlockSetBoxOnly
     procedure, private :: ClassSESSESBlockSetBraBoxOnly
     procedure, private :: ClassSESSESBlockSetKetBoxOnly
     procedure, private :: ClassSESSESBlockUnsetBoxOnly
     procedure, private :: ClassSESSESBlockUnsetBraBoxOnly
     procedure, private :: ClassSESSESBlockUnsetKetBoxOnly
     procedure, private :: ClassSESSESBlockSetAsympBsPermutation
     procedure, private :: ClassSESSESBlockUnsetAsympBsPermutation
     procedure, private :: ClassSESSESBlockSetFormattedWrite
     procedure, private :: ClassSESSESBlockUnsetFormattedWrite
     procedure, private :: ClassSESSESBlockIsBoxOnly
     procedure, private :: ClassSESSESBlockIsBraBoxOnly
     procedure, private :: ClassSESSESBlockIsKetBoxOnly
     procedure, private :: ClassSESSESBlockIsAvailable
     procedure, private :: ClassSESSESBlockGetStorageDir
     procedure, private :: ClassSESSESBlockGetNumFunQC
     procedure, private :: ClassSESSESBlockGetMinNumNotOvBs
     procedure, private :: ClassSESSESBlockFree
     final              :: ClassSESSESBlockFinal

     ! }}}
  end type ClassSESSESBlock

  

  public :: ValidSymmetries
  public :: CheckOperator
  public :: CheckGauge
  public :: CheckAxis
  public :: GetBlockStorageDir
  public :: SetLabel




contains



  function GetBlockStorageDir( SpaceBra, SpaceKet ) result(Dir)
    class(ClassSymmetricElectronicSpace), intent(in) :: SpaceBra
    class(ClassSymmetricElectronicSpace), intent(in) :: SpaceKet
    character(len=:), allocatable :: Dir
    !
    character(len=:), allocatable :: NucConfDir, CCDir, BraSymLabel, KetSymLabel
    !
    allocate( NucConfDir , source = SpaceBra%GetStorageDir() )
    allocate( CCDir      , source = GetCloseCouplingDir()    )
    allocate( BraSymLabel, source = SpaceBra%GetLabel()      )
    allocate( KetSymLabel, source = SpaceKet%GetLabel()      )
    !
    allocate( Dir, source = &
         AddSlash(NucConfDir)//&
         AddSlash(CCDir)//&
         BraSymLabel//'_'//AddSlash(KetSymLabel) )
    !
  end function GetBlockStorageDir




  subroutine CheckOperator( OperatorLabel )
    character(len=:), allocatable, intent(inout) :: OperatorLabel
    if ( OperatorLabel .is. OverlapLabel ) then
       call SetLabel( OverlapLabel, OperatorLabel )
    elseif ( OperatorLabel .is. HamiltonianLabel ) then
       call SetLabel( HamiltonianLabel, OperatorLabel )
    elseif ( OperatorLabel .is. KinEnergyLabel ) then
       call SetLabel( KinEnergyLabel, OperatorLabel )
    elseif ( OperatorLabel .is. DipoleLabel ) then
       call SetLabel( DipoleLabel, OperatorLabel )
    elseif ( OperatorLabel .is. CAPLabel ) then
       call SetLabel( CAPLabel, OperatorLabel )
    else
       call Assert( 'Invalid operator label, it must be one of this: '//&
            OverlapLabel//', '//&
            HamiltonianLabel//', '//&
            KinEnergyLabel//', '//&
            DipoleLabel//' or '//&
            CAPLabel//'.' )
    end if
  end subroutine CheckOperator


  subroutine CheckGauge( Gauge )
    character(len=:), allocatable, intent(inout) :: Gauge
    if ( Gauge .is. 'L' ) then
       call SetLabel( LengthLabel, Gauge )
    elseif ( Gauge .is. 'V' ) then
       call SetLabel( VelocityLabel, Gauge )
    else
       call Assert( 'Invalid gauge label, it must be "l" or "v".' )
    end if
  end subroutine CheckGauge



  subroutine CheckAxis( DipoleAxis )
    character(len=*), intent(in) :: DipoleAxis
    if ( (DipoleAxis .isnt. 'X') .and. &
         (DipoleAxis .isnt. 'Y') .and. &
         (DipoleAxis .isnt. 'Z') ) &
         call Assert( 'Invalid dipole orientation, it must be either "x", "y" or "z".' )
  end subroutine CheckAxis



  subroutine SetLabel( NewLabel, OldLabel )
    character(len=*),              intent(in)    :: NewLabel
    character(len=:), allocatable, intent(inout) :: OldLabel
    if ( allocated(OldLabel) ) deallocate( OldLabel )
    allocate( OldLabel, source = NewLabel )
  end subroutine SetLabel



  logical function ValidSymmetries( OpLabel, BraSymSpace, KetSymSpace, Axis ) result(ValidSym)
    character(len=*)                            , intent(in) :: OpLabel
    class(ClassSymmetricElectronicSpace), target, intent(in) :: BraSymSpace
    class(ClassSymmetricElectronicSpace), target, intent(in) :: KetSymSpace    
    character(len=:) , allocatable              , intent(inout) :: Axis
    type(ClassIrrep), pointer :: BraIrrep, KetIrrep
    type(ClassIrrep) :: OpIrrep
    type(ClassGroup), pointer :: Group
    !
    ValidSym = .false.
    !
    BraIrrep => BraSymSpace%GetIrrep()
    KetIrrep => KetSymSpace%GetIrrep()
    Group    => BraIrrep%GetGroup()
    OpIrrep  =  GetOperatorIrrep( Group, OpLabel, Axis )
    !
    if ( ValidIrreps(BraIrrep,OpIrrep,KetIrrep) ) ValidSym = .true.
    !
  end function ValidSymmetries


  !> Rearrange the B-splines in such a way that those that do not
  !! overlap with the diffuse Gaussian are reorder in increasing
  !! number of B-splines' index, for all the channels. Let's suppose
  !! that from a set of 100 B-splines tha last 50 do not overlap with
  !! the diffuse orbitals, and that we have 3 PWC. If we identify the
  !! B-splines as \f$Bs_{i}^{j}\f$, where \f$i\f$ is the function
  !! index and \f$j\f$ corresponds to the channel, originally, the
  !! B-splines are organized as follows:
  !!
  !! \f$Bs_{1}^{1}, Bs_{2}^{1}, ..., Bs_{100}^{1}, 
  !! Bs_{1}^{2}, Bs_{2}^{2}, ..., Bs_{100}^{2},
  !! Bs_{1}^{3}, Bs_{2}^{3}, ..., Bs_{100}^{3}, ...\f$.
  !!
  !! After transformation the order is the following:
  !!
  !! \f$Bs_{1}^{1}, Bs_{2}^{1}, ..., Bs_{50}^{1},  
  !! Bs_{1}^{2}, Bs_{2}^{2}, ..., Bs_{50}^{2},
  !! Bs_{1}^{3}, Bs_{2}^{3}, ..., Bs_{50}^{3},
  !! Bs_{51}^{1}, Bs_{51}^{2}, ..., Bs_{51}^{3}, 
  !! Bs_{52}^{1}, Bs_{52}^{2}, ..., Bs_{52}^{3}, ...
  !! Bs_{100}^{1}, Bs_{100}^{2}, ..., Bs_{100}^{3}\f$ 
  !!
  subroutine PermuteBsplines( FullMat, SubSpacesMat, SubSpacesBlock, Which )
    !> ClassMatrix build from the SubSpacesMat array.
    class(ClassMatrix), intent(inout) :: FullMat
    !> Array of ClassMatrix containing all the blocks for a definite
    !! kind of sub-spaces, i.e.: all the < loc | O | pwc Xlm >, or
    !! < pwc Xlm | O | pwc Xlm >, etc.
    class(ClassMatrix), intent(in)    :: SubSpacesMat(:,:)
    !> Array of a general sub-spaces block i.e.: 
    !! LocPWCBlock, PWCPWCBlock, etc.
    class(*), target  , intent(in)    :: SubSpacesBlock(:,:)
    !> Spacifies whether the rows or the columns will be swapped.
    character(len=*)  , intent(in)    :: Which
    !
    integer :: MinNumNotOverlapingBs
    integer :: NumOverlapingBs, NTotOvBs
    integer :: i, N, j
    integer :: OldIndexBeg, NewIndexBeg, OldIndexEnd, NewIndexEnd
    !XCHEMQC: define some temp variables to be used for the sake of 
    !... more economical memory usage. We started seeing problems here
    !... for comparatively large boxes.
    integer :: nRows, nCols
    type(ClassMatrix) :: CopyFullMat
    real(kind(1d0)), allocatable :: NewArray(:,:), OldArray(:,:)
    character*32, allocatable :: OldRowLabels(:), OldColumnLabels(:)
    character*32, allocatable :: NewRowLabels(:), NewColumnLabels(:)
    logical :: RowLabelPermute
    logical :: ColLabelPermute
    !
    if ( (Which .isnt. RowsLabel) .and. &
         (Which .isnt. ColsLabel) ) call Assert( &
         "Invalid parameter for permutation. "//&
         "It must be "//RowsLabel//" or "//ColsLabel//". " )
    !
    if ( (size(SubSpacesMat,1) /= size(SubSpacesBlock,1)) .or. &
         (size(SubSpacesMat,2) /= size(SubSpacesBlock,2)) ) call Assert( &
         'The dimensions of the matrix array and the block array must be the same.' )
    !
    CopyFullMat = FullMat
    call FullMat%Free()
    !XCHEMQC create backup of RowLabeld
    RowLabelPermute = .false.
    ColLabelPermute = .false.
    if (CopyFullMat%RowsLabelled() ) then
        call CopyFullMat%GetRowLabels( OldRowLabels ) 
        allocate(NewRowLabels(size(OldRowLabels)))
        NewRowLabels = ""
        RowLabelPermute = .true.
    endif
    !XCHEMQC create backup of ColumnLabels
    if (CopyFullMat%ColumnsLabelled() ) then
        call CopyFullMat%GetColumnLabels( OldColumnLabels ) 
        allocate(NewColumnLabels(size(OldColumnLabels)))
        NewColumnLabels = ""
        ColLabelPermute = .true.
    endif
    !END XCHEMQC
    call CopyFullMat%FetchMatrix(OldArray)
    !XCHEMQC: use new variables to be able to Free superfluous CopyFullMat
    !... before allocating NewArray
    nRows = CopyFullMat%NRows()
    nCols = CopyFullMat%NColumns()
    call CopyFullMat%Free()
    allocate( NewArray(nRows, nCols) )
    !allocate( NewArray(CopyFullMat%NRows(),CopyFullMat%NColumns()) )
    !END XCHEMQC
    NewArray = 0.d0
    !
    MinNumNotOverlapingBs = GetMinNumNotOverlapingBs( SubSpacesBlock, Which )
    !
    !
    if ( Which .is. RowsLabel ) then
       N = size(SubSpacesMat,1)
    else
       N = size(SubSpacesMat,2)
    end if
    
    
    !.. Permutation of the overlapping B-splines
    OldIndexBeg = 1
    NewIndexBeg = 1
    !
    NTotOvBs = 0
    do i = 1, N
       !
       if ( Which .is. RowsLabel ) then
          NumOverlapingBs = SubSpacesMat(i,1)%NRows() - MinNumNotOverlapingBs
       else
          NumOverlapingBs = SubSpacesMat(1,i)%NColumns() - MinNumNotOverlapingBs
       end if
       !
       NTotOvBs = NTotOvBs + NumOverlapingBs
       OldIndexEnd = OldIndexBeg + NumOverlapingBs - 1
       NewIndexEnd = NewIndexBeg + NumOverlapingBs - 1
       !
       if ( Which .is. RowsLabel ) then
          NewArray(NewIndexBeg:NewIndexEnd,:) = OldArray(OldIndexBeg:OldIndexEnd,:)
          !XCHEMQC include permutation of labels
          if (RowLabelPermute) NewRowLabels(NewIndexBeg:NewIndexEnd) = OldRowLabels(OldIndexBeg:OldIndexEnd)
          !END XCHEMQC
          OldIndexBeg = OldIndexBeg + SubSpacesMat(i,1)%NRows()
       else
          NewArray(:,NewIndexBeg:NewIndexEnd) = OldArray(:,OldIndexBeg:OldIndexEnd)
          !XCHEMQC include permutation of labels
          if (ColLabelPermute) NewColumnLabels(NewIndexBeg:NewIndexEnd) = OldColumnLabels(OldIndexBeg:OldIndexEnd)
          !END XCHEMQC
          OldIndexBeg = OldIndexBeg + SubSpacesMat(1,i)%NColumns()
       end if
       !
       NewIndexBeg = NewIndexEnd + 1
       !
    end do
    
    
    !.. 1st step in the permutation of non-overlapping B-splines
    if ( Which .is. RowsLabel ) then
       OldIndexBeg = SubSpacesMat(1,1)%NRows() - MinNumNotOverlapingBs + 1
    else
       OldIndexBeg = SubSpacesMat(1,1)%NColumns() - MinNumNotOverlapingBs + 1
    end if
    !
    NewIndexBeg = NTotOvBs + 1
    !
    do i = 1, N
       !
       OldIndexEnd = OldIndexBeg + MinNumNotOverlapingBs - 1
       NewIndexEnd = NewIndexBeg + MinNumNotOverlapingBs - 1
       !
       if ( Which .is. RowsLabel ) then
          NewArray(NewIndexBeg:NewIndexEnd,:) = OldArray(OldIndexBeg:OldIndexEnd,:)
          !XCHEMQC include permutation of labels
          if (RowLabelPermute) NewRowLabels(NewIndexBeg:NewIndexEnd) = OldRowLabels(OldIndexBeg:OldIndexEnd)
          !END XCHEMQC
          if ( i < N ) then
             OldIndexBeg = OldIndexEnd + SubSpacesMat(i+1,1)%NRows() - MinNumNotOverlapingBs + 1
          else
             exit
          end if
       else
          NewArray(:,NewIndexBeg:NewIndexEnd) = OldArray(:,OldIndexBeg:OldIndexEnd)
          !XCHEMQC include permutation of labels
          if (ColLabelPermute) NewColumnLabels(NewIndexBeg:NewIndexEnd) = OldColumnLabels(OldIndexBeg:OldIndexEnd)
          !END XCHEMQC
          if ( i < N ) then
             OldIndexBeg = OldIndexEnd + SubSpacesMat(1,i+1)%NColumns() - MinNumNotOverlapingBs + 1
          else
             exit
          end if
       end if
       !
       NewIndexBeg = NewIndexEnd + 1
       !
    end do
    
    
    !.. 2nd step in the permutation of non-overlapping B-splines
    deallocate( OldArray )
    allocate( OldArray, source = NewArray )
    if ( Which .is. RowsLabel ) then
        OldRowLabels = NewRowLabels
    else
        OldColumnLabels = NewColumnLabels
    endif
    !
    OldIndexBeg = NTotOvBs + 1
    NewIndexBeg = NTotOvBs + 1
    !
    do i = 1, MinNumNotOverlapingBs
       do j = 1, N
          if ( Which .is. RowsLabel ) then
             NewArray(NewIndexBeg + j-1,:) = OldArray(OldIndexBeg + (j-1)*MinNumNotOverlapingBs,:)
             !XCHEMQC include permutation of labels
             if (RowLabelPermute) NewRowLabels(NewIndexBeg + j-1) = OldRowLabels(OldIndexBeg + (j-1)*MinNumNotOverlapingBs)
             !END XCHEMQC
          else
             NewArray(:,NewIndexBeg + j-1) = OldArray(:,OldIndexBeg + (j-1)*MinNumNotOverlapingBs)
             !XCHEMQC include permutation of labels
             if (ColLabelPermute) NewColumnLabels(NewIndexBeg + j-1) = OldColumnLabels(OldIndexBeg + (j-1)*MinNumNotOverlapingBs)
             !END XCHEMQC
          end if
       end do
       OldIndexBeg = OldIndexBeg + 1
       NewIndexBeg = NewIndexBeg + N
    end do
   
    !XCHEMQC: deallocate OldArray before Allocating FullMat
    !... to avoid excessive memory usage
    deallocate(OldArray)
    FullMat = NewArray
    !XCHEMQC Put (permuted) labels back in place
    if ( Which .is. RowsLabel ) then
        if (RowLabelPermute) then
            call FullMat%SetBlankRowLabels()
            call FullMat%SetRowLabels(1, FullMat%NRows() , NewRowLabels )
        endif
        if (ColLabelPermute) then
            call FullMat%SetBlankColumnLabels()
            call FullMat%SetColumnLabels(1, FullMat%NColumns() , OldColumnLabels )
        endif
    else
        if (ColLabelPermute) then
            call FullMat%SetBlankColumnLabels()
            call FullMat%SetColumnLabels(1, FullMat%NColumns() , NewColumnLabels )
        endif
        if (RowLabelPermute) then
            call FullMat%SetBlankRowLabels()
            call FullMat%SetRowLabels(1, FullMat%NRows() , OldRowLabels )
        endif
    endif
    !ENDXCHEMQC
    deallocate( NewArray )
    !
  end subroutine PermuteBsplines
  
  

  !> Gets the minimum number of non-overlapping B-splines among all channels.
  integer function GetMinNumNotOverlapingBs( SubSpacesBlock, Which ) result(MinNBs)
    !> Array of a general sub-spaces block i.e.: 
    !! LocPWCBlock, PWCPWCBlock, etc.
    class(*), target, intent(in) :: SubSpacesBlock(:,:)
    !> Spacifies whether the rows or the columns will be swapped.
    character(len=*), intent(in) :: Which
    !
    type(LocPWCBlock),pointer :: LocPWC
    type(SRCPWCBlock),pointer :: SRCPWC
    type(PWCLocBlock),pointer :: PWCLoc
    type(PWCSRCBlock),pointer :: PWCSRC
    type(PWCPWCBlock),pointer :: PWCPWC
    integer :: i, N, ierror
    integer, allocatable :: Nvec(:)
    !
    if ( Which .is. RowsLabel ) then
       !
       N = size(SubSpacesBlock,1)
       allocate( Nvec(N) )
       !
       do i = 1, N
          select type( ptr => SubSpacesBlock(i,1) )
          class is ( PWCLocBlock )
             allocate( PWCLoc, source = ptr )
             Nvec(i) = PWCLoc%GetNumBsNotOvDiffBra()
             deallocate( PWCLoc )
          class is ( PWCSRCBlock )
             allocate( PWCSRC, source = ptr )
             Nvec(i) = PWCSRC%GetNumBsNotOvDiffBra()
             deallocate( PWCSRC )
          class is ( PWCPWCBlock )
             allocate( PWCPWC, source = ptr )
             Nvec(i) = PWCPWC%GetNumBsNotOvDiffBra()
             deallocate( PWCPWC )
          class default 
             call Assert( "Invalid electronic subspace block." )
          end select
       end do
       !
    elseif ( Which .is. ColsLabel ) then
       !
       N = size(SubSpacesBlock,2)
       allocate( Nvec(N) )
       !
       do i = 1, N
          select type( ptr => SubSpacesBlock(1,i) )
          class is ( LocPWCBlock )
             allocate( LocPWC, source = ptr )
             Nvec(i) = LocPWC%GetNumBsNotOvDiffKet()
             deallocate( LocPWC )
          class is ( SRCPWCBlock )
             allocate( SRCPWC, source = ptr )
             Nvec(i) = SRCPWC%GetNumBsNotOvDiffKet()
             deallocate( SRCPWC )
          class is ( PWCPWCBlock )
             allocate( PWCPWC, source = ptr, STAT=ierror )
             if ( ierror /= 0) call Assert( 'Error associating PWCPWC pointer.' )
             Nvec(i) = PWCPWC%GetNumBsNotOvDiffKet()
             deallocate( PWCPWC )
          class default 
             call Assert( "Invalid electronic subspace block." )
          end select
       end do
       !
    end if
    !
    MinNBs = MINVAL( Nvec )
    if ( MinNBs <= 0 ) call Assert( &
         'The minimum number of B-splines not overlapping with '//&
         'diffuse Gaussian must be higher than 0.' )
    !
  end function GetMinNumNotOverlapingBs



  !--------------------------------------------------
  ! Methods for ClassSESSESBlock
  !-------------------------------------------------


  subroutine ClassSESSESBlockFinal( Self )
    type(ClassSESSESBlock) :: Self
    call Self%Free()
  end subroutine ClassSESSESBlockFinal


  subroutine ClassSESSESBlockFree( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    integer :: i, j
    Self%SpaceBra => NULL()
    Self%SpaceKet => NULL()
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    Self%BoxStatesOnly      = .false.
    Self%BraBoxStatesOnly   = .false.
    Self%KetBoxStatesOnly   = .false.
    Self%AsympBsPermutation = .false.
    Self%FormattedWrite     = .false.
    Self%AvailableBlocks    = .false.
  end subroutine ClassSESSESBlockFree

  logical function ClassSESSESBlockIsDecoupleChannels( Self ) result( IsDecoupleChannels )
    class(ClassSESSESBlock), intent(in) :: Self
    IsDecoupleChannels = Self%DecoupleChannels
  end function ClassSESSESBlockIsDecoupleChannels

  logical function ClassSESSESBlockIsBoxOnly( Self ) result( IsBoxOnly )
    class(ClassSESSESBlock), intent(in) :: Self
    IsBoxOnly = Self%BoxStatesOnly
  end function ClassSESSESBlockIsBoxOnly


  logical function ClassSESSESBlockIsBraBoxOnly( Self ) result( IsBoxOnly )
    class(ClassSESSESBlock), intent(in) :: Self
    IsBoxOnly = Self%BraBoxStatesOnly
  end function ClassSESSESBlockIsBraBoxOnly


  logical function ClassSESSESBlockIsKetBoxOnly( Self ) result( IsBoxOnly )
    class(ClassSESSESBlock), intent(in) :: Self
    IsBoxOnly = Self%KetBoxStatesOnly
  end function ClassSESSESBlockIsKetBoxOnly


  logical function ClassSESSESBlockIsAvailable( Self ) result( BlockAv )
    class(ClassSESSESBlock), intent(in) :: Self
    BlockAv = Self%AvailableBlocks
  end function ClassSESSESBlockIsAvailable



  subroutine ClassSESSESBlockUnsetDecoupleChannels( Self ) 
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%DecoupleChannels = .false.
  end subroutine  ClassSESSESBlockUnsetDecoupleChannels

  subroutine ClassSESSESBlockSetDecoupleChannels( Self ) 
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%DecoupleChannels = .true.
  end subroutine  ClassSESSESBlockSetDecoupleChannels

  subroutine ClassSESSESBlockUnsetFormattedWrite( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%FormattedWrite = .false.
  end subroutine ClassSESSESBlockUnsetFormattedWrite


  subroutine ClassSESSESBlockSetFormattedWrite( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%FormattedWrite = .true.
  end subroutine ClassSESSESBlockSetFormattedWrite

  subroutine ClassSESSESBlockUnsetBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%BoxStatesOnly = .false.
  end subroutine ClassSESSESBlockUnsetBoxOnly


  subroutine ClassSESSESBlockUnsetBraBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%BraBoxStatesOnly = .false.
  end subroutine ClassSESSESBlockUnsetBraBoxOnly


  subroutine ClassSESSESBlockUnsetKetBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%KetBoxStatesOnly = .false.
  end subroutine ClassSESSESBlockUnsetKetBoxOnly

  
  subroutine ClassSESSESBlockSetBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%BoxStatesOnly = .true.
  end subroutine ClassSESSESBlockSetBoxOnly


  subroutine ClassSESSESBlockSetBraBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%BraBoxStatesOnly = .true.
  end subroutine ClassSESSESBlockSetBraBoxOnly


  subroutine ClassSESSESBlockSetKetBoxOnly( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%KetBoxStatesOnly = .true.
  end subroutine ClassSESSESBlockSetKetBoxOnly


  subroutine ClassSESSESBlockSetAsympBsPermutation( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%AsympBsPermutation = .true.
  end subroutine ClassSESSESBlockSetAsympBsPermutation


  subroutine ClassSESSESBlockUnsetAsympBsPermutation( Self )
    class(ClassSESSESBlock), intent(inout) :: Self
    Self%AsympBsPermutation = .false.
  end subroutine ClassSESSESBlockUnsetAsympBsPermutation




  !> Initializes the ClassSESSESBlock class.
  subroutine ClassSESSESBlockInit( Self, SpaceBra, SpaceKet, OpLabel, Axis, Force )
    !> Class to be initialized.
    class(ClassSESSESBlock),                      intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)     :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout)  :: Axis
    logical,                                     intent(in)     :: Force
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
    allocate( Self%LocSRCBlock(NumLocBra,NumSRCKet) )
    allocate( Self%LocPWCBlock(NumLocBra,NumPWCKet) )
    allocate( Self%SRCLocBlock(NumSRCBra,NumLocKet) )
    allocate( Self%SRCSRCBlock(NumSRCBra,NumSRCKet) )
    allocate( Self%SRCPWCBlock(NumSRCBra,NumPWCKet) )
    allocate( Self%PWCLocBlock(NumPWCBra,NumLocKet) )
    allocate( Self%PWCSRCBlock(NumPWCBra,NumSRCKet) )
    allocate( Self%PWCPWCBlock(NumPWCBra,NumPWCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Build( BraLoc, KetLoc, OpLabel, OpXlm, Force )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCLocBlock(i,j)%Build( BraSRC, KetLoc, OpLabel, OpXlm, Force )
          
          !.. < pwc | O | loc >
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             
             !.. < pwc Xlm | O | loc >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCLocBlock(CounterBra,j)%SetFormattedWrite()
             call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc  ) 
             call Self%PWCLocBlock(CounterBra,j)%Build( BraPWC, KetLoc, OpLabel, OpXlm, Force )
             
          end do

       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocSRCBlock(i,j)%Build( BraLoc, KetSRC, OpLabel, OpXlm, Force )
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(i,j)%Build( BraSRC, KetSRC, OpLabel, OpXlm, Force )
          
          do n = 1, Self%SpaceBra%GetNumPWC(i)

             !.. < pwc Xlm | O | src >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(CounterBra,j)%SetFormattedWrite()
             call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             call Self%PWCSRCBlock(CounterBra,j)%Build( BraPWC, KetSRC, OpLabel, OpXlm, Force )
             
          end do
          
       end do

    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          KetPWC => Self%SpaceKet%GetChannelPWC(j,k)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)

          !.. < loc | O | pwc Xlm >
          do i = 1, NumLocBra
             BraLoc  => Self%SpaceBra%GetChannelLS()
             if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%LocPWCBlock(i,CounterKet)%SetFormattedWrite()
             call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC  ) 
             call Self%LocPWCBlock(i,CounterKet)%Build( BraLoc, KetPWC, OpLabel, OpXlm, Force )
          end do
          
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,CounterKet)%SetFormattedWrite()
             call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC  ) 
             call Self%SRCPWCBlock(i,CounterKet)%Build( BraSRC, KetPWC, OpLabel, OpXlm, Force )
             
             do n = 1, Self%SpaceBra%GetNumPWC(i)

                !.. < pwc Xlm | O | pwc Xlm >
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBoxOnly()
                if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBraBoxOnly()
                if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetKetBoxOnly()
                if ( Self%FormattedWrite ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFormattedWrite()
                call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC  ) 
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Build( BraPWC, KetPWC, OpLabel, OpXlm, Force )
                
             end do
          end do

       end do
    end do


    !
  end subroutine ClassSESSESBlockInit




  !> Initializes the ClassSESSESBlock class.
  subroutine ClassSESSESBlockInitAndSave( Self, SpaceBra, SpaceKet, OpLabel, Axis, Force, ExtraLabel, IsBraLocChannel, &
    IsKetLocChannel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                      intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)     :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout)  :: Axis
    logical,                                     intent(in)     :: Force
    character(len=*), optional                 , intent(in)     :: ExtraLabel
    logical, optional, intent(in)                               :: IsBraLocChannel
    logical, optional, intent(in)                               :: IsKetLocChannel
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    character(len=:), allocatable :: BlockFileName
    logical :: validBlock
    logical :: LocIsBraLocChannel
    logical :: LocIsKetLocChannel
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    LocIsBraLocChannel =  merge( IsBraLocChannel, .true., present( IsBraLocChannel ) )
    LocIsKetLocChannel =  merge( IsKetLocChannel, .true., present( IsKetLocChannel ) )
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    !XCHEMQC How so? Shouldn't you be reading if there is a Local channel for a
    !XCHEMQC given symmetry?
    NumLocBra = merge( 1, 0, LocIsBraLocChannel )
    NumLocKet = merge( 1, 0, LocIsKetLocChannel )
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       allocate( Self%LocLocBlock(1,1) )
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(1,1)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(1,1)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(1,1)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(1,1)%SetFormattedWrite()
          call Self%LocLocBlock(1,1)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(1,1)%Build( BraLoc, KetLoc, OpLabel, OpXlm, Force )
         ! call Self%LocLocBlock(1,1)%Save( )
         ! call Self%LocLocBlock(1,1)%Free( )
          BlockFileName = Self%LocLocBlock(1,1)%getBlockName(Self%LocLocBlock(1,1)%SpaceBra,&
            Self%LocLocBlock(1,1)%SpaceKet)
          validBlock = Self%LocLocBlock(1,1)%ValidBlock(BlockFileName)
          write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
          if (force .or. (.not.(validBlock))) then
             write(output_unit,'(a)') "Compute it!"
             call Self%LocLocBlock(1,1)%Save( )
          else
             write(output_unit,'(a)') "Already present. Skip."
          endif
          call Self%LocLocBlock(1,1)%Free( )
          if (associated(Self%LocLocBlock(1,1)%SpaceBra)) deallocate(Self%LocLocBlock(1,1)%SpaceBra) 
          if (associated(Self%LocLocBlock(1,1)%SpaceKet)) deallocate(Self%LocLocBlock(1,1)%SpaceKet) 
          BraLoc => NULL()
       end do
       deallocate( Self%LocLocBlock)
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra
          allocate( Self%SRCLocBlock(1,1) )

          !.. < src | O | loc >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(1,1)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(1,1)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(1,1)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(1,1)%SetFormattedWrite()
          call Self%SRCLocBlock(1,1)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCLocBlock(1,1)%Build( BraSRC, KetLoc, OpLabel, OpXlm, Force )
         ! call Self%SRCLocBlock(1,1)%Save( )
         ! call Self%SRCLocBlock(1,1)%Free( )
          BlockFileName = Self%SrcLocBlock(1,1)%getBlockName(Self%SrcLocBlock(1,1)%SpaceBra,&
            Self%SrcLocBlock(1,1)%SpaceKet)
          validBlock = Self%SrcLocBlock(1,1)%ValidBlock(BlockFileName)
          write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
          if (force .or. (.not.(validBlock))) then
             write(output_unit,'(a)') "Compute it!"
             call Self%SrcLocBlock(1,1)%Save( )
          else
             write(output_unit,'(a)') "Already present. Skip."
          endif
          call Self%SrcLocBlock(1,1)%Free( )
          if (associated(Self%SRCLocBlock(1,1)%SpaceBra)) deallocate(Self%SRCLocBlock(1,1)%SpaceBra) 
          if (associated(Self%SRCLocBlock(1,1)%SpaceKet)) deallocate(Self%SRCLocBlock(1,1)%SpaceKet) 
          BraSRC => null()
          deallocate( Self%SRCLocBlock)
          
          !.. < pwc | O | loc >
          allocate( Self%PWCLocBlock(1,1) )
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             
             !.. < pwc Xlm | O | loc >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCLocBlock(1,1)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCLocBlock(1,1)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCLocBlock(1,1)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCLocBlock(1,1)%SetFormattedWrite()
             call Self%PWCLocBlock(1,1)%SetFileExtension( FileExtensionPWCLoc  ) 
             if ( present(ExtraLabel) ) then
                call Self%PWCLocBlock(1,1)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
             end if
             call Self%PWCLocBlock(1,1)%Build( BraPWC, KetLoc, OpLabel, OpXlm, Force )
             BlockFileName = Self%PWCLocBlock(1,1)%getBlockName(Self%PWCLocBlock(1,1)%SpaceBra,&
               Self%PWCLocBlock(1,1)%SpaceKet,OpLabel, OpXlm)
             validBlock = Self%PWCLocBlock(1,1)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%PWCLocBlock(1,1)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%PWCLocBlock(1,1)%Free( )
             if (associated(Self%PWCLocBlock(1,1)%SpaceBra)) deallocate(Self%PWCLocBlock(1,1)%SpaceBra) 
             if (associated(Self%PWCLocBlock(1,1)%SpaceKet)) deallocate(Self%PWCLocBlock(1,1)%SpaceKet) 
             BraPWC => null()
          end do
          deallocate( Self%PWCLocBlock )

       end do
       KetLoc => NULL()

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       allocate( Self%LocSRCBlock(1,1) )
       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(1,1)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(1,1)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(1,1)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(1,1)%SetFormattedWrite()
          call Self%LocSRCBlock(1,1)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocSRCBlock(1,1)%Build( BraLoc, KetSRC, OpLabel, OpXlm, Force )
          !call Self%LocSRCBlock(1,1)%Save( )
          !call Self%LocSRCBlock(1,1)%Free( )
          BlockFileName = Self%LocSrcBlock(1,1)%getBlockName(Self%LocSrcBlock(1,1)%SpaceBra,&
            Self%LocSrcBlock(1,1)%SpaceKet)
          validBlock = Self%LocSrcBlock(1,1)%ValidBlock(BlockFileName)
          write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
          if (force .or. (.not.(validBlock))) then
             write(output_unit,'(a)') "Compute it!"
             call Self%LocSrcBlock(1,1)%Save( )
          else
             write(output_unit,'(a)') "Already present. Skip."
          endif
          call Self%LocSrcBlock(1,1)%Free( )
          if (associated(Self%LocSRCBlock(1,1)%SpaceBra)) deallocate(Self%LocSRCBlock(1,1)%SpaceBra) 
          if (associated(Self%LocSRCBlock(1,1)%SpaceKet)) deallocate(Self%LocSRCBlock(1,1)%SpaceKet) 
          BraLoc => NULL()
       end do
       deallocate( Self%LocSRCBlock )

       do i = 1, NumSRCBra

          allocate( Self%SRCSRCBlock(1,1) )
          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(1,1)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(1,1)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(1,1)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(1,1)%SetFormattedWrite()
          call Self%SRCSRCBlock(1,1)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(1,1)%Build( BraSRC, KetSRC, OpLabel, OpXlm, Force )
          !call Self%SRCSRCBlock(1,1)%Save( )
          !call Self%SRCSRCBlock(1,1)%Free( )
          BlockFileName = Self%SrcSrcBlock(1,1)%getBlockName(Self%SrcSrcBlock(1,1)%SpaceBra,&
            Self%SrcSrcBlock(1,1)%SpaceKet)
          validBlock = Self%SrcSrcBlock(1,1)%ValidBlock(BlockFileName)
          write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
          if (force .or. (.not.(validBlock))) then
             write(output_unit,'(a)') "Compute it!"
             call Self%SrcSrcBlock(1,1)%Save( )
          else
             write(output_unit,'(a)') "Already present. Skip."
          endif
          call Self%SrcSrcBlock(1,1)%Free( )
          if (associated(Self%SRCSRCBlock(1,1)%SpaceBra)) deallocate(Self%SRCSRCBlock(1,1)%SpaceBra) 
          if (associated(Self%SRCSRCBlock(1,1)%SpaceKet)) deallocate(Self%SRCSRCBlock(1,1)%SpaceKet) 
          BraSRC => NULL()
          deallocate( Self%SRCSRCBlock )
          
          allocate( Self%PWCSRCBlock(1,1) )
          do n = 1, Self%SpaceBra%GetNumPWC(i)

             !.. < pwc Xlm | O | src >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(1,1)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(1,1)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(1,1)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(1,1)%SetFormattedWrite()
             call Self%PWCSRCBlock(1,1)%SetFileExtension( FileExtensionPWCSRC  ) 
             call Self%PWCSRCBlock(1,1)%Build( BraPWC, KetSRC, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(1,1)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             end if
             !XCHEMQC this is a problem, if
             !block exists and isnt forced it gets overwritten by 0 matrix
             !Proposed fix:
             BlockFileName = Self%PWCSRCBlock(1,1)%getBlockName(Self%PWCSRCBlock(1,1)%SpaceBra,&
               Self%PWCSRCBlock(1,1)%SpaceKet)
             validBlock = Self%PWCSRCBlock(1,1)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%PWCSRCBlock(1,1)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             write(output_unit,'(a)') ""
             call Self%PWCSRCBlock(1,1)%Free( )
             if (associated(Self%PWCSRCBlock(1,1)%SpaceBra)) deallocate(Self%PWCSRCBlock(1,1)%SpaceBra) 
             if (associated(Self%PWCSRCBlock(1,1)%SpaceKet)) deallocate(Self%PWCSRCBlock(1,1)%SpaceKet) 
             BraPWC => NULL() 
          end do
          deallocate( Self%PWCSRCBlock )
          
       end do
       KetSRC => NULL()
    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          KetPWC => Self%SpaceKet%GetChannelPWC(j,k)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)

          !.. < loc | O | pwc Xlm >
          allocate( Self%LocPWCBlock(1,1) )
          do i = 1, NumLocBra
             BraLoc  => Self%SpaceBra%GetChannelLS()
             if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(1,1)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(1,1)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(1,1)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%LocPWCBlock(1,1)%SetFormattedWrite()
             call Self%LocPWCBlock(1,1)%SetFileExtension( FileExtensionLocPWC  ) 
             call Self%LocPWCBlock(1,1)%Build( BraLoc, KetPWC, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%LocPWCBlock(1,1)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
             end if
             BlockFileName = Self%LocPWCBlock(1,1)%getBlockName(Self%LocPWCBlock(1,1)%SpaceBra,&
               Self%LocPWCBlock(1,1)%SpaceKet)
             validBlock = Self%LocPWCBlock(1,1)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%LocPWCBlock(1,1)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%LocPWCBlock(1,1)%Free( )
             if (associated(Self%LocPWCBlock(1,1)%SpaceBra)) deallocate(Self%LocPWCBlock(1,1)%SpaceBra) 
             if (associated(Self%LocPWCBlock(1,1)%SpaceKet)) deallocate(Self%LocPWCBlock(1,1)%SpaceKet) 
             BraLoc => NULL() 
          end do
          deallocate( Self%LocPWCBlock )
          
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             allocate( Self%SRCPWCBlock(1,1) )
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(1,1)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(1,1)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(1,1)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(1,1)%SetFormattedWrite()
             call Self%SRCPWCBlock(1,1)%SetFileExtension( FileExtensionSRCPWC  ) 
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(1,1)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             end if
             call Self%SRCPWCBlock(1,1)%Build( BraSRC, KetPWC, OpLabel, OpXlm, Force )
             !XCHEMQC this is a problem, if
             !block exists and isnt forced it gets overwritten by 0 matrix
             !Proposed fix:
             BlockFileName = Self%SRCPWCBlock(1,1)%getBlockName(Self%SRCPWCBlock(1,1)%SpaceBra,&
               Self%SRCPWCBlock(1,1)%SpaceKet)
             validBlock = Self%SRCPWCBlock(1,1)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%SRCPWCBlock(1,1)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%SRCPWCBlock(1,1)%Free( )
             if (associated(Self%SRCPWCBlock(1,1)%SpaceBra)) deallocate(Self%SRCPWCBlock(1,1)%SpaceBra) 
             if (associated(Self%SRCPWCBlock(1,1)%SpaceKet)) deallocate(Self%SRCPWCBlock(1,1)%SpaceKet) 
             BraSRC => NULL() 
             deallocate( Self%SRCPWCBlock )
             
             allocate( Self%PWCPWCBlock(1,1) )
             do n = 1, Self%SpaceBra%GetNumPWC(i)

                !.. < pwc Xlm | O | pwc Xlm >
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(1,1)%SetBoxOnly()
                if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(1,1)%SetBraBoxOnly()
                if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(1,1)%SetKetBoxOnly()
                if ( Self%FormattedWrite ) call Self%PWCPWCBlock(1,1)%SetFormattedWrite()
                call Self%PWCPWCBlock(1,1)%SetFileExtension( FileExtensionPWCPWC  ) 
                call Self%PWCPWCBlock(1,1)%Build( BraPWC, KetPWC, OpLabel, OpXlm, Force )
                if ( present(ExtraLabel) ) then
                   call Self%PWCPWCBlock(1,1)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
                end if
                !XCHEMQC this is a problem, if
                !block exists and isnt forced it gets overwritten by 0 matrix
                !Proposed fix:
                BlockFileName = Self%PWCPWCBlock(1,1)%GetBlockName(&
                  Self%PWCPWCBlock(1,1)%SpaceBra,Self%PWCPWCBlock(1,1)%SpaceKet)
                validBlock=Self%PWCPWCBlock(1,1)%ValidBlock(BlockFileName)
                write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
                if (force .or. (.not.(validBlock))) then
                    write(output_unit,'(a)') "Compute it!"
                    call Self%PWCPWCBlock(1,1)%Save( )
                 else
                    write(output_unit,'(a)') "Already present. Skip."
                endif
                write(output_unit,'(a)') ""
                call Self%PWCPWCBlock(1,1)%Free( )
                if (associated(Self%PWCPWCBlock(1,1)%SpaceBra)) deallocate(Self%PWCPWCBlock(1,1)%SpaceBra) 
                if (associated(Self%PWCPWCBlock(1,1)%SpaceKet)) deallocate(Self%PWCPWCBlock(1,1)%SpaceKet) 
                BraPWC => NULL() 
             end do
             deallocate( Self%PWCPWCBlock )
          end do
          KetPWC => NULL()
       end do
    end do


    !
  end subroutine ClassSESSESBlockInitAndSave

  subroutine ClassSESSESBlockInitAndSavePWCOnly( Self, SpaceBra, SpaceKet, OpLabel, Axis, Force, ExtraLabel,&
    IsBraLocChannel, IsKetLocChannel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                      intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)     :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout)  :: Axis
    logical,                                     intent(in)     :: Force
    character(len=*), optional                 , intent(in)     :: ExtraLabel
    logical, optional, intent(in)                               :: IsBraLocChannel
    logical, optional, intent(in)                               :: IsKetLocChannel
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    character(len=:), allocatable :: BlockFileName
    logical :: validBlock
    logical :: LocIsBraLocChannel
    logical :: LocIsKetLocChannel
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    LocIsBraLocChannel =  merge( IsBraLocChannel, .true., present( IsBraLocChannel ) )
    LocIsKetLocChannel =  merge( IsKetLocChannel, .true., present( IsKetLocChannel ) )
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    !XCHEMQC How so? Shouldn't you be reading if there is a Local channel for a
    !XCHEMQC given symmetry?
    NumLocBra = merge( 1, 0, LocIsBraLocChannel )
    NumLocKet = merge( 1, 0, LocIsKetLocChannel )
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocPWCBlock(NumLocBra,NumPWCKet) )
    allocate( Self%SRCPWCBlock(NumSRCBra,NumPWCKet) )
    allocate( Self%PWCLocBlock(NumPWCBra,NumLocKet) )
    allocate( Self%PWCSRCBlock(NumPWCBra,NumSRCKet) )
    allocate( Self%PWCPWCBlock(NumPWCBra,NumPWCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < pwc | O | loc >
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             
             !.. < pwc Xlm | O | loc >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCLocBlock(CounterBra,j)%SetFormattedWrite()
             call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc  ) 
             call Self%PWCLocBlock(CounterBra,j)%Build( BraPWC, KetLoc, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
             end if
             BlockFileName = Self%PWCLocBlock(CounterBra,j)%getBlockName(Self%PWCLocBlock(CounterBra,j)%SpaceBra,&
               Self%PWCLocBlock(CounterBra,j)%SpaceKet)
             validBlock = Self%PWCLocBlock(CounterBra,j)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%PWCLocBlock(CounterBra,j)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%PWCLocBlock(CounterBra,j)%Free( )
             
          end do

       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       do i = 1, NumSRCBra

          do n = 1, Self%SpaceBra%GetNumPWC(i)

             !.. < pwc Xlm | O | src >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(CounterBra,j)%SetFormattedWrite()
             call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             call Self%PWCSRCBlock(CounterBra,j)%Build( BraPWC, KetSRC, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             end if
             !XCHEMQC this is a problem, if
             !block exists and isnt forced it gets overwritten by 0 matrix
             !Proposed fix:
             BlockFileName = Self%PWCSRCBlock(CounterBra,j)%getBlockName(Self%PWCSRCBlock(CounterBra,j)%SpaceBra,&
               Self%PWCSRCBlock(CounterBra,j)%SpaceKet)
             validBlock = Self%PWCSRCBlock(CounterBra,j)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%PWCSRCBlock(CounterBra,j)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             write(output_unit,'(a)') ""
             call Self%PWCSRCBlock(CounterBra,j)%Free( )
             
          end do
          
       end do

    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          KetPWC => Self%SpaceKet%GetChannelPWC(j,k)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)

          !.. < loc | O | pwc Xlm >
          do i = 1, NumLocBra
             BraLoc  => Self%SpaceBra%GetChannelLS()
             if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%LocPWCBlock(i,CounterKet)%SetFormattedWrite()
             call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC  ) 
             call Self%LocPWCBlock(i,CounterKet)%Build( BraLoc, KetPWC, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
             end if
             BlockFileName = Self%LocPWCBlock(CounterBra,j)%getBlockName(Self%LocPWCBlock(CounterBra,j)%SpaceBra,&
               Self%LocPWCBlock(CounterBra,j)%SpaceKet)
             validBlock = Self%LocPWCBlock(CounterBra,j)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%LocPWCBlock(i,CounterKet)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%LocPWCBlock(i,CounterKet)%Free( )
          end do
          
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,CounterKet)%SetFormattedWrite()
             call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC  ) 
             call Self%SRCPWCBlock(i,CounterKet)%Build( BraSRC, KetPWC, OpLabel, OpXlm, Force )
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             end if
             !XCHEMQC this is a problem, if
             !block exists and isnt forced it gets overwritten by 0 matrix
             !Proposed fix:
             BlockFileName = Self%SRCPWCBlock(i,CounterKet)%getBlockName(Self%SRCPWCBlock(i,CounterKet)%SpaceBra,&
               Self%SRCPWCBlock(i,CounterKet)%SpaceKet)
             validBlock = Self%SRCPWCBlock(i,CounterKet)%ValidBlock(BlockFileName)
             write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
             if (force .or. (.not.(validBlock))) then
                write(output_unit,'(a)') "Compute it!"
                call Self%SRCPWCBlock(i,CounterKet)%Save( )
             else
                write(output_unit,'(a)') "Already present. Skip."
             endif
             call Self%SRCPWCBlock(i,CounterKet)%Free( )
             
             do n = 1, Self%SpaceBra%GetNumPWC(i)

                !.. < pwc Xlm | O | pwc Xlm >
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBoxOnly()
                if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBraBoxOnly()
                if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetKetBoxOnly()
                if ( Self%FormattedWrite ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFormattedWrite()
                call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC  ) 
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Build( BraPWC, KetPWC, OpLabel, OpXlm, Force )
                if ( present(ExtraLabel) ) then
                   call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
                end if
                !XCHEMQC this is a problem, if
                !block exists and isnt forced it gets overwritten by 0 matrix
                !Proposed fix:
                BlockFileName = Self%PWCPWCBlock(CounterBra,CounterKet)%GetBlockName(&
                  Self%PWCPWCBlock(CounterBra,CounterKet)%SpaceBra,Self%PWCPWCBlock(CounterBra,CounterKet)%SpaceKet)
                validBlock=Self%PWCPWCBlock(CounterBra,CounterKet)%ValidBlock(BlockFileName)
                write(output_unit,'(a,a)') "Consider Block:",trim(BlockFileName)
                if (force .or. (.not.(validBlock))) then
                    write(output_unit,'(a)') "Compute it!"
                    call Self%PWCPWCBlock(CounterBra,CounterKet)%Save( )
                 else
                    write(output_unit,'(a)') "Already present. Skip."
                endif
                write(output_unit,'(a)') ""
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Free( )
                
             end do
          end do

       end do
    end do


    !
  end subroutine ClassSESSESBlockInitAndSavePWCOnly

  subroutine ClassSESSESBlockInitAndSaveLocSRCOnly( Self, SpaceBra, SpaceKet, OpLabel, Axis, Force, ExtraLabel,&
    IsBraLocChannel, IsKetLocChannel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                      intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)     :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)     :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout)  :: Axis
    logical,                                     intent(in)     :: Force
    character(len=*), optional                 , intent(in)     :: ExtraLabel
    logical, optional, intent(in)                               :: IsBraLocChannel
    logical, optional, intent(in)                               :: IsKetLocChannel
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    character(len=:), allocatable :: BlockFileName
    logical :: validBlock
    logical :: LocIsBraLocChannel
    logical :: LocIsKetLocChannel
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    LocIsBraLocChannel =  merge( IsBraLocChannel, .true., present( IsBraLocChannel ) )
    LocIsKetLocChannel =  merge( IsKetLocChannel, .true., present( IsKetLocChannel ) )
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    !XCHEMQC How so? Shouldn't you be reading if there is a Local channel for a
    !XCHEMQC given symmetry?
    NumLocBra = merge( 1, 0, LocIsBraLocChannel )
    NumLocKet = merge( 1, 0, LocIsKetLocChannel )
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
    allocate( Self%LocSRCBlock(NumLocBra,NumSRCKet) )
    allocate( Self%SRCLocBlock(NumSRCBra,NumLocKet) )
    allocate( Self%SRCSRCBlock(NumSRCBra,NumSRCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Build( BraLoc, KetLoc, OpLabel, OpXlm, Force )
          BlockFileName = Self%LocLocBlock(i,j)%GetBlockName(Self%LocLocBlock(i,j)%SpaceBra,Self%LocLocBlock(i,j)%SpaceKet)
          write(output_unit,'(a,a)') "Block LocLoc: ",BlockFileName
          call Self%LocLocBlock(i,j)%Save( )
          call Self%LocLocBlock(i,j)%Free( )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCLocBlock(i,j)%Build( BraSRC, KetLoc, OpLabel, OpXlm, Force )
          BlockFileName = Self%SRCLocBlock(i,j)%GetBlockName(Self%SRCLocBlock(i,j)%SpaceBra,Self%SRCLocBlock(i,j)%SpaceKet)
          write(output_unit,'(a,a)') "Block SRCLoc: ",BlockFileName
          call Self%SRCLocBlock(i,j)%Save( )
          call Self%SRCLocBlock(i,j)%Free( )
          
       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocSRCBlock(i,j)%Build( BraLoc, KetSRC, OpLabel, OpXlm, Force )
          BlockFileName = Self%LocSRCBlock(i,j)%GetBlockName(Self%LocSRCBlock(i,j)%SpaceBra,Self%LocSRCBlock(i,j)%SpaceKet)
          write(output_unit,'(a,a)') "Block LocSRC: ",BlockFileName
          call Self%LocSRCBlock(i,j)%Save( )
          call Self%LocSRCBlock(i,j)%Free( )
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(i,j)%Build( BraSRC, KetSRC, OpLabel, OpXlm, Force )
          BlockFileName = Self%SRCSRCBlock(i,j)%GetBlockName(Self%SRCSRCBlock(i,j)%SpaceBra,Self%SRCSRCBlock(i,j)%SpaceKet)
          write(output_unit,'(a,a)') "Block SRCSRC: ",BlockFileName
          call Self%SRCSRCBlock(i,j)%Save( )
          call Self%SRCSRCBlock(i,j)%Free( )
          
       end do

    end do

    !
  end subroutine ClassSESSESBlockInitAndSaveLocSRCOnly


  !> Reads the ClassSESSESBlock class.
  subroutine ClassSESSESBlockLoad( Self, SpaceBra, SpaceKet, OpLabel, Axis, ExtraLabel, LoadLocStates )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    logical, optional                          , intent(in)    :: LoadLocStates
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    logical :: LocLoadLocStates  
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    LocLoadLocStates = merge(LoadLocStates, .true. , present( LoadLocStates )) 
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if (LocLoadLocStates ) then
        if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
        if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
        if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
        if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
        if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    endif
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    if ( LocLoadLocStates ) then
        allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
        allocate( Self%LocSRCBlock(NumLocBra,NumSRCKet) )
        allocate( Self%LocPWCBlock(NumLocBra,NumPWCKet) )
        allocate( Self%SRCLocBlock(NumSRCBra,NumLocKet) )
        allocate( Self%PWCLocBlock(NumPWCBra,NumLocKet) )
    endif
    allocate( Self%SRCSRCBlock(NumSRCBra,NumSRCKet) )
    allocate( Self%SRCPWCBlock(NumSRCBra,NumPWCKet) )
    allocate( Self%PWCSRCBlock(NumPWCBra,NumSRCKet) )
    allocate( Self%PWCPWCBlock(NumPWCBra,NumPWCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    if ( LocLoadLocStates ) then
        do j = 1, NumLocKet

           KetLoc  => Self%SpaceKet%GetChannelLS()

           !.. < loc | O | loc >
           do i = 1, NumLocBra
              BraLoc  => Self%SpaceBra%GetChannelLS()
              if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
              if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
              if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
              if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
              call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
              call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
           end do
           
           !.. Sum over the parent ions
           do i = 1, NumSRCBra

              !.. < src | O | loc >
              BraSRC => Self%SpaceBra%GetChannelSRC(i)
              if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
              if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
              if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
              if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
              call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
              call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm )
              
              !.. < pwc | O | loc >
              do n = 1, Self%SpaceBra%GetNumPWC(i)
                 
                 !.. < pwc Xlm | O | loc >
                 BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
                 CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                 if ( Self%BoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBoxOnly()
                 if ( Self%BraBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetBraBoxOnly()
                 if ( Self%KetBoxStatesOnly ) call Self%PWCLocBlock(CounterBra,j)%SetKetBoxOnly()
                 if ( Self%AsympBsPermutation ) call Self%PWCLocBlock(CounterBra,j)%SetAsympBsPermutation()
                 if ( Self%FormattedWrite ) call Self%PWCLocBlock(CounterBra,j)%SetFormattedWrite()
                 if ( present(ExtraLabel) ) then
                    call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
                 else
                    call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc  )
                 end if
                 call Self%PWCLocBlock(CounterBra,j)%Load( BraPWC, KetLoc, OpLabel, OpXlm )
                 
              end do

           end do

        end do
    endif

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       !.. < loc | O | src >
       if ( LocLoadLocStates ) then 
           do i = 1, NumLocBra
              BraLoc  => Self%SpaceBra%GetChannelLS()
              if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
              if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
              if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
              if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
              call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
              call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm )
           end do
       endif

       do i = 1, NumSRCBra

          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm )
          
          do n = 1, Self%SpaceBra%GetNumPWC(i)

             !.. < pwc Xlm | O | src >
             BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(CounterBra,j)%SetKetBoxOnly()
             if ( Self%AsympBsPermutation ) call Self%PWCSRCBlock(CounterBra,j)%SetAsympBsPermutation()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(CounterBra,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             else
                call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             end if
             call Self%PWCSRCBlock(CounterBra,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm )
             
          end do
          
       end do

    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          KetPWC => Self%SpaceKet%GetChannelPWC(j,k)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)

          !.. < loc | O | pwc Xlm >
          if ( LocLoadLocStates ) then
              do i = 1, NumLocBra
                 BraLoc  => Self%SpaceBra%GetChannelLS()
                 if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBoxOnly()
                 if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetBraBoxOnly()
                 if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(i,CounterKet)%SetKetBoxOnly()
                 if ( Self%AsympBsPermutation ) call Self%LocPWCBlock(i,CounterKet)%SetAsympBsPermutation()
                 if ( Self%FormattedWrite ) call Self%LocPWCBlock(i,CounterKet)%SetFormattedWrite()
                 if ( present(ExtraLabel) ) then
                    call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
                 else
                    call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC ) 
                 end if
                 call Self%LocPWCBlock(i,CounterKet)%Load( BraLoc, KetPWC, OpLabel, OpXlm )
              end do
          endif
          
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,CounterKet)%SetKetBoxOnly()
             if ( Self%AsympBsPermutation ) call Self%SRCPWCBlock(i,CounterKet)%SetAsympBsPermutation()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,CounterKet)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             else
                call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC ) 
             end if
             call Self%SRCPWCBlock(i,CounterKet)%Load( BraSRC, KetPWC, OpLabel, OpXlm )
             
             do n = 1, Self%SpaceBra%GetNumPWC(i)

                !.. < pwc Xlm | O | pwc Xlm >
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBoxOnly()
                if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetBraBoxOnly()
                if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetKetBoxOnly()
                if ( Self%AsympBsPermutation ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetAsympBsPermutation()
                if ( Self%FormattedWrite ) call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFormattedWrite()
                if ( present(ExtraLabel) ) then
                   call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
                else
                   call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC ) 
                end if
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
                
             end do
          end do

       end do
    end do


    !
  end subroutine ClassSESSESBlockLoad



  !> Reads the QC components in ClassSESSESBlock class.
  subroutine ClassSESSESBlockLoadQC( Self, SpaceBra, SpaceKet, OpLabel, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumLocBra, NumLocKet
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    !
    allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
    allocate( Self%LocSRCBlock(NumLocBra,NumSRCKet) )
    allocate( Self%SRCLocBlock(NumSRCBra,NumLocKet) )
    allocate( Self%SRCSRCBlock(NumSRCBra,NumSRCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm )
          
       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm )
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm )
          
       end do

    end do


    !
  end subroutine ClassSESSESBlockLoadQC



  !> Reads the polycentric Gaussian components in ClassSESSESBlock class.
  subroutine ClassSESSESBlockLoadPoly( Self, SpaceBra, SpaceKet, OpLabel, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                     intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: i, j, k, n
    integer :: NumSRCBra, NumSRCKet, NumLocBra, NumLocKet
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    logical, parameter :: OnlyPoly = .true.
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
    allocate( Self%LocSRCBlock(NumLocBra,NumSRCKet) )
    allocate( Self%SRCLocBlock(NumSRCBra,NumLocKet) )
    allocate( Self%SRCSRCBlock(NumSRCBra,NumSRCKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly    ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite   ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly    ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite   ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm, OnlyPoly )
          
       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       KetSRC => Self%SpaceKet%GetChannelSRC(j)

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly    ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite   ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm, OnlyPoly )
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          BraSRC => Self%SpaceBra%GetChannelSRC(i)
          if ( Self%BoxStatesOnly    ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite   ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, OnlyPoly )
          
       end do

    end do
    !
  end subroutine ClassSESSESBlockLoadPoly

  !> Reads the monocentric Gaussian components in ClassSESSESBlock class.
  subroutine ClassSESSESBlockLoadMonoIon( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: NumPWCBra, NumPWCKet
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. Check that the ion index is between 1 and the max number of ions
    if( IonBra < 1 .or. IonBra > Self%SpaceBra%GetNumChannels())then
       call Assert('Wrong Bra ion index in call to MonoIon')
    endif
    if( IonKet < 1 .or. IonKet > Self%SpaceKet%GetNumChannels())then
       call Assert('Wrong Ket ion index in call to MonoIon')
    endif
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )
    !
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%SRCSRCBlock(1,1) )
    allocate( Self%SRCPWCBlock(1,NumPWCKet) )
    allocate( Self%PWCSRCBlock(NumPWCBra,1) )
    allocate( Self%PWCPWCBlock(NumPWCBra,NumPWCKet) )
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < mono ion src | O | mono ion src >
    KetSRC => SpaceKet%GetChannelSRC( IonKet )
    BraSRC => SpaceBra%GetChannelSRC( IonBra )
    !
    if ( Self%BoxStatesOnly    ) call Self%SRCSRCBlock(1,1)%SetBoxOnly()
    if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(1,1)%SetBraBoxOnly()
    if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(1,1)%SetKetBoxOnly()
    if ( Self%FormattedWrite   ) call Self%SRCSRCBlock(1,1)%SetFormattedWrite()
    !
    call Self%SRCSRCBlock(1,1)%SetFileExtension( GetQCFileExtension() ) 
    call Self%SRCSRCBlock(1,1)%Load( BraSRC, KetSRC, OpLabel, OpXlm, BraOnlyMono = .TRUE., KetOnlyMono = .TRUE. )
    !
    !.. < pwc Xlm | O | mono ion src >
    do iPWCBra = 1, NumPWCBra
       !
       BraPWC     => Self%SpaceBra%GetChannelPWC  ( IonBra, iPWCBra )
       if ( Self%BoxStatesOnly      ) call Self%PWCSRCBlock(iPWCBra,1)%SetBoxOnly()
       if ( Self%BraBoxStatesOnly   ) call Self%PWCSRCBlock(iPWCBra,1)%SetBraBoxOnly()
       if ( Self%KetBoxStatesOnly   ) call Self%PWCSRCBlock(iPWCBra,1)%SetKetBoxOnly()
       if ( Self%FormattedWrite     ) call Self%PWCSRCBlock(iPWCBra,1)%SetFormattedWrite()
       if ( present(ExtraLabel) ) then
          call Self%PWCSRCBlock(iPWCBra,1)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
       else
          call Self%PWCSRCBlock(iPWCBra,1)%SetFileExtension( FileExtensionPWCSRC  ) 
       end if
       call Self%PWCSRCBlock(iPWCBra,1)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
       !
    end do
    !
    !.. < * | O | pwc Xlm >
    do iPWCKet = 1, NumPWCKet

       KetPWC     => Self%SpaceKet%GetChannelPWC  ( IonKet, iPWCKet )

       !.. < mono ion src | O | pwc Xlm >
       if ( Self%BoxStatesOnly      ) call Self%SRCPWCBlock(1,iPWCKet)%SetBoxOnly()
       if ( Self%BraBoxStatesOnly   ) call Self%SRCPWCBlock(1,iPWCKet)%SetBraBoxOnly()
       if ( Self%KetBoxStatesOnly   ) call Self%SRCPWCBlock(1,iPWCKet)%SetKetBoxOnly()
       if ( Self%FormattedWrite     ) call Self%SRCPWCBlock(1,iPWCKet)%SetFormattedWrite()
       if ( present(ExtraLabel) ) then
          call Self%SRCPWCBlock(1,iPWCKet)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
       else
          call Self%SRCPWCBlock(1,iPWCKet)%SetFileExtension( FileExtensionSRCPWC ) 
       end if
       call Self%SRCPWCBlock(1,iPWCKet)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyMono = .TRUE. )
       
       do iPWCBra = 1, NumPWCBra
          
          !.. < pwc Xlm | O | pwc Xlm >
          BraPWC     => Self%SpaceBra%GetChannelPWC  ( IonBra, iPWCBra )

          if ( Self%BoxStatesOnly      ) call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly   ) call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly   ) call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetKetBoxOnly()
          if ( Self%AsympBsPermutation ) call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetAsympBsPermutation()
          if ( Self%FormattedWrite     ) call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetFormattedWrite()
          if ( present(ExtraLabel) ) then
             call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
          else
             call Self%PWCPWCBlock(iPWCBra,iPWCKet)%SetFileExtension( FileExtensionPWCPWC ) 
          end if
          call Self%PWCPWCBlock(iPWCBra,iPWCKet)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
          !
       end do
       !
    end do
    !
  end subroutine ClassSESSESBlockLoadMonoIon
  subroutine ClassSESSESBlockLoadMonoIonPolyCondition( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, Conditioner, BsRemovedInf, Axis,ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    integer, allocatable, optional, intent(inout)                :: BsRemovedInf(:)
    !
    integer :: NumLocBra, NumLocKet
    integer :: NumSRCBra, NumSRCKet
    integer :: NumPWCBra, NumPWCKet
    integer :: i,j,n,k
    class(ClassConditionerBlock), intent(inout) :: Conditioner
    type(ClassMatrix) :: ConditionMat
    type(ClassXlm) :: Xlm,OverlapXlm
    character(len=:), allocatable :: StorageDir
    type(ClassIrrep), pointer :: DiffIrrep

    !
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. Check that the ion index is between 1 and the max number of ions
!    if( IonBra < 1 .or. IonBra > Self%SpaceBra%GetNumChannels())then
!       call Assert('Wrong Bra ion index in call to MonoIon')
!    endif
!    if( IonKet < 1 .or. IonKet > Self%SpaceKet%GetNumChannels())then
!       call Assert('Wrong Ket ion index in call to MonoIon')
!    endif
    !
    NumLocBra = 0
    NumLocKet = 0
    if ( IonBra == 0 ) NumLocBra = 1
    if ( IonKet == 0 ) NumLocKet = 1
    NumSRCBra = 1
    NumSRCKet = 1
    if ( IonBra == 0 ) NumSRCBra = Self%SpaceBra%GetNumChannels()
    if ( IonKet == 0 ) NumSRCKet = Self%SpaceKet%GetNumChannels()
    NumPWCBra = 0
    NumPWCKet = 0
    if ( IonBra > 0 ) NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    if ( IonKet > 0 ) NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )
    if (present(BsRemovedInf)) then
      if (allocated(BsRemovedInf)) deallocate(BsRemovedInf)
      allocate(BsRemovedInf(NumPWCKet))
    endif
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocLocBlock( NumLocBra,NumLocKet ) )
    allocate( Self%LocSRCBlock( NumLocBra,NumSRCKet ) )
    allocate( Self%LocPWCBlock( NumLocBra,NumPWCKet ) )
    allocate( Self%SRCLocBlock( NumSRCBra,NumLocKet ) )
    allocate( Self%SRCSRCBlock( NumSRCBra,NumSRCKet ) )
    allocate( Self%SRCPWCBlock( NumSRCBra,NumPWCKet ) )
    allocate( Self%PWCLocBlock( NumPWCBra,NumLocKet ) )
    allocate( Self%PWCSRCBlock( NumPWCBra,NumSRCKet ) )
    allocate( Self%PWCPWCBlock( NumPWCBra,NumPWCKet ) )
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !

    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >

          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          if ( IonBra == 0 ) then 
             call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
          !.. < pwc | O | loc >
          do n = 1, NumPWCBra
             
             !.. < pwc Xlm | O | loc >
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif
             if ( Self%BoxStatesOnly      ) call Self%PWCLocBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly   ) call Self%PWCLocBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly   ) call Self%PWCLocBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCLocBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCLocBlock(n,j)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
             else
                call Self%PWCLocBlock(n,j)%SetFileExtension( FileExtensionPWCLoc  )
             end if
             call Self%PWCLocBlock(n,j)%Load( BraPWC, KetLoc, OpLabel, OpXlm )
             allocate( StorageDir, source = GetStorageDir(braPWC) )
             !
             call Xlm%Init( BraPWC%GetL(), BraPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(BraPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCLocBlock(n,j)%Block%Multiply( ConditionMat, 'Left', 'T' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             
          end do

       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       if ( IonKet == 0 ) then
          KetSRC => Self%SpaceKet%GetChannelSRC(j)
       else
          KetSRC => Self%SpaceKet%GetChannelSRC(IonKet)
       endif

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          if ( IonKet == 0 ) then
             call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() )
          if ( IonBra == 0 ) then  
             if ( IonKet == 0) then
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyPoly = .TRUE., KetOnlyPoly = .TRUE. )
             else
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyPoly = .TRUE., KetOnlyMono = .TRUE. )
             endif
          else
             if ( IonKet == 0) then
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyMono = .TRUE., KetOnlyPoly = .TRUE. )
             else
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyMono = .TRUE., KetOnlyMono = .TRUE. )
             endif

          endif
          
          do n = 1, NumPWCBra
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             !.. < pwc Xlm | O | src >
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             else
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             end if
             if ( IonKet == 0 ) then
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
             else
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
             endif
             allocate( StorageDir, source = GetStorageDir(braPWC) )
             !
             call Xlm%Init( BraPWC%GetL(), BraPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(BraPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCSRCBlock(n,j)%Block%Multiply( ConditionMat, 'Left', 'T' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             
          end do
          
       end do

    end do


    !.. < * | O | pwc >
    do k = 1, NumPWCKet
       KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,k)

       !.. < loc | O | pwc Xlm >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocPWCBlock(i,k)%SetFormattedWrite()
          if ( present(ExtraLabel) ) then
             call Self%LocPWCBlock(i,k)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
          else
             call Self%LocPWCBlock(i,k)%SetFileExtension( FileExtensionLocPWC ) 
          end if
          call Self%LocPWCBlock(i,k)%Load( BraLoc, KetPWC, OpLabel, OpXlm )
          allocate( StorageDir, source = GetStorageDir(ketPWC) )
          !
          call Xlm%Init( ketPWC%GetL(), ketPWC%GetM() )
          call Conditioner%SetXlm( Xlm )
          !
          OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
          allocate( DiffIrrep, source = GetDiffuseIrrep(ketPWC,OverlapXlm) )
          call Conditioner%SetIrrep( DiffIrrep )
          !
          call Conditioner%ReadBlock( StorageDir, ConditionMat )
          !
          call Self%LocPWCBlock(i,k)%Block%Multiply( ConditionMat, 'Right', 'N' )
          call ConditionMat%Free()
          deallocate(StorageDir,DiffIrrep)
       end do
       
       !.. < src | O | pwc Xlm >
       do i = 1, NumSRCBra
          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif

          if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,k)%SetFormattedWrite()
          if ( present(ExtraLabel) ) then
             call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
          else
             call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC ) 
          end if
          if ( IonBra == 0 ) then
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
          allocate( StorageDir, source = GetStorageDir(ketPWC) )
          !
          call Xlm%Init( ketPWC%GetL(), ketPWC%GetM() )
          call Conditioner%SetXlm( Xlm )
          !
          OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
          allocate( DiffIrrep, source = GetDiffuseIrrep(ketPWC,OverlapXlm) )
          call Conditioner%SetIrrep( DiffIrrep )
          !
          call Conditioner%ReadBlock( StorageDir, ConditionMat )
          !
          if (present(BsRemovedInf)) BsRemovedInf(k) = Self%SRCPWCBlock(i,k)%Block%nColumns()-ConditionMat%nColumns()
          call Self%SRCPWCBlock(i,k)%Block%Multiply( ConditionMat, 'Right', 'N' )
          call ConditionMat%Free()
          deallocate(StorageDir,DiffIrrep)
          do n = 1, NumPWCBra

             !.. < pwc Xlm | O | pwc Xlm >
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif
             if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCPWCBlock(n,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
             else
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC ) 
             end if
             call Self%PWCPWCBlock(n,k)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
             allocate( StorageDir, source = GetStorageDir(ketPWC) )
             !
             call Xlm%Init( ketPWC%GetL(), ketPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(ketPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCPWCBlock(n,k)%Block%Multiply( ConditionMat, 'Right', 'N' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             allocate( StorageDir, source = GetStorageDir(braPWC) )
             !
             call Xlm%Init( braPWC%GetL(), braPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(braPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCPWCBlock(n,k)%Block%Multiply( ConditionMat, 'Left', 'T' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             
          end do
       end do

    end do



    !
  end subroutine ClassSESSESBlockLoadMonoIonPolyCondition

  !> Reads the monocentric Gaussian components in ClassSESSESBlock class.
  subroutine ClassSESSESBlockLoadMonoIonPoly( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: NumLocBra, NumLocKet
    integer :: NumSRCBra, NumSRCKet
    integer :: NumPWCBra, NumPWCKet
    integer :: i,j,n,k
    !
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. Check that the ion index is between 1 and the max number of ions
!    if( IonBra < 1 .or. IonBra > Self%SpaceBra%GetNumChannels())then
!       call Assert('Wrong Bra ion index in call to MonoIon')
!    endif
!    if( IonKet < 1 .or. IonKet > Self%SpaceKet%GetNumChannels())then
!       call Assert('Wrong Ket ion index in call to MonoIon')
!    endif
    !
    NumLocBra = 0
    NumLocKet = 0
    if ( IonBra == 0 ) NumLocBra = 1
    if ( IonKet == 0 ) NumLocKet = 1
    NumSRCBra = 1
    NumSRCKet = 1
    if ( IonBra == 0 ) NumSRCBra = Self%SpaceBra%GetNumChannels()
    if ( IonKet == 0 ) NumSRCKet = Self%SpaceKet%GetNumChannels()
    NumPWCBra = 0
    NumPWCKet = 0
    if ( IonBra > 0 ) NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    if ( IonKet > 0 ) NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    allocate( Self%LocLocBlock( NumLocBra,NumLocKet ) )
    allocate( Self%LocSRCBlock( NumLocBra,NumSRCKet ) )
    allocate( Self%LocPWCBlock( NumLocBra,NumPWCKet ) )
    allocate( Self%SRCLocBlock( NumSRCBra,NumLocKet ) )
    allocate( Self%SRCSRCBlock( NumSRCBra,NumSRCKet ) )
    allocate( Self%SRCPWCBlock( NumSRCBra,NumPWCKet ) )
    allocate( Self%PWCLocBlock( NumPWCBra,NumLocKet ) )
    allocate( Self%PWCSRCBlock( NumPWCBra,NumSRCKet ) )
    allocate( Self%PWCPWCBlock( NumPWCBra,NumPWCKet ) )
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !

    !.. < * | O | loc >
    do j = 1, NumLocKet

       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
       end do
       
       !.. Sum over the parent ions
       do i = 1, NumSRCBra

          !.. < src | O | loc >

          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif
          if ( Self%BoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCLocBlock(i,j)%SetFormattedWrite()
          call Self%SRCLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          if ( IonBra == 0 ) then 
             call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%SRCLocBlock(i,j)%Load( BraSRC, KetLoc, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
          !.. < pwc | O | loc >
          do n = 1, NumPWCBra
             
             !.. < pwc Xlm | O | loc >
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif
             if ( Self%BoxStatesOnly      ) call Self%PWCLocBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly   ) call Self%PWCLocBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly   ) call Self%PWCLocBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCLocBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCLocBlock(n,j)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
             else
                call Self%PWCLocBlock(n,j)%SetFileExtension( FileExtensionPWCLoc  )
             end if
             call Self%PWCLocBlock(n,j)%Load( BraPWC, KetLoc, OpLabel, OpXlm )
             
          end do

       end do

    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       
       if ( IonKet == 0 ) then
          KetSRC => Self%SpaceKet%GetChannelSRC(j)
       else
          KetSRC => Self%SpaceKet%GetChannelSRC(IonKet)
       endif

       !.. < loc | O | src >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocSRCBlock(i,j)%SetFormattedWrite()
          call Self%LocSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          if ( IonKet == 0 ) then
             call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%LocSRCBlock(i,j)%Load( BraLoc, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
       end do

       do i = 1, NumSRCBra

          !.. < src | O | src >
          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif
          if ( Self%BoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCSRCBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCSRCBlock(i,j)%SetFormattedWrite()
          call Self%SRCSRCBlock(i,j)%SetFileExtension( GetQCFileExtension() )
          if ( IonBra == 0 ) then  
             if ( IonKet == 0) then
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyPoly = .TRUE., KetOnlyPoly = .TRUE. )
             else
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyPoly = .TRUE., KetOnlyMono = .TRUE. )
             endif
          else
             if ( IonKet == 0) then
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyMono = .TRUE., KetOnlyPoly = .TRUE. )
             else
                call Self%SRCSRCBlock(i,j)%Load( BraSRC, KetSRC, OpLabel, OpXlm, &
                BraOnlyMono = .TRUE., KetOnlyMono = .TRUE. )
             endif

          endif
          
          do n = 1, NumPWCBra
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             !.. < pwc Xlm | O | src >
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             else
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             end if
             if ( IonKet == 0 ) then
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
             else
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
             endif
             
          end do
          
       end do

    end do


    !.. < * | O | pwc >
    do k = 1, NumPWCKet
       KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,k)

       !.. < loc | O | pwc Xlm >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocPWCBlock(i,k)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocPWCBlock(i,k)%SetFormattedWrite()
          if ( present(ExtraLabel) ) then
             call Self%LocPWCBlock(i,k)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
          else
             call Self%LocPWCBlock(i,k)%SetFileExtension( FileExtensionLocPWC ) 
          end if
          call Self%LocPWCBlock(i,k)%Load( BraLoc, KetPWC, OpLabel, OpXlm )
       end do
       
       !.. < src | O | pwc Xlm >
       do i = 1, NumSRCBra
          if ( IonBra == 0 ) then
             BraSRC => Self%SpaceBra%GetChannelSRC(i)
          else
             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)
          endif

          if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,k)%SetFormattedWrite()
          if ( present(ExtraLabel) ) then
             call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
          else
             call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC ) 
          end if
          if ( IonBra == 0 ) then
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
          else
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
          do n = 1, NumPWCBra

             !.. < pwc Xlm | O | pwc Xlm >
             if ( IonBra == 0 ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(i,n)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif
             if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCPWCBlock(n,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
             else
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC ) 
             end if
             call Self%PWCPWCBlock(n,k)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
             
          end do
       end do

    end do



    !
  end subroutine ClassSESSESBlockLoadMonoIonPoly

  subroutine ClassSESSESBlockGetMonoIonPolyLastBs( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, LastBs, Mat, ExtraLabel, Conditioner )

    class(ClassSESSESBlock),                  intent(inout)    :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> Bra or Ket last bspline
    character(len=3)                           , intent(in)    :: LastBs
    !> Output Matrix
    type(ClassMatrix)                         , intent(out)   :: Mat 
    !> Temporal Matrix
    type(ClassMatrix)                                         :: tmpMat
    !> Class to be initialized.
    !
    class(ClassConditionerBlock), optional, intent(inout) :: Conditioner
    integer :: NumSRCBra, NumSRCKet
    integer :: NumPWCBra, NumPWCKet
    integer :: i,j,n,k
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !
    if ( IonBra == 0 ) call Assert( "Don't make sense <Loc|LastBs> = 0" )
    if ( IonKet == 0 ) call Assert( "Don't make sense <LastBs|Loc> = 0" )
    NumSRCBra = 1
    NumSRCKet = 1
    NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )

    if ( LastBs .eq. 'BRA' ) then
       j = NumPWCBra

    elseif ( LastBs .eq. 'KET' ) then
       j = NumPWCKet
    else
       call Assert('LastBs not well specified (BRA or KET)')
    endif
  
    call self%UnsetBoxOnly()
    do i = 1, j
       if (present(Conditioner)) then
          call Self%LoadMonoIonPolyLastBsCondition( SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, LastBs, i, Conditioner=Conditioner )
       else
          call Self%LoadMonoIonPolyLastBs( SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, LastBs, i )
       endif
       call Self%AssembleChChLastBs( IonBra, IonKet, LastBs, tmpMat )
       if ( LastBs .eq. 'BRA' ) then
          k = tmpMat%NColumns()
          if ( i .eq. 1 ) call Mat%InitFull(NumPWCBra,k)
          call Mat%ComposeFromBlocks(i, i, 1, k, tmpMat )
       endif
       if ( LastBs .eq. 'KET' ) then
          k = tmpMat%NRows()
          if ( i .eq. 1 ) call Mat%InitFull(k,NumPWCKet)
          call Mat%ComposeFromBlocks(1, k, i, i, tmpMat )
       endif
       call tmpMat%Free
    enddo
    !
  end subroutine ClassSESSESBlockGetMonoIonPolyLastBs


  subroutine ClassSESSESBlockLoadMonoIonPolyLastBs( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, LastBs, PWC, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> index of the PWC for the Las Bs
    integer                                    , intent(in)    :: PWC
    !> Bra or Ket last bspline
    character(len=3)                           , intent(in)    :: LastBs
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: NumSRCBra, NumSRCKet
    integer :: NumPWCBra, NumPWCKet
    integer :: i,j,n,k
    !
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. Check that the ion index is between 1 and the max number of ions
!    if( IonBra < 1 .or. IonBra > Self%SpaceBra%GetNumChannels())then
!       call Assert('Wrong Bra ion index in call to MonoIon')
!    endif
!    if( IonKet < 1 .or. IonKet > Self%SpaceKet%GetNumChannels())then
!       call Assert('Wrong Ket ion index in call to MonoIon')
!    endif
    !
    if ( IonBra == 0 ) call Assert( "Don't make sense <Loc|LastBs> = 0" )
    if ( IonKet == 0 ) call Assert( "Don't make sense <LastBs|Loc> = 0" )
    NumSRCBra = 1
    NumSRCKet = 1
    NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )
    if ( LastBs .eq. 'BRA' ) NumPWCBra = 1
    if ( LastBs .eq. 'KET' ) NumPWCKet = 1
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    if ( LastBs .eq. 'BRA' ) then
       allocate( Self%PWCSRCBlock(NumPWCBra,NumSRCKet) )
    elseif (  LastBs .eq. 'KET' ) then 
       allocate( Self%SRCPWCBlock(NumSRCBra,NumPWCKet) )
    else
       call Assert('LastBs is not well specified')
    endif
    allocate( Self%PWCPWCBlock( NumPWCBra,NumPWCKet ) )
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    if ( LastBs .eq. 'BRA' ) then
    !.. < * | O | src >
       do j = 1, NumSRCKet
          
          if ( IonKet == 0 ) then
             KetSRC => Self%SpaceKet%GetChannelSRC(j)
          else
             KetSRC => Self%SpaceKet%GetChannelSRC(IonKet)
          endif
          do n = 1, NumPWCBra
             if ( LastBs .eq. 'BRA' ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,PWC)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             !.. < pwc Xlm | O | src >
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             else
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             end if
             if ( IonKet == 0 ) then
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
             else
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
             endif
          end do
       end do
    endif

    !.. < * | O | pwc >
    do k = 1, NumPWCKet
      
      if ( LastBs .eq. 'KET' ) then
         KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,PWC)
      else
         KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,k)
      endif
      !.. < src | O | pwc Xlm >
      do i = 1, NumSRCBra
          if ( LastBs .eq. 'KET' ) then   

             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)

             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             else
                call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC ) 
             end if
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyMono = .TRUE. )
          endif
          !.. < pwc Xlm | O | pwc Xlm >
          do n = 1, NumPWCBra
             if ( LastBs .eq. 'BRA' ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,PWC)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCPWCBlock(n,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
             else
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC ) 
             end if
             call Self%PWCPWCBlock(n,k)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
             
          end do
       end do

    end do
    !
  end subroutine ClassSESSESBlockLoadMonoIonPolyLastBs


  subroutine ClassSESSESBlockLoadMonoIonPolyLastBsCondition( Self, SpaceBra, SpaceKet, OpLabel, IonBra, IonKet, LastBs, PWC, Axis, ExtraLabel, Conditioner )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> index of the Bra Ion
    integer                                    , intent(in)    :: IonBra
    !> index of the Ket Ion
    integer                                    , intent(in)    :: IonKet
    !> index of the PWC for the Las Bs
    integer                                    , intent(in)    :: PWC
    !> Bra or Ket last bspline
    character(len=3)                           , intent(in)    :: LastBs
    !> Dipole orientation.
    character(len=:), allocatable, optional    , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    class(ClassConditionerBlock), intent(inout) :: Conditioner
    integer :: NumSRCBra, NumSRCKet
    integer :: NumPWCBra, NumPWCKet
    integer :: i,j,n,k
    type(ClassMatrix) :: ConditionMat
    type(ClassXlm) :: Xlm,OverlapXlm
    character(len=:), allocatable :: StorageDir
    type(ClassIrrep), pointer :: DiffIrrep
    !
    type(ClassShortRangeChannel)        , pointer :: BraSRC, KetSRC
    type(ClassPartialWaveChannel)       , pointer :: BraPWC, KetPWC
    type(ClassXlm) :: OpXlm
    integer :: iPWCBra, iPWCKet
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. Check that the ion index is between 1 and the max number of ions
!    if( IonBra < 1 .or. IonBra > Self%SpaceBra%GetNumChannels())then
!       call Assert('Wrong Bra ion index in call to MonoIon')
!    endif
!    if( IonKet < 1 .or. IonKet > Self%SpaceKet%GetNumChannels())then
!       call Assert('Wrong Ket ion index in call to MonoIon')
!    endif
    !
    if ( IonBra == 0 ) call Assert( "Don't make sense <Loc|LastBs> = 0" )
    if ( IonKet == 0 ) call Assert( "Don't make sense <LastBs|Loc> = 0" )
    NumSRCBra = 1
    NumSRCKet = 1
    NumPWCBra = Self%SpaceBra%GetNumPWC( IonBra )
    NumPWCKet = Self%SpaceKet%GetNumPWC( IonKet )
    if ( LastBs .eq. 'BRA' ) NumPWCBra = 1
    if ( LastBs .eq. 'KET' ) NumPWCKet = 1
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    if ( allocated(Self%LocSRCBlock) ) deallocate( Self%LocSRCBlock )
    if ( allocated(Self%LocPWCBlock) ) deallocate( Self%LocPWCBlock )
    if ( allocated(Self%SRCLocBlock) ) deallocate( Self%SRCLocBlock )
    if ( allocated(Self%SRCSRCBlock) ) deallocate( Self%SRCSRCBlock )
    if ( allocated(Self%SRCPWCBlock) ) deallocate( Self%SRCPWCBlock )
    if ( allocated(Self%PWCLocBlock) ) deallocate( Self%PWCLocBlock )
    if ( allocated(Self%PWCSRCBlock) ) deallocate( Self%PWCSRCBlock )
    if ( allocated(Self%PWCPWCBlock) ) deallocate( Self%PWCPWCBlock )
    !
    if ( LastBs .eq. 'BRA' ) then
       allocate( Self%PWCSRCBlock(NumPWCBra,NumSRCKet) )
    elseif (  LastBs .eq. 'KET' ) then 
       allocate( Self%SRCPWCBlock(NumSRCBra,NumPWCKet) )
    else
       call Assert('LastBs is not well specified')
    endif
    allocate( Self%PWCPWCBlock( NumPWCBra,NumPWCKet ) )
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    if ( LastBs .eq. 'BRA' ) then
    !.. < * | O | src >
       do j = 1, NumSRCKet
          
          if ( IonKet == 0 ) then
             KetSRC => Self%SpaceKet%GetChannelSRC(j)
          else
             KetSRC => Self%SpaceKet%GetChannelSRC(IonKet)
          endif
          do n = 1, NumPWCBra
             if ( LastBs .eq. 'BRA' ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,PWC)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             !.. < pwc Xlm | O | src >
             if ( Self%BoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCSRCBlock(n,j)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCSRCBlock(n,j)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             else
                call Self%PWCSRCBlock(n,j)%SetFileExtension( FileExtensionPWCSRC  ) 
             end if
             if ( IonKet == 0 ) then
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyPoly = .TRUE. )
             else
                call Self%PWCSRCBlock(n,j)%Load( BraPWC, KetSRC, OpLabel, OpXlm, OnlyMono = .TRUE. )
             endif
             allocate( StorageDir, source = GetStorageDir(braPWC) )
             !
             call Xlm%Init( braPWC%GetL(), braPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(braPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCSRCBlock(n,j)%Block%Multiply( ConditionMat, 'Left', 'T' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
          end do
       end do
    endif

    !.. < * | O | pwc >
    do k = 1, NumPWCKet
      
      if ( LastBs .eq. 'KET' ) then
         KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,PWC)
      else
         KetPWC => Self%SpaceKet%GetChannelPWC(IonKet,k)
      endif
      !.. < src | O | pwc Xlm >
      do i = 1, NumSRCBra
          if ( LastBs .eq. 'KET' ) then   

             BraSRC => Self%SpaceBra%GetChannelSRC(IonBra)

             if ( Self%BoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%SRCPWCBlock(i,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%SRCPWCBlock(i,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             else
                call Self%SRCPWCBlock(i,k)%SetFileExtension( FileExtensionSRCPWC ) 
             end if
             call Self%SRCPWCBlock(i,k)%Load( BraSRC, KetPWC, OpLabel, OpXlm, OnlyMono = .TRUE. )
             allocate( StorageDir, source = GetStorageDir(ketPWC) )
             !
             call Xlm%Init( ketPWC%GetL(), ketPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(ketPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%SRCPWCBlock(i,k)%Block%Multiply( ConditionMat, 'Right', 'N' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
          endif
          !.. < pwc Xlm | O | pwc Xlm >
          do n = 1, NumPWCBra
             if ( LastBs .eq. 'BRA' ) then
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,PWC)
             else
                BraPWC => Self%SpaceBra%GetChannelPWC(IonBra,n)
             endif

             if ( Self%BoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBoxOnly()
             if ( Self%BraBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetBraBoxOnly()
             if ( Self%KetBoxStatesOnly ) call Self%PWCPWCBlock(n,k)%SetKetBoxOnly()
             if ( Self%FormattedWrite ) call Self%PWCPWCBlock(n,k)%SetFormattedWrite()
             if ( present(ExtraLabel) ) then
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
             else
                call Self%PWCPWCBlock(n,k)%SetFileExtension( FileExtensionPWCPWC ) 
             end if
             call Self%PWCPWCBlock(n,k)%Load( BraPWC, KetPWC, OpLabel, OpXlm )
             allocate( StorageDir, source = GetStorageDir(ketPWC) )
             !
             call Xlm%Init( ketPWC%GetL(), ketPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(ketPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCPWCBlock(n,k)%Block%Multiply( ConditionMat, 'Right', 'N' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             allocate( StorageDir, source = GetStorageDir(braPWC) )
             !
             call Xlm%Init( braPWC%GetL(), braPWC%GetM() )
             call Conditioner%SetXlm( Xlm )
             !
             OverlapXlm = GetOperatorXlm(OverlapLabel,Axis)
             allocate( DiffIrrep, source = GetDiffuseIrrep(braPWC,OverlapXlm) )
             call Conditioner%SetIrrep( DiffIrrep )
             !
             call Conditioner%ReadBlock( StorageDir, ConditionMat )
             !
             call Self%PWCPWCBlock(n,k)%Block%Multiply( ConditionMat, 'Left', 'T' )
             call ConditionMat%Free()
             deallocate(StorageDir,DiffIrrep)
             
          end do
       end do

    end do
    !
  end subroutine ClassSESSESBlockLoadMonoIonPolyLastBsCondition




  !> Reads the localized components in ClassSESSESBlock class.
  !! In general just the polycentric submatrix.
  subroutine ClassSESSESBlockLoadLoc( Self, SpaceBra, SpaceKet, OpLabel, Axis, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock),                  intent(inout) :: Self
    !> Space of the Bra functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceBra
    !> Space of the Ket functions.
    type(ClassSymmetricElectronicSpace), target, intent(in)    :: SpaceKet
    !> Operator Label.
    character(len=*),                            intent(in)    :: OpLabel
    !> Dipole orientation.
    character(len=:), allocatable              , intent(inout) :: Axis
    !> Extra label to form the block name
    character(len=*), optional                 , intent(in)    :: ExtraLabel
    !
    integer :: i, j, k, n
    integer :: NumLocBra, NumLocKet
    type(ClassSymmetricLocalizedStates) , pointer :: BraLoc, KetLoc
    type(ClassXlm) :: OpXlm
    !
    Self%SpaceBra => SpaceBra
    Self%SpaceKet => SpaceKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !
    if ( allocated(Self%LocLocBlock) ) deallocate( Self%LocLocBlock )
    !
    allocate( Self%LocLocBlock(NumLocBra,NumLocKet) )
    !
    !
    OpXlm = GetOperatorXlm(OpLabel,Axis)
    !
    !.. < * | O | loc >
    do j = 1, NumLocKet
       
       KetLoc  => Self%SpaceKet%GetChannelLS()

       !.. < loc | O | loc >
       do i = 1, NumLocBra
          BraLoc  => Self%SpaceBra%GetChannelLS()
          if ( Self%BoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBoxOnly()
          if ( Self%BraBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetBraBoxOnly()
          if ( Self%KetBoxStatesOnly ) call Self%LocLocBlock(i,j)%SetKetBoxOnly()
          if ( Self%FormattedWrite ) call Self%LocLocBlock(i,j)%SetFormattedWrite()
          call Self%LocLocBlock(i,j)%SetFileExtension( GetQCFileExtension() ) 
          call Self%LocLocBlock(i,j)%Load( BraLoc, KetLoc, OpLabel, OpXlm )
       end do
       
    end do
    
    !
  end subroutine ClassSESSESBlockLoadLoc




  !> Save the ClassSESSESBlock class.
  subroutine ClassSESSESBlockSave( Self, ExtraLabel )
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    character(len=*), optional, intent(in)    :: ExtraLabel
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( .not.allocated(Self%LocLocBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%LocSRCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%LocPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%SRCLocBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%SRCSRCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%SRCPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCLocBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCSRCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    !

    !.. < * | O | loc >
    do j = 1, NumLocKet
       !.. < loc | O | loc >
       do i = 1, NumLocBra
          call Self%LocLocBlock(i,j)%Save( )
       end do
       !.. Sum over the parent ions
       do i = 1, NumSRCBra
          !.. < src | O | loc >
          call Self%SRCLocBlock(i,j)%Save( )
          !.. < pwc | O | loc >
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             !.. < pwc Xlm | O | loc >
             CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
             if ( present(ExtraLabel) ) then
                call Self%PWCLocBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCLoc//ExtraLabel )
             end if
             call Self%PWCLocBlock(CounterBra,j)%Save( )
          end do
       end do
    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       !.. < loc | O | src >
       do i = 1, NumLocBra
          call Self%LocSRCBlock(i,j)%Save( )
       end do
       do i = 1, NumSRCBra
          !.. < src | O | src >
          call Self%SRCSRCBlock(i,j)%Save( )
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             !.. < pwc Xlm | O | src >
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             if ( present(ExtraLabel) ) then
                call Self%PWCSRCBlock(CounterBra,j)%SetFileExtension( FileExtensionPWCSRC//ExtraLabel  ) 
             end if
             call Self%PWCSRCBlock(CounterBra,j)%Save( )
          end do
       end do
    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)
          !.. < loc | O | pwc Xlm >
          do i = 1, NumLocBra
             if ( present(ExtraLabel) ) then
                call Self%LocPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionLocPWC//ExtraLabel ) 
             end if
             call Self%LocPWCBlock(i,CounterKet)%Save( )
          end do
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             if ( present(ExtraLabel) ) then
                call Self%SRCPWCBlock(i,CounterKet)%SetFileExtension( FileExtensionSRCPWC//ExtraLabel ) 
             end if
             call Self%SRCPWCBlock(i,CounterKet)%Save( )
             do n = 1, Self%SpaceBra%GetNumPWC(i)
                !.. < pwc Xlm | O | pwc Xlm >
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                if ( present(ExtraLabel) ) then
                   call Self%PWCPWCBlock(CounterBra,CounterKet)%SetFileExtension( FileExtensionPWCPWC//ExtraLabel ) 
                end if
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Save( )
             end do
          end do
       end do
    end do


    !
  end subroutine ClassSESSESBlockSave



  !> Gets the number of rows and columns of the full quantum chemistry
  !! operator matrices.
  subroutine  ClassSESSESBlockGetNumFunQC( Self, LoadLocStates, NBra, NKet ) 
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(in)  :: Self
    logical                   , intent(in)  :: LoadLocStates
    integer                   , intent(out) :: NBra
    integer                   , intent(out) :: NKet
    !
    integer :: i, j
    integer :: NumSRCBra, NumSRCKet, NumLocBra, NumLocKet
    !
    NBra = 0
    NKet = 0
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()

    if ( LoadLocStates ) then
       !.. < * | O | loc >
       do j = 1, NumLocKet
          !.. < loc | O | loc >
          do i = 1, NumLocBra
             if ( j == 1 ) NBra = NBra + Self%LocLocBlock(i,j)%GetNRows()
          end do
          NKet = NKet + Self%LocLocBlock(1,j)%GetNColumns()
          !.. Sum over the parent ions
          do i = 1, NumSRCBra
             !.. < src | O | loc >
             if ( j == 1 ) NBra = NBra + Self%SRCLocBlock(i,j)%GetNRows()
          end do
          NKet = NKet + Self%SRCLocBlock(1,j)%GetNColumns()
       end do
    end if
    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       if ( LoadLocStates ) then
          !.. < loc | O | src >
          do i = 1, NumLocBra
             if ( j == 1 ) NBra = NBra + Self%LocSRCBlock(i,j)%GetNRows()
          end do
          NKet = NKet + Self%LocSRCBlock(1,j)%GetNColumns()
       end if
       do i = 1, NumSRCBra
          !.. < src | O | src >
          if ( j == 1 ) NBra = NBra + Self%SRCSRCBlock(i,j)%GetNRows()
       end do
       NKet = NKet + Self%SRCSRCBlock(1,j)%GetNColumns()
    end do
    !
  end subroutine ClassSESSESBlockGetNumFunQC



  !> Condition the blocks.
  subroutine ClassSESSESBlockCondition( Self, Conditioner )
    !> Class to be initialized.
    class(ClassSESSESBlock)     , intent(inout) :: Self
    class(ClassConditionerBlock), intent(inout) :: Conditioner
    !
    integer :: i, j, k, n, CounterKet, CounterBra
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    type(LocSRCBlock), allocatable :: OriginalLocSRCBlock(:,:)
    type(SRCLocBlock), allocatable :: OriginalSRCLocBlock(:,:)
    type(SRCSRCBlock), allocatable :: OriginalSRCSRCBlock(:,:)
    type(SRCPWCBlock), allocatable :: OriginalSRCPWCBlock(:,:)
    type(PWCSRCBlock), allocatable :: OriginalPWCSRCBlock(:,:)
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()
    !
    if ( .not.allocated(Self%LocPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%SRCPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCLocBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCSRCBlock) ) call Assert( 'Sub-block not allocated.' )
    if ( .not.allocated(Self%PWCPWCBlock) ) call Assert( 'Sub-block not allocated.' )
    !

    allocate( OriginalLocSRCBlock, source = Self%LocSRCBlock(:,:) )
    allocate( OriginalSRCLocBlock, source = Self%SRCLocBlock(:,:) )
    allocate( OriginalSRCSRCBlock, source = Self%SRCSRCBlock(:,:) )
    allocate( OriginalSRCPWCBlock, source = Self%SRCPWCBlock(:,:) )
    allocate( OriginalPWCSRCBlock, source = Self%PWCSRCBlock(:,:) )


    !.. < * | O | loc >
    do j = 1, NumLocKet
       !.. Sum over the parent ions
       do i = 1, NumSRCBra
          !.. < pwc | O | loc >
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             !.. < pwc Xlm | O | loc >
             CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
             call Self%PWCLocBlock(CounterBra,j)%Condition( Conditioner, OriginalSRCLocBlock(i,j) )
          end do
       end do
    end do

    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       do i = 1, NumSRCBra
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             !.. < pwc Xlm | O | src >
             CounterBra =  Self%SpaceBra%PWCChannelIndex(i,n)
             call Self%PWCSRCBlock(CounterBra,j)%Condition( Conditioner, OriginalSRCSRCBlock(i,j) )
          end do
       end do
    end do


    !.. < * | O | pwc >
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)
          !.. < loc | O | pwc Xlm >
          do i = 1, NumLocBra
             call Self%LocPWCBlock(i,CounterKet)%Condition( Conditioner, OriginalLocSRCBlock(i,j) )
          end do
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             call Self%SRCPWCBlock(i,CounterKet)%Condition( Conditioner, OriginalSRCSRCBlock(i,j) )
             do n = 1, Self%SpaceBra%GetNumPWC(i)
                !.. < pwc Xlm | O | pwc Xlm >
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                call Self%PWCPWCBlock(CounterBra,CounterKet)%Condition( Conditioner, OriginalSRCSRCBlock(i,j),&
                  OriginalPWCSRCBlock(CounterBra,j), OriginalSRCPWCBlock(i,CounterKet) )
             end do
          end do
       end do
    end do


    !
  end subroutine ClassSESSESBlockCondition



  !> Gets the minimum number of not overlapping B-splines among all channels.
  integer function ClassSESSESBlockGetMinNumNotOvBs( Self ) result(NBs)
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(in) :: Self
    !
    if (.not.allocated(Self%PWCSRCBlock) ) call Assert( &
         'The PWCSRC block is not allocated, impossible to get '//&
         'the minimum number of B-splines that do not overlap '//&
         'with the diffuse orbitals.' )
    !.. For every symmetric electronic space only one localized channel is present.
    NBs = GetMinNumNotOverlapingBs(Self%PWCSRCBlock,'Rows')
    !
  end function ClassSESSESBlockGetMinNumNotOvBs




  !> Assemble in one matrix all the blocks in ClassSESSESBlock class.
  subroutine ClassSESSESBlockAssemble( Self, LoadLocStates, OpMat )
    !> Class to be assemble.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    logical                   , intent(in)    :: LoadLocStates
    !> Operator matrix in the symmetric electronic space.
    class(ClassMatrix)        , intent(out)   :: OpMat
    !
    integer :: i, j, k, n, CounterBra, CounterKet, LocIndex
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:), LocSRCMat(:,:), LocPWCMat(:,:)
    type(ClassMatrix), allocatable :: SRCLocMat(:,:), SRCSRCMat(:,:), SRCPWCMat(:,:)
    type(Classmatrix), allocatable :: PWCLocMat(:,:), PWCSRCMat(:,:), PWCPWCMat(:,:)
    type(ClassMatrix) :: FullLocLoc, FullLocSRC, FullLocPWC 
    type(ClassMatrix) :: FullSRCLoc, FullSRCSRC, FullSRCPWC 
    type(ClassMatrix) :: FullPWCLoc, FullPWCSRC, FullPWCPWC 
    type(ClassMatrix), allocatable :: ArrayMat(:,:)
    !XCHEMQC
    character*32 :: ColumnLabel
    character*32 :: RowLabel
    !END XCEHMQC

    !
    if ( allocated(Self%SRCSRCBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    !.. It's better for consistencies always compute the permutation
    ! of the decouple B-splines at the assemble step.
    if ( DoBsPermutation ) then
       call Self%SetAsympBsPermutation()
    end if
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !
    NumPWCBra = Self%SpaceBra%GetNumPWC()
    NumPWCKet = Self%SpaceKet%GetNumPWC()

    if ( LoadLocStates ) then
       allocate( LocLocMat(NumLocBra,NumLocKet) )
       allocate( LocSRCMat(NumLocBra,NumSRCKet) )
       allocate( LocPWCMat(NumLocBra,NumPWCKet) )
       allocate( SRCLocMat(NumSRCBra,NumLocKet) )
       allocate( PWCLocMat(NumPWCBra,NumLocKet) )
    end if
    allocate( SRCSRCMat(NumSRCBra,NumSRCKet) )
    allocate( SRCPWCMat(NumSRCBra,NumPWCKet) )
    allocate( PWCSRCMat(NumPWCBra,NumSRCKet) )
    allocate( PWCPWCMat(NumPWCBra,NumPWCKet) )

    if ( LoadLocStates ) then
       !.. < * | O | loc >
       do j = 1, NumLocKet
          !.. < loc | O | loc >
          do i = 1, NumLocBra
             LocLocMat(i,j) = Self%LocLocBlock(i,j)%FetchBlock( )
             !XCHEMQC Set Block Names
             ColumnLabel="Local State #"
             RowLabel="Local State #"
             call LocLocMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true. )
             call LocLocMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true. )
             !END XCHEMQC
          end do
          !.. Sum over the parent ions
          do i = 1, NumSRCBra
             !.. < src | O | loc >
             SRCLocMat(i,j) = Self%SRCLocBlock(i,j)%FetchBlock( )
             !if ( Self%DecoupleChannels ) SRCLocMat(i,j) = 0.d0
             !XCHEMQC
             ColumnLabel="Local State #"
             RowLabel   =trim(Self%SpaceBra%GetPILabel(i))//" x GO # "
             call SRCLocMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
             call SRCLocMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
             !END XCHEMQC
             !.. < pwc | O | loc >
             do n = 1, Self%SpaceBra%GetNumPWC(i)
                !.. < pwc Xlm | O | loc >
                CounterBra              = Self%SpaceBra%PWCChannelIndex(i,n)
                PWCLocMat(CounterBra,j) = Self%PWCLocBlock(CounterBra,j)%FetchBlock( )
                !if ( Self%DecoupleChannels ) PWCLocMat(CounterBra,j) = 0.d0
                if ( Self%PWCLocBlock(CounterBra,j)%IsBoxOnly()    ) call PWCLocMat(CounterBra,j)%RemoveRows(1,'END')
                if ( Self%PWCLocBlock(CounterBra,j)%IsBraBoxOnly() ) call PWCLocMat(CounterBra,j)%RemoveRows(1,'END')
                !XCHEMQC
                ColumnLabel="Local State #"
                write(RowLabel,'(a,i0,a,i0)') trim(self%SpaceBra%GetPILabel(i))//" x BSO l=", self%SpaceBra%GetL(i,n),&
                  " m=",self%SpaceBra%GetM(i,n)
                call PWCLocMat(CounterBra,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
                call PWCLocMat(CounterBra,j)%SetRowLabels   (RowLabel)
                !END XCHEMQC

             end do
          end do
       end do
    end if
    
    do j = 1, NumSRCKet
       if ( LoadLocStates ) then
          !.. < loc | O | src >
          do i = 1, NumLocBra
             LocSRCMat(i,j) = Self%LocSRCBlock(i,j)%FetchBlock( )
             !if ( Self%DecoupleChannels ) LocSRCMat(i,j) = 0.d0
             !XCHEMQC
             ColumnLabel=trim(self%SpaceKet%GetPILabel(j))//" x GO # "
             RowLabel="Local State #"
             call LocSRCMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
             call LocSRCMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
             !END XCHEMQC
          end do
       end if
       do i = 1, NumSRCBra
          !.. < src | O | src >
          SRCSRCMat(i,j) = Self%SRCSRCBlock(i,j)%FetchBlock( )
          if ( Self%DecoupleChannels ) then 
            if ( trim(self%SpaceKet%GetPILabel(j)) .ne. trim(self%SpaceBra%GetPILabel(i)) ) SRCSRCMat(i,j) = 0.d0
          endif
          !XCHEMQC
          ColumnLabel=trim(self%SpaceKet%GetPILabel(j))//" x GO # "
          RowLabel   =trim(self%SpaceBra%GetPILabel(i))//" x GO # "
          call SRCSRCMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
          call SRCSRCMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
          !END XCHEMQC
          do n = 1, Self%SpaceBra%GetNumPWC(i)
             !.. < pwc Xlm | O | src >
             CounterBra              =  Self%SpaceBra%PWCChannelIndex(i,n)
             PWCSRCMat(CounterBra,j) = Self%PWCSRCBlock(CounterBra,j)%FetchBlock( )
             if ( Self%DecoupleChannels ) then 
               if ( trim(self%SpaceKet%GetPILabel(j)) .ne. trim(self%SpaceBra%GetPILabel(i)) ) PWCSRCMat(CounterBra,j) = 0.d0
             endif
             if ( Self%PWCSRCBlock(CounterBra,j)%IsBoxOnly()    ) call PWCSRCMat(CounterBra,j)%RemoveRows(1,'END')
             if ( Self%PWCSRCBlock(CounterBra,j)%IsBraBoxOnly() ) call PWCSRCMat(CounterBra,j)%RemoveRows(1,'END')
             !XCHEMQC
             ColumnLabel=trim(self%SpaceKet%GetPILabel(j))//" x GO # "
             write(RowLabel,'(a,i0,a,i0)') trim(self%SpaceBra%GetPILabel(i))//" x BSO l=", self%SpaceBra%GetL(i,n)," m=",&
               self%SpaceBra%GetM(i,n)
             call PWCSRCMat(CounterBra,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
             call PWCSRCMat(CounterBra,j)%SetRowLabels   (RowLabel)
             !END XCHEMQC
          end do
       end do
    end do


    !.. < * | O | pwc >
    !XCHEMQC
    write(6,'(a)') "-----  < * | O | pwc > -----"
    !END XCHEMQC
    do j = 1, NumSRCKet
       do k = 1, Self%SpaceKet%GetNumPWC(j)
          CounterKet = Self%SpaceKet%PWCChannelIndex(j,k)
          if ( LoadLocStates ) then
             !.. < loc | O | pwc Xlm >
             do i = 1, NumLocBra
                LocPWCMat(i,CounterKet) = Self%LocPWCBlock(i,CounterKet)%FetchBlock( )
                !if ( Self%DecoupleChannels ) LocPWCMat(i,CounterKet) = 0.d0
                if ( Self%LocPWCBlock(i,CounterKet)%IsBoxOnly()    ) call LocPWCMat(i,CounterKet)%RemoveColumns(1,'END')
                if ( Self%LocPWCBlock(i,CounterKet)%IsKetBoxOnly() ) call LocPWCMat(i,CounterKet)%RemoveColumns(1,'END')
                !XCHEMQC
                write(ColumnLabel, '(a,i0,a,i0)') trim(self%SpaceKet%GetPILabel(j))//" x BSO l=", self%SpaceKet%GetL(j,k)," m=",&
                  self%SpaceKet%GetM(j,k)
                RowLabel="Loc"
                call LocPWCMat(i,CounterKet)%SetColumnLabels(ColumnLabel)
                call LocPWCMat(i,CounterKet)%SetRowLabels   (RowLabel, enumerate = .true.)
                !END XCHEMQC
             end do
          end if
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             SRCPWCMat(i,CounterKet) = Self%SRCPWCBlock(i,CounterKet)%FetchBlock( )
             if ( Self%DecoupleChannels ) then 
               if ( trim(self%SpaceKet%GetPILabel(j)) .ne. trim(self%SpaceBra%GetPILabel(i)) ) SRCPWCMat(i,CounterKet) = 0.d0
             endif
             if ( Self%SRCPWCBlock(i,CounterKet)%IsBoxOnly()    ) call SRCPWCMat(i,CounterKet)%RemoveColumns(1,'END')
             if ( Self%SRCPWCBlock(i,CounterKet)%IsKetBoxOnly() ) call SRCPWCMat(i,CounterKet)%RemoveColumns(1,'END')
             !XCHEMQC
             write(ColumnLabel, '(a,i0,a,i0)') trim(self%SpaceKet%GetPILabel(j))//" x BSO l=", self%SpaceKet%GetL(j,k)," m=",&
               self%SpaceKet%GetM(j,k)
             RowLabel=trim(self%SpaceBra%GetPILabel(i))//" x GO # "
             call SRCPWCMat(i,CounterKet)%SetColumnLabels(ColumnLabel)
             call SRCPWCMat(i,CounterKet)%SetRowLabels   (RowLabel, enumerate = .true.)
             !END XCHEMQC
             do n = 1, Self%SpaceBra%GetNumPWC(i)
                !.. < pwc Xlm | O | pwc Xlm >
                CounterBra = Self%SpaceBra%PWCChannelIndex(i,n)
                PWCPWCMat(CounterBra,CounterKet) = Self%PWCPWCBlock(CounterBra,CounterKet)%FetchBlock( )
                if ( Self%DecoupleChannels ) then 
                  if ( trim(self%SpaceKet%GetPILabel(j)) .ne. trim(self%SpaceBra%GetPILabel(i)) ) then
                    PWCPWCMat(CounterBra,CounterKet) = 0.d0
                  endif
                endif
                if ( Self%PWCPWCBlock(CounterBra,CounterKet)%IsBoxOnly() ) then
                    call PWCPWCMat(CounterBra,CounterKet)%RemoveColumns(1,'END')
                endif
                if ( Self%PWCPWCBlock(CounterBra,CounterKet)%IsKetBoxOnly() ) then
                    call PWCPWCMat(CounterBra,CounterKet)%RemoveColumns(1,'END')
                endif
                if ( Self%PWCPWCBlock(CounterBra,CounterKet)%IsBoxOnly() ) then
                    call PWCPWCMat(CounterBra,CounterKet)%RemoveRows(1,'END')
                endif
                if ( Self%PWCPWCBlock(CounterBra,CounterKet)%IsBraBoxOnly() ) then
                    call PWCPWCMat(CounterBra,CounterKet)%RemoveRows(1,'END')
                endif
                !XCHEMQC
                write(ColumnLabel, '(a,i0,a,i0)') trim(self%SpaceKet%GetPILabel(j))//" x BSO l=", self%SpaceKet%GetL(j,k)," m=",&
                  self%SpaceKet%GetM(j,k)
                write(RowLabel,    '(a,i0,a,i0)') trim(self%SpaceBra%GetPILabel(i))//" x BSO l=", self%SpaceBra%GetL(i,n)," m=",&
                  self%SpaceBra%GetM(i,n)
                call PWCPWCMat(CounterBra,CounterKet)%SetColumnLabels(ColumnLabel)
                call PWCPWCMat(CounterBra,CounterKet)%SetRowLabels   (RowLabel)
                !END XCHEMQC
             end do
          end do
       end do
    end do

    if ( LoadLocStates ) then
       !
       call FullLocLoc%BuildUpMatrix( LocLocMat )
       deallocate( LocLocMat )
       call FullLocSRC%BuildUpMatrix( LocSRCMat )
       deallocate( LocSRCMat )
       call FullLocPWC%BuildUpMatrix( LocPWCMat )
       if ( Self%AsympBsPermutation ) then
          write(output_unit,*) "Permuting LocPWCMat's columns"
          call PermuteBsplines( FullLocPWC, LocPWCMat, Self%LocPWCBlock, 'Columns' )
       end if
       deallocate( LocPWCMat )
       call FullSRCLoc%BuildUpMatrix( SRCLocMat )
       deallocate( SRCLocMat )
       call FullPWCLoc%BuildUpMatrix( PWCLocMat )
       if ( Self%AsympBsPermutation ) then
          write(output_unit,*) "Permuting PWCLocMat's rows"
          call PermuteBsplines( FullPWCLoc, PWCLocMat, Self%PWCLocBlock, 'Rows' )
       end if
       !
       deallocate( PWCLocMat )
    end if
    !
    !
    call FullSRCSRC%BuildUpMatrix( SRCSRCMat )
    deallocate( SRCSRCMat )
    call FullSRCPWC%BuildUpMatrix( SRCPWCMat )
    if ( Self%AsympBsPermutation ) then
       write(output_unit,*) "Permuting SRCPWCMat's columns"
       call PermuteBsplines( FullSRCPWC, SRCPWCMat, Self%SRCPWCBlock, 'Columns' )
    end if
    deallocate( SRCPWCMat )
    call FullPWCSRC%BuildUpMatrix( PWCSRCMat )
    if ( Self%AsympBsPermutation ) then
       write(output_unit,*) "Permuting PWCSRCMat's rows"
       call PermuteBsplines( FullPWCSRC, PWCSRCMat, Self%PWCSRCBlock, 'Rows' )
    end if
    deallocate( PWCSRCMat )
    call FullPWCPWC%BuildUpMatrix( PWCPWCMat )
    if ( Self%AsympBsPermutation ) then
       write(output_unit,*) "Permuting PWCPWCMat's rows"
       call PermuteBsplines( FullPWCPWC, PWCPWCMat, Self%PWCPWCBlock, 'Rows' )
       write(output_unit,*) "Permuting PWCPWCMat's columns"
       call PermuteBsplines( FullPWCPWC, PWCPWCMat, Self%PWCPWCBlock, 'Columns' )
    end if
    !
    deallocate( PWCPWCMat )

    LocIndex = 1
    if ( LoadLocStates ) LocIndex = 0
    
    allocate( ArrayMat(3-LocIndex,3-LocIndex) )
    !
    if ( LoadLocStates ) then
       ArrayMat(1-LocIndex,1-LocIndex) = FullLocLoc
       call FullLocLoc%Free()
       ArrayMat(1-LocIndex,2-LocIndex) = FullLocSRC
       call FullLocSRC%Free()
       ArrayMat(1-LocIndex,3-LocIndex) = FullLocPWC
       call FullLocPWC%Free()
       !
       ArrayMat(2-LocIndex,1-LocIndex) = FullSRCLoc
       call FullSRCLoc%Free()
       !
       ArrayMat(3-LocIndex,1-LocIndex) = FullPWCLoc
       call FullPWCLoc%Free()
    end if
    !
    ArrayMat(2-LocIndex,2-LocIndex) = FullSRCSRC
    call FullSRCSRC%Free()
    ArrayMat(2-LocIndex,3-LocIndex) = FullSRCPWC
    call FullSRCPWC%Free()
    ArrayMat(3-LocIndex,2-LocIndex) = FullPWCSRC
    call FullPWCSRC%Free()
    ArrayMat(3-LocIndex,3-LocIndex) = FullPWCPWC
    call FullPWCPWC%Free()
    
    call OpMat%BuildUpMatrix( ArrayMat )
    !XCHEMQC
    write(6,'(A,L)') "Columns Labelled?", OpMat%ColumnsLabelled()

    deallocate( ArrayMat )
    !
  end subroutine ClassSESSESBlockAssemble

  subroutine ClassSESSESBlockAssembleChChLastBs( Self, PIIndexBra, PIIndexKet, LastBs, OpMat )
    !> Class to be assemble.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    !> Operator matrix in the symmetric electronic space.
    class(ClassMatrix)        , intent(out)   :: OpMat
    integer                   , intent(in)    :: PIIndexBra
    integer                   , intent(in)    :: PIIndexKet
    character(len=3)          , intent(in)    :: LastBs
    !
    integer :: i, j, k, n, IonIndexBra, IonIndexKet
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:), LocSRCMat(:,:), LocPWCMat(:,:)
    type(ClassMatrix), allocatable :: SRCLocMat(:,:), SRCSRCMat(:,:), SRCPWCMat(:,:)
    type(Classmatrix), allocatable :: PWCLocMat(:,:), PWCSRCMat(:,:), PWCPWCMat(:,:)
    type(ClassMatrix), allocatable :: ArrayMat(:,:)

    !
    if ( allocated(Self%SRCPWCBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    NumPWCBra = size( Self%PWCPWCBlock, 1 )
    NumPWCKet = size( Self%PWCPWCBlock, 2 )
    NumSRCBra = size( Self%SRCPWCBlock, 1 )
    NumSRCKet = size( Self%PWCSRCBlock, 2 )
    allocate( LocLocMat(0,0) )
    allocate( LocSRCMat(0,0) )
    allocate( LocPWCMat(0,0) )
    allocate( SRCLocMat(0,0) )
    allocate( SRCSRCMat(0,0) )
    allocate( PWCLocMat(0,0) )
    if ( LastBs .eq. 'BRA' ) then
       allocate( SRCPWCMat(0,0) )
       allocate( PWCSRCMat(NumPWCBra,NumSRCKet) )
    elseif (  LastBs .eq. 'KET' ) then 
       allocate( SRCPWCMat(NumSRCBra,NumPWCKet) )
       allocate( PWCSRCMat(0,0) )
    else
       call Assert('LastBs is not well specified')
    endif
    allocate( PWCPWCMat(NumPWCBra,NumPWCKet) )

    if ( LastBs .eq. 'BRA' ) then
    !.. < * | O | src >
       do j = 1, NumSRCKet
          IonIndexKet = PIIndexKet
          do n = 1, NumPWCBra
             !.. < pwc Xlm | O | src >
             PWCSRCMat(n,j) = Self%PWCSRCBlock(n,j)%FetchBlock( )
             if ( LastBs .eq. 'BRA' ) then
               call PWCSRCMat(n,j)%RemoveRows(PWCSRCMat(n,j)%NRows()-1,'START')
             else
               call PWCSRCMat(n,j)%RemoveRows(1,'END')
             endif
          end do
       end do
    endif

    !.. < * | O | pwc >

    !.. Only considers a single ion

    do k = 1, NumPWCKet


       if ( LastBs .eq. 'KET' ) then
          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             SRCPWCMat(i,k) = Self%SRCPWCBlock(i,k)%FetchBlock( )
             call SRCPWCMat(i,k)%RemoveColumns(SRCPWCMat(i,k)%NColumns()-1,'START')
          enddo
       endif
    !.. < pwc Xlm | O | pwc Xlm >

       do n = 1, NumPWCBra

          PWCPWCMat(n,k) = Self%PWCPWCBlock(n,k)%FetchBlock( )

          if ( LastBs .eq. 'KET' ) then
            call PWCPWCMat(n,k)%RemoveColumns(PWCPWCMat(n,k)%NColumns()-1,'START')
          else
            call PWCPWCMat(n,k)%RemoveColumns(1,'END')
          endif

          if ( LastBs .eq. 'BRA' ) then
            call PWCPWCMat(n,k)%RemoveRows(PWCPWCMat(n,k)%NRows()-1,'START')
          else
            call PWCPWCMat(n,k)%RemoveRows(1,'END')
          endif
       enddo
    enddo

    allocate( ArrayMat(3,3) )
    call ArrayMat(1,1)%BuildUpMatrix( LocLocMat ); deallocate( LocLocMat )
    call ArrayMat(1,2)%BuildUpMatrix( LocSRCMat ); deallocate( LocSRCMat )
    call ArrayMat(1,3)%BuildUpMatrix( LocPWCMat ); deallocate( LocPWCMat )
    call ArrayMat(2,1)%BuildUpMatrix( SRCLocMat ); deallocate( SRCLocMat )
    call ArrayMat(2,2)%BuildUpMatrix( SRCSRCMat ); deallocate( SRCSRCMat )
    call ArrayMat(2,3)%BuildUpMatrix( SRCPWCMat ); deallocate( SRCPWCMat )
    call ArrayMat(3,1)%BuildUpMatrix( PWCLocMat ); deallocate( PWCLocMat )
    call ArrayMat(3,2)%BuildUpMatrix( PWCSRCMat ); deallocate( PWCSRCMat )
    call ArrayMat(3,3)%BuildUpMatrix( PWCPWCMat ); deallocate( PWCPWCMat )
    call OpMat%BuildUpMatrix( ArrayMat )
    deallocate( ArrayMat )
    !
  end subroutine ClassSESSESBlockAssembleChChLastBs


  !> Assemble in one matrix the blocks for a given pair of symmetric
  !! channels, each corresponding to either the source+polycentric CC space, 
  !! or to a monocentric CC space for a given parent ion.
  !! It does not reorder the Bsplines
  !!
  subroutine ClassSESSESBlockAssembleChCh( Self, OpMat, PIIndexBra, PIIndexKet )
    !> Class to be assemble.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    !> Operator matrix in the symmetric electronic space.
    class(ClassMatrix)        , intent(out)   :: OpMat
    integer                   , intent(in)    :: PIIndexBra
    integer                   , intent(in)    :: PIIndexKet
    !
    integer :: i, j, k, n, IonIndexBra, IonIndexKet
    integer :: NumSRCBra, NumSRCKet, NumPWCBra, NumPWCKet, NumLocBra, NumLocKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:), LocSRCMat(:,:), LocPWCMat(:,:)
    type(ClassMatrix), allocatable :: SRCLocMat(:,:), SRCSRCMat(:,:), SRCPWCMat(:,:)
    type(Classmatrix), allocatable :: PWCLocMat(:,:), PWCSRCMat(:,:), PWCPWCMat(:,:)
    type(ClassMatrix), allocatable :: ArrayMat(:,:)
    !XCHEMQC
    character*32 :: ColumnLabel
    character*32 :: RowLabel
    !END XCEHMQC

    !
    if ( allocated(Self%SRCSRCBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    NumLocBra = 0; if( PIIndexBra == 0 ) NumLocBra = 1
    NumLocKet = 0; if( PIIndexKet == 0 ) NumLocKet = 1
    NumPWCBra = 0; if( PIIndexBra > 0  ) NumPWCBra = size( Self%PWCSRCBlock, 1 )
    NumPWCKet = 0; if( PIIndexKet > 0  ) NumPWCKet = size( Self%SRCPWCBlock, 2 )
    NumSRCBra = size( Self%SRCSRCBlock, 1 )
    NumSRCKet = size( Self%SRCSRCBlock, 2 )

    allocate( LocLocMat(NumLocBra,NumLocKet) )
    allocate( LocSRCMat(NumLocBra,NumSRCKet) )
    allocate( LocPWCMat(NumLocBra,NumPWCKet) )
    allocate( SRCLocMat(NumSRCBra,NumLocKet) )
    allocate( SRCSRCMat(NumSRCBra,NumSRCKet) )
    allocate( SRCPWCMat(NumSRCBra,NumPWCKet) )
    allocate( PWCLocMat(NumPWCBra,NumLocKet) )
    allocate( PWCSRCMat(NumPWCBra,NumSRCKet) )
    allocate( PWCPWCMat(NumPWCBra,NumPWCKet) )

    !.. < * | O | loc >
    do j = 1, NumLocKet
       !
       !.. < loc | O | loc >
       do i = 1, NumLocBra
          LocLocMat(i,j) = Self%LocLocBlock(i,j)%FetchBlock( )
          !XCHEMQC Set Block Names
          ColumnLabel="Local State #"
          RowLabel="Local State #"
          call LocLocMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true. )
          call LocLocMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true. )
          !END XCHEMQC
       end do
       !
       !.. Cycle over the parent ions
       do i = 1, NumSRCBra
          !
          !.. < src | O | loc >
          SRCLocMat(i,j) = Self%SRCLocBlock(i,j)%FetchBlock( )
          !if ( Self%DecoupleChannels ) SRCLocMat(i,j) = 0.d0
          !XCHEMQC
          ColumnLabel="Local State #"
          if( PIIndexBra == 0 )then
             IonIndexBra = i
          else
             IonIndexBra = PIIndexBra
          endif
          RowLabel   =trim(Self%SpaceBra%GetPILabel(IonIndexBra))//" x GO # "
          call SRCLocMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
          call SRCLocMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
          !END XCHEMQC
          !.. < pwc | O | loc >
          do n = 1, NumPWCBra
             !.. < pwc Xlm | O | loc >
             PWCLocMat(n,j) = Self%PWCLocBlock(n,j)%FetchBlock()
             if ( Self%PWCLocBlock(n,j)%IsBoxOnly()    ) call PWCLocMat(n,j)%RemoveRows(1,'END')
             if ( Self%PWCLocBlock(n,j)%IsBraBoxOnly() ) call PWCLocMat(n,j)%RemoveRows(1,'END')
             !XCHEMQC
             ColumnLabel="Local State #"
             write(RowLabel,'(a,i0,a,i0)') &
                  trim(        self%SpaceBra%GetPILabel(IonIndexBra))//&
                  " x BSO l=", self%SpaceBra%GetL      (IonIndexBra,n),&
                  " m="      , self%SpaceBra%GetM      (IonIndexBra,n)
             call PWCLocMat(n,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
             call PWCLocMat(n,j)%SetRowLabels   (RowLabel)
             !END XCHEMQC

          end do
       end do
    end do

    !.. < * | O | src >
    do j = 1, NumSRCKet

       if( PIIndexKet == 0 )then
          IonIndexKet = j
       else
          IonIndexKet = PIIndexKet
       endif

       !.. < loc | O | src >
       do i = 1, NumLocBra
          LocSRCMat(i,j) = Self%LocSRCBlock(i,j)%FetchBlock( )
          !if ( Self%DecoupleChannels ) LocSRCMat(i,j) = 0.d0
          !XCHEMQC
          ColumnLabel=trim(self%SpaceKet%GetPILabel(IonIndexKet))//" x GO # "
          RowLabel="Local State #"
          call LocSRCMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
          call LocSRCMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
          !END XCHEMQC
       end do

       !.. < src | O | src >
       do i = 1, NumSRCBra

          if( PIIndexBra == 0 )then
             IonIndexBra = i
          else
             IonIndexBra = PIIndexBra
          endif

          SRCSRCMat(i,j) = Self%SRCSRCBlock(i,j)%FetchBlock( )
          !XCHEMQC
          ColumnLabel=trim(self%SpaceKet%GetPILabel(IonIndexKet))//" x GO # "
          RowLabel   =trim(self%SpaceBra%GetPILabel(IonIndexBra))//" x GO # "
          call SRCSRCMat(i,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
          call SRCSRCMat(i,j)%SetRowLabels   (RowLabel   , enumerate = .true.)
          !END XCHEMQC
          do n = 1, NumPWCBra
             !.. < pwc Xlm | O | src >
             PWCSRCMat(n,j) = Self%PWCSRCBlock(n,j)%FetchBlock( )
             if ( Self%PWCSRCBlock(n,j)%IsBoxOnly()    ) call PWCSRCMat(n,j)%RemoveRows(1,'END')
             if ( Self%PWCSRCBlock(n,j)%IsBraBoxOnly() ) call PWCSRCMat(n,j)%RemoveRows(1,'END')
             !XCHEMQC
             ColumnLabel=trim(self%SpaceKet%GetPILabel(IonIndexKet))//" x GO # "
             write(RowLabel,'(a,i0,a,i0)') &
                  trim(        self%SpaceBra%GetPILabel(IonIndexBra))//&
                  " x BSO l=", self%SpaceBra%GetL      (IonIndexBra,n),&
                  " m="      , self%SpaceBra%GetM      (IonIndexBra,n)
             call PWCSRCMat(n,j)%SetColumnLabels(ColumnLabel, enumerate = .true.)
             call PWCSRCMat(n,j)%SetRowLabels   (RowLabel)
             !END XCHEMQC
          end do

       end do

    end do


    !.. < * | O | pwc >
    !XCHEMQC
    write(6,'(a)') "-----  < * | O | pwc > -----"
    !END XCHEMQC

    !.. Only considers a single ion
    if( PIIndexKet > 0 )then

       do k = 1, NumPWCKet

          !.. < loc | O | pwc Xlm >
          do i = 1, NumLocBra

             LocPWCMat(i,k) = Self%LocPWCBlock(i,k)%FetchBlock( )
             !if ( Self%DecoupleChannels ) LocPWCMat(i,CounterKet) = 0.d0
             if ( Self%LocPWCBlock(i,k)%IsBoxOnly()    ) call LocPWCMat(i,k)%RemoveColumns(1,'END')
             if ( Self%LocPWCBlock(i,k)%IsKetBoxOnly() ) call LocPWCMat(i,k)%RemoveColumns(1,'END')
             !XCHEMQC
             write(ColumnLabel, '(a,i0,a,i0)') &
                  trim(        self%SpaceKet%GetPILabel( PIIndexKet ) )//&
                  " x BSO l=", self%SpaceKet%GetL      ( PIIndexKet, k ),&
                  " m="      , self%SpaceKet%GetM      ( PIIndexKet, k )
             RowLabel="Loc"
             call LocPWCMat(i,k)%SetColumnLabels(ColumnLabel)
             call LocPWCMat(i,k)%SetRowLabels   (RowLabel, enumerate = .true.)
             !END XCHEMQC
          end do

          !.. < src | O | pwc Xlm >
          do i = 1, NumSRCBra
             !
             if( PIIndexBra == 0 )then
                IonIndexBra = i
             else
                IonIndexBra = PIIndexBra
             endif
             !
             SRCPWCMat(i,k) = Self%SRCPWCBlock(i,k)%FetchBlock( )
             if ( Self%SRCPWCBlock(i,k)%IsBoxOnly()    ) call SRCPWCMat(i,k)%RemoveColumns(1,'END')
             if ( Self%SRCPWCBlock(i,k)%IsKetBoxOnly() ) call SRCPWCMat(i,k)%RemoveColumns(1,'END')
             !XCHEMQC
             write(ColumnLabel, '(a,i0,a,i0)') &
                  trim(        self%SpaceKet%GetPILabel( PIIndexKet ) )//&
                  " x BSO l=", self%SpaceKet%GetL      ( PIIndexKet, k ),&
                  " m="      , self%SpaceKet%GetM      ( PIIndexKet, k )
             RowLabel=trim(self%SpaceBra%GetPILabel( IonIndexBra ))//" x GO # "
             call SRCPWCMat(i,k)%SetColumnLabels(ColumnLabel)
             call SRCPWCMat(i,k)%SetRowLabels   (RowLabel, enumerate = .true.)
             !END XCHEMQC
          enddo

          !.. < pwc Xlm | O | pwc Xlm >
          if( PIIndexBra > 0 )then

             do n = 1, NumPWCBra

                PWCPWCMat(n,k) = Self%PWCPWCBlock(n,k)%FetchBlock( )

                if ( Self%PWCPWCBlock(n,k)%IsBoxOnly() .or. &
                     Self%PWCPWCBlock(n,k)%IsKetBoxOnly() ) then
                    call PWCPWCMat(n,k)%RemoveColumns(1,'END')
                endif

                if ( Self%PWCPWCBlock(n,k)%IsBoxOnly() .or. &
                     Self%PWCPWCBlock(n,k)%IsBraBoxOnly() ) then
                    call PWCPWCMat(n,k)%RemoveRows(1,'END')
                endif

                !XCHEMQC
                write(ColumnLabel, '(a,i0,a,i0)') &
                     trim(        self%SpaceKet%GetPILabel( PIIndexKet ) )//&
                     " x BSO l=", self%SpaceKet%GetL      ( PIIndexKet, k ),&
                     " m="      , self%SpaceKet%GetM      ( PIIndexKet, k )
                write(RowLabel,    '(a,i0,a,i0)') &
                     trim(        self%SpaceBra%GetPILabel( PIIndexBra ) )//&
                     " x BSO l=", self%SpaceBra%GetL      ( PIIndexBra, n ),&
                     " m=",       self%SpaceBra%GetM      ( PIIndexBra, n )
                call PWCPWCMat(n,k)%SetColumnLabels(ColumnLabel)
                call PWCPWCMat(n,k)%SetRowLabels   (RowLabel)
                !END XCHEMQC

             end do

          endif

       enddo

    endif
    allocate( ArrayMat(3,3) )
    call ArrayMat(1,1)%BuildUpMatrix( LocLocMat ); deallocate( LocLocMat )
    call ArrayMat(1,2)%BuildUpMatrix( LocSRCMat ); deallocate( LocSRCMat )
    call ArrayMat(1,3)%BuildUpMatrix( LocPWCMat ); deallocate( LocPWCMat )
    call ArrayMat(2,1)%BuildUpMatrix( SRCLocMat ); deallocate( SRCLocMat )
    call ArrayMat(2,2)%BuildUpMatrix( SRCSRCMat ); deallocate( SRCSRCMat )
    call ArrayMat(2,3)%BuildUpMatrix( SRCPWCMat ); deallocate( SRCPWCMat )
    call ArrayMat(3,1)%BuildUpMatrix( PWCLocMat ); deallocate( PWCLocMat )
    call ArrayMat(3,2)%BuildUpMatrix( PWCSRCMat ); deallocate( PWCSRCMat )
    call ArrayMat(3,3)%BuildUpMatrix( PWCPWCMat ); deallocate( PWCPWCMat )

    call OpMat%BuildUpMatrix( ArrayMat )
    !XCHEMQC
    write(6,'(A,L)') "Columns Labelled?", OpMat%ColumnsLabelled()
    deallocate( ArrayMat )
    !
  end subroutine ClassSESSESBlockAssembleChCh





  !> Assemble in one matrix all the QC blocks in ClassSESSESBlock class.
  subroutine ClassSESSESBlockAssembleQC( Self, LoadLocStates, OpMat )
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    logical                   , intent(in)    :: LoadLocStates
    !> Space of the Bra functions.
    class(ClassMatrix)        , intent(out)   :: OpMat
    !
    integer :: i, j, LocIndex
    integer :: NumSRCBra, NumSRCKet, NumLocBra, NumLocKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:), LocSRCMat(:,:), SRCLocMat(:,:), SRCSRCMat(:,:)
    type(ClassMatrix) :: FullLocLoc, FullLocSRC, FullSRCLoc, FullSRCSRC
    type(ClassMatrix), allocatable :: ArrayMat(:,:)
    !
    if ( allocated(Self%LocLocBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !

    if ( LoadLocStates ) then
       allocate( LocLocMat(NumLocBra,NumLocKet) )
       allocate( LocSRCMat(NumLocBra,NumSRCKet) )
       allocate( SRCLocMat(NumSRCBra,NumLocKet) )
    end if
    allocate( SRCSRCMat(NumSRCBra,NumSRCKet) )

    if ( LoadLocStates ) then
       !.. < * | O | loc >
       do j = 1, NumLocKet
          !.. < loc | O | loc >
          do i = 1, NumLocBra
             LocLocMat(i,j) = Self%LocLocBlock(i,j)%FetchBlock( )
          end do
          !.. Sum over the parent ions
          do i = 1, NumSRCBra
             !.. < src | O | loc >
             SRCLocMat(i,j) = Self%SRCLocBlock(i,j)%FetchBlock( )
          end do
       end do
    end if
    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       if ( LoadLocStates ) then
          !.. < loc | O | src >
          do i = 1, NumLocBra
             LocSRCMat(i,j) = Self%LocSRCBlock(i,j)%FetchBlock( )
          end do
       end if
       do i = 1, NumSRCBra
          !.. < src | O | src >
          SRCSRCMat(i,j) = Self%SRCSRCBlock(i,j)%FetchBlock( )
       end do
    end do


    if ( LoadLocStates ) then
       !
       call FullLocLoc%BuildUpMatrix( LocLocMat )
       deallocate( LocLocMat )
       call FullLocSRC%BuildUpMatrix( LocSRCMat )
       deallocate( LocSRCMat )
       call FullSRCLoc%BuildUpMatrix( SRCLocMat )
       deallocate( SRCLocMat )
       !
    end if
    !
    call FullSRCSRC%BuildUpMatrix( SRCSRCMat )
    deallocate( SRCSRCMat )

    LocIndex = 1
    if ( LoadLocStates ) LocIndex = 0
    
    allocate( ArrayMat(2-LocIndex,2-LocIndex) )
    !
    if ( LoadLocStates ) then
       ArrayMat(1-LocIndex,1-LocIndex) = FullLocLoc
       call FullLocLoc%Free()
       ArrayMat(1-LocIndex,2-LocIndex) = FullLocSRC
       call FullLocSRC%Free()
       ArrayMat(2-LocIndex,1-LocIndex) = FullSRCLoc
       call FullSRCLoc%Free()
    end if
    !
    ArrayMat(2-LocIndex,2-LocIndex) = FullSRCSRC
    call FullSRCSRC%Free()
    
    call OpMat%BuildUpMatrix( ArrayMat )
    deallocate( ArrayMat )

    !
  end subroutine ClassSESSESBlockAssembleQC


  !> Assemble in one matrix all the polycentric Gaussian blocks in ClassSESSESBlock class.
  subroutine ClassSESSESBlockAssemblePoly( Self, LoadLocStates, OpMat )
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    logical                   , intent(in)    :: LoadLocStates
    !> Space of the Bra functions.
    class(ClassMatrix)        , intent(out)   :: OpMat
    !
    integer :: i, j, LocIndex
    integer :: NumSRCBra, NumSRCKet, NumLocBra, NumLocKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:), LocSRCMat(:,:), SRCLocMat(:,:), SRCSRCMat(:,:)
    type(ClassMatrix) :: FullLocLoc, FullLocSRC, FullSRCLoc, FullSRCSRC
    type(ClassMatrix), allocatable :: ArrayMat(:,:)
    !
    if ( allocated(Self%LocLocBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1
    !.. For every symmetric electronic channel only one short range channel is present.
    NumSRCBra = Self%SpaceBra%GetNumChannels()
    NumSRCKet = Self%SpaceKet%GetNumChannels()
    !

    if ( LoadLocStates ) then
       allocate( LocLocMat(NumLocBra,NumLocKet) )
       allocate( LocSRCMat(NumLocBra,NumSRCKet) )
       allocate( SRCLocMat(NumSRCBra,NumLocKet) )
    end if
    allocate( SRCSRCMat(NumSRCBra,NumSRCKet) )

    if ( LoadLocStates ) then
       !.. < * | O | loc >
       do j = 1, NumLocKet
          !.. < loc | O | loc >
          do i = 1, NumLocBra
             LocLocMat(i,j) = Self%LocLocBlock(i,j)%FetchBlock( )
          end do
          !.. Sum over the parent ions
          do i = 1, NumSRCBra
             !.. < src | O | loc >
             SRCLocMat(i,j) = Self%SRCLocBlock(i,j)%FetchBlock( )
          end do
       end do
    end if
    
    !.. < * | O | src >
    do j = 1, NumSRCKet
       if ( LoadLocStates ) then
          !.. < loc | O | src >
          do i = 1, NumLocBra
             LocSRCMat(i,j) = Self%LocSRCBlock(i,j)%FetchBlock( )
          end do
       end if
       do i = 1, NumSRCBra
          !.. < src | O | src >
          SRCSRCMat(i,j) = Self%SRCSRCBlock(i,j)%FetchBlock( )
       end do
    end do


    if ( LoadLocStates ) then
       !
       call FullLocLoc%BuildUpMatrix( LocLocMat )
       deallocate( LocLocMat )
       call FullLocSRC%BuildUpMatrix( LocSRCMat )
       deallocate( LocSRCMat )
       call FullSRCLoc%BuildUpMatrix( SRCLocMat )
       deallocate( SRCLocMat )
       !
    end if
    !
    call FullSRCSRC%BuildUpMatrix( SRCSRCMat )
    deallocate( SRCSRCMat )

    LocIndex = 1
    if ( LoadLocStates ) LocIndex = 0
    
    allocate( ArrayMat(2-LocIndex,2-LocIndex) )
    !
    if ( LoadLocStates ) then
       ArrayMat(1-LocIndex,1-LocIndex) = FullLocLoc
       call FullLocLoc%Free()
       ArrayMat(1-LocIndex,2-LocIndex) = FullLocSRC
       call FullLocSRC%Free()
       ArrayMat(2-LocIndex,1-LocIndex) = FullSRCLoc
       call FullSRCLoc%Free()
    end if
    !
    ArrayMat(2-LocIndex,2-LocIndex) = FullSRCSRC
    call FullSRCSRC%Free()
    
    call OpMat%BuildUpMatrix( ArrayMat )
    deallocate( ArrayMat )

    !
  end subroutine ClassSESSESBlockAssemblePoly


  !> Assemble in one matrix all the localized block in ClassSESSESBlock class. In general just the polycentric submatrix.
  subroutine ClassSESSESBlockAssembleLoc( Self, LoadLocStates, OpMat )
    !> Class to be initialized.
    class(ClassSESSESBlock)   , intent(inout) :: Self
    logical                   , intent(in)    :: LoadLocStates
    !> Space of the Bra functions.
    class(ClassMatrix)        , intent(out)   :: OpMat
    !
    integer :: i, j, LocIndex
    integer :: NumLocBra, NumLocKet
    type(ClassMatrix), allocatable :: LocLocMat(:,:)
    type(ClassMatrix) :: FullLocLoc
    type(ClassMatrix), allocatable :: ArrayMat(:,:)
    !
    if ( allocated(Self%LocLocBlock) ) then
       Self%AvailableBlocks = .true.
    else
       Self%AvailableBlocks = .false.
       return
    end if
    !
    !.. For every symmetric electronic space only one localized channel is present.
    NumLocBra = 1
    NumLocKet = 1

    if ( LoadLocStates ) then
       allocate( LocLocMat(NumLocBra,NumLocKet) )
    else
       call Assert( 'If it is requested the assembling of just '//&
            'the localized orbitals submatrix, then the '//&
            'variable "LoadLocStates" in the program '//&
            'configuration input file, must to be set .true.')
    end if

    !.. < * | O | loc >
    do j = 1, NumLocKet
       !.. < loc | O | loc >
       do i = 1, NumLocBra
          LocLocMat(i,j) = Self%LocLocBlock(i,j)%FetchBlock( )
       end do
       !.. Sum over the parent ions
    end do
    !
    
    call FullLocLoc%BuildUpMatrix( LocLocMat )
    deallocate( LocLocMat )
    !

    allocate( ArrayMat(1,1) )
    !
    ArrayMat(1,1) = FullLocLoc
    call FullLocLoc%Free()
    !
    call OpMat%BuildUpMatrix( ArrayMat )
    deallocate( ArrayMat )
    !
    !
  end subroutine ClassSESSESBlockAssembleLoc



  function ClassSESSESBlockGetStorageDir( Self ) result(Dir)
    class(ClassSESSESBlock), intent(in) :: Self
    character(len=:), allocatable :: Dir
    !
    character(len=:), allocatable :: NucConfDir, CCDir, BraSymLabel, KetSymLabel
    !
    allocate( NucConfDir, source = Self%SpaceBra%GetStorageDir() )
    allocate( CCDir,      source = GetCloseCouplingDir() )
    allocate( BraSymLabel, source = Self%SpaceBra%GetLabel() )
    allocate( KetSymLabel, source = Self%SpaceKet%GetLabel() )
    !
    allocate( Dir, source = &
         AddSlash(NucConfDir)//&
         AddSlash(CCDir)//&
         BraSymLabel//'_'//AddSlash(KetSymLabel) )
    !
  end function ClassSESSESBlockGetStorageDir



end module ModuleSymmetricElectronicSpaceOperators
