!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module ModuleDiagonalize

  use, intrinsic :: ISO_FORTRAN_ENV 
  
  use ModuleErrorHandling

  implicit none

  private

  real(kind(1d0)), parameter :: AIMAG_THRESHOLD = 1.d-14
  logical :: MODULE_DIAG_DEBUG_FLAG = .FALSE.

  private :: Short_DSYEV 
  private :: Short_ZGEEV 

  interface Short_Diag
     module procedure Short_DSYEV, Short_ZGEEV
  end interface Short_Diag
  public :: Short_Diag
  public :: ModuleDiagonalize_SetDebug  
  public :: ModuleDiagonalize_UnsetDebug  

contains
  
  subroutine ModuleDiagonalize_SetDebug()
      MODULE_DIAG_DEBUG_FLAG = .TRUE.
  end subroutine ModuleDiagonalize_SetDebug

  subroutine ModuleDiagonalize_UnsetDebug()
      MODULE_DIAG_DEBUG_FLAG = .FALSE.
  end subroutine ModuleDiagonalize_UnsetDebug

  subroutine Short_DSYEV( N, A, E )
    !
    implicit none
    !
    integer        , intent(in)    :: N
    real(kind(1d0)), intent(inout) :: A(:,:)
    real(kind(1d0)), intent(out)   :: E(:)
    !
    real(kind(1d0)), allocatable :: C(:,:)
    integer :: INFO, LWORK
    real(kind(1d0)), allocatable :: WORK(:)
    character(len=8) :: ERRMSG
    !
    if(N<=0)return
    !
    allocate(C(N,N))
    C=A(1:N,1:N)
    !
    !.. Determines optimal dimension for working space
    LWORK=-1 !.. This means that the call to DSYEV is a size query
    allocate(WORK(1))!.. The optimal size is returned in WORK(1)
    call DSYEV("V","U",N,C,N,E,WORK,LWORK,INFO)
    LWORK=int(WORK(1)+0.5d0)
    deallocate(WORK)
    !
    !.. Dimension the workspace to the optimal size
    !   and performs the diagonalization for real
    allocate(WORK(LWORK))
    call DSYEV("V","U",N,C,N,E,WORK,LWORK,INFO)
    deallocate(WORK)
    if( INFO /=0 )then
       write(ERRMSG,"(i)") INFO
       call ErrorMessage("DSYEV Failed: INFO ="//trim(ERRMSG))
       stop
    endif
    A(1:N,1:N)=C
    deallocate(C)
    !
    return
    !
  end subroutine Short_DSYEV
  

  subroutine Short_ZGEEV(n,A,E)

    implicit none

    integer, intent(in) :: n
    complex(kind(1d0)), intent(inout) :: A(:,:), E(:)

    real   (kind(1d0)), allocatable :: DA(:,:), DE(:)
    complex(kind(1d0))              :: DummyZMat(1,1)
    complex(kind(1d0)), allocatable :: zwork(:), C(:,:)
    real   (kind(1d0)), allocatable :: rwork(:)
    integer           , allocatable :: iperm(:)
    integer                         :: lwork, info
    complex(kind(1d0))              :: zw1
    real   (kind(1d0))              :: norm_Real_A
    real   (kind(1d0))              :: norm_Imag_A
    character(len=512) :: errmsg
    integer :: i


    !.. Check if the matrix is actually real, in which case it 
    !   uses DSYEV to diagonalize it
    !..
    norm_Real_A = sum(abs( dble(A(1:n,1:n))))
    norm_Imag_A = sum(abs(aimag(A(1:n,1:n))))
    if( MODULE_DIAG_DEBUG_FLAG )then
       write(*,*) "|Re(A)|_1 = ", norm_Real_A
       write(*,*) "|Im(A)|_1 = ", norm_Imag_A
    endif
    if( norm_Imag_A < norm_Real_A * AIMAG_THRESHOLD )then
       allocate( DA(n,n), DE(n) )
       DA=dble(A(1:n,1:n))
       DE=0.d0
       call Short_DSYEV(n,DA,DE)
       A(1:n,1:n)=(1.d0,0.d0) * DA
       E(1:n)=(1.d0,0.d0) * DE
       deallocate(DA,DE)
       return
    endif

    allocate(zwork(1))
    allocate(rwork(2*n))
    allocate(C(n,n))
    call ZGEEV('N','V',n,A,n,E,DummyZMat,1,C,n,zwork,-1,rwork,info)
    lwork=int(abs(zwork(1))+1)
    deallocate(zwork)
    allocate(zwork(lwork))
    call ZGEEV('N','V',n,A,n,E,DummyZMat,1,C,n,zwork,lwork,rwork,info)
    if(info/=0)then
       write(errmsg,"(a,i)") "Diagonalization failed, info=",info
       call ErrorMessage(errmsg)
       stop
    endif
    deallocate(zwork)
    deallocate(rwork)
    
    !
    !.. Rescale the right eigenvectors to obtain new right eigenvectors
    !   $U^R$ which, together with left eigenvectors $U^L = (U^R)^*$, 
    !   obey the standard orthogonality condition $(U^L)^\dagger U^R=1$.
    !..
    do i=1,n
       zw1 = (1.d0,0.d0) / sqrt(sum(C(:,i)**2))
       C(:,i) = zw1 * C(:,i)
       if(real(C(1,i))<0.d0) C(:,i)=-C(:,i)
    enddo
    !
    !.. The left eigenvectors $U^L$ of the matrix H obey the 
    !   following relations
    !
    !   1. 
    !   $$
    !       \sum_j (U^L_jk)^\dagger H_{ji} = \lambda_k (U^L_{ik})^\dagger
    !   $$
    !   
    !   2. They are normalized as
    !   $$
    !      \forall j,\qquad \sum_i ( U^L_{ij} )^2 = 1
    !   $$
    !
    !   3. The right eigenvectors, defined with the convention
    !   $$
    !        \sum_j H_{ij} U^R_{jk} = U^R_{ik} \lambda_k,
    !   $$     
    !   are related to the left eigenvectors $U^L$ by the
    !   following relation
    !   $$
    !       U^L_{ij} = ( U^R_{ij} )^*
    !   $$
    !
    !   4. The following further orthonormality relation also holds
    !   $$
    !       ( U^L )^\dagger U^R = 1
    !   $$

    !.. Order the eigenvalues in ascending order of the real part
    !..
    allocate( RWORK( n ), IPERM( n ) )
    RWORK = dble( E )
    do i = 1, n
       IPERM(i) = i
    enddo
    !
    !.. Return the permutation vector IPERM resulting from sorting
    !   RWORK in increasing order and do not sort RWORK.
    !
    !   The permutation is such that IPERM(i) is the index of the
    !   value in the original order of the RWORK array that is in
    !   the i-th location of the sorted order
    !..
    call DPSORT( RWORK, n, IPERM, 1, INFO )
    deallocate( RWORK )
    if( INFO /=0 )then
       write(errmsg,"(a,i5,a)")" Error ",INFO," in DPSORT"
       call ErrorMessage(errmsg)
       stop
    endif

    allocate(zwork(n))
    zwork=E
    do i=1,n
       E(i)=zwork(iperm(i))
       A(:,i)=C(:,iperm(i))
    enddo
    deallocate(C,zwork)
    
    return
  end subroutine Short_ZGEEV


end module ModuleDiagonalize
