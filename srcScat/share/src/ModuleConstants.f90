!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Sets several constants that are used throughout the programs.

module ModuleConstants

  implicit none

  !> Sets the kind of the real or complex parameters to be used.
  integer        , parameter :: DOUBLE= kind(1d0)
  !> Greek \f$\pi\f$.
  real   (DOUBLE), parameter :: PI    = 3.1415926535897932d0
  !> Inverse of fine-structure constant.
  real   (DOUBLE), parameter :: CAU   = 137.03599970
  !> Fine-structure constant.
  real   (DOUBLE), parameter :: ALPHA = 1.d0/CAU
  !> Zero complex number.
  complex(DOUBLE), parameter :: Z0    = (0.d0,0.d0)
  !> Real unit in complex number format.
  complex(DOUBLE), parameter :: Z1    = (1.d0,0.d0)
  !> Imaginary unit in complex number format.
  complex(DOUBLE), parameter :: Zi    = (0.d0,1.d0)
  !> Greek \f$1/\sqrt(2\pi)\f$.
  real   (DOUBLE), parameter :: ONE_OVER_SQRT_2PI = 1.d0/sqrt(2.d0*3.1415926535897932d0)
  !> Bohr Radius in meters
  real   (DOUBLE), parameter :: BOHR_RADIUS = 5.2917721067d-11
 
  ! Conversion factors.

  !> Time conversion factor from a.u. to eV.
  real(kind(1d0)), parameter :: Energy_au_to_eV=27.21138602d0
  !> Time conversion factor from a.u. to SI (seconds).
  real(kind(1d0)), parameter :: TimeConvAUtoSI=2.418884326505d-17
  !> Time conversion factor from femtoseconds to a.u.
  real(kind(1d0)), parameter :: TimeConvFStoAU=1.d-15/TimeConvAUtoSI
  !> Energy conversion factor from a.u. to SI (Joules).
  real(kind(1d0)), parameter :: EnergyConvAUtoSI=4.35974417d-18
  !> Length conversion factor from a.u. to SI (meter).
  real(kind(1d0)), parameter :: LengthConvAUtoSI=5.291772192d-11
  !> Intensity conversion factor from PW/cm² to a.u.
  real(kind(1d0)), parameter :: IntensityConvPWCM2toAU=1d15/EnergyConvAUtoSI*TimeConvAUtoSI*(LengthConvAUtoSI)**2/1d-4


end module ModuleConstants
